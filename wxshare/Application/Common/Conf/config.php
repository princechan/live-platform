<?php

return [
    //'AUTOLOAD_NAMESPACE' => ['Addons' => ONETHINK_ADDON_PATH], //扩展模块列表
    'DEFAULT_MODULE' => 'Home',
    'MODULE_DENY_LIST' => ['Common', 'User'],
    'MODULE_ALLOW_LIST' => ['Home', 'Admin'],
    'DEFAULT_FILTER' => '',
    //mysql
    'DB_TYPE' => DATABASE_TYPE,
    'DB_HOST' => DATABASE_HOST,
    'DB_NAME' => DATABASE_NAME,
    'DB_USER' => DATABASE_USER,
    'DB_PWD' => DATABASE_PWD,
    'DB_PORT' => DATABASE_PORT,
    'DB_PREFIX' => DATABASE_PREFIX,
    //redis
    'REDIS_HOST' => NOSQL_REDIS_HOST,
    'REDIS_PORT' => NOSQL_REDIS_PORT,
    'REDIS_AUTH' => NOSQL_REDIS_AUTH,
];
