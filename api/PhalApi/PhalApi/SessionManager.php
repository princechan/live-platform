<?php
/**
 * 暂时不启用
 * User: kobi.h
 */

class PhalApi_SessionManager extends SessionHandler
{
    private $host;
    private $port;
    private $password;
    /**
     * @var \Redis
     */
    private $redis;
    private $expireTime = 3;

    public function __construct()
    {
        $this->host = DI()->config->get('app.REDIS_HOST');
        $this->port = DI()->config->get('app.REDIS_PORT');
        $this->password = DI()->config->get('app.REDIS_AUTH');

        $this->initRedis();

        ini_set('session.save_handler', 'redis');
        ini_set('session.save_path', "tcp://{$this->host}:{$this->port}?auth={$this->password}");

        session_set_save_handler(
            [$this, 'open'],
            [$this, 'close'],
            [$this, 'read'],
            [$this, 'write'],
            [$this, 'destroy'],
            [$this, 'gc']
        );
        session_start();
    }

    private function initRedis()
    {
        $this->redis = new \Redis();
        $this->redis->pconnect($this->host, $this->port);
        $this->redis->auth($this->password);
        return $this->redis;
    }

    public function open($path, $name)
    {
        return true;
    }

    public function close()
    {
        return true;
    }

    public function read($id)
    {
        return $this->redis->get($id) ?: '';
    }

    public function write($id, $data)
    {
        if ($this->redis->set($id, $data, $this->expireTime)) {
            return true;
        }
        return false;
    }

    public function destroy($id)
    {
        if ($this->redis->delete($id)) {
            return true;
        }
        return false;
    }

    public function gc($maxlifetime)
    {
        return true;
    }

    public function __destruct()
    {
        session_write_close();
    }

}
