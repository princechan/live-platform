<?php

/**
 * User: kobi.h
 */
class Model_Auth_Login extends Model_Common
{
    const VERIFICATION_CODE = 'ver_code';
    const VERIFICATION_CODE_TIMEOUT = 300;

    /**
     * A6配置
     */
    const A6_PRODUCT_ID = 'A06';
    const A6_PRIVATE_KEY = '#@980live';

    const KEY_TOKEN = 'token_';
    const KEY_USER_INFO = 'userinfo_';

    protected $fields = 'id,user_nicename,avatar,avatar_thumb,sex,
    signature,coin,consumption,user_status,login_type,province,
    city,birthday,livecoin,iswhite';

    public static $quDao = [
        'A02' => '102',
        'C01' => '301',
    ];

    public static $codeFor = [
        'reg', 'login',
    ];

    /**
     * 会员登录
     * @param $user_login
     * @param $user_pass
     * @return array|bool
     */
    public function userLogin($user_login, $user_pass)
    {
        $rst = $this->loginC01($user_login, $user_pass);
        //0-失败，1-成功
        if ($rst['status'] == 0) {
            $this->setError('账号或密码错误');
            return false;
        }

        $info = DI()->notorm->users
            ->select($this->fields . ',user_pass')
            ->where('user_login=? and user_type="2"', $user_login)
            ->fetchOne();
        if (!$info) {
            $this->setError('用户不存在');
            return false;
        }

        if ($info['user_status'] == '0') {
            $this->setError('该账号已被禁用');
            return false;
        }

        unset($info['user_status']);
        $info['level'] = $this->getLevel($info['consumption']);
        unset($info['consumption']);
        $token = md5(md5($info['id'] . $user_login . time()));

        $info['token'] = $token;
        $info['avatar'] = Model_Helper_Func::getUrl($info['avatar']);
        $info['avatar_thumb'] = Model_Helper_Func::getUrl($info['avatar_thumb']);
        $info = Model_Auth_User::getInstance()->unifiedLoginData($info);

        $this->updateToken($info['id'], $token);
        Model_Auth_User::getInstance()->setUserInfoByToken($token, $info);

        return $info;
    }

    /**
     * 第三方用户注册、登陆Host
     */
    public static function getUserAuthHost()
    {
        return ENV_DEV ? 'http://www.a06new.com' : 'https://www.8869ks.com';
    }

    private function loginC01($username, $password)
    {
        if (ENV_DEV) {
            return [
                'status' => 1,
            ];
        }

        $url = self::getUserAuthHost() . '/api/marketing/login.htm';
        $data['timestamp'] = time();
        $data['token'] = md5(self::A6_PRODUCT_ID . $username . $data['timestamp'] . self::A6_PRIVATE_KEY);
        $data['login_name'] = $username;
        $data['password'] = $password;
        $result = Model_Helper_Curl::getInstance()->post($url, $data);
        switch ((int)$result['status']) {
            case 100:
                $msg = 'timestamp is not null';
                break;
            case 101:
                $msg = 'token is not null';
                break;
            case 102:
                $msg = 'token is error';
                break;
            case 103:
                $msg = '用户名不能为空';
                break;
            case 104:
                $msg = '有非法字符';
                break;
            case 105:
                $msg = '用户名必须以小写字母c开头';
                break;
            case 106:
                $msg = '密码不能为空';
                break;
            case 107:
                $msg = '用户名不存在';
                break;
            case 108:
                $msg = '用户名或密码错'; //第三方失败
                break;
            case 200:
                $msg = '登录成功';
                break;
            default:
                $msg = '登录失败';
                break;
        }

        return [
            'status' => (int)($result['status'] == 200),
            'msg' => $msg,
        ];
    }

    public function userLogin1($username, $password, $level, $roomid, $token)
    {
        $key = file_get_contents('/data/config/ybkey.ini');
        $sign = md5("username={$username}password={$password}level={$level}" . $key);

        if ($sign != $token) {
            return 1003;
        }

        $info = DI()->notorm
            ->users
            ->select($this->fields)
            ->where("user_login=? and user_pass=? and user_type='2'", $username, $password)
            ->fetchOne();

        if (!$info) {//用户不存在

            //加入一条数据
            $data = [
                'user_login' => $username,
                'mobile' => '',
                'user_nicename' => '手机用户' . substr($username, -4),
                'user_pass' => $password,
                'signature' => '这家伙很懒，什么都没留下',
                'avatar' => '/default.jpg',
                'avatar_thumb' => '/default_thumb.jpg',
                'last_login_ip' => '',
                'create_time' => date("Y-m-d H:i:s"),
                'last_login_time' => date("Y-m-d H:i:s"),
                'user_status' => 1,
                "user_type" => 2,//会员
            ];
            DI()->notorm->users->insert($data);
            $info = DI()->notorm->users->where("user_login=? and user_pass=? and user_type='2'", $username, $password)->fetchOne();

            unset($info['user_status']);
            $info['level'] = $this->getLevel($info['consumption']);
            unset($info['consumption']);
            $token = md5("username={$username}password={$password}level={$level}" . $key);
            $info['token'] = $token;
            $info['avatar'] = Model_Helper_Func::getUrl($info['avatar']);
            $info['avatar_thumb'] = Model_Helper_Func::getUrl($info['avatar_thumb']);

            //调用公共方法通过三方接口获取用户余额
            $coin = $this->getApiUserCoin($info['user_login'], $info['user_pass']);

            if ($coin == 0) {
                $info['coin'] = 0;
            } else {
                $info['coin'] = $coin;
            }

            $this->updateToken($info['id'], $token);
        } else {//用户存在
            unset($info['user_status']);
            $info['level'] = $this->getLevel($info['consumption']);//获取用户的等级
            unset($info['consumption']);
            $token = md5("username={$username}password={$password}level={$level}" . $key);
            $info['token'] = $token;
            $info['avatar'] = Model_Helper_Func::getUrl($info['avatar']);
            $info['avatar_thumb'] = Model_Helper_Func::getUrl($info['avatar_thumb']);
            $info['user_login'] = $username;
            $info['user_pass'] = $password;

            //调用公共方法通过三方接口获取用户余额
            $coin = $this->getApiUserCoin($info['user_login'], $info['user_pass']);

            if ($coin == 0) {
                $info['coin'] = 0;
            } else {
                $info['coin'] = $coin;
            }

            $this->updateToken($info['id'], $token);
        }

        return $info;
    }

    private function registerC01($username, $password)
    {
        if (ENV_DEV) {
            return [
                'status' => 1,
            ];
        }

        $url = self::getUserAuthHost() . "/api/marketing/register.htm";
        $data['timestamp'] = time();
        $data['token'] = md5(self::A6_PRODUCT_ID . $username . $data['timestamp'] . self::A6_PRIVATE_KEY);
        $data['login_name'] = $username;
        $data['password'] = $password;
        $data['ip_address'] = $_SERVER['REMOTE_ADDR'] ?: '0.0.0.0';
        $data['domain_name'] = 'www.xsm7.com';
        $result = Model_Helper_Curl::getInstance()->post($url, $data);
        switch ((int)$result['status']) {
            case 100:
                $msg = '时间戳为空';
                break;
            case 101:
                $msg = 'token为空';
                break;
            case 102:
                $msg = 'token错误';
                break;
            case 103:
                $msg = '用户名为空';
                break;
            case 104:
                $msg = '有非法字符';
                break;
            case 105:
                $msg = '用户名必须以小写字母c开头';
                break;
            case 106:
                $msg = '密码为空';
                break;
            case 107:
                $msg = '用户名长度必须为4~11位';
                break;
            case 108:
                $msg = '密码长度必须为6~16位';
                break;
            case 109:
                $msg = 'ip_address为空';
                break;
            case 110:
                $msg = 'domain_name为空';
                break;
            case 111:
                $msg = '用户名已存在'; //login_name does not exist
                break;
            case 112:
                $msg = '第三方注册失败';
                break;
            case 200:
                $msg = '注册成功';
                break;
            default:
                $msg = '注册失败';
                break;
        }

        return [
            'status' => (int)($result['status'] == 200),
            'message' => $result['msg'],
            'msg' => $msg,
        ];
    }

    /**
     * 会员注册
     * @param $user_login
     * @param $user_pass
     * @return array
     */
    public function userReg($user_login, $user_pass)
    {
        $rst = $this->registerC01($user_login, $user_pass);
        if ($rst['status'] == 0) {
            return $rst;
        }

        $channel = DI()->config->get('sys.user_channel');

        $data = [
            'user_login' => $user_login,
            'mobile' => $user_login,
            'user_nicename' => '手机用户' . substr($user_login, -4),
            'user_pass' => '',
            'signature' => '这家伙很懒，什么都没留下',
            'avatar' => '/default.jpg',
            'avatar_thumb' => '/default_thumb.jpg',
            'last_login_ip' => '',
            'create_time' => date("Y-m-d H:i:s"),
            'last_login_time' => date("Y-m-d H:i:s"),
            'user_status' => 1,
            "user_type" => 2,//会员
            'user_email' => '',
            'login_type' => 'phone',
            "qudao_id" => empty(self::$quDao[$channel]) ?: 102
        ];

        $isexist = DI()->notorm->users
            ->select('id')
            ->where('user_login=? and user_type="2"', $user_login)
            ->fetchOne();
        if ($isexist) {
            $rst['status'] = 0;
            $rst['msg'] = '用户已存在';
            return $rst;
        }

        $rs = DI()->notorm->users->insert($data);

        $info = [
            'id' => $rs['id'],
            'user_nicename' => $data['user_nicename'],
            'avatar' => Model_Helper_Func::getUrl($data['avatar']),
            'avatar_thumb' => Model_Helper_Func::getUrl($data['avatar_thumb']),
            'sex' => '2',
            'signature' => $data['signature'],
            'coin' => '0',
            'livecoin' => '0',
            'login_type' => $data['login_type'],
            'province' => '',
            'city' => '',
            'birthday' => '',
            'iswhite' => '0',
            'user_pass' => '',
            'level' => '1',
        ];

        $token = md5(md5($info['id'] . $user_login . time()));
        $info['token'] = $token;

        $info = Model_Auth_User::getInstance()->unifiedLoginData($info);

        $this->updateToken($info['id'], $token);
        Model_Auth_User::getInstance()->setUserInfoByToken($token, $info);

        $rst['data'] = $info;
        return $rst;
    }

    /**
     * 找回密码
     * @param $user_login
     * @param $user_pass
     * @return int
     */
    public function userFindPass($user_login, $user_pass)
    {
        $isexist = DI()->notorm->users
            ->select('id')
            ->where('user_login=? and user_type="2"', $user_login)
            ->fetchOne();
        if (!$isexist) {
            return 1006;
        }
        $user_pass = $this->setPass($user_pass);

        return (new Model_Auth_User())->update($isexist['id'], ['user_pass' => $user_pass]);
    }

    /**
     * 第三方会员登录
     * @param $openid
     * @param $type
     * @param $nickname
     * @param $avatar
     * @return int
     */
    public function userLoginByThird($openid, $type, $nickname, $avatar)
    {
        $info = DI()->notorm->users
            ->select($this->fields)
            ->where('openid=? and login_type=? and user_type="2"', $openid, $type)
            ->fetchOne();

        if (!$info) {
            /* 注册 */
            $user_pass = 'yunbaokeji';
            $user_pass = $this->setPass($user_pass);
            $user_login = $type . '_' . time() . rand(100, 999);

            if (!$nickname) {
                $nickname = $type . '用户-' . substr($openid, -4);
            } else {
                $nickname = urldecode($nickname);
            }
            if (!$avatar) {
                $avatar = '/default.jpg';
                $avatar_thumb = '/default_thumb.jpg';
            } else {
                $avatar = urldecode($avatar);
                $avatar_thumb = $avatar;
            }

            $data = [
                'user_login' => $user_login,
                'user_nicename' => $nickname,
                'user_pass' => $user_pass,
                'signature' => '这家伙很懒，什么都没留下',
                'avatar' => $avatar,
                'avatar_thumb' => $avatar_thumb,
                'last_login_ip' => '',
                'create_time' => date("Y-m-d H:i:s"),
                'last_login_time' => date("Y-m-d H:i:s"),
                'user_status' => 1,
                'openid' => $openid,
                'login_type' => $type,
                "user_type" => 2,//会员
            ];

            $rs = DI()->notorm->users->insert($data);

            $info['id'] = $rs['id'];
            $info['user_nicename'] = $data['user_nicename'];
            $info['avatar'] = Model_Helper_Func::getUrl($data['avatar']);
            $info['avatar_thumb'] = Model_Helper_Func::getUrl($data['avatar_thumb']);
            $info['sex'] = '2';
            $info['signature'] = $data['signature'];
            $info['coin'] = '0';
            $info['login_type'] = $data['login_type'];
            $info['province'] = '';
            $info['city'] = '';
            $info['birthday'] = '';
            $info['consumption'] = '0';
            $info['user_status'] = 1;
        }

        if ($info['user_status'] == '0') {
            return 1001;
        }

        unset($info['user_status']);
        $info['level'] = $this->getLevel($info['consumption']);
        unset($info['consumption']);

        $token = md5(md5($info['id'] . $openid . time()));
        $info['token'] = $token;
        $this->updateToken($info['id'], $token);

        return $info;
    }

    /**
     * 更新token 登陆信息
     * @param $uid
     * @param $token
     * @return int
     */
    public function updateToken($uid, $token)
    {
        $this->delcache([self::KEY_TOKEN . $uid]);

        (new Model_Auth_User())->update($uid, [
            "token" => $token,
            "expiretime" => time() + 60 * 60 * 24 * 300,
            'last_login_time' => date("Y-m-d H:i:s"),
            "last_login_ip" => '',
        ]);

        return 1;
    }

    public function checkIsLogin($uid)
    {
        $startDate = strtotime(date('Y-m-d 00:00:00'));//今日开始时间
        $endDate = strtotime(date('Y-m-d 23:59:59'));//今日结束时间
        $info = DI()->notorm->users_login_lists
            ->where("uid={$uid} and logintime>={$startDate} and logintime<={$endDate}")
            ->fetchOne();

        if ($info) {
            return 1;
        } else {
            $data = [
                'uid' => $uid,
                'logintime' => time()
            ];
            $rs = DI()->notorm->users_login_lists->insert($data);
            if ($rs) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    private function buildVerificationCodeKey($for)
    {
        $num = Model_Auth_User::getDeviceNum();
        if (!$num) {
            $this->setError('设备号不存在');
            return false;
        }

        return self::VERIFICATION_CODE . $num . $for;
    }

    /**
     * @param $code
     * @param string $for
     * @return bool
     */
    public function validateCode($code, $for = 'reg')
    {
        $key = $this->buildVerificationCodeKey($for);
        if (!$key) {
            return false;
        }

        $redisCode = Model_Helper_Redis::getInstance()->get($key);
        if (!$redisCode) {
            $this->setError('验证码不存在');
            return false;
        }

        if (strtolower($redisCode) == strtolower($code)) {
            return true;
        }

        $this->setError('验证码错误');
        return false;
    }

    /**
     * 获取验证码
     * @param $width
     * @param $height
     * @param string $for
     * @return bool
     */
    public function getVerificationCode($width, $height, $for = 'reg')
    {
        if (!in_array($for, self::$codeFor)) {
            $this->setError('for参数不合法');
            return false;
        }

        if ($width < 120) {
            $width = 120;
        }
        if ($height < 40) {
            $height = 40;
        }

        $fontSize = 22;
        $img = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
        $color = imagecolorallocate($img, mt_rand(1, 150), mt_rand(1, 150), mt_rand(1, 150));
        imagefill($img, 0, 0, $white);

        $code = '';
        $codeS = 'abcdefghijklmnpqrtuvwxyz234679ACDEFGHIJKLMNPQRTUVWXYZ';
        for ($i = 0; $i < 4; $i++) {
            $code .= $codeS[rand(0, strlen($codeS) - 1)];
        }

        // 验证码使用随机字体
        $ttfPath = WEB_PATH . '/Api/Controller/Verify/ttfs/';
        $dir = dir($ttfPath);
        $ttfs = [];
        while (false !== ($file = $dir->read())) {
            if ($file[0] != '.' && substr($file, -4) == '.ttf') {
                $ttfs[] = $file;
            }
        }
        $dir->close();

        $fontttf = $ttfs[0];
        $fontttf = $ttfPath . $fontttf;
        $codeSet = '2345678abcdefhijkmnpqrstuvwxyz';
        for ($i = 0; $i < 10; $i++) {
            //杂点颜色
            $noiseColor = imagecolorallocate($img, mt_rand(150, 225), mt_rand(150, 225), mt_rand(150, 225));
            for ($j = 0; $j < 5; $j++) {
                // 绘杂点
                imagestring($img, 5, mt_rand(-10, $width), mt_rand(-10, $height), $codeSet[mt_rand(0, 29)], $noiseColor);
            }
        }

        $codeNX = -5;
        for ($i = 0; $i < 4; $i++) {
            $codeNX += $fontSize;
            imagettftext($img, $fontSize, mt_rand(-15, 15), $codeNX, $fontSize * 1.6, $color, $fontttf, $code[$i]);
        }

        //set code to redis
        $key = $this->buildVerificationCodeKey($for);
        Model_Helper_Redis::getInstance()->set($key, strtolower($code), self::VERIFICATION_CODE_TIMEOUT);

        header("content-type: image/png");
        imagepng($img);
        imagedestroy($img);
        die;
    }

}
