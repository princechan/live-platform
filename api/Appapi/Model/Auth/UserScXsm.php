<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

class  Model_Auth_UserScXsm extends Model_Common
{
    const A6_PRODUCT_ID = 'A06';
    const A6_PRIVATE_KEY = '#@980live';

    public static function getHost()
    {
        return ENV_DEV ? 'http://www.a06new.com' : 'https://www.8869ks.com';
    }

    public function login($username, $password)
    {
        $checkUsername = $this->checkUsername($username);
        if (false === $checkUsername) {
            return false;
        }

//        if (ENV_DEV) {
//            return true;
//        }

        $url = self::getHost() . '/api/marketing/login.htm';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['login_name'] = $username;
        $data['token'] = $this->buildToken($username, $timestamp);
        $data['password'] = $password;
        $rs = Model_Helper_Curl::getInstance()->post($url, $data);
        switch ((int)$rs['status']) {
            case 100:
                $msg = 'timestamp is not null';
                break;
            case 101:
                $msg = 'token is not null';
                break;
            case 102:
                $msg = 'token is error';
                break;
            case 103:
                $msg = '用户名不能为空';
                break;
            case 104:
                $msg = '有非法字符';
                break;
            case 105:
                $msg = '用户名必须以小写字母c开头';
                break;
            case 106:
                $msg = '密码不能为空';
                break;
            case 107:
                $msg = '用户不存在';
                break;
            case 108:
                $msg = '用户名或密码错'; //第三方失败
                break;
            case 200:
                $msg = '登录成功';
                break;
            default:
                $msg = '登录失败';
                break;
        }

        if (isset($rs['status']) && $rs['status'] == 200) {
            return true;
        }

        $this->setError($msg);
        return false;
    }

    public function register($username, $password)
    {
        $checkUsername = $this->checkUsername($username);
        if (false === $checkUsername) {
            return false;
        }

        $checkPassword = $this->checkPassword($password);
        if (false === $checkPassword) {
            return false;
        }

//        if (ENV_DEV) {
//            return true;
//        }

        $url = self::getHost() . '/api/marketing/register.htm';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['login_name'] = $username;
        $data['token'] = $this->buildToken($username, $timestamp);
        $data['password'] = $password;
        $data['ip_address'] = $_SERVER['REMOTE_ADDR'] ?: '0.0.0.0';
        $data['domain_name'] = 'www.xsm7.com';
        $rs = Model_Helper_Curl::getInstance()->post($url, $data);
        switch ((int)$rs['status']) {
            case 100:
                $msg = '时间戳为空';
                break;
            case 101:
                $msg = 'token为空';
                break;
            case 102:
                $msg = 'token错误';
                break;
            case 103:
                $msg = '用户名为空';
                break;
            case 104:
                $msg = '有非法字符';
                break;
            case 105:
                $msg = '用户名必须以小写字母c开头';
                break;
            case 106:
                $msg = '密码为空';
                break;
            case 107:
                $msg = '用户名长度必须为4~11位';
                break;
            case 108:
                $msg = '密码长度必须为6~16位';
                break;
            case 109:
                $msg = 'ip_address为空';
                break;
            case 110:
                $msg = 'domain_name为空';
                break;
            case 111:
                $msg = '用户名已存在'; //login_name does not exist
                break;
            case 112:
                $msg = '第三方注册失败';
                break;
            case 200:
                $msg = '注册成功';
                break;
            default:
                $msg = '注册失败';
                break;
        }

        if (isset($rs['status']) && $rs['status'] == 200) {
            return true;
        }

        $this->setError($msg);
        return false;
    }

    private function checkPassword($password)
    {
        $num = preg_match('/^[a-zA-Z]+$/', $password);
        $word = preg_match('/^[0-9]+$/', $password);
        $check = preg_match('/^[a-zA-Z0-9]{6,16}$/', $password);
        if ($num || $word) {
            $this->setError('密码不能纯数字或纯字母');
            return false;
        } elseif (!$check) {
            $this->setError('密码由6-16位数字或字母组成');
            return false;
        }

        return true;
    }

    private function checkUsername($username)
    {
        $num = preg_match('/^[a-zA-Z]+$/', $username);
        $word = preg_match('/^[0-9]+$/', $username);
        $check = preg_match('/^[a-zA-Z0-9]{3,10}$/', $username);
        if ($num || $word) {
            $this->setError('账号不能纯数字或纯字母');
            return false;
        } elseif (!$check) {
            $this->setError('账号由3-10位数字或字母组成');
            return false;
        }

        return true;
    }

    public function checkLoginName($username)
    {
        if (ENV_DEV) {
            return true;
        }

        $url = self::getHost() . '/api/marketing/checkLoginName.htm';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['login_name'] = $username;
        $data['token'] = $this->buildToken($username, $timestamp);
        $rs = Model_Helper_Curl::getInstance()->post($url, $data);

        if (isset($rs['status']) && $rs['status'] == 200) {
            return true;
        }

        $this->setError('用户已经存在');
        return false;
    }

    private function buildToken($username, $timestamp)
    {
        return md5(self::A6_PRODUCT_ID . $username . $timestamp . self::A6_PRIVATE_KEY);
    }

}
