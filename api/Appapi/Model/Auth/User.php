<?php

class Model_Auth_User extends Model_Common
{
    const EXPIRE_TIME = 3600;
    const KEY_VISITOR_SENDER = 'vistor:senderid';

    /**
     * 游客信息
     */
    const VISITOR_ID_PREFIX = 'temp_';
    const VISITOR_COOKIE = 'ybzb_tmp_id';
    const VISITOR_NAME_PREFIX = '游客';

    /**
     * 5-麦手，6-主播
     */
    const VEST_TYPE_DJ = 5;
    const VEST_TYPE_ANCHOR = 6;

    public static $originLoginData = [
        'id' => '',
        'user_nicename' => '',
        'avatar' => '',
        'avatar_thumb' => '',
        'sex' => '',
        'signature' => '',
        'coin' => '0',
        'livecoin' => '0',
        'login_type' => '',
        'province' => '',
        'city' => '',
        'birthday' => '',
        'iswhite' => '0',
        'user_pass' => '',
        'level' => '',
        'token' => '',
    ];

    protected function getTableName($id)
    {
        return 'users';
    }

    /**
     * 修改信息统一方法，不能自己写 DI()->notorm->users->where('id=?', $uid)->update($data);
     * @param int $id
     * @param array $data
     * @return int|TRUE
     */
    public function update($id, $data)
    {
        $this->delCache(Model_Login::KEY_USER_INFO . $id);

        $rs = parent::update($id, $data);
        if (false === $rs) {
            $this->setError('失败');
            return false;
        }

        return $rs;
    }

    /**
     * 修改密码
     * @param $uid
     * @param $oldpass
     * @param $pass
     * @return int
     */
    public function updatePass($uid, $oldpass, $pass)
    {
        $userinfo = DI()->notorm->users
            ->select("user_pass")
            ->where('id=?', $uid)
            ->fetchOne();
        $oldpass = $this->setPass($oldpass);
        if ($userinfo['user_pass'] != $oldpass) {
            return 1003;
        }
        $newpass = $this->setPass($pass);

        return $this->update($uid, ['user_pass' => $newpass]);
    }

    /**
     * 用户信息
     * @param array $ids
     * @return array|bool
     */
    public function getUsersInfo(array $ids = [])
    {
        if (!$ids) {
            return false;
        }

        $in = implode(',', $ids);
        $list = DI()->notorm->users->select('*')->where("id in ({$in})")->fetchAll() ?: [];
        foreach ($list as &$li) {
            $li['avatar'] = Model_Helper_Func::getUrl($li['avatar']);
            $li['avatar_thumb'] = Model_Helper_Func::getUrl($li['avatar_thumb']);
        }

        return $list;
    }

    public function getUserInfo($uid)
    {
        if (!$uid) {
            $this->setError('缺参数');
            return false;
        }

        if ($this->isVisitor($uid)) {
            $info = $this->getVisitorInfo($uid);
        } else {
            $key = Model_Login::KEY_USER_INFO . $uid;
            $info = $this->getCache($key);
            if (!$info) {
                $info = DI()->notorm->users->where('id=?', $uid)->fetchOne();
                if ($info) {
                    $this->setCache($key, $info);
                }
            }
        }

        if (!$info) {
            return false;
        }

        if ($this->isVisitor($uid)) {
            $info['level'] = '0';
            $info['lives'] = '0';
            $info['follows'] = '0';
            $info['fans'] = '0';
        } else {
            $info['level'] = (string)$this->getLevel($info['consumption']);
            $info['lives'] = (string)Model_Live::getInstance()->getLives($uid);
            $info['follows'] = (string)Model_Auth_User::getInstance()->getFollowsNum($uid);
            $info['fans'] = (string)Model_Auth_User::getInstance()->getFansNum($uid);
        }

        $info['avatar'] = Model_Helper_Func::getUrl($info['avatar']);
        $info['avatar_thumb'] = Model_Helper_Func::getUrl($info['avatar_thumb']);
        unset($info['user_login'], $info['user_pass']);

        return $info;
    }

    public function checkName($uid, $name)
    {
        return DI()->notorm->users->select('id')->where('id!=? and user_nicename=?', $uid, $name)->fetchOne();
    }

    /**
     * 我的钻石
     * @param $uid
     * @return array
     */
    public function getBalance($uid)
    {
        $info = DI()->notorm->users
            ->select("coin")
            ->where('id=?  and user_type="2"', $uid)
            ->fetchOne();
        $coin = $info ? $info['coin'] : 0;

        return ['coin' => $coin];
    }

    public function getLivecoin($uid)
    {
        return DI()->notorm->users
            ->select('livecoin')
            ->where('id=?', $uid)
            ->fetchOne();
    }

    public function getChargeRules()
    {

        $rules = DI()->notorm->charge_rules
            ->select('id,coin,money,money_ios,product_id,give')
            ->order('orderno asc')
            ->fetchAll();

        return $rules;
    }

    /**
     * 我的收益
     * @param $uid
     * @return array
     */
    public function getProfit($uid)
    {
        $info = DI()->notorm->users
            ->select("votes,consumption")
            ->where('id=?', $uid)
            ->fetchOne();
        $level = $this->getLevel($info['consumption']);
        //等级限制金额
        $limitcash = $this->getLevelSection($level);

        $config = $this->getConfigPri();

        //提现比例
        $cash_rate = $config['cash_rate'];
        //剩余票数
        $votes = $info['votes'];
        //总可提现数
        $total = floor($votes / $cash_rate);

        $nowtime = time();
        //当天0点
        $today = date("Ymd", $nowtime);
        $today_start = strtotime($today) - 1;
        //当天 23:59:59
        $today_end = strtotime("{$today} + 1 day");

        //已提现
        $hascash = DI()->notorm->users_cashrecord
            ->where('uid=? and addtime>? and addtime<? and status!=2', $uid, $today_start, $today_end)
            ->sum("money");
        if (!$hascash) {
            $hascash = 0;
        }
        //今天可体现
        $todaycancash = $limitcash - $hascash;

        //今天能提
        if ($todaycancash < $total) {
            $todaycash = $todaycancash;
        } else {
            $todaycash = $total;
        }

        return [
            "votes" => $votes,
            "todaycash" => $todaycash,
            "total" => $total,
        ];
    }

    /**
     * 提现
     * @param $uid
     * @return bool|int
     */
    public function setCash($uid)
    {
        $isrz = DI()->notorm->users_auth->select("status")->where('uid=?', $uid)->fetchOne();
        if (!$isrz || $isrz['status'] != 1) {
            $this->setError('用户未认证');
            return false;
        }

        $info = DI()->notorm->users
            ->select("votes,consumption")
            ->where('id=?', $uid)
            ->fetchOne();
        $level = $this->getLevel($info['consumption']);
        //等级限制金额
        $limitcash = $this->getLevelSection($level);

        $config = $this->getConfigPri();

        //提现比例
        $cash_rate = $config['cash_rate'];
        /* 最低额度 */
        $cash_min = $config['cash_min'];
        //剩余票数
        $votes = $info['votes'];
        //总可提现数
        $total = floor($votes / $cash_rate);

        //已提现
        $nowtime = time();
        //当天0点
        $today = date("Ymd", $nowtime);
        $today_start = strtotime($today) - 1;
        //当天 23:59:59
        $today_end = strtotime("{$today} + 1 day");
        $hascash = DI()->notorm->users_cashrecord
            ->where('uid=? and addtime>? and addtime<? and status!=2', $uid, $today_start, $today_end)
            ->sum("money");
        if (!$hascash) {
            $hascash = 0;
        }

        //今天可体现
        $todaycancash = $limitcash - $hascash;
        //今天能提
        if ($todaycancash < $total) {
            $todaycash = $todaycancash;
        } else {
            $todaycash = $total;
        }

        if ($todaycash == 0) {
            $this->setError('今日提现已达上限');
            return false;
        }

        if ($todaycash < $cash_min) {
            $this->setError("提现最低额度为{$cash_min}元");
            return false;
        }

        $cashvotes = $todaycash * $cash_rate;
        $nowtime = time();

        $data = [
            "uid" => $uid,
            "money" => $todaycash,
            "votes" => $cashvotes,
            "orderno" => $uid . '_' . $nowtime . rand(100, 999),
            "status" => 0,
            "addtime" => $nowtime,
            "uptime" => $nowtime,
        ];

        $rs = DI()->notorm->users_cashrecord->insert($data);
        if ($rs) {
            (new Model_Auth_User)->update($uid, ['votes' => new NotORM_Literal("votes - {$cashvotes}")]);
        } else {
            $this->setError('提现失败，请重试');
            return false;
        }

        return $rs;
    }

    /**
     * 关注
     */
    public function setAttent($uid, $liveType, $touid)
    {
        if ($this->isVisitor($uid)) {
            $this->setError('游客不能关注');
            return false;
        }
        if ($this->isVisitor($touid)) {
            $this->setError('不能关注游客');
            return false;
        }
        if ($uid == $touid) {
            $this->setError('不能关注自己');
            return false;
        }

        $is = Model_Auth_User::getInstance()->getAttention($uid, $liveType, $touid);
        if ($is) {
            DI()->notorm->users_attention->where('id=?', $is['id'])->delete();
            return 0;
        } else {
            DI()->notorm->users_black->where('uid=? and touid=?', $uid, $touid)->delete();
            DI()->notorm->users_attention->insert([
                'uid' => $uid,
                'live_type' => $liveType,
                'touid' => $touid,
            ]);
            return 1;
        }
    }

    /**
     * 拉黑
     * @param $uid
     * @param $touid
     * @return int
     */
    public function setBlack($uid, $touid)
    {
        if ($this->isVisitor($uid)) {
            $this->setError('游客不能拉黑');
            return false;
        }
        if ($this->isVisitor($touid)) {
            $this->setError('不能拉黑游客');
            return false;
        }
        if ($uid == $touid) {
            $this->setError('不能拉黑自己');
            return false;
        }

        $isexist = DI()->notorm->users_black
            ->select("*")
            ->where('uid=? and touid=?', $uid, $touid)
            ->fetchOne();
        if ($isexist) {
            DI()->notorm->users_black
                ->where('uid=? and touid=?', $uid, $touid)
                ->delete();
            return 0;
        } else {
            DI()->notorm->users_attention
                ->where('uid=? and live_type=? and touid=?', $uid, 0, $touid)
                ->delete();
            DI()->notorm->users_black
                ->insert(["uid" => $uid, "touid" => $touid]);
            return 1;
        }
    }

    //关注列表，人 live_type=0
    public function getFollowsList($uid, $touid, $p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;
        $touids = DI()->notorm->users_attention
            ->select("touid")
            ->where('uid=? and live_type=?', $touid, 0)
            ->limit($start, $pnum)
            ->fetchAll();
        foreach ($touids as $k => $v) {
            $touids[$k] = $this->getUserInfo($v['touid']);
            if ($uid == $touid) {
                $isattent = 1;
            } else {
                $isattent = $this->isAttention($uid, $v['touid'], 0);
            }
            $touids[$k]['isattention'] = $isattent;
        }

        return $touids;
    }

    //粉丝列表，人 live_type=0
    public function getFansList($uid, $touid, $p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;
        $touids = DI()->notorm->users_attention
            ->select('uid')
            ->where('touid=? and live_type=?', $touid, 0)
            ->limit($start, $pnum)
            ->fetchAll();
        foreach ($touids as $k => $v) {
            $touids[$k] = $this->getUserInfo($v['uid']);
            $touids[$k]['isattention'] = $this->isAttention($uid, $v['uid'], 0); //0-未关注，1-已关注
        }

        return $touids;
    }

    //黑名单列表
    public function getBlackList($uid, $touid, $p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;
        $touids = DI()->notorm->users_black
            ->select("touid")
            ->where('uid=?', $touid)
            ->limit($start, $pnum)
            ->fetchAll();
        foreach ($touids as $k => $v) {
            $touids[$k] = $this->getUserInfo($v['touid']);
        }
        return $touids;
    }

    /**
     * 直播记录
     * @param $touid
     * @param $p
     * @return mixed
     */
    public function getLiverecord($touid, $p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;
        $record = DI()->notorm->users_liverecord
            ->select("id,uid,live_type,liveid,nums,starttime,endtime,title,city")
            ->where('uid=?', $touid)
            ->order("id desc")
            ->limit($start, $pnum)
            ->fetchAll();
        foreach ($record as $k => $v) {
            $record[$k]['datestarttime'] = date("Y年m月d日 H:i", $v['starttime']);
            $record[$k]['dateendtime'] = date("Y年m月d日 H:i", $v['endtime']);
        }

        return $record;
    }

    /* 个人主页 */
    public function getUserHome($uid, $touid)
    {
        $info = $this->getUserInfo($touid);

        $info['follows'] = Model_Helper_Func::numberFormat(Model_Auth_User::getInstance()->getFollowsNum($touid));
        $info['fans'] = Model_Helper_Func::numberFormat(Model_Auth_User::getInstance()->getFansNum($touid));
        $info['isattention'] = (string)$this->isAttention($uid, $touid, 0);
        $info['isblack'] = (string)$this->isBlack($uid, $touid);
        $info['isblack2'] = (string)$this->isBlack($touid, $uid);

        /* 贡献榜前三 */
        $rs = DI()->notorm->users_coinrecord
            ->select("uid,sum(totalcoin) as total")
            ->where('touid=?', $touid)
            ->group("uid")
            ->order("total desc")
            ->limit(0, 3)
            ->fetchAll();
        foreach ($rs as $k => $v) {
            $userinfo = $this->getUserInfo($v['uid']);
            $rs[$k]['avatar'] = $userinfo['avatar'];
        }
        $info['contribute'] = $rs;

        /* 是否直播 */
        $info['islive'] = '1';
        if ($uid == $touid) {
            $live['uid'] = '';
            $live['avatar'] = '';
            $live['avatar_thumb'] = '';
            $live['user_nicename'] = '';
            $live['title'] = '';
            $live['city'] = '';
            $live['stream'] = '';
            $live['pull'] = '';
            $info['islive'] = '0';
        } else {
            $live = DI()->notorm->users_live
                ->select("uid,avatar,avatar_thumb,user_nicename,title,city,stream,pull")
                ->where('uid=? and islive="1"', $touid)
                ->fetchOne();
            if (!$live) {
                $live['uid'] = '';
                $live['avatar'] = '';
                $live['avatar_thumb'] = '';
                $live['user_nicename'] = '';
                $live['title'] = '';
                $live['city'] = '';
                $live['stream'] = '';
                $live['pull'] = '';
                $info['islive'] = '0';
            } else {
                $live['pull'] = $this->PrivateKeyA('rtmp', $live['stream'], 0);
            }
        }

        //只能给单直播
        $live['live_type'] = 0;

        $info['liveinfo'] = $live;

        //直播记录
        $record = DI()->notorm->users_liverecord
            ->select("id,uid,live_type,liveid,nums,starttime,endtime,title,city")
            ->where('uid=?', $touid)
            ->order("id desc")
            ->limit(0, 20)
            ->fetchAll();
        foreach ($record as $k => $v) {
            $record[$k]['datestarttime'] = date("Y年m月d日 H:i", $v['starttime']);
            $record[$k]['dateendtime'] = date("Y年m月d日 H:i", $v['endtime']);
        }
        $info['liverecord'] = $record;

        $res = Model_Live::getInstance()->getLiveChatMsg($touid);
        $info['chat_num'] = $res['chat_num'];
        $info['chat_frequency'] = $res['chat_frequency'];

        return $info;
    }

    /* 贡献榜 */
    public function getContributeList($touid, $p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;

        $rs = DI()->notorm->users_coinrecord
            ->select("uid,sum(totalcoin) as total")
            ->where('touid=?', $touid)
            ->group("uid")
            ->order("total desc")
            ->limit($start, $pnum)
            ->fetchAll();

        foreach ($rs as $k => $v) {
            $rs[$k]['userinfo'] = $this->getUserInfo($v['uid']);
        }

        return $rs;
    }

    //阿里的配置信息
    ###############################################################
    const access_key_id = 'cKLrP3epGTcbxz07';
    const access_key_secret = 'D80MB2pRyr09rAY6QE0naLokMMqJ73';
    //录播的流地址和应用名配置信息
    const DomainName = 'testlive.anbig.com';
    const AppName = '5showcam';

    public function getAliCdnRecord($id, $access_key_id = self::access_key_id, $access_key_secret = self::access_key_secret, $DomainName = self::DomainName, $AppName = self::AppName)
    {
        date_default_timezone_set("UTC");
        if (empty($access_key_id) || empty($access_key_secret)) {
            $this->setError('Access key Id or access key secret is invalid');
            return false;
        }

        $liverecord = DI()->notorm->users_liverecord
            ->select('*')
            ->where('id=?', $id)
            ->fetchOne();

        if (!$liverecord) {
            $this->setError('直播回放不存在');
            return false;
        }

        $live_starttime = $liverecord['starttime'] - 100;
        $live_endtime = $liverecord['endtime'] + 100;
        $StartTime = gmdate("Y-m-d\TH:i:s\Z", $live_starttime);
        $EndTime = gmdate("Y-m-d\TH:i:s\Z", $live_endtime);

        $StreamName = $liverecord['stream'];
        $action = 'DescribeLiveStreamRecordIndexFiles';

        $specialParameter = [
            'AccessKeyId' => $access_key_id,
            'Action' => $action,
            'DomainName' => $DomainName,
            'AppName' => $AppName,
            'StreamName' => $StreamName,
            'StartTime' => $StartTime,
            'EndTime' => $EndTime,
        ];

        $parameter = $this->setParameter($specialParameter);
        $url = $this->getStringToSign($parameter, $access_key_secret);
        $ret = $this->curl_get($url);

        $res_arr = json_decode($ret, true);
        if (!$res_arr['RecordIndexInfoList']['RecordIndexInfo']) {
            $this->setError('直播回放不存在');
            return false;
        }

        return $res_arr['RecordIndexInfoList']['RecordIndexInfo'][0]['RecordUrl'];
    }

    private function setParameter($specialParameter)
    {
        $time = date('Y-m-d H:i:s', time());
        $var = strtr($time, ' ', 'T');
        $Timestamp = $var . 'Z';
        $signature_nonce = '';
        for ($i = 0; $i < 14; $i++) {
            $signature_nonce .= mt_rand(0, 9);
        }
        $publicParameter = [
            'Format' => 'JSON',
            'Version' => '2014-11-11',
            'SignatureMethod' => 'HMAC-SHA1',
            'TimeStamp' => $Timestamp,
            'SignatureVersion' => '1.0',
            'SignatureNonce' => $signature_nonce,
        ];

        $parameter = array_merge($publicParameter, $specialParameter);
        return $parameter;
    }

    private function curl_get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    // https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    // 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_HEADER, 0); // 不要http header 加快效率
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function getStringToSign($parameter, $access_key_secret)
    {
        ksort($parameter);
        foreach ($parameter as $key => $value) {
            $str[] = rawurlencode($key) . "=" . rawurlencode($value);
        }
        $ss = "";
        if (!empty($str)) {
            for ($i = 0; $i < count($str); $i++) {
                if (!isset($str[$i + 1])) {
                    $ss .= $str[$i];
                } else
                    $ss .= $str[$i] . "&";
            }
        }

        $StringToSign = "GET" . "&" . rawurlencode("/") . "&" . rawurlencode($ss);
        $signature = base64_encode(hash_hmac("sha1", $StringToSign, $access_key_secret . "&", true));
        $url = "https://cdn.aliyuncs.com/?" . $ss . "&Signature=" . rawurlencode($signature);
        return $url;
    }

    /**
     * 设备标识符
     * @return string
     */
    public static function getDeviceNum()
    {
        return isset($_REQUEST['device_num']) ? $_REQUEST['device_num'] : '';
    }

    /**
     * 用户token
     * @return string
     */
    public static function getToken()
    {
        return isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
    }

    /**
     * 判断账号是否超管
     * @param $uid
     * @return int
     */
    public function isSuper($uid)
    {
        $rs = DI()->notorm->users_super
            ->select("*")
            ->where('uid=?', $uid)
            ->fetchOne();

        return $rs ? 1 : 0;
    }

    /**
     * @deprecated
     * 11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19 超管
     * @param $uid
     * @param $liveType
     * @param $liveId
     * @return string
     */
    public function oldUserIdentity($uid, $liveType, $liveId)
    {
        $userIdentity = $this->getUserIdentity($uid, $liveId, $liveType);
        if ($userIdentity == 60) {
            return '19';//超管
        } elseif ($userIdentity == 50) {
            return '18';//主播
        }

        $identity = '11';
        $info = DI()->notorm->users->select('issuper,vest_id')->where('id=?', $uid)->fetchOne();
        if ($info['vest_id'] > 1) {
            if ($info['vest_id'] == 7) {//黑马
                $identity = '17';
            }
            if ($info['vest_id'] == 6) {//紫马
                $identity = '16';
            }
        } else {
            $vestInfo = $this->getUserVest($uid, $liveType, $liveId);
            if ($vestInfo) {
                switch ($vestInfo['vestid']) {
                    case 2:
                        $identity = '12';//绿马
                        break;
                    case 3:
                        $identity = '13';//蓝马
                        break;
                    case 4:
                        $identity = '14';//黄马
                        break;
                    case 5:
                        $identity = '15';//橙马
                        break;
                    default:
                        $identity = '11';//白马
                }
            }
        }

        return $identity;
    }

    public function getUserVest($uid, $liveType, $liveId)
    {
        return DI()->notorm->users_vest_lists
            ->where('uid=? and live_type=? and liveid=?', $uid, $liveType, $liveId)->fetchOne();
    }

    /**
     * 用户权限
     * @param $uid
     * @param $liveId
     * @param int $liveType 0-单 1-多
     * @return int 10-游客 30-观众 40-管理员 50-房间主播 60-超管 70-麦手
     */
    public function getUserIdentity($uid, $liveId, $liveType = 0)
    {
        if ($this->isVisitor($uid)) {
            return 10;
        }
        if ($this->isSuper($uid)) {
            return 60;
        }

        if ($liveType == 0) {
            if ($uid == $liveId) {
                return 50;
            }

            $manager = DI()->notorm->users_livemanager
                ->select("*")
                ->where('uid=? and liveuid=?', $uid, $liveId)
                ->fetchOne();
            if ($manager) {
                return 40;
            }
        } else {
            $list = DI()->notorm->muti_showmanager
                ->select('*')
                ->where('muti_id=?', $liveId)
                ->fetchAll() ?: [];
            foreach ($list as $value) {
                if ($value['uid'] == $uid) {
                    if ($value['vest_type'] == self::VEST_TYPE_DJ) {
                        return 70;
                    }
                    if ($value['vest_type'] == self::VEST_TYPE_ANCHOR) {
                        return 50;
                    }
                    break;
                }
            }
        }

        return 30;
    }

    /**
     * 游客信息
     * @param $id
     * @return mixed
     */
    public function getVisitorInfo($id)
    {
        $id = $this->genVisitorId($id);
        $user = json_decode(Model_Helper_Redis::getInstance()->get($id), true);
        if (!$user) {
            return [];
        }

        $emptyUser = $this->getUserEmptyInfo();
        if ($emptyUser) {
            $user = array_merge($emptyUser, $user);
        }

        return $user;
    }

    private function setVisitorInfoById($id, $user)
    {
        return Model_Helper_Redis::getInstance()->set($id, json_encode($user), self::EXPIRE_TIME);
    }

    private function getVisitorInfoByToken($token)
    {
        return json_decode(Model_Helper_Redis::getInstance()->get($token), true);
    }

    private function setVisitorInfoByToken($token, $user)
    {
        return Model_Helper_Redis::getInstance()->set($token, json_encode($user), self::EXPIRE_TIME);
    }

    /**
     * 是否是游客，true-是，false-否
     * @param $uid
     * @return bool
     */
    public function isVisitor($uid)
    {
        return 0 === stripos($uid, self::VISITOR_ID_PREFIX);
    }

    private function genVisitorId($hash)
    {
        $hash = str_replace([self::VISITOR_NAME_PREFIX, self::VISITOR_ID_PREFIX], '', $hash);
        return self::VISITOR_ID_PREFIX . $hash;
    }

    public function genVisitorInfo()
    {
        $hash = $this->genVisitorSenderId();
        $id = self::VISITOR_ID_PREFIX . $hash;
        $hashName = self::VISITOR_NAME_PREFIX . $hash;
        $token = md5($id . Model_Login::A6_PRIVATE_KEY);

        $user = [
            'id' => $id,
            'avatar' => Model_Helper_Func::getUrl('', 3),
            'avatar_thumb' => Model_Helper_Func::getUrl('', 3),
            'user_nicename' => $hashName,
            'token' => $token,
            'logged' => 0, //是否是登陆用户
            'issuper' => 0,
            'iswhite' => 0,
            'sex' => 0,
            'signature' => '',
            'consumption' => 0,
            'votestotal' => 0,
            'province' => '',
            'city' => '',
            'level' => 0,
        ];

        $this->setVisitorInfoByToken($token, $user);
        $this->setVisitorInfoById($id, $user);

        return $user;
    }

    private function genVisitorSenderId()
    {
        return Model_Helper_Redis::getInstance()->incr(self::KEY_VISITOR_SENDER);
    }

    /**
     * 存储7天用户空数据
     * @return mixed
     */
    private function getUserEmptyInfo()
    {
        $key = 'user-empty-info';
        $redis = Model_Helper_Redis::getInstance();
        $user = json_decode($redis->get($key), true);
        if (!$user) {
            $user = DI()->notorm->users->fetchOne();
            if ($user) {
                $redis->set($key, json_encode($user), 7 * 24 * 3600);
            }
        }

        if ($user && is_array($user)) {
            Model_Helper_Func::emptyArrayValue($user);
        }

        $user['sex'] = '0';
        $user['consumption'] = '0';
        $user['votestotal'] = '0';
        $user['issuper'] = '0';
        $user['isauth'] = '0';
        $user['iszombie'] = '0';
        $user['livecoin'] = '0';
        $user['iswhite'] = '0';
        $user['chat_num'] = '0';
        $user['chat_frequency'] = '0';
        $user['level'] = '0';

        return $user;
    }

    /**
     * 启动程序
     * @param string $token
     * @return array|mixed
     */
    public function bootstrap($token = '')
    {
        if ($token) {
            $user = $this->getUserInfoByToken($token);
        } else {
            $user = $this->genVisitorInfo();
        }

        if (!$user) {
            $user = $this->getVisitorInfoByToken($token);
        }

        if (!$user) {
            $user = $this->genVisitorInfo();
        }

        return $this->unifiedLoginData($user ?: []);
    }

    public function getUserInfoByToken($token)
    {
        return json_decode(Model_Helper_Redis::getInstance()->get($token), true);
    }

    public function setUserInfoByToken($token, $user)
    {
        if (!$token || !$user) {
            return false;
        }

        return json_decode(Model_Helper_Redis::getInstance()->set($token, json_encode($user)));
    }

    /**
     * 统一登陆数据
     * @param array $data
     * @return array
     */
    public function unifiedLoginData(array $data = [])
    {
        return array_merge(self::$originLoginData, $data);
    }

    public function updateAvatar($uid, $file)
    {
        if (!isset($file) || !is_array($file)) {
            $this->setError('file不存在');
            return false;
        }
        if ($file["error"] > 0) {
            $this->setError(T('failed to upload file with error: {error}', ['error' => $_FILES['file']['error']]));
            return false;
        }
        if (!$this->checkExt($file['name'])) {
            $this->setError('图片仅能上传 jpg,png,jpeg');
            return false;
        }
        $upload = Model_Helper_UploadFile::getInstance()->uploadOne($file);
        if (false === $upload) {
            return false;
        }

        $uploadFileInfo = Model_Helper_UploadFile::getInStance()->getUploadFileInfo();
        if (isset($uploadFileInfo[0]) && is_array($uploadFileInfo[0])) {
            $avatar = $uploadFileInfo[0]['dbsavepath'];
        } else {
            $this->setError('file上传失败');
            return false;
        }
        @unlink($_FILES['file']['tmp_name']);

        $data = [
            "avatar" => $avatar,
            "avatar_thumb" => $avatar,
        ];

        $info = $this->update($uid, $data);
        if (false === $info) {
            $this->setError('修改失败');
            return false;
        }

        return true;
    }

    /**
     * 判断用户是否被封禁IP
     * @param $uid
     * @return int
     */
    public function isLimitIP($uid)
    {
        $count = DI()->notorm->users_disable_ips
            ->select("id")
            ->where('uid=?', $uid)
            ->count('id');

        if ($count <= 0) {
            return 0;//未被限制IP
        }

        //有可能当前IP被封禁
        //获取当前的IP
        $currentIP = $_SERVER['REMOTE_ADDR'];
        $lists = DI()->notorm->users_disable_ips
            ->where("uid={$uid}")
            ->fetchAll();

        $isLimitIP = 0;
        foreach ($lists as $k => $v) {
            if ($currentIP == $v['begin_ip']) {//符合单独IP被限制
                $isLimitIP = 1;
                break;
            }

            $info = DI()->notorm->users_disable_ips
                ->where("uid={$uid} and inet_aton('{$currentIP}') between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")
                ->count();

            $count1 = intval($info);
            if ($count1 > 0) {
                $isLimitIP = 1;
                break;
            }
        }

        if ($isLimitIP == 1) {
            return 1;//被限制IP
        }

        return 0;
    }

    /**
     * 是否认证
     * @param $uid
     * @return int
     */
    public function isAuth($uid)
    {
        $status = DI()->notorm->users_auth->select("status")->where('uid=?', $uid)->fetchOne();
        if (!$status || $status['status'] != 1) {
            return 0;
        }
        return 1;
    }

    /**
     * 是否禁用
     * @param $uid
     * @return int 0-禁用，1-未禁用
     */
    public function isBan($uid)
    {
        if ($this->isVisitor($uid)) {
            return 1;
        }

        $user = DI()->notorm->users->select("user_status")->where('id=?', $uid)->fetchOne();
        if (!$user || $user['user_status'] == 0) {
            return 0;
        }

        return 1;
    }

    /**
     * 统计粉丝
     */
    public function getFansNum($uid, $liveType = 0)
    {
        return DI()->notorm->users_attention->where('touid=? and live_type=?', $uid, $liveType)->count();
    }

    /**
     * 统计关注
     */
    public function getFollowsNum($uid, $liveType = 0)
    {
        return DI()->notorm->users_attention->where('uid=? and live_type=?', $uid, $liveType)->count();
    }

    public function getAttention($uid, $liveType, $touid)
    {
        return DI()->notorm->users_attention->where('uid=? and live_type=? and touid=?', $uid, $liveType, $touid)->fetchOne();
    }

    /**
     * 判断是否关注
     */
    public function isAttention($uid, $touid, $liveType = 0)
    {
        return (int)$this->getAttention($uid, $liveType, $touid);
    }

    public function isBlack($uid, $touid)
    {
        if ($uid <= 0 || $touid <= 0) {
            return 0;
        }

        return (int)DI()->notorm->users_black->where('uid=? and touid=?', $uid, $touid)->fetchOne();
    }

}
