<?php
/**
 * Model.Common需要废弃，相应的全局方法放到这里
 * User: kobi.h
 */

class Model_Helper_Func
{
    /**
     * 去除emoji表情
     * @param $str
     * @return null|string|string[]
     */
    public static function filterEmoJi($str)
    {
        return preg_replace_callback('/./u', function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        }, $str);
    }

    /**
     * 返回带协议的域名
     */
    public static function getHost()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = self::isSsl() ? 'https://' : 'http://';
        return $protocol . $host;
    }

    /**
     * 判断是否SSL协议
     * @return boolean
     */
    public static function isSsl()
    {
        if (isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))) {
            return true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }
        return false;
    }

    /**
     * 转化数据库保存的文件路径，为可以访问的url
     * @param $file
     * @param int $defaultType
     * @return string
     */
    public static function getUrl($file = '', $defaultType = 1)
    {
        if (!$file) {
            switch ($defaultType) {
                case 2:
                    $file = self::getSingleVideoDefaultImage();
                    break;
                case 3:
                    $file = self::getYouKeVestIcon();
                    break;
                case 1:
                default:
                    $file = self::getUserDefaultImage();
            }
            return $file;
        }

        if (strpos($file, 'http') === 0) {
            return $file;
        } else if (strpos($file, '/') === 0) {
            return self::getHost() . $file;
        } else {
            return $file;
        }
    }

    private static function getUserDefaultImage()
    {
        return self::getUrl('/default.jpg');
    }

    private static function getSingleVideoDefaultImage()
    {
        return self::getUrl('/public/images/home/video-default-image.jpg');
    }

    private static function getYouKeVestIcon()
    {
        return self::getUrl('/public/images/user/vest-icon-youke.png');
    }

    /**
     * index data
     * @param array $array
     * @param string $key
     * @param string $sort
     * @return array
     */
    public static function index(array $array, $key = 'id', $sort = null)
    {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$key]) && $v) {
                $result[$v[$key]] = $v;
            }
        }
        $array = null;
        switch ($sort) {
            case 'desc':
                krsort($result);
                break;
            case 'asc':
                ksort($result);
                break;
            default:
        }

        return $result;
    }

    public static function emptyArrayValue(array &$array)
    {
        foreach ($array as & $v) {
            $v = '';
        }
    }

    /**
     * 应用标识(AppKey)
     * @return string
     */
    public static function getAppKey()
    {
        return APP_KEY;
    }

    /**
     * 数字格式化
     * @param $num
     * @return string
     */
    public static function numberFormat($num)
    {
        if ($num < 10000) {

        } else if ($num < 1000000) {
            $num = round($num / 10000, 2) . '万';
        } else if ($num < 100000000) {
            $num = round($num / 10000, 1) . '万';
        } else if ($num < 10000000000) {
            $num = round($num / 100000000, 2) . '亿';
        } else {
            $num = round($num / 100000000, 1) . '亿';
        }
        return $num;
    }

}
