<?php
/**
 * User: kobi.h
 */

class Model_Helper_Curl extends Model_Helper_Service
{
    public function get($url, array $get = [])
    {
        $url = $url . (strpos($url, '?') === false ? '?' : '') . http_build_query($get);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    // 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_HEADER, 0); // 不要http header 加快效率
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $res = curl_exec($ch);
        curl_close($ch);

        return $this->jsonDecode($res);
    }

    public function post($url, array $data = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($curl);
        curl_close($curl);

        return $this->jsonDecode($res);
    }

    private function jsonDecode($data)
    {
        return json_decode($data, true) ? json_decode($data, true) : $data;
    }

    /**
     * 资源是否存在
     * @param $path
     * @return bool
     */
    public function resourcesExist($path)
    {
        if (!$path) {
            return false;
        }

        $headers = get_headers($path);
        $statusCode = substr($headers[0], 9, 3);
        if (in_array($statusCode, [200, 304])) {
            return true;
        } else {
            return false;
        }
    }

}
