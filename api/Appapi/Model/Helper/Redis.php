<?php
/**
 * User: kobi.h
 */

class Model_Helper_Redis extends Model_Helper_Service
{
    const NODE_DEFAULT = 1;

    /**
     * @var \Redis
     */
    private $redis;

    private static $node;

    /**
     * @param int $node
     * @return \Redis|static
     */
    public static function getInstance($node = self::NODE_DEFAULT)
    {
        return parent::getInstance(self::$node = $node);
    }

    /**
     * Redis constructor.
     * @param int $node
     * @throws Exception
     */
    protected function __construct($node = self::NODE_DEFAULT)
    {
        if ($node == self::NODE_DEFAULT) {
            $host = DI()->config->get('app.REDIS_HOST');
            $port = DI()->config->get('app.REDIS_PORT');
            $password = DI()->config->get('app.REDIS_AUTH');
        } else {
            throw new Exception('node error');
        }

        $this->redis = new \Redis();
        $this->redis->pconnect($host, $port);
        $this->redis->auth($password);
        return $this->redis;
    }

    /**
     * @param $function
     * @param $parameters
     * @return mixed
     */
    public function __call($function, $parameters)
    {
        return call_user_func_array([$this->redis, $function], $parameters);
    }

    public function __destruct()
    {
        $this->redis->close();
    }

}
