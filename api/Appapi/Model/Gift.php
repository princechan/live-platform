<?php

class Model_Gift extends Model_Common
{
    /**
     * 获取礼物信息
     */
    public function getGiftInfo($giftid)
    {
        $info = DI()->notorm->gift->where("id=?", $giftid)->fetchOne();
        if (!$info) {
            return [];
        }

        $info['gifticon'] = Model_Helper_Func::getUrl($info['gifticon']);
        return $info;
    }

}
