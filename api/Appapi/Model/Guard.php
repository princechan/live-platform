<?php

class Model_Guard extends Model_Common
{
    /**
     * 判断是否是守护
     * @param $uid
     * @param $liveuid
     * @return array
     */
    public function checkIsGuard($uid, $liveuid)
    {
        $uid = (int)$uid;
        $liveuid = (int)$liveuid;

        $now = time();
        $rs = [
            'isGuard' => '0',
            'guard_level' => '1',
        ];

        $guardinfo = DI()->notorm->users_guard_lists
            ->select("*")
            ->where("uid=? and liveuid=? and effectivetime >?", $uid, $liveuid, $now)
            ->fetchOne();
        if ($guardinfo) {
            $rs['isGuard'] = '1';
            if ($guardinfo['level_endtime'] < $now) {
                $cha = ceil(($now - $guardinfo['level_endtime']) / 30);
                $guard_level = $guardinfo['guard_level'] + $cha;
                $level_endtime = $guardinfo['level_endtime'] + 60 * 60 * 24 * 30 * $cha;
                DI()->notorm->users_guard_lists->where("uid=? and liveuid=? ", $uid, $liveuid)
                    ->update(['guard_level' => $guard_level, 'level_endtime' => $level_endtime]);
                $rs['guard_level'] = (string)$guard_level;
            } else {
                $rs['guard_level'] = (string)$guardinfo['guard_level'];
            }
        }

        return $rs;
    }

}
