<?php

class Model_Game extends Model_Common
{
    public function record($liveuid, $stream, $token, $action, $time, $result)
    {
        $userinfo = DI()->notorm->users
            ->select("token,expiretime")
            ->where('id=?', $liveuid)
            ->fetchOne();
        if ($userinfo['token'] != $token || $userinfo['expiretime'] < time()) {
            return 700;
        }
        $game = DI()->notorm->game
            ->select("*")
            ->where('stream=? and state=0', $stream)
            ->fetchOne();
        if ($game) {
            return 1000;
        }
        $rs = DI()->notorm->game
            ->insert(["liveuid" => $liveuid, "stream" => $stream, 'action' => $action, 'state' => '0', 'result' => $result, "starttime" => $time]);
        if (!$rs) {
            return 1001;
        }
        return $rs;
    }

    public function endGame($liveuid, $gameid, $type)
    {
        $game = DI()->notorm->game
            ->select("*")
            ->where('id=? and state=0', $gameid)
            ->fetchOne();
        if (!$game) {
            return 1000;
        }
        $coin = "coin_" . $game['result'];

        $sql = "select uid,sum(" . $coin . ") as gamecoin from cmf_users_gamerecord where gameid=:gameid and status='0' group by uid";
        $params = [':gameid' => $gameid];
        $total = DI()->notorm->user->queryAll($sql, $params);
        if ($type == 2 || $type == 3) {
            foreach ($total as $k => $v) {
                (new Model_Auth_User)->update($v['uid'], ['coin' => new NotORM_Literal("coin + {$v['gamecoin']}")]);
            }
        } else {
            foreach ($total as $k => $v) {
                $gamecoin = $v['gamecoin'] * 2;
                (new Model_Auth_User)->update($v['uid'], ['coin' => new NotORM_Literal("coin + {$gamecoin}")]);
            }
        }

        DI()->notorm->game
            ->where('id = ? and liveuid =?', $gameid, $liveuid)
            ->update(['state' => $type, 'endtime' => time()]);

        DI()->notorm->users_gamerecord
            ->where('gameid=?', $gameid)
            ->update(['status' => '1']);

        $rs['stream'] = $game['stream'];
        $rs['starttime'] = $game['starttime'];
        return $rs;
    }

    public function gameBet($uid, $gameid, $coin, $action, $grade)
    {
        $userinfo = DI()->notorm->users
            ->select('coin')
            ->where('id = ?', $uid)
            ->fetchOne();
        if ($userinfo['coin'] < $coin) {
            return 1000;
        }
        $game = DI()->notorm->game
            ->select("*")
            ->where('id=?', $gameid)
            ->fetchOne();
        if (!$game || $game['state'] != "0") {
            return 1001;
        }
        $gamerecord = DI()->notorm->users_gamerecord
            ->select('id')
            ->where('action = ? and uid=? and gameid=? and liveuid=?', $action, $uid, $gameid, $game['liveuid'])
            ->fetchOne();

        $field = 'coin_' . $grade;

        if ($gamerecord) {
            $users_game = DI()->notorm->users_gamerecord
                ->where('id = ? ', $gamerecord['id'])
                ->update([$field => new NotORM_Literal("{$field} + {$coin}")]);
        } else {
            $users_game = DI()->notorm->users_gamerecord
                ->insert(["action" => $action, "uid" => $uid, 'gameid' => $gameid, 'liveuid' => $game['liveuid'], $field => $coin, "addtime" => time()]);
        }
        if (!$users_game) {
            return 1002;
        }

        (new Model_Auth_User)->update($uid, [
            'coin' => new NotORM_Literal("coin - {$coin}"),
            'consumption' => new NotORM_Literal("consumption + {$coin}"),
        ]);

        $info = DI()->notorm->users
            ->select('coin')
            ->where('id = ?', $uid)
            ->fetchOne();
        $rs['coin'] = $info['coin'];
        $rs['gametime'] = $game['starttime'];
        $rs['stream'] = $game['stream'];
        return $rs;
    }

    public function checkGame($liveuid, $stream)
    {
        $rs = ["brand" => [], "time" => "0", "id" => "0", "action" => "0"];
        $game = DI()->notorm->game
            ->select("*")
            ->where('liveuid=? and stream=? and state=?', $liveuid, $stream, 0)
            ->fetchOne();
        if ($game) {
            $action = $game["action"];
            $brandToken = $stream . "_" . $action . "_" . $game['starttime'] . "_Game";

            $brand = DI()->redis->get($brandToken);
            $brand = json_decode($brand, 1);
            $data = [];
            if ($action == 1) {
                $data[] = $brand[0][5];
                $data[] = $brand[1][5];
                $data[] = $brand[2][5];
            } else if ($action == 2) {
                $data[] = $brand[0][8];
                $data[] = $brand[1][8];
                $data[] = $brand[2][8];

            } else if ($action == 3) {
                $data[] = $brand[1];
                $data[] = $brand[2];
                $data[] = $brand[3];
                $data[] = $brand[4];
            } else if ($action == 4) {
                $data[] = $brand[0][8];
                $data[] = $brand[1][8];
                $data[] = $brand[2][8];
            } else if ($action == 5) {
                $data[] = $brand[0][5];
                $data[] = $brand[1][5];
                $data[] = $brand[2][5];
            }

            $rs['brand'] = $data;
            $time = 30 - (time() - $game['starttime']) + 3;
            if ($time < 0) {
                $time = "0";
            }
            $rs['time'] = (string)$time;
            $rs['id'] = (string)$game["id"];


            $rs['action'] = (string)$action;
            /* DI()->redis  -> set($BetToken,json_encode($data)); */
        }
        return $rs;
    }

    public function settleGame($uid, $gameid)
    {
        $game = DI()->notorm->game
            ->select("*")
            ->where('id=?', $gameid)
            ->fetchOne();
        if (!$game) {
            return 1000;
        }

        $coin = "coin_" . $game['result'];

        $sql = "select uid,sum(" . $coin . ") as gamecoin from cmf_users_gamerecord where gameid=:gameid and uid=:uid group by uid";
        $params = [':gameid' => $gameid, ':uid' => $uid];
        $total = DI()->notorm->user->queryAll($sql, $params);
        return $total;
    }
}