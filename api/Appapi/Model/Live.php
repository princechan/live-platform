<?php

class Model_Live extends Model_Common
{
    const KEY_ROOM_GAG = 'roomgag:';
    const KEY_CHANNEL_GAG = 'channelgag:';
    const KEY_VISITED_NUM = 'visitednum:';
    const KEY_CACHE_UVL = 'cache:uvl:';
    const KEY_CACHE_USERS_VEST = 'cache:users_vest';
    const KEY_SLIDE_LIVE_LIST = 'app_slide_live_list';
    const KEY_SLIDE_LIVE_V2 = 'app_slide_live_v2';
    const KEY_MULTI_SHOW = 'muti_show';

    /**
     * muti_show主表附属属性
     * @var array
     */
    public static $multiShowExtField = [
        'room_type' => 0, //房间类型 0-普通，1-锁门，2-扣券
        'room_password' => '',
        'room_live_coin' => 0,
    ];

    public function kicking($uid, $touid, $liveType, $liveId)
    {
        if (!$liveId || !$touid || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        if ($uid == $touid) {
            $this->setError('不能将自己踢出');
            return false;
        }

        $user_type = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
        if (!in_array($user_type, [40, 50, 60])) {
            $this->setError('您不是管理员，无权操作');
            return false;
        }

        $touser_type = Model_Auth_User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
        if ($touser_type == 50) {
            $this->setError('对方是主播，不能被踢出');
            return false;
        } else if ($touser_type == 40) {
            $this->setError('对方是管理员，不能被踢出');
            return false;
        } else if ($touser_type == 60) {
            $this->setError('对方是超管，不能被踢出');
            return false;
        }

        $this->setKick($liveType, $liveId, $touid);

        return true;
    }

    //踢人
    private function setKick($liveType, $liveId, $touid)
    {
        Model_Helper_Redis::getInstance()->hSet($this->getKickKey($liveType, $liveId), $touid, time() + 30);
        $this->delOnlineUser($liveType, $liveId, $touid);

        return true;
    }

    public function getKick($liveType, $liveId, $uid)
    {
        return Model_Helper_Redis::getInstance()->hGet($this->getKickKey($liveType, $liveId), $uid);
    }

    public function delKick($liveType, $liveId, $uid)
    {
        return Model_Helper_Redis::getInstance()->hDel($this->getKickKey($liveType, $liveId), $uid);
    }

    private function getKickKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return "kick:{$liveType}:{$liveId}";
    }

    /**
     * 禁言
     * @param $liveType
     * @param $liveId
     * @param $uid
     * @param $touid
     * @param $gagTime
     * @return bool
     */
    public function gag($liveType, $liveId, $uid, $touid, $gagTime)
    {
        if (!$liveId || !in_array($liveType, [0, 1]) || !$touid || !$gagTime) {
            $this->setError('参数错误');
            return false;
        }

        $uidI = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
        if ($uidI == 30) {
            $this->setError('你不是主播或者管理员');
            return false;
        }

        $touidI = Model_Auth_User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
        if ($touidI == 50) {
            $this->setError('对方是主播，不能禁言');
            return false;
        } else if ($touidI == 40) {
            $this->setError('对方是管理员，不能禁言');
            return false;
        } else if ($touidI == 60) {
            $this->setError('对方是超管，不能禁言');
            return false;
        }

        $key = $this->getGagKey($liveType, $liveId);
        if (!$key) {
            return false;
        }

        $redis = Model_Helper_Redis::getInstance();
        $result = (int)$redis->hGet($key, $touid);
        $time = time();
        if ($time < $result) {
            return true;
        }

        if ($gagTime) {
            $time = $time + $gagTime * 60;
        } else {
            $time = $time + 10 * 60;
        }

        $redis->hSet($key, $touid, $time);
        return true;
    }

    private function getGagKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_ROOM_GAG . "{$liveType}:{$liveId}";
    }

    /**
     * @param $liveType
     * @param $liveId
     * @param $touid
     * @return bool|int 0-你未被禁言,1-你已经被禁言
     */
    public function isGag($liveType, $liveId, $touid)
    {
        $key = $this->getGagKey($liveType, $liveId);
        if (!$key) {
            return 0;
        }

        $result = Model_Helper_Redis::getInstance()->hGet($key, $touid);
        if ($result && time() < $result) {
            return 1;
        }

        return 0;
    }

    /**
     * 获取直播房间在线用户列表数据
     * @param $liveType
     * @param $liveId
     * @return int
     */
    public function getOnlineUserNum($liveType, $liveId)
    {
        return (int)Model_Helper_Redis::getInstance()->hlen($this->getOnlineUserKey($liveType, $liveId));
    }

    /**
     * 排序 黑马 紫马 橙马 黄马 绿马 蓝马 白马 游客
     * @param $liveType
     * @param $liveId
     * @return array
     */
    public function getOnlineUserList($liveType, $liveId)
    {
        $list = Model_Helper_Redis::getInstance()->hGetAll($this->getOnlineUserKey($liveType, $liveId)) ?: [];
        $list = array_reverse($list);
        $black = $purple = $orange = $yellow = $green = $blue = $white = $other = $tmp = [];
        foreach ($list as $v) {
            $user = json_decode($v, true);
            if (is_numeric($user['id']) && $user['id'] > 0) {
                switch ($user['vest_id']) {
                    case 7:
                        $black[] = $user;
                        break;
                    case 6:
                        $purple[] = $user;
                        break;
                    case 5:
                        $orange[] = $user;
                        break;
                    case 4:
                        $yellow[] = $user;
                        break;
                    case 3:
                        $blue[] = $user;
                        break;
                    case 2:
                        $green[] = $user;
                        break;
                    case 1:
                        $white[] = $user;
                        break;
                    default:
                        $other[] = $user;
                }
            } else {
                $tmp[] = $user;
            }
        }

        return array_merge($black, $purple, $orange, $yellow, $green, $blue, $white, $other, $tmp);
    }

    private function getOnlineUserKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return "online:{$liveType}:{$liveId}";
    }

    public function addOnlineUser($liveType, $liveId, $uid, $data)
    {
        return Model_Helper_Redis::getInstance()->hSet($this->getOnlineUserKey($liveType, $liveId), $uid, json_encode($data));
    }

    private function delOnlineUser($liveType, $liveId, $uid)
    {
        return Model_Helper_Redis::getInstance()->hDel($this->getOnlineUserKey($liveType, $liveId), $uid);
    }

    private function formatSingleToMulti($data)
    {
        if (!$data) {
            return $data;
        }

        return [
            'id' => (string)$data['uid'],
            'live_name' => (string)$data['user_nicename'],
            'avatar' => (string)$data['avatar'],
            'avatar_thumb' => (string)$data['avatar_thumb'],
            'focus' => (string)$data['thumb'],
            'focus_thumb' => (string)$data['thumb'],
            'code' => '',
            'devide_family' => '',
            'status' => (string)$data['islive'],
            'remark' => '',
            'stream' => (string)$data['stream'],
            'pull' => (string)$data['pull'],
            'created_at' => '0',
            'updated_at' => '0',
            'speak_num' => (string)$data['chat_num'],
            'speak_frequency' => (string)$data['chat_frequency'],
            'multi_stream' => '',
            'room_type' => (string)$data['type'],
            'room_password' => (string)$data['type_val'],
            'room_live_coin' => (string)$data['type_val'],
            'anchor' => [],
            'dj' => [],
            'ami_anchor' => $data['ami_anchor'],
            'ami_dj' => false,
            'ami_manager' => $data['ami_manager'],
        ];
    }

    public function enterRoom($uid, $token, $liveType, $liveId, $city, $stream, $dataFor)
    {
        if (!in_array($liveType, [0, 1])) {
            $this->setError('参数live_type错误');
            return false;
        }
        if ($liveType == 0 && !$stream) {
            $this->setError('参数stream错误');
            return false;
        }

        if (!Model_Auth_User::getInstance()->isBan($uid)) {
            $this->setError('该账号已被禁用');
            return false;
        }

        //获取直播间设置的发言字数和发言频率
        if ($liveType == 0) {
            $live = DI()->notorm->users_live->where('uid=?', $liveId)->fetchOne();
            if ($live) {
                $chat_num = $live['chat_num'];
                $chat_frequency = $live['chat_frequency'];
                $live['ami_anchor'] = ($uid == $liveId);
                $live['ami_manager'] = in_array(Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, 0), [40, 50, 60]);
                if ($dataFor == 1) {
                    $live = $this->formatSingleToMulti($live);
                }
            }
        } else {
            $live = Model_MutiShow::getInstance()->getRoomInfo($liveId, $uid);
            $chat_num = @$live['speak_num'];
            $chat_frequency = @$live['speak_frequency'];
        }

        if (!$live) {
            $this->setError('直播已下线');
            return false;
        }

        if (!$chat_num) {
            $chat_num = 30;
        }
        if (!$chat_frequency) {
            $chat_frequency = 0;
        }

        $userinfo = Model_Auth_User::getInstance()->getUserInfo($uid);
        if ($userinfo['issuper'] == 1) {
            Model_Helper_Redis::getInstance()->hSet('super', $userinfo['id'], '1');
        } else {
            Model_Helper_Redis::getInstance()->hDel('super', $userinfo['id']);
        }
        if (!$city) {
            $city = '好像在火星';
        }

        if ($uid > 0) {
            Model_Auth_User::getInstance()->update($uid, ['city' => $city]);
        }

        $userinfo['city'] = $city;
        $userinfo['sign'] = md5("{$liveType}:{$liveId}:{$uid}");
        if ($uid > 0) {
            if ($uid == $liveId) {
                $userinfo['userType'] = 50;
            } else {
                $userinfo['userType'] = 30;
            }
        } else {//游客
            $userinfo['userType'] = 0;
            $userinfo['avatar_thumb'] = $userinfo['avatar'];
        }

        //游客数据只放3天
        $timeout = $uid > 0 ? null : 3 * 24 * 3600;
        Model_Helper_Redis::getInstance()->set($token, json_encode($userinfo), $timeout);

        $userlists = [];
        $nums = $this->getOnlineUserNum($liveType, $liveId);
        $list = $this->getOnlineUserList($liveType, $liveId) ?: [];
        foreach ($list as $v) {
            $guard = Model_Guard::getInstance()->checkIsGuard($v['id'], $liveId);
            $v['guard'] = $guard;
            $v['isguard'] = $guard['isGuard'];
            //获取用户的马甲
            $v['vestID'] = (string)$this->getUserVestId($v['id'], $liveType, $liveId);
            $userlists[] = $v;
        }

        $showvideo = '0';
        $ishowVideo = DI()->redis->hGet('ShowVideo', $liveId);
        if ($ishowVideo) {
            $showvideo = $ishowVideo;
        }

        $configpri = Model_Common::getInstance()->getConfigPri();
        $game = Model_Game::getInstance()->checkGame($liveId, $stream);
        $ismanager = (int)$this->checkManager($liveId, $uid);
        $guard = Model_Guard::getInstance()->checkIsGuard($uid, $liveId);

        $info = [
            'live_type' => $liveType,
            'live' => $live,
            'votestotal' => $liveType ? "" : (string)$this->getVotes($liveId),
            'barrage_fee' => $configpri['barrage_fee'],
            'userlist_time' => $configpri['userlist_time'],
            'chatserver' => $configpri['chatserver'],
            'push_url' => 'rtmp://' . $configpri['push_url'] . '/5showcam/',
            'pull_url' => '?vhost=' . $configpri['pull_url'],
            'showvideo' => $showvideo,
            'showvideo_url' => 'rtmp://' . $configpri['pull_url'] . '/5showcam/',
            'nums' => $nums,
            'game' => $game['brand'],
            'gametime' => $game['time'],
            'gameid' => $game['id'],
            'gameaction' => $game['action'],
            'coin' => $userinfo['coin'],
            'isauth' => $userinfo['isauth'],//用户是否认证
            'issuper' => $userinfo['issuper'],//用户是否是超管
            'ismanager' => strval($ismanager),//用户是否是主播的管理员
            'chat_num' => $chat_num,//房间发言字数
            'chat_frequency' => $chat_frequency,//房间发言频率
            'isGuard' => $guard['isGuard'],//用户是否是主播的守护
            'guard_level' => $guard['guard_level'],//用户守护等级
            'iswhite' => $userinfo['iswhite'],//用户是否在白名单
            'personal_chat_num' => $userinfo['chat_num'], //用户个人发言字数
            'personal_chat_frequency' => $userinfo['chat_frequency'], //用户个人发言频率
            'personal_send_url' => $userinfo['send_url'], //用户是否可以发送网址
            'isattention' => (string)Model_Auth_User::getInstance()->isAttention($uid, $liveId, $liveType),
            'userlists' => $userlists,
        ];

        /*******获取礼物PK的redis信息start********/
        $info['giftpk'] = [];
        $giftKey = "giftPK_" . $liveId;
        $giftPkInfo = Model_Helper_Redis::getInstance()->get($giftKey);
        if ($giftPkInfo) {
            $giftPkArr = json_decode($giftPkInfo, true);//转换为数组
            if ($giftPkArr['isEnd'] == 0) {
                //到PK结束的剩余时间
                $effectiveTime = $giftPkArr['lastStopTime'] - time();
                if ($effectiveTime <= 0) {//主播未正常停止游戏造成时间失效
                    Model_Helper_Redis::getInstance()->delete($giftKey);
                    $info['giftpk'] = [];
                } else {
                    $giftPkArr['effectiveTime'] = $effectiveTime;
                    $guestUserInfo = Model_Auth_User::getInstance()->getUserInfo($giftPkArr['guestID']);
                    $giftPkArr['guestAvatar'] = $guestUserInfo['avatar'];//客队头像
                    $giftPkArr['guestName'] = $guestUserInfo['user_nicename'];//客队昵称
                    $masterUserInfo = Model_Auth_User::getInstance()->getUserInfo($giftPkArr['uid']);
                    $giftPkArr['masterAvatar'] = $masterUserInfo['avatar'];//主队头像
                    $giftPkArr['masterName'] = $masterUserInfo['user_nicename'];//主队昵称
                    $guestGiftInfo = Model_Gift::getInstance()->getGiftInfo($giftPkArr['guestGiftID']);
                    $giftPkArr['guestGiftImg'] = $guestGiftInfo['gifticon'];//客队礼物图片
                    $masterGiftInfo = Model_Gift::getInstance()->getGiftInfo($giftPkArr['masterGiftID']);
                    $giftPkArr['masterGiftImg'] = $masterGiftInfo['gifticon'];//主队礼物图片
                    unset($giftPkArr['uid']);
                    unset($giftPkArr['guestID']);
                    $info['giftpk'][] = $giftPkArr;
                }
            }
        }
        /*******获取礼物PK的redis信息end********/
        /*******获取抢板凳的redis信息start********/
        $info['grabbench'] = [];
        $grabBenchKey = "grabbench_" . $liveId;
        $grabBenchInfo = DI()->redis->get($grabBenchKey);
        if ($grabBenchInfo) {
            $grabBenchArr = json_decode($grabBenchInfo, true);//解析成数组
            if ($grabBenchArr['isEnd'] == 1) {//活动已经结束
                DI()->redis->delete($grabBenchKey);//将redis删除
                $info['grabbench'] = [];
            } else {
                $grabbenchID = $grabBenchArr['grabbenchID'];
                //获取后台配置的连续点击时间间隔
                $hits_space = $configpri['grabbench_hits_space'];
                $effectiveTime = $grabBenchArr['effectiveTime'] - time();//到活动结束的剩余时间
                if ($effectiveTime <= 0) {
                    DI()->redis->delete($grabBenchKey);
                } else {
                    $grabBenchArr = [];
                    $grabBenchArr['hits_space'] = $hits_space;
                    $grabBenchArr['effectiveTime'] = strval($effectiveTime);
                    $grabBenchArr['grabbenchID'] = $grabbenchID;
                    $info['grabbench'][] = $grabBenchArr;
                }
            }
        }

        /*******获取抢板凳的redis信息end********/
        //获取房间是否被禁言
        $info['ischannelShutUp'] = $this->isChannelShutUp($liveType, $liveId);
        $vestID = $this->getUserVestId($uid, $liveType, $liveId);
        $vestInfo = $this->getUsersVest($vestID);
        $vestInfo['vest_man_url'] = Model_Helper_Func::getUrl($vestInfo['vest_man_url']);
        $vestInfo['vest_woman_url'] = Model_Helper_Func::getUrl($vestInfo['vest_woman_url']);
        $info['vestInfo'] = $vestInfo;

        $this->setRoomLivedNum($liveType, $liveId);

        return $info;
    }

    /**
     * 查看次数
     */
    public function getRoomLivedNum($liveType, $liveId)
    {
        return (int)Model_Helper_Redis::getInstance()->get($this->getVisitedNumKey($liveType, $liveId));
    }

    public function setRoomLivedNum($liveType, $liveId)
    {
        return Model_Helper_Redis::getInstance()->incr($this->getVisitedNumKey($liveType, $liveId));
    }

    public function delRoomLivedNum($liveType, $liveId)
    {
        return Model_Helper_Redis::getInstance()->del($this->getVisitedNumKey($liveType, $liveId));
    }

    private function getVisitedNumKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_VISITED_NUM . "{$liveType}:{$liveId}";
    }

    public function createRoom($uid, $token, $user_nicename, $avatar, $avatar_thumb,
                               $title, $province, $city, $lng, $lat, $type, $type_val, $chat_num, $chat_frequency)
    {
        $nowtime = time();
        $showid = $nowtime;
        $starttime = $nowtime;
        $stream = $uid . '_' . $nowtime;
        $pull = '';
        $push = $this->PrivateKeyA('rtmp', $stream, 0);
        $configpub = $this->getConfigPub();
        if ($configpub['maintain_switch'] == 1) {
            $this->setError($configpub['maintain_tips']);
            return false;
        }

        $isban = Model_Auth_User::getInstance()->isBan($uid);
        if (!$isban) {
            $this->setError('该账号已被禁用');
            return false;
        }

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            $this->setError('请重新登录');
            return false;
        }

        $configpri = $this->getConfigPri();
        if ($configpri['auth_islimit'] == 1) {
            $isauth = Model_Auth_User::getInstance()->isAuth($uid);
            if (!$isauth) {
                $this->setError('请先进行身份认证或等待审核');
                return false;
            }
        }

        $userinfo = Model_Auth_User::getInstance()->getUserInfo($uid);
        if ($userinfo['iswhite'] == 0) {
            $islimitIP = Model_Auth_User::getInstance()->isLimitIP($uid);
            if ($islimitIP) {
                $this->setError('您的IP已被封禁');
                return false;
            }
        }

        if ($configpri['level_islimit'] == 1) {
            if ($userinfo['level'] < $configpri['level_limit']) {
                $this->setError('等级小于' . $configpri['level_limit'] . '级，不能直播');
                return false;
            }
        }

        if (!$city) {
            $city = '好像在火星';
        }
        if (!$lng) {
            $lng = 0;
        }
        if (!$lat) {
            $lat = 0;
        }
        if (($type == 1 && $type_val == '') || ($type > 1 && $type_val <= 0)) {
            $this->setError('房间类型参数错误');
            return false;
        }

        if ($chat_num < 1 || $chat_num > 100) {
            $this->setError('发言字数限制1~100个字');
            return false;
        }

        if ($chat_frequency < 10 || $chat_frequency > 60) {
            $this->setError('发言频率限制10~60秒');
            return false;
        }

        $thumb = '';
        if (isset($_FILES['file']) && is_array($_FILES['file'])) {
            $file = $_FILES['file'];
            if ($file["error"] > 0) {
                DI()->logger->debug('failed to upload file with error: ' . $file['error']);
                $this->setError(T('failed to upload file with error: {error}', ['error' => $file['error']]));
                return false;
            }
            if (!$this->checkExt($file['name'])) {
                $this->setError('图片仅能上传 jpg,png,jpeg');
                return false;
            }
            $upload = Model_Helper_UploadFile::getInstance()->uploadOne($file);
            if (false === $upload) {
                return false;
            }

            $uploadFileInfo = Model_Helper_UploadFile::getInStance()->getUploadFileInfo();
            if (isset($uploadFileInfo[0]) && is_array($uploadFileInfo[0])) {
                $thumb = $uploadFileInfo[0]['dbsavepath'];
            } else {
                $this->setError('file上传失败');
                return false;
            }

            @unlink($_FILES['file']['tmp_name']);
        }

        $roomData = [
            "uid" => $uid,
            "avatar" => $avatar,
            "avatar_thumb" => $avatar_thumb,
            "user_nicename" => $user_nicename,
            "islive" => 1,
            "showid" => $showid,
            "starttime" => $starttime,
            "title" => $title,
            "province" => $province,
            "city" => $city,
            "stream" => $stream,
            "thumb" => $thumb,
            "pull" => $pull,
            "lng" => $lng,
            "lat" => $lat,
            "type" => $type,
            "type_val" => $type_val,
            "ispc" => 0,
            "chat_num" => $chat_num,
            "chat_frequency" => $chat_frequency,
        ];

        $result = $this->createSingleRoom($uid, $roomData);
        if ($result === false) {
            $this->setError('开播失败，请重试');
            return false;
        }

        Model_Auth_User::getInstance()->update($uid, ['city' => $city]);

        $userinfo['city'] = $city;
        //目前只有单直播
        $liveType = 0;
        $userinfo['sign'] = md5("{$liveType}:{$uid}:{$uid}");
        $userinfo['userType'] = 50;

        DI()->redis->set($token, json_encode($userinfo));
        $votestotal = $this->getVotes($uid);

        $info = [
            'liveid' => $uid,
            'userlist_time' => $configpri['userlist_time'],
            'barrage_fee' => $configpri['barrage_fee'],
            'chatserver' => $configpri['chatserver'],
            'votestotal' => $votestotal,
            'stream' => $stream,
            'push' => $push,
            'pull_wheat' => 'rtmp://' . $configpri['pull_url'] . '/5showcam/',
            'chat_num' => $chat_num,
            //用于主播在房间内发言时，判断是否匹配敏感词过滤（如果要求主播不过滤敏感词的话，将这个值改为1即可）
            'iswhite' => 0,
            'chat_frequency' => $chat_frequency,
        ];

        return $info;
    }

    private function createSingleRoom($uid, $data)
    {
        $isexist = DI()->notorm->users_live->select("uid")
            ->where('uid=?', $uid)
            ->fetchOne();

        if ($isexist) {
            $rs = DI()->notorm->users_live->where('uid = ?', $uid)->update($data);
        } else {
            $rs = DI()->notorm->users_live->insert($data);
        }

        //获取该主播所有的未抢完的红包
        $redPacketsKey = "redpackets:0:{$uid}";
        $redPacketsList = DI()->redis->hGetAll($redPacketsKey) ?: [];
        foreach ($redPacketsList as $k => $v) {
            $kArr = explode(':', $k);
            $senderID = $kArr[0];//发红包人的ID
            $sendTime = $kArr[1];//发红包的时间
            //去除字符串首尾的[]
            $v = trim($v, '[');
            $v = trim($v, ']'); //目前$v的值像这样： 0.56,0.44 或者只有一个时像这样0.56
            $sub = strpos($v, ",");//判断,在字符串中首次出现的位置
            $totalCoin = 0;//未被抢的红包的总金额
            if ($sub) {//说明是逗号分隔的多个数组
                $vArr = explode(',', $v);//将字符串分割成数组
                for ($i = 0; $i < count($vArr); $i++) {
                    $totalCoin += $vArr[$i];
                }
            } else {
                $totalCoin = $v;
            }

            //获取后台配置的金币和直播券兑换比例
            $configpri = $this->getConfigPri();
            $livecoin = $totalCoin * $configpri['live_coin_percent'];

            //获取该红包的记录
            $sendInfo = DI()->notorm->users_send_redpackets->where("uid=? and addtime=?", $senderID, $sendTime)->fetchOne();
            //向退款记录表中写入数据
            $data = [
                'uid' => $senderID,
                'packet_id' => $sendInfo['id'],
                'coin' => $totalCoin,
                'livecoin' => $livecoin,
                'addtime' => time(),

            ];
            DI()->notorm->redpcakets_back->insert($data);

            //更新该用户的直播券
            (new Model_Auth_User)->update($senderID, [
                'livecoin' => new NotORM_Literal("livecoin + {$livecoin}"),
            ]);
        }

        //将该主播直播间PK、红包、口令红包redis记录删除
        DI()->redis->delete('giftPK_' . $uid);
        DI()->redis->delete($redPacketsKey);
        DI()->redis->delete("redpacketstitle:0:{$uid}");

        return $rs;
    }

    public function checkLiveType($liveType)
    {
        if (!in_array($liveType, [0, 1])) {
            $this->setError('liveType error');
            return false;
        }
        return true;
    }

    /**
     * 修改直播状态
     * @param $uid
     * @param $liveType
     * @param $liveId
     * @param $stream
     * @param $status
     * @return bool
     */
    public function changeLive($uid, $liveType, $liveId, $stream, $status)
    {
        if (!$this->checkLiveType($liveType)) {
            return false;
        }
        if ($liveType == 0 && !$stream) {
            $this->setError('流不存在');
            return false;
        }

        if ($status == 1) {
            return $this->startRoom($liveType, $liveId, $stream);
        } else {
            return $this->stopRoom($uid, $liveType, $liveId, $stream);
        }
    }

    public function startRoom($liveType, $liveId, $stream)
    {
        if ($liveType == 0) {
            return $this->startSingleRoom($liveId, $stream);
        } elseif ($liveType == 1) {
            return $this->startMultiRoom($liveId);
        } else {
            return false;
        }
    }

    private function startSingleRoom($liveId, $stream)
    {
        $data = DI()->notorm->users_live->where('uid=? and stream=?', $liveId, $stream)->fetchOne();
        if (!$data) {
            $this->setError('直播不存在');
            return false;
        }

        $rs = DI()->notorm->users_live->where('uid=? and stream=?', $liveId, $stream)->update(['islive' => 1]);
        if (false === $rs) {
            $this->setError('开启失败');
            return false;
        }

        return true;
    }

    private function startMultiRoom($liveId)
    {
        $data = DI()->notorm->muti_show->where('id=?', $liveId)->fetchOne();
        if (!$data) {
            $this->setError('直播不存在');
            return false;
        }

        $rs = DI()->notorm->muti_show->where('id=?', $liveId)->update(['status' => 1]);
        if (false === $rs) {
            $this->setError('开启失败');
            return false;
        }

        return true;
    }

    public function stopRoom($uid, $liveType, $liveId, $stream)
    {
        if ($liveType == 0) {
            if ($uid != $liveId) {
                $this->setError('你不是主播，无权关闭直播');
                return false;
            }
            $rs = $this->stopSingleRoom($liveId, $stream);
        } elseif ($liveType == 1) {
            $rs = $this->stopMultiRoom($liveId);
        } else {
            $rs = false;
        }

        if ($rs) {
            $this->delRoomLivedNum($liveType, $liveId);
        }

        return $rs;
    }

    /**
     * 多直播间暂时没这个功能
     * @param $liveId
     * @return bool
     */
    private function stopMultiRoom($liveId)
    {
        return true;
        $data = DI()->notorm->muti_show->where('id=?', $liveId)->fetchOne();
        if (!$data) {
            $this->setError('直播不存在');
            return false;
        }

        $rs = DI()->notorm->muti_show->where('id=?', $liveId)->update(['status' => 0]);
        if (false === $rs) {
            $this->setError('关闭失败');
            return false;
        }

        return true;
    }

    private function stopSingleRoom($uid, $stream)
    {
        $info = DI()->notorm->users_live
            ->select("uid,showid,starttime,title,province,city,stream,lng,lat,type,type_val")
            ->where('uid=? and stream=? and islive="1"', $uid, $stream)
            ->fetchOne();

        if (!$info) {
            return true;
        }

        DI()->notorm->users_live->where('uid=?', $uid)->update(['islive' => 0]);

        $nowtime = time();
        $info['endtime'] = $nowtime;
        $stream2 = explode('_', $stream);
        $starttime = $stream2[1];

        $votes = DI()->notorm->users_coinrecord
            ->where('touid=? and showid=?', $uid, $starttime)
            ->sum('totalcoin');

        $livecoin = DI()->notorm->users_coinrecord
            ->where('touid=? and showid=?', $uid, $starttime)
            ->sum('total_livecoin');

        //获取
        $info['votes'] = 0;
        if ($votes || $livecoin) {
            $info['votes'] = $votes + $livecoin;
        }

        DI()->redis->hDel("livelist", $uid);
        DI()->redis->delete($uid . '_zombie');
        DI()->redis->delete($uid . '_zombie_uid');
        DI()->redis->delete('attention_' . $uid);
        DI()->redis->delete($this->getOnlineUserKey(0, $uid));
        DI()->redis->hDel("channelShutUp", $uid);//取消房间禁言

        $game = DI()->notorm->game
            ->select("*")
            ->where('stream=? and liveuid=? and state=?', $stream, $uid, "0")
            ->fetchOne();

        if ($game) {
            $coin = "coin_" . $game['result'];
            $sql = "select uid,sum(" . $coin . ") as gamecoin from cmf_users_gamerecord where gameid=:gameid group by uid";
            $params = [':gameid' => $game['id']];
            $total = DI()->notorm->user->queryAll($sql, $params);
            foreach ($total as $k => $v) {
                (new Model_Auth_User())->update($v['uid'], ['coin' => new NotORM_Literal("coin + {$v['gamecoin']}")]);
            }
            DI()->notorm->game
                ->where('id = ? and liveuid =?', $game['id'], $uid)
                ->update(['state' => '3', 'endtime' => time()]);
            $brandToken = $stream . "_" . $game["action"] . "_" . $game['starttime'] . "_Game";
            DI()->redis->delete($brandToken);
        }

        $info['nums'] = $this->getOnlineUserNum(0, $uid);
        $info['live_type'] = 0;
        $info['liveid'] = $uid;
        DI()->notorm->users_liverecord->insert($info);

        //将直播间PK的redis信息删除
        DI()->redis->delete('giftPK_' . $uid);
        //获取该直播间内所有的未抢完的红包
        $redPacketsKey = "redpackets:0:{$uid}";
        $redPacketsList = DI()->redis->hGetAll($redPacketsKey);//返回数组
        if ($redPacketsList) {
            foreach ($redPacketsList as $k => $v) {
                $kArr = explode(':', $k);
                $senderID = $kArr[0];//发红包人的ID
                $sendTime = $kArr[1];//发红包的时间
                //去除字符串首尾的[]
                $v = trim($v, '[');
                $v = trim($v, ']'); //目前$v的值像这样： 0.56,0.44 或者只有一个时像这样0.56
                $sub = strpos($v, ",");//判断,在字符串中首次出现的位置
                $totalCoin = 0;//未被抢的红包的总金额
                if ($sub) {//说明是逗号分隔的多个数组
                    $vArr = explode(',', $v);//将字符串分割成数组
                    for ($i = 0; $i < count($vArr); $i++) {
                        $totalCoin += $vArr[$i];
                    }
                } else {
                    $totalCoin = $v;
                }

                //获取后台配置的金币和直播券兑换比例
                $configpri = $this->getConfigPri();
                $livecoin = $totalCoin * $configpri['live_coin_percent'];
                //获取该红包的记录
                $sendInfo = DI()->notorm->users_send_redpackets->where("uid=? and addtime=?", $senderID, $sendTime)->fetchOne();
                if ($senderID > 1) {//1是管理员从后台发的红包，不用做退款处理
                    //向退款记录表中写入数据
                    $data = [
                        'uid' => $senderID,
                        'packet_id' => $sendInfo['id'],
                        'coin' => $totalCoin,
                        'livecoin' => $livecoin,
                        'addtime' => time(),

                    ];
                    DI()->notorm->redpcakets_back->insert($data);
                    //更新该用户的直播券
                    (new Model_Auth_User())->update($senderID, [
                        'livecoin' => new NotORM_Literal("livecoin + {$livecoin}"),
                    ]);
                }
            }
        }

        //将房间内所有红包redis删除
        DI()->redis->delete($redPacketsKey);
        //将房间内所有口令红包的口令redis记录删除
        DI()->redis->delete("redpacketstitle:0:{$uid}");
        DI()->redis->delete("grabbench_" . $uid);

        return true;
    }

    /**
     * 关播信息
     * @param $liveType
     * @param $liveId
     * @param $stream
     * @return array
     */
    public function stopInfo($liveType, $liveId, $stream)
    {
        if ($liveType == 0) {
            $rs = [
                'nums' => 0,
                'length' => 0,
                'votes' => 0,
            ];

            $streamArr = explode('_', $stream);
            $liveuid = $streamArr[0];
            $starttime = $streamArr[1];
            $liveinfo = DI()->notorm->users_liverecord
                ->select("starttime,endtime,nums,votes")
                ->where('uid=? and starttime=?', $liveuid, $starttime)
                ->fetchOne();
            if ($liveinfo) {
                $rs['length'] = $this->getSeconds($liveinfo['endtime'] - $liveinfo['starttime']);
                $rs['nums'] = $liveinfo['nums'];
                $rs['votes'] = $liveinfo['votes'];
            }
        } else {
            $rs = [];
        }

        return $rs;
    }

    /**
     * 时长格式化
     * @param $cha
     * @return string
     */
    private function getSeconds($cha)
    {
        $iz = floor($cha / 60);
        $hz = floor($iz / 60);
        $dz = floor($hz / 24);
        /* 秒 */
        $s = $cha % 60;
        /* 分 */
        $i = floor($iz % 60);
        /* 时 */
        $h = floor($hz / 24);
        /* 天 */
        if ($cha < 60) {
            return $cha . '秒';
        } else if ($iz < 60) {
            return $iz . '分' . $s . '秒';
        } else if ($hz < 24) {
            return $hz . '小时' . $i . '分' . $s . '秒';
        } else if ($dz < 30) {
            return $dz . '天' . $h . '小时' . $i . '分' . $s . '秒';
        }
    }

    /**
     * 获取房间信息统一方法
     * @param $multiId
     * @return array|bool
     */
    public function getMultiShowInfo($multiId)
    {
        $show = Model_MutiShow::getInstance()->get($multiId);
        if (!$show) {
            return false;
        }

        return array_merge($show, $this->getMultiShowExtData($multiId) ?: []);
    }

    public function getMultiShowExtData($multiId)
    {
        $data = Model_Helper_Redis::getInstance()->hGetAll(self::KEY_MULTI_SHOW . $multiId) ?: [];
        return array_merge(self::$multiShowExtField, $data);
    }

    /**
     * 检查直播状态
     */
    public function checkLive($uid, $liveType, $liveId, $stream)
    {
        if (!$this->checkLiveType($liveType)) {
            return false;
        }

        //maintain
        $configpub = Model_Common::getInstance()->getConfigPub();
        if ($configpub['maintain_switch'] == 1) {
            $this->setError($configpub['maintain_tips']);
            return false;
        }

        //is ban
        if (!Model_Auth_User::getInstance()->isBan($uid)) {
            $this->setError('该账号已被禁用');
            return false;
        }

        $userinfo = Model_Auth_User::getInstance()->getUserInfo($uid);
        if ($userinfo['iswhite'] == 0) {
            $islimitIP = Model_Auth_User::getInstance()->isLimitIP($uid);
            if ($islimitIP) {
                $this->setError('您的IP已被封禁');
                return false;
            }
        }

        //is kick
        $time = time();
        $kickTime = $this->getKick($liveType, $liveId, $uid);
        if ($kickTime > $time) {
            $surplus = $kickTime - $time;
            $this->setError('您已被踢出房间，剩余' . $surplus . '秒');
            return false;
        } else {
            $this->delKick($liveType, $liveId, $uid);
        }

        if ($liveType == 0) {
            return $this->checkSingleLive($uid, $liveId, $stream);
        } elseif ($liveType == 1) {
            return $this->checkMultiLive($uid, $liveId);
        } else {
            return true;
        }
    }

    private function checkSingleLive($uid, $liveId, $stream)
    {
        if (!$stream) {
            $this->setError('流参数缺失');
            return false;
        }

        $islive = DI()->notorm->users_live
            ->select("islive,type,type_val,starttime")
            ->where('uid=? and stream=?', $liveId, $stream)
            ->fetchOne();

        if (!$islive || $islive['islive'] == 0) {
            $this->setError('直播已结束');
            return false;
        }

        $rs['type'] = $islive['type'];
        $rs['type_msg'] = '';

        //获取后台配置信息
        $configPub = $this->getConfigPub();
        if ($islive['type'] == 1) {
            $rs['type_msg'] = md5($islive['type_val']);
        } else if ($islive['type'] == 2) {
            $userIdentity = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, 0);
            if (in_array($userIdentity, [40, 50, 60])) {//主播等放过
                $rs['type'] = '0';
                $rs['type_msg'] = '';
            } else {
                $rs['type_msg'] = '本房间为收费房间，需支付' . $islive['type_val'] . $configPub['name_coin'];
                if ($this->isRoomCharged($uid, $liveId, 0, $liveId, $stream)) {
                    $rs['type'] = '0';
                    $rs['type_msg'] = '';
                }
            }
        } else if ($islive['type'] == 3) {
            $rs['type_msg'] = '本房间为计时房间，每分钟支付需支付' . $islive['type_val'] . $configPub['name_coin'];
        }

        return $rs;
    }

    private function checkMultiLive($uid, $liveId)
    {
        $live = $this->getMultiShowInfo($liveId);
        if (!$live) {
            $this->setError('直播间不存在');
            return false;
        }

        if ($live['status'] == 0) {
            $this->setError('直播间关闭中');
            return false;
        } elseif ($live['status'] == 0) {
            $this->setError('直播间休息中');
            return false;
        }

        if (Model_Auth_User::getInstance()->isVisitor($uid)) {
            if ($live['room_type'] == 1) {
                $this->setError('当前房间为密码房间，请先登陆');
                return false;
            }
            if ($live['room_type'] == 2) {
                $this->setError('当前房间为付费房间，请先登陆');
                return false;
            }
        }

        $rs['type'] = $live['room_type'];
        $rs['type_msg'] = '';

        //获取后台配置信息
        $configPub = $this->getConfigPub();
        if ($live['room_type'] == 1) {
            $rs['type_msg'] = md5($live['room_password']);
        } else if ($live['room_type'] == 2) {
            $userIdentity = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, 1);
            if (in_array($userIdentity, [40, 50, 60])) {//主播等放过
                $rs['type'] = '0';
                $rs['type_msg'] = '';
            } else {
                $rs['type_msg'] = '本房间为收费房间，需支付' . $live['room_live_coin'] . $configPub['name_coin'];
                if ($this->isRoomCharged($uid, $liveId, 1, $liveId)) {
                    $rs['type'] = '0';
                    $rs['type_msg'] = '';
                }
            }
        } else if ($live['room_type'] == 3) {//暂时没用到
            $rs['type_msg'] = '本房间为计时房间，每分钟支付需支付' . $live['room_live_coin'] . $configPub['name_coin'];
        }

        return $rs;
    }

    /**
     * 是否已经付过费用
     * @param $uid
     * @param $touid
     * @param $liveType
     * @param $liveId
     * @param $stream
     * @return bool|mixed
     */
    public function isRoomCharged($uid, $touid, $liveType, $liveId, $stream = '')
    {
        if ($liveType == 0) {
            return DI()->notorm->users_coinrecord
                ->select('id')
                ->where('type="expend" and action="roomcharge" and uid=? and touid=? and live_type=? and liveid=? and stream=?', $uid, $touid, $liveType, $liveId, $stream)
                ->fetchOne();
        } elseif ($liveType == 1) {
            return DI()->notorm->users_coinrecord
                ->select('id')
                ->where('type="expend" and action="roomcharge" and uid=? and touid=? and live_type=? and liveid=?', $uid, $touid, $liveType, $liveId)
                ->fetchOne();
        } else {
            return true;
        }
    }

    public function roomCharge($uid, $liveType, $liveuid, $touid, $stream)
    {
        if ($liveType == 0) {
            if (!$stream) {
                $this->setError('流地址不存在');
                return false;
            }
            $islive = DI()->notorm->users_live
                ->select("islive,type,type_val,starttime")
                ->where('uid=?', $liveuid)
                ->fetchOne();
            if (!$islive || $islive['islive'] == 0) {
                $this->setError('直播已结束');
                return false;
            }
            if ($islive['type'] != 2) {
                $this->setError('该房间非扣费房间');
                return false;
            }
            $total = $islive['type_val'];
            if ($total <= 0) {
                $this->setError('房间费用有误');
                return false;
            }
            $showId = $islive['starttime'];
        } else {
            $multiLive = $this->getMultiShowInfo($liveuid);
            if ($multiLive['status'] == 0) {
                $this->setError('直播关闭');
                return false;
            } elseif ($multiLive['status'] == 2) {
                $this->setError('直播休息中');
                return false;
            }
            $total = $multiLive['room_live_coin'];
            if ($total <= 0) {
                $this->setError('房间费用有误');
                return false;
            }
            $showId = 0;
        }

        $user = DI()->notorm->users->select('livecoin')->where('id=?', $uid)->fetchOne();
        if ($this->isRoomCharged($uid, $touid, $liveType, $liveuid, $stream)) {
            return $user;
        }

        if ($user['livecoin'] < $total) {
            $this->setError('余额不足');
            return false;
        }

        (new Model_Auth_User())->update($uid, [
            'livecoin' => new NotORM_Literal("livecoin - {$total}"),
            'consumption' => new NotORM_Literal("consumption + {$total}"),
        ]);

        //更新直播 映票 累计映票
        (new Model_Auth_User)->update($liveuid, [
            'votes' => new NotORM_Literal("votes + {$total}"),
            'votestotal' => new NotORM_Literal("votestotal + {$total}"),
        ]);

        DI()->notorm->users_coinrecord->insert([
            "type" => 'expend',
            "action" => 'roomcharge',
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => 0,
            "giftcount" => 0,
            'total_livecoin' => $this->cnyToLiveCoin($total),
            "showid" => $showId,
            "addtime" => time(),
            'live_type' => $liveType,
            'liveid' => $liveuid,
            'stream' => $stream,
            "totalcoin" => $total,
        ]);

        $user = DI()->notorm->users->select('livecoin')->where('id=?', $uid)->fetchOne();
        return $user;
    }

    /**
     * 货币单位转换
     * @param $cny
     * @return string
     */
    public function cnyToLiveCoin($cny)
    {
        return $cny * $this->getExchangeRate();
    }

    /**
     * 获取汇率
     * @return int
     */
    public function getExchangeRate()
    {
        $config = $this->getConfigPri();
        $rate = isset($config['live_coin_percent']) ? $config['live_coin_percent'] : 1;
        if ($rate <= 0) {
            $rate = 1;
        }

        return $rate;
    }

    /* 判断是否僵尸粉 */
    public function isZombie($uid)
    {
        $userinfo = DI()->notorm->users
            ->select("iszombie")
            ->where("id='{$uid}'")
            ->fetchOne();

        return $userinfo['iszombie'];
    }

    /* 僵尸粉 */
    public function getZombie($stream, $where)
    {
        $ids = DI()->notorm->users_zombie
            ->select('uid')
            ->where("uid not in ({$where})")
            ->limit(0, 2)
            ->fetchAll();

        $info = [];
        if ($ids) {
            $ids2 = $this->array_column($ids, 'uid');
            $ids = implode(",", $ids2);

            $stream2 = explode('_', $stream);
            $showid = $stream2[1];

            $info = DI()->notorm->users
                ->select('id,user_nicename,avatar,sex,consumption,city')
                ->where("id in ({$ids}) ")
                ->fetchAll();
            foreach ($info as $k => $v) {
                $v['avatar'] = Model_Helper_Func::getUrl($v['avatar']);
                $info[$k]['avatar'] = $v['avatar'];
                $level = $this->getLevel($v['consumption']);
                $v['level'] = $level;
                $info[$k]['level'] = $level;
                $this->addOnlineUser(0, $v['id'], $v['id'], $v);
            }
        }

        return $info;
    }

    //弹窗
    public function getPop($touid, $liveType, $liveId)
    {
        $info = Model_Auth_User::getInstance()->getUserInfo($touid);
        if (!$info) {
            if (Model_Auth_User::getInstance()->isVisitor($touid)) {
                $this->delOnlineUser($liveType, $liveId, $touid);
                $this->setError('此游客已离开房间');
            } else {
                $this->setError('用户信息不存在');
            }
            return false;
        }

        $info['follows'] = Model_Auth_User::getInstance()->getFollowsNum($touid);
        $info['fans'] = Model_Auth_User::getInstance()->getFansNum($touid);
        $info['consumption'] = Model_Helper_Func::numberFormat($info['consumption']);
        $info['votestotal'] = Model_Helper_Func::numberFormat($info['votestotal']);
        $info['follows'] = Model_Helper_Func::numberFormat($info['follows']);
        $info['fans'] = Model_Helper_Func::numberFormat($info['fans']);
        unset($info['province']);
        unset($info['birthday']);
        unset($info['issuper']);
        return $info;
    }

    /**
     * 礼物列表
     * @return mixed
     */
    public function getGiftList()
    {
        $rs = DI()->notorm->gift
            ->select("id,type,giftname,needcoin,gifticon")
            ->order("orderno asc")
            ->fetchAll() ?: [];
        //获取后台配置的直播券和钻石兑换比例
        $configPri = $this->getConfigPri();
        foreach ($rs as &$v) {
            $v['gifticon'] = Model_Helper_Func::getUrl($v['gifticon']);
            $v['livecoin'] = $configPri['live_coin_percent'] * $v['needcoin'];//TODO 可以统一 live_coin_percent
        }

        return $rs;
    }

    //赠送礼物
    public function sendGift($uid, $touid, $liveType, $liveId, $stream, $giftid, $giftcount, $giftGroupNum, $type)
    {
        if (!$uid || !$touid || !$liveId) {
            $this->setError('参数缺失');
            return false;
        }

        if ($liveType == 0 && !$stream) {
            $this->setError('参数stream缺失');
            return false;
        }

        $userinfo = DI()->notorm->users
            ->select('*')
            ->where('id = ?', $uid)
            ->fetchOne();

        $giftinfo = DI()->notorm->gift
            ->select("giftname,gifticon,needcoin,is_win_gift")
            ->where('id=?', $giftid)
            ->fetchOne();

        if (!$giftinfo) {
            $this->setError('礼物信息不存在');
            return false;
        }

        if ($userinfo['livecoin'] <= 0) {
            $this->setError('余额不足');
            return false;
        }

        $coin = $userinfo['coin'];
        $total = $giftinfo['needcoin'] * $giftcount * $giftGroupNum;
        $addtime = time();
        $consumptionType = 'expend';
        $action = 'sendgift';

        //获取后台配置的钻石和直播券配置比例
        $configpri = $this->getConfigPri();
        $live_coin_percent = $configpri['live_coin_percent'];//直播券和钻石兑换比例
        $stream2 = explode('_', $stream);
        $showid = $stream2[1];
        $total_livecoin = $total * $live_coin_percent;

        if ($type == 0) {//未提示用户直播券不足
            $userinfo['livecoin'] = floatval($userinfo['livecoin']);
            if ($userinfo['livecoin'] > 0) {
                //判断用户的直播券是否足够支付该礼物价格
                if (($userinfo['livecoin'] - $total_livecoin >= 0)) {//用户有直播券且足够支付该礼物的价格【直接扣除用户的直播券】
                    $sendCoin = $total;//用户赠送的钻石，虽然扣除的是直播券，但giftPK上要加礼物的钻石价格
                    // 用户直播券扣除
                    (new Model_Auth_User())->update($uid, [
                        'livecoin' => new NotORM_Literal("livecoin - {$total_livecoin}"),
                    ]);

                    /* 更新当前用户的经验值 */
                    (new Model_Auth_User)->update($uid, [
                        'consumption' => new NotORM_Literal("consumption + {$total}"),
                    ]);

                    //主播映票增加
                    (new Model_Auth_User)->update($touid, [
                        'votes' => new NotORM_Literal("votes + {$total}"),
                        'votestotal' => new NotORM_Literal("votestotal + {$total}"),
                    ]);

                    //向消费记录里添加一条数据
                    $insert = [
                        "type" => $consumptionType,
                        "action" => $action,
                        "uid" => $uid,
                        "touid" => $touid,
                        "giftid" => $giftid,
                        "giftcount" => $giftcount,
                        "giftgroup_num" => $giftGroupNum,
                        "totalcoin" => $total,
                        "showid" => $showid,
                        "total_livecoin" => $total_livecoin,
                        "addtime" => $addtime,
                        'live_type' => $liveType,
                        'liveid' => $liveId,
                    ];
                    DI()->notorm->users_coinrecord->insert($insert);
                } else {
                    $this->setError('余额不足');
                    return false;
                }
            }
        } else if ($type == 1) {//确认 [直播券不够，直接用钻石支付]
            //暂时不开启 钻石支付
            $this->setError('余额不足');
            return false;

            $sendCoin = $total;//用户赠送的钻石，虽然扣除的是直播券，但giftPK上要加礼物的钻石价格
            /* 更新用户余额 消费 */
            $isuid = (new Model_Auth_User)->update($uid, [
                'coin' => new NotORM_Literal("coin - ({$total})"),
                'consumption' => new NotORM_Literal("consumption + {$total}"),
            ]);

            $removeStatus = $this->removeApiUserCoin($userinfo['user_login'], $userinfo['user_pass'], $total - $winCoin);
            if ($removeStatus != 0) {
                return 1001;
            }

            /* 更新当前用户的经验值 */
            $isuid = (new Model_Auth_User)->update($uid, ['consumption' => new NotORM_Literal("consumption + {$total}")]);

            //更新主播的映票，累计映票
            $istouid = (new Model_Auth_User)->update($touid, [
                'votes' => new NotORM_Literal("votes + {$total}"),
                'votestotal' => new NotORM_Literal("votestotal + {$total}"),
            ]);

            //向消费记录里添加一条数据
            $insert = [
                "type" => $consumptionType,
                "action" => $action,
                "uid" => $uid,
                "touid" => $touid,
                "giftid" => $giftid,
                "giftcount" => $giftcount,
                "giftgroup_num" => $giftGroupNum,
                "totalcoin" => $total,
                "showid" => $showid,
                "total_livecoin" => 0,
                "addtime" => $addtime,
            ];
            $isup = DI()->notorm->users_coinrecord->insert($insert);
        }

        /*******判断中奖礼物start********/
        $iswin = 0;//默认不中奖
        $winCoin = 0;//中奖钻石数
        //判断该礼物是不是中奖礼物
        if (intval($giftinfo['is_win_gift']) == 1) {
            //获取后台配置信息
            $configPub = $this->getConfigPub();
            //获取后台配置的中奖几率
            $gift_win_percent = $configPub['gift_win_percent'];
            //获取中奖倍率
            $gift_win_multiple = $configPub['gift_win_multiple'];
            if ($gift_win_percent >= 100) {
                $gift_win_percent = 100;
            } else if ($gift_win_percent < 0) {
                $gift_win_percent = 0;
            }

            if ($gift_win_percent > 0 && $gift_win_percent < 100) {
                $randNum = rand(1, 99);
                if ($randNum <= $gift_win_percent) {//中奖了
                    //计算中奖的金额
                    $winCoin = $gift_win_multiple * $total;
                    //计算中奖的直播券
                    $winLiveCoin = $live_coin_percent * $winCoin;
                    //向中奖纪录表里添加数据
                    $data = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'stream' => $stream,
                        'giftid' => $giftid,
                        'gift_coin' => $giftinfo['needcoin'],
                        'gift_win_multiple' => floatval($gift_win_multiple),
                        'win_coin' => $winCoin,
                        'addtime' => time(),
                        'win_livecoin' => $winLiveCoin,
                    ];

                    DI()->notorm->users_sendgift_win->insert($data);
                    $iswin = 1;
                }
            } else if ($gift_win_percent == 100) {//直接中奖
                //计算中奖的金额
                $winCoin = $gift_win_multiple * $total;
                //计算中奖的直播券
                $winLiveCoin = $live_coin_percent * $winCoin;
                //向中奖纪录表里添加数据
                $data = [
                    'uid' => $uid,
                    'liveuid' => $liveId,
                    'stream' => $stream,
                    'giftid' => $giftid,
                    'gift_coin' => $giftinfo['needcoin'],
                    'gift_win_multiple' => floatval($gift_win_multiple),
                    'win_coin' => $winCoin,
                    'addtime' => time(),
                    'win_livecoin' => $winLiveCoin,
                ];

                DI()->notorm->users_sendgift_win->insert($data);
                $iswin = 1;
            }
        }

        /*******判断中奖礼物end********/
        //判断用户如果中奖，给用户加直播券
        if ($iswin == 1) {
            (new Model_Auth_User)->update($uid, ['livecoin' => new NotORM_Literal("livecoin + {$winLiveCoin}")]);
        }

        /*******主播发起送礼物PK时，用户送礼物给相应主播加数据start********/
        $giftPKinfo = DI()->redis->get('giftPK_' . $liveId);
        //判断redis数据是否存在
        if ($giftPKinfo) {
            $pkArr = json_decode($giftPKinfo, true);//将json转为数组
            //获取pk的状态数据
            $pkIsEnd = $pkArr['isEnd'];
            if ($pkIsEnd == 1) {//代表PK已经结束
                DI()->redis->delete("giftPK_" . $liveId);
            } else {//pk未结束
                //获取礼物PK数据的id
                $pkID = $pkArr['pkID'];
                //获取pk redis里的主队礼物id
                $masterGiftID = $pkArr['masterGiftID'];
                //获取pk redis里的客队礼物id
                $guestGiftID = $pkArr['guestGiftID'];
                //客队人id
                $guestID = $pkArr['guestID'];

                if ($masterGiftID == $giftid) {//赠送礼物同主队礼物相同
                    $pkArr['masterGiftNum'] += $giftcount * $giftGroupNum;
                    $pkArr['masterGiftTotalCoin'] += $sendCoin;

                    //写入数据库pk赠送记录表中
                    $dataMsg = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'giftid' => $giftid,
                        'num' => $giftcount,
                        'group_num' => $giftGroupNum,
                        'coin' => $sendCoin,
                        'guestid' => $guestID,
                        'send_type' => 0,
                        'pk_id' => $pkID,
                        'addtime' => time(),
                    ];
                    DI()->notorm->giftpk_lists->insert($dataMsg);
                }

                if ($guestGiftID == $giftid) {//赠送的礼物同客队礼物相同
                    $pkArr['guestGiftNum'] += $giftcount * $giftGroupNum;
                    $pkArr['guestGiftTotalCoin'] += $sendCoin;

                    //写入数据库pk赠送记录表中
                    $dataMsg = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'giftid' => $giftid,
                        'num' => $giftcount,
                        'group_num' => $giftGroupNum,
                        'coin' => $sendCoin,
                        'guestid' => $guestID,
                        'send_type' => 1,
                        'pk_id' => $pkID,
                        'addtime' => time(),
                    ];
                    DI()->notorm->giftpk_lists->insert($dataMsg);
                }

                //将数组重新打包成json串
                $giftPKinfo = json_encode($pkArr);
                DI()->redis->set("giftPK_" . $liveId, $giftPKinfo);
            }
        }

        /*******主播发起送礼物PK时，用户送礼物给相应主播加数据end********/
        $userinfo2 = DI()->notorm->users
            ->select('consumption,coin,livecoin')
            ->where('id = ?', $uid)
            ->fetchOne();

        $level = $this->getLevel($userinfo2['consumption']);
        $votestotal = $this->getVotes($uid);
        $gifttoken = md5(md5($action . $uid . $liveId . $giftid . $giftcount . $total . $showid . $addtime . rand(100, 999)));
        $balanceAmount = $coin - $total > 0 ? $coin - $total : 0;
        $latestCoin = number_format($balanceAmount, 1);

        $result = [
            "uid" => $uid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "giftname" => $giftinfo['giftname'],
            "gifticon" => Model_Helper_Func::getUrl($giftinfo['gifticon']),
            "level" => $level,
            "coin" => $latestCoin,
            "votestotal" => $votestotal,
            "gifttoken" => $gifttoken,
            "iswin" => $iswin,
            "winCoin" => $winCoin,
            "winLiveCoin" => $winLiveCoin,
            'giftGroupNum' => $giftGroupNum,
            "livecoin" => $userinfo2['livecoin'],
        ];

        return $result;
    }

    // 发送弹幕
    public function sendBarrage($uid, $touid, $liveType, $liveId, $content)
    {
        if (!$content) {
            $this->setError('弹幕内容不能为空');
            return false;
        }
        if (!$touid) {
            $this->setError('发送给的主播不能为空');
            return false;
        }

        $userinfo = DI()->notorm->users->where('id = ?', $uid)->fetchOne();
        $configpri = $this->getConfigPri();
        $giftinfo = [
            "giftname" => '弹幕',
            "gifticon" => '',
            "needcoin" => $configpri['barrage_fee'],
        ];

        $giftid = 1;
        $giftcount = 1;
        $total = $giftinfo['needcoin'] * $giftcount;

        $addtime = time();
        $action = 'sendbarrage';

        if ($userinfo['livecoin'] < $total) {
            $this->setError('余额不足');
            return false;
        }

        (new Model_Auth_User)->update($uid, [
            'livecoin' => new NotORM_Literal("livecoin - {$total}"),
            'consumption' => new NotORM_Literal("consumption + {$total}"),
        ]);

        /* 更新直播 魅力值 累计魅力值 */
        (new Model_Auth_User)->update($touid, [
            'votes' => new NotORM_Literal("votes + {$total}"),
            'votestotal' => new NotORM_Literal("votestotal + {$total}"),
        ]);

        $insert = [
            "type" => 'expend',
            "action" => $action,
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "showid" => $liveId,
            "addtime" => $addtime,
            'live_type' => $liveType,
            'liveid' => $liveId,
        ];
        DI()->notorm->users_coinrecord->insert($insert);

        $userinfo2 = DI()->notorm->users
            ->select('consumption,coin')
            ->where('id = ?', $uid)
            ->fetchOne();

        $level = $this->getLevel($userinfo2['consumption']);
        $votestotal = $this->getVotes($touid);
        $barragetoken = md5(md5($action . $uid . $total . $giftid . $giftcount . $total . $liveId . $addtime . rand(100, 999)));

        $result = [
            "uid" => $uid,
            "content" => $content,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "giftname" => $giftinfo['giftname'],
            "gifticon" => $giftinfo['gifticon'],
            "level" => $level,
            "coin" => $userinfo['coin'],
            "barragetoken" => $barragetoken,
            "votestotal" => $votestotal,
        ];
        DI()->redis->set($barragetoken, json_encode($result));

        return [
            'barragetoken' => $barragetoken,
            'level' => $result['level'],
            'livecoin' => $total,
        ];
    }

    public function setAdmin($uid, $liveuid, $touid)
    {
        if ($liveuid != $uid) {
            $this->setError('你不是该房间主播，无权操作');
            return false;
        }

        $userLiveManagerMod = DI()->notorm->users_livemanager;
        $isexist = $userLiveManagerMod
            ->select("*")
            ->where('uid=? and  liveuid=?', $touid, $liveuid)
            ->fetchOne();
        if (!$isexist) {
            $count = $userLiveManagerMod
                ->where('liveuid=?', $liveuid)
                ->count();
            if ($count >= 5) {
                $this->setError('最多设置5个管理员');
                return false;
            }
            $rs = $userLiveManagerMod
                ->insert(["uid" => $touid, "liveuid" => $liveuid]);
            if ($rs !== false) {
                return 1;
            }
        } else {
            $rs = $userLiveManagerMod
                ->where('uid=? and  liveuid=?', $touid, $liveuid)
                ->delete();
            if ($rs !== false) {
                return 0;
            }
        }

        $this->setError('操作失败，请重试');
        return false;
    }

    /**
     * 管理员列表
     * @param $liveType
     * @param $liveId
     * @return mixed
     */
    public function getAdminList($liveType, $liveId)
    {
        if (!in_array($liveType, [0, 1]) || !$liveId) {
            $this->setError('参数错误');
            return false;
        }

        $list = $this->getOnlineUserList($liveType, $liveId) ?: [];
        $userIds = array_column($list, 'id');
        //紫马、黑马、橙马
        $userIdStr = implode(',', $userIds);
        $rs = $userIds ? DI()->notorm->users->select('id')
            ->where("id IN($userIdStr) and vest_id IN (5,6,7)")->fetchAll() : [];
        $list = [];
        foreach ($rs as $k => $v) {
            $value = Model_Auth_User::getInstance()->getUserInfo($v['id']);
            $list[] = $value;
        }

        return $list;
    }

    /**
     * 举报
     * @param $uid
     * @param $touid
     * @param $content
     * @return mixed
     */
    public function setReport($uid, $touid, $content)
    {
        if (!$content) {
            $this->setError('举报内容不能为空');
            return false;
        }

        $data = ["uid" => $uid, "touid" => $touid, 'content' => $content, 'addtime' => time(), 'uptime' => time(),];
        $exist = DI()->notorm->users_report->where('uid=? AND touid=?', $uid, $touid)->fetchOne();
        if ($exist) {
            $rs = DI()->notorm->users_report->where('id = ?', $exist['id'])->update($data);
        } else {
            $rs = DI()->notorm->users_report->insert($data);
        }

        if (!$rs) {
            $this->setError('举报失败，请重试');
            return false;
        }

        return true;
    }

    /**
     * 主播总映票
     * @param $liveuid
     * @return mixed
     */
    public function getVotes($liveuid)
    {
        $liveuid = (int)$liveuid;
        $info = DI()->notorm->users
            ->select("votestotal")
            ->where('id=?', $liveuid)
            ->fetchOne();
        return $info ? $info['votestotal'] : 0;
    }

    /**
     * 超管关闭直播间
     * @param $uid
     * @param $token
     * @param $liveuid
     * @param int $type 0-超管关闭直播  1-禁止直播
     * @return int
     */
    public function superStopRoom($uid, $token, $liveType, $liveuid, $type)
    {
        $userinfo = DI()->notorm->users
            ->select("token,expiretime,issuper")
            ->where('id=? ', $uid)
            ->fetchOne();
        if ($userinfo['token'] != $token || $userinfo['expiretime'] < time()) {
            $this->setError('token已过期，请重新登陆');
            return false;
        }

        if ($userinfo['issuper'] == 0) {
            $this->setError('你不是超管，无权操作');
            return false;
        }

        //关闭并禁用
        if ($liveType == 0 && $type == 1) {
            (new Model_Auth_User)->update($liveuid, ['user_status' => 0]);
        }

        $rs = $liveType == 0 ? $this->superStopSingleRoom($liveuid) : $this->superStopMultiRoom($liveuid);
        if ($rs) {
            $this->delRoomLivedNum($liveType, $liveuid);
        }

        return $rs;
    }

    private function superStopMultiRoom($liveId)
    {
        return true;
    }

    private function superStopSingleRoom($liveuid)
    {
        $info = DI()->notorm->users_live
            ->select("uid,showid,starttime,title,province,city,stream,lng,lat,type,type_val")
            ->where('uid=? and islive="1"', $liveuid)
            ->fetchOne();

        if ($info) {
            $nowtime = time();
            $info['endtime'] = $nowtime;
            $info['nums'] = $this->getOnlineUserNum(0, $liveuid);
            $info['live_type'] = 0;
            $info['liveid'] = $liveuid;
            DI()->notorm->users_liverecord->insert($info);

            DI()->redis->hDel("livelist", $liveuid);
            DI()->redis->delete($liveuid . '_zombie');
            DI()->redis->delete($liveuid . '_zombie_uid');
            DI()->redis->delete('attention_' . $liveuid);
            DI()->redis->delete($this->getOnlineUserKey(0, $liveuid));
        }

        DI()->notorm->users_live->where('uid=?', $liveuid)->update(['islive' => 0]);

        //将直播间PK的redis信息删除
        DI()->redis->delete('giftPK_' . $liveuid);

        //获取该直播间内所有的未抢完的红包
        $redPacketsKey = "redpackets:0:{$liveuid}";

        $redPacketsList = DI()->redis->hGetAll($redPacketsKey);//返回数组
        if ($redPacketsList) {
            foreach ($redPacketsList as $k => $v) {
                $kArr = explode(':', $k);
                $senderID = $kArr[0];//发红包人的ID
                $sendTime = $kArr[1];//发红包的时间
                //去除字符串首尾的[]
                $v = trim($v, '[');
                $v = trim($v, ']'); //目前$v的值像这样： 0.56,0.44 或者只有一个时像这样0.56
                $sub = strpos($v, ",");//判断,在字符串中首次出现的位置
                $totalCoin = 0;//未被抢的红包的总金额
                if ($sub) {//说明是逗号分隔的多个数组
                    $vArr = explode(',', $v);//将字符串分割成数组
                    for ($i = 0; $i < count($vArr); $i++) {
                        $totalCoin += $vArr[$i];
                    }
                } else {
                    $totalCoin = $v;
                }

                //获取后台配置的金币和直播券兑换比例
                $configpri = $this->getConfigPri();

                $livecoin = $totalCoin * $configpri['live_coin_percent'];

                //获取该红包的记录
                $sendInfo = DI()->notorm->users_send_redpackets->where("uid=? and addtime=?", $senderID, $sendTime)->fetchOne();
                //向退款记录表中写入数据
                $data = [
                    'uid' => $senderID,
                    'packet_id' => $sendInfo['id'],
                    'coin' => $totalCoin,
                    'livecoin' => $livecoin,
                    'addtime' => time(),

                ];
                DI()->notorm->redpcakets_back->insert($data);

                //更新该用户的直播券
                (new Model_Auth_User)->update($senderID, ['livecoin' => new NotORM_Literal("livecoin + {$livecoin}")]);
            }
        }

        //将房间内所有红包redis删除
        DI()->redis->delete($redPacketsKey);
        //将房间内所有口令红包的口令redis记录删除
        DI()->redis->delete("redpacketstitle:0:{$liveuid}");
        DI()->redis->delete("grabbench_" . $liveuid);

        return true;
    }

    /**
     * 频道第一条
     * @return array|bool
     */
    public function getFirstLive()
    {
        $info = [];
        $liveid = DI()->notorm->channel->select("liveid")->order('orderno asc')->fetchOne();
        $field = 'uid,avatar,avatar_thumb,user_nicename,title,city,stream,lng,lat,pull,thumb,ispc,chat_num,chat_frequency';
        if ($liveid) {
            $info = DI()->notorm->users_live
                ->select($field)
                ->where("islive=1 and uid={$liveid['liveid']}")
                ->order("starttime desc")
                ->fetchOne();
        }
        if (!$info) {
            $info = DI()->notorm->users_live
                ->select($field)
                ->where("islive=1")
                ->order("starttime desc")
                ->fetchOne();
        }

        if (!$info) {
            $this->setError('暂无直播');
            return false;
        }

        $info['nums'] = (string)$this->getOnlineUserNum(0, $info['uid']);
        if (!$info['thumb']) {
            $info['thumb'] = $info['avatar'];
        }
        $info['pull'] = $this->PrivateKeyA('rtmp', $info['stream'], 0);
        //$info['pull'] = Model_Common::getM3u8Stream($info['stream']);

        return $info;
    }

    public function getChannelLive($p)
    {
        $pnum = 50;
        $start = ($p - 1) * $pnum;
        $result = DI()->notorm->users_live->where(['islive' => 1])
            ->order('starttime DESC')
            ->limit($start, $pnum)
            ->fetchAll() ?: [];
        foreach ($result as & $v) {
            $v['nums'] = (int)$this->getRoomLivedNum(0, $v['uid']);
            $v['avatar'] = Model_Helper_Func::getUrl($v['avatar']);
            $v['avatar_thumb'] = Model_Helper_Func::getUrl($v['avatar_thumb']);
            $v['thumb'] = $v['thumb'] ? Model_Helper_Func::getUrl($v['thumb']) : $v['avatar'];
            $v['pull'] = $this->PrivateKeyA('rtmp', $v['stream'], 0);
            $rs = $this->getLiveChatMsg($v['uid']);
            $v['chat_num'] = $rs['chat_num'];
            $v['chat_frequency'] = $rs['chat_frequency'];
        }

        return $result;
    }

    public function checkManager($liveuid, $uid)
    {
        $result = DI()->notorm->users_livemanager
            ->select("*")
            ->where("uid=? and liveuid=?", $uid, $liveuid)
            ->fetchOne();

        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }

    //发红包
    public function sendRedPackets($uid, $liveType, $liveId, $num, $totalMoney, $title, $type)
    {
        if ($num > 10000 || $num < 1) {
            $this->setError('红包数量错误');
            return false;
        }
        if ($totalMoney < 1) {
            $this->setError('红包金额错误');
            return false;
        }
        if (is_float($totalMoney)) {
            $this->setError('请填写整数');
            return false;
        }

        if (0.01 * $num > $totalMoney) {
            $this->setError('单个红包金额最少0.01元，请重新选择');
            return false;
        }

        $configPri = $this->getConfigPri();
        $live_coin_percent = $configPri['live_coin_percent'];
        $totalLiveCoinMoney = $live_coin_percent * $totalMoney;
        $userinfo = DI()->notorm->users->select('livecoin')->where("id=?", $uid)->fetchOne();
        if ($userinfo['livecoin'] < $totalLiveCoinMoney) {
            $this->setError('金额不足，无法发送红包');
            return false;
        }

        (new Model_Auth_User)->update($uid, [
            'livecoin' => new NotORM_Literal("livecoin - {$totalLiveCoinMoney}"),
            'consumption' => new NotORM_Literal("consumption + {$totalMoney}"),
        ]);

        $now = time();
        $rs = [];
        //判断红包的类型
        if ($type == 1) { //口令红包
            //将口令记录到redis里
            DI()->redis->hSet("redpacketstitle:{$liveType}:{$liveId}", "{$uid}:{$now}", $title);
        }

        $total = $totalMoney;//红包总金额
        $min = 0.01;//每个人最少能收到0.01元
        for ($i = 1; $i < $num; $i++) {
            $safe_total = ($total - ($num - $i) * $min) / ($num - $i);//随机安全上限
            $money = mt_rand($min * 100, $safe_total * 100) / 100;
            $total = $total - $money;
            $rs[] = $money;
        }

        $rs[] = $total;
        DI()->redis->hSet("redpackets:{$liveType}:{$liveId}", "{$uid}:{$now}", json_encode($rs));

        DI()->notorm->users_send_redpackets->insert([
            'uid' => $uid,
            'live_type' => $liveType,
            'liveid' => $liveId,
            'num' => $num,
            'total_money' => $totalMoney,
            'title' => $title,
            'type' => $type,
            'addtime' => $now,
        ]);

        return strval($now);
    }

    //抢红包
    public function robRedPackets($uid, $liveType, $liveId, $sendRedUid, $sendtime, $type, $title)
    {
        $result = [
            'state_code' => 0,
            'msg' => '',
        ];

        //获取后台配置的钻石和直播券兑换比例
        $configPri = $this->getConfigPri();
        $live_coin_percent = $configPri['live_coin_percent'];

        $redPacketsKey = "redpackets:{$liveType}:{$liveId}";
        $redPacketsHashKey = "{$sendRedUid}:{$sendtime}";
        $redPackets = DI()->redis->hGet($redPacketsKey, $redPacketsHashKey);
        $redPacketsTitleKey = "redpacketstitle:{$liveType}:{$liveId}";

        if (!$redPackets) {
            $result['state_code'] = 1001;
            $result['msg'] = '红包已抢完';
            return $result;
        }

        //判断用户是否已经抢过红包
        $robMsg = DI()->notorm->users_rob_redpackets
            ->where('uid=? and senduid=? and live_type=? and liveid=? and sendtime=? and type=?', $uid, $sendRedUid, $liveType, $liveId, $sendtime, $type)
            ->fetchOne();
        if ($robMsg) { //记录存在
            $result['state_code'] = 1003;
            $result['msg'] = '你已经抢到红包了';
            return $result;
        }

        //判断红包类型
        if ($type == 1) {  //如果是口令红包
            //从redis里取出口令
            $redisPacketTitle = DI()->redis->hGet($redPacketsTitleKey, $redPacketsHashKey);
            if ($redisPacketTitle != $title) { //判断口令是否一致
                $result['state_code'] = 1002;
                $result['msg'] = '口令错误';
                return $result;
            }
        }

        $packetArr = json_decode($redPackets, true);//将json转为数组
        //判断数组的长度
        $packetCount = count($packetArr);
        if ($packetCount == 1) { //最后一个红包
            $val = $packetArr[0];
            //将该红包从redis里整个删除掉
            DI()->redis->hDel($redPacketsKey, $redPacketsHashKey);
            //将口令从redis里删除掉
            DI()->redis->hDel($redPacketsTitleKey, $redPacketsHashKey);
            $result['end'] = 1; //该红包被抢完
        } else { //红包超过2个
            $sub = array_rand($packetArr, 1); //取得数组的随机下标
            $val = $packetArr[$sub]; //获取数组下标对应的值
            //将redis取出的数组对应下标的值删除掉
            unset($packetArr[$sub]);
            //将数组重新排序
            $packetArr = array_merge($packetArr);
            //重新给redis赋值
            $redPacketsInfo = json_encode($packetArr); //数组重新打包
            //重新给redis赋值
            DI()->redis->hSet($redPacketsKey, $redPacketsHashKey, $redPacketsInfo);
            $result['end'] = 0;
        }

        //计算该用户抢得的直播券
        $liveCoin = $val * $live_coin_percent;
        //给用户加上该直播券
        (new Model_Auth_User)->update($uid, ['livecoin' => new NotORM_Literal("livecoin + {$liveCoin}")]);
        //向抢红包记录里添加一条数据
        DI()->notorm->users_rob_redpackets->insert([
            'uid' => $uid,
            'senduid' => $sendRedUid,
            'coin' => $val,
            'livecoin' => $liveCoin,
            'live_coin_percent' => $live_coin_percent,
            'type' => 1,
            'sendtime' => $sendtime,
            'title' => $title,
            'addtime' => time(),
            'live_type' => $liveType,
            'liveid' => $liveId,
        ]);

        $result['liveCoin'] = $liveCoin;

        return $result; //返回数据
    }

    //获取礼物组别列表
    public function giftGroupLists()
    {
        return DI()->notorm->gift_group
            ->select("group_name,group_val")
            ->order("orderno asc")
            ->fetchAll();
    }

    /*礼物PK*/
    public function giftPK($uid, $guestID, $effectiveTime, $masterGiftID, $guestGiftID)
    {
        //将PK信息写入数据库
        $data = [
            'uid' => $uid,
            'guest_id' => $guestID,
            'master_giftid' => $masterGiftID,
            'guest_giftid' => $guestGiftID,
            'effective_time' => $effectiveTime,
            'addtime' => time(),

        ];
        $rs = DI()->notorm->gift_pk->insert($data);

        $pkID = $rs['id'];
        //将之前的pk信息删掉
        DI()->redis->delete('giftPK_' . $uid);
        //算出最终的结束时间
        $lastStopTime = time() + $effectiveTime;

        //将信息存入redis里
        $data1 = [
            'pkID' => $pkID,
            'uid' => $uid,
            'guestID' => $guestID,
            'masterGiftID' => $masterGiftID,
            'masterGiftNum' => 0,
            'masterGiftTotalCoin' => 0,
            'guestGiftID' => $guestGiftID,
            'guestGiftNum' => 0,
            'guestGiftTotalCoin' => 0,
            'isEnd' => 0,
            'lastStopTime' => $lastStopTime //中途进房间的用户倒计时用
        ];

        DI()->redis->set('giftPK_' . $uid, json_encode($data1));

        if ($rs) {
            return $pkID;
        } else {
            return 0;
        }
    }

    /*PK结束*/
    public function stopPK($uid, $pkID)
    {

        $rs = [];
        //获取redis信息
        $giftPkInfo = DI()->redis->get("giftPK_" . $uid);

        //var_dump($giftPkInfo);

        if ($giftPkInfo) {
            $giftPkArr = json_decode($giftPkInfo, true);//将json转为数组
            $giftPkArr['isEnd'] = 1;//将数组的结束状态改为1

            //获取主队送礼物总价
            $masterGiftTotalCoin = $giftPkArr['masterGiftTotalCoin'];
            //获取客队送礼物总价
            $guestGiftTotalCoin = $giftPkArr['guestGiftTotalCoin'];

            $guestID = $giftPkArr['guestID'];

            $masterFirstUserCoin = 0;
            $guestFirstUserCoin = 0;


            //获取给主队送礼人的名字和总数
            $sql = "select uid,sum(coin) as total from cmf_giftpk_lists where(send_type=0 and pk_id=" . $pkID . ") group by uid order by total desc";
            //var_dump($sql);
            $info = DI()->notorm->giftpk_lists->query($sql);

            if ($info) {
                foreach ($info as $k => $v) {
                    if ($k == 0) {
                        $masterFirstUserCoin = $v['total'];
                        $masterFirstUserInfo = Model_Auth_User::getInstance()->getUserInfo($v['uid']);
                        break;
                    }
                }
            } else {
                $masterFirstUserCoin = "0";
            }


            //var_dump($masterFirstUserCoin);


            //获取给客队送礼人的名字和总数
            $sql = "select uid,sum(coin) as total from cmf_giftpk_lists where(send_type=1 and pk_id=" . $pkID . ") group by uid order by total desc";
            //var_dump($sql);
            $info = DI()->notorm->giftpk_lists->query($sql);
            //var_dump($info);
            if ($info) {
                foreach ($info as $k => $v) {
                    if ($k == 0) {
                        $guestFirstUserCoin = $v['total'];
                        $guestFirstUserInfo = Model_Auth_User::getInstance()->getUserInfo($v['uid']);
                        break;
                    }
                }
            } else {

                $guestFirstUserCoin = "0";
            }

            //var_dump($guestFirstUserCoin);


            //返回主队的信息
            $masterUserInfo = Model_Auth_User::getInstance()->getUserInfo($uid);
            $masterUserNicename = $masterUserInfo['user_nicename'];//主队昵称
            //返回给主队送礼最多人的昵称和总数($masterFirstUserCoin)
            if ($masterFirstUserInfo) {
                $masterFirstUserNicename = $masterFirstUserInfo['user_nicename'];//主队送礼最多人的昵称
            } else {
                $masterFirstUserNicename = "";
            }


            //返回客队的信息
            $guestUserInfo = Model_Auth_User::getInstance()->getUserInfo($guestID);
            $guestUserNicename = $guestUserInfo['user_nicename'];//客队昵称
            //返回给客队送礼最多人的昵称和总数($guestFirstUserCoin)

            if ($guestFirstUserInfo) {
                $guestFirstUserNicename = $guestFirstUserInfo['user_nicename'];//客队送礼最多人的昵称
            } else {
                $guestFirstUserNicename = "";
            }


            //判断哪队得胜
            if ($masterGiftTotalCoin > $guestGiftTotalCoin) {//主队胜

                $rs['winType'] = 1;


                //var_dump($masterFirstUserNicename);


            } else if ($masterGiftTotalCoin < $guestGiftTotalCoin) {//客队胜

                $rs['winType'] = 2;


                //var_dump($guestFirstUserNicename);


            } else {//平局

                $rs['winType'] = 0;

            }


            $rs['masterUserNicename'] = $masterUserNicename;
            $rs['masterFirstUserNicename'] = $masterFirstUserNicename;
            $rs['masterFirstUserCoin'] = strval($masterFirstUserCoin);

            $rs['guestUserNicename'] = $guestUserNicename;
            $rs['guestFirstUserNicename'] = $guestFirstUserNicename;
            $rs['guestFirstUserCoin'] = strval($guestFirstUserCoin);


            //重新将数组封装成json
            $giftPkInfo = json_encode($giftPkArr);
            DI()->redis->set("giftPK_" . $uid, $giftPkInfo);

            //将redis删除
            DI()->redis->delete("giftPK_" . $uid);

        }


        //修改对应pk发起记录的结束时间
        DI()->notorm->gift_pk->where("id=?", $pkID)->update(['endtime' => time()]);

        return $rs;

    }


    /*发起抢板凳活动*/
    public function createGrabBench($uid, $effectiveTime, $winNums)
    {
        //先将之前的抢板凳活动redis删除下
        DI()->redis->delete('grabbench_' . $uid);

        $now = time();

        //写入数据库记录
        $data1 = [
            'uid' => $uid,
            'effectivetime' => $effectiveTime,
            'win_nums' => $winNums,
            'addtime' => $now,
        ];


        $info = DI()->notorm->users_grabbench->insert($data1);

        $grabbenchID = $info['id'];


        //获取后台配置的用户点击时间间隔秒数
        $configpri = $this->getConfigPri();

        $hits_space = $configpri['grabbench_hits_space'];

        $effectiveTime = $now + $effectiveTime;//重新计算有效到期时间
        //将winNums以逗号分隔成数组
        $winNumArr = explode(',', $winNums);

        $data = [
            'grabbenchID' => $grabbenchID,
            'win_num' => $winNumArr,
            'num' => 0,
            'effectiveTime' => $effectiveTime,
            'isEnd' => 0,
        ];

        $rs = [];


        if ($info) {

            //存入redis中
            DI()->redis->set('grabbench_' . $uid, json_encode($data));


            $rs['grabbenchID'] = $grabbenchID;
            $rs['hits_space'] = $hits_space;

            return $rs;
        } else {
            return 0;
        }
    }


    /*抢板凳*/
    public function grabBench($uid, $liveuid, $grabbenchID)
    {

        $grabBenchInfo = DI()->redis->get('grabbench_' . $liveuid);

        if ($grabBenchInfo) {

            $grabBenchArr = json_decode($grabBenchInfo, true);//将json转为数组

            //判断该redis解析数组isEnd属性
            if ($grabBenchArr['isEnd'] == 1) {

                //将redis删除
                DI()->redis->delete('grabbench_' . $liveuid);
                return -1;
            } else {
                if (array_key_exists($uid, $grabBenchArr)) {//之前已经抢过

                    //获取该用户的信息
                    $userArr = $grabBenchArr[$uid];

                    if ($userArr['iswin'] == 1) {
                        return 0;//中奖了

                    } else {

                        $num = $grabBenchArr['num'] + 1;//得到该用户抢得的数字
                        $winNumArr = $grabBenchArr['win_num'];//主播设置的中奖号码数组

                        if (in_array($num, $winNumArr)) {//中奖了

                            $grabBenchArr[$uid]['iswin'] = 1;
                            $grabBenchArr[$uid]['num'][] = $num;

                            //向抢板凳中奖记录表里添加数据
                            $data1 = [
                                'uid' => $uid,
                                'grabbenchID' => $grabbenchID,
                                'num' => $num,
                                'addtime' => time(),
                            ];

                            DI()->notorm->grabbench_win_lists->insert($data1);


                        } else {//未中奖
                            $grabBenchArr[$uid]['iswin'] = 0;
                            $grabBenchArr[$uid]['num'][] = $num;
                        }

                        //向数据库中写入该用户抢得的记录

                        $getNumArr = $grabBenchArr[$uid]['num'];
                        $count = count($getNumArr);

                        $numStr = "";
                        for ($i = 0; $i < $count; $i++) {
                            $numStr = $numStr . $getNumArr[$i] . ",";
                        }

                        $numStr = substr($numStr, 0, strlen($numStr) - 1); //去掉最后一个字符

                        $data = [
                            'nums' => $numStr,
                            'addtime' => time(),
                        ];


                        DI()->notorm->grabbench_lists->where("grabbenchID=?", $grabbenchID)->update($data);

                        $grabBenchArr['num'] = $num;

                        //将数组重新解析成json后赋给redis
                        $grabBenchInfo = json_encode($grabBenchArr);

                        DI()->redis->set('grabbench_' . $liveuid, $grabBenchInfo);

                        return $num;

                    }


                } else {//之前没有抢过【第一次】

                    $num = $grabBenchArr['num'] + 1;//得到该用户抢得的数字
                    $winNumArr = $grabBenchArr['win_num'];//主播设置的中奖号码数组
                    if (in_array($num, $winNumArr)) {//中奖了

                        $grabBenchArr[$uid] = [
                            'num' => [$num],
                            'iswin' => 1,
                        ];


                        //向抢板凳中奖记录表里添加数据
                        $data1 = [
                            'uid' => $uid,
                            'grabbenchID' => $grabbenchID,
                            'num' => $num,
                            'addtime' => time(),
                        ];

                        DI()->notorm->grabbench_win_lists->insert($data1);


                    } else {

                        $grabBenchArr[$uid] = [
                            'num' => [$num],
                            'iswin' => 0,
                        ];
                    }

                    //向数据库中写入该用户抢得的记录

                    $data = [
                        'grabbenchID' => $grabbenchID,
                        'uid' => $uid,
                        'nums' => $num,
                        'addtime' => time(),
                    ];


                    DI()->notorm->grabbench_lists->insert($data);

                    $grabBenchArr['num'] = $num;

                    //将数组重新解析成json后赋给redis
                    $grabBenchInfo = json_encode($grabBenchArr);

                    DI()->redis->set('grabbench_' . $liveuid, $grabBenchInfo);

                    return $num;
                }
            }


        } else {//活动不存在

            return -1;
        }
    }

    /*倒计时结束，抢板凳活动结束*/

    public function stopGrabBench($uid, $grabbenchID)
    {

        //查询中奖号码
        $lists = DI()->notorm->grabbench_win_lists->where("grabbenchID=?", $grabbenchID)->order("num")->fetchAll();

        if ($lists) {
            foreach ($lists as $k => $v) {
                $userInfo = Model_Auth_User::getInstance()->getUserInfo($v['uid']);
                $lists[$k]['user_nicename'] = $userInfo['user_nicename'];
            }

            $grabBenchInfo = DI()->redis->get('grabbench_' . $uid);

            $grabBenchArr = json_decode($grabBenchInfo, true);//转为数组

            $grabBenchArr['isEnd'] = 1;

            $grabBenchInfo = json_encode($grabBenchArr);

            DI()->redis->set('grabbench_' . $liveuid, $grabBenchInfo);

            DI()->redis->delete('grabbench_' . $uid);//将redis删除

            return $lists;

        } else {

            DI()->redis->delete('grabbench_' . $uid);//将redis删除

            return -1;
        }


    }


    public function getCarouse()
    {
        $info = DI()->notorm->users_carouse
            ->where("id=1")
            ->fetchOne();


        if ($info) {
            $randNum = rand(1, $info['nums']);
            $info['randNum'] = strval($randNum);
            $info['imgUrl'] = Model_Helper_Func::getUrl($info['url']);
            unset($info['url']);
            unset($info['addtime']);
            return $info;
        } else {
            return -1;
        }
    }

    /**
     * 用户点击购买守护时，返回守护的信息和用户余额
     * @param $uid
     * @return array
     */
    public function getBuyGuard($uid)
    {
        $configPub = $this->getConfigPub();
        $guardCoin = $configPub['guard_coin'];
        $userInfo = DI()->notorm->users->where("id=?", $uid)->fetchOne();

        return [
            'guardCoin' => $guardCoin,
            'userCoin' => $userInfo['livecoin'],
        ];
    }

    public function buyGuard($uid, $touid, $liveType, $liveId)
    {
        if (!$uid || !$touid || !$liveId) {
            $this->setError('参数缺失');
            return false;
        }

        $configPub = $this->getConfigPub();
        $guardCoin = $configPub['guard_coin'];
        $userInfo = DI()->notorm->users->where("id=?", $uid)->fetchOne();
        $userCoin = $userInfo['livecoin'];
        if ($userCoin < $guardCoin) {
            $this->setError('余额不足');
            return false;
        }

        $info = DI()->notorm->users_guard_lists->where("uid=? and live_type=? and liveid=?", $uid, $liveType, $liveId)->fetchOne();
        $now = time();
        $add = 30 * 24 * 60 * 60;
        if ($info) {//有记录
            //判断到期日期是否大于当前日期
            if ($info['effectivetime'] > $now) {//未超期【只需要更改守护到期日期、等级到期日期和最大等级即可】
                $effectivetime = $info['effectivetime'] + $add;
                $largest_level = $info['largest_level'] + 1;
                if ($largest_level >= 5) {
                    $largest_level = 5;
                }

                $data = [
                    'effectivetime' => $effectivetime,
                    'addtime' => $now,
                    'largest_level' => $largest_level,
                ];
            } else {//超期【需要降低用户的守护等级，同时更新用户的守护日期和等级到期日期】
                //判断超期时长
                $overTime = $now - $info['effectivetime'];
                $num = ceil($overTime / 30);
                $level = $info['guard_level'] - $num;
                if ($level <= 1) {
                    $level = 1;
                }

                $effectivetime = $now + $add;
                $level_endtime = $now + $add;

                $data = [
                    'guard_level' => $level,
                    'effectivetime' => $effectivetime,
                    'addtime' => $now,
                    'level_endtime' => $level_endtime,
                    'largest_level' => $level,
                ];
            }
            //更新用户信息
            $rs = DI()->notorm->users_guard_lists->where("uid=? and live_type=? and liveid=?", $uid, $liveType, $liveId)->update($data);
        } else {//没有记录
            $effectivetime = $now + $add;
            $data = [
                'uid' => $uid,
                'addtime' => $now,
                'guard_level' => 1,
                'effectivetime' => $effectivetime,
                'level_endtime' => $effectivetime,
                'largest_level' => 1,
                'live_type' => $liveType,
                'liveid' => $liveId,
            ];
            //向守护记录表中添加数据
            $rs = DI()->notorm->users_guard_lists->insert($data);
        }

        if (!$rs) {
            $this->setError('购买守护失败');
            return false;
        }

        //向消费记录表中添加一条数据
        $data = [
            'type' => 'expend',
            'action' => 'buyguard',
            'uid' => $uid,
            'giftid' => 0,
            'giftcount' => 1,
            'giftgroup_num' => 1,
            'totalcoin' => $guardCoin,
            'total_livecoin' => 0,
            'showid' => 0,
            'addtime' => time(),
            'live_type' => $liveType,
            'liveid' => $liveId,
        ];

        DI()->notorm->users_coinrecord->insert($data);

        (new Model_Auth_User)->update($uid, [
            'livecoin' => new NotORM_Literal("livecoin - {$guardCoin}"),
            'consumption' => new NotORM_Literal("consumption + {$guardCoin}"),
        ]);

        //给主播加映票
        (new Model_Auth_User())->update($touid, [
            'votes' => new NotORM_Literal("votes + {$guardCoin}"),
            'votestotal' => new NotORM_Literal("votestotal + {$guardCoin}"),
        ]);

        //查询守护记录
        $guardInfo = DI()->notorm->users_guard_lists
            ->where("uid=? and live_type=? and liveid=? and effectivetime>?", $uid, $liveType, $liveId, time())->fetchOne();

        $rs = [];
        $rs['guardInfo'] = $guardInfo;
        $rs['livecoin'] = $userCoin;

        return $rs;
    }

    public function checkisSuper($uid)
    {
        $rs = DI()->notorm->users
            ->where("id=?", $uid)->fetchOne();

        if ($rs['issuper'] == 1) {
            $isSuper = 1;
        } else {
            $isSuper = 0;
        }

        return $isSuper;
    }

    public function getVestLists($forceUpdate = false)
    {
        $key = self::KEY_CACHE_USERS_VEST;
        $redis = Model_Helper_Redis::getInstance();
        $list = $redis->get($key);
        if ($list && !$forceUpdate) {
            return json_decode($list, true);
        }

        $list = DI()->notorm->users_vest->fetchAll() ?: [];
        foreach ($list as &$v) {
            $v['vest_man_url'] = Model_Helper_Func::getUrl($v['vest_man_url']);
            $v['vest_woman_url'] = Model_Helper_Func::getUrl($v['vest_woman_url']);
        }
        if ($list) {
            $redis->set($key, json_encode($list), 3600);
        }

        return $list;
    }

    public function getUserVest($uid, $liveType, $liveId, $forceUpdate = false)
    {
        $key = $this->getUserVestKey($uid, $liveType, $liveId);
        $redis = Model_Helper_Redis::getInstance();
        $value = $redis->get($key);
        if ($value && !$forceUpdate) {
            return json_decode($value, true);
        }

        $value = DI()->notorm->users_vest_lists
            ->where('uid=? and live_type=? and liveid=?', $uid, $liveType, $liveId)
            ->fetchOne();
        if ($value) {
            $redis->set($key, json_encode($value));
        }

        return $value;
    }

    private function getUserVestKey($uid, $liveType, $liveId)
    {
        return self::KEY_CACHE_UVL . "{$uid}:{$liveType}:{$liveId}";
    }

    public function updateUserVest($uid, $liveType, $liveId, $data)
    {
        $rs = DI()->notorm->users_vest_lists
            ->where('uid=? and live_type=? and liveid=?', $uid, $liveType, $liveId)
            ->update($data);
        if ($rs) {
            $this->getUserVest($uid, $liveType, $liveId, true);
        }

        return $rs;
    }

    public function changeVest($uid, $touid, $vestid, $liveType, $liveId)
    {
        if (Model_Auth_User::getInstance()->isVisitor($touid)) {
            $this->setError('游客不能修改马甲');
            return false;
        }

        $insert = [
            'uid' => $touid,
            'live_type' => $liveType,
            'liveid' => $liveId,
            'vestid' => $vestid,
            'addtime' => time(),
        ];

        //判断当前用户的身份
        $userInfo = DI()->notorm->users->where("id=?", $uid)->fetchOne();
        $touserInfo = DI()->notorm->users->where("id=?", $touid)->fetchOne();

        if ($userInfo['issuper'] == 1) {//当前用户是超管
            //判断对方的身份
            if ($touserInfo['issuper'] == 1) {//对方是超管
                $this->setError('对方是超管，无法设置马甲');
                return false;
            }

            //判断对方是不是主播
            $touserIdentity = Model_Auth_User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
            if ($touserIdentity == 50) {
                $this->setError('对方是主播，无法设置马甲');
                return false;
            }

            //判断对方的马甲ID
            if ($touserInfo['vest_id'] == 6 || $touserInfo['vest_id'] == 7) {//黑马或紫马
                $this->setError('对方是黑马或紫马，无法设置');
                return false;
            } else {
                //从房间马甲列表查询
                $vestInfo = $this->getUserVest($touid, $liveType, $liveId);
                if ($vestInfo) {
                    if ($vestid == $vestInfo['vestid']) {//马甲相同
                        $this->setError('马甲相同，无法设置');
                        return false;
                    } else {
                        $data = [
                            'vestid' => $vestid,
                            'addtime' => time(),
                        ];
                        $rs = $this->updateUserVest($touid, $liveType, $liveId, $data);
                        if ($rs === false) {
                            $this->setError('马甲设置失败');
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    $rs = DI()->notorm->users_vest_lists->insert($insert);
                    if ($rs === false) {
                        $this->setError('马甲设置失败');
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } else {
            $userIdentity = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
            if ($userIdentity == 50) {    //当前用户是主播
                if ($touserInfo['issuper'] == 1) {//对方是超管
                    $this->setError('对方是超管，无法设置马甲');
                    return false;
                }

                //判断对方的马甲ID
                if ($touserInfo['vest_id'] == 6 || $touserInfo['vest_id'] == 7) {//黑马或紫马
                    $this->setError('对方是黑马或紫马，无法设置');
                    return false;
                } else {
                    //从房间马甲列表查询
                    $vestInfo = $this->getUserVest($touid, $liveType, $liveId);
                    if ($vestInfo) {
                        if ($vestid == $vestInfo['vestid']) {//马甲相同
                            $this->setError('马甲相同，无法设置');
                            return false;
                        } else {
                            $data = [
                                'vestid' => $vestid,
                                'addtime' => time(),
                            ];
                            $rs = $this->updateUserVest($touid, $liveType, $liveId, $data);
                            if ($rs === false) {
                                $this->setError('马甲设置失败');
                                return false;
                            } else {
                                return true;
                            }
                        }
                    } else {
                        $rs = DI()->notorm->users_vest_lists->insert($insert);
                        if ($rs === false) {
                            $this->setError('马甲设置失败');
                            return false;
                        } else {
                            return true;
                        }
                    }
                }
            } else {//当前用户是普通马甲用户
                if ($touserInfo['issuper'] == 1) {//对方是超管
                    $this->setError('对方是超管，无法设置马甲');
                    return false;
                }

                if ($touserInfo['vest_id'] == 6 || $touserInfo['vest_id'] == 7) {//对方是紫马或黑马
                    $this->setError('对方是黑马或紫马，无法设置');
                    return false;
                }

                //判断当前用户的身份
                if ($userInfo['vest_id'] == 6 || $userInfo['vest_id'] == 7) {//紫马或黑马
                    if ($vestid <= 5) {//设置的马甲是橙马以下
                        $vestInfo = $this->getUserVest($touid, $liveType, $liveId);
                        if ($vestInfo) {
                            $data = [
                                'vestid' => $vestid,
                                'addtime' => time(),
                            ];
                            $rs = $this->updateUserVest($touid, $liveType, $liveId, $data);
                            if ($rs === false) {
                                $this->setError('马甲设置失败');
                                return false;
                            } else {
                                return true;
                            }
                        } else {
                            $rs = DI()->notorm->users_vest_lists->insert($insert);
                            if ($rs === false) {
                                $this->setError('马甲设置失败');
                                return false;
                            } else {
                                return true;
                            }
                        }
                    } else {
                        $this->setError('没有设置权限');
                        return false;
                    }
                } else {
                    //从房间马甲列表里读取当前用户的马甲id
                    $userVestInfo = $this->getUserVest($uid, $liveType, $liveId);
                    if ($userVestInfo['vestid'] > $vestid) {
                        $vestInfo = $this->getUserVest($touid, $liveType, $liveId);
                        if ($vestInfo) {
                            $data = [
                                'vestid' => $vestid,
                                'addtime' => time(),
                            ];
                            $rs = $this->updateUserVest($touid, $liveType, $liveId, $data);
                            if ($rs === false) {
                                $this->setError('马甲设置失败');
                                return false;
                            } else {
                                return true;
                            }
                        } else {
                            $rs = DI()->notorm->users_vest_lists->insert($insert);
                            if ($rs === false) {
                                $this->setError('马甲设置失败');
                                return false;
                            } else {
                                return true;
                            }
                        }
                    } else {
                        $this->setError('没有设置权限');
                        return false;
                    }
                }
            }
        }
    }

    /**
     * 频道禁言
     * @param $liveType
     * @param $liveId
     * @param $userId
     * @return array|bool
     */
    public function channelGag($liveType, $liveId, $userId)
    {
        if (!$userId) {
            $this->setError('请先登录');
            return false;
        }

        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        $key = $this->getChannelShutUpKey($liveType, $liveId);
        $redis = Model_Helper_Redis::getInstance();
        if ($redis->get($key)) {
            $this->setError('该房间已被禁言');
            return false;
        }

        //判断当前用户是否具备频道禁言权限
        $userInfo = DI()->notorm->users->where("id=?", $userId)->fetchOne();
        $userIdentity = Model_Auth_User::getInstance()->getUserIdentity($userId, $liveId, $liveType);
        if ($userIdentity == 60) {//超管
            $redis->set($key, $userId);
            return "{$userInfo['user_nicename']}(超管)";
        } elseif (in_array($userIdentity, [40, 50])) {//主播
            $redis->set($key, $userId);
            return "{$userInfo['user_nicename']}(主播)";
        } else {//普通用户
            if ($userInfo['vest_id'] == 6 || $userInfo['vest_id'] == 7) {//用户在后台被设置了紫马或黑马
                $vestInfo = $this->getUsersVest($userInfo['vest_id']);
                if ($vestInfo['gap_all'] == 1) {
                    $redis->set($key, $userId);
                    return "{$userInfo['user_nicename']}({$vestInfo['vest_name']})";
                }
            } else {
                //判断用户的房间马甲权限
                $vestListInfo = $this->getUserVest($userId, $liveType, $liveId);
                if ($vestListInfo) {
                    $vestInfo = $this->getUsersVest($vestListInfo['vestid']);
                    if ($vestInfo['gap_all'] == 1) {
                        $redis->set($key, $userId);
                        return "{$userInfo['user_nicename']}({$vestInfo['vest_name']})";
                    }
                }
            }
        }

        $this->setError('您没有房间禁言权限');
        return false;
    }

    /**
     * 删除禁言
     * @param $userId
     * @param $liveType
     * @param $liveId
     * @return bool|array
     */
    public function delChannelGap($userId, $liveType, $liveId)
    {
        if (!$userId) {
            $this->setError('请先登录');
            return false;
        }

        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        $channelShutUpUid = $this->isChannelShutUp($liveType, $liveId);
        if (!$channelShutUpUid) {
            $this->setError('该房间未被禁言');
            return false;
        }

        //判断当前用户是否是超管
        $userInfo = DI()->notorm->users->where("id=?", $userId)->fetchOne();
        $userIsSuper = $userInfo['issuper'];
        //判断禁言房间用户是否是超管
        $channelShutUpUser = DI()->notorm->users->where("id=?", $channelShutUpUid)->fetchOne();
        $key = $this->getChannelShutUpKey($liveType, $liveId);
        $redis = Model_Helper_Redis::getInstance();
        $userIdentity = Model_Auth_User::getInstance()->getUserIdentity($userId, $liveId, $liveType);
        $channelShutUpUserIdentity = Model_Auth_User::getInstance()->getUserIdentity($channelShutUpUid, $liveId, $liveType);
        if ($channelShutUpUser['issuper'] == 1) {//超管禁言
            if ($userIsSuper == 1) {//是超管
                $redis->del($key);
                return "{$userInfo['user_nicename']}(超管)";
            }
        } else {
            if (in_array($channelShutUpUserIdentity, [40, 50])) {//是主播禁言
                //判断当前用户是否是超管
                if ($userIdentity == 60) {
                    $redis->del($key);
                    return $userInfo['user_nicename'];
                } elseif (in_array($userIdentity, [40, 50])) {//判断当前用户是否是主播
                    $redis->del($key);
                    return "{$userInfo['user_nicename']}(主播)";
                }
            } else {//马甲用户禁言
                //判断当前用户是否是超管
                if ($userIsSuper == 1) {
                    $redis->del($key);
                    return $userInfo['user_nicename'];
                } elseif (in_array($userIdentity, [40, 50])) {//判断当前用户是否是主播
                    $redis->del($key);
                    return "{$userInfo['user_nicename']}(主播)";
                } else {
                    //获取当前用户的马甲
                    //判断当前用户的马甲等级
                    if ($userInfo['vest_id'] > 1) {
                        $currentUserVestID = $userInfo['vest_id'];
                    } else {
                        $vestListUserInfo = $this->getUserVest($userId, $liveType, $liveId);
                        if ($vestListUserInfo) {
                            $currentUserVestID = $vestListUserInfo['vestid'];
                        } else {
                            $currentUserVestID = 1;//默认白马甲
                        }
                    }

                    //判断禁言用户的马甲ID
                    if ($channelShutUpUser['vest_id'] > 1) {
                        $gagUserVestID = $channelShutUpUser['vest_id'];
                    } else {
                        //从房间马甲列表里读取
                        $gagListUserInfo = $this->getUserVest($channelShutUpUid, $liveType, $liveId);
                        if ($gagListUserInfo) {
                            $gagUserVestID = $gagListUserInfo['vestid'];
                        } else {
                            $gagUserVestID = 1;//默认白马甲
                        }
                    }

                    //判断当前用户的马甲权限级别是否比禁言用户的马甲权限等级高
                    if ($currentUserVestID > $gagUserVestID) {
                        //判断当前用户马甲等级是否有频道禁言权限
                        $vestInfo = $this->getUsersVest($currentUserVestID);
                        if ($vestInfo['gap_all'] == 1) {//当前用户马甲有频道禁言权限
                            $redis->del($key);
                            return "{$userInfo['user_nicename']}({$vestInfo['vest_name']})";
                        }
                    } else if ($currentUserVestID == $gagUserVestID) {//马甲等级相同
                        if ($userId == $channelShutUpUid) {//当前用户是频道禁言用户
                            //判断当前用户马甲等级是否有频道禁言权限
                            $vestInfo = $this->getUsersVest($currentUserVestID);
                            if ($vestInfo['gap_all'] == 1) {//当前用户马甲有频道禁言权限
                                $redis->del($key);
                                return "{$userInfo['user_nicename']}({$vestInfo['vest_name']})";
                            }
                        }
                    }
                }
            }
        }

        $this->setError('您的等级不够,无法解除禁言');
        return false;
    }

    /**
     * 判断频道是否禁言
     * @param $liveType
     * @param $liveId
     * @return bool|string
     */
    public function isChannelShutUp($liveType, $liveId)
    {
        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }
        return (int)Model_Helper_Redis::getInstance()->get($this->getChannelShutUpKey($liveType, $liveId));
    }

    private function getChannelShutUpKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_CHANNEL_GAG . "{$liveType}:{$liveId}";
    }

    /**
     * 获取直播间设置的发言字数和频率
     * @param $liveid
     * @return array
     */
    public function getLiveChatMsg($liveid)
    {
        $arr = [];
        $info = DI()->notorm->users_live->where('uid=? and islive=?', $liveid, 1)->fetchOne();
        if ($info) {
            $arr['chat_num'] = $info['chat_num'];
            $arr['chat_frequency'] = $info['chat_frequency'];
        } else {
            $arr['chat_num'] = "";
            $arr['chat_frequency'] = "";
        }

        return $arr;
    }

    /**
     * 私聊权限
     */
    public function getPrivateChatPermission($uid, $touid, $liveType, $liveId)
    {
        if (!$uid || !$touid || !$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('缺参数');
            return false;
        }

        if ($uid == $touid) {
            $this->setError('你没有私信权限');
            return false;
        }

        //user vest_id
        $uidVestId = $this->getUserVestId($uid, $liveType, $liveId);
        $touidVestId = $this->getUserVestId($touid, $liveType, $liveId);
        //user vest info
        $userVest = $this->getUsersVest($uidVestId);
        $touserVest = $this->getUsersVest($touidVestId);
        if ($userVest && $userVest['private_chat'] != 1) {
            $this->setError('你没有私信权限');
            return false;
        }
        if ($touserVest && $touserVest['private_chat'] != 1) {
            $this->setError('对方没有私信权限');
            return false;
        }

        if ($uidVestId <= 0) {
            if ($touidVestId <= 1) {
                $this->setError('没有私信权限');
                return false;
            }
        } elseif ($uidVestId == 1) {
            if ($touidVestId <= 1) {
                $this->setError('没有私信权限');
                return false;
            }
        }

        return true;
    }

    public function getUsersVest($id, $forceUpdate = false)
    {
        if (!$id) {
            return false;
        }

        $redis = Model_Helper_Redis::getInstance();
        $key = $this->getUsersVestKey($id);
        $value = $redis->get($key);
        if ($value && !$forceUpdate) {
            return json_decode($value, true);
        }

        $value = DI()->notorm->users_vest->where("id=?", $id)->fetchOne();
        if ($value) {
            $redis->set($key, json_encode($value), 24 * 3600);
        }

        return $value;
    }

    private function getUsersVestKey($id)
    {
        return "cache:uv:{$id}";
    }

    /**
     * 获取用户vest_id
     */
    public function getUserVestId($uid, $liveType, $liveId)
    {
        if ($liveId) {
            $userVestList = $this->getUserVest($uid, $liveType, $liveId);
            if ($userVestList) {
                return (int)$userVestList['vestid'];
            }
        }

        $user = Model_Auth_User::getInstance()->getUserInfo($uid);
        if (!$user) {
            return 0;
        }

        return (int)$user['vest_id'];
    }

    public function getLives($uid)
    {
        //直播中
        $count1 = DI()->notorm->users_live->where('uid=? and islive="1"', $uid)->count();
        //回放
        $count2 = DI()->notorm->users_liverecord->where('uid=? ', $uid)->count();
        return $count1 + $count2;
    }

}
