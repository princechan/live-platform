<?php

class Model_Charge extends Model_Common
{
    /**
     * 生成订单号
     * @param $uid
     * @return string
     */
    public function genOrderId($uid)
    {
        return $uid . '_' . date('YmdHis') . rand(100, 999);
    }

    /**
     * 创建订单
     * @param $changeid
     * @param $orderinfo
     * @return int
     */
    public function createOrder($changeid, $orderinfo)
    {
        $charge = DI()->notorm->charge_rules->select('*')->where('id=?', $changeid)->fetchOne();
        if (!$charge || $charge['coin'] != $orderinfo['coin'] ||
            ($charge['money'] != $orderinfo['money'] && $charge['money'] != $orderinfo['money_ios'])
        ) {
            return 1003;
        }

        $orderinfo['coin_give'] = $charge['give'];

        return DI()->notorm->users_charge->insert($orderinfo);
    }

}
