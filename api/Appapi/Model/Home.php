<?php

class Model_Home extends Model_Common
{
    /**
     * 轮播主播
     * @return array
     */
    public function getSlideLive()
    {
        $key = Model_Live::KEY_SLIDE_LIVE_LIST;
        $list = Model_Helper_Redis::getInstance()->get($key);
        $list = json_decode($list, true);
        if ($list) {
            return $list;
        }

        $list = DI()->notorm->home_recommend->where('show_id>?', 0)->fetchAll() ?: [];
        if (!$list) {
            return [];
        }

        $singleLiveIds = [];
        $multiLiveIds = [];
        foreach ($list as $l) {
            switch ($l['show_type']) {
                case 0:
                    $singleLiveIds[] = (int)$l['show_id'];
                    break;
                case 1:
                    $multiLiveIds[] = (int)$l['show_id'];
                    break;
            }
        }

        $singleLiveIdStr = implode($singleLiveIds, ',');
        $multiLiveIdStr = implode($multiLiveIds, ',');
        $singleLiveList = $singleLiveIds ? DI()->notorm->users_live->where("uid in ({$singleLiveIdStr}) and islive=?", 1)->fetchAll() : [];
        $singleLiveList = Model_Helper_Func::index($singleLiveList, 'uid');
        $multiLiveList = $multiLiveIds ? DI()->notorm->muti_show->where("id in ({$multiLiveIdStr}) and status in (1,2)")->fetchAll() : [];
        $multiLiveList = Model_Helper_Func::index($multiLiveList, 'id');

        foreach ($list as &$li) {
            //type：0-直播，1-默认图片
            //直播封面图
            if ($li['show_cover']) {
                $li['show_cover'] = Model_Helper_Func::getUrl($li['show_cover']);
            }
            //默认封面图图片
            if ($li['default_cover']) {
                $li['default_cover'] = Model_Helper_Func::getUrl($li['default_cover']);
            }
            //single:islive(0-关闭,1-开启), multi:status(0-关闭,1-开启，2-休息)
            $li['status'] = 0;
            $li['video_url'] = '';
            //show_type: 0-单直播 1-多直播
            if ($li['show_type'] == 0) {
                if (empty($singleLiveList[$li['show_id']])) {
                    continue;
                }
                $live = $singleLiveList[$li['show_id']];
                $li['status'] = $live['islive'];
                //stream
                $li['video_url'] = Model_Common::getM3u8Stream($live['stream']);
            } elseif ($li['show_type'] == 1) {
                if (empty($multiLiveList[$li['show_id']])) {
                    continue;
                }
                $live = $multiLiveList[$li['show_id']];
                $li['status'] = $live['status'];
            } else {
                continue;
            }
        }

        if ($list) {
            Model_Helper_Redis::getInstance()->set($key, json_encode($list), 5 * 60);
        }

        return $list;
    }

    /**
     * 轮播V1.2
     * @return array
     */
    public function slideLiveV2($uid)
    {
        $key = Model_Live::KEY_SLIDE_LIVE_V2;
        $list = json_decode(Model_Helper_Redis::getInstance()->get($key), true);
        if ($list) {
            return $list;
        }

        $recommend = DI()->notorm->home_recommend->where('show_id>?', 0)->fetchAll() ?: [];
        if (!$recommend) {
            return [];
        }

        $singleLiveIds = [];
        $multiLiveIds = [];
        foreach ($recommend as $l) {
            switch ($l['show_type']) {
                case 0:
                    $singleLiveIds[] = (int)$l['show_id'];
                    break;
                case 1:
                    $multiLiveIds[] = (int)$l['show_id'];
                    break;
            }
        }

        $singleLiveIdStr = implode($singleLiveIds, ',');
        $multiLiveIdStr = implode($multiLiveIds, ',');
        //single live
        $singleLiveList = $singleLiveIds ? DI()->notorm->users_live->where("uid in ({$singleLiveIdStr}) and islive=?", 1)->fetchAll() : [];
        foreach ($singleLiveList as &$sl) {
            $sl['nums'] = Model_Live::getInstance()->getRoomLivedNum(0, $sl['uid']);
        }
        $singleLiveList = Model_Helper_Func::index($singleLiveList, 'uid');

        //multi live
        $multiLiveList = $multiLiveIds ? DI()->notorm->muti_show->where("id in ({$multiLiveIdStr}) and status in (1,2)")->fetchAll() : [];
        foreach ($multiLiveList as &$ml) {
            $ml['nums'] = Model_Live::getInstance()->getRoomLivedNum(1, $ml['id']);
            $ml = array_merge($ml, Model_Live::getInstance()->getMultiShowExtData($ml['id']));
        }
        $multiLiveList = Model_Helper_Func::index($multiLiveList, 'id');

        $list = [];
        foreach ($recommend as $li) {
            //show_type: 0-单直播 1-多直播
            if ($li['show_type'] == 0) {
                if (empty($singleLiveList[$li['show_id']])) {
                    continue;
                }
                $live = $singleLiveList[$li['show_id']];
            } elseif ($li['show_type'] == 1) {
                if (empty($multiLiveList[$li['show_id']])) {
                    continue;
                }
                $live = $multiLiveList[$li['show_id']];
            } else {
                continue;
            }

            $value = $this->unifiedLiveData($live, $uid);

            //type：0-直播，1-默认图片
            $value['type'] = $li['type'];
            //直播封面图
            if ($li['show_cover']) {
                $value['show_cover'] = Model_Helper_Func::getUrl($li['show_cover']);
            }
            //默认封面图图片
            if ($li['default_cover']) {
                $value['default_cover'] = Model_Helper_Func::getUrl($li['default_cover']);
            }

            $list[] = $value;
        }

        if ($list) {
            Model_Helper_Redis::getInstance()->set($key, json_encode($list), 5 * 60);
        }

        return $list;
    }

    /**
     * 首页热门直播
     * @param $uid
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getLiveList($uid, $page = 1, $limit = 10)
    {
        //TODO uuid做一下缓存，数据跟着设备走
        $multiList = $this->getMultiLiveList() ?: [];
        if ($page == 1) {
            $singleList = $this->getSingleLiveList(0, $limit - count($multiList));
            $data = array_merge($multiList, $singleList);
        } else {
            $data = $this->getSingleLiveList(($page - 1) * $limit - count($multiList), $limit);
        }

        $list = [];
        foreach ($data as $value) {
            $list[] = $this->unifiedLiveData($value, $uid);
        }

        return $list;
    }

    /**
     * 统一直播数据结构
     * @param array $value
     * @param string $uid
     * @return array
     */
    public function unifiedLiveData(array $value, $uid = '')
    {
        if (!$value) {
            return [];
        }

        if (isset($value['islive'])) {
            $data = [
                'uid' => $value['uid'],
                'avatar' => Model_Helper_Func::getUrl($value['avatar']),
                'avatar_thumb' => Model_Helper_Func::getUrl($value['avatar_thumb']),
                'user_nicename' => $value['user_nicename'],
                'title' => $value['title'],
                'city' => $value['city'],
                'stream' => $value['stream'],
                'pull' => Model_Common::getM3u8Stream($value['stream']),
                'thumb' => Model_Helper_Func::getUrl($value['thumb']),
                'nums' => (string)$value['nums'],
                'identity' => Model_Auth_User::getInstance()->getUserIdentity($uid, $value['uid']),//用户身份
                'status' => ($value['islive'] == 0 ? '2' : ($value['islive'] == 1 ? '1' : '0')), //0-关闭,1-开启，2-休息
                'lock' => intval($value['type'] == 1),
                'live_type' => 0,
                'live_id' => $value['uid'],
                'ispc' => $value['ispc'],
                'chat_num' => $value['chat_num'],
                'chat_frequency' => $value['chat_frequency'],
                'room_type' => $value['type'], //房间类型 0-普通房间，1-密码房间，2-付费房间
                'room_type_val' => $value['type'] == 1 ? md5($value['type_val']) : $value['type_val'],//对应的值
            ];
        } else {
            $data = [
                'uid' => 0,
                'avatar' => Model_Helper_Func::getUrl($value['avatar']),
                'avatar_thumb' => Model_Helper_Func::getUrl($value['avatar_thumb']),
                'user_nicename' => $value['live_name'],
                'title' => '',
                'city' => '',
                'stream' => '',
                'pull' => '',
                'thumb' => Model_Helper_Func::getUrl($value['focus']),
                'nums' => (string)$value['nums'],
                'status' => $value['status'],
                'lock' => intval($value['room_type'] == 1),
                'identity' => Model_Auth_User::getInstance()->getUserIdentity($uid, $value['id'], 1),
                'live_type' => 1,
                'live_id' => $value['id'],
                'ispc' => 0, //$value['ispc'], 多直播间暂时没有该属性
                'chat_num' => $value['speak_num'],
                'chat_frequency' => $value['speak_frequency'],
                'room_type' => $value['room_type'], //房间类型 0-普通房间，1-密码房间，2-付费房间
                'room_type_val' => in_array($value['room_type'], [1, 2]) ? ($value['room_type'] == 1 ? md5($value['room_password']) : $value['room_live_coin']) : '',
            ];
        }

        if (isset($value['ami_anchor'])) {
            $data['ami_anchor'] = $value['ami_anchor'];
        }
        if (isset($value['ami_manager'])) {
            $data['ami_manager'] = $value['ami_manager'];
        }

        return $data;
    }

    /**
     * 直播中状态在前，休息中状态在后
     * @param int $limit
     * @return array
     */
    public function getMultiLiveList($limit = 8)
    {
        $list = DI()->notorm->muti_show
            ->where('status IN (1,2)')
            ->order('status ASC, updated_at DESC')
            ->limit($limit)
            ->fetchAll() ?: [];
        foreach ($list as &$li) {
            $li['nums'] = Model_Live::getInstance()->getRoomLivedNum(1, $li['id']);
            $li = array_merge($li, Model_Live::getInstance()->getMultiShowExtData($li['id']));
        }

        return $list;
    }

    public function getSingleLiveList($offset = 0, $limit = 10)
    {
        $list = DI()->notorm->users_live
            ->order('islive DESC, starttime DESC')
            ->limit($offset, $limit)
            ->fetchAll() ?: [];
        foreach ($list as &$li) {
            $li['nums'] = Model_Live::getInstance()->getRoomLivedNum(0, $li['uid']);
        }

        return $list;
    }

    /**
     * 搜索房间
     * @param $key
     * @param string $uid
     * @return array|bool
     */
    public function searchRoom($key, $uid = '')
    {
        if (!$key) {
            $this->setError('参数缺失');
            return false;
        }

        $singleLive = DI()->notorm->users_live
            ->where('(uid=? or user_nicename=?) and uid!=?', $key, $key, $uid)->fetchOne() ?: [];
        if ($singleLive) {
            $singleLive['nums'] = Model_Live::getInstance()->getRoomLivedNum(0, $singleLive['uid']);
        }

        $multiLive = DI()->notorm->muti_show
            ->where('(id=? or live_name=?)', $key, $key)->fetchOne() ?: [];
        if ($multiLive) {
            $multiLive['nums'] = Model_Live::getInstance()->getRoomLivedNum(1, $multiLive['id']);
            $multiLive = array_merge($multiLive, Model_Live::getInstance()->getMultiShowExtData($multiLive['id']));
        }

        $list = [];
        if ($singleLive) {
            $list[] = $this->unifiedLiveData($singleLive, $uid);
        }
        if ($multiLive) {
            $list[] = $this->unifiedLiveData($multiLive, $uid);
        }

        return $list;
    }

    /**
     * 关注列表
     * @param $uid
     * @param $p
     * @return array
     */
    public function getFollow($uid, $p)
    {
        $limit = 50;
        $start = ($p - 1) * $limit;

        $attentions = DI()->notorm->users_attention->where('uid=?', $uid)
            ->order('id DESC')->limit($start, $limit)->fetchAll() ?: [];

        $singleLiveIds = [];
        $multiLives = [];
        foreach ($attentions as $att) {
            if ($att['live_type'] == 0) {
                $singleLiveIds[] = $att['touid'];
            } else {
                $multiLives[] = Model_Live::getInstance()->getMultiShowInfo($att['touid']);
            }
        }

        $singleLives = $singleLiveIds ? DI()->notorm->users_live
            ->where(['uid' => $singleLiveIds])->fetchAll() ?: [] : [];
        $singleLives = Model_Helper_Func::index($singleLives, 'uid');
        $multiLives = Model_Helper_Func::index($multiLives, 'id');

        $list = [];
        foreach ($attentions as $att) {
            if ($att['live_type'] == 0) {
                $value = isset($singleLives[$att['touid']]) ? $singleLives[$att['touid']] : null;
                if (!$value) {
                    continue;
                }

                $value['pull'] = $this->PrivateKeyA('rtmp', $value['stream'], 0);
                $value['nums'] = Model_Live::getInstance()->getRoomLivedNum(0, $value['uid']);
            } else {
                $value = isset($multiLives[$att['touid']]) ? $multiLives[$att['touid']] : null;
                if (!$value) {
                    continue;
                }

                $value['nums'] = Model_Live::getInstance()->getRoomLivedNum(1, $value['id']);
            }

            if (!$value['thumb']) {
                $value['thumb'] = $value['avatar'];
            }

            $list[] = $this->unifiedLiveData($value, $uid);
        }

        return $list;
    }

    //最新
    public function getNew($lng, $lat, $p)
    {
        $limit = 50;
        $start = ($p - 1) * $limit;
        $where = " islive='1' ";

        if ($p != 1) {
            $endtime = $_SESSION['new_starttime'];
            $where .= " and starttime < {$endtime}";
        }

        $result = DI()->notorm->users_live
            ->select("uid,avatar,avatar_thumb,user_nicename,title,city,stream,lng,lat,pull,thumb")
            ->where($where)
            ->order("starttime desc")
            ->limit($start, $limit)
            ->fetchAll();
        foreach ($result as $k => $v) {
            $result[$k]['nums'] = (string)Model_Live::getInstance()->getOnlineUserNum(0, $v['uid']);
            if (!$v['thumb']) {
                $result[$k]['thumb'] = $v['avatar'];
            }
            $result[$k]['pull'] = $this->PrivateKeyA('rtmp', $v['stream'], 0);

            $distance = '好像在火星';
            if ($lng && $lat && $v['lat'] && $v['lng']) {
                $distance = $this->getDistance($lat, $lng, $v['lat'], $v['lng']);
            } else if ($v['city']) {
                $distance = $v['city'];
            }

            $result[$k]['distance'] = $distance;
            unset($result[$k]['lng']);
            unset($result[$k]['lat']);

        }
        if ($result) {
            $last = array_slice($result, count($result) - 2, count($result) - 1);
            $_SESSION['new_starttime'] = $last['starttime'];
        }

        return $result;
    }

    /**
     * @desc 根据两点间的经纬度计算距离
     */
    private function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6371000; //近似地球半径 单位 米
        /*
          Convert these degrees to radians
          to work with the formula
        */

        $lat1 = ($lat1 * pi()) / 180;
        $lng1 = ($lng1 * pi()) / 180;

        $lat2 = ($lat2 * pi()) / 180;
        $lng2 = ($lng2 * pi()) / 180;

        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;

        $distance = round($calculatedDistance) / 1000;

        return $distance . 'km';
    }

    /**
     * 搜索
     * @param $uid
     * @param $key
     * @param $p
     * @return mixed
     */
    public function search($uid, $key, $p)
    {
        $limit = 50;
        $start = ($p - 1) * $limit;
        $where = ' user_type="2" and ( id=? or user_nicename like ? ) and id!=?';
        if ($p != 1) {
            $id = $_SESSION['search'];
            $where .= " and id < {$id}";
        }

        $result = DI()->notorm->users
            ->select("id,user_nicename,avatar,sex,signature,consumption")
            ->where($where, $key, "%{$key}%", $uid)
            ->order("id desc")
            ->limit($start, $limit)
            ->fetchAll();
        foreach ($result as $k => $v) {
            $result[$k]['level'] = (string)$this->getLevel($v['consumption']);
            $result[$k]['isattention'] = (string)Model_Auth_User::getInstance()->isAttention($uid, $v['id'], 0);
            $result[$k]['avatar'] = Model_Helper_Func::getUrl($v['avatar']);
            unset($result[$k]['consumption']);
        }

        if ($result) {
            $last = $result[count($result) - 1];
            $_SESSION['search'] = $last['id'];
        }

        return $result;
    }

    public function updateOnlineNums()
    {
        $nums = DI()->redis->get("totalNums");
        $data = [
            'num' => $nums,
            'addtime' => time(),
        ];
        DI()->notorm->online_nums
            ->insert($data);

        return $nums;
    }

}
