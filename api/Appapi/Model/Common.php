<?php

class Model_Common extends PhalApi_Model_NotORM
{
    private $errCode = 0;
    private $errMsg = '';
    protected static $instance = [];

    /**
     * @return static|mixed
     */
    public static function getInstance()
    {
        $class = get_called_class();
        $key = serialize(func_get_args()) . $class;
        if (empty(self::$instance[$key])) {
            self::$instance[$key] = new $class;
        }
        return self::$instance[$key];
    }

    public function setError($error = '', $errCode = 400)
    {
        $this->errMsg = $error;
        $this->errCode = $errCode;
    }

    public function getLastErrCode()
    {
        return (int)$this->errCode;
    }

    public function getLastErrMsg()
    {
        return $this->errMsg;
    }

    public function getLastErrorInfo()
    {
        return ['errcode' => $this->errCode, 'errmsg' => $this->errMsg,];
    }

    /**
     * @throws Exception
     */
    public function throwException()
    {
        throw new Exception($this->errMsg, $this->errCode);
    }

    ############################################
    #####      以下代码需要删除
    ############################################

    /**
     * 获取m3u8流
     * @param $stream
     * @return array|string
     */
    public function getM3u8Stream($stream)
    {
        $configPri = self::getConfigPri();
        if ($configPri['cdn_switch'] == 4 && $configPri['ws_hls_pull']) {
            $stream .= '/index.m3u8';
        } else {
            $stream .= '.m3u8';
        }

        return $this->PrivateKeyA('http', $stream, 0);
    }

    /**
     * @desc 获取推拉流地址
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function PrivateKeyA($host, $stream, $type)
    {
        $configpri = $this->getConfigPri();
        $cdn_switch = $configpri['cdn_switch'];
        switch ($cdn_switch) {
            case '1':
                $url = $this->PrivateKey_ali($host, $stream, $type);
                break;
            case '2':
                $url = $this->PrivateKey_tx($host, $stream, $type);
                break;
            case '3':
                $url = $this->PrivateKey_qn($host, $stream, $type);
                break;
            case '4':
                $url = $this->PrivateKey_ws($host, $stream, $type);
                break;
        }

        return $url;
    }

    /**
     * @desc 阿里云直播A类鉴权
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function PrivateKey_ali($host, $stream, $type)
    {
        $configpri = $this->getConfigPri();
        $key = $configpri['auth_key'];
        if ($type == 1) {
            $domain = $host . '://' . $configpri['push_url'];
            $time = time() + 60 * 60 * 10;
        } else {
            $domain = $host . '://' . $configpri['pull_url'];
            $time = time() - 60 * 30 + $configpri['auth_length'];
        }

        $filename = "/5showcam/" . $stream;
        if ($key != '') {
            $sstring = $filename . "-" . $time . "-0-0-" . $key;
            $md5 = md5($sstring);
            $auth_key = "auth_key=" . $time . "-0-0-" . $md5;
        }
        if ($type == 1) {
            if ($auth_key) {
                $auth_key = '&' . $auth_key;
            }
            $url = $domain . $filename . '?vhost=' . $configpri['pull_url'] . $auth_key;
        } else {
            if ($auth_key) {
                $auth_key = '?' . $auth_key;
            }
            $url = $domain . $filename . $auth_key;
        }

        return $url;
    }

    /**
     * @desc 腾讯云推拉流地址
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function PrivateKey_tx($host, $stream, $type)
    {
        $configpri = $this->getConfigPri();
        $bizid = $configpri['tx_bizid'];
        $push_url_key = $configpri['tx_push_key'];

        $stream_a = explode('.', $stream);
        $streamKey = $stream_a[0];
        $ext = $stream_a[1];

        $live_code = $bizid . "_" . $streamKey;
        $now_time = time() + 3 * 60 * 60;
        $txTime = dechex($now_time);

        $txSecret = md5($push_url_key . $live_code . $txTime);
        $safe_url = "&txSecret=" . $txSecret . "&txTime=" . $txTime;

        if ($type == 1) {
            //$push_url = "rtmp://" . $bizid . ".livepush2.myqcloud.com/live/" .  $live_code . "?bizid=" . $bizid . "&record=flv" .$safe_url;	可录像
            $url = "rtmp://" . $bizid . ".livepush2.myqcloud.com/live/" . $live_code . "?bizid=" . $bizid . "" . $safe_url;
        } else {
            $url = 'http://' . $bizid . ".liveplay.myqcloud.com/live/" . $live_code . ".flv";
        }

        return $url;
    }

    /**
     * @desc 七牛云直播
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function PrivateKey_qn($host, $stream, $type)
    {

        $configpri = $this->getConfigPri();
        $ak = $configpri['qn_ak'];
        $sk = $configpri['qn_sk'];
        $hubName = $configpri['qn_hname'];
        $push = $configpri['qn_push'];
        $pull = $configpri['qn_pull'];
        $stream_a = explode('.', $stream);
        $streamKey = $stream_a[0];
        $ext = $stream_a[1];

        if ($type == 1) {
            $time = time() + 60 * 60 * 10;
            //RTMP 推流地址
            $url = \Qiniu\Pili\RTMPPublishURL($push, $hubName, $streamKey, $time, $ak, $sk);
        } else {
            if ($ext == 'flv') {
                $pull = str_replace('pili-live-rtmp', 'pili-live-hdl', $pull);
                //HDL 直播地址
                $url = \Qiniu\Pili\HDLPlayURL($pull, $hubName, $streamKey);
            } else if ($ext == 'm3u8') {
                $pull = str_replace('pili-live-rtmp', 'pili-live-hls', $pull);
                //HLS 直播地址
                $url = \Qiniu\Pili\HLSPlayURL($pull, $hubName, $streamKey);
            } else {
                //RTMP 直播放址
                $url = \Qiniu\Pili\RTMPPlayURL($pull, $hubName, $streamKey);
            }
        }

        return $url;
    }

    /**
     * @desc 网宿推拉流
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function PrivateKey_ws($host, $stream, $type)
    {
        $configpri = $this->getConfigPri();
        if ($type == 1) {
            $domain = $host . '://' . $configpri['ws_push'];
        } else {
            $domain = $host . '://' . $configpri['ws_pull'];
        }

        $filename = "/" . $configpri['ws_apn'] . "/" . $stream;
        $url = $domain . $filename;

        return $url;
    }

    /*
     * ###########################################
     * 下面代码都可以删除了
     * ###########################################
     */
    /* 设置缓存 */
    public function setcache($key, $info)
    {
        $config = $this->getConfigPri();
        if ($config['cache_switch'] != 1) {
            return 1;
        }

        DI()->redis->set($key, json_encode($info));
        DI()->redis->setTimeout($key, $config['cache_time']);

        return 1;
    }

    /* 设置缓存 可自定义时间*/
    public function setcaches($key, $info, $time)
    {
        DI()->redis->set($key, json_encode($info));
        DI()->redis->setTimeout($key, $time);
        return 1;
    }

    /* 获取缓存 */
    public function getcache($key)
    {
        $config = $this->getConfigPri();

        if ($config['cache_switch'] != 1) {
            $isexist = false;
        } else {
            $isexist = DI()->redis->get($key);
        }

        return json_decode($isexist, true);
    }

    /* 获取缓存 不判断后台设置 */
    public function getcaches($key)
    {
        return json_decode(DI()->redis->get($key), true);
    }

    /* 删除缓存 */
    public function delcache($key)
    {
        return DI()->redis->delete($key);
    }

    /* 密码检查 */
    public function passcheck($user_pass)
    {
        $num = preg_match("/^[a-zA-Z]+$/", $user_pass);
        $word = preg_match("/^[0-9]+$/", $user_pass);
        $check = preg_match("/^[a-zA-Z0-9]{6,12}$/", $user_pass);
        if ($num || $word) {
            return 2;
        } else if (!$check) {
            return 0;
        }
        return 1;
    }

    /* 密码加密 */
    public function setPass($pass)
    {
        $authcode = 'rCt52pF2cnnKNB3Hkp';
        $pass = "###" . md5(md5($authcode . $pass));
        return $pass;
    }

    /* 公共配置 */
    public function getConfigPub()
    {
        $key = 'publicConfig';
        $config = $this->getcaches($key);
        if (!$config) {
            $config = DI()->notorm->config
                ->select('*')
                ->where(" id ='1'")
                ->fetchOne();
            $this->setcaches($key, $config, 300);
        }

        if ($config) {
            $config['apk_url'] = Model_Helper_Func::getUrl($config['apk_url']);
        }

        return $config;
    }

    /**
     * 私密配置
     * @return bool|mixed
     */
    public function getConfigPri()
    {
        $key = 'privateConfig';
        $config = $this->getcaches($key);
        if (!$config) {
            $config = DI()->notorm->config_private
                ->select('*')
                ->where("id ='1'")
                ->fetchOne();
            $this->setcaches($key, $config, 60);
        }

        return $config;
    }

    /**
     * @param $uid
     * @param $token
     * @return int
     */
    public function checkToken($uid, $token)
    {
        $user = Model_Auth_User::getInstance()->getUserInfo($uid);
        if (!$user) {
            return 700;
        }

        if (Model_Auth_User::getInstance()->isVisitor($uid)) {
            return 0;
        }

        if ($user['token'] != $token || $user['expiretime'] < time()) {
            return 700;
        }

        return 0;
    }

    /* 会员等级 */
    public function getLevel($experience)
    {
        $levelid = 1;
        $key = 'level';
        $level = $this->getCache($key);
        if (!$level) {
            $level = DI()->notorm->experlevel
                ->select("levelid,level_up")
                ->order("level_up asc")
                ->fetchAll();
            $this->setCache($key, $level);
        }

        foreach ($level as $k => $v) {
            if ($v['level_up'] >= $experience) {
                $levelid = $v['levelid'];
                break;
            }
        }
        return $levelid;
    }

    /* 等级区间限额 */
    public function getLevelSection($level)
    {
        $key = 'experlevel_limit';
        $experlevel_limit = $this->getCache($key);
        if (!$experlevel_limit) {
            $experlevel_limit = DI()->notorm->experlevel_limit
                ->select("withdraw,level_up")
                ->order("level_up asc")
                ->fetchAll();
            $this->setCache($key, $experlevel_limit);
        }

        foreach ($experlevel_limit as $k => $v) {
            if ($v['level_up'] >= $level) {
                $withdraw = $v['withdraw'];
                break;
            }

        }
        return $withdraw;
    }

    /* 过滤字符 */
    public function filterField($field)
    {
        $configpri = $this->getConfigPri();
        $sensitive_field = $configpri['sensitive_field'];
        $sensitive = explode(",", $sensitive_field);
        $replace = [];
        $preg = [];
        foreach ($sensitive as $k => $v) {
            if ($v) {
                $re = '';
                $num = mb_strlen($v);
                for ($i = 0; $i < $num; $i++) {
                    $re .= '*';
                }
                $replace[$k] = $re;
                $preg[$k] = '/' . $v . '/i';
            } else {
                unset($sensitive[$k]);
            }
        }

        return preg_replace($preg, $replace, $field);
    }

    /**
     * @deprecated
     * 通过三方接口获取用户余额
     */
    public function getApiUserCoin($user_login, $user_pass)
    {
        $data = [
            'username' => $user_login,
            'password' => $user_pass,
        ];

        $url = 'http://api.k8531.com/api-get-zhibo-balance.htm';
        $response = $this->curl_post($data, $url);
        $response = json_decode($response, 1);
        if ($response['status'] == 0) {
            $coin = $response['data']['balance'];
        } else {
            return 0;//查询失败
        }

        return $coin;
    }

    /**
     * @deprecated
     * 通过三方接口扣除用户消费
     */
    public function removeApiUserCoin($user_login, $user_pass, $total)
    {
        $data = [
            'username' => $user_login,
            'password' => $user_pass,
            'amount' => $total,
        ];

        $url = 'http://api.k8531.com/api-transfer-zhibo-balance.htm';
        $response = $this->curl_post($data, $url);
        $response = json_decode($response, 1);
        if ($response['status'] == -3) { //余额不足
            return false;
        } else if ($response['status'] == 1) {//操作失败
            return false;
        } else if ($response['status'] == 999) {//操作异常
            return false;
        } else {
            return true;
        }
    }

    function curl_post($curlPost, $url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
        $return_str = curl_exec($curl);
        curl_close($curl);
        return $return_str;
    }

    /**
     * 检测文件后缀
     * @param $filename
     * @return bool
     */
    public function checkExt($filename)
    {
        $config = ["jpg", "png", "jpeg"];
        $ext = pathinfo(strip_tags($filename), PATHINFO_EXTENSION);

        return empty($config) ? true : in_array(strtolower($ext), $config);
    }

    /**
     * 检验手机号
     * @param $mobile
     * @return false|int
     */
    public function checkMobile($mobile)
    {
        return preg_match("/^1[3|4|5|7|8]\d{9}$/", $mobile);
    }

}
