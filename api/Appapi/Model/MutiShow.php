<?php

/**
 * User: kobi.h
 */
class Model_MutiShow extends Model_Base
{
    protected function getTableName($id)
    {
        return 'muti_show';
    }

    /**
     * muti_show config
     */
    const ROOM_TYPE_GENERAL = 0;
    const ROOM_TYPE_LOCK = 1;
    const ROOM_TYPE_LIVE_COIN = 2;

    const ANCHOR_LIMIT = 50;

    //路由前缀，专门给多人房间
    const ROUTE_PREFIX = '0';

    const KEY_MULTI_SHOW = 'muti_show';

    /**
     * muti_show主表附属属性
     * @var array
     */
    public static $multiShowExtField = [
        'room_type' => '0', //房间类型 0-普通，1-锁门，2-扣券
        'room_password' => '',
        'room_live_coin' => '0',
    ];

    /**
     * 获取房间信息统一方法
     * @param $multiId
     * @return array|bool
     */
    public function getMultiShowInfo($multiId)
    {
        $show = $this->get($multiId);
        if (!$show) {
            return false;
        }

        return array_merge($show, $this->getMultiShowExtData($multiId) ?: []);
    }

    /**
     * 详情
     * @param $roomId
     * @param $uid
     * @return bool|mixed
     */
    public function getRoomInfo($roomId, $uid, $online = true)
    {
        if (!$roomId) {
            return false;
        }

        $show = $this->getMultiShowInfo($roomId);
        if (!$show) {
            $this->setError('多视频直播不存在');
            return false;
        }

        $show['avatar'] = Model_Helper_Func::getUrl($show['avatar']);
        $show['focus'] = Model_Helper_Func::getUrl($show['focus']);
        $show['multi_stream'] = $show['multi_stream'] ? json_decode($show['multi_stream'], true) : '';
        $list = DI()->notorm->muti_showmanager->select('*')->where('muti_id=?', $roomId)->fetchAll() ?: [];
        $users = Model_Helper_Func::index(Model_Auth_User::getInstance()->getUsersInfo(array_column($list, 'uid')) ?: [], 'id');
        $show['anchor'] = [];
        $show['dj'] = [];
        $show['ami_anchor'] = false;
        $show['ami_dj'] = false;

        foreach ($list as $li) {
            if (empty($users[$li['uid']])) {
                continue;
            }

            unset($li['muti_id'], $li['sort_no']);
            $user = $users[$li['uid']];
            $li['user_nicename'] = $user['user_nicename'];
            $li['avatar'] = $user['avatar'];
            $li['sex'] = $user['sex'];

            switch ($li['vest_type']) {
                case Model_Auth_User::VEST_TYPE_ANCHOR:
                    if ($li['uid'] == $uid) {
                        $show['ami_anchor'] = true;
                    }
                    if ($online && $li['status'] != 0) {
                        $show['anchor'][] = $li;
                    }
                    break;
                case Model_Auth_User::VEST_TYPE_DJ:
                    if ($li['uid'] == $uid) {
                        $show['ami_dj'] = true;
                    }
                    if ($online && $li['status'] != 0) {
                        $show['dj'][] = $li;
                    }
                    break;
            }
        }

        $show['ami_manager'] = $show['ami_anchor'];

        return $show;
    }

    /**
     * 开始/关闭直播,开始/关闭喊麦API
     * @param $uid
     * @param $multiId
     * @param $vestType
     * @param $status
     * @return bool
     */
    public function setMultiManagerStatus($uid, $multiId, $vestType, $status)
    {
        if (!$uid || !$multiId || !$vestType) {
            $this->setError('缺参数');
            return false;
        }
        if (!in_array($status, [0, 1])) {
            $this->setError('status参数错误');
            return false;
        }
        switch ($vestType) {
            case 'anchor':
                $vestType = Model_Auth_User::VEST_TYPE_ANCHOR;
                break;
            case 'dj':
                $vestType = Model_Auth_User::VEST_TYPE_DJ;
                break;
            default:
                $this->setError('vest_type参数错误');
                return false;
        }

        $ex = DI()->notorm->muti_showmanager
            ->where('muti_id=? and uid=? and vest_type=?', $multiId, $uid, $vestType)->fetchOne();
        if (!$ex) {
            $this->setError('您不在该房间');
            return false;
        }

        if ($vestType == Model_Auth_User::VEST_TYPE_ANCHOR) {
            $showNum = $this->getMultiShowNum($multiId, $vestType, 1);
            if ($showNum > self::ANCHOR_LIMIT) {
                $this->setError('当前房间已满');
                return false;
            }
        }

        $rs = DI()->notorm->muti_showmanager->where(['id' => $ex['id']])->update([
            'status' => $status,
            'updated_at' => time(),
            'ispc' => 0,
        ]);
        if (false === $rs) {
            $this->setError('操作失败');
            return false;
        }
        DI()->notorm->muti_show->where('id = ?', $multiId)->update([
            'updated_at' => time(),
        ]);

        return true;
    }

    public function getMultiShowNum($multiId, $vestType = 0, $status = 0)
    {
        return DI()->notorm->muti_showmanager
            ->where('muti_id=? and vest_type=? and status=?', $multiId, $vestType, $status)
            ->count();
    }

    /**
     * 检查路由是否是多视频直播
     * @param $roomNum
     * @return bool
     */
    public function routeIsMultiLive($roomNum)
    {
        return 0 === stripos($roomNum, self::ROUTE_PREFIX);
    }

    /**
     * 是否是1号角色
     * @param $uid
     * @param $multiId
     * @param int $vestType
     * @return bool
     */
    public function isFirstManager($uid, $multiId, $vestType = Model_Auth_User::VEST_TYPE_ANCHOR)
    {
        if (!$uid || !$multiId) {
            return false;
        }

        $first = DI()->notorm->muti_showmanager
            ->where('muti_id=? and vest_type=?', $multiId, $vestType)
            ->order("sort_no DESC")
            ->fetchOne();
        if (!$first) {
            return false;
        }

        return $first['uid'] == $uid;
    }

    /**
     * 设置房间密码
     * @param $uid
     * @param $multiId
     * @param $password
     * @return bool
     */
    public function setMultiShowPassword($uid, $multiId, $password)
    {
        if (!$uid || !$multiId || !$password) {
            $this->setError('缺参数');
            return false;
        }
        if (!$this->validateRoomPassword($password)) {
            return false;
        }

        if (!$this->isFirstManager($uid, $multiId)) {
            $this->setError('没有权限，你不是第一个主播');
            return false;
        }

        if (!$this->getMultiShowInfo($multiId)) {
            $this->setError('房间不存在');
            return false;
        }

        return $this->setMultiShowExtData($multiId, ['room_password' => $password]);
    }

    public function setMultiShowExtData($multiId, array $param)
    {
        $fields = array_keys(self::$multiShowExtField);
        foreach ($param as $key => $value) {
            if (in_array($key, $fields)) {
                $rs = Model_Helper_Redis::getInstance()->hSet(self::KEY_MULTI_SHOW . $multiId, $key, $value);
                if (false === $rs) {
                    $this->setError('设置失败');
                    return false;
                }
            }
        }

        DI()->notorm->muti_show->where("id=?", $multiId)->update(['updated_at' => time()]);

        return true;
    }

    public function getMultiShowExtData($multiId)
    {
        $data = Model_Helper_Redis::getInstance()->hGetAll(self::KEY_MULTI_SHOW . $multiId) ?: [];
        return array_merge(self::$multiShowExtField, $data);
    }

    /**
     * validate room password
     * @param $password
     * @return bool
     */
    public function validateRoomPassword($password)
    {
        if (!$password) {
            $this->setError('密码为空');
            return false;
        }
        if (!is_numeric($password) || mb_strlen($password) > 6) {
            $this->setError('只能输入数字，最多6位');
            return false;
        }

        return true;
    }


}
