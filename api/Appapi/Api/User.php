<?php

class Api_User extends Api_Base
{
    public function getRules()
    {
        return [
            'iftoken' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'getBaseInfo' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'updateAvatar' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'file' => ['name' => 'file', 'type' => 'file', 'min' => 0, 'max' => 1024 * 1024 * 30, 'range' => ['image/jpg', 'image/jpeg', 'image/png'], 'ext' => ['jpg', 'jpeg', 'png']],
            ],
            'updateFields' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'fields' => ['name' => 'fields', 'type' => 'string', 'require' => true, 'desc' => '修改信息，json字符串'],
            ],
            'updatePass' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'oldpass' => ['name' => 'oldpass', 'type' => 'string', 'require' => true, 'desc' => '旧密码'],
                'pass' => ['name' => 'pass', 'type' => 'string', 'require' => true, 'desc' => '新密码'],
                'pass2' => ['name' => 'pass2', 'type' => 'string', 'require' => true, 'desc' => '确认密码'],
            ],
            'getBalance' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'getProfit' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'setCash' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'setAttent' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID',],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID',],
            ],
            'isAttent' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
            ],
            'isBlacked' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
            ],
            'checkBlack' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
            ],
            'setBlack' => [
                //PhalApi::IS_POST => true,
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID',],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID',],
            ],
            'getBindCode' => [
                'mobile' => ['name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '手机号'],
            ],
            'setMobile' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'mobile' => ['name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '手机号'],
                'code' => ['name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '验证码'],
            ],
            'getFollowsList' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'min' => 1, 'default' => 1, 'desc' => '页数'],
            ],
            'getFansList' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'min' => 1, 'default' => 1, 'desc' => '页数'],
            ],
            'getBlackList' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'min' => 1, 'default' => 1, 'desc' => '页数'],
            ],
            'getLiverecord' => [
                'touid' => ['name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'min' => 1, 'default' => 1, 'desc' => '页数'],
            ],
            'getAliCdnRecord' => [
                'id' => ['name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '直播记录ID'],
            ],
            'getUserHome' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
            ],
            'getContributeList' => [
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'getPmUserInfo' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '对方ID'],
            ],
            'getMultiInfo' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'uids' => ['name' => 'uids', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '用户ID，多个以逗号分割'],
                'type' => ['name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '关注类型，0 未关注 1 已关注'],
            ],
            'Bonus' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
        ];
    }

    /**
     * 判断token
     */
    public function ifToken()
    {
        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        return $this->renderObject();
    }

    /**
     * 获取用户信息
     * level 等级
     * lives 直播数量
     * follows 关注数
     * fans 粉丝数
     */
    public function getBaseInfo()
    {
        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $info = (new Model_Auth_User())->getUserInfo($this->uid);

        return $this->renderObject($info);
    }

    /**
     * 头像上传
     */
    public function updateAvatar()
    {
        $uid = $this->uid;
        $checkToken = $this->checkToken($uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }
        if (!isset($_FILES['file'])) {
            return $this->renderError(T('miss upload file'));
        }

        $domain = Model_Auth_User::getInstance();
        $data = $domain->updateAvatar($uid, $_FILES['file']);
        if (false === $data) {
            return $this->renderError($domain->getLastErrMsg());
        }

        $user = $domain->getUserInfo($uid);

        return $this->renderObject($user);
    }

    /**
     * 修改用户信息
     */
    public function updateFields()
    {
        $uid = $this->uid;
        $token = $this->token;
        $fields = json_decode($this->fields, true) ?: [];

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }
        if (!$fields) {
            return $this->renderError('信息不能为空');
        }

        foreach ($fields as $k => $v) {
            $fields[$k] = $this->checkNull($v);
        }

        $domain = new Model_Auth_User();
        if (array_key_exists('user_nicename', $fields)) {
            if ($fields['user_nicename'] == '') {
                return $this->renderError('昵称不能为空');
            }
            if ($domain->checkName($uid, $fields['user_nicename'])) {
                return $this->renderError('昵称重复，请修改');
            }
            $fields['user_nicename'] = Model_Common::getInstance()->filterField($fields['user_nicename']);
        }

        if (array_key_exists('signature', $fields)) {
            $fields['signature'] = Model_Common::getInstance()->filterField($fields['signature']);
        }

        $info = $domain->update($uid, $fields);
        if ($info === false) {
            return $this->renderError('修改失败');
        }

        return $this->renderObject($fields);
    }

    /**
     * 修改密码
     */
    public function updatePass()
    {
        $uid = $this->uid;
        $token = $this->token;
        $oldpass = $this->oldpass;
        $pass = $this->pass;
        $pass2 = $this->pass2;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        if ($pass != $pass2) {
            return $this->renderError('两次新密码不一致');
        }

        $check = Model_Common::getInstance()->passcheck($pass);
        if ($check == 0) {
            return $this->renderError('密码为6-12位数字与字母组合');
        } else if ($check == 2) {
            return $this->renderError('密码不能纯数字或纯字母');
        }

        $domain = new Model_Auth_User();
        $info = $domain->updatePass($uid, $oldpass, $pass);

        if ($info == 1003) {
            return $this->renderError('旧密码错误');
        } else if ($info === false) {
            return $this->renderError('修改失败');
        }

        return $this->renderObject();
    }

    /**
     * 我的钻石
     * @desc 用于获取用户余额,充值规则 支付方式信息
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.coin 用户余额
     * @return array info.rules 充值规则
     * @return string info.rules[].id 充值规则
     * @return string info.rules[].coin 钻石
     * @return string info.rules[].money 价格
     * @return string info.rules[].money_ios 苹果充值价格
     * @return string info.rules[].product_id 苹果项目ID
     * @return string info.rules[].give 赠送钻石，为0时不显示赠送
     * @return string info.aliapp_switch 支付宝开关，0表示关闭，1表示开启
     * @return string info.aliapp_partner 支付宝合作者身份ID
     * @return string info.aliapp_seller_id 支付宝帐号
     * @return string info.aliapp_key_android 支付宝安卓密钥
     * @return string info.aliapp_key_ios 支付宝苹果密钥
     * @return string info.wx_switch 微信支付开关，0表示关闭，1表示开启
     * @return string info.wx_appid 开放平台账号AppID
     * @return string info.wx_appsecret 微信应用appsecret
     * @return string info.wx_mchid 微信商户号mchid
     * @return string info.wx_key 微信密钥key
     * @return string msg 提示信息
     */
    public function getBalance()
    {
        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Auth_User();
        $info = $domain->getBalance($this->uid);

        $rules = $domain->getChargeRules();
        $info['rules'] = $rules;

        $configpri = Model_Common::getInstance()->getConfigPri();

        $info['aliapp_switch'] = $configpri['aliapp_switch'];
        $info['aliapp_partner'] = $configpri['aliapp_switch'] == 1 ? $configpri['aliapp_partner'] : '';
        $info['aliapp_seller_id'] = $configpri['aliapp_switch'] == 1 ? $configpri['aliapp_seller_id'] : '';
        $info['aliapp_key_android'] = $configpri['aliapp_switch'] == 1 ? $configpri['aliapp_key_android'] : '';
        $info['aliapp_key_ios'] = $configpri['aliapp_switch'] == 1 ? $configpri['aliapp_key_ios'] : '';

        $info['wx_switch'] = $configpri['wx_switch'];
        $info['wx_appid'] = $configpri['wx_switch'] == 1 ? $configpri['wx_appid'] : '';
        $info['wx_appsecret'] = $configpri['wx_switch'] == 1 ? $configpri['wx_appsecret'] : '';
        $info['wx_mchid'] = $configpri['wx_switch'] == 1 ? $configpri['wx_mchid'] : '';
        $info['wx_key'] = $configpri['wx_switch'] == 1 ? $configpri['wx_key'] : '';

        return $this->renderObject($info);
    }

    /**
     * 我的收益，包括可体现金额，今日可提现金额
     */
    public function getProfit()
    {
        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Auth_User();
        $info = $domain->getProfit($this->uid);

        return $this->renderObject($info);
    }

    /**
     * 用户提现
     */
    public function setCash()
    {
        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Auth_User();
        $info = $domain->setCash($this->uid);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 判断是否关注 isattent 关注信息，0表示未关注，1表示已关注
     */
    public function isAttent()
    {
        $info = Model_Auth_User::getInstance()->isAttention($this->uid, $this->touid, 0);

        return $this->renderObject([
            'isattent' => (string)$info,
        ]);
    }

    /**
     * 关注/取消关注
     */
    public function setAttent()
    {
        $uid = (int)$this->uid;
        $liveType = (int)$this->live_type;
        $touid = $this->touid;

        $domain = new Model_Auth_User();
        $info = $domain->setAttent($uid, $liveType, $touid);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'isattent' => (string)$info,
        ]);
    }

    /**
     * 判断是否拉黑 拉黑信息,0表示未拉黑，1表示已拉黑
     */
    public function isBlacked()
    {
        $info = Model_Auth_User::getInstance()->isBlack($this->uid, $this->touid);

        return $this->renderObject([
            'isblack' => (string)$info,
        ]);
    }

    /**
     * 检测拉黑状态
     * @desc 用于私信聊天时判断私聊双方的拉黑状态
     * u2t  是否拉黑对方,0表示未拉黑，1表示已拉黑
     * t2u  是否被对方拉黑,0表示未拉黑，1表示已拉黑
     */
    public function checkBlack()
    {
        $uid = $this->uid;
        $touid = $this->touid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $u2t = Model_Auth_User::getInstance()->isBlack($uid, $touid);
        $t2u = Model_Auth_User::getInstance()->isBlack($touid, $uid);
        $privateChat = (int)Model_Live::getInstance()->getPrivateChatPermission($uid, $touid, $liveType, $liveId);

        return $this->renderObject([
            'u2t' => (string)$u2t,
            't2u' => (string)$t2u,
            'privateChat' => (string)$privateChat, //0-no,1-yes
        ]);
    }

    /**
     * 拉黑/取消拉黑 isblack 拉黑信息,0表示未拉黑，1表示已拉黑
     */
    public function setBlack()
    {
        $domain = new Model_Auth_User();
        $info = $domain->setBlack($this->uid, $this->touid);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'isblack' => (string)$info,
        ]);
    }

    /**
     * 获取找回密码短信验证码
     */
    public function getBindCode()
    {
        $mobile = $this->mobile;

        $ismobile = Model_Common::getInstance()->checkMobile($mobile);
        if (!$ismobile) {
            return $this->renderError('请输入正确的手机号');
        }

        if ($_SESSION['set_mobile'] == $mobile && $_SESSION['set_mobile_expiretime'] > time()) {
            return $this->renderError('验证码5分钟有效，请勿多次发送');
        }

        $_SESSION['set_mobile'] = $mobile;
        $_SESSION['set_mobile_code'] = '123456';
        $_SESSION['set_mobile_expiretime'] = time() + 60 * 5;

        return $this->renderObject();
    }

    /**
     * 绑定手机号
     */
    public function setMobile()
    {
        $mobile = $this->mobile;

        if ($mobile != $_SESSION['set_mobile']) {
            return $this->renderError('手机号码不一致');
        }

        if ($this->code != $_SESSION['set_mobile_code']) {
            return $this->renderError('验证码错误');
        }

        $checkToken = $this->checkToken($this->uid, $this->token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Auth_User();
        $data = ["mobile" => $mobile];
        $result = $domain->update($this->uid, $data);
        if ($result === false) {
            return $this->renderError('绑定失败');
        }

        return $this->renderObject();
    }

    /**
     * 关注列表 isattent 是否关注,0表示未关注，1表示已关注
     */
    public function getFollowsList()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getFollowsList($this->uid, $this->touid, $this->p);

        return $this->renderArray($info);
    }

    /**
     * 粉丝列表
     */
    public function getFansList()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getFansList($this->uid, $this->touid, $this->p);

        return $this->renderArray($info);
    }

    /**
     * 黑名单列表
     */
    public function getBlackList()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getBlackList($this->uid, $this->touid, $this->p);

        return $this->renderArray($info);
    }

    /**
     * 直播记录 nums 观看人数 datestarttime 格式化的开播时间 dateendtime 格式化的结束时间 video_url 回放地址 file_id 回放标示
     */
    public function getLiverecord()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getLiverecord($this->touid, $this->p);

        return $this->renderArray($info);
    }

    /**
     * 获取阿里云cdn录播地址
     */
    public function getAliCdnRecord()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getAliCdnRecord($this->id);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'url' => $info,
        ]);
    }

    /**
     * 个人主页
     */
    public function getUserHome()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getUserHome($this->uid, $this->touid);

        return $this->renderObject($info);
    }

    /**
     * 个人主页
     * @desc 用于获取个人主页数据
     * @return int code 操作码，0表示成功
     * @return array info 排行榜列表
     * @return string info[].total 贡献总数
     * @return string info[].userinfo 用户信息
     * @return string msg 提示信息
     */
    public function getContributeList()
    {
        $domain = new Model_Auth_User();
        $info = $domain->getContributeList($this->touid, $this->p);

        return $this->renderArray($info);
    }

    /**
     * 私信用户信息
     */
    public function getPmUserInfo()
    {
        $uid = $this->uid;
        $touid = $this->touid;

        $info = Model_Auth_User::getInstance()->getUserInfo($touid);
        $info['isattention'] = (string)Model_Auth_User::getInstance()->isAttention($uid, $touid, 0);
        $info['isattention2'] = (string)Model_Auth_User::getInstance()->isAttention($touid, $uid, 0);

        return $this->renderObject($info);
    }

    /**
     * 获取多用户信息
     */
    public function getMultiInfo()
    {
        $uid = $this->uid;
        $uids = explode(',', $this->uids);
        $type = $this->type;

        $list = [];
        foreach ($uids as $userId) {
            if ($userId) {
                $userinfo = Model_Auth_User::getInstance()->getUserInfo($userId);
                if ($userinfo) {
                    $userinfo['utot'] = Model_Auth_User::getInstance()->isAttention($uid, $userId, 0); //是否关注，0未关注，1已关注
                    $userinfo['ttou'] = Model_Auth_User::getInstance()->isAttention($userId, $uid, 0); //对方是否关注我，0未关注，1已关注
                    if ($userinfo['utot'] == $type) {
                        $list[] = $userinfo;
                    }
                }
            }
        }

        return $this->renderArray($list);
    }

    /**
     * 登录奖励
     * @desc 用于用户登录奖励
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.bonus_switch 登录开关，0表示未开启
     * @return string info.bonus_day 登录天数,0表示已奖励
     * @return string info.bonus_list 登录奖励列表
     * @return string info.bonus_list[].day 登录天数
     * @return string info.bonus_list[].coin 登录奖励
     * @return string msg 提示信息
     */
    public function bonus()
    {
        $uid = $this->uid;
        $token = $this->checkNull($this->token);

        $info = Model_Login::getInstance()->loginBonus($uid, $token);
        if ($info == 700) {
            return $this->renderError('请重新登录', 700);
        }

        return $this->renderObject($info);
    }

}
