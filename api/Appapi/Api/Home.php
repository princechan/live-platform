<?php

class Api_Home extends Api_Base
{
    public function getRules()
    {
        return [
            'getHot' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'getFollow' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'getNew' => [
                'lng' => ['name' => 'lng', 'type' => 'string', 'desc' => '经度值'],
                'lat' => ['name' => 'lat', 'type' => 'string', 'desc' => '纬度值'],
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'search' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'min' => 1, 'desc' => '用户ID'],
                'key' => ['name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '请填写关键词'],
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'searchRoom' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'key' => ['name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '请填写关键词'],
            ],
            'bootstrap' => [
                //PhalApi::IS_POST => true,
                'token' => ['name' => 'token', 'type' => 'string', 'require' => false, 'desc' => '用户token'],
            ],
            'getSlideLive' => [

            ],
            'slideLiveV2' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
            ],
        ];
    }

    /**
     * 搜索房间
     * @return array
     */
    public function searchRoom()
    {
        $key = $this->checkNull($this->key);
        $uid = $this->checkNull($this->uid);

        $list = Model_Home::getInstance()->searchRoom($key, $uid);

        return $this->renderArray($list);
    }

    /**
     * 配置信息
     */
    public function getConfig()
    {
        return $this->renderObject(Model_Common::getInstance()->getConfigPub());
    }

    /**
     * 获取过滤词汇
     */
    public function getFilterField()
    {
        $info = Model_Common::getInstance()->getConfigPri();
        $info = $info['sensitive_field'] ? explode(',', $info['sensitive_field']) : [];

        return $this->renderArray($info);
    }

    /**
     * 登录方式开关信息
     * @return mixed
     * @return array info
     * @return array info.login_qq qq登录，0表示关闭，1表示开启
     * @return array info.login_wx 微信登录，0表示关闭，1表示开启
     * @return array info.login_sina 新浪微博登陆，0表示关闭，1表示开启
     * @return string msg 提示信息
     */
    public function getLogin()
    {
        $info = Model_Common::getInstance()->getConfigPri();

        return $this->renderObject([
            'login_qq' => $info['login_qq'],
            'login_wx' => $info['login_wx'],
            'login_sina' => $info['login_sina'],
        ]);
    }

    /**
     * 首页热门直播
     */
    public function getHot()
    {
        $uid = $this->uid;
        $page = (int)$this->p;
        if ($page <= 1) {
            $page = 1;
        }

        $limit = 10;
        $list = Model_Home::getInstance()->getLiveList($uid, $page, $limit);

        return $this->renderObject([
            'has_more' => $limit == count($list),
            'list' => $list,
            'slide_livers' => $page == 1 ? Model_Home::getInstance()->getSlideLive() : [],
        ]);
    }

    /**
     * 轮播
     */
    public function getSlideLive()
    {
        return $this->renderArray(Model_Home::getInstance()->getSlideLive());
    }

    /**
     * 轮播V1.2
     */
    public function slideLiveV2()
    {
        return $this->renderArray(Model_Home::getInstance()->slideLiveV2($this->uid));
    }

    /**
     * 关注的直播列表
     */
    public function getFollow()
    {
        return $this->renderArray((new Model_Home())->getFollow($this->uid, $this->p));
    }

    /**
     * 获取首页最新开播的主播列表
     */
    public function getNew()
    {
        $lng = $this->checkNull($this->lng);
        $lat = $this->checkNull($this->lat);
        $p = $this->checkNull($this->p);

        if (!$lng) {
            $lng = 0;
        }

        if (!$lat) {
            $lat = 0;
        }

        if (!$p) {
            $p = 1;
        }

        $domain = new Model_Home();
        $info = $domain->getNew($lng, $lat, $p);

        return $this->renderArray($info);
    }

    /**
     * 用于首页搜索会员
     */
    public function search()
    {
        $uid = $this->checkNull($this->uid);
        $key = $this->checkNull($this->key);
        $p = $this->checkNull($this->p);

        if (!$p) {
            $p = 1;
        }

        return $this->renderArray((new Model_Home())->search($uid, $key, $p));
    }

    /**
     * 更新在线人数【cmf_online_nums】，通过定时任务访问该接口
     */
    public function updateOnlineNums()
    {
        $domain = new Model_Home();
        $info = $domain->updateOnlineNums();

        return $this->renderObject($info);
    }

    /**
     * 启动程序（游客才调用）
     */
    public function bootstrap()
    {
        $token = trim($this->token);
        $data = Model_Auth_User::getInstance()->bootstrap($token);

        return $this->renderObject($data);
    }

}
