<?php

class Api_Login extends Api_Base
{
    public function getRules()
    {
        return [
            'userLogin' => [
                PhalApi::IS_POST => true,
                PhalAPi::USE_CUSTOM_VALIDATION => true,
                'user_login' => ['name' => 'user_login', 'type' => 'string', 'require' => true, 'min' => '3', 'max' => '30', 'desc' => '账号',],
                'user_pass' => ['name' => 'user_pass', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '30', 'desc' => '密码',],
            ],
            'userLogin1' => [
                'username' => ['name' => 'username', 'type' => 'string', 'min' => '6', 'require' => true, 'max' => '30', 'desc' => '登录账号'],
                'password' => ['name' => 'password', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '35', 'desc' => '密码'],
                'level' => ['name' => 'level', 'type' => 'int', 'require' => true, 'min' => '1', 'desc' => '等级'],
                'roomid' => ['name' => 'roomid', 'type' => 'int', 'default' => '', 'desc' => '房间号'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
            ],
            'userReg' => [
                PhalApi::IS_POST => true,
                PhalAPi::USE_CUSTOM_VALIDATION => true,
                'user_login' => ['name' => 'user_login', 'type' => 'string', 'require' => true, 'min' => '6', 'max' => '30', 'desc' => '账号',],
                'user_pass' => ['name' => 'user_pass', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '30', 'desc' => '密码',],
                'code' => ['name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '验证码',],
                'device_num' => ['name' => 'device_num', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '64', 'desc' => '设备号',],
            ],
            'userFindPass' => [
                'user_login' => ['name' => 'user_login', 'type' => 'string', 'require' => true, 'min' => '6', 'max' => '30', 'desc' => '账号'],
                'user_pass' => ['name' => 'user_pass', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '30', 'desc' => '密码'],
                'user_pass2' => ['name' => 'user_pass2', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '30', 'desc' => '确认密码'],
                'code' => ['name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '验证码'],
            ],
            'userLoginByThird' => [
                'openid' => ['name' => 'openid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '第三方openid'],
                'type' => ['name' => 'type', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '第三方标识'],
                'nicename' => ['name' => 'nicename', 'type' => 'string', 'default' => '', 'desc' => '第三方昵称'],
                'avatar' => ['name' => 'avatar', 'type' => 'string', 'default' => '', 'desc' => '第三方头像'],
            ],
            'getForgetCode' => [
                'mobile' => ['name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '手机号'],
            ],
            'checkIsLogin' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'],
            ],
            'getVerificationCode' => [
                PhalApi::IS_POST => true,
                'device_num' => ['name' => 'device_num', 'type' => 'string', 'require' => true, 'min' => '1', 'max' => '64', 'desc' => '设备号',],
                'width' => ['name' => 'width', 'type' => 'int', 'require' => false, 'default' => 120, 'desc' => '宽度'],
                'height' => ['name' => 'height', 'type' => 'int', 'require' => false, 'default' => 40, 'desc' => '高度'],
            ],
        ];
    }

    /**
     * 会员登陆 需要密码
     */
    public function userLogin()
    {
        $user_login = $this->checkNull($this->user_login);
        $user_pass = $this->checkNull($this->user_pass);

        $domain = Model_Login::getInstance();
        $info = $domain->userLogin($user_login, $user_pass);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject($info);
    }

    /**
     * 会员注册
     */
    public function userReg()
    {
        $user_login = $this->checkNull($this->user_login);
        $user_pass = $this->checkNull($this->user_pass);
        $code = $this->checkNull($this->code);
        $device_num = $this->device_num;

        if (!$device_num) {
            return $this->renderError('缺参数：设备号');
        }

        $srv = Model_Login::getInstance();

        $validateCode = $srv->validateCode($code);
        if (false === $validateCode) {
            return $this->renderError($srv->getLastErrMsg());
        }

        $info = $srv->userReg($user_login, $user_pass);
        if (false === $info) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject($info);
    }

    /**
     * 会员登录（客户专用）
     */
    public function userLogin1()
    {
        $username = $this->checkNull($this->username);
        $password = $this->checkNull($this->password);
        $level = $this->level;
        $roomid = $this->roomid;
        $token = $this->token;

        $domain = Model_Login::getInstance();
        $info = $domain->userLogin1($username, $password, $level, $roomid, $token);

        if ($info == 1001) {
            return $this->renderError('账号或密码错误');
        } else if ($info == 1002) {
            return $this->renderError('该账号已被禁用');
        } else if ($info == 1003) {
            return $this->renderError('未知错误');
        }

        return $this->renderObject($info);
    }

    /**
     * 会员找回密码
     * @desc 用于会员找回密码 0表示成功，1表示验证码错误，2表示用户密码不一致,3短信手机和登录手机不一致 4、用户不存在 801 密码6-12位数字与字母
     */
    public function userFindPass()
    {
        $user_login = $this->checkNull($this->user_login);
        $user_pass = $this->checkNull($this->user_pass);
        $user_pass2 = $this->checkNull($this->user_pass2);
        $code = $this->checkNull($this->code);

        if ($user_login != $_SESSION['forget_mobile']) {
            return $this->renderError('手机号码不一致');
        }

        if ($code != $_SESSION['forget_mobile_code']) {
            return $this->renderError('验证码错误');
        }

        if ($user_pass != $user_pass2) {
            return $this->renderError('两次输入的密码不一致');
        }

        $check = Model_Common::getInstance()->passcheck($user_pass);
        if ($check == 0) {
            return $this->renderError('密码6-12位数字与字母');
        } else if ($check == 2) {
            return $this->renderError('密码不能纯数字或纯字母');
        }

        $domain = Model_Login::getInstance();
        $info = $domain->userFindPass($user_login, $user_pass);

        if ($info == 1006) {
            return $this->renderError('该帐号不存在');
        } else if ($info === false) {
            return $this->renderError('重置失败，请重试');
        }

        $_SESSION['forget_mobile'] = '';
        $_SESSION['forget_mobile_code'] = '';
        $_SESSION['forget_mobile_expiretime'] = '';

        return $this->renderObject();
    }

    /**
     * 第三方登录
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info.id 用户ID
     * @return string info.user_nicename 昵称
     * @return string info.avatar 头像
     * @return string info.avatar_thumb 头像缩略图
     * @return string info.sex 性别
     * @return string info.signature 签名
     * @return string info.coin 用户余额
     * @return string info.login_type 注册类型
     * @return string info.level 等级
     * @return string info.province 省份
     * @return string info.city 城市
     * @return string info.birthday 生日
     * @return string info.token 用户Token
     * @return string msg 提示信息
     */
    public function userLoginByThird()
    {
        $openid = $this->checkNull($this->openid);
        $type = $this->checkNull($this->type);
        $nicename = $this->checkNull($this->nicename);
        $avatar = $this->checkNull($this->avatar);

        $domain = Model_Login::getInstance();
        $info = $domain->userLoginByThird($openid, $type, $nicename, $avatar);
        if ($info == 1001) {
            return $this->renderError('该账号已被禁用');
        }

        return $this->renderObject($info);
    }

    /**
     * 获取找回密码短信验证码
     * @desc 用于找回密码获取短信验证码
     * @return int code 操作码，0表示成功,2发送失败
     * @return array info
     * @return string msg 提示信息
     */

    public function getForgetCode()
    {
        $mobile = $this->mobile;

        $ismobile = Model_Common::getInstance()->checkMobile($mobile);
        if (!$ismobile) {
            return $this->renderError('请输入正确的手机号');
        }

        if ($_SESSION['forget_mobile'] == $mobile && $_SESSION['forget_mobile_expiretime'] > time()) {
            return $this->renderError('验证码1分钟有效，请勿多次发送');
        }

        /* 发送验证码 */
        /*
        $mobile_code = $this->random(6, 1);
        $result=$this->sendCode($mobile,$mobile_code);
        if($result['code']===0){
            $_SESSION['forget_mobile'] = $mobile;
            $_SESSION['forget_mobile_code'] = $mobile_code;
            $_SESSION['forget_mobile_expiretime'] = time() +60*5;
        }else{
            $rs['code']=1002;
            $rs['msg']=$result['msg'];
        }
        */

        $_SESSION['forget_mobile'] = $mobile;
        $_SESSION['forget_mobile_code'] = '123456';
        $_SESSION['forget_mobile_expiretime'] = time() + 60 * 1;

        return $this->renderObject();
    }

    public function checkIsLogin()
    {
        $uid = $this->uid;

        $domain = Model_Login::getInstance();
        $info = $domain->checkIsLogin($uid);
        if ($info == 1) {
            $msg = "数据添加成功或已经存在";
        } else {
            $msg = "数据添加失败";
        }

        return $this->renderObject([
            'msg' => $msg,
        ]);
    }

    /**
     * 获取验证码
     */
    public function getVerificationCode()
    {
        $width = $this->checkNull($this->width, 120);
        $height = $this->checkNull($this->height, 40);

        $srv = Model_Login::getInstance();
        $data = $srv->getVerificationCode($width, $height);
        if (false === $data) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

}
