<?php

class Api_Live extends Api_Base
{
    public function getRules()
    {
        return [
            'createRoom' => [
                //PhalApi::IS_POST => true,
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID',],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token',],
                'user_nicename' => ['name' => 'user_nicename', 'type' => 'string', 'require' => true, 'desc' => '用户昵称 url编码',],
                'avatar' => ['name' => 'avatar', 'type' => 'string', 'require' => true, 'desc' => '用户头像 url编码',],
                'avatar_thumb' => ['name' => 'avatar_thumb', 'type' => 'string', 'require' => true, 'desc' => '用户小头像 url编码',],
                'title' => ['name' => 'title', 'type' => 'string', 'default' => '', 'desc' => '直播标题 url编码',],
                'province' => ['name' => 'province', 'type' => 'string', 'default' => '', 'desc' => '省份',],
                'city' => ['name' => 'city', 'type' => 'string', 'default' => '', 'desc' => '城市',],
                'lng' => ['name' => 'lng', 'type' => 'string', 'default' => '0', 'desc' => '经度值',],
                'lat' => ['name' => 'lat', 'type' => 'string', 'default' => '0', 'desc' => '纬度值',],
                'type' => ['name' => 'type', 'type' => 'int', 'default' => '0', 'desc' => '直播类型，0是普通直播，1是密码直播，2是收费直播，3是计时直播',],
                'type_val' => ['name' => 'type_val', 'type' => 'string', 'default' => '', 'desc' => '类型值',],
                'chat_num' => ['name' => 'chat_num', 'type' => 'string', 'default' => '30', 'desc' => '直播间发言字数',],
                'chat_frequency' => ['name' => 'chat_frequency', 'type' => 'string', 'desc' => '直播间发言频率',],
            ],
            'changeLive' => [
                //PhalApi::IS_POST => true,
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID',],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token',],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => false, 'desc' => '流名'],
                'status' => ['name' => 'status', 'type' => 'int', 'require' => true, 'desc' => '直播状态 0关闭 1直播',],
            ],
            'stopRoom' => [
                //PhalApi::IS_POST => true,
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID',],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token',],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => false, 'desc' => '流名'],
            ],
            'stopInfo' => [
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'],
            ],
            'checkLive' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'],
            ],
            'roomCharge' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'],
            ],
            'enterRoom' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'],
                'city' => ['name' => 'city', 'type' => 'string', 'default' => '', 'desc' => '城市'],
            ],
            'showVideo' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'liveuid' => ['name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'],
                'touid' => ['name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '上麦会员ID'],
            ],
            'getZombie' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'stream' => ['name' => 'stream', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '流名'],
            ],
            'getUserLists' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
            ],
            'getPop' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '对方ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
            ],
            'getGiftList' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
            ],
            'sendGift' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'touid' => ['name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'],
                'giftid' => ['name' => 'giftid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物ID'],
                'giftcount' => ['name' => 'giftcount', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物数量'],
                'giftGroupNum' => ['name' => 'giftGroupNum', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物组别数量'],
                'type' => ['name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '赠送礼物的类型，0没有提示直播券不足，1提示直播券不足'],
            ],
            'sendBarrage' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'touid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '送的对象用户ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'content' => ['name' => 'content', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '弹幕内容'],
            ],
            'setAdmin' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'liveuid' => ['name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'],
                'touid' => ['name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
            ],
            'getAdminList' => [
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID'],
            ],
            'setReport' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '对方ID'],
                'content' => ['name' => 'content', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '举报内容'],
            ],
            'getVotes' => [
                'liveuid' => ['name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'],
            ],
            'setShutUp' => [
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID'],
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '用户token'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '禁言用户ID'],
                'minutes' => ['name' => 'minutes', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '禁言时长（分钟）'],
            ],
            'kicking' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '对方ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID'],
            ],
            'superStopRoom' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'require' => true, 'min' => 1, 'desc' => '会员token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'type' => ['name' => 'type', 'type' => 'int', 'default' => 0, 'desc' => '关播类型 0表示关闭当前直播 1表示关闭当前直播并禁用账号'],
            ],
            'searchMusic' => [
                'key' => ['name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '关键词'],
                'p' => ['name' => 'p', 'type' => 'int', 'min' => 1, 'default' => 1, 'desc' => '页数'],
            ],
            'getDownurl' => [
                'audio_id' => ['name' => 'audio_id', 'type' => 'int', 'require' => true, 'desc' => '歌曲ID'],
            ],
            'getAuthKs' => [
                'uid' => ['name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'],
            ],
            'getChannelLive' => [
                'p' => ['name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'],
            ],
            'sendRedPackets' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'num' => ['name' => 'num', 'type' => 'int', 'require' => true, 'desc' => '红包个数'],
                'totalMoney' => ['name' => 'totalMoney', 'type' => 'int', 'require' => true, 'desc' => '红包总金额'],
                'title' => ['name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '标题或者口令'],
                'type' => ['name' => 'type', 'type' => 'int', 'min' => '0', 'require' => true, 'desc' => '红包类型，0普通红包，1口令红包'],
            ],
            'robRedPackets' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'sendRedUid' => ['name' => 'sendRedUid', 'type' => 'int', 'require' => true, 'min' => '1', 'desc' => '发送红包用户的ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'sendtime' => ['name' => 'sendtime', 'type' => 'string', 'require' => true, 'desc' => '发红包时间'],
                'type' => ['name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '红包类型'],
                'title' => ['name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '红包标题或口令'],
            ],
            'giftGroupLists' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
            ],
            'giftPK' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'guestID' => ['name' => 'guestID', 'type' => 'int', 'require' => true, 'min' => 1, 'desc' => '客队人ID'],
                'effectiveTime' => ['name' => 'effectiveTime', 'type' => 'int', 'require' => true, 'desc' => 'PK有效时间'],
                'masterGiftID' => ['name' => "masterGiftID", 'type' => 'int', 'require' => true, 'desc' => '主队礼物ID'],
                'guestGiftID' => ['name' => 'guestGiftID', 'type' => 'int', 'require' => true, 'desc' => '客队礼物ID'],
            ],
            'stopPK' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'pkID' => ['name' => 'pkID', 'type' => 'int', 'require' => true, 'min' => '1', 'desc' => 'pkID'],
            ],
            'createGrabBench' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'effectiveTime' => ['name' => 'effectiveTime', 'type' => 'int', 'require' => true, 'desc' => '抢板凳有效时间'],
                'winNums' => ['name' => 'winNums', 'type' => 'string', 'require' => true, 'desc' => '抢板凳的中奖号码'],
            ],
            'grabBench' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'liveuid' => ['name' => 'liveuid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '直播间ID'],
                'grabbenchID' => ['name' => 'grabbenchID', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '创建抢板凳游戏的ID'],
            ],
            'stopGrabBench' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'grabbenchID' => ['name' => 'grabbenchID', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '创建抢板凳游戏的ID'],
            ],
            'getCarouse' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'startGameUid' => ['name' => 'startGameUid', 'type' => 'int', 'require' => true, 'desc' => '开启游戏人ID'],
                'stream' => ['name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流地址'],
            ],
            'getBuyGuard' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
            ],
            'buyGuard' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '会员ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '对方ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID'],
            ],
            'checkisSuper' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
            ],
            'changeVest' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'touid' => ['name' => 'touid', 'type' => 'string', 'min' => '1', 'require' => true, 'desc' => '对方ID'],
                'vestid' => ['name' => 'vestid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '新卡的马甲ID'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID'],
            ],
            'channelGag' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
            ],
            'delChannelGap' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'live_type' => ['name' => 'live_type', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '直播类型',],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
            ],
            'setMultiManagerStatus' => [
                PhalApi::IS_POST => true,
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'token' => ['name' => 'token', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '用户token'],
                'liveid' => ['name' => 'liveid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '房间ID',],
                'vest_type' => ['name' => 'vest_type', 'type' => 'string', 'require' => true, 'min' => '1', 'desc' => '角色'],
                'status' => ['name' => 'status', 'type' => 'int', 'min' => 0, 'require' => true, 'desc' => '设置的状态：0-关闭，1-开启',],
            ],
        ];
    }

    /**
     * 创建开播
     * @desc 用于用户开播生成记录
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.userlist_time 用户列表请求间隔
     * @return string info.barrage_fee 弹幕价格
     * @return string info.votestotal 主播映票
     * @return string info.stream 流名
     * @return string info.push 推流地址
     * @return string info.chatserver socket地址
     * @return string info.pull_wheat 连麦播流地址
     * @return string msg 提示信息
     */
    public function createRoom()
    {
        $uid = $this->uid;
        $token = $this->checkNull($this->token);
        $user_nicename = $this->checkNull($this->user_nicename);
        $avatar = $this->checkNull($this->avatar);
        $avatar_thumb = $this->checkNull($this->avatar_thumb);
        $title = $this->checkNull($this->title);
        $province = $this->checkNull($this->province);
        $city = $this->checkNull($this->city);
        $lng = $this->checkNull($this->lng);
        $lat = $this->checkNull($this->lat);
        $type = (int)$this->type;
        $type_val = $this->checkNull($this->type_val);
        $chat_num = (int)$this->chat_num ?: 30;
        $chat_frequency = (int)$this->chat_frequency ?: 0;

        $srv = new Model_Live();
        $data = $srv->createRoom($uid, $token, $user_nicename, $avatar, $avatar_thumb,
            $title, $province, $city, $lng, $lat, $type, $type_val, $chat_num, $chat_frequency);

        if (false === $data) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject($data);
    }

    /**
     * 修改直播状态
     * @desc 用于主播修改直播状态
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.msg 成功提示信息
     * @return string msg 提示信息
     */
    public function changeLive()
    {
        $liveType = $this->live_type;
        $liveId = $this->liveid;
        $uid = $this->uid;
        $token = $this->checkNull($this->token);
        $stream = $this->checkNull($this->stream);

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $srv = new Model_Live();
        $data = $srv->changeLive($uid, $liveType, $liveId, $stream, $this->status);
        if (false === $data) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 关闭直播
     * @desc 用于用户结束直播
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.msg 成功提示信息
     * @return string msg 提示信息
     */
    public function stopRoom()
    {
        $liveType = $this->live_type;
        $liveId = $this->liveid;
        $uid = $this->uid;
        $token = $this->checkNull($this->token);
        $stream = $this->checkNull($this->stream);

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $srv = new Model_Live();
        $data = $srv->stopRoom($uid, $liveType, $liveId, $stream);
        if (false === $data) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 直播结束信息
     * @desc 用于直播结束页面信息展示
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.nums 人数
     * @return string info.length 时长
     * @return string info.votes 映票数
     * @return string msg 提示信息
     */
    public function stopInfo()
    {
        $liveType = $this->live_type;
        $liveId = $this->liveid;
        $stream = $this->checkNull($this->stream);

        $domain = new Model_Live();
        $info = $domain->stopInfo($liveType, $liveId, $stream);

        return $this->renderObject($info);
    }

    /**
     * 直播检查
     */
    public function checkLive()
    {
        $uid = $this->uid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $stream = trim($this->stream);

        $srv = new Model_Live();
        $data = $srv->checkLive($uid, $liveType, $liveId, $stream);
        if ($data === false) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject($data);
    }

    /**
     * 房间扣费
     */
    public function roomCharge()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $stream = $this->stream;

        $touid = $liveType ? 0 : $liveId;

        if (Model_Auth_User::getInstance()->isVisitor($uid)) {
            return $this->renderError('请登录');
        }

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->roomCharge($uid, $liveType, $liveId, $touid, $stream);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject($info);
    }

    /**
     * 进入直播间
     * @desc 用于用户进入直播
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.votestotal 直播映票
     * @return string info.barrage_fee 弹幕价格
     * @return string info.userlist_time 用户列表获取间隔
     * @return string info.chatserver socket地址
     * @return string info.isattention 是否关注主播，0表示未关注，1表示已关注
     * @return string info.nums 房间人数
     * @return string info.push_url 推流地址
     * @return string info.pull_url 播流地址
     * @return string info.showvideo 连麦用户ID，0表示未连麦
     * @return string info.showvideo_url 连麦播流地址
     * @return array info.userlists 用户列表
     * @return array info.game 押注信息
     * @return string info.gametime 游戏剩余时间
     * @return string info.gameid 游戏记录ID
     * @return string info.gameaction 游戏类型，1表示炸金花，2表示牛牛，3表示转盘
     * @return string info.chat_num 房间聊天字数
     * @return string info.chat_frequency 房间聊天频率
     * @return string msg 提示信息
     */
    public function enterRoom()
    {
        $uid = $this->uid;
        $token = trim($this->token);
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $city = trim($this->city);
        $stream = trim($this->stream);
        $dataFor = trim($_REQUEST['data_for']);

        $srv = Model_Live::getInstance();
        $info = $srv->enterRoom($uid, $token, $liveType, $liveId, $city, $stream, $dataFor);
        if (false === $info) {
            return $this->renderError($srv->getLastErrMsg(), 700);
        }

        return $this->renderObject($info);
    }

    /**
     * 连麦信息
     */
    public function showVideo()
    {
        $uid = $this->uid;
        $token = $this->checkNull($this->token);
        $liveuid = $this->liveuid;
        $touid = $this->touid;

        if ($uid != $liveuid) {
            return $this->renderError('您不是主播');
        }

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        DI()->redis->hset('ShowVideo', $liveuid, $touid);

        return $this->renderObject();
    }

    /**
     * 获取僵尸粉
     * @desc 用于获取僵尸粉
     * @return mixed
     * @return int code 操作码，0表示成功
     * @return array info 僵尸粉信息
     * @return string msg 提示信息
     */
    public function getZombie()
    {
        $uid = $this->uid;
        $stream = $this->checkNull($this->stream);

        $stream2 = explode('_', $stream);
        $liveuid = $stream2[0];
        $domain = new Model_Live();
        $iszombie = $domain->isZombie($liveuid);
        if ($iszombie == 0) {
            return $this->renderError('未开启僵尸粉');
        }

        $isvisit = DI()->redis->sIsMember($liveuid . '_zombie_uid', $uid);
        if ($isvisit) {
            return $this->renderError('用户已访问');
        }

        $times = DI()->redis->get($liveuid . '_zombie');
        if ($times && $times > 10) {
            return $this->renderError('次数已满');
        } else if ($times) {
            $times = $times + 1;
        } else {
            $times = 0;
        }

        DI()->redis->set($liveuid . '_zombie', $times);
        DI()->redis->sAdd($liveuid . '_zombie_uid', $uid);

        //用户列表
        $uidlist = Model_Live::getInstance()->getOnlineUserList(0, $uid) ?: [];
        $uids = Model_Common::getInstance()->array_column($uidlist, 'id');
        $uid = implode(",", $uids);

        $where = '0';
        if ($uid) {
            $where .= ',' . $uid;
        }
        $where = str_replace(",,", ',', $where);
        $where = trim($where, ",");
        $list = $domain->getZombie($stream, $where);
        $nums = Model_Live::getInstance()->getOnlineUserNum(0, $liveuid);

        return $this->renderObject([
            'list' => $list,
            'nums' => $nums,
        ]);
    }

    /**
     * 获取在线用户列表
     */
    public function getUserLists()
    {
        $uid = $this->uid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $list = Model_Live::getInstance()->getOnlineUserList($liveType, $liveId) ?: [];
        $nums = Model_Live::getInstance()->getOnlineUserNum($liveType, $liveId);
        foreach ($list as &$v) {
            $guard = Model_Guard::getInstance()->checkIsGuard($v['id'], $uid);
            $v['guard'] = $guard;
            $v['isguard'] = $guard['isGuard'];
            $v['vestID'] = (string)Model_Live::getInstance()->getUserVestId($v['id'], $liveType, $liveId);
        }

        return $this->renderObject([
            'userlist' => $list,
            'nums' => $nums,
            'votestotal' => Model_Live::getInstance()->getVotes($uid),
        ]);
    }

    /**
     * 直播间弹窗信息
     * @return mixed
     * @return int status 当前用户点击对方用户时返回的状态，0:选的比自己马甲等级低 1：同级或比自己大【举报功能】 2：超管对主播 【有关播，停播功能】 3：超管对超管【不举报】 4：自己对自己
     * @return string info.consumption 消费总数
     * @return string info.votestotal 票总数
     * @return string info.follows 关注数
     * @return string info.fans 粉丝数
     * @return string info.isattention 是否关注，0未关注，1已关注
     * @return string info.action 操作显示，0表示自己，30表示普通用户，40表示管理员，501表示主播设置管理员，502表示主播取消管理员，503表示主播操作超管，60表示超管管理主播，601表示超管操作普通用户，602表示超管操作管理员 特别注意: 503 601 602只是后来添加的action用来做标识没有实际操作(都是为私信服务的)
     * @return string info[1].uidIdentity 当前用户的身份 11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19：超管
     * @return string info[1].touidIdentity 被选用户的身份 11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19：超管
     */
    public function getPop()
    {
        $uid = $this->uid;
        $touid = $this->touid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $domain = new Model_Live();
        $info = $domain->getPop($touid, $liveType, $liveId);
        if (!$info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        $info['isattention'] = (string)Model_Auth_User::getInstance()->isAttention($uid, $touid, $liveType);
        if ($uid == $touid) {
            $info['action'] = '0';
        } else {
            $uid_admin = Model_Auth_User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
            $touid_admin = Model_Auth_User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
            if ($uid_admin == 40 && $touid_admin == 30) {
                $info['action'] = '40';
            } else if ($uid_admin == 50 && $touid_admin == 30) {
                $info['action'] = '501';
            } else if ($uid_admin == 50 && $touid_admin == 40) {
                $info['action'] = '502';
            } else if ($uid_admin == 50 && $touid_admin == 60) {
                $info['action'] = '503';
            } else if ($uid_admin == 60 && $touid_admin == 30) {
                $info['action'] = '601';
            } else if ($uid_admin == 60 && $touid_admin == 40) {
                $info['action'] = '602';
            } else if ($uid_admin == 60 && $touid_admin == 50) {
                $info['action'] = '60';
            } else {
                $info['action'] = '30';
            }
        }

        $uidIdentity = Model_Auth_User::getInstance()->oldUserIdentity($uid, $liveType, $liveId);
        $touidIdentity = Model_Auth_User::getInstance()->oldUserIdentity($touid, $liveType, $liveId);

        $status = "0";
        $vestOption = [];
        //私聊权限
        $vestOption['private_chat'] = (string)intval(Model_Live::getInstance()->getPrivateChatPermission($uid, $touid, $liveType, $liveId));

        if ($uid == $touid) {
            $status = "4";
            $vestOption['set_admin'] = "0";
            $vestOption['away_user'] = "0";
            $vestOption['away_admin'] = "0";
            $vestOption['gap'] = "0";
        } else {
            if ($uidIdentity == 19) {//当前用户是超管
                if ($touidIdentity == 18) {//对方是主播
                    $status = "2";
                    $vestOption['set_admin'] = "0";
                    $vestOption['away_user'] = "0";
                    $vestOption['away_admin'] = "0";
                    $vestOption['gap'] = "0";
                }
                if ($touidIdentity == 19) {//对方是超管
                    $status = "3";
                    $vestOption['set_admin'] = "0";
                    $vestOption['away_user'] = "0";
                    $vestOption['away_admin'] = "0";
                    $vestOption['gap'] = "0";
                }
                if ($touidIdentity >= 11 && $touidIdentity < 16) {//普通马甲
                    $vestOption['set_admin'] = "1";
                    $vestOption['away_user'] = "1";
                    $vestOption['away_admin'] = "1";
                    $vestOption['gap'] = "1";
                }
                if ($touidIdentity == 16 || $touidIdentity == 17) {//紫马或黑马
                    $vestOption['set_admin'] = "0";
                    $vestOption['away_user'] = "1";
                    $vestOption['away_admin'] = "1";
                    $vestOption['gap'] = "1";
                }
            } else if ($uidIdentity == 18) {//当前用户是主播
                if ($touidIdentity == 19) {//对方是超管
                    $status = "3";
                    $vestOption['set_admin'] = "0";
                    $vestOption['away_user'] = "0";
                    $vestOption['away_admin'] = "0";
                    $vestOption['gap'] = "0";
                }
                if ($touidIdentity == 16 || $touidIdentity == 17) {//对方是紫马或黑马
                    $vestOption['set_admin'] = "0";
                    $vestOption['away_user'] = "1";
                    $vestOption['away_admin'] = "1";
                    $vestOption['gap'] = "1";
                }
                if ($touidIdentity >= 11 && $touidIdentity < 16) {//对方是普通马甲
                    $vestOption['set_admin'] = "1";
                    $vestOption['away_user'] = "1";
                    $vestOption['away_admin'] = "1";
                    $vestOption['gap'] = "1";
                }
            } else {
                $vestOption = Model_Live::getInstance()->getUsersVest($uidIdentity - 10);
                //判断当前马甲和对方马甲的大小
                if ($uidIdentity > $touidIdentity) {//当前马甲大于对方的马甲
                    //读取当前用户的马甲权限
                    if ($uidIdentity == 12) {
                        $vestOption['set_admin'] = "0";
                    }
                    unset($vestOption['id']);
                    unset($vestOption['vest_name']);
                    unset($vestOption['vest_man_url']);
                    unset($vestOption['vest_woman_url']);
                    unset($vestOption['gap_all']);
                } else {//同级或比对方小
                    $vestOption['set_admin'] = "0";
                    unset($vestOption['id']);
                    unset($vestOption['vest_name']);
                    unset($vestOption['vest_man_url']);
                    unset($vestOption['vest_woman_url']);
                    unset($vestOption['gap_all']);
                    $status = "1";
                }
            }
        }

        //overload vest_id
        $info['vest_id'] = (string)Model_Live::getInstance()->getUserVestId($touid, $liveType, $liveId);
        $info['status'] = $status;
        $info['vestInfo'] = [
            'uidIdentity' => $uidIdentity,
            'touidIdentity' => $touidIdentity,
        ];
        $info['vestOption'] = $vestOption ?: (object)[];

        return $this->renderObject($info);
    }

    /**
     * 礼物列表
     */
    public function getGiftList()
    {
        $uid = $this->uid;

        $domain = new Model_Live();
        $giftlist = $domain->getGiftList();
        $domain2 = new Model_Auth_User();
        $coin = $domain2->getBalance($uid);
        $livecoin = $domain2->getLivecoin($uid);

        return $this->renderObject([
            'coin' => $coin,
            'livecoin' => $livecoin,
            'giftlist' => $giftlist,
        ]);
    }

    /**
     * 赠送礼物
     */
    public function sendGift()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $stream = $this->stream;
        $giftid = $this->giftid;
        $giftcount = $this->giftcount;
        $giftGroupNum = $this->giftGroupNum;
        $type = $this->type;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $result = $domain->sendGift($uid, $touid, $liveType, $liveId, $stream, $giftid, $giftcount, $giftGroupNum, $type);
        if (false === $result) {
            return $this->renderError($domain->getLastErrMsg());
        }

        DI()->redis->set($result['gifttoken'], json_encode($result));
        $data['gifttoken'] = $result['gifttoken'];
        $data['level'] = $result['level'];
        $data['coin'] = $result['coin'];
        $data['iswin'] = strval($result['iswin']);
        $data['wincoin'] = strval($result['winCoin']);
        $data['livecoin'] = strval($result['livecoin']);
        $data['giftname'] = strval($result['giftname']);
        $data['winMsg'] = "恭喜您中得" . $result['winLiveCoin'] . "直播券！";

        return $this->renderObject($data);
    }

    /**
     * 发送弹幕
     */
    public function sendBarrage()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $content = trim($this->content);

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $result = $domain->sendBarrage($uid, $touid, $liveType, $liveId, $content);
        if (false === $result) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject($result);
    }

    /**
     * 设置/取消管理员 (isadmin 是否是管理员，0表示不是管理员，1表示是管理员)
     */
    public function setAdmin()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveuid = $this->liveuid;
        $touid = $this->touid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->setAdmin($uid, $liveuid, $touid);
        if (false == $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'isadmin' => $info,
        ]);
    }

    /**
     * 管理员列表
     */
    public function getAdminList()
    {
        $domain = new Model_Live();
        $info = $domain->getAdminList($this->live_type, $this->liveid);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderArray($info);
    }

    /**
     * 用户举报
     */
    public function setReport()
    {
        $uid = $this->uid;
        $touid = $this->touid;
        $token = $this->token;
        $content = trim($this->content);

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }
        if (!$content) {
            return $this->renderError('举报内容不能为空');
        }

        $domain = new Model_Live();
        $info = $domain->setReport($uid, $touid, $content);
        if ($info === false) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 主播映票
     */
    public function getVotes()
    {
        $domain = new Model_Live();
        $info = $domain->getVotes($this->liveuid);
        return $this->renderObject($info);
    }

    /**
     * 禁言
     */
    public function setShutUp()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $minutes = $this->minutes;
        $liveType = $this->live_type;
        $liveId = $this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('token已过期，请重新登陆', 700);
        }

        $rs = Model_Live::getInstance()->gag($liveType, $liveId, $uid, $touid, $minutes);
        if (false === $rs) {
            return $this->renderError(Model_Live::getInstance()->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 直播间踢人
     */
    public function kicking()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $liveType = $this->live_type;
        $liveId = $this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $rs = Model_Live::getInstance()->kicking($uid, $touid, $liveType, $liveId);
        if (false === $rs) {
            return $this->renderError(Model_Live::getInstance()->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 超管关播
     */
    public function superStopRoom()
    {
        $domain = new Model_Live();
        $rs = $domain->superStopRoom($this->uid, $this->token, $this->live_type, $this->liveid, $this->type);
        if (false === $rs) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 歌曲查询（百度音乐）
     */
    public function searchMusic()
    {
        $key = trim($this->key);
        if (!$key) {
            return $this->renderError('缺参数');
        }

        $url = 'http://musicmini.baidu.com/app/search/searchList.php?qword=' . $key . '&ie=utf-8&page=' . $p;
        $ch = curl_init();
        $timeout = 10;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $contents = curl_exec($ch);
        curl_close($ch);

        $p = "/<table[^>]+>(.+)<\/table>/sU"; //表格部分代码
        preg_match_all($p, $contents, $img);

        $p2 = '/<tr.*>.*<td class="sName.*key="(.+)".*>.*<a href="javascript:;" onclick="playSong\((.+),.*\)".*>.*<\/a>.*<\/td>.*<td class="uName.*key="(.+)".*>.*<\/td>.*<\/tr>/sU';
        preg_match_all($p2, $img[0][0], $m, PREG_SET_ORDER);
        $info = [];
        foreach ($m as $k => $v) {
            $info[$k]['audio_id'] = str_replace("&#039;", '', $v[2]);
            $info[$k]['audio_name'] = $v[1];
            $info[$k]['artist_name'] = $v[3];
        }

        return $this->renderArray($info);
    }

    /**
     * 歌曲信息（百度音乐）
     */
    public function getDownurl()
    {
        $audioId = trim($this->audio_id);
        if (!$audioId) {
            return $this->renderError('歌曲不存在');
        }

        $url = "http://music.baidu.com/data/music/links?songIds={$audioId}";
        $ch = curl_init();
        $timeout = 10;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $contents = curl_exec($ch);
        curl_close($ch);

        $body = json_decode($contents, true);
        $errorCode = $body['errorCode'];
        $songList = $body['data']['songList'];

        if ($errorCode != 22000 || !$songList) {
            return $this->renderError('歌曲不存在');
        }

        $info = [];
        $info['audio_link'] = $songList[0]['songLink'];
        $info['audio_ext'] = $songList[0]['format'];
        $info['time_len'] = $songList[0]['time'];
        $info['audio_size'] = $songList[0]['size'];
        $info['lrcLink'] = $songList[0]['lrcLink'];

        return $this->renderObject($info);
    }

    /**
     * 金山云鉴权
     */
    public function getAuthKs()
    {
        $uid = $this->uid;

        $configpri = Model_Common::getInstance()->getConfigPri();
        $ak = $configpri['ks_ak'];
        $sk = $configpri['ks_sk'];
        $uniqname = $configpri['ks_uniqname'];

        $time = time() + 60 * 60 * 5;
        $nonce = md5('yunbao' . time());
        $vdoid = 'yunbao';
        $resource = "nonce={$nonce}&uid={$uid}&uniqname={$uniqname}&vdoid={$vdoid}";
        $signature = base64_encode(hash_hmac('sha1', "GET\n{$time}\n{$resource}", $sk, true));
        $info['url'] = $configpri['ks_url'];
        $info['uniqname'] = $uniqname;
        $info['authString'] = "accesskey={$ak}&expire={$time}&{$resource}&signature={$signature}";

        return $this->renderObject($info);
    }

    /**
     * 获取频道第一条直播信息
     */
    public function getFirstLive()
    {
        $domain = new Model_Live();
        $info = $domain->getFirstLive();
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject($info);
    }

    /**
     * 获取频道直播列表
     */
    public function getChannelLive()
    {
        $domain = new Model_Live();
        $list = $domain->getChannelLive($this->p);

        return $this->renderArray($list);
    }

    /**
     * 发红包
     */
    public function sendRedPackets()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $num = $this->num;
        $totalMoney = $this->totalMoney;
        $title = $this->title;
        $type = $this->type;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->sendRedPackets($uid, $liveType, $liveId, $num, $totalMoney, $title, $type);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        $user = Model_Auth_User::getInstance()->getUserInfo($uid);
        $user['live_type'] = $liveType;
        $user['liveid'] = strval($liveId);
        $user['liveuid'] = strval($liveId);
        $user['uid'] = $user['id'];
        $user['sendtime'] = $info;
        $user['title'] = $title;
        $user['sendRedUid'] = $user['id'];
        $user['redPacketType'] = strval($type);

        return $this->renderObject($user);
    }

    /**
     * 抢红包
     */
    public function robRedPackets()
    {
        $uid = $this->uid;
        $token = $this->token;
        $sendRedUid = $this->sendRedUid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;
        $sendtime = $this->sendtime;
        $type = $this->type;
        $title = $this->title;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        if (!$sendRedUid || $sendRedUid < 1) {
            return $this->renderError('无法抢红包');
        }

        $domain = new Model_Live();
        $info = $domain->robRedPackets($uid, $liveType, $liveId, $sendRedUid, $sendtime, $type, $title);

        return $this->renderObject($info);
    }

    /**
     * 直播间礼物列表显示分组
     */
    public function giftGroupLists()
    {
        $uid = $this->uid;
        $token = $this->token;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->giftGroupLists();

        return $this->renderArray($info);
    }

    /**
     * 直播间礼物PK
     */
    public function giftPK()
    {
        $uid = $this->uid;
        $token = $this->token;
        $guestID = $this->guestID;
        $effectiveTime = $this->effectiveTime;
        $masterGiftID = $this->masterGiftID;
        $guestGiftID = $this->guestGiftID;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录');
        }

        if ($effectiveTime < 30) {
            return $this->renderError('有效时间应该大于30秒');
        }

        if ($effectiveTime > 3600) {
            return $this->renderError('有效时间应该小于3600秒');
        }

        if ($uid == $guestID) {
            return $this->renderError('客队ID不能跟主队一样');
        }

        if ($masterGiftID == $guestGiftID) {
            return $this->renderError('主队和客队礼物不能相同');
        }

        //获取主队人的信息
        $masterUserInfo = Model_Auth_User::getInstance()->getUserInfo($uid);
        //获取客队人的信息
        $guestUserInfo = Model_Auth_User::getInstance()->getUserInfo($guestID);

        //判断客队人信息
        if (!$guestUserInfo) {
            return $this->renderError('客队主播不存在');
        }

        $domain = new Model_Live();
        $info = $domain->giftPK($uid, $guestID, $effectiveTime, $masterGiftID, $guestGiftID);
        if (!$info) {
            return $this->renderError('发起礼物PK失败');
        }

        //获取主队礼物信息
        $masterGiftInfo = Model_Gift::getInstance()->getGiftInfo($masterGiftID);
        //获取客队礼物信息
        $guestGiftInfo = Model_Gift::getInstance()->getGiftInfo($guestGiftID);

        $info['msg'] = "成功发起礼物PK";
        $info['guestName'] = $guestUserInfo['user_nicename'];//客队昵称
        $info['guestAvatar'] = $guestUserInfo['avatar'];//客队头像
        $info['masterGiftImg'] = $masterGiftInfo['gifticon'];//主队礼物图片
        $info['guestGiftImg'] = $guestGiftInfo['gifticon'];//主队礼物图片
        $info['masterAvatar'] = $masterUserInfo['avatar'];//主队头像
        $info['masterName'] = $masterUserInfo['user_nicename'];//主队昵称
        $info['masterGiftID'] = strval($masterGiftID);//主队礼物id
        $info['guestGiftID'] = strval($guestGiftID);//客队礼物id
        $info['pkID'] = strval($info);//PK记录id
        $info['effectiveTime'] = strval($effectiveTime);//PK有效时间

        return $this->renderObject($info);
    }

    /**
     * 直播间礼物PK结束
     * @desc 用于直播间礼物pk结束
     * @return string info['masterUserNicename'] 主队用户昵称
     * @return string info['masterFirstUserNicename'] 主队送礼最多人的昵称
     * @return int info['masterFirstUserCoin'] 主队送礼最多人的赠送总数
     * @return int info['guestUserNicename'] 客队用户昵称
     * @return string info['guestFirstUserNicename'] 客队送礼最多人的昵称
     * @return int info['guestFirstUserCoin'] 客队送礼最多人的赠送总数
     * @return int info['winType'] 双方PK的结果，0代表平局，1代表主队胜，2代表客队胜
     */
    public function stopPK()
    {
        $uid = $this->uid;
        $token = $this->token;
        $pkID = $this->pkID;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->stopPK($uid, $pkID);

        return $this->renderObject($info);
    }

    /**
     * 主播创建抢板凳游戏
     * @desc 用于主播创建抢板凳游戏
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return int grabbenchID 抢板凳记录ID
     * @return int hits_space 同一用户连续两次点击时间间隔
     */
    public function createGrabBench()
    {
        $uid = $this->uid;
        $token = $this->token;
        $effectiveTime = $this->effectiveTime;
        $winNums = $this->winNums;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        if ($effectiveTime < 30) {
            return $this->renderError('游戏有效时间要大于30秒');
        }

        if ($effectiveTime > 3600) {
            return $this->renderError('游戏有效时间要小于3600秒');
        }

        $domain = new Model_Live();
        $info = $domain->createGrabBench($uid, $effectiveTime, $winNums);
        if ($info == 0) {
            return $this->renderError('游戏创建失败');
        }

        return $this->renderObject($info);
    }

    /**
     * 用户点击抢板凳活动
     * @desc 用于用户点击抢板凳活动
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return string info 抢到的号码
     */
    public function grabBench()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveuid = $this->liveuid;
        $grabbenchID = $this->grabbenchID;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->grabBench($uid, $liveuid, $grabbenchID);
        if ($info == -1) {
            return $this->renderError('抢板凳游戏已经结束');
        }

        if ($info == 0) {//该用户已经中奖
            return $this->renderError('您已中奖，不能再抢');
        }

        $arr = [];
        if ($info > 0) {
            $arr['num'] = strval($info);
        }

        return $this->renderObject($arr);
    }

    /**
     * 抢板凳活动结束
     * @desc 用于抢板凳活动结束
     * @return mixed
     * @return int code 状态码，0表示成功
     * @return msg string 提示信息
     * @return array info 中奖号码和中奖人昵称
     * @return int info.num 中奖号码
     * @return string info.user_nicename 中奖人昵称
     */
    public function stopGrabBench()
    {
        $uid = $this->uid;
        $token = $this->token;
        $grabbenchID = $this->grabbenchID;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->stopGrabBench($uid, $grabbenchID);
        if ($info == -1) {
            $info = [];
        }

        return $this->renderArray($info);
    }

    /**
     * @desc 用于转盘游戏
     * @return mixed
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 数组信息
     * @return int info.nums 转盘总分类数
     * @return int info.randNum 随机的中奖号码
     * @return string info.imgUrl 后台配置的转盘图片
     */
    public function getCarouse()
    {
        $uid = $this->uid;
        $token = $this->token;
        $startGameUid = $this->startGameUid;
        $stream = $this->stream;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        if ($startGameUid == "") {
            return $this->renderError('请填写开启游戏用户ID');
        }

        if ($uid == $startGameUid) {
            return $this->renderError('用户不能是自己');
        }

        $startUserInfo = Model_Auth_User::getInstance()->getUserInfo($startGameUid);
        if ($startUserInfo) {
            $list = Model_Live::getInstance()->getOnlineUserList(0, $uid);
            //判断该用户是否在本房间内
            $inRoom = 0;
            foreach ($list as $v) {
                if ($v['id'] == $startGameUid) {
                    $inRoom = 1;
                    break;
                }
            }

            if ($inRoom == 1) {
                $domain = new Model_Live();
                $info = $domain->getCarouse();
                if ($info == -1) {
                    return $this->renderError('暂无转盘游戏');
                }
            } else {
                return $this->renderError('该用户不在本房间');
            }
        } else {
            return $this->renderError('该用户不存在');
        }

        $info['startGameUid'] = strval($startGameUid);

        return $this->renderObject($info);
    }

    /**
     * 购买守护时返回信息
     */
    public function getBuyGuard()
    {
        $uid = $this->uid;
        $token = $this->token;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->getBuyGuard($uid);

        return $this->renderObject($info);
    }

    /**
     * 用户购买守护
     */
    public function buyGuard()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $liveType = $this->live_type;
        $liveId = $this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->buyGuard($uid, $touid, $liveType, $liveId);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject($info);
    }

    /**
     * 检测用户是否是超管
     */
    public function checkisSuper()
    {
        $uid = $this->uid;

        $domain = new Model_Live();
        $info = $domain->checkisSuper($uid);

        return $this->renderObject([
            'issuper' => $info,
        ]);
    }

    /**
     * 获取马甲列表
     */
    public function getVestLists()
    {
        $domain = new Model_Live();
        $info = $domain->getVestLists();

        return $this->renderArray($info);
    }

    /**
     * 更换马甲
     */
    public function changeVest()
    {
        $uid = $this->uid;
        $token = $this->token;
        $touid = $this->touid;
        $vestid = $this->vestid;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->changeVest($uid, $touid, $vestid, $liveType, $liveId);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        $vestInfo = Model_Live::getInstance()->getUsersVest($vestid);
        $vestInfo['vest_man_url'] = Model_Helper_Func::getUrl($vestInfo['vest_man_url']);
        $vestInfo['vest_woman_url'] = Model_Helper_Func::getUrl($vestInfo['vest_woman_url']);

        return $this->renderObject($vestInfo);
    }

    /**
     * 频道禁言
     */
    public function channelGag()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->channelGag($liveType, $liveId, $uid);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'msg' => $info . '将房间禁言啦',
        ]);
    }

    /**
     * 房间解除禁言
     */
    public function delChannelGap()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveType = (int)$this->live_type;
        $liveId = (int)$this->liveid;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $domain = new Model_Live();
        $info = $domain->delChannelGap($uid, $liveType, $liveId);
        if (false === $info) {
            return $this->renderError($domain->getLastErrMsg());
        }

        return $this->renderObject([
            'msg' => $info . '将频道解除禁言',
        ]);
    }

    /**
     * 开始/关闭直播,开始/关闭喊麦API
     */
    public function setMultiManagerStatus()
    {
        $uid = $this->uid;
        $token = $this->token;
        $liveId = (int)$this->liveid;
        $vestType = trim($this->vest_type);
        $status = (int)$this->status;

        $checkToken = $this->checkToken($uid, $token);
        if ($checkToken == 700) {
            return $this->renderError('请重新登录', 700);
        }

        $srv = Model_MutiShow::getInstance();
        $info = $srv->setMultiManagerStatus($uid, $liveId, $vestType, $status);
        if (false === $info) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

}
