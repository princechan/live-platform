<?php

/**
 * 鉴于项目的特殊性，后期基类定义的方法只能为protected|private
 */
class Api_Base extends PhalApi_Api
{
    protected function renderObject($info = [])
    {
        if (empty($info)) {
            $info = (object)[];
        }

        return $this->render($info);
    }

    protected function renderArray($info = [])
    {
        if (empty($info)) {
            $info = [];
        }

        return $this->render($info);
    }

    protected function renderError($msg, $code = 1)
    {
        return $this->render([], $code, $msg);
    }

    private function render($info = [], $code = 0, $msg = '')
    {
        return [
            'code' => $code,
            'msg' => $msg,
            'info' => $info,
        ];
    }

    /**
     * 去除NULL 判断空处理 主要针对字符串类型
     * @param $value
     * @param string $default
     * @return string
     */
    protected function checkNull($value, $default = '')
    {
        $value = Model_Helper_Func::filterEmoJi(urldecode(htmlspecialchars(trim($value))));
        if (strstr($value, 'null') || (!$value && $value != 0)) {
            $value = '';
        }

        return $value ?: $default;
    }

    /**
     * validate token
     * @param $uid
     * @param $token
     * @return int
     */
    protected function checkToken($uid, $token)
    {
        return (new Model_Common)->checkToken($uid, $token);
    }

    protected function userCheck()
    {

    }

    public function __construct()
    {
        DI()->redis = Model_Helper_Redis::getInstance();
    }

}
