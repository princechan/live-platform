<?php

class Api_Charge extends Api_Base
{
    public function getRules()
    {
        return [
            'getAliOrder' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'changeid' => ['name' => 'changeid', 'type' => 'int', 'require' => true, 'desc' => '充值规则ID'],
                'coin' => ['name' => 'coin', 'type' => 'string', 'require' => true, 'desc' => '钻石'],
                'money' => ['name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'],
            ],
            'getWxOrder' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'changeid' => ['name' => 'changeid', 'type' => 'string', 'require' => true, 'desc' => '充值规则ID'],
                'coin' => ['name' => 'coin', 'type' => 'string', 'require' => true, 'desc' => '钻石'],
                'money' => ['name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'],
            ],
            'getIosOrder' => [
                'uid' => ['name' => 'uid', 'type' => 'string', 'require' => true, 'desc' => '用户ID'],
                'changeid' => ['name' => 'changeid', 'type' => 'string', 'require' => true, 'desc' => '充值规则ID'],
                'coin' => ['name' => 'coin', 'type' => 'string', 'require' => true, 'desc' => '钻石'],
                'money' => ['name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'],
            ],
        ];
    }

    /**
     * 微信支付
     * @desc 用于 微信支付 获取订单号
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info支付信息
     * @return string msg 提示信息
     */
    public function getWxOrder()
    {
        $uid = $this->uid;
        $changeid = $this->changeid;
        $coin = $this->checkNull($this->coin);
        $money = $this->checkNull($this->money);

        $orderid = Model_Charge::getInstance()->genOrderId($uid);
        $type = 2;

        if ($coin == 0) {
            return $this->renderError('信息错误');
        }

        $configpri = Model_Common::getInstance()->getConfigPri();
        $configpub = Model_Common::getInstance()->getConfigPub();

        //配置参数检测
        if ($configpri['wx_appid'] == "" || $configpri['wx_mchid'] == "" || $configpri['wx_key'] == "") {
            return $this->renderError('微信未配置');
        }

        $orderinfo = [
            "uid" => $uid,
            "touid" => $uid,
            "money" => $money,
            "coin" => $coin,
            "orderno" => $orderid,
            "type" => $type,
            "status" => 0,
            "addtime" => time(),
        ];

        $info = Model_Charge::getInstance()->createOrder($changeid, $orderinfo);
        if ($info == 1003) {
            return $this->renderError('订单信息有误，请重新提交');
        } else if (!$info) {
            return $this->renderError('订单生成失败');
        }

        $noceStr = md5(rand(100, 1000) . time());
        $paramarr = [
            "appid" => $configpri['wx_appid'],
            "body" => "充值{$coin}虚拟币",
            "mch_id" => $configpri['wx_mchid'],
            "nonce_str" => $noceStr,
            "notify_url" => $configpub['site'] . '/Appapi/pay/notify_wx',
            "out_trade_no" => $orderid,
            "total_fee" => $money * 100,
            "trade_type" => "APP",
        ];
        $sign = $this->sign($paramarr, $configpri['wx_key']);//生成签名
        $paramarr['sign'] = $sign;
        $paramXml = "<xml>";
        foreach ($paramarr as $k => $v) {
            $paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
        }
        $paramXml .= "</xml>";

        $ch = curl_init();
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
        @curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_POST, 1);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
        @$resultXmlStr = curl_exec($ch);
        if (curl_errno($ch)) {
            file_put_contents('./wxpay.txt', date('y-m-d H:i:s') . ' 提交参数信息 ch:' . json_encode(curl_error($ch)) . "\r\n", FILE_APPEND);
        }
        curl_close($ch);

        $result2 = $this->xmlToArray($resultXmlStr);

        $time2 = time();
        $prepayid = $result2['prepay_id'];
        $noceStr = md5(rand(100, 1000) . time());
        $paramarr2 = [
            "appid" => $configpri['wx_appid'],
            "noncestr" => $noceStr,
            "package" => "Sign=WXPay",
            "partnerid" => $configpri['wx_mchid'],
            "prepayid" => $prepayid,
            "timestamp" => $time2,
        ];
        $paramarr2["sign"] = $this->sign($paramarr2, $configpri['wx_key']);

        return $this->renderObject($paramarr2);
    }

    /**
     * sign拼装获取
     */
    public function sign($param, $key)
    {
        $sign = "";
        foreach ($param as $k => $v) {
            $sign .= $k . "=" . $v . "&";
        }
        $sign .= "key=" . $key;
        $sign = strtoupper(md5($sign));
        return $sign;
    }

    /**
     * xml转为数组
     */
    public function xmlToArray($xmlStr)
    {
        $postStr = $xmlStr;
        $msg = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        return $msg;
    }

    /**
     * 支付宝支付
     * @desc 用于支付宝支付 获取订单号
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.orderid 订单号
     * @return string msg 提示信息
     */
    public function getAliOrder()
    {
        $uid = $this->uid;
        $changeid = $this->changeid;
        $coin = $this->checkNull($this->coin);
        $money = $this->checkNull($this->money);

        $orderid = Model_Charge::getInstance()->genOrderId($uid);
        $type = 1;

        if ($coin == 0) {
            return $this->renderError('信息错误');
        }

        $orderinfo = [
            "uid" => $uid,
            "touid" => $uid,
            "money" => $money,
            "coin" => $coin,
            "orderno" => $orderid,
            "type" => $type,
            "status" => 0,
            "addtime" => time(),
        ];

        $info = Model_Charge::getInstance()->createOrder($changeid, $orderinfo);
        if ($info == 1003) {
            return $this->renderError('订单信息有误，请重新提交');
        } else if (!$info) {
            return $this->renderError('订单生成失败');
        }

        return $this->renderObject([
            'orderid' => $orderid,
        ]);
    }

    /**
     * 苹果支付
     * @desc 用于苹果支付 获取订单号
     * @return int code 操作码，0表示成功
     * @return array info
     * @return string info.orderid 订单号
     * @return string msg 提示信息
     */
    public function getIosOrder()
    {
        $uid = $this->uid;
        $changeid = $this->changeid;
        $coin = $this->checkNull($this->coin);
        $money = $this->checkNull($this->money);

        $orderid = Model_Charge::getInstance()->genOrderId($uid);
        $type = 3;

        if ($coin == 0) {
            return $this->renderError('信息错误');
        }

        $orderinfo = [
            "uid" => $uid,
            "touid" => $uid,
            "money" => $money,
            "coin" => $coin,
            "orderno" => $orderid,
            "type" => $type,
            "status" => 0,
            "addtime" => time(),
        ];

        $info = Model_Charge::getInstance()->createOrder($changeid, $orderinfo);
        if ($info == 1003) {
            return $this->renderError('订单信息有误，请重新提交');
        } else if (!$info) {
            return $this->renderError('订单生成失败');
        }

        return $this->renderObject([
            'orderid' => $orderid,
        ]);
    }

}
