<?php
/**
 * 分库分表的自定义数据库路由配置
 */

return [
    /**
     * DB数据库服务器集群
     */
    'servers' => [
        'db_appapi' => [
            'host' => DATABASE_HOST,
            'name' => DATABASE_NAME,
            'user' => DATABASE_USER,
            'password' => DATABASE_PWD,
            'port' => DATABASE_PORT,
            'charset' => 'UTF8',
        ],
    ],
    /**
     * 自定义路由表
     */
    'tables' => [
        '__default__' => [
            'prefix' => DATABASE_PREFIX,
            'key' => 'id',
            'map' => [
                ['db' => 'db_appapi'],
            ],
        ],
    ],
];
