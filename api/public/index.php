<?php

include_once '../../data/conf/bootstrap.php';

define('API_PATH', dirname(dirname(__FILE__)));
define('WEB_PATH', dirname(dirname(dirname(__FILE__))) . '/application');

require_once dirname(__FILE__) . '/init.php';
require_once dirname(__FILE__) . '/qiniucdn/Pili_v2.php';
//装载你的接口
DI()->loader->addDirs('Appapi');
$api = new PhalApi();
$rs = $api->response();
$rs->output();
