#!/bin/bash

find ./a04/public/home/js/ -name 'eventListen.js' | xargs perl -pi -e 's|https://www.testzhibo.com:19967|https://im.xsm7.com:19967|g'
echo '[8].Replace /a04/IM/s1.js Https Path and Keys'
find ./a04/IM/ -name 's1.js' | xargs perl -pi -e 's|/data/IM/keys/testzhibo.com|/usr/local/IM/keys/im.xsm7.com|g'
echo '[12].Copy upload and image files to ./a04/data/upload'
cp -R /data/wwwroot/live/data/upload ./a04/data/
echo '[13].Backup file live to 'live-`date "+%Y-%m-%d"`
mv live live-`date "+%Y-%m-%d"`
echo '[14].Rename a04 to live'
mv a04 live
find live/data/conf/ -name 'bootstrap.php' | xargs perl -pi -e 's|app_key|a04|g'
cp -rf live/data/conf-prod/a04/config.php live/data/conf/
echo '[15].Change assets version'
php /data/wwwroot/live/index.php cli/sys/clearCache
echo '[16].Change live own & mod'
chown -R www:www live
find live -type d -exec chmod 766 {} \;
find live -type f -exec chmod 644 {} \;
