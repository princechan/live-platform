#!/bin/bash

echo '[1].Unzip file to live-platform'
unzip -o -q live-platform.zip 
echo '[2].Replace /live-platform/api/Config/app.php'
find ./live-platform/api/Config/ -name 'app.php' | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
find ./live-platform/api/Config/ -name 'app.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
echo '[3].Replace /live-platform/api/Config/dbs.php'
find ./live-platform/api/Config/ -name 'dbs.php' | xargs perl -pi -e 's|db.xxx.xxx|192.168.1.3|g'
find ./live-platform/api/Config/ -name 'dbs.php' | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
find ./live-platform/api/Config/ -name 'dbs.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
echo '[4].Replace /live-platform/data/conf/dbs.php'
find ./live-platform/data/conf/ -name 'db.php' | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
find ./live-platform/data/conf/ -name 'db.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
echo '[5].Replace /live-platform/alipay/alipay_app/notify_url.php'
find ./live-platform/alipay/alipay_app/ -name 'notify_url.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
find ./live-platform/alipay/alipay_app/ -name 'notify_url.php' | xargs perl -pi -e 's|db.xxx.xxx|192.168.1.3|g'
find ./live-platform/alipay/alipay_app/ -name 'notify_url.php' | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
echo '[6].Replace /live-platform/wxpay/pay/notify_jsapi.php'
find ./live-platform/wxpay/pay/ -name 'notify_jsapi.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
find ./live-platform/wxpay/pay/ -name 'notify_jsapi.php' | xargs perl -pi -e 's|db.xxx.xxx|192.168.1.3|g'
find ./live-platform/wxpay/pay/ -name 'notify_jsapi.php' | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
echo '[7].Replace /live-platform/wxshare/Application/Common/Conf/config.php'
find ./live-platform/wxshare/Application/Common/Conf/ -name 'config.php' | xargs perl -pi -e 's|db.xxx.xxx|192.168.1.3|g'
find ./live-platform/wxshare/Application/Common/Conf/ -name 'config.php'  | xargs perl -pi -e 's|10.71.42.70|192.168.1.3|g'
find ./live-platform/wxshare/Application/Common/Conf/ -name 'config.php' | xargs perl -pi -e 's|tieweishivps|73x62rkEp4X|g'
find ./live-platform/public/home/js/ -name 'eventListen.js' | xargs perl -pi -e 's|https://www.testzhibo.com:19967|https://im.xsm7.com:19967|g'
echo '[8].Replace /live-platform/IM/s1.js Https Path and Keys'
find ./live-platform/IM/ -name 's1.js' | xargs perl -pi -e 's|/data/IM/keys/testzhibo.com|/usr/local/IM/keys/im.xsm7.com|g'
echo '[9].Replace /public/home/hxChat/js/easemob.im.config.js for xiaoshimei AppKey'
find ./live-platform/public/home/hxChat/js/ -name 'easemob.im.config.js' | xargs perl -pi -e 's|1150171228115205#yunbaodemo|1123180125178876#xiaoshimei|g'
find ./live-platform//public/js/imPackage/static/js/ -name 'webim.config.js' | xargs perl -pi -e 's|1150171228115205#yunbaodemo|1123180125178876#xiaoshimei|g'
echo '[10].Replace /public/home/show/js/rtc.js for Agora AppKey'
find ./live-platform/public/home/show/js/ -name 'rtc.js' | xargs perl -pi -e 's|e649a576c3df4b5981c342cbc5a47b9a|57b76d568b4f4b6fbb7fbffe85cb579a|g'
echo '[11].Copy file IM dir to IM service(103.70.226.157)/user/local/IM/s1-new.js'
scp -r ./live-platform/IM/s1.js root@192.168.1.2:/usr/local/IM/keys/
echo '[12].Copy upload and image files to ./live-platform/data/upload'
cp -R /data/wwwroot/live/data/upload ./live-platform/data/
echo '[13].Backup file live to 'live-`date "+%Y-%m-%d"`
mv live live-`date "+%Y-%m-%d"`
echo '[14].Rename live-platform to live'
mv live-platform live
echo '[15].Change assets version'
php /data/wwwroot/live/index.php cli/sys/clearCache
echo '[16].Change live own & mod'
chown -R www:www live
find live -type d -exec chmod 766 {} \;
find live -type f -exec chmod 644 {} \;
