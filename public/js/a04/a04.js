/**
 * noah.g
 * a04 尊龙 兼容脚本
 */
// 更换logo
$(".hd-logo")
  .find("img")
  .attr("src", "/public/images/a04/a04-logo.png");

// 提示修改为g开头
$(".hd-login")
  .find(".name")
  .find("label")
  .text("账号以m开头");

// 兼容直播按钮样式渲染
$('.homelink').hide();
window.onload = function () {
  // 根据视频区域的dom判断是不是直播室
  if (document.querySelector("#LF-area-video") !== null) {
    // 修改logo位置
    $(".act").css("background", "inherit");
    $(".container").css({
      width: "80%",
      float: "left",
      marginLeft: "25px"
    });
  }
  // 兼容直播按钮样式渲染
  $('.homelink').show();
  this.setTimeout(function () {
    // 登录窗修改为g开头
    $(".js_boxlogin_users_input").attr(
      "placeholder",
      "输入用户名(m开头)/手机号码"
    );
    // 账号密码长度修改
    $(".js_login_users_input").attr("maxlength", "11");
    $(".js_login_pass_input").attr("maxlength", "16");
    $(".js_boxlogin_users_input").attr("maxlength", "11");
    $(".js_boxlogin_pass_input").attr("maxlength", "16");
    $(".js_reg_users_input").attr("maxlength", "11");
    $(".js_reg_pass_input").attr("maxlength", "16");
  }, 2000);
};

$(function () {
  // 根据视频区域的dom判断是不是直播室
  if (document.querySelector("#LF-area-video") === null) {
    // 添加底部信息
    var footdom =
      '<div class="subnav"><div class="css-bottom-container fl-l"><div class="css-bottom-logo fl-l"><img src="/public/images/a04/a04-bottom-logo.png"></div><div class="css-bottom-words fl-l"><img src="/public/images/a04/a04-bottom-words.png"></div></div> <div class="fl-r"><ul class="css-subnavlist"> <li><a href="javascript:void(0);">关于开心直播</a></li> <li><a href="javascript:void(0);">合作加盟</a></li> <li><a href="javascript:void(0);">联系客服</a></li> <li><a href="javascript:void(0);">优惠活动</a></li> <li><a href="javascript:void(0);">开心风采</a></li> </ul><p class="css-bottomt-copyright">© 2017 尊龙娱乐版权所有. All Rights Reserved</p></div> </div>';
    $("#doc-ft")
      .find(".container")
      .html(footdom);
  } else {
    var copyLinkdom = '<div class="leftset"><a class="action-button obtrusive" data-clipboard-text="" id="js-copybtn">复制直播地址</a></div>';
    $('.box-left').append(copyLinkdom);
    $('#js-copybtn').attr('data-clipboard-text', window.location.href);
    $('#js-copybtn').on('click', function () {
      layer.msg("复制成功");
    })
    new ClipboardJS('#js-copybtn');
  }
  // 隐藏多人直播间
  $("#lobby")
    .find(".box-hd")
    .eq(0)
    .hide();
  $("#lobby")
    .find(".box-bd")
    .eq(0)
    .hide();
  // 添加切换是否显示密码图标
  var pwddom = '<a class="pwd_icn" href="javascript:void(0);"></a>';
  $(".pasw").append(pwddom);
  // 切换是否显示密码
  $(".pwd_icn").on("click", function () {
    if ($(this).hasClass("open")) {
      $(this)
        .parent()
        .find(".js_login_pass_input")
        .prop("type", "password");
      $(this).removeClass("open");
    } else {
      $(this)
        .parent()
        .find(".js_login_pass_input")
        .prop("type", "text");
      $(this).addClass("open");
    }
  });
});