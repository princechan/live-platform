"use strict";

// 私信改 noah.g
// var room = new room();
var loginIm = {
  pwd: "",
  hxLogin: function hxLogin(data) {
    if (data && data.userid) {
      handlePageLimit();
      var total = getPageCount(); //获取登录数
      if (total > PAGELIMIT) {
        alert("私信聊天最多支持" + PAGELIMIT + "个resource同时登录");
        return;
      }
      var isTempUser = data.isTempUser;
      this.pwd = isTempUser
        ? "tmpPwd" + data.userid.replace("temp_", "")
        : "fmscms" + data.userid;
      return this.pwd;
    } else {
      alert("用户ID不存在，无法创建私信");
    }
  }
};

layui
  .config({
    base: "/public/js/imPackage/static/js/"
  })
  .extend({
    socket: "socket"
  });
layui.use(["layim", "socket"], function(layim, socket) {
  // let $ = layui.jquery;
  var socket = layui.socket;
  var token = $("#CurrentUid").val();
  // var rykey = $('#HxChatUid').val();
  socket.config({
    user: token,
    pwd: loginIm.pwd,
    layim: layim
  });
  //基础配置
  layim.config({
    init: {
      // url: "/json/layim.json?action=get_user_data",
      url: "/index.php?g=api&m=im&a=getUserData",
      type: "get",
      data: {}
    }, //获取主面板列表信息，下文会做进一步介绍
    //获取群成员
    members: {
      url: "class/doAction.php?action=groupMembers",
      data: {}
    },
    //上传图片接口
    uploadImage: {
      url: "class/doAction.php?action=uploadImage", //（返回的数据格式见下文）
      type: "post" //默认post
      //上传文件接口
    },
    uploadFile: {
      url: "class/doAction.php?action=uploadFile",
      type: "post" //默认post
    },
    //自定义皮肤
    uploadSkin: {
      url: "class/doAction.php?action=uploadSkin",
      type: "post" //默认post
    },
    //选择系统皮肤
    systemSkin: {
      url: "class/doAction.php?action=systemSkin",
      type: "post" //默认post
    },
    //获取推荐好友
    getRecommend: {
      url: "/json/getRecommend.json?action=getRecommend",
      type: "get", //默认
      data: {}
    },
    //查找好友总数
    findFriendTotal: {
      // url: "/json/findFriendTotal.json?action=findFriendTotal",
      url: "/index.php?g=api&m=im&a=findFriendTotal",
      type: "get" //默认
    },
    //查找好友
    findFriend: {
      // url: "/json/findFriend.json?action=findFriend",
      url: "/index.php?g=api&m=im&a=findFriend",
      type: "get" //默认
    },
    //获取好友资料
    getInformation: {
      url: "/getInformation.json?action=getInformation",
      type: "get" //默认
    },
    //保存我的资料
    saveMyInformation: {
      url: "class/doAction.php?action=saveMyInformation",
      type: "post" //默认
    },
    //提交建群信息
    commitGroupInfo: {
      url: "class/doAction.php?action=commitGroupInfo",
      type: "get" //默认
    },
    //获取系统消息
    getMsgBox: {
      url: "/json/getMsgBox.json?action=getMsgBox",
      type: "get" //默认post
    },
    getChatLogTotal: {
      // url: '../../../json/chatlogtotal.json?action=getChatLogTotal',
      url: "/index.php?g=api&m=im&a=getChatLogTotal",
      type: "get" //默认post
    },
    getChatLog: {
      // url: '../../../json/chatlog.json?action=getChatLog',
      url: "/index.php?g=api&m=im&a=getChatLog",
      type: "get"
    },
    //扩展工具栏，下文会做进一步介绍（如果无需扩展，剔除该项即可）
    // tool: [{
    //     alias: 'code', //工具别名
    //     title: '代码', //工具名称
    //     icon: '&#xe64e;' //工具图标，参考图标文档
    // }],
    msgbox: layui.cache.dir + "css/modules/layim/html/msgbox.html", //消息盒子页面地址，若不开启，剔除该项即可
    find: layui.cache.dir + "css/modules/layim/html/find.html", //发现页面地址，若不开启，剔除该项即可
    chatLog: layui.cache.dir + "css/modules/layim/html/chatlog.html", //聊天记录页面地址，若不开启，剔除该项即可
    createGroup: layui.cache.dir + "css/modules/layim/html/createGroup.html", //创建群页面地址，若不开启，剔除该项即可
    Information: layui.cache.dir + "css/modules/layim/html/getInformation.html", //好友群资料页面若不开启，剔除该项即可

    notice: true, //是否开启桌面消息提醒，默认false
    systemNotice: false, //是否开启系统消息提醒，默认false
    voice:false,
    title: "私信",
    min: true,
    isAudio: false,
    isVideo: false,
    isgroup: false,
    uploadImage: false,
    uploadFile: false,
    isFindInList: true, // 自定义判断可删 layimg.js删相应判断
    // isfriend: false,
    // search: false,
    // find: false,
    msgbox: false
  });

  layim.on("chatChange", function(obj) {
    Login.closePop();
  });
  layim.on("ready", function(options) {
    if(_DATA.user.vest_id.toString()==='0' || _DATA.user.vest_id.toString()==='1'){
      $('.layim-tool-find').remove();
    }
    $(".layui-icon").map((i, el) => {
      if ($(el).attr("lay-type") === "history") {
        $(el).trigger("click");
      }
    });
  });
});
