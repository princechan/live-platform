$(function () {
    // 列表加载开始
    var limit = 8;
    var page = 1;
    var has_more;
    var getSingleLives = './index.php?g=home&m=index&a=getSingleLives';
    $.get(getSingleLives, {'page': page, 'limit': limit}, function (res) {
        switch (res.code) {
            case 200:
                var html = '';
                has_more = res.data.has_more;
                $.each(res.data.list, function (ind, val) {
                    // console.log(has_more+';'+val.islive);

                    html += '<li><div class="cont_top"><img class="thumb" src="' + val.thumb + '"/>';

                    if (val.islive == 1) {
                        html += '<div class="zhiboing"><em></em>直播中</div>';
                    } else {
                        html += '<div class="zhiboending"><em></em>休息中</div>';
                            }


                    html += '<div class="cont_mask"><a href="/' + val.uid + '" target="_blank"><em></em></a></div></div>\
                                <div class="cont_bottom">\
                                <span class="tit">' + val.user_nicename + '</span>\
                                <span class="read"><em></em>' + val.nums + '</span>\
                                </div>\
                            </li>';

                });

                $('.liveHall').append(html);

                $('body').mousewheel(function (event, delta) {
                    if (has_more == true && delta == -1 && document.documentElement.scrollTop + document.documentElement.clientHeight == document.documentElement.scrollHeight) {
                        var html = '';
                        page++;

                        $.get(getSingleLives, {'page': page, 'limit': limit}, function (res) {
                            has_more = res.data.has_more;
                            // console.log(res);

                            $.each(res.data.list, function (ind, val) {
                                // console.log(res);

                                html += '<li><div class="cont_top"><img class="thumb" src="' + val.thumb + '"/>';

                                if (val.islive == 1) {
                                    html += '<div class="zhiboing"><em></em>直播中</div>';
                                } else {
                                    html += '<div class="zhiboending"><em></em>休息中</div>';
                                        }


                                html += '<div class="cont_mask"><a href="/' + val.uid + '" target="_blank"><em></em></a></div></div>\
                                            <div class="cont_bottom">\
                                            <span class="tit">' + val.user_nicename + '</span>\
                                            <span class="read"><em></em>' + val.nums + '</span>\
                                            </div>\
                                        </li>';

                            });

                            $('.liveHall').append(html);
                        });
                    }
                });
                break;
            default:
                break;
        }
    });
    // 列表加载结束

    //  播放器开始 
    var videocont = '';
    var firsteach = true;
    $.get('./index.php?g=api&m=liveing&a=recommend', function (res) {
        switch (res.code) {
            case 200:
                res.data.live_list.forEach(function (val, indx) {

                        if ( val.type == 0 && firsteach ) {
                            firsteach=false;
                            if (val.video_url != '') {

                                // videocont += '<div class="videocont" data-id="' + indx + '"><div class="live_list_loding"><img src="' + val.show_cover + '" /><span class="loadingtext">视频加载中...</span></div><div class="live_list_bg"><a href="/' + val.uid + '">进入直播间</a></div>\
                                //         <video id="my-video' + val.id + '" class="video-js vjs-default-skin vjs-big-play-centered" width="880" height="495"><source src="' + val.video_url + '" type="application/x-mpegURL"></video>\
                                //          </div>';
                                
                                videocont += '<li class="videocont"><div class="live_list_bg"><a href="/' + val.route_id + '">进入直播间</a></div><img class="videodefault" src="' + val.show_cover + '" />\
                                         </li>';

                            } else {
                                videocont += '<li class="videocont"><div class="live_list_bg"><a href="/' + val.route_id + '">进入直播间</a></div><img class="videodefault" src="' + val.show_cover + '" />\
                                         </li>';
                            }  

                        }else if (val.type == 1 && firsteach) {
                                firsteach=false;
                                videocont += '<li class="default_list_bg"><div class="live_list_bg"><a href="/' + val.route_id + '">进入直播间</a></div><img class="videodefault" src="' + val.default_cover + '" /></li>';
                        }
                    
                        $('.box-video .dot-wrap').append('<li><em>'+ (Number(indx)+1) +'</em></li>');
                });

                $('.box-video .ck-slide-wrapper').append(videocont);

                setTimeout(function(){
                    $('.g-box .box-video .ck-slidebox .dot-wrap li:first-child').addClass('current');
                    $('.g-box .box-video .ck-slide-wrapper li:first-child').css('z-index','1');

                    $('.ck-slide').ckSlide({
                        autoPlay: false
                     });
                    
                },500);

                setTimeout(function () {
                    if ($('.videocont video').length > 0) {
                        var player = videojs('my-video1');
                        player.ready(function () {
                            player.play();
                        });
                        player.on('error', function () {
                            player.dispose();
                        });
                        player.on('canplay', function () {
                            $(this.el_).siblings('.live_list_loding').fadeOut(1000);
                        });
                    }
                }, 3000);
            break;
        }
    });

    // 播放器结束
    // 进入直播按钮开始
    $('body').on('mouseenter', '.videocont, .default_list_bg', function (event) {
        event.preventDefault();
        $('.live_list_bg').fadeIn(300);
        $('.live_list_bg').addClass('act');
    });

    $('body').on('mouseleave', '.videocont, .default_list_bg', function (event) {
        event.preventDefault();
        $('.live_list_bg').fadeOut(300);
        $('.live_list_bg').removeClass('act');
    });

    //进入直播按钮结束

    


});