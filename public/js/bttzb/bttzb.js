/**
 * noah.g
 * a01 bttzb 兼容脚本
 */
// 更换logo
$(".hd-logo")
  .find("img")
  .attr("src", "/public/images/bttzb/a01-logo.png");

// 提示修改为g开头
$(".hd-login")
  .find(".name")
  .find("label")
  .text("账号以g开头");
window.onload = function () {
  // 根据视频区域的dom判断是不是直播室
  if (document.querySelector("#LF-area-video") !== null) {
    // 修改logo位置
    $(".act").css("background-color", "inherit");
    $(".container").css({
      width: "80%",
      float: "left",
      marginLeft: "25px"
    });
  }
  this.setTimeout(function () {
    // 登录窗修改为g开头
    $(".js_boxlogin_users_input").attr(
      "placeholder",
      "输入用户名(g开头)/手机号码"
    );
    // 账号密码长度修改
    $(".js_login_users_input").attr("maxlength", "9");
    $(".js_login_pass_input").attr("maxlength", "10");
    $(".js_boxlogin_users_input").attr("maxlength", "9");
    $(".js_boxlogin_pass_input").attr("maxlength", "10");
    $(".js_reg_users_input").attr("maxlength", "9");
    $(".js_reg_pass_input").attr("maxlength", "10");
  }, 2000);
};

$(function () {
  // 根据视频区域的dom判断是不是直播室
  if (document.querySelector("#LF-area-video") === null) {
    // 隐藏copyright
    var footdom =
      '<div class="subnav"> <ul> <li><a href="javascript:void(0);">关于开心直播</a></li> <li><a href="javascript:void(0);">合作加盟</a></li> <li><a href="javascript:void(0);">联系客服</a></li> <li><a href="javascript:void(0);">优惠活动</a></li> <li><a href="javascript:void(0);">开心风采</a></li> </ul> </div>';
  }
  $("#doc-ft")
    .find(".container")
    .html(footdom);
  // 隐藏多人直播间
  $("#lobby")
    .find(".box-hd")
    .eq(0)
    .hide();
  $("#lobby")
    .find(".box-bd")
    .eq(0)
    .hide();
  // 添加切换是否显示密码图标
  var pwddom = '<a class="pwd_icn" href="javascript:void(0);"></a>';
  $(".pasw").append(pwddom);
  // 切换是否显示密码
  $(".pwd_icn").on("click", function () {
    if ($(this).hasClass("open")) {
      $(this)
        .parent()
        .find(".js_login_pass_input")
        .prop("type", "password");
      $(this).removeClass("open");
    } else {
      $(this)
        .parent()
        .find(".js_login_pass_input")
        .prop("type", "text");
      $(this).addClass("open");
    }
  });
});