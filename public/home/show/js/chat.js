var Interface = {
  startEndLive: function (type) {
    if (type == 1) {
      var msg = '{"retmsg":"ok","retcode":"000000","msg":[{"uid":"0","timestamp":"","sex":"0","level":"0","touid":"0","msgtype":"1","tougood":"","_method_":"StartEndLive","touname":"","action":"18","ugood":"","ct":"直播关闭","city":""}]}';
      window.location.href = "/";
    } else {
      var msg = '{"retmsg":"ok","retcode":"000000","msg":[{"uid":"0","timestamp":"","sex":"0","level":"0","touid":"0","msgtype":"1","tougood":"","_method_":"StartEndLive","touname":"","action":"15","ugood":"","ct":"直播开始","city":""}]}';
    }
    Socket.emitData('broadcast', msg);
  },

  startEndLiveEven: function (type, stream) {

    if (type == 1) {
      var msg = '{"retmsg":"ok","retcode":"000000","msg":[{"uid":"0","timestamp":"","sex":"0","level":"0","touid":"0","msgtype":"1","tougood":"","_method_":"startEndLiveEven","touname":"","action":"18","ugood":"","ct":"连麦关闭","city":""}]}';

    } else {
      var msg = '{"retmsg":"ok","retcode":"000000","msg":[{"uid":"0","timestamp":"","sex":"0","level":"0","touid":"0","msgtype":"1","tougood":"","_method_":"startEndLiveEven","touname":"","action":"15","ugood":"","ct":"连麦开始","stream":"' + stream + '","city":""}]}';
    }

    Socket.emitData('broadcast', msg);
  },

  stopRoom: function () {
    getSwfObject("container").stopRoom();

  }
};
/* function ceshizhong()
{
	var  msg = '{"retmsg":"ok","retcode":"000000","msg":[{"liveuid":"0","Jinhuatoken":"66_1488765986_jinhua","gameid":"36","time":"3","msgtype":"1","_method_":"startJinhua","action":"1","ct":""}]}';
	Socket.emitData('broadcast',msg);
} */


//设置标题(title)、房间类型(type)、及其房间类型的收费金额(stand)、已经请求僵尸粉
var liveType = {
  getzombie_url: './api/public/index.php?service=Live.getZombie',
  checkLive_url: './index.php?g=home&m=Show&a=checkLive',
  selectPlay_url: './index.php?g=home&m=Show&a=selectPlay',
  roomChargeUrl: './index.php?g=home&m=Spend&a=roomCharge',
  checkMultiLive: '/index.php?g=home&m=show&a=checkMultiLive',

  //flash调用js弹出设置类型窗口
  Choice: function (obs) {
    // console.log('obs');
    _DATA.obs = obs;
    //liveType.selectPlay();
    liveType.uploadThumb();
    /* var choice='{"title":"未设置标题","type":"","stand":"","obs":"'+obs+'"}';
		document.getElementById('webplayer')._myclock(choice); */
  },
  uploadThumb: function () {
    document.getElementById('ds-dialog-bg').style.display = 'block';
    $("#selectPlay").html($("#selectPlayTemplate").html()).css({
      "left": getMiddlePos('selectPlay').pl + "px",
      "z-index": 210,
    }).show();
  },
  upload: function () {
    liveType.closeUpload();
    _DATA.thumb = $("#thumb").val();
    sendMsgNum = $("#sendMsgNum").val();
    sendMsgFrequency = $("#sendMsgFrequency").val();
    liveType.selectPlay();

  },
  closeUpload: function () {
    $('#selectPlay').hide();
    $('#ds-dialog-bg').hide();
  },
  selectPlay: function () {
    $('#giveBox').hide();
    $.ajax({
      type: "post",
      url: "./index.php?g=home&m=Show&a=selectplay",
      data: "",
      success: function (data) {
        document.getElementById('ds-dialog-bg').style.display = 'block';
        $("#selectPlay").css({
          "left": getMiddlePos('selectPlay').pl + "px",
          "z-index": 210,
        }).show();
        $("#selectPlay").html(data);
      }
    });

  },
  closePorp: function () {
    $('#selectPlay').hide();
    var str = '<div class="dialog-tc">\
				<div class="mode">\
					<a>设置直播房间信息</a>\
					<span onclick="liveType.closePorp()" class="rotate"></span>\
				</div>\
				<div class="mount-method" id="mount_method" data-type="0">\
					<a class="mount_btn_on" onclick="flashuploadcut(\'thumb_images\', \'附件上传\',\'thumb\',thumb_images,\'1,jpg|jpeg|gif|png|bmp,1,,,1\',\'\',\'\',\'\');return false;">上传</a>\
					<input type="hidden" name="thumb" id="thumb" value="">\
					<div class="clearboth"></div>\
					<div class="sendMsgSet">发言字数：<input type="text" id="sendMsgNum" placeholder="请填写发言字数" /></div>\
					<div class="sendMsgWoring">(1~100之间,不填默认30)</div>\
					<div class="sendMsgSet">发言频率：<input type="text" id="sendMsgFrequency" placeholder="请填写发言频率，单位为秒" /></div>\
					<div class="sendMsgWoring">(10~60之间,不填默认0)</div>\
					<div></div>\
				</div>\
				<div class="deal-footer">\
					<a class="mount_con" onclick="liveType.upload()">确定</a>\
					<a class="mount_cancel" onclick="liveType.closeUpload()">取消</a>\
				</div>\
			</div>';
    // $('#selectPlay').html(str);
    document.getElementById('ds-dialog-bg').style.display = 'none';

  },
  other: function () {
    var id = $(".mount-method .mount_btn_on").attr("id");
    var type = "";
    var stand = "";
    if (id == "putong_btn") {
      type = "0";
    } else if (id == "mima_btn") {
      type = "1";
      stand = $("#gift_number").val();
      if (stand == "") {
        liveType.closePorp();
        layer.msg("密码不能为空");
        setTimeout(function () {
          location.reload();
        }, 2000);
        return !1;
      }
    } else if (id == "shoufei_btn") {
      type = "2";
      stand = $("#gift_number").val();
      if (stand == "") {
        liveType.closePorp();
        layer.msg("金额不能为空");
        setTimeout(function () {
          location.reload();
        }, 2000);
        return !1;
      }
    }
    liveType.closePorp();

    // console.log('other');
    var choice = '{"title":"未设置标题","type":"' + type + '","stand":"' + stand + '","thumb":"' + _DATA.thumb + '"}';
    document.getElementById('webplayer')._myclock(choice);
  },
  getzombie: function (uid, stream) {
    if (_DATA.user && _DATA.user.logged == 1) { //已登录
      $.ajax({
        url: this.getzombie_url,
        data: {
          uid: uid,
          stream: stream
        },
        dataType: 'json',
        success: function (data) {}
      });
    }
  },

  //**多人直播间房间类型开始
  checkLiveMulti: function () {
    var room = new Room();
    var liveuid = room.id;

    $.ajax({

      url: this.checkMultiLive,
      data: {
        live_id: liveuid
      },
      type: "GET",
      dataType: 'json',
      success: function (res) {

        switch (res.code) {
          case 200:
            if (!room.userIsAnchor) {

              if (res.data.room_type == 1) {
                // console.log("密码锁门房间");
                // alert(res.data.room_password);
                liveType.passRoom(res.data.room_password, res.data.msg);

              } else if (res.data.room_type == 2) {
                // console.log("付费扣券房间");
                liveType.chargeMulti(res.data.msg, room.typeId, liveuid);

              } else if (res.data.room_type == 0) {

                // console.log("普通房间");
                // layer.msg("普通房间");
                multi.init(); //加载多视频

              } else {
                multi.init(); //加载多视频
              }
            } else {
              multi.init();
              // Video.endRecommend();
              // console.log('主播本人房间')
            }

            break;

          default:
            multi.init();
        }

      },
      error: function () {
        layer.msg("网络请求出错！");
      }
    });

    Socket.nodejsInit();
  },
  //**多人直播间房间类型结束

  checkLive: function () {
    var room = new Room();
    if (room.isLive) {
      var liveuid = _DATA.anchor.id;
      var stream = _DATA.live.stream;

      $.ajax({
        url: this.checkLive_url,
        data: {
          liveuid: liveuid,
          stream: stream
        },
        dataType: 'json',
        success: function (data) {
          // console.log('房间类型');
          // console.log(data);
          $('.player-praises .vidcont .praises').attr('id', _DATA.anchor.id) //点赞uid
          liveType.type();
          switch (data.code) {

            case 0:
              if (data.land == 0) {

                if (data.type == 1) {
                  // debugger;

                  var mag_name = "当前房间为密码房间";
                  liveType.passRoom(data.type_msg, mag_name);
                } else if (data.type == 2) {

                  liveType.charge(data.type_msg, room.typeId, liveuid, stream);
                } else if (data.type == 0) {

                  liveType.jwplayer();
                }


              } else {

                var t = setTimeout("window.location.href='/'", 20000);
                layer.alert(data.type_msg, {
                  skin: 'layui-layer-molv' //样式类名
                    ,
                  closeBtn: 0
                }, function () {
                  window.location.href = '/';
                });
              }

              break;

            default:
              layer.msg(data.msg);

          }

        },
        error: function (request) {
          layer.msg("信息处理错误");
        }
      });
    } else { //当前主播未开播
      // console.log('当前房间没有直播');
      Video.endRecommend();
    }
    Socket.nodejsInit();
  },

  //**房间密码验证
  passRoom: function (type_msg, mag_name) {
    var t = setTimeout("window.location.href='/'", 20000);
    // console.log('房间类型密码');
    var room = new Room();

    layer.prompt({
      title: mag_name,
      btn2: function () {
        window.location.href = '/'
      },
      formType: 0,


    }, function (pass, index) {

      if (pass == type_msg) {

        clearTimeout(t);

        $.get('/index.php?g=home&m=show&a=checkRoomPassword', {
          live_type: room.typeId,
          liveid: room.id,
          password: pass

        }, function () {

          if (room.typeId == 1) {
            multi.init();

          } else if (room.typeId == 0 && !room.userIsAnchor) {

            liveType.jwplayer();
          }

          liveType.type();
          layer.close(index);


        });

      } else {
        // console.log("密码错误");
        mag_name = "密码错误，请重新输入";
        setTimeout(liveType.passRoom(type_msg, mag_name), 2000);
      }
    });
  },


  //**多人直播付费房间开始
  chargeMulti: function (type_msg, live_type, liveuid) {
    var t = setTimeout("window.location.href='/'", 20000);
    // console.log("扣费房间开始");
    var room = new Room();
    layer.confirm(type_msg, {
      btn: ['进入', '放弃']
    }, function () {

      $.post("./index.php?g=home&m=Spend&a=roomCharge", {
        live_type: live_type,
        liveid: liveuid
      }, function (data) {

        var data = $.parseJSON(data);

        // console.log(data);
        if (data.code == 0) {
          clearTimeout(t);

          layer.closeAll();
          _DATA.user.coin = data.livecoin;
          User.updateMoney();

          multi.init();


        } else {
          layer.msg(data.msg);
          setTimeout("window.location.href='/'", 2000);
        }
      });

    }, function () {
      window.location.href = "./";
    });
  },
  //**多人直播付费房间结束

  //**多人直播间修改房间密码
  liveMima: function (msg) {

    var room = new Room();

    // var _this=this;
    if (room.userIsAnchor) {
      // console.log("主播");

      layer.prompt({
        title: '修改房间密码',
        value: _DATA.multiLive.room_password,
        formType: 0

      }, function (value, index) {
        // console.log(value);

        $.post('/index.php?m=show&a=setMultiShowPassword', {
          room_id: _DATA.multiLive.id,
          password: value

        }, function (res) {

          switch (res.code) {
            case 200:
              layer.msg('修改成功');
              _DATA.multiLive.room_password = value;

              // _this.systemNot(data);

              Socket.emitData('broadcast', msg);
              break;

            default:
              // console.log(res.message);
              layer.msg(res.message);

          }
        });

      });


    } else {

      // console.log("主播没有开始直播");

    }
  },

  //**单人直播间修改房间密码
  liveMimasigle: function (msg) {

    var room = new Room();

    // var _this=this;
    if (room.userIsAnchor) {
      // console.log("主播");

      layer.prompt({
        title: '修改房间密码',
        value: _DATA.live.type_val,
        formType: 0

      }, function (value, index) {
        // console.log(value);

        $.post('/index.php?m=show&a=setMultiShowPassword', {
          room_id: _DATA.live.uid,
          password: value

        }, function (res) {

          switch (res.code) {
            case 200:
              layer.msg('修改成功');
              _DATA.live.type_val = value;

              // _this.systemNot(data);

              Socket.emitData('broadcast', msg);
              break;

            default:
              // console.log(res.message);
              layer.msg(res.message);

          }
        });

      });


    } else {



    }
  },

  charge: function (type_msg, live_type, liveid, stream) {
    var t = setTimeout("window.location.href='/'", 20000);
    // console.log("扣费房间");
    layer.confirm(type_msg, {
      btn: ['进入', '放弃']
    }, function () {
      $.post('/index.php?g=home&m=spend&a=roomCharge', {
        live_type: live_type,
        liveid: liveid,
        stream: stream
      }, function (data) {
        // debugger;
        var data = $.parseJSON(data);

        if (data.code == 0) {
          clearTimeout(t);

          layer.closeAll();
          _DATA.user.coin = data.livecoin;
          User.updateMoney();

          liveType.jwplayer();

        } else {
          clearTimeout(t);
          layer.msg(data.msg);
          setTimeout("window.location.href='/'", 2000);
        }
      });

    }, function () {

      window.location.href = "./";
    });
  },
  //根据这里选择时使用jw还是ck播放视频
  type: function () {
    // liveType.ckplayer();

    // $(".channel-player-v2").html('<video src="'+_DATA.live.pull+'"></video>');
    // return;

    var fls = flashChecker();

    if (fls.f) {

      // console.log('flash ok');

    } else {
      //alert("您没有安装flash");
      // console.log('flash no');
      $(".channel-player-v2").html('<a href="https://get.adobe.com/cn/flashplayer/" class="flashLoadMsg" target="_blank">安装或者启用FLASH播放器</a>');
    }


  },
  jwplayer: function () { //用户观看视频播放器

    jwplayer('playerzmblbkjP').setup({
      provider: "rtmp",
      flashplayer: "public/swf/jwplayer.flash.swf",
      file: _DATA.live.pull,
      image: "public/home/images/loading.png",
      autostart: true,
      'stretching': 'bestfit',
      height: '640px',
      width: '360px',
      controls: true
    });
  },
  ckplayer: function () {
    var flashvars = {
      f: _DATA.live.pull, //视频地址rtmp://testlive.anbig.com/5showcam/1737_1487723653
      a: '', //调用时的参数，只有当s>0的时候有效
      s: '0', //调用方式，0=普通方法（f=视频地址），1=网址形式,2=xml形式，3=swf形式(s>0时f=网址，配合a来完成对地址的组装)
      c: '0', //是否读取文本配置,0不是，1是
      t: '10|10', //视频开始前播放swf/图片时的时间，多个用竖线隔开
      y: '', //这里是使用网址形式调用广告地址时使用，前提是要设置l的值为空
      z: '', //缓冲广告，只能放一个，swf格式
      e: '8', //视频结束后的动作，0是调用js函数，1是循环播放，2是暂停播放并且不调用广告，3是调用视频推荐列表的插件，4是清除视频流并调用js功能和1差不多，5是暂停播放并且调用暂停广告
      v: '100', //默认音量，0-100之间
      p: '1', //视频默认0是暂停，1是播放，2是不加载视频
      h: '0', //播放http视频流时采用何种拖动方法，=0不使用任意拖动，=1是使用按关键帧，=2是按时间点，=3是自动判断按什么(如果视频格式是.mp4就按关键帧，.flv就按关键时间)，=4也是自动判断(只要包含字符mp4就按mp4来，只要包含字符flv就按flv来)
      k: '32|63', //提示点时间，如 30|60鼠标经过进度栏30秒，60秒会提示n指定的相应的文字
      n: '这是提示点的功能，如果不需要删除k和n的值|提示点测试60秒', //提示点文字，跟k配合使用，如 提示点1|提示点2
      wh: '', //宽高比，可以自己定义视频的宽高或宽高比如：wh:'4:3',或wh:'1080:720'
      lv: '0', //是否是直播流，=1则锁定进度栏
      loaded: 'loadedHandler', //当播放器加载完成后发送该js函数loaded
      //调用播放器的所有参数列表结束
      //以下为自定义的播放器参数用来在插件里引用的
      my_title: "111",
      my_url: encodeURIComponent(window.location.href) //本页面地址
      //调用自定义播放器参数结束
    };
    var params = {
      bgcolor: '#FFF',
      allowFullScreen: true,
      allowScriptAccess: 'always'
    }; //这里定义播放器的其它参数如背景色（跟flashvars中的b不同），是否支持全屏，是否支持交互
    var video = ['http://img.ksbbs.com/asset/Mon_1605/0ec8cc80112a2d6.mp4->video/mp4'];
    CKobject.embed('public/playback/ckplayer.swf', 'playerzmblbkjP', 'ckplayer_playerzmblbkjP', '100%', '100%', false, flashvars, video, params);
  }
}
//房间管理操作
var Controls = {
  cancel_url: './index.php?g=home&m=Spend&a=cancel',
  tiren_url: './index.php?g=home&m=Spend&a=tiren',
  black_url: './index.php?g=home&m=Spend&a=black',
  stopRoom_url: './api/public/index.php?service=Live.superStopRoom',
  report_url: './index.php?g=home&m=Spend&a=report',
  gag_url: '/index.php?g=home&m=Spend&a=gag',
  admin_close: $("#admin_close"),
  admin_stopit: $("#admin_stopit"),
  admin_report: $("#admin_report"),
  init: function () {
    this.addEvent();
  },
  addEvent: function () {
    var _this = this;
    _this.admin_close.on("click", function () {
        // console.log('关闭直播');
        var type = _this.admin_close.attr("data-type");
        _this.stopRoom(type);

      }),
      _this.admin_stopit.on("click", function () {
        var type = _this.admin_stopit.attr("data-type");
        _this.stopRoom(type);
      }),
      _this.admin_report.on("click", function () {
        layer.prompt({
          title: '请填写举报内容',
          formType: 2
        }, function (text, index) {
          layer.close(index);
          _this.report(text);
        })

      })
  },
  cancel: function () //设置 取消 管理
  {
    var room = new Room();
    var touid = $("#popup_info").attr("popup-data-uid");
    $.ajax({
      dataType: 'json',
      url: this.cancel_url,
      // data: {touid: touid, roomid: _DATA.anchor.id},// 你的formid
      data: {
        touid: touid,
        roomid: room.id
      }, // 你的formid
      error: function (request) {
        layer.msg("数据请求失败！");
      },
      success: function (data) {
        layer.msg(data.msg);
        $("#popup_info").remove();
        if (data.isadmin == 1) {
          ct = data.user_nicename + "被设为管理员";
        } else {
          ct = data.user_nicename + "被删除管理员";
        }
        var msg = '{"msg":[{"_method_":"SystemNot","action":"13","ct":"' + ct + '","msgtype":"4","uname":"' + _DATA.user.user_nicename + '","toname":"' + data.user_nicename + '","touid":' + touid + ',"uid":' + _DATA.user.id + ',"level":' + _DATA.user.level + '}],"retcode":"000000","retmsg":"ok"}';
        Socket.emitData('broadcast', msg);
      }
    });

  },
  gag: function () //禁言
  {
    // 
    // debugger;
    var room = new Room();
    var touid = $("#popup_info").attr("popup-data-uid");
    var name = $("#popup_info").attr("popup-data-name");
    $("#popup_info").remove();


    // if (_DATA.anchor.id == _DATA.user.id || _DATA.user.vest_id == 7 || _DATA.user.issuper == 1) {//是主播或者黑马或者超管【可以设置禁言时间】
    if (room.userIsAnchor || room.userIsDj || _DATA.user.vest_id == 7 || _DATA.user.issuper == 1) { //是主播或者麦手或者黑马或者超管【可以设置禁言时间】

      layer.confirm('禁言时间(分钟)：<input type="text" id="gagTime" ', {
        btn: ['禁言', '取消'] //按钮
      }, function () {
        var gagTime = parseFloat($("#gagTime").val());
        if (gagTime < 1 || gagTime > 60) {
          layer.msg("禁言时间在1~60分钟之间");
          return;
        }

        $.ajax({
          url: "/index.php?g=home&m=Spend&a=gag",
          //url:this.gag_url,
          // data: {touid: touid, roomid: _DATA.anchor.id, gagTime: gagTime},
          data: {
            touid: touid,
            liveid: room.id,
            live_type: room.typeId,
            gagTime: gagTime
          },
          type: 'POST',
          dataType: 'json',
          success: function (data) {

            if (data.code == 0) {
              layer.msg(data.info);
              //uid操作人ID uname操作人昵称 touid 被禁言人的ID toname被禁言人昵称
              var msg = '{"msg":[{"_method_":"ShutUpUser","action":"1","ct":"' + name + '被禁言了' + gagTime + '分钟","msgtype":"4","uid":' + _DATA.user.id + ',"uname":"' + _DATA.user.user_nicename + '","touid":' + touid + ',"toname":"' + name + '"}],"retcode":"000000","retmsg":"ok"}';
              Socket.emitData('broadcast', msg);
              layer.closeAll();
            } else {
              layer.msg(data.msg);
            }
          },

          error: function (data) {
            layer.msg("禁言失败");
          }
        });


      }, function () {
        layer.closeAll();
      });


    } else {


      $.ajax({
        url: this.gag_url,
        // data: {touid: touid, roomid: _DATA.anchor.id},
        data: {
          touid: touid,
          roomid: room.id
        },
        dataType: 'json',
        success: function (data) {
          if (data.code == 0) {
            layer.msg(data.info);
            //uid操作人ID uname操作人昵称 touid 被禁言人的ID toname被禁言人昵称
            var msg = '{"msg":[{"_method_":"ShutUpUser","action":"1","ct":"' + name + '被禁言了10分钟","msgtype":"4","uid":' + _DATA.user.id + ',"uname":"' + _DATA.user.user_nicename + '","touid":' + touid + ',"toname":"' + name + '"}],"retcode":"000000","retmsg":"ok"}';
            Socket.emitData('broadcast', msg);
          } else {
            layer.msg(data.msg);
          }
        }
      });
    }

  },
  tiren: function () //踢人
  {
    // debugger;

    var room = new Room();
    var touid = $("#popup_info").attr("popup-data-uid");
    var name = $("#popup_info").attr("popup-data-name");
    $("#popup_info").remove();
    var param = {
      touid: touid,
      liveid: room.id,
      live_type: room.typeId
    };
    if (room.typeId == 0) {
      Object.assign(param, {
        stream: _DATA.live.stream
      });
    }
    $.ajax({
      url: this.tiren_url,
      type: 'post',
      // data: {touid: touid, roomid: _DATA.anchor.id, stream: _DATA.live.stream},
      data: param,
      dataType: 'json',
      success: function (data) {
        if (data.code == 0) {
          layer.msg("操作成功");
          var msg = '{"msg":[{"_method_":"KickUser","action":"2","ct":"' + name + '被踢出房间","msgtype":"4","uid":' + _DATA.user.id + ',"uname":"' + _DATA.user.user_nicename + '","touid":' + touid + ',"touname":"' + name + '"}],"retcode":"000000","retmsg":"ok"} ';
          Socket.emitData('broadcast', msg);

        } else {
          layer.msg(data.msg);
        }
      }
    });
  },

  stopRoom: function (type) //超管关闭直播0 禁止直播1
  {
    // debugger;
    // console.log('关闭直播2');

    // debugger;
    var room = new Room();

    $.ajax({
      url: this.stopRoom_url,
      // data: {uid: _DATA.user.id, token: _DATA.user.token, liveuid: _DATA.anchor.id, type: type},
      data: {
        uid: _DATA.user.id,
        token: _DATA.user.token,
        liveuid: room.id,
        type: type
      },
      dataType: 'json',
      success: function (data) {
        switch (data.ret) {

          case 200:
            if (data.data.code == 0) {
              var msg = '{"msg":[{"_method_":"stopLive","action":"10","ct":"该直播间涉嫌违规，已被停播","msgtype":"4","uid":' + _DATA.user.id + ',"uname":"' + _DATA.user.user_nicename + '","touid":' + _DATA.anchor.id + ',"touname":"' + _DATA.anchor.user_nicename + '"}],"retcode":"000000","retmsg":"ok"} ';
              Socket.emitData('broadcast', msg);
              // layer.msg(data['data']['info']['0']['msg']);
            } else {
              // console.log(data.data.msg);

              // var msg = '{"msg":[{"_method_":"anchorOffline","action":"2","msgtype":"0"}],"retcode":"000000","retmsg":"OK"}';

              //  Socket.emitData('broadcast', msg);
              //  console.log(msg);
              // window.location.href = "./";
              window.location.reload(true);
            }

            break;

          default:
            // console.log(data.msg);
            layer.msg('未开始直播!!');

        }

      }
    });
  },


  report: function (content) //举报
  {
    // debugger;
    var room = new Room();
    $.ajax({
      url: this.report_url,
      // data: {tlleuid: _DATA.user.id, token: _DATA.user.token, liveuid: _DATA.anchor.id, content: content},
      data: {
        tlleuid: _DATA.user.id,
        token: _DATA.user.token,
        liveuid: room.id,
        content: content
      },
      dataType: 'json',
      success: function (data) {
        layer.msg(data.msg);
      },
      error: function (request) {
        layer.msg("信息处理错误");
      },
    });
  },
  black: function () {
    // debugger;
    var touid = $("#popup_info").attr("popup-data-uid");
    if (touid != _DATA.user.id) {
      $.ajax({
        url: this.black_url,
        data: {
          touid: touid
        },
        dataType: 'json',
        success: function (data) {
          if (data.code == 0) {
            layer.msg(data.msg);
          } else {
            layer.msg(data.msg);
          }
        }
      });
    } else {
      layer.msg('无法将自己拉黑');
    }
    $("#popup_info").remove();
  },

  HxChat_User: function () {
    debugger;
    var room = new Room();
    //判断环信窗口是否显示
    var status = $(".hxChatWindow").css("display");

    if (status == 'none') {
      $(".hxChatWindow").show(300);
    }

    var uid = $("#popup_info").attr("popup-data-uid");
    // var roomid = _DATA.anchor.id;
    var roomid = room.id;
    searchMember(uid, roomid);

  },
  //设置马甲
  setAdmin: function () {
    // debugger;
    var room = new Room();
    var bamaObj = _DATA.vests["1"];
    var baimaTitle = "";
    var lvmaObj = _DATA.vests["2"];
    var lvmaTitle = "";
    var lanmaObj = _DATA.vests["3"];
    var lanmaTitle = "";
    var huangmaObj = _DATA.vests["4"];
    var huangmaTitle = "";
    var chengmaObj = _DATA.vests["5"];
    var chengmaTitle = "";
    var zimaObj = _DATA.vests["6"];
    var zimaTitle = "";
    var heimaObj = _DATA.vests["7"];
    var heimaTitle = "";

    if (bamaObj.away_admin == 1) {
      baimaTitle += "踢管理员 √";
    }
    if (bamaObj.away_user == 1) {
      baimaTitle += "踢人 √";
    }
    if (bamaObj.gap == 1) {
      baimaTitle += "禁止说话 √";
    }
    if (bamaObj.gap_all == 1) {
      baimaTitle += "频道禁言 √";
    }
    if (bamaObj.private_chat == 1) {
      baimaTitle += "私聊 √";
    }
    if (bamaObj.set_admin == 1) {
      baimaTitle += "设置马甲 √";
    }


    if (lvmaObj.away_admin == 1) {
      lvmaTitle += "踢管理员 √";
    }
    if (lvmaObj.away_user == 1) {
      lvmaTitle += "踢人 √";
    }
    if (lvmaObj.gap == 1) {
      lvmaTitle += "禁止说话 √";
    }
    if (lvmaObj.gap_all == 1) {
      lvmaTitle += "频道禁言 √";
    }
    if (lvmaObj.private_chat == 1) {
      lvmaTitle += "私聊 √";
    }
    if (lvmaObj.set_admin == 1) {
      lvmaTitle += "设置马甲 √";
    }


    if (lanmaObj.away_admin == 1) {
      lanmaTitle += "踢管理员 √";
    }
    if (lanmaObj.away_user == 1) {
      lanmaTitle += "踢人 √";
    }
    if (lanmaObj.gap == 1) {
      lanmaTitle += "禁止说话 √";
    }
    if (lanmaObj.gap_all == 1) {
      lanmaTitle += "频道禁言 √";
    }
    if (lanmaObj.private_chat == 1) {
      lanmaTitle += "私聊 √";
    }
    if (lanmaObj.set_admin == 1) {
      lanmaTitle += "设置马甲 √";
    }


    if (huangmaObj.away_admin == 1) {
      huangmaTitle += "踢管理员 √";
    }
    if (huangmaObj.away_user == 1) {
      huangmaTitle += "踢人 √";
    }
    if (huangmaObj.gap == 1) {
      huangmaTitle += "禁止说话 √";
    }
    if (huangmaObj.gap_all == 1) {
      huangmaTitle += "频道禁言 √";
    }
    if (huangmaObj.private_chat == 1) {
      huangmaTitle += "私聊 √";
    }
    if (huangmaObj.set_admin == 1) {
      huangmaTitle += "设置马甲 √";
    }


    if (chengmaObj.away_admin == 1) {
      chengmaTitle += "踢管理员 √";
    }
    if (chengmaObj.away_user == 1) {
      chengmaTitle += "踢人 √";
    }
    if (chengmaObj.gap == 1) {
      chengmaTitle += "禁止说话 √";
    }
    if (chengmaObj.gap_all == 1) {
      chengmaTitle += "频道禁言 √";
    }
    if (chengmaObj.private_chat == 1) {
      chengmaTitle += "私聊 √";
    }
    if (chengmaObj.set_admin == 1) {
      chengmaTitle += "设置马甲 √";
    }


    if (zimaObj.away_admin == 1) {
      zimaTitle += "踢管理员 √";
    }
    if (zimaObj.away_user == 1) {
      zimaTitle += "踢人 √";
    }
    if (zimaObj.gap == 1) {
      zimaTitle += "禁止说话 √";
    }
    if (zimaObj.gap_all == 1) {
      zimaTitle += "频道禁言 √";
    }
    if (zimaObj.private_chat == 1) {
      zimaTitle += "私聊 √";
    }
    if (zimaObj.set_admin == 1) {
      zimaTitle += "设置马甲 √";
    }


    if (heimaObj.away_admin == 1) {
      heimaTitle += "踢管理员 √";
    }
    if (heimaObj.away_user == 1) {
      heimaTitle += "踢人 √";
    }
    if (heimaObj.gap == 1) {
      heimaTitle += "禁止说话 √";
    }
    if (heimaObj.gap_all == 1) {
      heimaTitle += "频道禁言 √";
    }
    if (heimaObj.private_chat == 1) {
      heimaTitle += "私聊 √";
    }
    if (heimaObj.set_admin == 1) {
      heimaTitle += "设置马甲 √";
    }


    //如果是超管
    if (_DATA.user.issuper == 1) {

      var touid = $("#popup_info").attr("popup-data-uid");


      var vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
			  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
			  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
			  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
			  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
			 ';

      $.ajax({
        type: "post",
        url: "./index.php?g=home&m=Show&a=selectvest",
        data: {
          uid: _DATA.user.id,
          touid: touid,
          vestLists: vestLists
        },
        success: function (data) {
          document.getElementById('ds-dialog-bg').style.display = 'block';
          $("#selectPlay").css({
            "left": getMiddlePos('selectPlay').pl + "px",
            "z-index": 210,
          }).show();
          $("#selectPlay").html(data);
        }
      });


    }
    //如果是主播
    else if (_DATA.user.id == _DATA.anchor.id) {
      var touid = $("#popup_info").attr("popup-data-uid");

      var vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
			  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
			  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
			  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
			  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
			 ';

      $.ajax({
        type: "post",
        url: "./index.php?g=home&m=Show&a=selectvest",
        data: {
          uid: _DATA.user.id,
          touid: touid,
          vestLists: vestLists
        },
        success: function (data) {
          document.getElementById('ds-dialog-bg').style.display = 'block';
          $("#selectPlay").css({
            "left": getMiddlePos('selectPlay').pl + "px",
            "z-index": 210,
          }).show();
          $("#selectPlay").html(data);
        }
      });


    }

    //如果是普通用户
    else {

      var touid = $("#popup_info").attr("popup-data-uid");

      //获取当前用户的马甲等级id和对方用户的马甲等级
      $.ajax({
        url: '/index.php?g=Home&m=Show&a=checkVest',
        type: 'POST',
        dataType: 'json',
        data: {
          touid: touid,
          liveid: room.id,
          live_type: room.typeId
        },
        success: function (data) {
          var userVestID = data.userVestID; //当前用户的马甲id
          var touidVestID = data.touidVestID; //对方的马甲id

          if (userVestID > touidVestID) {
            var vestLists = "";

            if (userVestID == 7) { //当前用户是黑马


              if (touidVestID == 5) { //对方是橙马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								 ';
              }

              if (touidVestID == 4) { //对方是黄马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 3) { //对方是蓝马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 2) { //对方是绿马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 1) { //对方是白马
                vestLists = '<input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }


            } else if (userVestID == 6) {

              if (touidVestID == 5) { //对方是橙马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								 ';
              }

              if (touidVestID == 4) { //对方是黄马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 3) { //对方是蓝马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 2) { //对方是绿马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }

              if (touidVestID == 1) { //对方是白马
                vestLists = '<input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								  <input  type="button" class="vestBtn" title="' + chengmaTitle + '" value="橙马" data-vest="5" />\
								 ';
              }


            } else if (userVestID == 5) {


              if (touidVestID == 4) { //对方是黄马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								 ';
              }

              if (touidVestID == 3) { //对方是蓝马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								 ';
              }

              if (touidVestID == 2) { //对方是绿马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								 ';
              }

              if (touidVestID == 1) { //对方是白马
                vestLists = '<input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								  <input  type="button" class="vestBtn" title="' + huangmaTitle + '" value="黄马" data-vest="4" />\
								 ';
              }


            } else if (userVestID == 4) { //当前用户是黄马


              if (touidVestID == 3) { //对方是蓝马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								 ';
              }

              if (touidVestID == 2) { //对方是绿马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								 ';
              }

              if (touidVestID == 1) { //对方是白马
                vestLists = '<input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />\
								  <input  type="button" class="vestBtn" title="' + lanmaTitle + '" value="蓝马" data-vest="3" />\
								 ';
              }


            } else if (userVestID == 3) { //当前用户是蓝马

              if (touidVestID == 2) { //对方是绿马
                vestLists = '<input  type="button" class="vestBtn" value="白马" title="' + baimaTitle + '" data-vest="1" />';
              }

              if (touidVestID == 1) { //对方是白马
                vestLists = '<input  type="button" class="vestBtn" title="' + lvmaTitle + '" value="绿马" data-vest="2" />';
              }


            }


            $.ajax({
              type: "post",
              url: "./index.php?g=home&m=Show&a=selectvest",
              data: {
                uid: _DATA.user.id,
                touid: touid,
                vestLists: vestLists
              },
              success: function (data) {
                document.getElementById('ds-dialog-bg').style.display = 'block';
                $("#selectPlay").css({
                  "left": getMiddlePos('selectPlay').pl + "px",
                  "z-index": 210,
                }).show();
                $("#selectPlay").html(data);
              }
            });


          } else {
            layer.msg("您的等级不够,无法设置管理");
            return;
          }
        },
        error: function (data) {
          layer.msg("查询失败,无法设置管理");
        }
      });

    }


  },

  //更改用户马甲
  changeVest: function () {
    var room = new Room();
    var selectVestID = $("#selectVestID").val();

    if (selectVestID) {
      //获取房间id
      // var roomid = _DATA.anchor.id;
      var roomid = room.id;
      var touid = $("#vestToUid").val();
      var user_nicename = $("#vestToUserNiceName").val();
      if (roomid == "" || touid == "" || isNaN(roomid) || isNaN(touid)) {
        layer.msg("参数错误,无法设置马甲");
        return;
      } else {
        var param = {
          liveid: roomid,
          touid: touid,
          vestid: selectVestID,
          live_type: room.typeId
        };
        // if(room.typeId == 0) {
        //     Object.assign(param, {stream: _DATA.live.stream});
        // }

        $.ajax({
          url: '/index.php?g=Home&m=Show&a=changeVest',
          type: 'POST',
          dataType: 'json',
          data: param,
          success: function (data) {
            if (data.code == 200) {
              layer.msg('设置成功', {
                icon: 1
              });
              var user_nicename = data.data.user_nicename;
              var vestName = _DATA.vests[selectVestID]['vest_name'];
              var gap_all = _DATA.vests[selectVestID]['gap_all'];
              User.getOnline(); //更新用户列表
              //发送socket
              var msg = '{"msg":[{"_method_":"changeVest","action":"1","ct":"' + user_nicename + '被更换为' + vestName + '","msgtype":"4","touid":"' + touid + '","gap_all":"' + gap_all + '"}],"retcode":"000000","retmsg":"ok"}';
              Socket.emitData('broadcast', msg);
            } else {
              layer.msg("设置马甲失败");
            }
          },
          error: function (data) {
            layer.msg("设置马甲失败");
          }
        });

        $("#selectPlay").html('');
        $("#selectPlay").hide();
        $("#ds-dialog-bg").hide();
        $("#popup_info").remove();
      }

    } else {
      layer.msg("请选择马甲");
      return;
    }
  }


}
/*
 *弹窗 提示效果
 *
 *调用
 * alert 	:(new Dialog).alert(msg, fn, isclose);							msg 内容 fn 确定/取消执行的function  isclose 点击右上角X 是否执行fn
 * confirm :(new Dialog).confirm(msg, fn, isclose);						msg 内容 fn 确定/取消执行的function  isclose 点击右上角X 是否执行fn
 * tip 		:(new Dialog).tip(content, Ele_contrast, options);  content 提示内容 Ele_contrast 位置对象 options 参数
 *或
 *	var dialog=new Dialog; .alert(msg, fn, isclose);
 *
 */
var Dialog = function () {
  var Dialog = {
    _default: function () {
      var setting = {
        container: null,
        width: "auto",
        height: "auto",
        left: "auto",
        right: "auto",
        top: "auto",
        bottom: "auto",
        lock: !0,
        fixed: !1,
        drag: !0,
        skin: "dds-dialog",
        title: "Dialog Title",
        content: "Dialog Content",
        onOpen: function () {},
        onClosed: function () {},
        bgClosed: !1,
        zIndexCout: "dialogZindexCout",
        initZIndex: 2e3,
        fx: 0,
        timing: !1,
        delay: 5e3,
        tipClose: !1,
        isDestroy: !0,
        towards: "bottom"
      };
      return setting;
    },
    _dislog: function (options) {
      var _default = this._default();
      var setting = $.extend({},
        _default, options);
      for (var name in setting) this[name] = setting[name];
      this.timer = null
    },
    init: function () {
      this.dom(),
        this.setParams(),
        this.addEvent(),
        this.open(),
        this.setFx(),
        this.timing && this.setTiming()
    },
    getH: function () {
      if (!this.isFullScreen) return this.relPanel.height();
      var ch = document.documentElement.clientHeight || document.body.clientHeight,
        sh = document.documentElement.scrollHeight || document.body.scrollHeight;
      return ch < sh ? sh : ch
    },
    dom: function () {
      this.relPanel = this.container,
        this.isFullScreen = !1,
        this.container === null && (this.container = $(document.body), this.relPanel = $(window), this.isFullScreen = !0),
        this.lock && (this.pageBg = $('<div class="' + this.skin + '-bg"></div>'), this.pageBg.css({
          height: this.getH(),
          zIndex: window[this.zIndexCout] || this.initZIndex
        }), this.container.append(this.pageBg)),
        this.boxer = $("<div></div>"),
        this.boxer.addClass(this.skin + "-boxer"),
        this.container.append(this.boxer);
      var str = '<span class="' + this.skin + '-closed"></span>';
      this.boxer.append('<table cellspacing="0" cellpadding="0" border="0"><tbody><tr><td><div class="' + this.skin + '-tl"></div></td>' + '<td><div class="' + this.skin + '-tc">' + '<span class="' + this.skin + '-title">' + this.title + "</span>" + str + "</div></td>" + '<td><div class="' + this.skin + '-tr"></div></td></tr>' + '<tr><td class="' + this.skin + '-cl"></td>' + '<td><div class="' + this.skin + '-cc">' + this.content + "</div></td>" + '<td class="' + this.skin + '-cr"></td></tr>' + '<tr><td><div class="' + this.skin + '-bl"></div></td>' + '<td><div class="' + this.skin + '-bc"></div></td>' + '<td><div class="' + this.skin + '-br"></div></td>' + "</tr></tbody></table>").css({
          width: this.width,
          height: this.height,
          zIndex: window[this.zIndexCout] || this.initZIndex
        }),
        typeof window[this.zIndexCout] != "undefined" ? window[this.zIndexCout] += 1 : window[this.zIndexCout] = this.initZIndex + 1
    },
    setTiming: function () {
      var _this = this;
      this.boxer.bind("mouseover",
        function () {
          clearTimeout(_this.timer)
        }).bind("mouseout",
        function () {
          _this.openTiming()
        })
    },
    openTiming: function () {
      var _this = this;
      this.timer = setTimeout(function () {
          _this.fx === 3 ? _this.boxer.animate({
              opacity: 0
            },
            "200", "swing",
            function () {
              _this.destroy()
            }) : _this.destroy()
        },
        this.delay)
    },
    setFx: function () {
      if (this.fx === 3) {
        this.boxer.css({
            opacity: 0
          }),
          this.boxer.animate({
              opacity: 1
            },
            "400", "swing");
        return
      }
      var l = this.boxer.offset().left;
      if (this.fx === 2) this.boxer.animate({
          left: l + 5
        },
        100, "swing").animate({
          left: l - 5
        },
        100, "swing").animate({
          left: l
        },
        100, "swing");
      else if (this.fx === 1) {
        var t = this.boxer.offset().top,
          w = this.boxer.width(),
          h = this.boxer.height();
        this.boxer.css({
            opacity: 0,
            width: 0,
            height: 0,
            left: l + w / 2,
            top: t + h / 2
          }),
          this.boxer.animate({
              opacity: 1,
              top: t,
              left: l,
              width: w,
              height: h
            },
            200, "swing")
      }
    },
    setBg: function () {
      this.pageBg.css({
        height: this.getH()
      })
    },
    setPos: function (t, l) {
      return this.right != "auto" ? (this.bottom != "auto" ? this.boxer.css({
        right: this.right,
        bottom: this.bottom
      }) : this.boxer.css({
        right: this.right,
        top: t
      }), !1) : (this.bottom != "auto" ? this.boxer.css({
        left: l,
        bottom: t
      }) : this.boxer.css({
        left: l,
        top: t
      }), !1)
    },
    setParams: function () {
      var tc = $("." + this.skin + "-tc", this.boxer),
        tl = $("." + this.skin + "-tl", this.boxer),
        tr = $("." + this.skin + "-tr", this.boxer),
        cl = $("." + this.skin + "-cl", this.boxer),
        cc = $("." + this.skin + "-cc", this.boxer),
        cr = $("." + this.skin + "-cr", this.boxer),
        bl = $("." + this.skin + "-bl", this.boxer),
        bc = $("." + this.skin + "-bc", this.boxer),
        br = $("." + this.skin + "-br", this.boxer),
        w = this.boxer.width(),
        h = this.boxer.height();
      tc.css({
          width: w - tl.width() - tr.width()
        }),
        bc.css({
          width: w - tl.width() - tr.width()
        }),
        this.height != "auto" && cc.css({
          height: h - tl.height() - tr.height()
        }),
        this.width != "auto" && cc.css({
          width: w - tl.width() - tr.width()
        });
      var t = (this.relPanel.height() - h) / 2,
        l = (this.relPanel.width() - w) / 2;
      this.relPanel.height() < h && (t = 15),
        this.fixed || (t += $(document).scrollTop(), l += $(document).scrollLeft()),
        t = this.top != "auto" ? this.top : t,
        l = this.left != "auto" ? this.left : l,
        this.setPos(t, l)
    },
    reWAH: function (width, height) {
      this.width = width,
        this.height = height,
        this.boxer.css({
          width: width,
          height: height
        });
      var w = this.boxer.width(),
        h = this.boxer.height(),
        cc = $("." + this.skin + "-cc", this.boxer),
        tl = $("." + this.skin + "-tl", this.boxer),
        tr = $("." + this.skin + "-tr", this.boxer);
      this.height != "auto" && cc.css({
          height: h - tl.height() - tr.height()
        }),
        this.width != "auto" && cc.css({
          width: w - tl.width() - tr.width()
        }),
        this.reSize()
    },
    reSize: function () {
      var t = (this.relPanel.height() - this.boxer.height()) / 2,
        l = (this.relPanel.width() - this.boxer.width()) / 2;
      this.relPanel.height() < this.boxer.height() && (t = 15),
        this.fixed || (t += $(document).scrollTop(), l += $(document).scrollLeft()),
        t = this.top != "auto" ? this.top : t,
        l = this.left != "auto" ? this.left : l,
        this.setPos(t, l),
        this.lock && this.setBg()
    },
    addEvent: function () {
      var _this = this;
      this.fixed && $(this.boxer).css({
          position: "fixed"
        }),

        $("." + this.skin + "-closed", this.boxer).bind("click",
          function () {
            _this.destroy()
          }).hover(function () {
            $(this).addClass(_this.skin + "-closed-over")
          },
          function () {
            $(this).removeClass(_this.skin + "-closed-over")
          }),
        $(this.relPanel).bind("resize",
          function () {
            _this.reSize()
          }),
        this.bgClosed && this.pageBg.bind("click",
          function () {
            _this.destroy()
          })
    },
    open: function () {
      this.show(),
        this.onOpen(this)
    },
    show: function () {
      this.pageBg && this.pageBg.css("display", "block"),
        $(this.boxer).css("display", "block")
    },
    close: function () {
      this.pageBg && this.pageBg.css("display", "none"),
        $(this.boxer).css("display", "none"),
        this.onClosed()
    },
    destroy: function () {
      this.close();
      if (!this.isDestroy) return !1;
      this.pageBg && this.pageBg.remove(),
        $(this.boxer).remove()
    },
    alert: function (content, fun, isclose) {
      this._dislog();
      this.title = "提示";
      var _this = this,
        txt = content || "";
      return this.content = '<div class="' + this.skin + '-alert">' + '<div class="' + this.skin + '-txt">' + txt + "</div>" + '<div class="' + this.skin + '-btnc">' + '<input type="button" value="" class="' + this.skin + '-btn"/>' + "</div></div>",
        this.onOpen = function (Class) {
          var btn = $("input", Class.boxer);
          btn.click(function () {
              Class.destroy(),
                fun && $.isFunction(fun) && fun()
            }),
            fun && isclose && $("." + Class.skin + "-closed", Class.boxer).bind("click",
              function () {
                fun()
              })
        },
        this.init(),

        this
    },
    confirm: function (content, fun, isclose) {
      this._dislog();
      this.title = "询问";
      var txt = content || "";
      return this.content = '<div class="' + this.skin + '-confirm"><div class="' + this.skin + '-txt">' + txt + "</div>" + '<div class="' + this.skin + '-btn">' + '<input type="button" value="" class="' + this.skin + '-sure" />' + '<input type="button" value="" class="' + this.skin + '-cancel" /></div>',
        this.onOpen = function (Class) {
          $("." + Class.skin + "-cancel", Class.boxer).click(function () {
              Class.destroy(),
                fun && $.isFunction(fun) && fun(!1)
            }),
            $("." + Class.skin + "-sure", Class.boxer).click(function () {
              Class.destroy(),
                fun && $.isFunction(fun) && fun(!0)
            }),
            fun && isclose && $("." + Class.skin + "-closed", Class.boxer).bind("click",
              function () {
                fun(!1)
              })
        },
        this.init(),
        this
    },
    tip: function (content, Ele_contrast, options) {
      this._dislog();
      var _this = this;
      this.title = "",
        this.content = '<div class="' + this.skin + '-con">' + content + "</div>",
        this.lock = !1,
        this.drag = !1,
        this.fx = 3,
        this.timing = !0,
        this.emptySpace = 1,
        this.repairSpace = 0;
      var contrast = $(Ele_contrast),
        top = contrast.offset().top,
        left = contrast.offset().left,
        width = contrast.width(),
        height = contrast.height();
      for (var name in options) this[name] = options[name];
      this.dom(),
        this.boxer.addClass(this.skin + "-tip");
      if (this.tipClose) {
        var _closed = $("." + this.skin + "-closed", this.boxer);
        _closed.addClass(this.skin + "-closed-on"),
          _closed.bind("click",
            function () {
              _this.destroy()
            }).hover(function () {
              $(this).addClass(_this.skin + "-closed-over")
            },
            function () {
              $(this).removeClass(_this.skin + "-closed-over")
            })
      }
      var w = this.boxer.width(),
        h = this.boxer.height(),
        arrowsPos = {
          top: 0,
          left: 0
        };
      return this.towards === "bottom" ? (this.top = top - h - this.emptySpace - 6 + this.repairSpace, arrowsPos = {
            top: h - this.emptySpace,
            left: width / 2
          },
          width <= w ? (this.left = left, left + w >= $(this.relPanel).width() && (this.left = left - w + width, arrowsPos.left = w - width / 2 - 8)) : (this.left = left + (width - w) / 2, arrowsPos.left = w / 2)) : this.towards === "top" ? (this.top = top + h + this.emptySpace + this.repairSpace, arrowsPos = {
            top: -6 + this.emptySpace,
            left: width / 2
          },
          width <= w ? (this.left = left, left + w >= $(this.relPanel).width() && (this.left = left - w + width, arrowsPos.left = w - width / 2 - 8)) : (this.left = left + (width - w) / 2, arrowsPos.left = w / 2)) : this.towards === "left" ? (this.top = top - h / 2 + height / 2, arrowsPos = {
            top: (h - 20) / 2,
            left: -5
          },
          this.left = left + this.emptySpace + width + 8 + this.repairSpace) : this.towards === "right" && (this.top = top - h / 2 + height / 2, arrowsPos = {
            top: (h - 20) / 2,
            left: w - 5
          },
          this.left = left - this.emptySpace - w - 8 - this.repairSpace),
        this.boxer.css({
          top: this.top,
          left: this.left
        }),
        this.boxer.append('<div class="' + this.skin + "-arrows " + this.skin + "-" + this.towards + '" style="left:' + arrowsPos.left + "px;top:" + arrowsPos.top + 'px;"></div>'),
        this.onOpen(this),
        this.setFx(),
        this.timing && (this.setTiming(), this.openTiming()),
        this
    }
  };
  return Dialog;
};
/*
	*滚动条
	*调用
	*var scroll=new Scroll;
					scroll.init({
							Ele_panel: '.MR-guard-container .list-inner',  //内容区域
							Ele_scroll: '.MR-guard-container .scroller'	//滚动条
					});
 */
var Scroll = function () {
  var Scroll = {
    _default: function () {
      var setting = {
        Ele_panel: null,
        Ele_scroll: null,
        fixedHeight: 0,
        height: "auto",
        width: "auto",
        rollerX: !1,
        rollerY: !0,
        flow: !1,
        minHeight: 15,
        minWidth: 15,
        skin: "dds-scroll",
        onScroll: function (type, positionPercent) {}
      };
      return setting;
    },
    _scroll: function (options) {
      var _default = this._default();
      var setting = $.extend({},
        _default, options);
      for (var name in setting) this[name] = setting[name];
      this.scrollCbTimer = null
    },
    init: function (options) {

      this._scroll(options),
        this.Ele_panel = $(this.Ele_panel),
        this.panel_parent = this.Ele_panel.parent(),
        this.Ele_scroll = $(this.Ele_scroll),
        this.rollerY ? (this.height = this.height == "auto" ? this.panel_parent.height() : this.height, this.setHeight(this.height)) : this.rollerX && (this.width = this.width == "auto" ? this.panel_parent.width() : this.width, this.setWidth(this.width)),
        this.flow && this.Ele_scroll.hide(),
        this.dom()
    },
    setHeight: function (h) {
      this.height = h,
        this.panel_parent.css({
          height: this.height
        }),
        this.Ele_scroll.css({
          height: this.height
        }),
        this.scrollPanel && (this.scrollPanel.css({
          height: this.height
        }), this.resetH())
    },
    setWidth: function (w) {
      this.width = w,
        this.panel_parent.css({
          width: this.width
        }),
        this.Ele_scroll.css({
          width: this.width
        }),
        this.scrollPanel && (this.scrollPanel.css({
          width: this.width
        }), this.resetW())
    },
    dom: function () {
      this.scrollPanel = $('<div class="' + this.skin + '-panel"><div class="top-bg"></div><div class="bottom-bg"></div></div>'),
        this.scrollPanel.css({
          height: this.height
        }),
        this.slider = $('<div class="' + this.skin + '-slider"><div class="top-bg"></div><div class="bottom-bg"></div></div>'),
        this.scrollPanel.append(this.slider),
        this.Ele_scroll.append(this.scrollPanel),
        this.rollerY ? this.resetH() : this.rollerX && (this.scrollPanel.addClass(this.skin + "-panel-x"), this.resetW()),
        this.addEvent()
    },
    clearScreen: function () {
      this.rollerY ? (this.resetH(), this.slider.css({
        top: 0
      }), this.panel_parent.scrollTop(0)) : this.rollerX && (this.resetW(), this.slider.css({
        left: 0
      }), this.panel_parent.scrollLeft(0))
    },
    addEvent: function () {
      var _this = this;
      this.slider.hover(function () {
            $(this).addClass(_this.skin + "-over")
          },
          function () {
            $(this).removeClass(_this.skin + "-over")
          }),
        this.scrollPanel.bind("click",
          function (e) {
            if (_this.slider.css("display") != "none") {
              var source = e.srcElement || e.target;
              if (source.className != _this.skin + "-panel") return;
              if (_this.rollerY) {
                var _y = e.pageY - _this.scrollPanel.offset().top,
                  _top = _this.slider.position().top;
                _y >= _top ? _this.slider.css({
                  top: _y - _this.slider.height()
                }) : _this.slider.css({
                  top: _y
                })
              } else if (this.rollerX) {
                var _x = e.pageX - _this.scrollPanel.offset().left,
                  _left = _this.slider.position().left;
                _x >= _left ? _this.slider.css({
                  left: _x - _this.slider.width()
                }) : _this.slider.css({
                  left: _x
                })
              }
              _this.scrollTxt()
            }
          });
      var options = {};
      this.rollerY ? _options = {
        lockX: !0,
        limit: !0,
        mxbottom: this.height,
        onMove: function () {
          _this.scrollTxt()
        }
      } : this.rollerX && (_options = {
        lockY: !0,
        limit: !0,
        mxright: this.width,
        onMove: function () {
          _this.scrollTxt()
        }
      });

      var isFF = navigator.userAgent.indexOf("Firefox") >= 0 ? !0 : !1;
      isFF ? this.Ele_panel[0].addEventListener("DOMMouseScroll",
        function (e) {
          e.preventDefault();
          var delta = -e.detail / 3;
          delta && (_this.rollerY ? _this.panel_parent[0].scrollTop -= delta * 20 : _this.rollerX && (_this.panel_parent[0].scrollLeft -= delta * 20), _this.setScrollPos())
        }, !1) : this.Ele_panel[0].onmousewheel = function (e) {
        var e = e ? e : window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = !1;
        var delta = e.wheelDelta ? e.wheelDelta / 120 : -e.detail / 3;
        delta && (_this.rollerY ? _this.panel_parent[0].scrollTop -= delta * 20 : _this.panel_parent[0].scrollLeft -= delta * 20, _this.setScrollPos())
      }
    },
    setScrollPos: function () {
      if (this.rollerY) {
        var H = this.Ele_panel.height(),
          _h = this.panel_parent[0].scrollTop,
          _top = _h * this.height / H;
        _h >= H - this.height && (_top = this.height - this.slider.height()),
          this.slider.css({
            top: _top
          }),
          this.onScrollCb("y", _h / (H - this.height))
      } else if (this.rollerX) {
        var W = this.Ele_panel.width(),
          _w = this.panel_parent[0].scrollLeft,
          _left = _w * this.width / W;
        _w >= W - this.width && (_left = this.width - this.slider.width()),
          this.slider.css({
            left: _left
          }),
          this.onScrollCb("x", _w / (W - this.width))
      }
    },
    scrollTxt: function () {
      if (this.rollerY) {
        var _top = this.slider.position().top,
          H = this.Ele_panel.height(),
          _h = _top * H / this.height;
        _top <= 0 ? _h = 0 : _top >= this.height - this.slider.height() && (_h = H - this.height),
          this.panel_parent.scrollTop(_h),
          this.onScrollCb("y", _h / (H - this.height))
      } else if (this.rollerX) {
        var _left = this.slider.position().left,
          W = this.Ele_panel.width(),
          _w = _left * W / this.width;
        _left <= 0 ? _w = 0 : _left >= this.width - this.slider.width() && (_w = W - this.width),
          this.panel_parent.scrollLeft(_w),
          this.onScrollCb("x", _w / (W - this.width))
      }
    },
    resetW: function () {
      var w = this.coutW();
      this.slider.css({
          width: w
        }),
        w <= 0 || this.width < this.minWidth ? (this.slider.hide(), this.scrollPanel.hide()) : (this.slider.show(), this.scrollPanel.show()),
        w + this.slider.position().left > this.width && this.toRight()
    },
    resetH: function () {
      var h = this.coutH();
      this.slider.css({
          height: h
        }),
        h <= 0 || this.height < this.minHeight ? (this.slider.hide(), this.scrollPanel.hide()) : (this.slider.show(), this.scrollPanel.show()),
        h + this.slider.position().top > this.height && this.toBottom()
    },
    toRight: function () {
      if (this.coutW() <= 0) return !1;
      this.slider.css({
          left: this.width - this.coutW()
        }),
        this.panel_parent.scrollLeft(99999)
    },
    toLeft: function () {
      this.slider.css({
          left: 0
        }),
        this.panel_parent.scrollTop(0)
    },
    toBottom: function () {
      if (this.coutH() <= 0) return !1;
      this.slider.css({
          top: this.height - this.coutH()
        }),
        this.panel_parent.scrollTop(99999)
    },
    toTop: function () {
      this.slider.css({
          top: 0
        }),
        this.panel_parent.scrollTop(0)
    },
    coutW: function () {
      var W = this.Ele_panel.width();
      return W <= this.width ? 0 : this.width * this.width / W
    },
    coutH: function () {
      var H = this.Ele_panel.height();
      return H == 0 && this.fixedHeight != 0 && (H = this.fixedHeight),
        H <= this.height ? 0 : this.height * this.height / H
    },
    onScrollCb: function (type, percent) {
      this.scrollCbTimer && (clearTimeout(this.scrollCbTimer), this.scrollCbTimer = null);
      var _this = this;
      this.scrollCbTimer = setTimeout(function () {
          _this.onScroll.call(_this, type, percent),
            _this.scrollCbTimer = null
        },
        200)
    }
  };
  return Scroll;
};


/* 页面缩放高度范围 */
var minHeight = 640;
var maxHeight = 5960;

/* 虚拟币名称 */
var coin_text = _DATA.getConfigPub.name_coin;

/* 工具方法 */
var WlTools = {
  FormatNowDate: function () {
    var mDate = new Date();
    var H = mDate.getHours();
    var i = mDate.getMinutes();
    var s = mDate.getSeconds();
    if (H < 10) {
      H = '0' + H;
    }
    if (i < 10) {
      i = '0' + i;
    }
    if (s < 10) {
      s = '0' + s;
    }
    //return H + ':' + i + ':' + s;
    return H + ':' + i;
  },
  /**
   *  JS版  数字 金额格式化
   * @param string s 需要处理的数字串
   * @param string n 保留小数的位数
   */
  fmoney: function (s, n) {
    n = n >= 0 && n <= 20 ? n : 2;
    s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; //更改这里n数也可确定要保留的小数位
    var l = s.split(".")[0].split("").reverse(),
      r = s.split(".")[1];
    t = "";
    for (i = 0; i < l.length; i++) {
      t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
    }
    if (n > 0) {
      return t.split("").reverse().join("") + "." + r.substring(0, n); //保留2位小数  如果要改动 把substring 最后一位数改动就可
    } else {
      return t.split("").reverse().join("");
    }
  },
  jsonTostr: function (json) {
    return JSON.stringify(json);
  },
  strTojson: function (str) {
    return JSON.parse(str);
  }

};

/* 判断 */
var Check = {
  checkCoin: function (needcoin) {
    if (needcoin > _DATA.user.coin) {
      return !1;
    } else {
      return !0;
    }
  },
  checkLogin: function () {
    if (_DATA.user && _DATA.user.logged == 1) { // 此处判断登录的逻辑与凯发不同，需使用logged参数判断， 0表示未登录，1表示已登录
      return !0;
    } else {
      // $(".hd-login .no-login").click();

      layer.confirm('登录后才可以和主播互动呦，还可以参与各种活动', {

        btn: ['确定'] //按钮

      }, function (index) {

        layer.close(index);

      });
      return !1;
    }

  },
  checkNum: function (num) {
    var patrn = /^(\d)*$/;
    if (patrn.test(num)) {
      return !0;
    } else {
      return !1;
    }
  }
};

/* 会员 */
var User = {
  userMoney: $(".MR-user .money"),
  listBtn: $("#LF-toggle-online"),
  attionBtn: $(".BTN-add-attention"),
  moreListBtn: $(".MR-online .user-con .more-toggle"),
  userCon: $(".MR-online .user-con .boarder ul"),
  userCite: $(".MR-online .nav-tab cite"),
  adiCon: $(".MR-online .admin-con .boarder ul"),
  user_Ele_panel: '.MR-online .user-con .boarder ul',
  user_Ele_scroll: '.MR-online .user-con .scroller',
  adi_Ele_panel: '.MR-online .admin-con .boarder ul',
  adi_Ele_scroll: '.MR-online .admin-con .scroller',
  showUserListUrl: './index.php?g=home&m=show&a=getUserList',
  attionUrl: './index.php?g=home&m=show&a=attention',
  attionNumUrl: './index.php?g=home&m=show&a=getattentionnums',
  popupInfo: './index.php?g=home&m=show&a=popupInfo',
  info: $(".SR-area-info"),
  minus_h: 475,
  defnums: 20,
  user_ismore: 1,
  user_times: 0,
  user_isfirst: 1,
  adi_isfirst: 1,
  popup_to: 85,
  init: function () {
    this.setUsersScorll();
    this.setUserHeight();
    this.addEvent();
  },
  /*用户列表弹窗popup*/
  /***
   * 10游客 30观众 40管理员 50房间主播 60超管  70麦手
   * 黑马 7 紫马 6 橙马  5 黄马  4 蓝马  3 绿马  2 白马  1
   ***/

  popup: function (id) {
    var room = new Room();
    var _this = this;
    if ($("#popup_info").length > 0) {

      $("#popup_info").remove();
    }

    // debugger;
    var top = $("#anchor_" + id).position().top;
    var parentHeight = $("#nav_con").height();
    // var winHeight = $(window).height();
    // winHeight = winHeight - 65;
    if (parentHeight - top < 140 && id != _DATA.user.id) {
      top = top - 80;
    } else {
      top += 38;
    }
    // top = top + 360;
    var html = '';

    // if (_DATA.user != null) {
    // if (_DATA.user && _DATA.user.logged == 1) { //已登录
    if (_DATA.user) { //已登录

      // if (parseFloat(id) > 0 && id != _DATA.user.id) {
      if (id != _DATA.user.id) {
        $.ajax({
          type: "POST",
          url: this.popupInfo,
          // data: {touid: id, roomid: _DATA.anchor.id},
          data: {
            touid: id,
            liveid: room.id,
            live_type: room.typeId
          },
          /*async: false,*/
          error: function (request) {
            layer.msg("网络请求出错！");
          },
          success: function (data) {
            var data = JSON.parse(data);
            data.uid_isTourist = (_DATA.user.id.indexOf('temp') != -1); //当前用户是否是游客
            data.touid_isTourist = (id.indexOf('temp') != -1); //对方用户是否是游客
            if (!data.info) { //如果info不存在，则说明游客已下线
              layer.msg("此游客已离开房间");
              return;
            }

            // 发起私聊与新版IM对接所需数据 noah.g
            var NHinfo = data.info;
            var NHavatar = NHinfo.avatar;
            var NHid = NHinfo.id;
            var NHnickname = NHinfo.user_nicename;
            /**
             *   50房间主播 60超管 70麦手， 在这里前端做一个转换， 将麦手数值变为49， 方便逻辑处理
             * @type {number}
             */
            var uid_admin = parseInt(data.uid_admin);
            var touid_admin = parseInt(data.touid_admin);
            var uid_vestid = parseInt(data.uid_vestid);
            var touid_vestid = parseInt(data.touid_vestid);
            if (uid_admin == 70) {
              uid_admin = 49;
            }
            if (touid_admin == 70) {
              touid_admin = 49;
            }

            html += '<div class="popup" id="popup_info" popup-data-name="' + data.info.user_nicename + '" popup-data-uid="' + data.info.id + '" style="top:' + top + 'px;display:none"><div class="pop_content">';
            html += '<div class="popup_img"><img src="' + data.info.avatar + '"/></div><div class="handle"><div class="name">昵称:<a>' + data.info.user_nicename + '</a></div>';
            // debugger;

            if (uid_admin > 10) {

              if (uid_admin == 60) { /*当前身份为超管时*/

                if (touid_admin == 50) { //对方身份为主播
                  if (room.typeId == 0) {
                    html += '<span onclick="Controls.stopit()" id="popup_stopit">停止直播</span>';
                    // html += '<span onclick="Controls.close()" id="popup_close">关闭直播</span>';
                  } else if (room.typeId == 1) {
                    var touid = data.info.id;
                    if (rtc.liveStreams && rtc.liveStreams[touid]) { //该主播正在直播
                      html += '<span id="popup_stop_live" data-uid="' + touid + '" data-uname="' + data.info.user_nicename + '" >关闭直播</span>';
                    }
                  }
                } else if (touid_admin == 49) { //对方身份为麦手
                  if (room.typeId == 0) {
                    //单直播间暂时无麦手
                  } else if (room.typeId == 1) {
                    var touid = data.info.id;
                    if (rtc.macStreams && rtc.macStreams[touid]) { //该麦手正在直播
                      html += '<span id="popup_stop_mac" data-uid="' + touid + '" data-uname="' + data.info.user_nicename + '" >关闭喊麦</span>';
                    }
                  }

                } else if (touid_admin < 60) { //对方身份不为超管

                  if (!data.touid_isTourist) {
                    html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                  }
                  if (touid_vestid == 1) {
                    html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                  }
                  if (touid_vestid > 1) {
                    html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                  }

                  if (touid_vestid < 6 && !data.touid_isTourist) {
                    html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                  }
                }
                // html += '<span onclick="Controls.HxChat_User()" id="HxChat_User">发起私聊</span>';
                // html += '<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';

              } else if (uid_admin == 50) { /* 当前用户为主播 */

                if (room.typeId == 0) { //单直播间
                  if (touid_admin < 60) { //当对方身份不为超管时

                    if (!data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }

                    if (touid_vestid == 1) {

                      html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                    }

                    if (touid_vestid > 1) {

                      html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                    }

                    if (touid_vestid < 6 && !data.touid_isTourist) {

                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }

                  }
                } else if (room.typeId == 1) { //多直播间
                  if (touid_admin < 49) {

                    if (!data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }

                    if (touid_vestid == 1) {

                      html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                    }

                    if (touid_vestid > 1) {

                      html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                    }

                    if (touid_vestid < 6 && !data.touid_isTourist) {

                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }
                  }
                }
                // html += '<span onclick="Controls.HxChat_User()" id="popup_tiren">发起私聊</span>';
                // html += '<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';

              } else if (uid_admin == 49) { /* 当前用户为麦手 */


                if (touid_admin < 50) { //当对方身份不为主播 及超管 时

                  if (!data.touid_isTourist) {
                    html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                  }

                  if (touid_vestid == 1) {

                    html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                  }

                  if (touid_vestid > 1) {

                    html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                  }

                  if (touid_vestid < 6 && !data.touid_isTourist) {

                    html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                  }

                }
                // html += '<span onclick="Controls.HxChat_User()" id="popup_tiren">发起私聊</span>';
                // html += '<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
              } else if (uid_vestid == 7) { //当前用户为黑马
                if (touid_admin <= 30) {
                  if (touid_vestid < 6) { //对方是紫马及以下
                    //获取黑马的权限

                    //踢管理员
                    if (_DATA.vests['7']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['7']['away_user'] == 1 && data.info.iswhite == 0) {
                      if (touid_vestid == 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                      }
                    }
                    //禁言
                    if (_DATA.vests['7']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    if (_DATA.vests['7']['set_admin'] == 1 && !data.touid_isTourist) {
                      if (touid_vestid < 6) {
                        html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                      }


                    }
                    //私聊
                    // if (_DATA.vests['7']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }


                  }

                }


              } else if (uid_vestid == 6) { //当前用户为紫马
                if (touid_admin <= 30) {

                  if (touid_vestid < 6) { //对方是橙马及以下
                    //获取紫马的权限

                    //踢管理员
                    if (_DATA.vests['6']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['6']['away_user'] == 1 && data.info.iswhite == 0) {
                      if (touid_vestid == 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                      }
                    }
                    //禁言
                    if (_DATA.vests['6']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    if (_DATA.vests['6']['set_admin'] == 1 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }
                    //私聊
                    // if (_DATA.vests['6']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }
                  }
                }

              } else if (uid_vestid == 5) { //当前用户为橙马

                if (touid_admin <= 30) {

                  if (touid_vestid < 5) {

                    //踢管理员
                    if (_DATA.vests['5']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['5']['away_user'] == 1 && data.info.iswhite == 0) {
                      if (touid_vestid == 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                      }
                    }
                    //禁言
                    if (_DATA.vests['5']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    if (_DATA.vests['5']['set_admin'] == 1 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }
                    //私聊
                    // if (_DATA.vests['5']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }
                  }


                }


              } else if (uid_vestid == 4) { //当前用户是黄马

                if (touid_admin <= 30) {

                  if (touid_vestid < 4) {

                    //踢管理员
                    if (_DATA.vests['4']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['4']['away_user'] == 1 && data.info.iswhite == 0) {
                      if (touid_vestid == 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                      }
                    }
                    //禁言
                    if (_DATA.vests['4']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    if (_DATA.vests['4']['set_admin'] == 1 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }
                    //私聊
                    // if (_DATA.vests['4']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }
                  }

                }


              } else if (uid_vestid == 3) { //当前用户是蓝马
                if (touid_admin <= 30) {
                  if (touid_vestid < 3) {

                    //踢管理员
                    if (_DATA.vests['3']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['3']['away_user'] == 1 && data.info.iswhite == 0) {
                      if (touid_vestid == 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                      }
                    }
                    //禁言
                    if (_DATA.vests['3']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    if (_DATA.vests['3']['set_admin'] == 1 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置马甲</span>';
                    }
                    //私聊
                    // if (_DATA.vests['3']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }
                  }
                }


              } else if (uid_vestid == 2) { //当前用户是绿马

                if (touid_admin <= 30) {

                  if (touid_vestid < 2) {

                    //踢管理员
                    if (_DATA.vests['2']['away_admin'] == 1 && data.info.iswhite == 0) {

                      if (touid_vestid > 1) {
                        html += '<span onclick="Controls.tiren()" id="popup_tiren">踢管理员</span>';
                      }
                    }
                    //踢人
                    if (_DATA.vests['2']['away_user'] == 1 && data.info.iswhite == 0) {
                      html += '<span onclick="Controls.tiren()" id="popup_tiren">踢出房间</span>';
                    }
                    //禁言
                    if (_DATA.vests['2']['gap'] == 1 && data.info.iswhite == 0 && !data.touid_isTourist) {
                      html += '<span onclick="Controls.gag()" id="popup_gag">禁止发言</span>';
                    }
                    //设置管理
                    /*if(_DATA.vests['2']['set_admin']==1){
												html+='<span onclick="Controls.setAdmin()" id="popup_setAdmin">设置管理</span>';
											}*/
                    //私聊
                    // if (_DATA.vests['2']['private_chat'] == 1) {
                    //     // html += '<span onclick="Controls.HxChat_User()" id="popup_cancel">私聊</span>';
                    //     html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">发起私聊</span>';
                    // }
                  }
                }


              }


              //判断对方身份 只有对方不是游客都 并且 自己也不是游客
              if (touid_admin > 10 && !data.uid_isTourist) {

                if (data.isBlack == 1) {
                  html += '<span onclick="Controls.black()" id="popup_black">移除拉黑</span>';
                } else {
                  html += '<span onclick="Controls.black()" id="popup_black">加黑名单</span>';
                }
                //html+='<span onclick="Controls.HxChat_User()" id="HxChat_User">开始私聊</span>';

              }


              // if(html.indexOf("私聊") == -1) {
              //     //当对方为超管或者房间的主播，则所有人都可以对其聊天
              //     if(touid_admin == 60) {
              //         // html+='<span onclick="Controls.HxChat_User()" id="HxChat_User">开始私聊</span>';
              //         html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">开始私聊</span>';
              //     }
              //     //当对方为房间的主播或麦手，则所有人都可以对其聊天
              //     else if(room.typeId == 0 && data.info && data.info.id == _DATA.live.uid) {
              //         // html+='<span onclick="Controls.HxChat_User()" id="HxChat_User">开始私聊</span>';
              //         html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">开始私聊</span>';
              //     }
              //     else if(room.typeId == 1 && data.info) {
              //         var anchors =  _DATA.multiLive.anchor;
              //         $.each(anchors, function(i, v) {
              //             if(v.uid == data.info.id) {
              //                 // html+='<span onclick="Controls.HxChat_User()" id="HxChat_User">开始私聊</span>';
              //                 html +='<span layim-event="chat" data-type="history" data-index="0" data-id="'+NHid+'" data-picurl="'+NHavatar+'" data-uname="' + NHnickname + '" id="layim-friend'+NHid+'">开始私聊</span>';
              //             }
              //         });

              //         // var djs =  _DATA.multiLive.dj;
              //         // $.each(djs, function(i, v) {
              //         //     if(v.uid == data.info.id) {
              //         //         html+='<span onclick="Controls.HxChat_User()" id="HxChat_User">开始私聊</span>';
              //         //     }
              //         // });
              //     }
              // }
            }

            // 判断是否允许私聊 private_chat字段  0-不可以聊，1-可以聊 noah.g 
            if (base_data.app_key === 'xsm') {
              if (data.private_chat === 1) {
                html += '<span layim-event="chat" data-type="history" data-index="0" data-id="' + NHid + '" data-picurl="' + NHavatar + '" data-uname="' + NHnickname + '" id="layim-friend' + NHid + '">开始私聊</span>';
              }
            }
            html += '</div></div></div>';
            $("#nav_con").append(html);
            $("#popup_info").fadeIn();
          }
        });

      } else if (id == _DATA.user.id) {

        html += '<div class="popupOwn" id="popup_info" style="top:' + top + 'px;display:none"><div class="pop_contentOwn">';
        html += '找到自己啦！';
        html += '</div></div>';
        $("#nav_con").append(html);
        $("#popup_info").fadeIn();
      }
    }
  },
  /* 用户列表 */
  setUsersScorll: function () {
    this.usersscroll = new Scroll;
    this.usersscroll.init({
      Ele_panel: this.user_Ele_panel,
      Ele_scroll: this.user_Ele_scroll
    });
    this.usersscroll.toBottom();
  },
  resetusersH: function (height, h) {

    $(".MR-online .user-con .boarder").css("height", h - 30);
    //     this.usersscroll.setHeight(h - 40),
    //     this.usersscroll.resetH(),
    //     this.usersscroll.toBottom();
  },
  resetuserssH: function () {
    this.usersscroll.resetH(),
      this.usersscroll.toBottom();
  },
  setUserHeight: function () {
    var _this = this;
    var height = $(window).height();
    if (height < minHeight) {
      height = minHeight;
    } else if (height > maxHeight) {
      height = maxHeight;
    }
    // var h = height - _this.minus_h;
    var h = ($('#LF-stager').height() - $('.ck-slide').height()) / 2;
    // this.info.css("height", height);
    _this.resetusersH(height, h);
  },
  /* 管理员列表 */
  setAdiScorll: function () {
    this.adiscroll = new Scroll;
    this.adiscroll.init({
      Ele_panel: this.adi_Ele_panel,
      Ele_scroll: this.adi_Ele_scroll
    });
    this.adiscroll.toBottom();
  },
  resetadiH: function (height, h) {
    // $(".MR-online .admin-con .boarder").css("height", h),
    //     this.adiscroll.setHeight(h),
    //     this.adiscroll.resetH(),
    //     this.adiscroll.toBottom();
  },
  resetadisH: function () {
    this.adiscroll.resetH(),
      this.adiscroll.toBottom();
  },
  setAdiHeight: function () {
    var _this = this;
    var height = $(window).height();
    if (height > minHeight && height < maxHeight) {
      var h = height - _this.minus_h;
      _this.resetadiH(height, h);
    }
  },
  addEvent: function () {
    var _this = this;
    $(window).on("resize", function () {
      _this.setUserHeight();
    })

    // $(document).mouseup(function (e) {
    //     var _con = $('#LF-nav-bg,#LF-toggle-online');   // 设置目标区域

    //     if (_con.hasClass("bg-show")) {
    //         if (!_con.is(e.target) && _con.has(e.target).length === 0) { // Mark 1
    //             _this.closeNavBg();

    //         }
    //     }

    // });

    this.listBtn.on("click", function () {
      if ($(this).hasClass("on")) {
        if ($("#popup_info").length > 0) {
          $("#popup_info").remove();
        }
        $(this).removeClass("on");
        _this.closeNavBg();
      } else {
        $(this).addClass("on");
        _this.showNavBg();
      }

    })
    this.moreListBtn.on("click", function () {
      _this.getOnline();
    })
    this.attionBtn.on("click", function () {
      _this.setAttention();

    })


    /**
     * 超管关闭多直播间的直播
     */
    $("#nav_con").on('click', ' #popup_stop_live', function () {
      var uid = $(this).attr('data-uid');
      var uname = $(this).attr('data-uname');
      var room = new Room();
      var msg = {
        "msg": [{
          "_method_": "multiStopLive",
          "action": 1,
          "ct": '关闭直播',
          "uname": _DATA.user.user_nicename,
          "touid": uid,
          "touname": uname,
          "roomid": room.id
        }],
        "retcode": "000000",
        "retmsg": "ok"
      };
      Socket.emitData('broadcast', JSON.stringify(msg));
    });

    /**
     * 超管关闭多直播间的喊麦
     */
    $("#nav_con").on('click', ' #popup_stop_mac', function () {
      var uid = $(this).attr('data-uid');
      var uname = $(this).attr('data-uname');
      var room = new Room();
      var msg = {
        "msg": [{
          "_method_": "multiStopMac",
          "action": 1,
          "ct": '关闭喊麦',
          "touid": uid,
          "touname": uname,
          "roomid": room.id
        }],
        "retcode": "000000",
        "retmsg": "ok"
      };
      Socket.emitData('broadcast', JSON.stringify(msg));
    });


  },
  setAttention: function () {
    var _this = this;
    if (!Check.checkLogin()) {
      //(new Dialog).tip("请登录", _this.chatSendBtn, {delay:2e3});
      return !1;
    }
    var attion = _this.attionBtn.html();
    if (attion == '已关注') {
      //(new Dialog).tip("请登录", _this.chatSendBtn, {delay:2e3});
      return !1;
    }
    $.ajax({
      url: _this.attionUrl,
      data: {
        roomnum: _DATA.anchor.id
      },
      dataType: 'json',
      success: function (data) {
        if (data.error == 0) {
          _this.attionBtn.html(data.msg);
          $(".fans-num").html(data.data);
          var ct = _DATA.user.user_nicename + "关注了主播";
          var msg = '{"msg":[{"_method_":"SystemNot","action":"13","ct":"' + ct + '","msgtype":"4","uname":"' + _DATA.user.user_nicename + '","toname":"' + _DATA.anchor.user_nicename + '","touid":' + _DATA.anchor.id + ',"uid":' + _DATA.user.id + ',"level":' + _DATA.user.level + '}],"retcode":"000000","retmsg":"ok"}';
          Socket.emitData('broadcast', msg);
        } else {
          (new Dialog).alert(data.msg);
        }
      }
    })

  },
  setAttentionNums: function () {
    var _this = this;
    $.ajax({
      url: _this.attionNumUrl,
      data: {
        anchorid: _DATA.anchor.id
      },
      dataType: 'json',
      success: function (data) {
        if (data.error == 0) {
          $(".fans-num").html(data.data);
        }
      }
    })

  },
  showNavBg: function () {
    $("#LF-nav-bg").show().addClass("bg-show").animate({
      "left": '70px',
      "opacity": 1
    }, 200);
    this.getOnline();
  },
  closeNavBg: function () {
    if ($("#popup_info").length > 0) {
      $("#popup_info").remove();
    }
    this.listBtn.removeClass("on");
    $("#LF-nav-bg").removeClass("bg-show").animate({
      "left": '-190px',
      "opacity": 0
    }, 200);
    setTimeout(function () {
      $("#LF-nav-bg").hide();
    }, 300)
  },
  getOnline: function () {
    var room = new Room();
    var _this = this;
    if (_this.user_ismore == 1) {
      //这里的人数是通过输出用户列表是进行统计，如果想得到redis中的人数 可以使用data.nums
      $.ajax({
        url: _this.showUserListUrl,
        // data: {live_type: 0, liveid: _DATA.anchor.id, times: _this.user_times, defnums: _this.defnums},
        data: {
          live_type: room.typeId,
          liveid: room.id
        },
        dataType: "json",
        success: function (data) {
          // console.log(data);

          var list = data.list,
            len = list.length,
            html = '';
          var n = 0;
          for (var i = 0; i < len; i++) {
            var userinfo = list[i];
            n = n + 1;
            html += '<li data-cardid="' + userinfo.id + '" id="anchor_' + userinfo.id + '" class="anchor"><a class="xinxi" onclick=User.popup("' + userinfo.id + '")>';
            html += '<span class="ICON-noble-level ICON-nl-' + userinfo.level + '"></span>';
            //html+='<i class="ICON-medal" title="消费等级"><img class="medal-img" src=""></i>';
            //html+='<i class="ICON-active-level-bg" title="活跃等级"><img src="">普通</i>';
            html += '<span class="name" title="' + userinfo.user_nicename + '">' + userinfo.user_nicename + '</span>';
            var sex = userinfo.sex;
            if (sex == 0) {
              sex = 1;
            }
            // html += '<span class="isGuard"><img src="' + userinfo['vestIcon'] + '" style="width:20px;height:20px;min-width:20px;"></span>';//马甲图标
            html += '<span class="isGuard"><img src="/public/home/show/images/vest/vest_' + userinfo.vestid + '_' + sex + '.png"+  style="width:20px;height:20px;min-width:20px;"></span>'; //马甲图标
            if (userinfo.isGuard == 1) {
              if (userinfo.guardLevel == 1) {
                html += '<span class="isGuard"><img src="/public/home/show/images/guardBg1.png" style="width:20px;height:20px;"></span>'; //守护1图标
              } else if (userinfo.guardLevel == 2) {
                html += '<span class="isGuard"><img src="/public/home/show/images/guardBg2.png" style="width:20px;height:20px;"></span>'; //守护2图标
              } else if (userinfo.guardLevel == 3) {
                html += '<span class="isGuard"><img src="/public/home/show/images/guardBg3.png" style="width:20px;height:20px;"></span>'; //守护3图标
              } else if (userinfo.guardLevel == 4) {
                html += '<span class="isGuard"><img src="/public/home/show/images/guardBg4.png" style="width:20px;height:20px;"></span>'; //守护4图标
              } else if (userinfo.guardLevel == 5) {
                html += '<span class="isGuard"><img src="/public/home/show/images/guardBg5.png" style="width:20px;height:20px;"></span>'; //守护5图标
              }
            }

            html += '<div class="yanjing"><div class="yanjing_img"></div></div>';
            html += '</a></li>';
          }
          if (_this.user_isfirst == 1) {
            _this.userCon.html(html);
            _this.userCite.html(data.nums);

          } else {
            _this.userCon.append(html);
            _this.userCite.html(data.nums);
          }


          //	_this.resetuserssH();
          /*if(len < _this.defnums){
						_this.moreListBtn.hide();
					}else{
						_this.moreListBtn.show();
					}*/

          // if (_DATA.user != null) {
          if (_DATA.user && _DATA.user.logged == 1) { //已登录

            var strifyLeft = JSON.stringify(data);
            //显示进入房间的用户信息
            var msg = '{"msg":[{"_method_":"showuserinfo","action":0,"ct":"","msgtype":"2","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '","usinfo":' + strifyLeft + '}],"retcode":"000000","retmsg":"OK"}';


            // console.log('onlineuser1');
            Socket.emitData('broadcast', msg);
            // console.log(msg);
            // console.log('onlineuser2');

          }


        },
        error: function (request) {}

      })

    }


  },
  updateMoney: function () {
    this.userMoney.find("cite").html(_DATA.user.coin);
    this.userMoney.find("cite").attr("title", _DATA.user.coin + coin_text);
  }
};
/* 视频 */
var Video = {
  resolution_w: 16,
  resolution_h: 9,
  plus_w: 816,
  stager: $("#LF-stager"),
  LF_video: $("#LF-area-video"),
  player: $("#LF-area-video .channel-player-v2"),
  player_multi: $("#LF-area-video .channel-player-multi"),
  about: $(".MR-about"),
  aboutdh: 40,
  abouth: 110,
  endReUrl: './index.php?g=home&m=show&a=endRecommend',
  endReCon: $("#SR-video-rec-con"),
  timea: null,
  init: function () {
    this.setHeight();
    this.addEvent();
  },

  setHeight: function () {
    var width = $(window).width();
    var height;


    if (width > 1600 && width <= 2000) {

      var stager_w = $(window).width() - $('#LF-stager').offset().left;
      this.stager.css({
        "width": stager_w
      });

      w = ($('#LF-stager').width() - $('#LF-area-info').width() - $('#LF-area-chat').width()) * 0.7 - 25;

      h = w * 9 / 16;


    } else if (width > 2000) {

      w = 768;
      h = 432;


    } else if (width <= 1600) {
      w = 560;
      h = 315;

      var stager_w = w + $('#LF-area-info').width() + $('#LF-area-chat').width() + 25;


      this.stager.css({
        "width": stager_w
      });

    }


    this.LF_video.css({
      "height": $(window).height() - $('#LF-stager').offset().top - 10
    }); //设置视频显示区域的宽度
    this.player.css({
      "width": w,
      "height": h
    }); //设置视频显示区域的高度
    this.player_multi.css({
      "width": w,
      "height": h
    }); //设置视频多人连麦显示区域的高度
    this.stager.css({
      "height": $(window).height() - $('#LF-stager').offset().top
    });
    // this.stager.css({"width": stager_w});
    //alert("开始执行");

    //alert("执行结束");
    // $(".MR-msg .msg-gift").css("height",0.55*(height-50)-72);

    //alert(0.55*(height-50)-72);


    var videoWidth = $("#LF-area-video").width(); //获取中间视频区域的宽度
    var sendWidth = $(".MR-moregift").width(); //获取送礼物区域的宽度
    $("#LF-chat-gift").css("width", videoWidth - 2); //为礼物列表宽度赋值
    // $("#LF-chat-gift").css("width", videoWidth - sendWidth - 5);


    $("#LF-chat-gift").css("height", 0.3 * height - 30);
    $("#LF-chat-gift .gift-group").css("width", videoWidth - sendWidth);
    $(".MR-gift .M-arrow-scroll .con").css("height", 50); //更改礼物自适应高度
    // var giftListsW = $("#LF-chat-gift").width();//获取礼物列表宽度
    // $(".MR-moregift").css("right", giftListsW - videoWidth);

    $('#player-praises').css('height', h - 40);

    //多视频区域
    this.endReCon.find('.minvideo').css({
      "height": h / 3,
      "width": w / 3
    });

    if ($('.channel-player-multi').length > 0) {

      $("#msg_gift .boarder").css({
        "height": this.LF_video.height() - this.endReCon.height() - this.player_multi.height() - $('#LF-chat-gift').height() - 30
      }); //更改送礼物记录的高度 多人直播间

    } else {

      $("#msg_gift .boarder").css({
        "height": this.LF_video.height() - this.player.height() - $('#LF-chat-gift').height() - 30
      }); //更改送礼物记录的高度  单人直播间
    }

    // console.log( $("#msg_gift .boarder").css('height') );

  },
  addEvent: function () {
    var _this = this;
    $(window).on("resize", function () {
      _this.setHeight();
    })
    _this.timea = setTimeout(function () {
      _this.about.hide();
    }, 10000);
    _this.player.hover(function () {
        _this.about.show();
      },
      function () {
        if (_this.timea) {
          clearInterval(_this.timea);
        }
        _this.timea = setTimeout(function () {
          _this.about.hide();
        }, 3000);
      }
    )
    _this.about.hover(function () {
        if (_this.timea) {
          clearInterval(_this.timea);
        }
        $(this).addClass("MR-about-hover").animate({
          height: _this.abouth
        }, 200);
      },
      function () {
        $(this).removeClass("MR-about-hover").animate({
          height: _this.aboutdh
        }, 200);
        if (_this.timea) {
          clearInterval(_this.timea);
        }
        _this.timea = setTimeout(function () {
          _this.about.hide();
        }, 3000);
      }
    )

  },
  endRecommend: function () {
    return !1;
    var uid = _DATA.user && _DATA.user.id || 0;
    var liveuid = _DATA.anchor.id;
    if (uid == liveuid) {
      return !1;
    }
    var _this = this;
    $("#playerzmblbkjP_wrapper").css('display', 'none');
    $.ajax({
      url: _this.endReUrl,
      data: {},
      dataType: 'json',
      success: function (data) {
        if (data.error == 0) {
          var len = data.data.length;
          if (len > 0) {
            var html = '';
            for (var i = 0; i < len; i++) {
              html += '<li>\
											<div class="rec-item">\
												<a href="/' + data.data[i].uid + '"><img src="' + data.data[i].userinfo.avatar + '" /></a>\
												<span class="ICON-anchor-level ICON-al-' + data.data[i].userinfo.level + ' rec-row"></span>\
												<span class="rec-author-name rec-row">' + data.data[i].userinfo.user_nicename + '</span>\
												</div>\
										</li>';
            }
            _this.endReCon.find(".video-rec-con ul").html(html);
            //_this.endReCon.show();
          }
        }
      }
    })
  },

  statRecommend: function () {
    window.location.href = "";
  }
};

/**
 * 注册弹窗频率
 */
(function () {
  if (_DATA.user && _DATA.user.logged == 0) {
    if (_DATA.config && _DATA.config.register_pop_ups_frequency && _DATA.config.register_pop_ups_frequency != '0' && _DATA.config.register_pop_ups_frequency != 'undefined') {
      setInterval(function () {
        Login.login();
      }, _DATA.config.register_pop_ups_frequency * 60 * 1000);
    }
  }
})();

/**
 * 聊天
 */
var Chat = {
  interval: 5,
  chat_max_text_len: 30,
  fly_gold_max_text_len: 30,
  fly_site_max_text_len: 15,
  chat_default_value: '和大家聊会儿天？',
  flt_default_value: '请输入...',
  isfree: 0,
  sendUrl: '/index.php?g=home&m=spend&a=sendHorn',
  isShutUp: '/index.php?g=home&m=spend&a=isShutUp',
  isGuard: '/index.php?g=home&m=spend&a=isGuard',
  Ele_panel: '.msg-chat .MR-chat .boarder ul',
  Ele_scroll: '.msg-chat .MR-chat .scroller',
  minus_h: 391,
  chatSendBtn: $(".MR-talk .send-btn"),
  chatContent: $(".MR-talk .speaker input"),
  chatCite: $(".MR-talk .speaker cite"),
  hornToggle: $(".MR-horn .toggle"),
  hornSelector: $(".MR-horn .selector"),
  hornDia: $(".MR-horn .dialog"),
  hornDiaT: $(".MR-horn .dialog textarea"),
  hornType: 0,

  init: function () {
    this.setScorll();
    this.addEvent();
    this.setHeight();
  },
  setScorll: function () {
    this.scroll = new Scroll;
    this.scroll.init({
      Ele_panel: this.Ele_panel,
      Ele_scroll: this.Ele_scroll
    });
    this.scroll.toBottom();
  },
  resetH: function (height, h) {

    height = height - $("#doc-hd .topbar").height() - 20; // Kilian add //GUS 15改为20，左边对其

    $("#LF-area-chat").css("height", height);
    $("#LF-chat-msg").css("height", height - 90);
    $("#LF-chat-msg .MR-msg .MR-chat .boarder").css("height", height - 90);
    this.scroll.setHeight(height - 90);
    this.scroll.resetH();
    this.scroll.toBottom();
  },
  resetsH: function () {
    this.scroll.resetH();
    if (screenScroll == 1) { //如果页面里的公共变量设置为自动滚动到页面底部
      this.scroll.toBottom();
    }
  },
  setHeight: function () {
    var _this = this;
    var height = $(window).height();
    if (height > maxHeight) {
      // height = maxHeight;
      height;
    } else if (height < minHeight) {
      // height = minHeight;
      height;
    }
    var h = height - _this.minus_h;

    _this.resetH(height, h);
  },

  addEvent: function () {
    var _this = this;
    $(window).on("resize", function () {
      _this.setHeight();
    });

    this.chatContent.on({
      focus: function () {
        if (_this.chatContent.val() == _this.chat_default_value) {
          _this.chatContent.addClass("txt-focus").val("");
        }
      },
      blur: function () {
        if (_this.chatContent.val() == '') {
          _this.chatContent.removeClass("txt-focus").val(_this.chat_default_value);
        }
      },
      keyup: function () {
        var room = new Room();
        var chatmsg = $.trim(_this.chatContent.val());
        var txtnum = chatmsg.length;

        // if (_DATA.user != null) {
        if (_DATA.user && _DATA.user.logged == 1) { //已登录

          // if (_DATA.anchor.id != _DATA.user.id) {//用户
          if (!room.userIsAnchor) { //用户


            if (_DATA.user.issuper == 1 || _DATA.user.iswhite == 1) { //超管或白名单

              _this.chatCite.html("");

            } else {

              //判断是否守护
              if (_DATA.guardInfo != null) {
                _this.chat_max_text_len = _DATA.getConfigPub.guard_digits_num;
              } else if (_DATA.user.chat_num > 0) { //个人设置发言字数
                _this.chat_max_text_len = _DATA.user.chat_num;
              } else if (room.chat_num > 0) { //主播设置房间发言字数
                _this.chat_max_text_len = room.chat_num;
              }


              if (txtnum > _this.chat_max_text_len) {

                var num = txtnum - _this.chat_max_text_len;
                _this.chatCite.html(num);
                _this.chatCite.addClass("txt-error");
              } else {
                var num = _this.chat_max_text_len - txtnum;
                _this.chatCite.html(num);
                _this.chatCite.removeClass("txt-error");
              }
            }


          } else { //主播
            _this.chatCite.html("");
          }
        }


      },
      keydown: function (e) {
        if (e.keyCode == 13) {
          //_this.sendChat();
          _this.checkFrequency();
        }
      }
    });
    this.chatSendBtn.on("click", function () {
      _this.checkFrequency();
    });
    this.hornToggle.on("click", function () {
      _this.showHorn();
    });
    this.hornSelector.find(".closed").on("click", function () {
      _this.closeHorn();
    });
    this.hornSelector.find(".btn").on("click", function () {
      if ($(this).hasClass("gold")) {
        _this.hornType = 1;
      } else {
        _this.hornType = 2;
      }
      _this.showHornDia();
    });
    this.hornDiaT.on({
      focus: function () {
        if (_this.hornDiaT.val() == _this.flt_default_value) {
          _this.hornDiaT.addClass("txt-focus").val("");
        }
      },
      blur: function () {
        if (_this.hornDiaT.val() == '') {
          _this.hornDiaT.removeClass("txt-focus").val(_this.flt_default_value);
        }
      },
      keyup: function () {
        var room = new Room();

        var chatmsg = $.trim(_this.hornDiaT.val());

        var txtnum = chatmsg.length;

        if (_this.hornType == 1) {
          var max = _this.fly_gold_max_text_len;
        } else {
          var max = _this.fly_site_max_text_len;

          //判断当前用户是否是守护
          if (_DATA.guardInfo != "" && _DATA.config.guard_digits_num > 0) {
            max = _DATA.config.guard_digits_num;
          } else {
            // max = _DATA.live.chat_num;
            max = room.chat_num;
          }


        }

        if (txtnum > max) {
          var num = txtnum - max;
          _this.hornDia.find("span").eq(0).html("已经超过" + num + "个字");
          _this.hornDia.find("span").eq(0).addClass("txt-error");
        } else {
          var num = max - txtnum;
          _this.hornDia.find("span").eq(0).html("还能输入" + num + "个字");
          _this.hornDia.find("span").eq(0).removeClass("txt-error");
        }
      }
    })


    this.hornDia.find(".closed").on("click", function () {
      _this.closeHormDia();
    });
    this.hornDia.find(".horn-send").on("click", function () {
      _this.sendHorn();
    })

  },
  checkFrequency: function () {
    var room = new Room();
    //非主播才加发言频率限制
    // if (_DATA.user && _DATA.anchor.id != _DATA.user.id && isHitsInterval == 1) {
    if (_DATA.user && !room.userIsAnchor && isHitsInterval == 1) {
      layer.msg("您发言频率太快,请稍候再发送");
      $(".MR-talk .speaker input").val('');
      return;
    }

    var _this = this;
    // if (_DATA.user != null) {
    if (_DATA.user && _DATA.user.logged == 1) { //已登录
      // if (_DATA.anchor.id != _DATA.user.id) {//非主播才加发言频率限制
      if (!room.userIsAnchor) { //非主播才加发言频率限制
        if (_DATA.user.issuper == 0) { //用户非超管
          if (_DATA.user.iswhite == 0) { //用户不在白名单里
            /*发言倒计时*/
            if ($(".MR-talk .send-btn").attr("disabled")) {
              return;
            }
            //判断用户是否是守护
            if (_DATA.guardInfo != null) { //是守护
              //判断发言字数是否超过规定长度
              if ($(".MR-talk .speaker").find("cite").eq(0).hasClass('txt-error')) {
                layer.msg("发言字数太长");
                return;
              }

              if (_DATA.config.guard_digits_interval > 0) { //守护发言频率大于0
                isHitsInterval = 1;
                var frequency = _DATA.config.guard_digits_interval;
                var frequencyInterval = setInterval(function () {
                  $(".MR-talk .send-btn").attr("disabled", true);
                  $(".MR-talk .send-btn").text(frequency + 's');
                  isHitsInterval = 1;
                  if (frequency == 0) {
                    clearInterval(frequencyInterval);
                    $(".MR-talk .send-btn").attr("disabled", false);
                    $(".MR-talk .send-btn").text('发送');
                    isHitsInterval = 0;
                  }
                  frequency--;
                }, 1000);
              } else { //是守护，但是后台没有设置守护发言频率，就按照直播间主播设置的
                isHitsInterval = 1;
                var frequency = room.chat_frequency;
                var frequencyInterval = setInterval(function () {
                  $(".MR-talk .send-btn").attr("disabled", true);
                  $(".MR-talk .send-btn").text(frequency + 's');
                  isHitsInterval = 1;
                  if (frequency == 0) {
                    clearInterval(frequencyInterval);
                    $(".MR-talk .send-btn").attr("disabled", false);
                    $(".MR-talk .send-btn").text('发送');
                    isHitsInterval = 0;
                  }
                  frequency--;
                }, 1000);
              }
            } else if (room.chat_frequency > 0) { //用户自己设置了发言频率
              isHitsInterval = 1;
              var frequency = room.chat_frequency;
              var frequencyInterval = setInterval(function () {
                $(".MR-talk .send-btn").attr("disabled", true);
                $(".MR-talk .send-btn").text(frequency + 's');
                isHitsInterval = 1;
                if (frequency == 0) {
                  clearInterval(frequencyInterval);
                  $(".MR-talk .send-btn").attr("disabled", false);
                  $(".MR-talk .send-btn").text('发送');
                  isHitsInterval = 0;
                }
                frequency--;
              }, 1000);
            } else {
              if (room.chat_frequency > 0) { //主播设置了直播间发言频率
                //判断发言字数是否超过规定长度
                if ($(".MR-talk .speaker").find("cite").eq(0).hasClass('txt-error')) {
                  layer.msg("发言字数太长");
                  return;
                }

                isHitsInterval = 1;
                var frequency = room.chat_frequency;
                var frequencyInterval = setInterval(function () {
                  $(".MR-talk .send-btn").attr("disabled", true);
                  $(".MR-talk .send-btn").text(frequency + 's');
                  isHitsInterval = 1;
                  if (frequency == 0) {
                    clearInterval(frequencyInterval);
                    $(".MR-talk .send-btn").attr("disabled", false);
                    $(".MR-talk .send-btn").text('发送');
                    isHitsInterval = 0;
                  }
                  frequency--;
                }, 1000);
              } else {
                //主播没设置直播间发言频率
                isHitsInterval = 1;
                var sendDelay = 2;
                var sendDelayInterval = setInterval(function () {
                  $(".MR-talk .send-btn").attr("disabled", true);
                  isHitsInterval = 1;
                  if (sendDelay == 0) {
                    clearInterval(sendDelayInterval);
                    $(".MR-talk .send-btn").attr("disabled", false);
                    isHitsInterval = 0;
                  }
                  sendDelay--;
                }, 1000);
              }
            }
          } else {
            //用户在白名单
            isHitsInterval = 1;
            var sendDelay = 2;
            var sendDelayInterval = setInterval(function () {
              $(".MR-talk .send-btn").attr("disabled", true);
              isHitsInterval = 1;
              if (sendDelay == 0) {
                clearInterval(sendDelayInterval);
                $(".MR-talk .send-btn").attr("disabled", false);
                isHitsInterval = 0;
              }
              sendDelay--;

            }, 1000);
          }
        } else {
          //用户是超管
          isHitsInterval = 1;
          var sendDelay = 2;
          var sendDelayInterval = setInterval(function () {
            $(".MR-talk .send-btn").attr("disabled", true);
            isHitsInterval = 1;
            if (sendDelay == 0) {
              clearInterval(sendDelayInterval);
              $(".MR-talk .send-btn").attr("disabled", false);
              isHitsInterval = 0;
            }
            sendDelay--;
          }, 1000);
        }
      } else {
        //用户是主播
        isHitsInterval = 1;
        var sendDelay = 2;
        var sendDelayInterval = setInterval(function () {
          $(".MR-talk .send-btn").attr("disabled", true);
          isHitsInterval = 1;
          if (sendDelay == 0) {
            clearInterval(sendDelayInterval);
            $(".MR-talk .send-btn").attr("disabled", false);
            isHitsInterval = 0;
          }
          sendDelay--;
        }, 1000);
      }
      _this.sendChat();
    } else {
      // limit_talk_type 0-禁止发言 1-可以发言    limit_talk_num: 游客每天最多发送消息数
      if (_DATA.config.limit_talk_type == '1') {
        if (parseInt(_DATA.config.daily_send_msg_num) >= parseInt(_DATA.config.limit_talk_num)) {
          layer.msg('游客每天最多发送' + _DATA.config.limit_talk_num + '条消息');
          // } else if(_DATA.config.hasCheckedDayliSendMsgNum) {
          //     _this.sendChat();
          //     _DATA.config.daily_send_msg_num += 1;
        } else {
          _DATA.config.daily_send_msg_num += 1;
          _this.sendChat();
          // _DATA.config.hasCheckedDayliSendMsgNum = true;
          $.ajax({
            url: '/index.php?g=home&m=show&a=setDailySendMsgNum',
            type: 'POST',
            dataType: 'json',
            data: {
              liveid: room.id,
              live_type: room.typeId
            },
            success: function (data) {
              if (data.code == 200) {
                var sendedNum = parseInt(data.data.num);
                if (sendedNum >= parseInt(_DATA.config.limit_talk_num)) {
                  // layer.msg('游客每天最多发送' + _DATA.config.limit_talk_num + '条消息');
                } else {

                }
              } else {
                _DATA.config.daily_send_msg_num += 1;
                _this.sendChat();
              }
            },
            error: function () {
              _DATA.config.daily_send_msg_num += 1;
              _this.sendChat();
            }
          })
        }

      } else if (_DATA.config.limit_talk_type == '0') {
        layer.msg('禁止发言');
      }
    }
  },
  showHorn: function () {
    var _this = this;
    this.hornSelector.show().addClass("selector-show");

  },
  closeHorn: function () {
    this.hornSelector.removeClass("selector-show").hide();
  },
  showHornDia: function () {
    var _this = this;
    if (this.hornType == 1) {
      this.hornDia.find("h4")[0].className = "gold", this.hornDia.find("i").html("金喇叭"), this.hornDia.find(".detail").removeClass("detail-site");
      /* 	_this.hornDia.find("span").eq(0).html("还能输入"+_this.fly_gold_max_text_len+"个字"); */
    } else {
      this.hornDia.find("h4")[0].className = "site", this.hornDia.find("i").html("弹幕"), this.hornDia.find(".detail").addClass("detail-site");
      /* _this.hornDia.find("span").eq(0).html("还能输入"+_this.fly_site_max_text_len+"个字"); */
    }
    _this.hornDiaT.val(_this.flt_default_value).removeClass("txt-focus");
    this.hornSelector.removeClass("selector-show").addClass("selector-fx");
    setTimeout(function () {
        _this.hornDia.show().addClass("dialog-fx"),
          _this.hornSelector && _this.hornSelector.hide().removeClass("selector-fx")
      },
      600)
  },
  closeHormDia: function () {
    this.hornDia.removeClass("dialog-fx").hide();
  },
  sendChat: function () {
    var _this = this;
    var chatmsg = $.trim(_this.chatContent.val());
    var room = new Room();
    if (!chatmsg || chatmsg == _this.chat_default_value) {
      (new Dialog).tip("内容不能为空", _this.chatContent, {
        delay: 2e3
      });
      return !1;
    }

    //判断频道是否禁言
    $.ajax({
      url: '/index.php?g=home&m=spend&a=checkChannelShutUp',
      type: 'POST',
      dataType: 'json',
      // data: {showid: _DATA.anchor.id},
      data: {
        liveid: room.id,
        live_type: room.typeId
      },
      success: function (data) {
        //不是主播，也不是超管
        // if (_DATA.anchor.id != _DATA.user.id && _DATA.user.issuper == 0) {
        if (!room.userIsAnchor && _DATA.user.issuper == 0) {
          if (data.data.is_shut_up == true || data.data.is_shut_up == 'true') { //频道被禁言
            layer.msg("该频道被禁言");
            return;
          } else {
            if (_DATA.user.iswhite == 0) { //用户不在白名单内
              if (chatmsg.length > _this.chat_max_text_len) {
                (new Dialog).tip("内容不能超过" + _this.chat_max_text_len + "个字", _this.chatContent, {
                  delay: 2e3
                });
                return !1;
              }

              //判断用户是否可以发送网址
              if (_DATA.user.send_url == 0) { //不可发送网址

                //屏蔽用户输入的网址
                var reg = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/;
                if (reg.test(chatmsg)) {
                  layer.msg("不能输入网址");
                  _this.chatContent.val('');
                  return !1;
                }

                var isUrl = 0;

                var arr = Array('.', 'com', 'cn', 'org', 'gov', 'vip', 'com.cn', 'org.cn', 'gov.cn', '.cc', '.xyz', '.cn', '.org', '.gov', '.vip', '.xin');

                for (var i = 0; i < arr.length; i++) {
                  if (chatmsg.indexOf(arr[i]) >= 0) {
                    isUrl = 1;
                    break;
                  }
                }

                if (isUrl == 1) {
                  layer.msg("不能输入网址");
                  _this.chatContent.val('');
                  return !1;
                }
              }

              $.ajax({

                dataType: 'json',
                url: _this.isShutUp,
                type: "POST",
                /*async:false,*/
                // data: {showid: _DATA.anchor.id},// 你的formid
                data: {
                  liveid: room.id,
                  live_type: room.typeId
                }, // 你的formid
                error: function (request) {
                  layer.msg("数据请求失败！");
                },
                success: function (data) {
                  if (data['info'] == 1) {
                    layer.msg("你已经被禁言");
                  } else {
                    /*if(data['admin']==60)
												{
													var msg='{"msg":[{"_method_":"SystemNot","action":"13","ct":"'+chatmsg+'","msgtype":"4","uname":"'+_DATA.user.user_nicename+'","toname":"'+_DATA.anchor.user_nicename+'","touid":'+_DATA.anchor.id+',"uid":'+_DATA.user.id+',"level":'+_DATA.user.level+'}],"retcode":"000000","retmsg":"ok"}';
												}
												else
												{*/
                    var msg = {
                      "msg": [{
                        "_method_": "SendMsg",
                        "action": 0,
                        "ct": chatmsg,
                        "msgtype": "2",
                        "isfilter": "0",
                        "tougood": "",
                        "touid": "",
                        "touname": "",
                        "ugood": _DATA.user.id,
                        "uid": _DATA.user.id,
                        "uname": _DATA.user.user_nicename,
                        "level": _DATA.user.level
                      }],
                      "retcode": "000000",
                      "retmsg": "OK"
                    };
                    Socket.emitData('broadcast', JSON.stringify(msg));

                    // var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"' + chatmsg + '","msgtype":"2","isfilter":"0","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '"}],"retcode":"000000","retmsg":"OK"}';
                    //}
                    // Socket.emitData('broadcast', msg);
                  }
                }

              });

            } else {

              var msg = {
                "msg": [{
                  "_method_": "SendMsg",
                  "action": 0,
                  "ct": chatmsg,
                  "msgtype": "2",
                  "isfilter": "1",
                  "tougood": "",
                  "touid": "",
                  "touname": "",
                  "ugood": _DATA.user.id,
                  "uid": _DATA.user.id,
                  "uname": _DATA.user.user_nicename,
                  "level": _DATA.user.level
                }],
                "retcode": "000000",
                "retmsg": "OK"
              };
              Socket.emitData('broadcast', JSON.stringify(msg));

              // var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"' + chatmsg + '","msgtype":"2","isfilter":"1","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '"}],"retcode":"000000","retmsg":"OK"}';
              // Socket.emitData('broadcast', msg);
            }

            _this.chatContent.val('');
            // if (_DATA.anchor.id != _DATA.user.id) {//不是主播
            if (!room.userIsAnchor) { //不是主播
              if (_DATA.user.iswhite == 1) {
                _this.chat_max_text_len = "";
              } else {
                if (_DATA.guardInfo != null) {
                  _this.chat_max_text_len = _DATA.getConfigPub.guard_digits_num;
                } else if (_DATA.user.chat_num > 0) { //个人设置发言字数
                  _this.chat_max_text_len = _DATA.user.chat_num;
                  // } else if (_DATA.live.chat_num > 0) {
                } else if (room.chat_num > 0) {
                  _this.chat_max_text_len = room.chat_num;
                }
              }
            }
            _this.chatCite.html(_this.chat_max_text_len); //发送成功后重置数字

          }
        } else { //主播或超管
          /*$.ajax({
						  dataType:'json',
					      url:_this.isShutUp,
					      type:"POST",
					      async:false,
					      data:{showid:_DATA.anchor.id},// 你的formid
					      error: function(request) {
					        layer.msg("数据请求失败！");
					      },
					      success: function(data){
									if(data['info']==1)
									{
										layer.msg("你已经被禁言");
									}
									else
									{
										if(data['admin']==60)
										{
											var msg='{"msg":[{"_method_":"SystemNot","action":"13","ct":"'+chatmsg+'","msgtype":"4","uname":"'+_DATA.user.user_nicename+'","toname":"'+_DATA.anchor.user_nicename+'","touid":'+_DATA.anchor.id+',"uid":'+_DATA.user.id+',"level":'+_DATA.user.level+'}],"retcode":"000000","retmsg":"ok"}';
										}
										else
										{
											var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"'+chatmsg+'","msgtype":"2","tougood":"","touid":"","touname":"","ugood":"'+_DATA.user.id+'","uid":"'+_DATA.user.id+'","uname":"'+_DATA.user.user_nicename+'","level":"'+_DATA.user.level+'"}],"retcode":"000000","retmsg":"OK"}';
										}
										Socket.emitData('broadcast',msg);
									}
					      }
				    	});*/

          if (_DATA.user.issuper == 1) {
            // var msg = '{"msg":[{"_method_":"SystemNot","action":"13","ct":"' + chatmsg + '","msgtype":"4","uname":"' + _DATA.user.user_nicename + '","toname":"' + _DATA.anchor.user_nicename + '","touid":' + _DATA.anchor.id + ',"uid":' + _DATA.user.id + ',"level":' + _DATA.user.level + '}],"retcode":"000000","retmsg":"ok"}';
            var msg = {
              "msg": [{
                "_method_": "SystemNot",
                "action": "13",
                "ct": chatmsg,
                "msgtype": "4",
                "uname": _DATA.user.user_nicename,
                "toname": _DATA.anchor.user_nicename,
                "touid": _DATA.anchor.id,
                "uid": _DATA.user.id,
                "level": _DATA.user.level
              }],
              "retcode": "000000",
              "retmsg": "ok"
            };
          } else {
            // var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"' + chatmsg + '","msgtype":"2","isfilter":"0","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '"}],"retcode":"000000","retmsg":"OK"}';
            var msg = {
              "msg": [{
                "_method_": "SendMsg",
                "action": 0,
                "ct": chatmsg,
                "msgtype": "2",
                "isfilter": "0",
                "tougood": "",
                "touid": "",
                "touname": "",
                "ugood": _DATA.user.id,
                "uid": _DATA.user.id,
                "uname": _DATA.user.user_nicename,
                "level": _DATA.user.level
              }],
              "retcode": "000000",
              "retmsg": "OK"
            };
          }

          Socket.emitData('broadcast', JSON.stringify(msg));
          _this.chatContent.val('');
          _this.chat_max_text_len = ""; //主播或超管不限制字数，也不显示字数
          _this.chatCite.html(_this.chat_max_text_len); //发送成功后重置数字
        }
      },
      error: function (data) {
        layer.msg("获取频道是否禁言失败");
        return;
      }
    });
  },
  //发送图片start
  checkimgshutup: function () {
    var room = new Room();
    $.ajax({
      dataType: 'json',
      url: this.isShutUp,
      data: {
        showid: room.id
      },
      type: 'POST',
      error: function (request) {
        layer.msg("数据请求失败！");
      },
      success: function (data) {
        debugger;
        if (data['info'] == 1) {
          layer.msg("你已经被禁言");
        } else {
          var adminInfo = data['admin'];
          $.ajax({
            url: '/index.php?g=home&m=spend&a=checkChannelShutUp',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
              liveid: room.id,
              live_type: room.typeId
            },
            success: function (data) {
              if ( /*!room.userIsAnchor && */ _DATA.user.issuper == 0) { //不是主播，也不是超管
                if (data.info == 1) { //频道被禁言
                  layer.msg("该频道被禁言");
                  return;
                } else {
                  issuperadmin = adminInfo;
                  flashupload('thumb_images', '附件上传', 'socket', thumb_images, '1,jpg|jpeg|gif|png|bmp,1,,,1', '', '', '');
                }
              }
            },
            error: function (data) {
              layer.msg("获取频道禁言失败");
            }
          });
        }
      }
    });
  },
  sendimg: function (img) {

    if (img.indexOf("http") >= 0) {
      imgurl = img;
    } else {

      imgurl = _DATA.config.site + '/' + img;
    }
    if (issuperadmin == 60) {
      // var msg = '{"msg":[{"_method_":"SystemNot","action":"13","ct":"' + imgurl + '","isimg":"1","msgtype":"4","uname":"' + _DATA.user.user_nicename + '","toname":"' + _DATA.anchor.user_nicename + '","touid":' + _DATA.anchor.id + ',"uid":' + _DATA.user.id + ',"level":' + _DATA.user.level + '}],"retcode":"000000","retmsg":"ok"}';
      var msg = {
        "msg": [{
          "_method_": "SendMsg",
          "action": 0,
          "ct": imgurl,
          "isimg": "1",
          "msgtype": "2",
          "tougood": "",
          "touid": "",
          "touname": "",
          "ugood": _DATA.user.id,
          "uid": _DATA.user.id,
          "uname": _DATA.user.user_nicename,
          "level": _DATA.user.level
        }],
        "retcode": "000000",
        "retmsg": "OK"
      };
    } else {
      // var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"' + imgurl + '","isimg":"1","msgtype":"2","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '"}],"retcode":"000000","retmsg":"OK"}';
      var msg = {
        "msg": [{
          "_method_": "SendMsg",
          "action": 0,
          "ct": imgurl,
          "isimg": "1",
          "msgtype": "2",
          "tougood": "",
          "touid": "",
          "touname": "",
          "ugood": _DATA.user.id,
          "uid": _DATA.user.id,
          "uname": _DATA.user.user_nicename,
          "level": _DATA.user.level
        }],
        "retcode": "000000",
        "retmsg": "OK"
      };
    }
    Socket.emitData('broadcast', JSON.stringify(msg));


  },

  //发送图片end

  sendHorn: function () {
    var room = new Room();
    var _this = this;
    var chatmsg = $.trim(_this.hornDiaT.val());
    if (!Check.checkLogin()) {
      //(new Dialog).tip("请登录", _this.giftSendBtn, {delay:2e3});
      return !1;
    }
    if (!chatmsg || chatmsg == _this.flt_default_value) {
      (new Dialog).tip("内容不能为空", _this.hornDiaT, {
        delay: 2e3
      });
      return !1;
    }
    if (_this.hornType == 1) {
      var needcoin = 0;
    } else {
      /*本站喇叭走这里*/
      var needcoin = 5000;

    }
    var coin = $("#LF-user .MR-user .login .money cite").html();

    /*if(coin<needcoin &&_DATA.user.coin<needcoin){
			if(!Check.checkCoin(needcoin)){
				(new Dialog).tip('<div class="status-no-money" id="_temp_DDS_noEnoughMoney">\
									<span>抱歉，您的余额不足哦</span>\
								</div>', _this.hornDia.find(".horn-send"), {delay:2e3});
				return !1;
			}
		}*/
    /*        var stream = (_DATA.live && _DATA.live.stream) || 0;
            if (stream == 0) {

                layer.msg("主播未开播，不能发送弹幕");
                return !1;

            }*/
    if (!room.isLive) {
      layer.msg("主播未开播，不能发送弹幕");
      return;
    }

    if (room.typeId == 0) {
      var touid = _DATA.anchor.id;
    } else if (room.typeId == 1) {
      var touid = $("#agora_local").attr('uid');
    }

    //判断当前用户的身份
    if (_DATA.user.issuper == 1) { //超管

      $.ajax({
        url: _this.sendUrl,
        type: 'post',
        // data: {liveuid: _DATA.anchor.id, content: chatmsg, stream: stream},
        data: {
          liveid: room.id,
          live_type: room.typeId,
          content: chatmsg,
          touid: touid
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.code != 0) {
            layer.msg(data.msg);
            return !1;
          } else {
            var data = data.info;
            _DATA.user.level = data.level;
            _DATA.user.coin = data.coin;
            User.updateMoney();
            var msg = '{"msg":[{"sex":"","action":"7","city":"","usign":"","ugood":"' + _DATA.user.id + '","roomnum":"' + room.id + '","level":"' + data.level + '","timestamp":"","equipment":"app","uid":"' + _DATA.user.id + '","ct":"' + data.barragetoken + '","touid":"0","_method_":"SendBarrage","msgtype":"1","tougood":"","uhead":"' + _DATA.user.avatar + '","uname":"' + _DATA.user.user_nicename + '","touname":""}],"retmsg":"OK","retcode":"000000"}';
            Socket.emitData('broadcast', msg);
            _this.hornDia.removeClass("dialog-fx").hide();
            _this.hornDiaT.val(_this.flt_default_value);
          }
        },
        error: function (data) {}
      })

      // } else if (_DATA.user.id == _DATA.live.uid) {  //主播
    } else if (room.userIsAnchor) { //主播

      $.ajax({
        url: _this.sendUrl,
        type: 'post',
        // data: {liveuid: _DATA.anchor.id, content: chatmsg, stream: stream},
        data: {
          liveid: room.id,
          live_type: room.typeId,
          content: chatmsg,
          touid: touid
        },
        dataType: 'JSON',
        success: function (data) {
          if (data.code != 0) {
            layer.msg(data.msg);
            return !1;
          } else {
            var data = data.info;
            _DATA.user.level = data.level;
            _DATA.user.coin = data.coin;
            User.updateMoney();
            var msg = '{"msg":[{"sex":"","action":"7","city":"","usign":"","ugood":"' + _DATA.user.id + '","roomnum":"' + room.id + '","level":"' + data.level + '","timestamp":"","equipment":"app","uid":"' + _DATA.user.id + '","ct":"' + data.barragetoken + '","touid":"0","_method_":"SendBarrage","msgtype":"1","tougood":"","uhead":"' + _DATA.user.avatar + '","uname":"' + _DATA.user.user_nicename + '","touname":""}],"retmsg":"OK","retcode":"000000"}';
            Socket.emitData('broadcast', msg);
            _this.hornDia.removeClass("dialog-fx").hide();
            _this.hornDiaT.val(_this.flt_default_value);
          }
        },
        error: function (data) {}
      })


    } else { //普通用户

      //判断直播间是否禁言

      $.ajax({
        url: '/index.php?g=home&m=spend&a=checkChannelShutUp',
        type: 'POST',
        dataType: 'json',
        data: {
          showid: _DATA.anchor.id
        },
        async: false,
        success: function (data) {
          if (data.info == 1) {
            layer.msg("房间被禁言");
            return;
          }
        },
        error: function (data) {

        }
      });


      if (_DATA.user.iswhite == 1) { //用户在白名单

        $.ajax({
          url: _this.sendUrl,
          type: 'POST',
          data: {
            liveid: room.id,
            live_type: room.typeId,
            content: chatmsg,
            touid: touid
          },
          // data: {liveuid: _DATA.anchor.id, content: chatmsg, stream: stream},
          dataType: 'JSON',
          success: function (data) {
            if (data.code != 0) {
              layer.msg(data.msg);
              return !1;
            } else {
              var data = data.info;
              _DATA.user.level = data.level;
              _DATA.user.coin = data.coin;
              User.updateMoney();
              var msg = '{"msg":[{"sex":"","action":"7","city":"","usign":"","ugood":"' + _DATA.user.id + '","roomnum":"' + room.id + '","level":"' + data.level + '","timestamp":"","equipment":"app","uid":"' + _DATA.user.id + '","ct":"' + data.barragetoken + '","touid":"0","_method_":"SendBarrage","msgtype":"1","tougood":"","uhead":"' + _DATA.user.avatar + '","uname":"' + _DATA.user.user_nicename + '","touname":""}],"retmsg":"OK","retcode":"000000"}';
              Socket.emitData('broadcast', msg);
              _this.hornDia.removeClass("dialog-fx").hide();
              _this.hornDiaT.val(_this.flt_default_value);
            }
          },
          error: function (data) {}
        })

      } else { //用户不在白名单


        if (_this.hornDia.find("span").eq(0).hasClass("txt-error")) { //字数超了

          layer.msg("发言字数太多");

        } else {

          $.ajax({
            url: _this.sendUrl,
            type: 'POST',
            data: {
              liveid: room.id,
              live_type: room.typeId,
              content: chatmsg,
              touid: touid
            },
            // data: {liveuid: _DATA.anchor.id, content: chatmsg, stream: stream},
            dataType: 'JSON',
            success: function (data) {
              if (data.code != 0) {
                layer.msg(data.msg);
                return !1;
              } else {
                var data = data.info;
                _DATA.user.level = data.level;
                _DATA.user.coin = data.coin;
                User.updateMoney();
                var msg = '{"msg":[{"sex":"","action":"7","city":"","usign":"","ugood":"' + _DATA.user.id + '","roomnum":"' + room.id + '","level":"' + data.level + '","timestamp":"","equipment":"app","uid":"' + _DATA.user.id + '","ct":"' + data.barragetoken + '","touid":"0","_method_":"SendBarrage","msgtype":"1","tougood":"","uhead":"' + _DATA.user.avatar + '","uname":"' + _DATA.user.user_nicename + '","touname":""}],"retmsg":"OK","retcode":"000000"}';
                Socket.emitData('broadcast', msg);
                _this.hornDia.removeClass("dialog-fx").hide();
                _this.hornDiaT.val(_this.flt_default_value);


                //****************************************因为弹幕发送完后会重新加载，所以倒计时无效*****************************************

                //用户是守护
                if (_DATA.guardInfo != null) {

                  //判断守护的设置
                  if (_DATA.config.guard_digits_interval > 0) {

                    var guardCount = _DATA.config.guard_digits_interval;

                    var guardHornInterval = setInterval(function () {
                      $(".MR-horn .dialog .horn-send").attr("disabled", true);
                      $(".MR-horn .dialog .horn-send").text(guardCount + 's');
                      if (guardCount == 0) {
                        clearInterval(guardHornInterval);
                        $(".MR-horn .dialog .horn-send").attr("disabled", false);
                        $(".MR-horn .dialog .horn-send").text('发送');
                      }
                      guardCount--;
                    }, 1000);
                  } else {
                    //判断房间是否设置发言频率
                    if (room.chat_frequency > 0) {
                      var count = room.chat_frequency;
                      var hornInterval = setInterval(function () {
                        $(".MR-horn .dialog .horn-send").attr("disabled", true);
                        $(".MR-horn .dialog .horn-send").text(count + 's');
                        if (count == 0) {
                          clearInterval(hornInterval);
                          $(".MR-horn .dialog .horn-send").attr("disabled", false);
                          $(".MR-horn .dialog .horn-send").text('发送');
                        }
                        count--;
                      }, 1000);
                    }
                  }
                } else { //不是守护
                  //判断房间是否设置发言频率
                  if (room.chat_frequency > 0) {
                    var count = room.chat_frequency;

                    var hornInterval = setInterval(function () {
                      $(".MR-horn .dialog .horn-send").attr("disabled", true);
                      $(".MR-horn .dialog .horn-send").text(count + 's');
                      if (count == 0) {
                        clearInterval(hornInterval);
                        $(".MR-horn .dialog .horn-send").attr("disabled", false);
                        $(".MR-horn .dialog .horn-send").text('发送');
                      }
                      count--;
                    }, 1000);
                  }
                }
                //****************************************因为弹幕发送完后会重新加载，所以倒计时无效*****************************************
              }
            },
            error: function (data) {}
          })
        }
      }
    }

    //刷新排行榜
    Rank.adddate();
  },
  clearChat: function () {

  },
  setDisabled: function () {


  }
};
/* 礼物记录 */
var Giftlist = {
  Ele_panel: '.msg-gift .MR-chat .boarder ul',
  Ele_scroll: '.msg-gift .MR-chat .scroller',

  init: function () {
    this.setScorll();
    this.addEvent();
  },
  setScorll: function () {
    this.scroll = new Scroll;
    this.scroll.init({
      Ele_panel: this.Ele_panel,
      Ele_scroll: this.Ele_scroll
    });
  },
  resetH: function () {
    this.scroll.resetH(),
      this.scroll.toBottom();
  },
  addEvent: function () {
    var _this = this;
    $(window).on("resize", function () {
      var GiftMsgListH = $(".MR-msg .msg-gift").height();
      // $(".msg-gift .MR-chat .boarder").css("height", GiftMsgListH);
      _this.resetH();
    })
  }

};

/**
 * 礼物
 */
var Gift = {
  giftLi: $(".gift-con .con li"),
  giftTip: $(".MR-gift-tip"),
  giftGroup: $(".gift-group"),
  giftSendBtn: $(".MR-gift .send-btn"),
  giftSelBox: $(".MR-gift .select-box"),
  giftNum: $(".MR-gift .num-input"),
  difHeight: 170,
  selectId: 0,
  giftSelListH: 0,
  sendUrl: './index.php?g=home&m=Spend&a=sendGift',
  tiptime: null,
  init: function () {
    this.addEvent();
  },
  addEvent: function () {
    var _this = this;
    this.giftLi.on("click", function () {
      $(this).siblings().removeClass("selected");
      $(this).addClass("selected");
      _this.selectId = $(this).data("id");
    });
    this.giftLi.hover(function () {
      _this.tiptime && clearTimeout(_this.tiptime);
      _this.showtips($(this));
    }, function () {
      _this.tiptime = setTimeout(function () {
          _this.hidetips();
        },
        50)
    });
    this.giftTip.hover(function () {
      _this.tiptime && clearTimeout(_this.tiptime);
    }, function () {
      _this.giftTip.hide();
    });
    this.giftSendBtn.on("click", function () {
      _this.sendGift();
    });
  },
  showtips: function (ele) {
    var giftid = ele.data("id"),
      giftinfo = _DATA.gift[giftid],
      pos = ele.position();

    // this.giftTip.find(".tip-img").attr("backgrond", giftinfo.gifticon);
    this.giftTip.find(".tip-img").css('background-image', 'url(' + giftinfo.gifticon + ')');
    this.giftTip.find(".gift-tip-name").html(giftinfo.giftname);
    this.giftTip.find(".gift-tip-price").html(giftinfo.needcoin + coin_text);
    this.giftTip.find(".gift-tip-desc").html('');

    this.giftTip.attr("style", "");
    var tipWidth = this.giftTip.width();
    pos.left + tipWidth > this.giftGroup.width() ? this.giftTip.css({
      right: "" + (this.giftGroup.width() - pos.left - 53) + "px"
    }) : this.giftTip.css({
      left: "" + pos.left + "px"
    });
    this.giftTip.show();
  },
  hidetips: function () {
    this.giftTip.hide();
  },
  sendGift: function () {
    var _this = this;
    var room = new Room();
    // if (!Check.checkLogin()) { //这里的让后端接口进行登录验证
    //     (new Dialog).tip("请登录", _this.giftSendBtn, {delay: 2e3});
    //     return !1;
    // }
    // 
    if (_DATA.enterChat == 0) {
      layer.msg("礼物网络服务器实时通信失败！");
      return;
    }

    if (_DATA.anchor.id == _DATA.user.id) {
      // (new Dialog).tip("不允许给自己送礼物", _this.giftSendBtn, {delay: 2e3});
      layer.msg("不允许给自己送礼物！");
      return !1;
    }
    if (!room.isLive) {
      // (new Dialog).tip("主播未开播，不能送礼物", _this.giftSendBtn, {delay: 2e3});
      layer.msg("主播未开播，不能送礼物！");
      return !1;
    }
    if (_this.selectId == 0) {
      (new Dialog).tip("请选择礼物", _this.giftSendBtn, {
        delay: 2e3
      });
      return !1;
    }

    var giftGroupNum = $(".giftGroupNumVal").val();
    var showid = (_DATA.live && _DATA.live.showid) || 0;

    if (isNaN(giftGroupNum)) {
      layer.msg("请确认赠送礼物的数量");
      return;
    }
    if (giftGroupNum == '') {
      giftGroupNum = 1;
    }

    //touid 主播Id giftid礼物ID giftcount 礼物数量
    //
    var touid, selecename; //多人直播间大视屏主播
    if (room.typeName == 'single') {
      touid = _DATA.anchor.id;
      selecename = "";
    } else {
      touid = $('.giftGroupSelect option:selected').val();
      selecename = $('.giftGroupSelect option:selected').text();
    }
    // console.log('touid'+touid);

    $.ajax({
      url: _this.sendUrl,
      data: {
        liveid: room.id,
        touid: touid,
        giftid: _this.selectId,
        // showid: showid,
        giftGroupNum: giftGroupNum,
        liveType: room.typeId,
        sendType: 0
      },
      type: "POST",
      success: function (data) {
        var data = JSON.parse(data);
        // console.log('礼物开始');
        // console.log(data);

        if (data.errno != 0) {
          if (data.errno == 1003) {
            //询问框
            layer.confirm('您的直播券不足，是否使用' + data.coin_name + '支付？', {
              btn: ['确定', '取消'] //按钮
            }, function () {

              $.ajax({
                url: _this.sendUrl,
                type: 'POST',
                data: {
                  liveid: room.id,
                  touid: touid,
                  giftid: _this.selectId,
                  // showid: showid,
                  giftGroupNum: giftGroupNum,
                  sendType: 1
                },
                cache: true,
                success: function (data) {
                  var data = JSON.parse(data);
                  _DATA.user.level = data.level;
                  _DATA.user.coin = data.coin;
                  User.updateMoney();
                  data.level = data.level.toString();
                  data.uid = data.uid.toString();

                  if (data.iswin == 1) {
                    layer.msg("恭喜您中得" + data.winLiveCoin + "直播券");
                  }

                  var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"SendGift","evensend":"' + data.evensend + '","action":"0","ct":"' + data.gifttoken + '","msgtype":"1","level":"' + data.level + '","giftGroupNum":"' + data.giftGroupNum + '","selecename":"' + selecename + '","uid":"' + data.uid + '","timestamp":"' + WlTools.FormatNowDate() + '","uname":"' + _DATA.user.user_nicename + '","uhead":"' + _DATA.user.avatar + '"}]}';
                  Socket.emitData('broadcast', msg);
                  //Rank.adddate();
                },
                error: function (data) {
                  layer.msg("赠送礼物失败啦");
                  return;
                }
              });

              layer.closeAll();
            }, function () {
              layer.closeAll();
            });
          } else if (data.errno == 1004) {
            //游客送礼提示文案：立即登录赠送礼物
            //点击确定：调用登录窗体
            Login.tishibox();
            $('.js_tishibox_pop').find('.tit2').html(data.msg);
            return;
          } else {
            // (new Dialog).alert(data.msg);
            Login.tishibox();
            $('.js_tishibox_pop').find('.tit2').html(data.msg);

            return !1;
          }
        } else {
          _DATA.user.level = data.level;
          _DATA.user.coin = data.coin;
          User.updateMoney();
          data.level = data.level.toString();
          data.uid = data.uid.toString();

          if (data.iswin == 1) {
            layer.msg("恭喜您中得" + data.winLiveCoin + "直播券");
          }

          var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"SendGift","evensend":"' + data.evensend + '","action":"0","ct":"' + data.gifttoken + '","msgtype":"1","level":"' + data.level + '","giftGroupNum":"' + data.giftGroupNum + '","selecename":"' + selecename + '","uid":"' + data.uid + '","timestamp":"' + WlTools.FormatNowDate() + '","uname":"' + _DATA.user.user_nicename + '","uhead":"' + _DATA.user.avatar + '"}]}';
          Socket.emitData('broadcast', msg);
        }
      },
      error: function (data) {
        alert("请重试");
      }
    });
  }
};

/**
 * 排行榜
 */
var Rank = {
  minus_h: 243,
  tab: $(".MR-rank .tab"),
  tabli: $(".MR-rank .tab li"),
  tabc: $(".MR-rank .con"),
  info: $("#LF-area-info"),
  rank: $(".MR-rank"),
  min_height: 155,
  max_height: 280,
  showRankUrl: '/index.php?g=home&m=show&a=rank',
  init: function () {
    this.adddate();
    this.setHeight();
    this.addEvent();
    this.switchTab();
  },
  setScorll: function () {
    var _this = this;
    this.tabli.each(function (i) {
      var scroller = new Scroll;
      scroller.init({
        Ele_panel: _this.tabc.find(".tab-con").eq(i).find(".boards"),
        Ele_scroll: _this.tabc.find(".tab-con").eq(i).find(".board-scroll")
      });
      $(this).data("scroller", scroller);
    })
  },

  setHeight: function () {
    var _this = this;
    var height = $(window).height();
    if (height > maxHeight) {
      height = maxHeight;
    } else if (height < minHeight) {
      height = minHeight;
    }
    var h = height - _this.minus_h;

    //获取右侧视频播放区域的高度
    var videoH = $(".channel-player-v2").height();
    //alert(videoH);
    //判断右侧视频区域的高度同当前区域设定的宽高大小
    var rankListH = 0;
    if (videoH > _this.max_height) {
      rankListH = _this.max_height;
    } else if (videoH < _this.min_height) {
      rankListH = _this.min_height;
    } else {
      rankListH = videoH;
    }


    // _this.info.css("height", height);
    _this.rank.find(".other").css("height", ($('#LF-stager').height() - $('.ck-slide').height()) / 2 - 90);
    /*_this.rank.find(".other").css("height",280);*/
  },
  resetSH: function () {
    this.tabli.each(function () {
      var _scroller = $(this).data("scroller");
      /* _scroller.resetH();
				_scroller.toTop(); */
    })
  },
  addEvent: function () {
    var _this = this;
    $(window).on("resize", function () {

      _this.setHeight();
      _this.resetSH();

    })
  },
  switchTab: function () {
    var _this = this;
    this.tabli.on("click", function (i) {
      var i = $(this).index();
      $(this).addClass("on");
      $(this).siblings().removeClass("on");
      _this.tabc.find(".tab-con").hide();
      _this.tabc.find(".tab-con").eq(i).show();

      var _scroller = $(this).data("scroller");
      /* _scroller.resetH();
				_scroller.toTop(); */

    })
  },
  adddate: function () {
    var room = new Room();
    if (!room.id) return;

    var _this = this;
    //var showid = (_DATA.live && _DATA.live.showid) || 0;
    $.ajax({
      url: this.showRankUrl,
      type: 'GET',
      // data: {liveid: _DATA.anchor.id, live_type: 0},
      data: {
        liveid: room.id,
        live_type: room.typeId
      },
      dataType: 'json',
      success: function (res) {
        if (res.code == 200) {
          /* var html="";
          var html_now_t=_this.setThr(data.now.slice(0,3) );
          var html_now_o=_this.setOther(data.now.slice(3) );
          var html_all_t=_this.setThr(data.all.slice(0,3) );
          var html_all_o=_this.setOther(data.all.slice(3) );
          html+='<div class="tab-con now" ><div class="thr">'+html_now_t+'</div><div class="other"><div class="boards"><ul class="clearfix">'+html_now_o+'</ul></div><div class="board-scroll"></div></div></div>'
                  +'<div class="tab-con all hide" ><div class="thr">'+html_all_t+'</div><div class="other"><div class="boards"><ul class="clearfix">'+html_all_o+'</ul></div><div class="board-scroll"></div></div></div>';
          _this.tabc.html(html);
          _this.setScorll();
           */
          var html = '';
          var data = res.data;
          var html_now_o = _this.setOther(data.now);
          var html_all_o = _this.setOther(data.all);
          html += '<div class="tab-con now" id="sortShow"><div class="other"><div class="boards"><ul class="clearfix">' + html_now_o + '</ul></div><div class="board-scroll"></div></div></div>' +
            '<div class="tab-con all hide" ><div class="other"><div class="boards"><ul class="clearfix">' + html_all_o + '</ul></div><div class="board-scroll"></div></div></div>';

          _this.tabc.html(html);
          // User.getOnline(); //获取排行版后为啥要再次获取用户列表？此处先注释
          $("#LF-nav-bg").css("display", "block");
          $("#LF-area-info").append($("#LF-nav-bg"));

          _this.setHeight();
        } else {
          // console.log(res.message);
          // layer.msg('未开始直播');
        }
      },
      error: function () {

      }
    })
  },
  setThr: function (arr) {
    var len = 3,
      classes = ["f", "s", "t"];
    _html = "";
    for (var i = 0; i < len; i++) {
      var _temp = {
        uid: "",
        total: "",
        userinfo: {
          user_nicename: "",
          avatar: ""
        }
      };
      arr[i] && (_temp = arr[i]),
        _html += '<div class="' + classes[i] + '">' + '<div class="stage">',
        _temp.total != "" && (_html += '<span class="ICON-coins">' + _temp.total + "</span>"),
        _html += "</div>",
        _temp.userinfo.avatar != "" && (_html += '<div class="photo"><img src="' + _temp.userinfo.avatar + '" data-id="' + _temp.uid + '" />' + "<cite>" + (i + 1) + '</cite><i></i></div><p class="name" title="' + _temp.userinfo.user_nicename + '">' + _temp.userinfo.user_nicename + "</p>"),

        _html += "</div>"
    }
    return _html;
  },
  setOther: function (arr) {
    /* 	var _html = "";
		for (var i = 0,len = arr.length; i < len; i++){
			_html += '<li data-id="' + arr[i].uid + '">',
			_html += "<label>" + (i + 4) + "</label>",
			_html += '<span title="' + arr[i].userinfo.user_nicename + '" class="name">' + arr[i].userinfo.user_nicename  + "</span>",
			_html += '<span class="ICON-coins">' + arr[i].total + "</span>",
			_html += "</li>";

		}
		return _html; */
    var _html = "";
    for (var i = 0, len = arr.length; i < len; i++) {
      _html += '<li data-id="' + arr[i].uid + '">';

      if (i == 0) {
        _html += "<label class='firstSendIcon'></label>";
      } else if (i == 1) {
        _html += "<label class='secondSendIcon'></label>";
      } else if (i == 2) {
        _html += "<label class='thirdSendIcon'></label>";
      } else {
        _html += "<label>" + (i + 1) + "</label>";
      }

      // 贡献榜兼容脏数据 noah.g
      try {
        _html += '<span title="' + arr[i].userinfo.user_nicename + '" class="name">' + arr[i].userinfo.user_nicename + "</span>";
        _html += '<span class="ICON-coins">' + arr[i].total_livecoin + "</span>";
      } catch (e) {}
      _html += "</li>";
    }
    return _html;
  }
};

/* 分享 */
var Share = {
  share: $(".nav-link  .share"),
  info: $("#LF-share"),
  shareBtn: $("#LF-share .detail a"),
  title: _DATA.config.sitename,
  url: encodeURIComponent(location.href),
  pic: encodeURIComponent(_DATA.anchor.avatar),
  shareText: encodeURIComponent('天啦噜直播还可以这么玩儿，宝宝们快来围观！我在 #云豹直播#，一个高颜值的直播平台！（分享自@云豹直播）'),
  api: {
    weibo: "http://service.weibo.com/share/share.php?url={$url}&title={$desc}&pic={$pic}",
    qq: "http://connect.qq.com/widget/shareqq/index.html?url={$url}&desc=&title={$title}&summary={$desc}&pics={$pic}&site={$site}&style=201&width=600&height=400",
    qzone: "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url={$url}&summary={$desc}&pics={$pic}&width=98&height=22"
  },
  tiptime: null,
  init: function () {
    this.addEvent();
  },
  addEvent: function () {
    var _this = this;
    _this.share.hover(function () {
      _this.tiptime && clearTimeout(_this.tiptime);
      _this.showInfo($(this));
    }, function () {
      _this.tiptime = setTimeout(function () {
          _this.hideInfo();
        },
        50)
    })
    _this.info.hover(function () {
      _this.tiptime && clearTimeout(_this.tiptime);
    }, function () {
      _this.info.hide();
    })
    _this.shareBtn.on("click", function () {
      var target = $(this).data("target");
      _this.shareInfo(target);

    })
  },
  showInfo: function () {
    this.info.show().animate({
      "opacity": 1,
      "top": 0
    }, 200);
  },
  hideInfo: function () {
    this.info.hide().css("opacity", 0);
  },
  shareInfo: function (target) {
    var s = this.api[target];
    var u = '';
    u = s.replace("{$url}", this.url),
      u = u.replace("{$title}", this.title),
      u = u.replace("{$desc}", this.shareText),
      u = u.replace("{$pic}", this.pic),
      window.open(u)
  }

};
/*点亮*/
var Light = {
  light_url: './index.php?g=home&m=Spend&a=light',
  praises: $(".praises"),
  init: function () {

    this.addEvent();
  },
  addEvent: function () {
    var _this = this;

    _this.praises.on("click", function () {

      var room = new Room();
      var praises_paret = $(this).attr('id');
      // console.log(praises_paret);

      if (_DATA.enterChat == 0) {
        layer.msg("点亮网络服务器实时通信失败！");
        return;
      }

      if (Check.checkLogin()) {

        if (room.userIsAnchor && room.userIsLive) {


          if ($(this).parents().is('.channel-player-multi')) { //多人

            layer.msg("不能为自己点亮");
            return;

          } else if ($(this).parents('.player-praises').siblings('.channel-player-v2').hasClass('channel-player-v2')) { //单人

            layer.msg("不能为自己点亮");
            return;

          } else {

            _this.light(praises_paret);
          }

        } else {


          if (room.isLive) {
            _this.light(praises_paret);
            // console.log('light');
          } else {
            layer.msg("主播未开播");
          }

        }

      }
    })
  },
  light: function (praises_paret) {
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    var time = 5 + _DATA.user.lighttime;
    if (_DATA.user.light == 0) {
      _DATA.user.light = 1;
      var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"SendMsg","action":"0","msgtype":"2","level":"' + _DATA.user.level + '","uname":"' + _DATA.user.user_nicename + '","uid":"' + _DATA.user.id + '","heart":"1","ct":"我点亮了"}]}';
      Socket.emitData('broadcast', msg);
      // console.log('light-mesg');
    }
    // console.log(this)
    var light = '{"msg":[{"_method_":"light","action":"2","msgtype":"0","ele":"' + praises_paret + '"}],"retcode":"000000","retmsg":"OK"}';

    Socket.emitData('broadcast', light);
    // console.log('light-light');
  }
};
/* 大厅 */
var Hall = {
  times: 1,
  defnums: 12,
  ismore: 1,
  Ele_panel: '.msg-gift .MR-chat .boarder ul',
  Ele_scroll: '.msg-gift .MR-chat .scroller',
  hall_url: './index.php?g=home&m=show&a=getHall',
  hall_btn: $("#LF-toggle-hall"),
  hall_area: $("#LF-hall-bg"),
  hall_area_s: $("#LF-hall-bg .hall-body"),
  hall_body: $(".hall-area .hall-body ul"),
  hall_close: $(".hall-area .hall-close"),
  init: function () {
    //this.setScorll();
    this.addEvent();
  },
  setScorll: function () {
    this.scroll = new Scroll;
    this.scroll.init({
      Ele_panel: this.Ele_panel,
      Ele_scroll: this.Ele_scroll
    });
  },
  resetH: function () {
    this.scroll.resetH()
  },
  addEvent: function () {
    var _this = this;
    // _this.hall_btn.on("click", function () {
    //     if ($(this).hasClass("on")) {
    //         _this.closeNavBg();
    //     } else {
    //         _this.showNavBg();
    //     }
    //
    // })
    _this.hall_close.on("click", function () {
      _this.closeNavBg();
    })

    _this.hall_area_s.scroll(function () {
      var srollPos = _this.hall_area_s.scrollTop(); //滚动条距顶部距离(页面超出窗口的高度)
      var totalheight = parseFloat(_this.hall_area_s.height()) + parseFloat(srollPos);
      if ((_this.hall_area_s.height() - 50) <= totalheight && _this.ismore) {
        _this.ismore = 0;
        _this.getHall();
      }
    });
    $("body").on("mouseenter", ".host-pic",
        function () {
          $(this).find(".play-mask").show()
        }),
      $("body").on("mouseleave", ".host-pic",
        function () {
          $(this).find(".play-mask").hide()
        })
  },
  showNavBg: function () {
    var _this = this;
    $("body").css("overflow", "hidden");
    _this.hall_btn.addClass("on");
    _this.hall_area.show().animate({
      "left": '70px',
      "opacity": 1
    }, 200)
    _this.times = 1;
    _this.ismore = 1;
    _this.getHall();
  },
  closeNavBg: function () {
    var _this = this;
    $("body").css("overflow", "auto");
    _this.hall_btn.removeClass("on");
    _this.hall_area.animate({
      "left": '-600px',
      "opacity": 0
    }, 200);
    setTimeout(function () {
      _this.hall_area.hide();
    }, 300)
  },
  getHall: function () {
    var _this = this;
    $.ajax({
      url: _this.hall_url,
      data: {
        times: _this.times,
        defnums: _this.defnums
      },
      dataType: "json",
      success: function (data) {
        var list = data,
          len = list.length,
          html = '';
        for (var i = 0; i < len; i++) {
          var userinfo = list[i];
          html += '<li><div class="live-panel"><div class="host-pic">';
          html += '<img width="180" height="101" src="' + userinfo.thumb + '">';
          html += '<a class="play-mask" href="./index.php?g=home&m=show&a=index&roomnum=' + userinfo.uid + '"  style="display: none;"><div class="back"></div><span class="play-btn">播放</span></a></div>';
          html += '<h4 class="name"><a href="./index.php?g=home&m=show&a=index&roomnum=' + userinfo.uid + '" >' + userinfo.user_nicename + '</a></h4>';
          html += '<p class="status"><span class="live-time"><span class="ico ico-clock"></span><span class="time">' + userinfo.length + '</span></span><span class="viewer"><span class="ico ico-peo"></span><span class="num">' + userinfo.nums + '</span></span></p>';
          html += '<span class="live-tip ">直播中<span class="arrow"></span></span>';
          html += '</div></li>';
        }
        if (_this.times == 1) {
          _this.hall_body.html(html);
        } else {
          _this.hall_body.append(html);
        }
        if (len < _this.defnums) {
          _this.ismore = 0;
        } else {
          _this.ismore = 1;
          _this.times++;
        }
      },
      error: function (request) {}

    })

  }


};
/* 初始化 */
var Initial = {
  init: function () {
    Video.init();
    Chat.init();
    Giftlist.init();
    Gift.init();
    Controls.init();
    Light.init();
    Rank.init();
    User.init();
    Share.init();
    Hall.init();
    Light.light();
  },
  addEvent: function () {
    /* $("#dragLine").KSubfield({
				_axes: "y",
				_axesElement: "#upChat,#downChat",
				_topHeight: 170,
				_bottomHeight: i
		})	 */
  }

};
var Select = {};
$(function () {
  /*$.fn.extend({
		showGiftList: function(t) {
			return this.each(function(e) {
					var a = $("li", this),
					i = new Array;
					a.each(function(e) {
							return e < t.num ? void $(this).show() : void $(this).hide()
					});
					if(a.size() % t){
						for (var e = 0; e < t.num - a.size() % t.num; e++) i.push("<li " + (a.size() < t.num ? "": 'style="display:none"') + "></li>");
						$(this).append(i.join(""))
					}

			}),
			this
		}
	});*/
  /*$(".gift-group .gift-wrap .con ul").showGiftList({
		num: 9
	});*/
  $(".left-arrow, .right-arrow").click(function () {
    var t = $(this).hasClass("right-arrow"),
      e = $(".gift-wrap:visible"),
      a = $("li:visible:" + (t ? "last" : "first"), e),
      i = a.index();
    if (!(!t && 0 >= i || t && i >= $("li", e).size() - 1)) {
      $("li:visible", e).hide();
      for (var n = 1; 9 >= n; n++) {
        var o = t ? i + n : i - n;
        $("li:eq(" + o + ")", e).show()
      }
    }
  })

  $("#LF-nav-bg *:not('.clearfix')").click(function (event) {

    if (event.target == this) {
      $("LF-nav-bg .MR-online .popup, #LF-nav-bg .MR-online .popupOwn").remove();
    }
  });

  $(document).on('mouseleave', '#LF-nav-bg .MR-online .popup, #LF-nav-bg .MR-online .popupOwn', function (event) {

    event.stopPropagation();

    this.remove();
  })

  swfobject.embedSWF("/public/home/js/Gifts.swf", "LF-gift-flash", "100%", "100%", "10.0", "", {}, {
    wmode: "transparent",
    allowscriptaccess: "always"
  });
  Initial.init();


});
/**
 * 获取layer居中的位置
 */
var getMiddlePos = function (obj) {
  this.objPop = obj;
  this.winW = oPos.windowWidth();
  this.winH = oPos.windowHeight();
  this.dScrollTop = oPos.scrollY();
  this.dScrollLeft = oPos.scrollX();
  this.dWidth = $('#' + this.objPop).width(), dHeight = $('#' + this.objPop).height();
  this.dLeft = (this.winW / 2) - (this.dWidth) / 2 + this.dScrollLeft;
  this.dTop = (this.winH / 2) - (this.dHeight / 2) + this.dScrollTop;
  return {
    "pl": this.dLeft,
    'pt': this.dTop
  };
}
/**
 * 判断浏览器
 */
var Sys = {};
var Gift_obj = {};
var Gift_numobj = {};
var ua = navigator.userAgent.toLowerCase();
Sys.ie = (s = ua.match(/msie ([\d.]+)/)) ? true : false;
Sys.ie6 = (s = ua.match(/msie ([0-6]\.+)/)) ? s[1] : false;
Sys.ie7 = (s = ua.match(/msie ([7]\.+)/)) ? s[1] : false;
Sys.ie8 = (s = ua.match(/msie ([8]\.+)/)) ? s[1] : false;
Sys.firefox = (s = ua.match(/firefox\/([\d.]+)/)) ? true : false;
Sys.chrome = (s = ua.match(/chrome\/([\d.]+)/)) ? true : false;
Sys.opera = (s = ua.match(/opera.([\d.]+)/)) ? s[1] : false;
Sys.safari = (s = ua.match(/version\/([\d.]+).*safari/)) ? s[1] : false;
Sys.ie6 && document.execCommand("BackgroundImageCache", false, true);
Sys.ispro = ""; //是否推广url过来
String.prototype.hasString = function (a) {
  if (typeof a == "object") {
    for (var b = 0, c = a.length; b < c; b++)
      if (!this.hasString(a[b]))
        return false;
    return true
  } else if (this.indexOf(a) != -1)
    return true
};

/**
 * 计算位置
 */
var dom = document.documentElement || document.body;
var oPos = {
  width: function (a) {
    return parseInt(a.offsetWidth)
  },
  height: function (a) {
    return parseInt(a.offsetHeight)
  },
  pageWidth: function () {
    return document.body.scrollWidth || document.documentElement.scrollWidth
  },
  pageHeight: function () {
    return document.body.scrollHeight || document.documentElement.scrollHeight
  },
  windowWidth: function () {
    var a = document.documentElement;
    return self.innerWidth || a && a.clientWidth || document.body.clientWidth
  },
  windowHeight: function () {
    var a = document.documentElement;
    return self.innerHeight || a && a.clientHeight || document.body.clientHeight
  },
  scrollX: function () {
    var b = document.documentElement;
    return self.pageXOffset || b && b.scrollLeft || document.body.scrollLeft
  },
  scrollY: function () {
    var b = document.documentElement;
    return self.pageYOffset || b && b.scrollTop || document.body.scrollTop
  },
  popW: function () {
    return Math.max(dom.clientWidth, dom.scrollWidth)
  },
  popH: function () {
    return Math.max(dom.clientHeight, dom.scrollHeight)
  }
}
var mousePosition = function (e) {
  var e = e || window.event;
  return {
    x: e.clientX + oPos.scrollX(),
    y: e.clientY + oPos.scrollY()
  }
}


$(function () {

  /*控制礼物组显示或隐藏*/

  $(".giftGroupIcon").on('click', function () {

    var dis = $(".giftGroups").css('display');
    if (dis == 'block') {
      $(".giftGroups").hide();
      $(this).parent().removeAttr('style');
      $(this).parent().css('color', '#000')
      $(this).css('background-image', 'url(/public/home/show/images/giftIconUp.png)');
    } else {
      $(".giftGroups").show();
      $(this).parent().attr('style', 'border-color:#ff3366');
      $(this).siblings().css('color', '#ff3366')
      $(this).css('background-image', 'url(/public/home/show/images/giftIconDown.png)');
    }
  });

  /*选择礼物组数值*/
  $(".giftGroups li").on('click', function () {
    var vals = $(this).children('span').text();
    $('.giftGroupNum').removeAttr('style');
    $(".giftGroupNumVal").val(vals);
    $(".giftGroups").hide();
    $('.giftGroupIcon').css('background-image', 'url(/public/home/show/images/giftIconUp.png)');

  });

  /*购买守护功能*/

  $("#bugGuard").on('click', function () {
    //询问框
    layer.confirm('是否花费' + _DATA.config.guard_coin + 'CNY购买一个月守护？', {
      btn: ['购买', '取消'] //按钮
    }, function () {
      $.ajax({
        url: '/api/public/index.php?service=Live.buyGuard',
        type: 'POST',
        dataType: 'json',
        data: {
          uid: _DATA.user.id,
          token: _DATA.user.token,
          liveuid: _DATA.anchor.id
        },
        success: function (data) {
          var res = data.data;

          if (res.code != 0) {
            layer.msg(res.msg);
          } else {

            layer.msg('购买守护成功');
            $(".MR-talk .speaker cite").text(_DATA.config.guard_digitsnum);
            Chat.chat_max_text_len = _DATA.config.guard_digitsnum;

            //更新守护信息
            _DATA.guardInfo = JSON.stringify(res.info[0].guardInfo);

            if (_DATA.config.guard_digits_num > 0) {
              $(".MR-talk .speaker cite").text(_DATA.config.guard_digits_num);
              Chat.chat_max_text_len = _DATA.config.guard_digits_num;

            }

            //刷新用户列表
            User.getOnline();

            //刷新排行榜
            Rank.adddate();
          }
        },
        error: function () {
          layer.msg("购买守护失败");
        }
      });


    }, function () {
      layer.closeAll();
    });
  });


  $("#ChannelShutUp").click(roomShutUp);


});


/*
 *解除房间禁言和设定房间禁言方法
 */
var roomShutUp = function () {
  var room = new Room();
  if ($("#ChannelShutUp").text().replace(/\s/g, "") == '频道禁言') { //禁言
    //询问框
    layer.confirm('是否将频道禁言', {
      btn: ['确定', '取消'] //按钮
    }, function () {
      var userID = _DATA.user.id;
      $.ajax({
        url: '/index.php?g=Home&m=Spend&a=channelShutUp',
        type: 'POST',
        dataType: 'json',
        data: { /*userID: userID,*/
          liveid: room.id,
          live_type: room.typeId
        },
        success: function (data) {
          if (data.code == 400) {
            layer.msg(data.message);
            return;
          } else if (data.code == 200) {
            layer.msg('房间禁言成功', {
              icon: 1
            });
            $("#ChannelShutUp").text('解除禁言').toggleClass("obtrusive").toggleClass("unobtrusive");;
            var vestName = "";

            if (_DATA.user.issuper == 1) {
              vestName = "(超管)";
            } else if (room.userIsAnchor) {
              vestName = "(主播)";
            } else {
              vestName = "(" + data.data.vestName + ")";
            }
            var msg = '{"msg":[{"_method_":"channel_gap","action":"0","ct":"' + _DATA.user.user_nicename + vestName + '将房间禁言啦","msgtype":"0"}],"retcode":"000000","retmsg":"OK"}';
            Socket.emitData('broadcast', msg);
          } else {
            layer.msg(data.message);
          }


          /* if (data.code != 0) {
               layer.msg(data.msg);
               return;
           } else {
               layer.msg('房间禁言成功', {icon: 1});
               $("#ChannelShutUp").text('解除禁言');
               var vestName = "";

               if (_DATA.user.issuper == 1) {
                   vestName = "(超管)";
               } else if (room.userIsAnchor) {
                   vestName = "(主播)";
               } else {
                   vestName = "(" + data.vestName + ")";
               }
               var msg = '{"msg":[{"_method_":"channel_gap","action":"0","ct":"' + _DATA.user.user_nicename + vestName + '将房间禁言啦","msgtype":"0"}],"retcode":"000000","retmsg":"OK"}';
               Socket.emitData('broadcast', msg);
               return;
           }*/
        },
        error: function (data) {
          layer.msg("频道禁言失败啦");
        }
      });
    }, function () {
      layer.closeAll();
    });
  } else { //解除禁言
    layer.confirm('是否将频道解除禁言', {
      btn: ['解除', '取消'] //按钮
    }, function () {
      var userID = _DATA.user.id;
      $.ajax({
        url: '/index.php?g=Home&m=Spend&a=delChannelShutUp',
        type: 'POST',
        dataType: 'json',
        // data: {userID: userID, roomid: room.id},
        data: {
          liveid: room.id,
          live_type: room.typeId
        },
        success: function (data) {
          if (data.code == 400) {
            layer.msg(data.message);
            return;
          } else if (data.code == 200) {
            layer.msg('房间禁言成功解除', {
              icon: 1
            });
            $("#ChannelShutUp").text('频道禁言').toggleClass("obtrusive").toggleClass("unobtrusive");;
            var vestName = "";
            if (_DATA.user.issuper == 1) {
              vestName = "(超管)";
            } else if (room.userIsAnchor) {
              vestName = "(主播)";
            } else {
              vestName = "(" + data.data.vestName + ")";
            }
            var msg = '{"msg":[{"_method_":"channel_gap","action":"1","ct":"' + _DATA.user.user_nicename + vestName + '将房间解除禁言","msgtype":"0"}],"retcode":"000000","retmsg":"OK"}';
            Socket.emitData('broadcast', msg);
          } else {
            layer.msg(data.message);
          }

          // if (data.code != 0) {
          //     layer.msg(data.msg);
          //     return;
          // } else {
          //     layer.msg('房间禁言成功解除', {icon: 1});
          //     $("#ChannelShutUp").text('频道禁言');
          //     var vestName = "";
          //     if (_DATA.user.issuper == 1) {
          //         vestName = "(超管)";
          //     } else if (room.userIsAnchor) {
          //         vestName = "(主播)";
          //     } else {
          //         vestName = "(" + data.vestName + ")";
          //     }
          //     var msg = '{"msg":[{"_method_":"channel_gap","action":"1","ct":"' + _DATA.user.user_nicename + vestName + '将房间解除禁言","msgtype":"0"}],"retcode":"000000","retmsg":"OK"}';
          //     Socket.emitData('broadcast', msg);
          // }
        },
        error: function (data) {
          layer.msg("解除禁言失败啦");
        }
      });
    }, function () {
      layer.closeAll();
    });
  }
};




$(function () {
  $(".searchUser").on('click', function () {
    $(".searchRes").html("");
    $(".searchUserAreas").show();

  });

  $(".searchUserAreas cite").on('click', function () {

    $(".searchRes").html("");

    $(".searchUserAreas").hide();
  });

  $(".searchBtn").on('click', function () {
    var user_nicename = $(".searchInput").val();
    if (user_nicename == "") {
      layer.msg("请输入用户昵称");
      return;
    }

    $.ajax({
      url: '/index.php?g=Home&m=Show&a=searchInfoByNicename',
      type: 'POST',
      dataType: 'json',
      data: {
        user_nicename: user_nicename
      },
      success: function (data) {
        if (data.code == 0) {
          var arr = data.info;
          var str = "<li>\
								<p class='searchUserID'>ID</p>\
								<p class='searchUserNicename'>用户昵称</p>\
								<div class='clearboth'></div>\
							</li>";

          for (var i = 0; i < arr.length; i++) {
            str += "<li><p class='searchUserID'>" + arr[i].id + "</p><p class='searchUserNicename'>" + arr[i].user_nicename + "</p><div class='clearboth'></div></li>";
          }

          $(".searchRes").html(str);
        } else {
          $(".searchRes").html("未查询到相关数据");
        }
      },
      error: function (data) {
        layer.msg("查询失败啦");
      }
    });
  });


  // **礼物滚动

  var step = 0;
  $('.MR-gift .M-arrow-scroll .con').on("mousewheel", function (event, delta) {
    // console.log(step)
    if (delta < 0 && step < $(this).children().height() - 50) {
      step += 10;

    } else if (delta > 0 && step > 0) {
      step -= 10;
    }

    $(this).scrollTop(step);
    return false;
  });


});




//判断flash插件是否被禁用
function flashChecker() {
  var hasFlash = 0;　　　　 //是否安装了flash
  var flashVersion = 0;　　 //flash版本
  if (document.all) {
    var swf = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
    if (swf) {
      hasFlash = 1;
      VSwf = swf.GetVariable("$version");
      flashVersion = parseInt(VSwf.split(" ")[1].split(",")[0]);
    }
  } else {
    if (navigator.plugins && navigator.plugins.length > 0) {
      var swf = navigator.plugins["Shockwave Flash"];
      if (swf) {
        hasFlash = 1;
        var words = swf.description.split(" ");
        for (var i = 0; i < words.length; ++i) {
          if (isNaN(parseInt(words[i]))) {
            continue;
          }
          flashVersion = parseInt(words[i]);
        }
      }
    }
  }
  return {
    f: hasFlash,
    v: flashVersion
  };
}
