/*
 * Description: 房间信息的一些封装
*/

;(function (global, oDoc, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(oDoc) :
        typeof define === 'function' && define.amd ? define([], function () {
                return factory(oDoc);
            }) :
            (global.Room = factory(oDoc));
})(this, document, function (oDoc) {
    'use strict';

    function Room() {
        if (!_DATA.live) {
            _DATA.live = {
                "uid": "",
                "user_nicename": "",
                "stream": "",
                "islive": "1",
                "chat_num": "30",
                "chat_frequency": "0",
                "avatar": "/data/upload/20180320/affde8d2202b1616e3c27f191921dd84.png",
                "avatar_thumb": "/data/upload/20180320/affde8d2202b1616e3c27f191921dd84.png",
                "thumb": "",
                "showid": "",
                "starttime": "",
                "title": "未设置标题",
                "province": "",
                "city": "好像在火星",
                "pull": "",
                "lng": "",
                "lat": "",
                "topicid": "0",
                "type": "0",
                "type_val": "",
                "ispc": "1",
                "lianmai_stream": null
            }
        }

        if(!_DATA.anchor) {
            _DATA.anchor = {}
        }

        if (_DATA.multiLive && _DATA.multiLive.id) {
            this.typeId = 1;
            this.typeName = 'multi'; // 单直播间： 'single'  多直播间： 'multi'
            this.id = _DATA.multiLive.id; // 房间的id
            this.userIsAnchor = _DATA.multiLive.ami_anchor; // 当前用户是否是此房间的主播
            this.userIsDj = _DATA.multiLive.ami_dj; // 当前用户是否是此房间的麦手
            this.userIsLive = (!$.isEmptyObject(rtc.liveStreams) && !!rtc.liveStreams[_DATA.user.id]); // 当前用户是否在直播
            this.chat_num = parseInt(_DATA.multiLive.chat_num || 30);   // 聊天的字数
            this.chat_frequency = parseInt(_DATA.multiLive.chat_frequency || 0); // 聊天的频率
            this.isLive = !$.isEmptyObject(rtc.liveStreams);  // 当前房间是否在直播
            // this.ispc = ;
        } else {
                this.typeId = 0;
                this.typeName = 'single'; // 单直播间： 'single'  多直播间： 'multi'
                // this.id =  _DATA.live.uid; // 房间的id
                this.id = _DATA.anchor.id; // 房间的id
                this.userIsAnchor =  (_DATA.user && _DATA.user.id == _DATA.anchor.id);             // 当前用户是否是此房间的主播
                // this.userIsDj =   // 当前用户是否是此房间的麦手
                this.userIsLive =  this.userIsAnchor || (_DATA.live.islive == 1);    // 当前用户是否在直播
                this.chat_num =parseInt(_DATA.live.chat_num);             // 聊天的字数
                this.chat_frequency = parseInt(_DATA.live.chat_frequency);             // 聊天的频率
                this.isLive = (_DATA.live.islive == 1);             // 当前房间是否在直播
                this.ispc = (_DATA.live.ispc == 1);
        }
        this.userIsSuper = (_DATA.user && _DATA.user.issuper && _DATA.user.issuper == 1); //用户是否是超管
        this.userIsLogged = (_DATA.user && _DATA.user.logged == 1);             // 用户是否登陆
    }

    /**
     *
     */
    Room.prototype.set = function () {

    };


    return Room;
});


