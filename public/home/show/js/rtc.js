var rtc={
    client:{},			                            		//客户端对象
    localStream:{},                              		    //本地流对象
    devices:{                                              //本地设备列表
        videoArray:[],
        audioArray:[]
    },
    role:"user",                                        //当前角色：user 普通用户,anchor 连麦主播,speaker 连麦麦手
    isMediaAccessAllowed: false,  //是否获得了摄像头和麦克风的权限
    isEventInit: false,
    audioCombo:"audioSource",                           //音频设备选择下拉元素id
    videoCombo:"videoSource",                           //视频设备选择下拉元素id
    prevRegion:"agora_preview",                         //视频预览区域元素id
    playRegion:"agora_local",                         //大视频播放区域元素id
    liveStreams: {}, //主播视频流的集合, 以uid为key
    macStreams: {}, //麦手音频流的集合, 以uid为key
    anchors: {},
    config:{
        appkey:"84a87a0c3dda44c695743dd84428c0cb",     //appKey 默认
        channel:"201802"
    },
    user:{
        uid:"",
        role:"user"                                 //默认用户角色
    },
    getBrowerVersions: function( ) {
        var u = navigator.userAgent,
            app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
            iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
            qq: u.match(/\sQQ/i) == " qq" //是否QQ
        };
    },
    /*
        根据角色检查浏览器兼容性
        client.init
        @param: config
        @param: user
        return: null
        */
    checkBrowser:function(roleType){
        var versions = rtc.getBrowerVersions();
        var room = new Room();
        var isSupport=true;
        try{
            if(!AgoraRTC.checkSystemRequirements() && ((room.userIsAnchor || room.userIsDj))) {   //非用户角色才严格检查浏览器
                isSupport=false;
            }else{ //用户角色检查是否使用谷歌火狐opera类浏览器
                isSupport=(versions.presto || versions.webKit || versions.gecko || versions.android || versions.weixin)?true:false;
            }
        }catch(e){
            isSupport=false;
        }
        return isSupport;
    },



    /*
    初始化配置及创建Client 对象
    client.init
    @param: config
    @param: user
    return: null
    */
    init:function(config,user){
        if(!rtc.checkBrowser()) {
            alert("请切换为极速（chrome内核）模式，或下载firfox或chrome浏览器观看");
            return;
        }
        rtc.config=$.extend(rtc.config,config);
        rtc.user=$.extend(rtc.user,user);
        rtc.curRole=rtc.user.curRole||"user";          //如果config.curRole为空。则默认为普通用户角色
        rtc.localUid = rtc.user.uid;
        // console.log(rtc.config);
        // console.log(rtc.user);
        rtc.getDevices(true);
    },
    /*
	离开当前房间频道
	crt.leave
	@param: null
	return: null
	*/
    leave:function(){
        $('#leave').attr('disabled',"true");
        rtc.client.leave(function () {
            // console.log("Leavel channel successfully");
        }, function (err) {
            // console.log("Leave channel failed");
        });
    },
    /*
    预览展示
    crt.preview
    @param: null
    return: null
    */
    preview:function(){
        this.createStream();
        rtc.localStream.init(function() {
            // console.log("getUserMedia successfully");
            if(rtc.user.isAmiAnchor){ //如果是主播
                rtc.localStream.play(rtc.prevRegion);
            }
            rtc.getDevices();
        }, function (err) {
            // console.log("getUserMedia failed", err);
        });
    },
    cancelPreview: function() {
        rtc.isMediaAccessAllowed = false;
        if(rtc.localStream) {
            rtc.localStream.stop();
            rtc.localStream.close();
        }
        
    },
    disableVideo: function(steam) {
        steam.disableVideo();
    },
    enableVideo: function(steam) {
        steam.enableVideo();
    },
    disableAudio: function(steam) {
        steam.disableAudio();
    },
    enableAudio: function(steam) {
        steam.enableAudio();
    },
    /*
	发布本地视频（主播）
	crt.publish
	@param: null
	return: null
	*/
    publish:function(callback) {
        $('#publish').attr('disabled',"true");
        $('#unpublish').removeAttr("disabled");
        if(!rtc.localStream) {
            this.createStream();
        }
        if(rtc.localStream){
            rtc.localStream.stop(rtc.prevRegion); //销毁预览视频区
	        rtc.localStream.play(rtc.playRegion); //在大窗口显示预览


            if(rtc.isEventInit) {
                callback && callback();
                return;
            }
            rtc.isEventInit = true;

            //启用大小流
            rtc.client.enableDualStream(function(){
                console.log("Enable dual stream success");
            },function(err){
                console.log("Enable dual stream failed:"+err);
            });

            

            rtc.client.on('stream-published', function (evt) {

                // $("#steamId").val(evt.stream.getId());
                // console.log("Publish local stream successfully");
                if(rtc.liveStreams[rtc.localUid]) return; // 上播后，因为大小流的问题，stream-published会被触发两次
                $("#" + rtc.playRegion).attr({uid: rtc.localUid});
                callback && callback();
                rtc.liveStreams[rtc.localUid] = evt.stream;
                multi.getUserName(rtc.localUid, 1);
            });

            rtc.client.publish(rtc.localStream, function (err) {
                // console.log("Publish local stream error: " + err);
                layer.alert("上播失败，请检查网络",
                    {
                        skin: 'layui-layer-molv' //样式类名
                        ,closeBtn: 0,
                        shift: 5,
                        icon: 2,
                    }, function(){
                        location.reload();
                    });
            });

        }
    },
    /*
   发布本地音频(麦手)
   crt.publish
   @param: null
   return: null
   */
    publish2:function(callback) {
        $('#publish').attr('disabled',"true");
        $('#unpublish').removeAttr("disabled");
        if(!rtc.localStream) {
            this.createStream();
        }
        if(rtc.localStream){
            if(rtc.isEventInit) {
                callback && callback();
                return;
            }
            rtc.isEventInit = true;

            rtc.client.on('stream-published', function (evt) {
                // $("#steamId").val(evt.stream.getId());
                // console.log("Publish local stream successfully");
                callback && callback();
                rtc.macStreams[rtc.localUid] = evt.stream;
            });

            rtc.client.publish(rtc.localStream, function (err) {
                // console.log("Publish local stream error: " + err);
                layer.alert("上麦失败，请检查网络",
                    {
                        skin: 'layui-layer-molv' //样式类名
                        ,closeBtn: 0,
                        shift: 5,
                        icon: 2,
                    }, function(){
                        location.reload();
                    });
            });

        }
    },
    /*
	 创建本地视频流
	 crt.createStream
	 @param: null
	 return: null
	 */
    createStream:function(){
        var config={streamID:rtc.localUid, audio:true,video:true,cameraId:$('#' + rtc.videoCombo).val(),microphoneId: $('#' + rtc.audioCombo).val(),  screen: false};
        if($("#wheatSetting").is(":visible")){ //如果打开的是喊麦设置弹框（此处不能用角色判断，因为一个用户可以多角色）
            config.video=false;
            config.cameraId="";
        }
        // console.log(JSON.stringify(config, 2, 2))
        rtc.localStream = AgoraRTC.createStream(config);
        if (config.video) {
            rtc.localStream.setVideoProfile('360P');
        }
        //如果用户有授权音视频成功
        rtc.localStream.on("accessAllowed", function() {
            rtc.isMediaAccessAllowed = true;
            // console.log("accessAllowed");
        });
        //如果用户有授权失败
        rtc.localStream.on("accessDenied", function() {
            rtc.isMediaAccessAllowed = false;
            alert("请启用摄像头或麦克风设备");
        });
    },
    /*
	取消发布本地视频
	crt.unpublish
	@param: null
	return: null
	*/
    unpublish:function() {
        $('#unpublish').attr('disabled',"true");
        $('#publish').removeAttr("disabled");
        rtc.client.unpublish(rtc.localStream, function (err) {
            // console.log("Unpublish local stream failed" + err);
        });

        rtc.deleteStream(rtc.localStream);
    },

    /**
     * 是否是直播的流
     * @param stream
     * @returns {boolean}
     */
    isLiveStream: function(stream) {
        var anchors =  _DATA.multiLive.anchor;
        var streamId = stream.getId();
        var isAnchor = false;
        if(anchors) {
            $.each(anchors, function(i, v) {
                if(v.uid == streamId) {
                    isAnchor = true;
                }
            });
        } else {
            isAnchor = true;
        }
        if (stream.video && isAnchor) {
            return true;
        } else {
            return false;
        }
    },
    /**
     * 是否是喊麦的流
     * @param stream
     * @returns {boolean}
     */
    isMacStream: function(stream) {
        var djs =  _DATA.multiLive.dj;
        var streamId = stream.getId();
        var isDj = false;
        if(djs) {
            $.each(djs, function(i, v) {
                if(v.uid == streamId) {
                    isDj = true;
                }
            });
        } else {
            isDj = true;
        }
        if (stream.audio && !stream.video && isDj) {
            return true;
        } else {
            return false;
        }
    },

    /**
     * 删除流
     * @param stream
     */
    deleteStream: function(stream) {
        var streamId = stream.getId();
        stream.stop();
        if(rtc.isLiveStream(stream)) {
            delete rtc.liveStreams[streamId];
        } else if(rtc.isMacStream(stream)){
            delete rtc.macStreams[streamId];
            $("#audioopen").hide();
        }
        $('#agora_remote' + streamId).remove();
        $('.vidcont').each(function(){
            var uid = $(this).attr('uid');
            if(uid == streamId) {
                $(this).empty();
            }
        });
    },

    /*
	加入房间,不区分角色。任何角色都应该
	crt.join
	@param: null
	return: null
	*/
    join:function(){
        $('#join,#hasVideo').attr('disabled',"true");
        var channel_key = null;
        // rtc.client = AgoraRTC.createClient({mode: 'h264_interop'}); //这种模式下搜狗浏览器播放视频为黑屏
        rtc.client = AgoraRTC.createClient({mode: 'interop'});
        rtc.client.init(rtc.config.appkey, function () {
            // console.log("AgoraRTC client initialized");
            var joinId = null;
            if(rtc.localUid.indexOf('temp') == -1) {
                joinId = parseInt(rtc.localUid);
            }
            rtc.client.join(null, rtc.config.channel, joinId, function(uid) {
            // rtc.client.join(null, rtc.config.channel, null, function(uid) { //TODO 多视频测试用
            //     rtc.localUid = uid; //TODO 多视频测试用
                // console.log("User " + (uid) + " join channel successfully");
            }, function(err) {
                // console.log("Join channel failed", err);
                debugger;
             
            });
        }, function (err) {
            // console.log("AgoraRTC client init failed", err);
        });
        rtc.addClentEvent();
    },
    /*
	添加事件处理
	crt.getDevices
	@param: null
	return: null
	*/
    addClentEvent:function(){
        if(rtc.client){
            var channelKey = "";
            rtc.client.on('error', function(err) {
                // console.log("Got error msg:", err.reason);
                if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
                    rtc.client.renewChannelKey(channelKey, function(){
                        // console.log("Renew channel key successfully");
                    }, function(err){
                        // console.log("Renew channel key failed: ", err);
                    });
                }
            });
            //远程音视频流加入事件
            rtc.client.on('stream-added', function (evt) {
                var stream = evt.stream;
                // console.log("New stream added: " + stream.getId());
                // console.log("Subscribe ", stream);
                 rtc.client.subscribe(stream, function (err) {
                     // console.log("Subscribe stream failed", err);
                 });
            });


            //远程音视频流订阅成功事件
            rtc.client.on('stream-subscribed', function (evt) {
                var stream = evt.stream;
                rtc.client.setRemoteVideoStreamType(stream, 1);
                var streamId = stream.getId();
                // var attributes = stream.getAttributes();
                // console.log("Subscribe remote stream successfully: " + stream.getId());
                if ($('#agora_remote'+streamId).length === 0) {
                    if(rtc.isLiveStream(stream)) {
                            // $('div#video').append('<div id="agora_remote'+stream.getId()+'" style="float:left; width:810px;height:607px;display:inline-block;"></div>');
                            var start = 0;
                            if(rtc.user.isAmiAnchor && $("#" + rtc.playRegion).is(':empty'))  { //主播角色 && 该主播未上播
                                start = 1;
                            }
                            if(Object.keys(rtc.liveStreams).length == 3) {
                                start = 0;
                            }
                            var vicont = $(".vidcont:empty").eq(start);


                            var streamType = 1;
                            if(vicont.attr('id') == 'agora_local') { //如果是大窗口，就播放大流；否则就是小窗口，播放小流
                                streamType = 0;
                            }
                            rtc.client.setRemoteVideoStreamType(stream, streamType);


                            vicont.attr({uid: streamId}).append('<div id="agora_remote'+streamId+'" style="float:left; width:100%;height:100%;display:inline-block;"></div>');
                            rtc.liveStreams[streamId] =  stream;


                            stream.play('agora_remote' + streamId);
                            multi.getUserName(streamId, 1);
                    } else if(rtc.isMacStream(stream)){
                        rtc.macStreams[streamId] =  stream;
                        $("#audioopen").show().append('<div id="agora_remote'+streamId+'"></div>');
                        // $("#multiDjName").text(attributes.uname + '麦手');
                        stream.play('agora_remote' + streamId);
                        multi.getUserName(streamId, 2);

                    }
                }

            });
            //远程音视频流已删除事件
            rtc.client.on('stream-removed', function (evt) {
                var stream = evt.stream;
                if (stream) {
                    rtc.deleteStream(stream);
                    //重新排序视频
                    multi.rangeVideo();
                }
                // console.log("Remote stream is removed " + stream.getId());
            });
            //用户已离开会议室事件 即对方调用了 client.leave().
            rtc.client.on('peer-leave', function (evt) {
                var stream = evt.stream;
                if (stream) {
                    rtc.deleteStream(stream);
                }
                // console.log('peer-leave')
            });
            //通知应用程序频道频道内谁在说话事件
            rtc.client.on('active-speaker', function(evt) {
                var uid = evt.uid;
                // console.log("update active speaker: client " + uid);
            });
        }
    },
    /*
	得到相关设置并填充设备选择框
	crt.getDevices
	@param: null
	return: null
	*/
    getDevices:function(fromInit){
        if(fromInit) {
            $("#videoSource").empty();
            $("#audioSource").empty();
        }

        AgoraRTC.getDevices(function (devices) {
            $.each(devices,function(i,device){
                var combox,content;
                if(device.kind === 'audioinput'){
                    rtc.devices.audioArray.push(device);
                    combox=rtc.audioCombo;
                    content=device.label || 'microphone ' + (rtc.devices.audioArray.length);
                }else if (device.kind === 'videoinput'){
                    rtc.devices.videoArray.push(device);
                    combox=rtc.videoCombo;
                    content=device.label || 'camera ' + (rtc.devices.videoArray.length);
                }

                combox="#"+combox;
                if($(combox)){
                    if(fromInit) {
                        $(combox).append("<option value='{0}'>{1}</option>".replace('{0}',device.deviceId).replace('{1}',content));
                    } else {
                        $.each($(combox).find('option'), function(index, $option) {
                            if($(this).attr('value') == device.deviceId) {
                                $(this).text(content)
                            }
                        });
                    }
                }
            });
        });
    },
}