;
$(function () {
    /*红包弹窗定位*/
    var screenW = window.screen.width;
    var redPacketsW = $("#redPecketsArea").width();
    var leftW = (screenW - redPacketsW) / 2;
    var screenH = window.screen.height;
    var redPacketsH = $("#redPecketsArea").height();
    var topH = (screenH - redPacketsH) / 2;
    $("#redPecketsArea").css('left', leftW + 30).css('top', topH - 30);

    /*点击红包按钮弹窗*/
    $("#redPacketBtn").on('click', function () {
        $("#redPecketsArea").show();
        if (_DATA.anchor.id == _DATA.user.id) {
            closeGiftPK();
        }
    });

    $(".redPacketTitle item").on('click', function () {
        $("#redPecketsArea").hide();
    });

    /*普通红包和口令红包切换*/
    $(".redPacketTab li").on('click', function () {
        var tabID = $(this).attr("id");
        $(".redPacketTab").find("p").removeClass('current');
        if (tabID == "ordinaryTab") {
            $(".ordinaryPacket").show();
            $(".commandPacket").hide();
        } else {
            $(".ordinaryPacket").hide();
            $(".commandPacket").show();
        }

        $(this).children("p").addClass('current');
    });

    /*普通红包点击提交*/
    $("#ordinaryPacketBtn").on('click', function () {
        var num = $("#ordinaryPacketNum").val();
        var totalMoney = $("#ordinaryPacketMoney").val();
        var title = $("#ordinaryPacketCommand").val();
        if (num > 10000 || num < 1) {
            layer.msg('红包数量错误');
            return;
        }

        if (totalMoney < 1) {
            layer.msg("红包金额错误");
            return;
        }

        if (parseInt(totalMoney) != totalMoney) {
            layer.msg("红包金额必须是整数");
            return;
        }

        if (0.01 * num > totalMoney) {
            layer.msg("单个红包金额最少0.01元，请重新选择");
            return;
        }

        var site_url = _DATA.getConfigPub.site_url;

        $.ajax({
            url: site_url + '/index.php?service=Live.sendRedPackets',
            type: 'POST',
            dataType: 'json',
            data: {
                uid: _DATA.user.id,
                token: _DATA.user.token,
                liveuid: _DATA.anchor.id,
                num: num,
                totalMoney: totalMoney,
                title: title,
                type: 0
            },
            success: function (data) {
                var res = data.data;
                if (res.code != 0) {
                    layer.msg(res.msg);
                    return;
                } else {
                    //成功发送红包
                    $("#ordinaryPacketNum").val('');
                    $("#ordinaryPacketMoney").val('');
                    $("#ordinaryPacketCommand").val('恭喜发财');
                    $("#redPecketsArea").hide();

                    //拼socket
                    var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"SendRedPack","sendRedUid":"' + res.info[0].uid + '","action":"0","user_nicename":"' + res.info[0].user_nicename + '","avatar":"' + res.info[0].avatar + '","msgtype":"0","redPacketType":"0","liveuid":"' + res.info[0].liveuid + '","sendtime":"' + res.info[0].sendtime + '","uid":"' + res.info[0].uid + '","title":"' + res.info[0].title + '","level":"' + res.info[0].level + '"}]}';
                    Socket.emitData('broadcast', msg);

                }
            },
            error: function (data) {

            }
        });
    });

    /*口令红包点击提交*/
    $("#commandPacketBtn").on('click', function () {
        var num = $("#commandPacketNum").val();
        var totalMoney = $("#commandPacketMoney").val();
        var title = $("#commandPacketCommand").val();
        if (num > 50 || num < 1) {
            layer.msg('红包数量错误');
            return;
        }

        if (totalMoney > 1000 || totalMoney < 1) {

            layer.msg("红包金额错误");
            return;
        }

        if (0.01 * num > totalMoney) {
            layer.msg("单个红包金额最少0.01元，请重新选择");
            return;
        }

        var site_url = _DATA.getConfigPub.site_url;

        $.ajax({
            url: site_url + 'index.php?service=Live.sendRedPackets',
            type: 'POST',
            dataType: 'json',
            data: {
                uid: _DATA.user.id,
                token: _DATA.user.token,
                liveuid: _DATA.anchor.id,
                num: num,
                totalMoney: totalMoney,
                title: title,
                type: 1
            },
            success: function (data) {
                var res = data.data;
                if (res.code != 0) {
                    layer.msg(res.msg);
                    return;
                } else {//成功发送红包
                    //layer.msg("红包发送成功",{icon: 1});
                    $("#commandPacketNum").val('');
                    $("#commandPacketMoney").val('');
                    $("#commandPacketCommand").val('恭喜发财');
                    $("#redPecketsArea").hide();

                    //拼socket
                    var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"SendRedPack","sendRedUid":"' + res.info[0].uid + '","action":"0","user_nicename":"' + res.info[0].user_nicename + '","avatar":"' + res.info[0].avatar + '","msgtype":"0","redPacketType":"1","liveuid":"' + res.info[0].liveuid + '","sendtime":"' + res.info[0].sendtime + '","uid":"' + res.info[0].uid + '","title":"' + res.info[0].title + '","level":"' + res.info[0].level + '"}]}';
                    Socket.emitData('broadcast', msg);

                }
            },
            error: function (data) {
                layer.msg("发红包失败");
            }
        });
    });
});

/*点击拆红包*/
function openRedPacket(sendRedUid, liveuid, sendtime, type, title, avatar, user_nicename) {
    var screenW = window.screen.width;
    var openRedPacketW = 300;
    var leftW = (screenW - openRedPacketW) / 2 + 30;
    var screenH = window.screen.height;
    var openRedPacketH = 400;
    var topH = (screenH - openRedPacketH) / 2;
    var packetType = "";

    if (_DATA.user == null) {
        layer.msg("先登录再抢红包哟");
        return;
    }

    var html = "<div class='openRedPacket' style='left:" + leftW + "px;top:" + topH + "px;'>";
    html += "<span class='packetCloseBtn' onclick='closeOpenRedPacket()'>×</span>";
    html += "<p class='sendPacketUserImg'><img src='" + avatar + "' /></p>";
    html += "<p class='sendPacketUserName'>" + user_nicename + "</p>";
    if (type == 0) {
        packetType = "普通";
    } else if (type == 1) {
        packetType = "口令";
    }
    html += "<p class='sendPacketType'>发了一个" + packetType + "红包</p>";
    if (type == 0) {
        html += "<p class='sendPacketTitle'>" + title + "</p>";
    } else if (type == 1) {
        html += "<p class='sendPacketTitle'>口令：" + title + "</p>";
    }
    if (type == 1) {
        html += "<p class='sendPacketCommandInput'><input class='commandInput' placeholder='请输入口令'></p>";
    }

    //onclick='robRedPackets("+sendRedUid+","+liveuid+",\'"+sendtime+"\',"+type+",\'"+title+"\')'

    html += "<p class='robRedPacketBtn'><input type='button' value='抢红包' onclick='robRedPackets(" + sendRedUid + "," + liveuid + ",\"" + sendtime + "\"," + type + ",\"" + user_nicename + "\",\"" + avatar + "\",\"" + title + "\")'  /></p>";
    html += "</div>";
    $("#redPecketsArea").after(html);
}

/*抢红包界面关闭*/
function closeOpenRedPacket() {
    $(".openRedPacket").remove();
}

/*抢红包*/
function robRedPackets(sendRedUid, liveuid, sendtime, type, user_nicename, avatar, title) {
    var site_url = _DATA.getConfigPub.site_url;
    if (type == 1) {//口令红包
        var command = $(".commandInput").val();
        if (command == "") {
            layer.msg("请输入口令");
            return;
        }
        if (command !== title) {
            layer.msg("口令不一致");
            return;
        }
    } else {
        command = title;
    }
    $.ajax({
        url: site_url + 'index.php?service=Live.robRedPackets',
        type: 'POST',
        dataType: 'json',
        data: {
            uid: _DATA.user.id,
            token: _DATA.user.token,
            sendRedUid: sendRedUid,
            liveuid: liveuid,
            sendtime: sendtime,
            type: type,
            title: command
        },
        success: function (data) {
            var res = data.data;
            if (res.code != 0) {

                layer.msg(res.msg);
                $(".openRedPacket").remove();
                return;
            } else {
                $(".openRedPacket").html('');
                var html = "";
                var packetType = "";
                html += "<span class='packetCloseBtn' onclick='closeOpenRedPacket()'>×</span>";
                html += "<p class='sendPacketUserImg'><img src='" + avatar + "' /></p>";
                html += "<p class='sendPacketUserName'>" + user_nicename + "</p>";
                if (type == 0) {
                    packetType = "普通";
                } else if (type == 1) {
                    packetType = "口令";
                }
                html += "<p class='sendPacketType'>" + packetType + "红包</p>";
                html += "<p class='packetWinCoin'><span>" + res.info[0].liveCoin + "</span>直播券</p>";
                html += "<p class='packetWinCoinMsg'>已存入账户余额</p>";
                html += "<p class='packetWinCoinBtn'><input type='button' value='确定' onclick='closeOpenRedPacket()' /></p>";
                $(".openRedPacket").html(html);
                if (type == 1) {//口令红包发送socket
                    /*通过ajax判断用户是否是超管*/
                    $.ajax({
                        url: './index.php?g=home&m=Spend&a=isShutUp',
                        type: 'POST',
                        dataType: 'json',
                        data: {showid: liveuid},
                        success: function (data) {
                            if (data['admin'] == 60) {
                                var msg = '{"msg":[{"_method_":"SystemNot","action":"13","ct":"' + title + '","msgtype":"4","uname":"' + _DATA.user.user_nicename + '","toname":"' + _DATA.anchor.user_nicename + '","touid":' + _DATA.anchor.id + ',"uid":' + _DATA.user.id + ',"level":' + _DATA.user.level + '}],"retcode":"000000","retmsg":"ok"}';
                            } else {
                                var msg = '{"msg":[{"_method_":"SendMsg","action":0,"ct":"' + title + '","msgtype":"2","tougood":"","touid":"","touname":"","ugood":"' + _DATA.user.id + '","uid":"' + _DATA.user.id + '","uname":"' + _DATA.user.user_nicename + '","level":"' + _DATA.user.level + '"}],"retcode":"000000","retmsg":"OK"}';
                            }

                            Socket.emitData('broadcast', msg);
                        },
                        error: function (data) {
                            return;
                        }
                    });
                }
            }
        }, error: function (data) {
            layer.msg("抢红包失败");
        }
    });
}
