var multi = {
    init: function() {
        var room = new Room();
        if(!_DATA.user) {
            //防止360在ie模式下报错
            _DATA.user = {
                "id":"temp_5a852ed48d009", "avatar":"", "user_nicename":"游客","logged":0,"issuper":0,"iswhite":0,"token":"4785e276bee09a7051a46c1ec79f7f96","sex":0,"signature":"","consumption":0,"votestotal":0,"province":"","city":"","level":0};
        }
        rtc.init(
            {
                // channel: '10000',
                channel: room.id,
                // channel: 10000,
            },
            {
                uid: _DATA.user.id,
                uname: _DATA.user.user_nicename,
                // role: "user",
                isAmiAnchor: _DATA.multiLive.ami_anchor,
                isAmiDj: _DATA.multiLive.ami_dj,
            }
        );
        rtc.join();
    },
    /**
     * 获取用户的名字
     */
    getUserName: function(uid, type) {
        var anchors =  _DATA.multiLive.anchor;
        var djs =  _DATA.multiLive.dj;
        var user_nicename = "";
        if(type == 1 && anchors) {
            $.each(anchors, function(i, v) {
                if(v.uid == uid) {
                    user_nicename = v.user_nicename;
                }
            });
            if(rtc.liveStreams[uid]) {
                rtc.liveStreams[uid].user_nicename =  user_nicename;
                multi.setVideoUserName();
            }
        } else if(type == 2 && djs) {
            $.each(djs, function(i, v) {
                if(v.uid == uid) {
                    user_nicename = v.user_nicename;
                }
            });
            $("#multiDjName").text(user_nicename + '麦手');
        } else {
            $.ajax({
                type: "get",
                url: "./index.php?m=user&a=getUserInfo",
                data: {
                    user_id: uid
                },
                success: function (data) {
                    if(data.code == 200) {
                        // callback && callback(data.data.user_nicename)
                        user_nicename = data.data.user_nicename;
                        if(type == 1) {
                            if(rtc.liveStreams[uid]) {
                                rtc.liveStreams[uid].user_nicename =  user_nicename;
                                multi.setVideoUserName();
                            }
                        } else if(type == 2) {
                            $("#multiDjName").text(user_nicename + '麦手');
                        }

                    }
                }
            });
        }

    },
    /**
     * 將用戶名放到对应的视频区域或音频区域
     */
    setVideoUserName: function() {
        $('.vidcont').each(function(){
            var uid = $(this).attr('uid');
            if( !$(this).is(':empty') && rtc.liveStreams[uid] && rtc.liveStreams[uid].user_nicename){
                var user_nicename = rtc.liveStreams[uid].user_nicename;
                $(this).attr({uname: user_nicename});
                $(this).siblings('.player-praises').find('.praises').attr('id',uid);
                $(this).siblings('.vidmask').find('.anchor-name').text(user_nicename);
                $(this).siblings('.vidtop').find('.anchor-name').text(user_nicename);
            }
        });
    },
    /**
     * 对视频重新排列
     */
    rangeVideo: function() {
        if($('.vidcont:empty').length == 0) return;
        var emptyIndex = $('.vidcont').index($('.vidcont:empty'));
        if($('.vidcont:empty').length == 1 && emptyIndex ==0 &&  _DATA.multiLive.ami_anchor) return; //如果是主播，则大视频区域要留给主播

        if( emptyIndex == 0 && _DATA.multiLive.ami_anchor) { //如果是主播，则大视频区域要留给主播
            emptyIndex = $('.vidcont').index($('.vidcont:empty').eq(1));
        }

        for (var i = emptyIndex; i < 4; i++) {
            if(!$('.vidcont').eq(i).is(":empty")) {
                var uid = $('.vidcont').eq(i).attr('uid');
                var stream = rtc.liveStreams[uid];
                stream.stop();
                $('#agora_remote' + uid).remove();

                var start = 0;
                if( emptyIndex == 0 && _DATA.multiLive.ami_anchor) { //如果是主播，则大视频区域要留给主播
                    start = 1;
                }
                var vicont = $(".vidcont:empty").eq(start);


                var streamType = 1;
                if(vicont.attr('id') == 'agora_local') { //如果是大窗口，就播放大流；否则就是小窗口，播放小流
                    streamType = 0;
                }
                rtc.client.setRemoteVideoStreamType(stream, streamType);

                vicont.attr({uid: uid}).append('<div id="agora_remote'+uid+'" style="float:left; width:100%;height:100%;display:inline-block;"></div>');
                stream.play('agora_remote' + uid);

            }
        }
        multi.setVideoUserName();

    },
    /**
     * 系统消息通知
     * @param txt
     */
    systemNotify: function(txt, notice){
        var msg = {
            "msg": [{
                "_method_": "SystemNot",
                'notice': notice || "", //'anchorOnline',  anchorOffline
                "action": "13",
                "ct":  txt,
                "msgtype": "4",
                "uname":  _DATA.user.user_nicename,
                "uid": _DATA.user.id,
                ispc: 1
            }], "retcode": "000000", "retmsg": "ok"
        };
        Socket.emitData('broadcast', JSON.stringify(msg));
    },


    //麦手让主播静音
    disableAudio:function(data) {
        if(data.liveUid == _DATA.user.id) {
            if(data.action == 1) {
                layer.alert("你已经被" + data.uname + "麦手" + data.ct,
                    {
                        skin: 'layui-layer-molv' //样式类名
                        ,closeBtn: 0,
                        shift: 5,
                        icon: 2,
                    }, function(){
                        layer.closeAll();
                    });
                rtc.disableAudio(rtc.liveStreams[data.liveUid]);
                multi.systemNotify("主播"+_DATA.user.user_nicename+"已经被" + data.uname + "麦手" + data.ct);
            } else if(data.action == 0) {
                // layer.alert("你已经被" + data.uname + "麦手" + data.ct,
                //     {
                //         skin: 'layui-layer-molv' //样式类名
                //         , closeBtn: 0,
                //         shift: 5,
                //         icon: 1
                //     }, function () {
                //         layer.closeAll();
                //     });
                layer.msg("你已经被" + data.uname + "麦手" + data.ct, {icon: 1});
                rtc.enableAudio(rtc.liveStreams[data.liveUid]);
                multi.systemNotify("主播"+_DATA.user.user_nicename+"已经被" + data.uname + "麦手" + data.ct);
            }
        }
    },

    /**
     * 关闭直播
     */
    closeLive: function() {
        if (!_DATA.multiLive.ami_anchor) {
            return
        }
        // rtc.leave();
        rtc.unpublish();
        rtc.cancelPreview();

        $(".multi-openlive").show();
        $(".multi-closelive").hide();
        multi.setMultiShowManagerStatus('anchor', 0);
    },

    /**
     * 关闭喊麦
     */
    closeMac: function(){
        if (!_DATA.multiLive.ami_dj) {
            return
        }
        rtc.unpublish();
        rtc.cancelPreview();

        $(".multi-startmac").show();
        $(".multi-closemac").hide();
        $('#audioopen').hide();
        multi.setMultiShowManagerStatus('dj', 0);
    },


    /**
     * 超管关闭直播
     */
    multiStopLive: function(data) {
        if(data.touid == _DATA.user.id) {
            multi.closeLive();
            multi.systemNotify('（超管）'+ data.uname + '关闭了' + data.touname + '的直播', 'anchorOffline');
            layer.alert("你已经被" + data.uname + "关闭了直播",
                {
                    skin: 'layui-layer-molv' //样式类名
                    ,closeBtn: 0,
                    shift: 5,
                    icon: 2,
                }, function(){
                    layer.closeAll();
                });
        }
    },

    /**
     * 超管关闭喊麦
     */
    multiStopMac: function(data) {
        if(data.touid == _DATA.user.id) {
            multi.closeMac();
            multi.systemNotify('（超管）'+ data.uname + '关闭了' + data.touname + '的喊麦');
            layer.alert("你已经被" + data.uname + "关闭了喊麦",
                {
                    skin: 'layui-layer-molv' //样式类名
                    ,closeBtn: 0,
                    shift: 5,
                    icon: 2,
                }, function(){
                    layer.closeAll();
                });
        }
    },

    /**
     * 后端记录直播状态（开始/关闭直播,开始/关闭喊麦）
     * status 设置的状态：0-关闭，1-开启
     */
    setMultiShowManagerStatus: function(vest_type, status) {
        var room = new Room();
        $.ajax({
            url: "/index.php?m=show&a=setMultiShowManagerStatus",
            data: {room_id: room.id, vest_type: vest_type, status: status},
            type: 'POST',
            dataType: 'json',
            success: function (data) {
            },
            error: function (data) {
            }
        });

    }

};

// multi.rangeVideo();



$(function () {

    /**
     * 切换视频
     */
    $("#videoSource").change(function () {
        rtc.cancelPreview();
        rtc.preview();
    });


    /**
     * 切换麦克风
     */
    $("#audioSource").change(function () {
        rtc.cancelPreview();
        rtc.preview();
    });

    /**
     * 打开直播设置
     */
    $(".multi-openlive").click(function () {
        if (!_DATA.multiLive.ami_anchor) {
            return
        }
        if( Object.keys(rtc.liveStreams).length>=4) {
            layer.msg("主播数已满");
            return;
        }
        $("#liveSetting, #liveSettingDialogBg").show();
        rtc.preview();
    });


    /**
     * 开始直播
     */
    $('.multi-start-live').click(function () {
        if (!_DATA.multiLive.ami_anchor) {
            return
        }
        if(!rtc.isMediaAccessAllowed) {
            alert("请启用摄像头或麦克风设备");
            return;
        }
        rtc.publish(function() {
            $(".multi-openlive").hide();
            $(".multi-closelive").show();
            multi.systemNotify(_DATA.user.user_nicename + '主播上线啦', 'anchorOnline');
            multi.setMultiShowManagerStatus('anchor', 1);
        });
        $("#liveSetting, #liveSettingDialogBg").hide();
    });





    /**
     * 关闭直播
     */
    $('.multi-closelive').click(function () {
        multi.closeLive();
        multi.systemNotify(_DATA.user.user_nicename + '主播下线了', 'anchorOffline');
    });



    /**
     * 打开喊麦设置
     */
    $(".multi-startmac").click(function () {
        if (!_DATA.multiLive.ami_dj) {
            return
        }
        if( Object.keys(rtc.macStreams).length>=1) {
            layer.msg("已有麦手上麦");
            return;
        }
        $("#wheatSetting, #wheatSettingDialogBg").show();
        rtc.preview();
    });


    /**
     * 开始喊麦
     */
    $('#wheatSetting .multi-start-mac').click(function () {
        if (!_DATA.multiLive.ami_dj) {
            return;
        }
        if(!rtc.isMediaAccessAllowed) {
            alert("请启用摄像头或麦克风设备");
            return;
        }
        rtc.publish2(function() {
            $(".multi-startmac").hide(); //隐藏“开启喊麦”按钮
            $(".multi-closemac").show(); //显示“关闭喊麦”按钮
            $("#multiDjName").text(_DATA.user.user_nicename + '麦手');
            $(".audioopen").attr({uid: rtc.localUid}).show();
            multi.systemNotify(_DATA.user.user_nicename + '麦手上线啦');
            multi.setMultiShowManagerStatus('dj', 1);
        });
        $("#wheatSetting, #wheatSettingDialogBg").hide();
    });


    /**
     * 关闭喊麦
     */
    $('.multi-closemac').click(function () {
        multi.closeMac();
        multi.systemNotify(_DATA.user.user_nicename + '麦手下线了');
        $('.controldj i').trigger("click");
    });


    /**
     * 关闭直播或喊麦设置
     */
    $(".multi-setting-close").click(function () {
        $("#liveSetting, #liveSettingDialogBg").hide();
        $("#wheatSetting, #wheatSettingDialogBg").hide();
        rtc.cancelPreview();
    });

    /**
     * 播放、暂停视频 （通用设置）
     */
    $(".multi-colsevideo").click(function () {
        if ($(this).val() == '暂停视频') {
            rtc.disableVideo(rtc.localStream);
            $(this).val('播放视频');
        } else {
            rtc.enableVideo(rtc.localStream);
            $(this).val('暂停视频');
        }
    });

    /**
     * 播放、暂停视频 （视频上的按钮）
     */
    $(".video-control").click(function () {
        var vidcont = $(this).parent().siblings('.vidcont');
        var uid = parseFloat(vidcont.attr('uid'));
        var stream = rtc.liveStreams[uid];
        if ($(this).hasClass('bofang')) {
            rtc.enableVideo(stream);
            rtc.enableAudio(stream);
            $(this).removeClass('bofang').addClass('zanting');
        } else {
            rtc.disableVideo(stream);
            rtc.disableAudio(stream);
            $(this).removeClass('zanting').addClass('bofang');
        }
    });


    // $('.minvideo').click(function() {
    //     console.log($('.minvideo').index( this ))
    //     var uid = $(this).attr('uid');
    //     var stream = rtc.liveStreams[uid];
    //     rtc.disableVideo(stream);
    // });


    /**
     * 静音操作
     */
    $('.controldj .item input').on('click', function (event) {
        if (!_DATA.multiLive.ami_dj) { return }
        var action, ct;
        var index = parseInt($(this).attr('data-index'));
        var liveUid  = $(".vidcont").eq(index).attr('uid');

        if ($(this).is(":checked")) { //静音
            $(this).addClass('act');
            action = 1;
            ct = '静音';
            rtc.liveStreams[liveUid].isAudioDisabled = true;
        } else { //取消静音
            $(this).removeClass('act');
            action = 0;
            ct = '取消静音';
            rtc.liveStreams[liveUid].isAudioDisabled = false;
        }


        var msg = {
            "msg": [{
                "_method_": "DisableAudio",
                "action": action,
                "ct": ct,
                // "msgtype": "4",
                "uid":  _DATA.user.id,
                "uname": _DATA.user.user_nicename,
                "liveUid": liveUid
            }],
            "retcode": "000000",
            "retmsg": "ok"
        };
        // console.log(msg);
        Socket.emitData('broadcast', JSON.stringify(msg));
    });

    /**
     * 打开麦手控制弹框
     */
    $('.controldj i, .maccontrol').on('click', function (event) {

        if (!_DATA.multiLive.ami_dj) { return }
        if($.isEmptyObject(rtc.macStreams) ||  _DATA.user.id != Object.keys(rtc.macStreams)[0]) return;
        //麦手上麦了才能控制主播是否静音
        $.each($(".vidcont"), function(i) {
            var uid = $(this).attr('uid');
            var input = $('.controldj .item').eq(i).find('input');
            if(uid && !$(this).is(":empty")) {
                if(rtc.liveStreams[uid].isAudioDisabled) {
                    input.addClass('act').attr('disabled', false);;
                } else {
                    input.removeClass('act').attr('disabled', false);;
                }
            } else {
                input.removeClass('act').attr('disabled', true);
            }
        });


        if (!$('.controldj').is('.act')) {
            $('.controldj').addClass('act');
        } else {
            $('.controldj').removeClass('act');
        }

    });




    //显示浮层
    $('.vidcont').on('mouseenter', function () {

        if (!$(this).is(':empty')) {
            $(this).siblings('.vidmask').fadeIn(100);
            $(this).siblings('.player-praises').find('.dzang').css('display','block');
            $(this).siblings('.vidtop').fadeIn(100);
        }

    });

    //隐藏浮层
    $('.video-rec-con .minvideo').on('mouseleave', function () {
       
        $(this).find('.vidmask').fadeOut(100);
        $(this).find('.player-praises .dzang').css('display','none');
    });


    $('.channel-player-multi').on('mouseleave', function () {
        $('.channel-player-multi .vidmask, .channel-player-multi .vidtop').fadeOut(100);

    });


    //**大小视频切换


    var switchTimer = null; //控制1.5秒只能点一次
    $('.video-rec-con .vidmask .qihuan').on('click', function () {
        if(switchTimer) return;
        switchTimer = true;

        var minUid = $(this).parent().siblings('.vidcont').attr('uid');
        var maxUid = $('#channelPlayerMulti .vidcont').attr('uid');
        rtc.liveStreams[minUid].stop();
        $('#agora_remote' + minUid).remove();
        rtc.liveStreams[maxUid].stop();
        $('#agora_remote' + maxUid).remove();


        setTimeout(function() {
            $(".vidcont:empty").eq(0).attr({uid: minUid}).append('<div id="agora_remote'+minUid+'" style="float:left; width:100%;height:100%;display:inline-block;"></div>');
            rtc.client.setRemoteVideoStreamType( rtc.liveStreams[minUid], 0); //大流
            rtc.liveStreams[minUid].play('agora_remote' + minUid);

            setTimeout(function( ) { //解决视频切换变黑的问题
                $(".vidcont:empty").eq(0).attr({uid: maxUid}).append('<div id="agora_remote'+maxUid+'" style="float:left; width:100%;height:100%;display:inline-block;"></div>');
                rtc.client.setRemoteVideoStreamType(rtc.liveStreams[maxUid], 1); //小流
                rtc.liveStreams[maxUid].play('agora_remote' + maxUid);
                multi.setVideoUserName();
            }, 200)


        }, 0)


        setTimeout(function() {
            switchTimer = null;
        }, 1500)
    });


    // var isFullScreen = false;
    /**
     * 全屏切换(变大)
     */
    $('.quanping').click(function() {
        // isFullScreen = !isFullScreen;
        var uid = $("#agora_local").attr("uid");
        var stream = rtc.liveStreams[uid];

        stream.stop();

        // if(isFullScreen) {
            $('#agora_local').html("");
            stream.play('multiFullScreen');
            $("#multiFullScreen").attr({uid: uid}).fadeIn();
            $("#multiFullScreenClose").show();
        // } else {
        //     $("#multiFullScreen").fadeOut().html("");
        //     stream.play('agora_local');
        //     $("#agora_local").attr({uid: uid});
        // }
    });


    function switchSmall(){
        var uid = $("#multiFullScreen").attr("uid");
        var stream = rtc.liveStreams[uid];
        if(!$("#multiFullScreen").is(":empty")) {
            stream.stop();
            $("#multiFullScreen").hide().html("");
            stream.play('agora_local');
            $("#agora_local").attr({uid: uid});
        }
        $("#multiFullScreenClose").hide();
    }

    /**
     * 全屏切换(变小)
     */
    $(document).keydown(function (e) {
        if (e.which === 27) {
            switchSmall();
        }
    });

    $("#multiFullScreenClose").click(function() {
        switchSmall();
    });




    //***主播锁房间修改密码

  $('.disabledchannel').on('click',function(){
        
        var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"mimadoor","action":"0","ami_anchor":' + _DATA.anchor.ami_anchor + ',"uname":"' + _DATA.user.user_nicename + '","uid":"' + _DATA.user.id + '","ct":"'+ _DATA.multiLive.live_name +'房间已更换密码"}]}';
        
        liveType.liveMima(msg);
    });


    /**
     * 页面卸载时关闭视频流
     */
    window.onbeforeunload = function () {
        var room = new Room();
        if (room.userIsAnchor && room.userIsLive) {
            multi.setMultiShowManagerStatus('anchor', 0);
            rtc.leave();
        }

        if(room.userIsDj && room.userIsLive) {
            multi.setMultiShowManagerStatus('dj', 0);
            rtc.leave();
        }
    }


    /**送多人礼物框**/

    $('.gift-group, #channelPlayerMulti').on('mouseenter',function(){

        $('.giftGroupSelect select').empty();
        $('.vidcont').each(function(index){
            var uname = $(this).attr('uname');
            if(!uname) {
                uname = '主播' + (index + 1);
            }

            if( !$(this).is(':empty') ){
                $('.giftGroupSelect select').append('<option value='+$(this).attr('uid')+'>'+uname+'</option>');
            }
        });
    });

    /**送多人礼物框**/

    /**在线用户弹出框**/

    $(document).on('click', '#popup_info', function(event){
        event.stopPropagation();
        
        $('.MR-online .popup').remove();
    });

    /**在线用户弹出框**/


});

// multi.init();