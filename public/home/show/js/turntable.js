$(function(){
	var i=1;
	/*加载让跑马灯背景闪烁*/
	setInterval(function(){
		
		//console.log("当前数字"+i);

		if(i%2==0){
			
			$("#turntableIngs").css('background-image','url(/public/home/show/images/raceLamp0.png)');
			//console.log("1_"+i);
		}else{
			

			$("#turntableIngs").css('background-image','url(/public/home/show/images/raceLamp1.png)');
			//console.log("2_"+i);
		}

		i++;

	},500);


	var screenW=window.screen.width;
	var turntableW=$("#turntableArea").width();
	var leftW=(screenW-turntableW)/2;

	var screenH=window.screen.height;
	var turntableH=$("#turntableArea").height();
	var topH=(screenH-turntableH)/2;
	$("#turntableArea").css('left',leftW+30).css('top',topH-60);

	$("#turntable").on('click',function(){
		if(isStartTurnTable==1){
			return;
		}

		$("#turntableArea").show();
	});


	$("#turntableArea item").on('click',function(){
		$("#turntableArea").hide();
	});

	//点击确定时判断用户是否存在
	$(".turntableSure").click(function() {



		var userID=$(".turntableOpenUser").val();

		var stream=_DATA.anchor.stream;
		if(userID==""){
			layer.msg('请填写开启转盘人的ID');
			return;
		}

		if(userID==_DATA.user.id){//主播自己

			layer.msg('转盘开启人不能是自己');
			return;
		}

		$.ajax({
			url: '/index.php?g=home&m=show&a=checkUser',
			type: 'POST',
			dataType: 'json',
			data: {uid:userID,stream:stream},
			success:function(data){
				if(data.code!=0){
					layer.msg(data.msg);
					return;
				}else{
					var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"Carouse","action":"0","msgtype":"0","randNum":"'+data.info.randNum+'","nums":"'+data.info.nums+'","startGameUid":"'+userID+'","imgUrl":"'+data.info.imgUrl+'"}]}';
					Socket.emitData('broadcast',msg);

					$("#turntableArea").hide();

					setTimeout(function(){

						if(isStartTurnTable==0){
							var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"Carouse","action":"2","msgtype":"1","startGameUid":"'+userID+'","ct":""}]}';
							Socket.emitData('broadcast',msg);
							$("#turntableIngs").hide();
							$("#turntableArea").show();
						}


					},10000);
	
						
				}
			},
			error:function(data){
				layer.msg("转盘游戏开启失败");
			}
		});
		
		
	});



	$(".turntableCancel").on('click',function(){
		$("#turntableArea").hide();
		var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"Carouse","action":"2","msgtype":"0"}]}';
		Socket.emitData('broadcast',msg);
	});




});


function startTurnTable(){
	
	$(".turntableStartBtn").remove();
	var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"Carouse","action":"1","msgtype":"0"}]}';
	Socket.emitData('broadcast',msg);
	
	
	
}


function CancelTurnTable(){
	
	var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"Carouse","action":"2","msgtype":"0"}]}';
	Socket.emitData('broadcast',msg);
	
	
	
}