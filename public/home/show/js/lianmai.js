$(function(){
	$(".lianmaiIcon").on('click',function(){

		$.ajax({
			url: '/index.php?g=Home&m=Show&a=checkIsLive',
			type: 'POST',
			dataType: 'json',
			data: {liveuid:_DATA.live.uid,uid:_DATA.user.id},
			success:function(data){
				if(data.code!=0){
					layer.msg(data.msg);
					return;
				}else{
					var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"0","msgtype":"0","sendUid":"'+_DATA.user.id+'","liveUid":"'+_DATA.live.uid+'","user_nicename":"'+data.info+'","recordID":"'+data.recordID+'"}]}';
					Socket.emitData('broadcast',msg);
				}

			},error:function(data){
				layer.msg("申请连麦失败啦");
			}
		});
	});

	$(".lianmaiCancelIcon").on('click',function(){

		//主播
		if(_DATA.anchor.id==_DATA.user.id){//主播

			$("#lianmaiPlayer").html('');

		}else{  //连麦用户点击

			$("#playerzmblbkjP").remove();
		}

		

		$.ajax({
			url: '/index.php?g=Home&m=Show&a=stopLianmai',
			type: 'POST',
			dataType: 'json',
			data: {liveid:_DATA.anchor.id},
			success:function(data){
				//发送socket广播到房间
				var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"5","msgtype":"0","sendUid":"'+_DATA.user.id+'"}]}';
				//console.log(msg);
				Socket.emitData('broadcast',msg);
			},
			error:function(data){

			}
		});
		

	});



});



function AgreeLianmai(sendUid,recordID){
	if(sendUid==""||isNaN(sendUid)||recordID==""||isNaN(recordID)){
		layer.msg('同意连麦失败啦');
		return;
	}else{
		$.ajax({
			url: '/index.php?g=Home&m=show&a=changeStatus',
			type: 'POST',
			dataType: 'json',
			data: {sendUid:sendUid,recordID:recordID,status:1},
			success:function(data){
				if(data.code==0){
					var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"2","msgtype":"0","sendUid":"'+sendUid+'","stream":"'+data.stream+'"}]}';
					Socket.emitData('broadcast',msg);
				}else{
					layer.msg("同意连麦失败啦");
					return;
				}
			},
			error:function(data){
				layer.msg('同意连麦失败啦');
				return;
			}
		});
	}
}



function RefuseLianmai(sendUid,recordID){

	if(sendUid==""||isNaN(sendUid)||recordID==""||isNaN(recordID)){
		layer.msg('拒绝连麦失败啦');
		return;
	}else{
		
		$.ajax({
			url: '/index.php?g=Home&m=show&a=changeStatus',
			type: 'POST',
			dataType: 'json',
			data: {recordID:recordID,status:2},
			success:function(data){
				if(data.code==0){
					var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"1","msgtype":"0","sendUid":"'+sendUid+'"}]}';
					Socket.emitData('broadcast',msg);
				}else{
					layer.msg("拒绝连麦失败啦");
					return;
				}
			},
			error:function(data){
				layer.msg('拒绝连麦失败啦');
				return;
			}
		});
		
		



	}


}

//十秒后主播未同意或拒绝用户的连麦

function TenMinutesConnectVideoCheck (userid,roomid){

		if(isConnectVideo==0){

			//console.log(userid+'---'+roomid);
		 	$.ajax({
		 		url: '/index.php?g=Home&m=Show&a=checkLianmaiStatus',
		 		type: 'POST',
		 		dataType: 'json',
		 		data: {userid:userid,roomid:roomid},
		 		success:function(data){
		 			if(data.code==0){
		 				layer.msg("主播未同意或拒绝连麦");
		 				var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"3","msgtype":"0","sendUid":"'+userid+'"}]}';
						Socket.emitData('broadcast',msg);
		 				
		 				return;
		 			}/*else{
		 				
		 				layer.msg(data.msg);
		 				return;
		 				
		 			}*/
		 		},
		 		error:function(data){
		 		}
		 	});


		}
		
		
				 	
				 		
	
}



function sendConnectMsg(stream,sendUid){

	$.ajax({
		url: '/index.php?m=show&a=changeLianmaiStream',
		type: 'POST',
		dataType: 'json',
		data: {liveid:_DATA.live.uid,stream:stream},
		success:function(data){
			if(data.code==0){
				var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"lianmai","action":"4","msgtype":"0","sendUid":"'+sendUid+'","stream":"'+stream+'"}]}';
				Socket.emitData('broadcast',msg);
			}else{
				layer.msg(data.msg);
			}
		},
		error:function(data){

		}
	});
	
	

	
}