$(function(){
	/*抢板凳弹窗定位*/

	var screenW=window.screen.width;
	var redPacketsW=$("#grabBenchArea").width();
	var leftW=(screenW-redPacketsW)/2;

	var screenH=window.screen.height;
	var redPacketsH=$("#grabBenchArea").height();
	var topH=(screenH-redPacketsH)/2;
	$("#grabBenchArea").css('left',leftW+30).css('top',topH-60);

	$("#grabBench").on('click',function(){

		if(isGrabBench==1){
			return;
		}

		$("#grabBenchArea").show();
	});

	/*关闭抢板凳开始界面*/
	$("#grabBenchArea item").on('click', function() {
		$("#grabBenchArea").hide();

	});


	/*活动开始按钮*/
	$("#grabBenchStartBtn").on('click', function() {
		

		var effectiveTime=$("#grabBenchTime").val();
		var winNums=$("#grabBenchCodes").val();

		if(isNaN(effectiveTime)){
			layer.msg("请填写游戏时间");
			return;
		}

		if(effectiveTime<30||effectiveTime>3600){
			layer.msg("游戏时间在30秒至3600秒之间");
			return;
		}

		if(winNums==""){
			layer.msg("请填写中奖号码");
			return;
		}

		var NotIsNum=0;

		if(winNums.indexOf(',')>0){
			var strs= new Array(); //定义数组
			strs=winNums.split(",");
			for(i=0;i<strs.length;i++){
				if(isNaN(strs[i])){
					NotIsNum=1;
					break;

				}
			}

			if(NotIsNum==1){
				layer.msg("中奖号码必须是数字");
				return;
			}
		}else{
			if(isNaN(winNums)){
				layer.msg("中奖号码必须是数字");
				return;
			}
		}

		
		$("#grabWinNums").text('');//将用户选择号码显示区域置空
		$('.grabNums').text('');//将用户抢到号码置空
		$("#grabBenchRes").hide();

		$.ajax({
			url: '/api/public/index.php?service=Live.createGrabBench',
			type: 'POST',
			dataType: 'json',
			data: {uid:_DATA.user.id,token:_DATA.user.token,effectiveTime:effectiveTime,winNums:winNums},
			success:function(data){

				var res=data.data;
				if(res.code!=0){
					layer.msg(res.msg);
					return;
				}

				$("#grabBenchArea").hide();
				$("#grabBenchTime").val('');
				$("#grabBenchCodes").val('');



				//拼接socket
				var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"GrabBench","action":"0","msgtype":"0","effectiveTime":"'+effectiveTime+'","grabbenchID":"'+res.info.grabbenchID+'","hits_space":"'+res.info.hits_space+'","ct":""}]}';
				Socket.emitData('broadcast',msg);

			},
			error:function(data){
				layer.msg("活动开始失败");
				return;
			}
		});

	});

	/*点击抢板凳*/
	$("#grabBenchIngs").on('click',function(){

		if(isHitsInterval==1){
			layer.msg("请稍候再抢");
			return;
		}

		isHitsInterval=1;//将倒计时可点击改为1【在倒计时，不可点击】



		$.ajax({
			url: '/api/public/index.php?service=Live.grabBench',
			type: 'POST',
			dataType: 'json',
			async:false,
			data: {uid:_DATA.user.id,token:_DATA.user.token,liveuid:_DATA.anchor.id,grabbenchID:grabBenchID},
			
			success:function(data){
				
				var res=data.data;	
				if(res.code!=0){
					isHitsInterval=0;
					layer.msg(res.msg);
					return;
				}else{

					//执行倒计时start
					var hitsSpace=grabBenchHits_Space;//抢板凳点击倒计时

					hitsInterval=setInterval(function(){
						
						if(hitsSpace==0){
							window.clearInterval(hitsInterval);
							isHitsInterval=0;
							$(".grabBenchClickTime span").text('0');
							$(".grabBenchClickTime").hide();

							return;
						}

						$(".grabBenchClickTime span").text(hitsSpace);
						$(".grabBenchClickTime").show();
						//isHitsInterval=1;【作废】

						hitsSpace--;

					},1000);

					//执行倒计时end
					
					
					layer.msg("抢到的号码："+res.info[0].num);
					grabBenchNumsArr.push(res.info[0].num);
					//console.log(grabBenchNumsArr);
				}
			},
			error:function(data){
				layer.msg("抢板凳失败啦，稍候再试哟");
			}
		});



		



		


	});




	/*抢板凳结果展示*/

	var grabBenchResW=$("#grabBenchRes").width();
	var leftW=(screenW-grabBenchResW)/2;

	var grabBenchResH=$("#grabBenchRes").height();
	var topH=(screenH-grabBenchResH)/2;
	$("#grabBenchRes").css('left',leftW+30).css('top',topH-60);



	$("#grabBenchRes item").on('click',function(){
		$("#grabBenchRes").hide();
	});

	$("#grabBenchIngs item").on('click',function(e){
		$("#grabBenchIngs").hide();
		e.stopPropagation();
	});


	$("#grabBenchClock").on('click',function(){
		//非主播点击显示
		if(_DATA.user.id!=_DATA.anchor.id){
			$("#grabBenchIngs").show();
		}
		
	});



	/*用户中途进房间*/
	//console.log(_DATA.grabBenchInfo.isEnd);
	if(_DATA.grabBenchInfo.isEnd==0){


		isGrabBench=1;
		grabBenchHits_Space=_DATA.grabBenchInfo.hits_space;
		grabBenchID=_DATA.grabBenchInfo.grabbenchID;

		//倒计时显示
		
		var effectiveTime=_DATA.grabBenchInfo.effectiveTime;//抢板凳活动倒计时

			var interval=window.setInterval(function(){

				if(effectiveTime==0){
					window.clearInterval(interval);//将倒计时清空
					if(hitsInterval){
						window.clearInterval(hitsInterval);//将用户点击倒计时清空【grabBench.js中】
					}
					
					$(".grabBenchClockNum").height(60);
					$(".grabBenchClockNum").css('line-height','65px');
					$(".grabBenchClockNum").text("wating");
					$(".grabBenchUnit").text('');
					$("#grabBenchIngs").hide();


					/*执行倒计时结束后的关闭*/

					if(_DATA.anchor.id==_DATA.user.id){//判断是否是主播方

						stopGrabBench(data.grabbenchID);//调用抢板凳结束函数【grabBench.js中】
						
					}
					return;
				}

				$(".grabBenchClockNum").text(effectiveTime);
				effectiveTime--;


			},1000);

			/*活动倒计时显示*/

			$("#grabBenchClock").show();

			$("#grabBenchIngs").show();
	}


});



function stopGrabBench(grabBenchID){
	$.ajax({
		url: '/api/public/index.php?service=Live.stopGrabBench',
		type: 'POST',
		dataType: 'json',
		data: {uid:_DATA.user.id,token:_DATA.user.token,grabbenchID:grabBenchID},
		success:function(data){
			var res=data.data;

			//console.log(res);
			grabBenchHits_Space=0;
			isGrabBench=0;
			grabBenchID=null;

			//console.log("执行结束");
			//console.log("grabBenchHits_Space"+grabBenchHits_Space);
			//console.log("isGrabBench"+isGrabBench);

			var msg='{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"GrabBench","action":"1","msgtype":"0","winArray":'+JSON.stringify(res.info)+',"ct":""}]}';
			//console.log(msg);
			Socket.emitData('broadcast',msg);
		},
		error:function(data){
			layer.msg("获取抢板凳活动结果失败");
			$("#grabBenchClock").hide();
		}
	});
	
	
}