$(function(){


	/*礼物PK弹窗定位*/

	var screenW=window.screen.width;
	var giftPkW=$("#giftPKArea").width();
	var leftW=(screenW-giftPkW)/2;

	var screenH=window.screen.height;
	var giftPkH=$("#giftPKArea").height();
	var topH=(screenH-giftPkH)/2;
	$("#giftPKArea").css('left',leftW+30).css('top',topH-30);

	$(".masterID").val(_DATA.anchor.id);

	/*显示giftPK界面*/
	$("#giftPK").on('click',function(){

		if(isGiftPK==1){
			return;
		}
		$("#giftPKArea").show();

		//红包界面隐藏
		$("#redPecketsArea").hide();
	});

	/*关闭giftPK界面*/
	$(".giftPkCancel").on('click',function(){closeGiftPK()});

	$(".giftPkTop item").on('click',function(){closeGiftPK()});

	var site_url=_DATA.getConfigPub.site_url;


	/*点击确定按钮提交*/



	$(".giftPkConfim").on('click',function(){

		var guestID=$(".guestID").val();
		var effectiveTime=$(".pkTime").val();
		var masterGiftID=$(".masterGiftID option:selected").val();
		var guestGiftID=$(".guestGiftID option:selected").val();



		if(effectiveTime<30){
			layer.msg('有效时间应该大于30秒');
			return;
		}

		if(guestID==""||isNaN(guestID)){
			layer.msg('请正确填写PK客队主播ID');
			return;
		}



		if(effectiveTime>3600){
			layer.msg('有效时间应该小于3600秒');
			return;
		}

		if(masterGiftID==0||guestGiftID==0){
			layer.msg("请选择PK礼物");
			return;
		}

		if(masterGiftID==guestGiftID){
			layer.msg("主队和客队礼物不能相同");
			return;
		}


		$.ajax({
			url: site_url+'/index.php?service=Live.giftPK',
			type: 'POST',
			dataType: 'json',
			data: {uid: _DATA.user.id,token:_DATA.user.token,guestID:guestID,effectiveTime:effectiveTime,masterGiftID:masterGiftID,guestGiftID:guestGiftID},
			success:function(data){
				var res=data.data;
				if(res.code!=0){
					layer.msg(res.msg);
					return;
				}else{//成功发起礼物PK

					closeGiftPK();

					//拼socket
					var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"giftPK","guestAvatar":"'+res.info[0].guestAvatar+'","action":"0","guestGiftID":"'+res.info[0].guestGiftID+'","guestGiftImg":"'+res.info[0].guestGiftImg+'","msgtype":"0","guestName":"'+res.info[0].guestName+'","masterAvatar":"'+res.info[0].masterAvatar+'","masterGiftID":"'+res.info[0].masterGiftID+'","masterGiftImg":"'+res.info[0].masterGiftImg+'","masterName":"'+res.info[0].masterName+'","pkID":"'+res.info[0].pkID+'","effectiveTime":"'+res.info[0].effectiveTime+'"}]}'; 
					Socket.emitData('broadcast',msg);


				}
			},
			error:function(data){
				layer.msg("创建礼物PK失败");
			}
		});
		
	});


	$("#giftPkClock").on('click',function(){
		$("#giftPKIngs").show();
	});


});

/*关闭giftPK界面*/
function closeGiftPK(){
	$("#giftPKArea").hide();
	$(".pkTime").val('');
	$(".guestID").val('');
	$(".masterGiftID option:first").prop("selected", 'selected');
	$(".guestGiftID option:first").prop("selected", 'selected');
}


/*关闭giftPK进度展示*/
$(".giftPKIngsCon item").on('click',function(){
	$("#giftPKIngs").hide();
});


/*礼物PK倒计时结束时请求结束接口函数*/

function stopGiftPK(pkID){


	var site_url=_DATA.getConfigPub.site_url;

	$.ajax({
		url: site_url+'/index.php?service=Live.stopPK',
		type: 'POST',
		dataType: 'json',
		data: {uid:_DATA.user.id,token:_DATA.user.token,pkID:pkID},
		success:function(data){
			var res=data.data;

			//console.log(data);

			var winType=res.info.winType;

			//winType  0:平局，1主队胜，2客队胜
			var msg = '{"retcode":"000000","retmsg":"ok","msg":[{"_method_":"giftPK","winType":"'+winType+'","action":"1","masterUserNicename":"'+res.info.masterUserNicename+'","masterFirstUserNicename":"'+res.info.masterFirstUserNicename+'","msgtype":"0","masterFirstUserCoin":"'+res.info.masterFirstUserCoin+'","guestUserNicename":"'+res.info.guestUserNicename+'","guestFirstUserNicename":"'+res.info.guestFirstUserNicename+'","guestFirstUserCoin":"'+res.info.guestFirstUserCoin+'"}]}'; 
			Socket.emitData('broadcast',msg);
			

		},
		error:function(data){
			layer.msg('获取礼物PK结果失败');
				$("#giftPkClock").hide();
			}
		});
					
}


/*PK结果展示初始化*/

$(function(){
	var screenW=window.screen.width;
	var giftPkW=$("#giftPkResult").width();
	var leftW=(screenW-giftPkW)/2;

	var screenH=window.screen.height;
	var giftPkH=$("#giftPkResult").height();
	var topH=(screenH-giftPkH)/2;
	$("#giftPkResult").css('left',leftW+30).css('top',topH-60);

	$("#giftPkResult item").on('click',function(){

		$("#giftPkResult").hide();
	});

});


/*用户中途进房间*/

$(function(){

	if(_DATA.giftPkInfo.isEnd==0){//有PK进行中

		giftPkMasterGiftID=_DATA.giftPkInfo.masterGiftID;
		giftPkGuestGiftID=_DATA.giftPkInfo.guestGiftID;
		isGiftPK=1;
		
		//倒计时显示
		$("#giftPkClock").show();

		var effectiveTime=_DATA.giftPkInfo.effectiveTime;

		var interval=window.setInterval(function(){

			if(effectiveTime==0){
				window.clearInterval(interval);
				$(".giftPkClockNum").height(60);
				$(".giftPkClockNum").css('line-height','65px');
				$(".giftPkClockNum").text("wating");
				$(".giftPkUnit").text('');
				$("#giftPKIngs").hide();
				
				return;
			}

			$(".giftPkClockNum").text(effectiveTime);
			effectiveTime--;


		},1000);


		$(".masterUserImg").attr('src',_DATA.giftPkInfo.masterAvatar);
		$(".masterName").text(_DATA.giftPkInfo.masterName);

		$(".guestUserImg").attr('src',_DATA.giftPkInfo.guestAvatar);
		$(".guestName").text(_DATA.giftPkInfo.guestName);

		$(".masterGiftImg img").attr('src',_DATA.giftPkInfo.masterGiftImg);
		$(".guestGiftImg img").attr('src',_DATA.giftPkInfo.guestGiftImg);

		$(".giftPkNumsL").text(_DATA.giftPkInfo.masterGiftTotalCoin);
		$(".giftPkNumsR").text(_DATA.giftPkInfo.guestGiftTotalCoin);

		$(".masterGiftPercent").css('width',(_DATA.giftPkInfo.masterGiftTotalCoin/(_DATA.giftPkInfo.masterGiftTotalCoin+_DATA.giftPkInfo.guestGiftTotalCoin)*100)+'%');
		$(".guestGiftPercent").css('width',(_DATA.giftPkInfo.guestGiftTotalCoin/(_DATA.giftPkInfo.masterGiftTotalCoin+_DATA.giftPkInfo.guestGiftTotalCoin)*100)+'%');

		$("#giftPKIngs").show();
		

	}

	
});





