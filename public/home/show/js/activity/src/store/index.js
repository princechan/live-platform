import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
import getters from './getters'

Vue.use(Vuex);

// 0-活动未发布,1-活动发布,2-活动准备(倒计时),3-活动进行中（先进来的用户）,4-活动结束,5-活动删除 6:排行版  7： 答题结束 8:活动准备（开始答题）  9:活动进行中（后进来的用户）
if(_DATA.activity.status == 3) {
  _DATA.activity.status= 9;
}

const state = {
  config: _DATA.config,
  getConfigPub: _DATA.getConfigPub,
  anchor: _DATA.anchor,
  live: _DATA.live,
  gift: _DATA.gift,
  user: _DATA.user,
  giftPkInfo: _DATA.giftPkInfo,
  grabBenchInfo: _DATA.grabBenchInfo,
  guardInfo: _DATA.guardInfo,
  vests: _DATA.vests,
  thumb: _DATA.thumb,
  obs: _DATA.obs,
  push: _DATA.push,
  enterChat: _DATA.enterChat,
  activity: _DATA.activity,
  room: new window.Room(),
  subjects: [],
  currentSubject: {},
  subjectTotal: 0,
  userActivityStatus: -1,  //字符串,-1:出局；1，可继续答题 0：未报名

  gameInfo: {
    "activity_id":"1", //活动id
    "title":"百家乐大赛",
    "intro":"边看美女直播，边玩百家乐，美女陪你赢钱",
    "image_path":"http://n.sinaimg.cn/translate/20170726/Zjd3-fyiiahz2863063.jpg"
  },
  gameStatus: 0, //游戏状态: status 0:未开始 1:开始下注 2：下注结束 3:开牌  4:底牌 5:公布比賽结果
  selectedChip: {}, // 选中的筹码
  gameBorderInfo: {
    isActive: false,
    type: 0, //0：日榜 1：本场
    title: '日榜'
  }, 
}


export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
})
