import {
  CHANGE_ACTIVITY_STATUS,
  ADD_SUBJECT,
  UPDATE_SUBJECT,
  SET_CURRENT_SUBJECT,
  SET_SUBJECT_TOTAL,
  INSERT_ANSWER_POEOPLE_NUM,
  SET_USER_ACTIVITY_STATUS,
  SET_GAME_STATUS,
  UPDATE_GAME_SELECTED_CHIP,
  UPDATE_GAME_BORDER_INFO,
  SET_GAME_INFO,
} from './mutation-types.js'

import {
  setStore,
  getStore
} from '../utils/mUtils'


export default {

  //修改用户名
  [CHANGE_ACTIVITY_STATUS](state, newActivity) {
    state.activity = { ...state.activity,
      ...newActivity
    }
  },

  //新增题目
  [ADD_SUBJECT](state, newSubject) {
    state.subjects.push(newSubject)
  },

  [SET_CURRENT_SUBJECT](state, newSubject) {
    state.currentSubject = newSubject;
  },

  [SET_SUBJECT_TOTAL](state, num) {
    state.subjectTotal = num;
  },

  [SET_USER_ACTIVITY_STATUS](state, newUserActivityState) {
    state.userActivityStatus = newUserActivityState;
  },
  

  // 修改题目
  [UPDATE_SUBJECT](state, newSubject) {
    let subjectsCopy = [...state.subjects];
    subjectsCopy.forEach((subject, index) => {
      if(subject.question == newSubject.question) {
        subjectsCopy[index] = newSubject;
      }
    })
    state.subjects = subjectsCopy;
  },

  
  [INSERT_ANSWER_POEOPLE_NUM](state, arr) {
    let currentSubjectCopy = {...state.currentSubject};
    currentSubjectCopy.content.answer.forEach((a, index) => {
      arr.forEach((v) => {
        if(v.answer == a.text) {
          a.num = v.num;
        }
      })
    })
    state.currentSubject = currentSubjectCopy;
  },

  /**
   * 设置游戏的状态
   */
  [SET_GAME_STATUS](state, newState) {
    state.gameStatus = newState;
  },

  /**
   * 设置选中的筹码
   */
  [UPDATE_GAME_SELECTED_CHIP](state, newState) {
    state.selectedChip = newState;
  },

  [UPDATE_GAME_BORDER_INFO](state, newState) {
    state.gameBorderInfo = Object.assign({}, state.gameBorderInfo, newState);
  },

  [SET_GAME_INFO](state, newState) {
    state.game = newState;
  },


}
