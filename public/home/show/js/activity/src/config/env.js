/**
 * 配置编译环境和线上环境之间的切换
 *
 * baseUrl: 域名地址
 * imUrl: im
 * imgBaseUrl: 图片所在域名地址
 *
 */

//测试
let baseUrl = '';
// 10.71.42.71 port:5200

if (process.env.NODE_ENV == 'development') {
  // baseUrl = 'http://localhost:5000';
  baseUrl = 'http://game.test.live.com/api';
}else if(process.env.NODE_ENV == 'production'){
}

export {
	baseUrl,
}
