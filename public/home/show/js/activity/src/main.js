import Vue from 'vue'
import App from './App'
import App2 from './App2'
import GameContainer from './GameContainer';
import GameBoardContainer from './GameBoardContainer';
import GameEntranceContainer from './GameEntranceContainer';
import VueBus from "./plugins/vue-bus";
import infiniteScroll from 'vue-infinite-scroll';
import VueTabs from 'vue-nav-tabs'
// import 'vue-nav-tabs/themes/vue-tabs.css'
import store from './store';

Vue.use(VueBus);
Vue.use(infiniteScroll);
Vue.use(VueTabs)

new Vue({
  replace: false,
  store,
  render: h => h(App),
  methods: {}
}).$mount('#answerContainer')


new Vue({
  replace: false,
  store,
  render: h => h(App2),
  methods: {}
}).$mount('#answerEntranceContainer');

new Vue({
  replace: false,
  store,
  render: h => h(GameContainer),
  methods: {}
}).$mount('#gameContainer');

new Vue({
  replace: false,
  store,
  render: h => h(GameBoardContainer),
  methods: {}
}).$mount('#gameBoardContainer');

new Vue({
  replace: false,
  store,
  render: h => h(GameEntranceContainer),
  methods: {}
}).$mount('#gameEntranceContainer');