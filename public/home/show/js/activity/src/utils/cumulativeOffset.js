/**
 * 获取元素相对#gameBox的距离
 */
export default (element) => {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element && element != document.getElementById("gameBox"));

    return {
        top: top,
        left: left
    };
}

