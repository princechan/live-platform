# 说明

## 技术栈

vue2 + vuex + webpack + ES6/7 + fetch + sass + flex


## 项目运行

```
cd web

npm install

npm run dev

```


## 项目发布
```
npm run build
```

