var webpack = require('webpack')
var path = require('path')
var projectRoot = path.resolve(__dirname, '../')
var answerJsRoot = path.resolve(__dirname, '../../')


module.exports = {
    entry: {
    	activity: [
    	 path.resolve(projectRoot, 'dist/act.js'),
    	 path.resolve(projectRoot, 'dist/static/js/manifest.js'),
    	 path.resolve(projectRoot, 'dist/static/js/vendor.js'),
    	 path.resolve(projectRoot, 'dist/static/js/app.js'),
    	]
    },
    output: {
        // path: config.build.assetsRoot,
        // publicPath: process.env.NODE_ENV === 'production' ? config.build.assetsPublicPath : config.dev.assetsPublicPath,
        path: path.resolve(answerJsRoot, ''),
        filename: 'activity.js'
        // filename: '[name].js'
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
}
