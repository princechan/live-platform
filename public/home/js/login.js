// noah.g 修改带有noah.g注释部分 
var Login = {
  login_url: './index.php?g=Api&m=Thirdpartyuser&a=login',
  reg_pt_url: './index.php?g=Api&m=Thirdpartyuser&a=register',
  reg_phone_url: './index.php?g=Api&m=Thirdpartyuser&a=register_mobile',
  loginout_url: './index.php?g=Api&m=Thirdpartyuser&a=logout',
  forget_url: './index.php?g=home&m=user&a=forget',
  captcha_url: './index.php?g=Api&m=Thirdpartyuser&a=get_captcha',
  code_url: './index.php?g=Api&m=Thirdpartyuser&a=mobile_code',
  reg_chek: './index.php?g=api&m=Thirdpartyuser&a=checkLoginName',
  dombody: $("body"),
  type: 'login',
  checkPhone: /^0?1[3|4|5|7|8|9][0-9]\d{8}$/,
  checkPass: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/,
  getRoomAdminUserList: './index.php?g=Home&m=Show&a=getRoomAdminUserList',
  createTmpEasemobUrl: '/index.php?g=user&m=public&a=createTmpEasemob',

  hxLogin: function (data) {
    if (data && data.userid) {
      handlePageLimit();
      var total = getPageCount(); //获取登录数
      if (total > PAGELIMIT) {
        alert('私信聊天最多支持' + PAGELIMIT + '个resource同时登录');
        return;
      }
      var isTempUser = data.isTempUser;
      conn.open({
        apiUrl: Easemob.im.config.apiURL,
        user: data.userid,
        pwd: isTempUser ? ('tmpPwd' + data.userid.replace("temp_", "")) : ("fmscms" + data.userid),
        appKey: Easemob.im.config.appkey //连接时提供appkey
      });
    } else {
      alert("用户ID不存在，无法创建私信");
    }
  },
  /*注册账号 //data对象示例 data={'userid';'','isTempUser':false};*/
  hxReg: function (data, callback) {
    //环信注册代码
    if (data && data.userid) {
      var isTempUser = data.isTempUser;
      var options = {
        username: data.userid,
        password: isTempUser ? ('tmpPwd' + data.userid.replace("temp_", "")) : ("fmscms" + data.userid),
        appKey: Easemob.im.config.appkey,
        apiUrl: Easemob.im.config.apiURL,
        success: callback || function () {

        },
        error: function (e) {
          // console.log(e)
        }
      };
      Easemob.im.Helper.registerUser(options);
      isTempUser ? this.createTmpEasemob(data.userid) : null;
    } else {
      alert('用户ID不存在，无法创建私信');
    }
  },
  /*退出账号*/
  hxLogout: function () {
    try { //将环信私信功能注销掉
      if (conn) {
        conn.stopHeartBeat();
        conn.close();
        clearPageSign();
      }
    } catch (e) {
      alert("环信退出失败");
    }
  },
  createTmpEasemob: function (name) {
    var _this = this;
    $.ajax({
      url: _this.createTmpEasemobUrl,
      data: {
        name: name
      },
      type: "POST",
      dataType: "JSON",
      success: function (res) {}
    })
  },
  init: function () {
    this.threeOpen();
  },
  threeOpen: function () {
    var room = new Room();
    // try {
    //     if (_DATA.anchor.id == _DATA.user.id) { //如果是“我要直播”页面，则直接return
    //         return;
    //     }
    // } catch (e) {
    // }
    _this = this;
    $.ajax({
      url: "./index.php?g=home&m=user&a=threeparty",
      success: function (data) {
        var str = JSON.parse(data);
        Login.dom(str['qq'], str['weibo'], str['weixin']);
        Login.addEvent();

        if ($('#LF-pager').length > 0) { //全局变量，首页没有加载，添加了判断 ,gus add

          // if (!_DATA.live.islive != "") {//主播休息中判断
          if (room.typeId == 0 && !room.isLive) { //主播休息中判断
            _this.js_login_pop_cover.fadeIn(300);
            _this.js_tishibox_pop.fadeIn(300);
            _this.js_tishibox_pop[0].children[1].children[0].innerHTML = '<p>主播休息中</p><span><em class="js_daojishi">5</em>秒后自动关闭</span>';
            Login.daojishi(5);
          }
        }
      }
    });
  },
  dom: function (qq, weibo, weixin) {
    var _this = this;
    var threeQQ = '',
      threeWeibo = '',
      threeWeixin = '';

    if (qq == "1") {
      threeQQ = '<div class="qq js_qq js_login_qq"><span></span></div>';
    }
    if (weibo == "1") {
      threeWeibo = '<div class="weibo js_weibo js_login_weibo"><span></span></div>';
    }
    if (weixin == "1") {
      threeWeixin = '<div class="weixin js_weixin js_login_weixin"><span></span></div>';
    }

    var login_name_prefix = base_data.login_name_prefix;

    var login_html = '<div class="login_pop js_login_pop">\
						<div class="title">\
							<span class="close js_close js_login_close"></span>\
							<span class="denglu act">登 录</span>\
							<span class="zhuce">注 册</span>\
						</div><article>\
							<div class="warring js_log_warring">请输入手机号码</div>\
							<div class="phoneArea">\
								<i class="phoneIcon usericon"></i>\
								<input class="phone  js_boxlogin_users_input" type="text" placeholder="输入用户名(' + login_name_prefix + '开头)/手机号码" maxlength="10">\
							</div>\
							<div class="phoneArea">\
								<i class="phoneIcon passicon"></i>\
								<input class="pass js_boxlogin_pass_input" type="password" placeholder="输入密码" maxlength="16">\
							</div>\
							<a class="submit js_login_submit get_none off">登 录</a>\
						</article>\
						<p class="other_login_tip"><span class="logreguser">注册账号</span><span class="loginpass">忘记密码</span></p>\
					</div>';

    var reg_html = '<div class="login_pop js_reg_pop">\
					<div class="title">\
						<span class="close js_close js_login_close"></span>\
						<span class="denglu">登 录</span>\
						<span class="zhuce act">注 册</span>\
					</div>\
					<div class="tips">\
						<span class="putongred act">普通注册</span>&nbsp;\
					</div>\
					<article class="js_reg_pop_pt"><div class="warring js_reg_warring"></div>\
						<div class="phoneArea">\
						<i class="phoneIcon usericon"></i><span class="usersr js_reg_usersr_text">' + login_name_prefix + '</span>\
						<input class="users js_reg_users_input" type="tel" placeholder="输入用户名" maxlength="10"><em class=""></em>\
						\</div>\
						<div class="phoneArea">\
						<i class="phoneIcon passicon"></i>\
						<input class="pass js_reg_pass_input" type="password" placeholder="输入密码" maxlength="16">\
					\</div>\
					<div class="key_con ">\
							<div class="keyBorder">\
								<i class="keyIcon ccodeicon"></i>\
								<input class="key js_reg_captcha_input" type="text" placeholder="输入验证码" maxlength="6">\
							</div>\
							<img class="get_none js_reg_captcha_img" src="">\
						</div>\
						<p><input class="retagrebox" checked="checked"  type="checkbox"  /><em><a style="color: #da5537;" href="./index.php?m=page&a=agreement" target="_blank">同意《用户协议》</a></em></p>\
						<a class="submit js_reg_submit get_none off">立即注册</a>\
					\</article>\
					<article class="js_reg_pop_mobile" style="display:none;"><div class="warring js_reg_warring"></div>\
						<div class="phoneArea">\
							<i class="phoneIcon"></i>\
							<input class="phone js_reg_phone_input" type="text" placeholder="输入手机号码" maxlength="11">\
						</div>\
						<div class="key_con">\
							<div class="keyBorder">\
								<i class="keyIcon"></i>\
								<input class="key js_reg_code_input" type="text" placeholder="输入验证码" maxlength="6">\
							</div>\
							<a class="get_none js_reg_getcode">获取验证码</a>\
						</div>\
						<p><input type="checkbox"/><em><a style="color: #da5537;" href="./index.php?m=page&a=agreement" target="_blank">同意《用户协议》</a></em></p>\
						<a class="submit js_reg_phone_submit get_none off">立即注册</a>\
					</article>\
					<p class="other_login_tip"></p>\
				</div>';
    var forget_html = '<div class="login_pop js_forget_pop">\
					<div class="title">\
						<span class="close js_close js_login_close"></span>\
						忘记密码\
					</div>\
					<article style="display:none;">\
						<div class="weibo js_weibo js_login_weibo"><span></span></div>\
						<div class="weixin js_weixin js_login_weixin"><span></span></div>\
						<div class="qq js_qq js_login_qq"><span></span></div>\
					</article>\
					<article>\
						<div class="tips" style="display:none;"></div>\
					</article>\
					<article>\
						<div class="warring js_forget_warring">请输入手机号码</div>\
						<div class="phoneArea">\
							<i class="phoneIcon"></i>\
							<input class="phone js_forget_phone_input" type="text" placeholder="输入手机号码" maxlength="11">\
						</div>\
						<div class="phoneArea">\
							<i class="phoneIcon"></i>\
							<input class="pass js_forget_pass_input" type="password" placeholder="输入新密码" >\
						</div>\
						<div class="phoneArea">\
							<i class="phoneIcon"></i>\
							<input class="pass js_forget_repass_input" type="password" placeholder="输入确认密码" >\
						</div>\
						<div class="key_con">\
							<div class="keyBorder">\
								<i class="keyIcon"></i>\
								<input class="key js_forget_code_input" type="text" placeholder="输入验证码" maxlength="6">\
							</div>\
							<a class="get_none js_forget_getcode">获取验证码</a>\
						</div>\
						<p><span class="login-btn fl js-reg">注 册</span><span class="login-btn fr js-login">登 录</span></p>\
						<a class="submit js_forget_submit get_none">确认</a>\
					</article>\
					<p class="other_login_tip"></p>\
					<div class="login_popbox js_forget_popbox">\
						<div class="inner">\
							<i class="close"></i>\
							<div class="warring js_forget_captcha_warring"></div>\
							<input class="js_forget_captcha_input" type="text" placeholder="请输入右侧字符" maxlength="20"/>\
							<img src=""/ class="js_forget_captcha_img">\
							<a class="js_forget_captcha get_none">确 定</a>\
						</div>\
					</div>';

    var callAdmin_html = '<div class="login_pop js_chozhi_pop"><div class="title">联系管理员\
								<span class="close js_close js_login_close"></span>\
								</div>\
								<div class="chozhizhibo"><div class="tit2">点击下方开始私聊与管理员聊天</div>\
								<div class="chongzhicont"><p><span class="admuse"></span><input class="siliao" type="button" value="开启私聊" ></p></div>\
								</div>\
								<div class="chongzhibottom"><input type="button" class="chozhismt" value="我知道了" /></div>\
							</div>';

    var tishibox_html = '<div class="login_pop js_tishibox_pop"><div class="title">提示\
								<span class="close js_close js_login_close"></span>\
								</div>\
								<div class="tishibox"><div class="tit2"></div>\
								</div>\
								<div class="chongzhibottom"><input type="button" class="chozhismt" value="我知道了" /></div>\
							</div>';
    var loadingif_html = '<div class="loadingif js_loadingif_pop"><div class="tit">正在登陆！</div><div class="loadingifspri"></div></div>'

    login_html_bg = '<div class="login_pop_cover js_login_pop_cover"></div>';

    this.dombody.append(login_html_bg + login_html + reg_html + threeQQ + threeWeibo + threeWeixin + loadingif_html + callAdmin_html + tishibox_html + forget_html);
  },
  addEvent: function () {
    this.js_login_pop = $(".js_login_pop"),
      this.js_login_pop_cover = $(".js_login_pop_cover"),
      this.js_reg_pop = $(".js_reg_pop"),
      this.js_forget_pop = $(".js_forget_pop"),
      this.js_close = $(".js_close"),
      this.js_login_phone_input = $(".js_login_phone_input"),
      this.js_login_users_input = $(".js_login_users_input")
    this.js_login_pass_input = $(".js_login_pass_input"),
      this.js_login_submit = $(".js_login_submit"),
      this.js_boxlogin_users_input = $(".js_boxlogin_users_input");
    this.js_boxlogin_pass_input = $(".js_boxlogin_pass_input");

    this.js_reg_warring = $('.js_reg_warring');
    this.js_log_warring = $('.js_log_warring');
    this.js_chozhi_pop = $('.js_chozhi_pop');
    this.js_tishibox_pop = $('.js_tishibox_pop');
    this.js_daojishi = $('.js_daojishi');
    this.js_loadingif_pop = $('.js_loadingif_pop');


    this.js_reg_usersr_text = $(".js_reg_usersr_text"),
      this.js_reg_phone_input = $(".js_reg_phone_input"),
      this.js_reg_users_input = $(".js_reg_users_input"),
      this.js_reg_pass_input = $(".js_reg_pass_input"),
      this.js_reg_repass_input = $(".js_reg_repass_input"),
      this.js_reg_code_input = $(".js_reg_code_input"),
      this.js_reg_getcode = $(".js_reg_getcode"),
      this.js_reg_popbox = $(".js_reg_popbox");
    this.popboxclose = $(".login_popbox .close");
    this.js_reg_captcha_input = $(".js_reg_captcha_input"),
      this.js_reg_captcha_img = $(".js_reg_captcha_img"),
      this.js_reg_captcha = $(".js_reg_captcha");
    this.js_reg_submit = $(".js_reg_submit");
    this.js_reg_phone_submit = $(".this.js_reg_phone_submit");

    this.js_forget_phone_input = $(".js_forget_phone_input"),
      this.js_forget_pass_input = $(".js_forget_pass_input"),
      this.js_forget_repass_input = $(".js_forget_repass_input"),
      this.js_forget_code_input = $(".js_forget_code_input"),
      this.js_forget_getcode = $(".js_forget_getcode"),
      this.js_forget_popbox = $(".js_forget_popbox");
    this.js_forget_captcha_input = $(".js_forget_captcha_input"),
      this.js_forget_captcha_img = $(".js_forget_captcha_img"),
      this.js_forget_captcha = $(".js_forget_captcha");
    this.js_forget_submit = $(".js_forget_submit");

    var _this = this;

    $(".js-login").on("click", function (e) {
        e.preventDefault(), _this.login()
      }),
      $(".js-reg").on("click", function (e) {
        e.preventDefault(), _this.reg()

      }),

      $(".js-forget").on("click", function (e) {
        e.preventDefault(), _this.forget()
      }),
      $("#beloginBox .login").on("click", function (e) {
        e.preventDefault(), _this.login()
      }),
      $("#beloginBox .reg").on("click", function (e) {
        e.preventDefault(), _this.reg()
      }),
      $(".hd-login .logout").click(function (e) {
        e.preventDefault(), _this.logout()
      }),
      $(".hd-login .already-login").on("click", function () {
        $(".icon-more-ed").length === 0 ? $(".icon-more").addClass("icon-more-ed") : $(".icon-more").removeClass("icon-more-ed"), $(".userinfo").fadeToggle(300)
      }),
      $(".hd-login").on('mouseleave', function () {
        $(this).find('.userinfo').fadeOut();
        $(".icon-more").removeClass("icon-more-ed");
      });

    $(".hd-nav .more").hover(function (e) {
        $(this).addClass("hover")
      }, function (e) {
        $(this).removeClass("hover")
      }).on("click", ".link", function (e) {
        e.preventDefault()
      }),
      _this.js_close.on("click", function () {
        _this.closePop()
      })
    _this.popboxclose.on("click", function () {
      $(".login_popbox").fadeOut(300)
    })

    $(".js_login_weibo").click(function () {
      window.location.href = "index.php?g=home&m=User&a=weibo";
    }), $(".js_login_weixin").click(function () {
      window.location.href = "index.php?g=home&m=User&a=weixin";
    }), $(".js_login_qq").click(function () {
      /* alert("等待第三方配置...") */
      window.location.href = "index.php?g=home&m=User&a=qq";
    })
  },
  closePop: function () {
    try {

      this.js_login_pop.fadeOut(300),
        this.js_reg_pop.fadeOut(300),
        this.js_forget_pop.fadeOut(300),
        this.js_chozhi_pop.fadeOut(300),
        this.js_tishibox_pop.fadeOut(300),

        this.js_login_pop_cover.fadeOut(300),
        $(".js_reg_pop input").val("");
      $(".js_login_pop input").val("");
      $(".js_forget_pop input").val("");
      $(".js_reg_captcha_img").attr('src', '');

    } catch (e) {
      console.info(e)
    }
  },
  loginVerify: function () {
    var _this = this;
    var phone = _this.js_login_phone_input,
      users = _this.js_login_users_input,
      pass = _this.js_login_pass_input,
      login = _this.js_login_submit,
      usersbox = _this.js_boxlogin_users_input,
      passbox = _this.js_boxlogin_pass_input;

    // phone.on("keyup input propertychange", function() {
    // 	var e = $.trim($(this).val());
    // 	e.length > 10 && W()
    // })
    // pass.on("keyup input propertychange", function() {
    // })

    usersbox.on("keyup input propertychange", function () {
      _this.js_log_warring.hide(0).html('');
      var e = $.trim($(this).val());
      e.length > 10 && W()

    })
    passbox.on("keyup input propertychange", function () {
      _this.js_log_warring.hide(0).html('');
      W()
    })

    function W() {
      d = $.trim(usersbox.val()), v = $.trim(passbox.val()), d.length > 10 && v.length > 5 ? Y() : N()
    }

    function Y() {
      login.removeClass("get_none").addClass("get_key").unbind().click(L)
    }

    function N() {
      login.unbind().removeClass("get_key").addClass("get_none")
    }

    function L() {
      _this.dologin();
    }

    Login.getCaptcha();
  },
  loginWarring: function (msg) {
    var e = null,
      t, n;
    t = $(".login_popbox"), t.is(":hidden") ? n = $(".js_" + this.type + "_warring") : n = $(".login_popbox .js_" + this.type + "_warring"), n.text(msg).show(), e && (window.clearTimeout(e), e = null), e = window.setTimeout(function () {
      e = null, n.fadeOut(300)
    }, 5e3)
  },


  login: function () {
    this.js_login_pop_cover.fadeIn(300),
      this.js_reg_pop.fadeOut(300),
      this.js_forget_pop.fadeOut(300),
      this.js_login_pop.fadeIn(300),
      this.type = 'login',
      this.loginVerify();
  },
  dologin: function () {

    var _this = this,
      users = this.js_login_users_input.val(),
      pass = this.js_login_pass_input.val();

    $.ajax({
      url: _this.login_url,
      data: {
        username: users,
        password: pass
      },
      type: "POST",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      beforeSend: function () {
        _this.js_login_pop_cover.fadeIn(300);
        _this.js_loadingif_pop.fadeIn(300);
        _this.js_loadingif_pop.find('.tit').html('登录中...');
      },
      success: function (data) {
        if (data.status == 1) {
          // window.location.reload();
          // $('.usermes .loging').hide(0);
          // $('.usermes .loginout').show(0);
          _this.js_loadingif_pop.find('.tit').html(data.info);
          window.location.reload();


        } else {

          _this.js_loadingif_pop.fadeOut(300);
          _this.js_login_users_input.val('');
          _this.js_login_pass_input.val('');
          Login.login();
          _this.js_log_warring.show(0).html(data.info);

        }

      }
    })

  },
  doboxlogin: function () {

    var _this = this,
      // phone=this.js_login_phone_input.val(),
      users = this.js_boxlogin_users_input.val(),
      pass = this.js_boxlogin_pass_input.val();

    $.ajax({
      url: _this.login_url,
      data: {
        username: users,
        password: pass
      },
      type: "POST",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      beforeSend: function () {
        _this.js_login_pop.fadeOut(300);
        _this.js_loadingif_pop.fadeIn(300);
        _this.js_loadingif_pop.find('.tit').html('登录中...');
      },
      success: function (data) {
        if (data.status == 1) {
          // window.location.reload();
          // $('.usermes .loging').hide(0);
          // $('.usermes .loginout').show(0);
          _this.js_loadingif_pop.find('.tit').html(data.info);
          window.location.reload();

        } else {
          _this.js_login_pop.fadeIn(300);
          _this.js_loadingif_pop.fadeOut(300);
          _this.js_log_warring.show(0).html(data.info);

        }

      }
    })

  },
  regVerify: function () { //注册数据注入检查
    var _this = this;
    var phone = _this.js_reg_phone_input,
      users = _this.js_reg_users_input,
      pass = _this.js_reg_pass_input,
      repass = _this.js_reg_repass_input,
      code = _this.js_reg_code_input,
      getcode = _this.js_reg_getcode,
      popbox = _this.js_reg_popbox,
      captcha = _this.js_reg_captcha_input,
      captchaimg = _this.js_reg_captcha_img,
      captchasub = _this.js_reg_captcha,
      reg = _this.js_reg_submit;

    phone.on("keyup input propertychange", function () {
      _this.js_reg_warring.hide(0).html('');
      var e = $.trim($(this).val());
      e.length > 10 ? (C() ? GY() : GN()) : GN(), W()
    })
    pass.on("keyup input propertychange", function () {
      _this.js_reg_warring.hide(0).html('');
      W()
    })
    repass.on("keyup input propertychange", function () {
      W()
    })
    users.on("keyup input propertychange", function () {
      _this.js_reg_warring.hide(0).html('');
      W()
    })
    code.on("keyup input propertychange", function () {
      W()
    })
    captcha.on("keyup input propertychange", function () {
      var e = $.trim($(this).val());
      e.length == 4 ? CapY() : CapN()
    })
    captchaimg.on("click", function () {
      Login.getCaptcha();
    })

    getcode.on('click', function () {
      Login.getCodeU();
    });

    Login.getCaptcha();

    function C() {
      if (!_this.checkPhone.test(phone.val())) {
        _this.loginWarring("您输入的手机号有误")
        return !1;
      }
      return !0;
    }

    /* 点击 获取验证码 */
    function G() {
      popbox.fadeIn(300), getCaptcha()
    }

    function GY() {
      getcode.hasClass("login_counting") || getcode.removeClass("get_none").addClass("get_key").unbind().click(G)
    }

    function GN() {
      getcode.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 计时器 */
    function O() {
      function r(e) {
        getcode.text(e)
      }

      GN(), CapN();
      var e = 60,
        n = window.setInterval(function () {
          if (e > 0) {
            var i = e-- + "s 重新获取";
            getcode.addClass("login_counting"), r(i)
          } else window.clearInterval(n), n = null, r("获取验证码"), getcode.removeClass("login_counting"), C() && GY()
        }, 1e3)
    }

    /* 验证码 */
    function Cap() {
      _this.type = 'captcha', getCodeU()
    }

    function CapY() {
      captchasub.removeClass("get_none").addClass("get_key").unbind().click(Cap)
    }

    function CapN() {
      captchasub.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 注册页面 */
    function W() {
      d = $.trim(phone.val()), v = $.trim(pass.val()), rv = $.trim(repass.val()), c = $.trim(code.val()), d.length > 10 && v.length > 5 && rv.length > 5 && c.length == 6 ? Y() : N()
    }

    function Y() {
      reg.removeClass("get_none").addClass("get_key").unbind().click(R)
    }

    function N() {
      reg.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 发送短信验证码 */
    // function getCodeU(){
    // 	$.ajax({
    // 		url: _this.code_url,
    // 		data: {mobile:phone.val(),captcha:captcha.val()},
    // 		type: "GET",
    // 		dataType: "JSON",
    // 		// jsonp: "callback",
    // 		cache: !1,
    // 		success:function(data){
    // 			if(data && data.errno ==0){
    // 				_this.type='reg',popbox.fadeOut(300),O()
    // 			}else{
    // 				_this.loginWarring(data.errmsg),CapN(),getCaptcha();
    // 				return !1;
    // 			}

    // 		}
    // 	})


    // }
    /* 图片验证码 */
    // function getCaptcha(){
    // 	$.ajax({
    // 		url: _this.captcha_url,
    // 		data: {},
    // 		type: "GET",
    // 		dataType: "JSON",
    // 		// jsonp: "callback",
    // 		cache: !1,
    // 		success:function(data){
    // 			if(data){
    // 				_this.js_reg_captcha_img.attr("src",data.captcha);
    // 				// _this.js_reg_captcha_img.attr("src",data.data.captcha+"&v=" + parseInt(Math.random() * 1e8, 10));
    // 			}else{
    // 				_this.loginWarring(data.errmsg);
    // 				return !1;
    // 			}

    // 		}
    // 	})
    // }
    function R() {
      if (!_this.checkPass.test(pass.val())) {
        _this.loginWarring("密码必须包含字母和数字，6-12位")
        return !1;
      }
      if (pass.val() != repass.val()) {
        _this.loginWarring("两次密码不一致")
        return !1;
      }
      _this.doreg();
    }
  },
  getCodeU: function () {
    var _this = this;
    $.ajax({
      url: _this.code_url,
      // data: {mobile:phone.val(),captcha:captcha.val()},
      type: "GET",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      success: function (data) {
        if (data) {
          // _this.type='reg',popbox.fadeOut(300),O()
        } else {
          _this.loginWarring(data.errmsg), CapN(), getCaptcha();
          return !1;
        }

      }
    })


  },

  getCaptcha: function () {
    var _this = this;
    $.ajax({
      url: _this.captcha_url,
      data: {},
      type: "GET",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      success: function (data) {
        if (data) {
          _this.js_reg_captcha_img.attr("src", data.captcha + "&=" + Math.random());
          // _this.js_reg_captcha_img.attr("src",data.data.captcha+"&v=" + parseInt(Math.random() * 1e8, 10));
        } else {
          _this.loginWarring(data.errmsg);
          return !1;
        }

      }
    })
  },
  reg: function () { //注册弹出框

    this.js_login_pop_cover.fadeIn(300),
      this.js_login_pop.fadeOut(300),
      this.js_forget_pop.fadeOut(300),
      this.js_reg_pop.fadeIn(300),
      this.type = 'reg',
      this.regVerify()

  },
  doreg: function () { //注册发送
    var _this = this,
      agreement = 1,
      // phone=this.js_reg_phone_input.val(),
      users = this.js_reg_usersr_text.text() + this.js_reg_users_input.val().trim(),
      pass = this.js_reg_pass_input.val(),
      code = this.js_reg_captcha_input.val();
    // qudao=this.GetQueryString('qudao');//调用函数获取渠道值

    // 是否来自a01 btt,a04 zl,xsm noah.g
    if (base_data.app_key === 'a01') {
      var usernameTest = /^[0-9A-Za-z]{4,9}$/;
      var passTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,10}$/;
      if (!usernameTest.test(users)) {
        this.js_reg_warring.show(0).html('账号为4-9位');
        return false;
      }
      if (!passTest.test(pass)) {
        this.js_reg_warring.show(0).html('密码为字母开头8-10位字母数字组合');
        return false;
      }
    } else if (base_data.app_key === 'a04') {
      var usernameTest = /^[0-9A-Za-z]{4,11}$/;
      var passTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/;
      if (!usernameTest.test(users)) {
        this.js_reg_warring.show(0).html('账号为4-11位');
        return false;
      }
      if (!passTest.test(pass)) {
        this.js_reg_warring.show(0).html('密码为字母开头6-16位字母数字组合');
        return false;
      }
    } else if (base_data.app_key === 'xsm') {
      var usernameTest = /^[0-9A-Za-z]{3,10}$/;
      var passTest = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/;
      if (!usernameTest.test(users)) {
        this.js_reg_warring.show(0).html('账号为3-10位');
        return false;
      }
      if (!passTest.test(pass)) {
        this.js_reg_warring.show(0).html('密码为字母开头6-16位字母数字组合');
        return false;
      }
    }
    $.ajax({
      url: _this.reg_pt_url,
      data: {
        username: users,
        password: pass,
        code: code,
        agreement: agreement
      },
      type: "POST",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      beforeSend: function () {
        _this.js_reg_pop.fadeOut(300);
        _this.js_loadingif_pop.fadeIn(300);
        _this.js_loadingif_pop.find('.tit').html('注册中...');
      },
      success: function (data) {
        if (data.status == 1) {
          // _this.hxReg(data, function (result) {//去掉环信回调

          _this.js_loadingif_pop.find('.tit').html(data.msg);
          setTimeout(function () { //使用  setTimeout（）方法设定定时1500毫秒
            _this.js_loadingif_pop.fadeOut(300);
            window.location.reload();
          }, 1000);

          // });

        } else {
          if (data.info) {
            _this.js_reg_warring.show(0).html(data.info);
          } else {
            _this.js_reg_warring.show(0).html(data.msg);
          }

          _this.js_reg_pop.fadeIn(300);
          _this.js_loadingif_pop.fadeOut(300);
        }
      }
    })
  },
  dorechek: function () {
    var _this = this;
    var username = this.js_reg_usersr_text.text() + $.trim(this.js_reg_users_input.val());

    $.ajax({
      url: _this.reg_chek,
      type: 'GET',
      dataType: 'json',
      data: {
        username: username
      },
      success: function (res) {

        switch (res.code) {

          case 200:
            _this.js_reg_pop.find('em').removeClass();

            if (res.data.is_valid == 1) {

              _this.js_reg_pop.find('em').addClass('keyong');

            } else {

              _this.js_reg_pop.find('em').addClass('keyong_no');
            }

            break;

          case 400:
            _this.js_reg_pop.find('em').removeClass();
            _this.js_reg_warring.show(0).html(res.message);

        }
      }
    })
  },

  dophonereg: function () { //注册发送
    var _this = this,
      phone = this.js_reg_phone_input.val(),
      code = this.js_reg_code_input.val();

    $.ajax({
      url: _this.reg_phone_url,
      data: {
        mobile: phone,
        code: code
      },
      type: "POST",
      dataType: "JSON",
      // jsonp: "callback",
      cache: !1,
      success: function (data) {
        if (data.status == 1) {
          // layer.msg("注册成功！");
          setTimeout(function () { //使用  setTimeout（）方法设定定时1500毫秒
            window.location.reload(); //页面刷新
          }, 1500);

        } else {
          _this.js_reg_warring.show(0).html(data.info);
        }

      }
    })

  },
  logout: function () {
    var _this = this;
    $.ajax({
      url: _this.loginout_url,
      data: {},
      type: "GET",
      dataType: "json",
      // jsonp: "callback",
      cache: !1,
      success: function (data) {
        _this.hxLogout();
        window.location.reload();
      }
    })

  },
  forgetVerify: function () {
    var _this = this;
    var phone = _this.js_forget_phone_input,
      pass = _this.js_forget_pass_input,
      repass = _this.js_forget_repass_input,
      code = _this.js_forget_code_input,
      getcode = _this.js_forget_getcode,
      popbox = _this.js_forget_popbox,
      captcha = _this.js_forget_captcha_input,
      captchaimg = _this.js_forget_captcha_img,
      captchasub = _this.js_forget_captcha,
      forget = _this.js_forget_submit;

    phone.on("keyup input propertychange", function () {
      var e = $.trim($(this).val());
      e.length > 10 ? (C() ? GY() : GN()) : GN(), W()
    })
    pass.on("keyup input propertychange", function () {
      W()
    })
    repass.on("keyup input propertychange", function () {
      W()
    })
    code.on("keyup input propertychange", function () {
      W()
    })
    captcha.on("keyup input propertychange", function () {
      var e = $.trim($(this).val());
      e.length == 4 ? CapY() : CapN()
    })
    captchaimg.on("click", function () {
      getCaptcha()
    })

    function C() {
      if (!_this.checkPhone.test(phone.val())) {
        _this.loginWarring("您输入的手机号有误")
        return !1;
      }
      return !0;
    }

    /* 点击 获取验证码 */
    function G() {
      popbox.fadeIn(300), getCaptcha()
    }

    function GY() {
      getcode.hasClass("login_counting") || getcode.removeClass("get_none").addClass("get_key").unbind().click(G)
    }

    function GN() {
      getcode.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 计时器 */
    function O() {
      function r(e) {
        getcode.text(e)
      }

      GN(), CapN();
      var e = 60,
        n = window.setInterval(function () {
          if (e > 0) {
            var i = e-- + "s 重新获取";
            getcode.addClass("login_counting"), r(i)
          } else window.clearInterval(n), n = null, r("获取验证码"), getcode.removeClass("login_counting"), C() && GY()
        }, 1e3)
    }

    /* 验证码 */
    function Cap() {
      _this.type = 'forget_captcha', getCodeU()
    }

    function CapY() {
      captchasub.removeClass("get_none").addClass("get_key").unbind().click(Cap)
    }

    function CapN() {
      captchasub.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 忘记密码页面 */
    function W() {
      d = $.trim(phone.val()), v = $.trim(pass.val()), rv = $.trim(repass.val()), c = $.trim(code.val()), d.length > 10 && v.length > 5 && rv.length > 5 && c.length == 6 ? Y() : N()
    }

    function Y() {
      forget.removeClass("get_none").addClass("get_key").unbind().click(R)
    }

    function N() {
      forget.unbind().removeClass("get_key").addClass("get_none")
    }

    /* 发送短信验证码 */
    function getCodeU() {
      $.ajax({
        url: _this.code_url,
        data: {
          mobile: phone.val(),
          captcha: captcha.val()
        },
        type: "GET",
        dataType: "jsonp",
        jsonp: "callback",
        cache: !1,
        success: function (data) {
          if (data && data.errno == 0) {
            _this.type = 'forget', popbox.fadeOut(300), O()
          } else {
            _this.loginWarring(data.errmsg), CapN(), getCaptcha();
            return !1;
          }

        }
      })
    }

    /* 图片验证码 */
    function getCaptcha() {
      $.ajax({
        url: _this.captcha_url,
        data: {},
        type: "POST",
        dataType: "JSON",
        // jsonp: "callback",
        cache: !1,
        success: function (data) {
          if (data && data.errno == 0) {
            _this.js_forget_captcha_img.attr("src", data.data.captcha + "&v=" + parseInt(Math.random() * 1e8, 10));
          } else {
            _this.loginWarring(data.errmsg);
            return !1;
          }

        }
      })
      return;
    }

    function R() {
      if (!_this.checkPass.test(pass.val())) {
        _this.loginWarring("密码必须包含字母和数字，6-12位")
        return !1;
      }
      if (pass.val() != repass.val()) {
        _this.loginWarring("两次密码不一致")
        return !1;
      }
      _this.doforget();
    }
  },
  forget: function () {
    this.js_login_pop_cover.fadeIn(300),
      this.js_reg_pop.fadeOut(300),
      this.js_login_pop.fadeOut(300),
      this.js_forget_pop.fadeIn(300),
      this.type = 'forget',
      this.forgetVerify()
  },
  doforget: function () {
    var _this = this,
      phone = this.js_forget_phone_input.val(),
      pass = this.js_forget_pass_input.val(),
      code = this.js_forget_code_input.val();
    $.ajax({
      url: _this.forget_url,
      data: {
        mobile: phone,
        pass: pass,
        code: code
      },
      type: "GET",
      dataType: "jsonp",
      jsonp: "callback",
      cache: !1,
      success: function (data) {
        if (data && data.errno == 0) {
          $(".js_reg_pop input").val("");
          $(".js_login_pop input").val("");
          $(".js_forget_pop input").val("");
          alert("重置成功");
          _this.login();
        } else {
          _this.loginWarring(data.errmsg);
          return !1;
        }

      }
    })

  },

  GetQueryString: function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);

    if (r != null) {
      return unescape(r[2]);
    } else {
      return null;
    }
  },


  tishibox: function (url) {
    _this = this;
    _this.js_login_pop_cover.fadeIn(300);
    _this.js_tishibox_pop.fadeIn(300);

    $.get(url, function (res) {

      switch (res.code) {

        case 200:
          _this.js_tishibox_pop[0].children[1].children[0].innerHTML = '<p>' + res.data.message + '</p><span><em class="js_daojishi">5</em>秒后自动关闭</span>';

          Login.daojishi(5);
          break;

        case 400:
          _this.js_tishibox_pop.find('.tit2').html('<p>' + res.message + '</p><span><em class="js_daojishi">5</em>秒后自动关闭</span>');

          Login.daojishi(5);
      };

    });

  },

  boxchozhi: function () {

    this.js_login_pop_cover.fadeIn(300);
    this.js_chozhi_pop.fadeIn(300);
    // this.type='login'
    // this.loginVerify();
  },

  daojishi: function (num) {
    _this = this;
    var t = setInterval(function () {
      num -= 1;
      _this.js_tishibox_pop.find('.js_daojishi').text(num);
      if (num == 0) {
        clearInterval(t);
        Login.closePop();
      }
    }, 1000);
  }


}


////////////////////////////////页面加载开始////////////////////////////////


$(function () {

  Login.init();

  $(".j-follow").on("click", function (t) {
    t.preventDefault(),
      $(this).follow()
  });

  $('.logint').on('click', function (event) {
    event.preventDefault();
    Login.dologin();

  });


  $(document).keydown(function (event) {

    if (event.which == 13 && foucsmit == true) {
      foucsmit = false;
      Login.dologin();
    }
  });

  $('.loginout .useout').on('click', function (event) {
    event.preventDefault();
    Login.logout();
  });

  $('body').on('click', '.js_reg_submit', function (event) {
    event.preventDefault();

    if (!$('input.retagrebox').is(':checked')) {
      _this.js_reg_warring.show(0).html('请勾选用户协议');
      return;
    }

    Login.doreg();
    Login.getCaptcha();
  });

  $('body').on('click', '.js_reg_phone_submit', function (event) {
    event.preventDefault();
    Login.dophonereg();

  });

  $('body').on('click', '.js_login_submit', function (event) {
    event.preventDefault();
    Login.doboxlogin();
  });

  $('body').on('click', '.chozhismt', function (event) {
    event.preventDefault();
    Login.closePop();

    if ($('.js_tishibox_pop').find('.tit2').text() == '立即登录赠送礼物') {
      Login.dologin();
    }
  });

  $('body').on('blur', '.js_reg_users_input', function (event) {
    event.preventDefault();
    Login.dorechek();
  });

  $('body').on('click', '.retagrebox', function (event) {

    if ($(this).is(':checked')) {
      $(this).removeClass('act');
    } else {
      $(this).addClass('act');
    }
  });

  // 充值弹出
  $('body').on('click', '.leftset .callAdmin', function (event) {
    event.preventDefault();
    // 改造私聊发起noah.g
    var room = new Room();
    $.get(Login.getRoomAdminUserList, {
      live_type: room.typeId,
      liveid: room.id
    }, function (res) {
      if (res.code == 200) {
        Login.boxchozhi();
        var chongzhicont = '';
        if (JSON.stringify(res.data) == '{}') {
          //no one
        } else {
          res.data.forEach(function (val, indx) {
            if (val.id != _DATA.user.id) {
              chongzhicont += '<li layim-event="chat" data-type="history" data-index="0" data-id="' + val.id + '" data-picurl="' + val.avatar + '" data-uname="' + val.user_nicename + '" id="layim-friend' + val.id + '"><img src="' + val.avatar + '"><span>' + val.user_nicename + '</span></li>';
              // chongzhicont += '<p><span>' + val.user_nicename + '</span><input class="siliao" type="button" value="开始私聊" data-user="' + val.user_nicename + '" /></p>';
            }
          });
        }
        $('.js_chozhi_pop .chongzhicont').html(chongzhicont);
      } else {
        layer.msg(res.message);
      }
    })
  });

  // 开始私聊
  $('body').on('click', '.chongzhicont input', function () {
    // console.log($(this).data('user'));
    Login.closePop();
    $('.MinChat').trigger('click');
    $('#searchfriend').val($(this).data('user'))
    setTimeout(function () {
      $('#searchFriend').trigger('click');
    }, 1000);

  });

  // *********

  ///////////////////////////////////////////////////

  var n = {
    add: function (e) {
      e.addClass("followed").html("已关注")
    },
    cancel: function (e) {
      e.removeClass("followed").html("+ 关注")
    },
    error: function (e) {
      var t = {
          add: "关注失败，请重试",
          cancel: "取消失败，请重试"
        },
        n = $('<div class="follow-tips"><h4>很抱歉</h4><p>' + t[e] + "</p></div>");
      $("body>.follow-tips").remove(), n.appendTo("body"), setTimeout(function () {
        $("body>.follow-tips").addClass("transition")
      }, 100), setTimeout(function () {
        $("body>.follow-tips").removeClass("transition")
      }, 2e3), setTimeout(function () {
        $("body>.follow-tips").remove()
      }, 2200)
    }
  };
  $.fn.follow = function (e, r, i) {
    var s = $(this);
    r = r || (s.hasClass("followed") ? "cancel" : "add"), e = e || s.closest("[data-userid]").data("userid");
    if (!_DATA.user) {
      $(".hd-login .no-login").click();
      return !1;
    }

    var t = './index.php?m=user&a=follow_',
      o = i || n[r];
    $.ajax(t + r, {
      dataType: "jsonp",
      data: {
        touid: e,
        fmt: "jsonp"
      },
      jsonp: "_callback",
      success: function (e) {
        if (!e.errno) {
          $("#author-info .follows h4").html(e.data.follows);
          $("#author-info .fans h4").html(e.data.fans);
          return o(s, r);
        }
        n.error(r)
      }
    })
  }

});


function beforeSearch() {
  var room = new Room();
  //获取输入框的值
  var uid = $("#searchfriend").val();
  //当前房间id
  // var roomid = _DATA.anchor.id;
  // var roomid = room.id;
  searchMember(uid, room.id, room.typeId);
}

function searchMember(uid, liveid, live_type) {
  var searchInfo = uid;
  if (searchInfo == '') {
    alert('请输入用户昵称或ID');
    return;
  }

  //将searchResult清空
  $(".searchResult").html("");
  $.ajax({
    type: 'GET',
    url: './index.php?g=Home&m=User&a=searchMember',
    data: {
      keyword: searchInfo,
      liveid: liveid,
      live_type: live_type
    },
    dataType: 'json',
    success: function (data) {
      $("#searchfriend").val("");
      if (data.code == 200) {
        var array = data['info'];
        //判断陌生人列表中是否存在该id的li，如果存在，将input清空
        var momoLilength = $("#momogrouplistUL").children("#" + array.id).length;

        if (momoLilength == 0) {
          var errimg = '/default.jpg';
          // var errimg = '/public/images/user/vest-icon-youke.png'; 
          var msg = "<li class='searchUser' type='chat' class='offline' className='offline' onclick=chooseContactDivClick(this) id='" + array['id'] + "'><img style='float:left;' onerror='this.src=\"" + errimg + "\"' src='" + array['avatar'] + "'><span class='chatUserName'>" + array['user_nicename'] + "</span><div class='clearboth'></div><li>";

          $(".searchResult").append(msg);
          $(".searchResult").addClass('searchMsg');
          $(".searchResult").height(60);
          $("#momogrouplistUL").css("height", 378).css("overflow", 'auto');
          $("#momogrouplist").css('height', 376);
          $("#contractlist11").css('height', 380);
          // 点击开始私聊后人员选中 noah.g
          $('.searchUser').trigger('click');
        } else {
          $("#searchfriend").val("");
          $(".searchResult").html();
          $(".searchResult").height(0).removeClass('searchMsg');
          $("#contractlist11").css('height', 440);
          $("#momogrouplistUL").css("height", 437).css("overflow", 'auto');
          alert("该用户已经在列表中");
        }
      } else {
        alert("您没有私聊的马甲权限");
      }
    },
    error: function () {
      var msg = "查询失败！!";
      $(".searchResult").html(msg);
      $(".searchResult").css("height", 25);
      $(".accordion-group").css("height", 416);
      $("#contractlist11").css('height', 416); //改变陌生人列表高度，让左侧聊天人列表高度和右侧聊天窗口高度对齐
      $("#momogrouplist").css("height", 412).css("overflow", 'auto');
      $("#momogrouplistUL").css("height", 412).css("overflow", 'auto');
    }
  });
}
