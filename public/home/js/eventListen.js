/**
 * 客户端socket.io接收与发送，连接socket服务器，连接状态设置为成功
 */
_DATA.enterChat = 0;
var Socket = {
    nodejsInit: function () {
        this.inituser();
    },
    inituser: function () {
        var room = new Room();
        var processData = {liveid: room.id, live_type: room.typeId};
        if (room.typeId == 0) {
            Object.assign(processData, {stream: _DATA.live.stream});
        }


        $.ajax({
            type: 'POST',
            url: './index.php?g=home&m=show&a=setNodeInfo',
            data: processData,
            dataType: 'json',
            success: function (data) {
                if (data.error == 0) {
                    socket.emit('conn', {
                        uid: data.userinfo.id,
                        roomnum: data.userinfo.roomnum,
                        nickname: data.userinfo.user_nicename,
                        stream: data.userinfo.stream,
                        equipment: 'pc',
                        token: data.userinfo.token,
                        live_type: data.userinfo.live_type,
                        live_id: data.userinfo.live_id
                    });
                    // console.log( 'enterChat:' + _DATA.enterChat );

                    var enterChat = setInterval(function () {
                        if (_DATA.enterChat != 1) {
                            var msg = {ct: '聊天服务器未连接，请刷新'};
                            JsInterface.systemNot(msg);

                        } else {
                            // if (_DATA.live && _DATA.live.islive == 1 && _DATA.user != null) {
                            if (room.isLive && room.user && room.userIsLogged && room.typeId == 0) {
                                liveType.getzombie(_DATA.user.id, data.userinfo.stream);
                            }
                            clearInterval(enterChat);
                        }
                    }, 2000);
                } else {

                }
            }
        });
    },
    emitData: function (event, msg) {
        a = JSON.parse(msg);
        socket.emit(event, msg);
    }
};
/**
 * 客户端广播接收broadcasting
 */
socket.on('broadcastingListen', function (data) {
    for (var i in data) {
        if (i != 'remove') {
            if (data[i] == "stopplay") {
                JsInterface.superStopRoom();
            } else {
                JsInterface.chatFromSocket(data[i]);
            }
        }
    }
});

socket.on('heartbeat', function (data) {
    socket.emit("heartbeat", "heartbeat");
});

socket.on('conn', function (data) {
    // console.log('conn');
    // console.log(data);
    if (data[0] == 'ok') {
        _DATA.enterChat = 1;
        var room = new Room();
        if (_DATA.user && _DATA.user.id && room.isLive && !room.userIsAnchor) {
            msg = '{"msg":[{"_method_":"requestFans","action":"","timestamp":"' + WlTools.FormatNowDate() + '","ct":"","msgtype":"1","level":"","uid":"","sex":"","uname":"","uhead":"","usign":"","city":"好像在黑洞","level":""}],"retcode":"000000","retmsg":"ok"}';
            Socket.emitData('broadcast', msg);
        }
    }
});
