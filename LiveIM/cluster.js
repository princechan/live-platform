var cluster = require('cluster'),
    net = require('net'),
    config=require('./modules/config'),
    log4js=require('log4js'),
    servers = require('./modules/server.js'),
    farmhash = require('farmhash');


var num_processes = require('os').cpus().length;
var iphash=config.iphash;
log4js.configure(config["configure"]);
servers.logger=log4js.getLogger("INFO");
if (cluster.isMaster) {
    var workers = [];
    var count=0;
    var spawn = function(i) {
        workers[i] = cluster.fork();
        workers[i].on('exit', function(worker, code, signal) {
            servers.logger.error('respawning worker', i);
            spawn(i);
        });
    };
    for (var i = 0; i < num_processes; i++) {
        spawn(i);
    }
    var server = net.createServer({ pauseOnConnect: true }, function(connection) {
        count=count+1;
        var idx=iphash?(farmhash.fingerprint32(connection.remoteAddress) % num_processes):(farmhash.fingerprint32(count+"") % num_processes);
        workers[idx].send('sticky-session', connection);
    }).listen(config.port,function(){
        servers.logger.info("守护线程启动："+config.port+",启动内核数:"+num_processes)
    });
} else {
    servers.create(config,0,true);
}