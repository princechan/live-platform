module.exports = {
    "name": "a04",
    "redis": {
        "host": "localhost",
        "password": "tieweishivps",
        "port": 6382
    },
    "https": {
        "enabled": false,
        "cert": '/usr/local/nginx/conf/vhost/keys/a04.live.com.crt',
        "key": '/usr/local/nginx/conf/vhost/keys/a04.live.com.key'
    },
    "port": 3000,
    "iphash": false,
    "token": '1234567',
    "api": "https://a04.live.com/api/public/index.php",
    "io_options": {
        "pingTimeout": 20000,
        "pingInterval": 15000,
        "transports": [
            "polling", "websocket"
        ]
    },
    "keys": {
        "online": "online:",
        "roomgag": 'roomgag:',
        "warning": 'public_chat_room_system_msg'
    },
    "configure": {
        "appenders": {
            "console": {
                "type": "console"
            },
            "trace": {
                "type": "file",
                "filename": "logs/server-access-a04.log",
                "maxLogSize": 31457280
            },
            "http": {
                "type": "logLevelFilter",
                "appender": "trace",
                "level": "trace",
                "maxLevel": "trace"
            },
            "info": {
                "type": "dateFile",
                "filename": "logs/server-info-a04.log",
                "pattern": ".yyyy-MM-dd",
                "layout": {
                    "type": "pattern",
                    "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
                },
                "compress": true
            },
            "maxInfo": {
                "type": "logLevelFilter",
                "appender": "info",
                "level": "debug",
                "maxLevel": "info"
            },
            "error": {
                "type": "dateFile",
                "filename": "logs/server-error-a04.log",
                "pattern": ".yyyy-MM-dd",
                "layout": {
                    "type": "pattern",
                    "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
                },
                "compress": true
            },
            "minError": {
                "type": "logLevelFilter",
                "appender": "error",
                "level": "error"
            }
        },
        "categories": {
            "default": {
                "appenders": [
                    "console",
                    "http",
                    "maxInfo",
                    "minError"
                ],
                "level": "all"
            }
        }
    }
};