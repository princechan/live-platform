var func=require('./func.js');
var sockets=[];

exports.socketUtil = function(io,client,domain,config) {
    func.init(io,client,config);
    io.on('connection', function (socket) {
        client.incr("totalNums");
        func.loadKeywords();
        socket.on('conn', function (data) {
            if(!data|| !(data.token)){
                console.error("join room but data is error");return false;
            }
            func.reconnSocket(sockets[data.uid],socket,data);//socket 重连处理
            console.log("UID[" + data.uid + "]--process=" + process.pid);
            func.connByToken(socket,data,sockets,io);
        });
        socket.on('broadcast', function (data) {
            if(socket.token) {
                console.log("------------");
                console.log(data);
                func.switchAction(data,socket,sockets);
            }
        });
        socket.on('activity',function(data){
            if(socket.token){
                func.switchActivity(data,socket,sockets);
            }
        }),
        /*监控关闭房间*/
        socket.on('superadminaction',function(data){
            if(data['token'] == config['token']){
                func.process_msg(data['roomnum'],'stopplay');
            }
        });
        /*系统管理员发送红包*/
        socket.on('superRedPacket',function(data){
            var json  = typeof data == 'object'?data:JSON.parse(data);
            var content  = typeof data == 'object'?JSON.stringify(data):data;
            func.process_msg(json.msg[0].roomnum,content);
        });
        socket.on('virtualenterRoom',function(data){
            if(data['token'] == config['token']){
                var json={"msg":[{"_method_":"SendMsg","action":"0","ct":"","msgtype":"0"}],"retcode":"000000","retmsg":"OK"};info=data['virtualinfo'];
                json["msg"][0]["ct"]={"id":info['id']+"","user_nicename":info['user_nicename'],"avatar":info['avatar'],"level":info['level']+""};
                func.process_msg(data['roomnum'],json);
            }
        });
        /* 后台系统信息 */
        socket.on('systemadmin',function(data){
            if(data['token'] == config['token']){
                io.emit('broadcastingListen',['{"msg":[{"_method_":"SystemNot","action":"1","ct":"'+ data.content +'","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
            }
        });
        /* 系统直播间广告 */
        socket.on('systemadvertise',function(data){
            if(data['token'] == config['token']){
                io.emit('broadcastingListen',['{"msg":[{"_method_":"SystemAdvertise","action":"1","ct":"'+ data.content +'","url":"'+data.url+'","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
            }
        });

        /* 离开房间 */
        socket.on('leave',function(){
            console.log("leave");
            socket.emit('disconnect');
        });
        /* 断开连接 */
        socket.on('disconnect', function () {
            func.disconnect(socket,domain,sockets,io);
        });
    });
};
