var io,subclient;
exports.socketUtil = function(socketIo,client) {
	io = socketIo;
    subclient=client;
    io.on('connection', function (socket) {
        socket.on('join', function (data) {
            socket.uid=data.uid;
            socket.roomId=data.roomId;
            socket.join(data.roomId);
            client.set("online:"+data.roomId+":"+data.uid,process.pid+"-"+data.uid,function(err){
                if(err) return;
            });
        });
        socket.on('msg', function (data) {
            if(socket.roomId){
                client.keys("online:"+socket.roomId+":*", function (er, keys) {
                    if(er) return;
                    var nums=keys.length;
                    var text="Curry Room["+socket.roomId+"] have ：["+nums+"] person";
                    console.log(text);
                    io.sockets.in(socket.roomId).emit('notice',text);
                });
            }
        });
        socket.on('leave',function(){
            socket.emit('disconnect');
        });
        socket.on('disconnect', function () {
            if(socket.uid && socket.roomId){
                console.log("User[%d] Leave Room[%s]",socket.uid,socket.roomId);
                client.del("online:"+socket.roomId+":"+socket.uid,function(err){
                    if(err) return;
                    socket.leave(socket.roomId);
                });
            }
        });
    });
};
