var chat_history={},chat_interval={}
/**
 * LiveIM 通用处理类（不兼容A02）
 */
module.exports={
    request:require('request'),
    io:{},
    client:{},
    config:{},
    keywords:[],
    warning:"直播内容包含任何低俗、暴露和涉黄内容，账号会被封禁；安全部门会24小时巡查哦～",
    init:function(iosocket,redisClient,config){
        this.io=iosocket;
        this.client=redisClient;
        this.config=config;
        this.loadKeywords();
    },
    ajaxApi:function(path,callback){
        isHttps=this.config["api"].toLowerCase().substr(0,5)=="https"?true:false;
        this.request(this.config["api"]+path,isHttps?{strictSSL: false}:{},callback);
    },
    ajaxPost:function(path,data,callback){
        isHttps=this.config["api"].toLowerCase().substr(0,5)=="https"?true:false;
        this.request.post({url:this.config["api"]+path,form:data,rejectUnauthorized: false},callback);
    },
    clearResouce:function(){
        //this.client.quit();
        //this.client=null;
        //this.request=null;
        this.chat_history=null;
        this.chat_interval=null;
    },
    /**
     * 得到过滤字符串及告警用语
     * @param client
     * @returns {string[]}
     */
    loadKeywords:function(){
        var self=this;
        self.client.get("filterwords",function(err,res){
            if(!err && res){
                self.keywords=res.trim().split(",");
            }
        });
        self.client.get(this.config["keys"]["warning"],function(err,res){
            if(!err && res){
                self.warning=res.trim();
            }
        });
    },
    /**
     * 过滤字符串
     * @param content
     * @param keyArr
     * @returns {*}
     */
    filterContent:function(content){
        if(this.keywords!=undefined && this.keywords.length>0){
            var text=content;
            for(var i=0;i<this.keywords.length;i++){
                if(this.keywords[i].trim()==""){continue;}
                var reg = new RegExp(this.keywords[i], "ig");
                //判断内容中是否包括敏感词    *去*
                if(text.indexOf(this.keywords[i])!=-1){
                     text = text.replace(reg,"**");
                }
            }
            return text;
        }
        return content;
    },
    /**
     * 旧连接重连后断线处理
     * @param old_socket
     * @param socket
     * @param data
     */
    reconnSocket:function(old_socket,socket,data){
        if(old_socket && old_socket!=socket){
            console.log("重连"+old_socket.id);
            if (data.uid == data.roomnum && data.stream == old_socket.stream) {
                old_socket.reusing = 1;
            }
            old_socket.disconnect();
        }
    },
    /**
     * 追加属性值
     * @param src
     * @param target
     * @param isDeep
     */
    appendAttr:function(src,target){
        if(!src ||!target) return ;
        for(var key in target){
            src[key] = target[key];
        }
    },
    /**
     * 发送处理
     * @param io
     * @param roomnum
     * @param data
     */
    process_msg:function(roomnum,data){
        var _this=this;
        if(!chat_history[roomnum]){
            chat_history[roomnum]=[];
        }
        chat_history[roomnum].push(typeof data == 'object'?JSON.stringify(data):data);
        chat_interval[roomnum] || (chat_interval[roomnum]=setInterval(function(){
            if(chat_history[roomnum].length>0){
                var value=chat_history[roomnum].splice(0,chat_history[roomnum].length);
                _this.io.sockets.in(roomnum).emit("broadcastingListen",value);
            }else{
                clearInterval(chat_interval[roomnum]);
                chat_interval[roomnum]=null;
            }
        },200));
        //var data=JSON.stringify(json);
    },
    /**
     * 加入房间校验函数
     * @param socket
     * @param data
     * @param sockets
     * @param io
     */
    connByToken:function(socket,data,sockets){
        var _this=this;
        _this.client.get(data.token, function (err, res) {
            if(err){
                return;
            }else if(res) {
                var userInfo = JSON.parse(res);
                if (userInfo["id"] == data.uid) {
                    console.log("[初始化验证成功]--" + data.uid + "---" + data.roomnum + '---' + data.stream);
                    var attr = {"uid": data.uid,"token": data.token,"sign": userInfo['sign'],"roomnum": data.roomnum,"live_id": data.live_id||data.roomnum,"live_type": data.live_type||0,"stream": data.stream,"nicename": userInfo['user_nicename'],"uType": parseInt(userInfo['userType']),"reusing": 0};
                    _this.appendAttr(socket, attr);
                    socket.join(data.roomnum);
                    sockets[data.uid] = socket;
                    socket.emit('conn', ['ok']);
                    var json={"msg":[{"_method_":"SendMsg","action":"0","ct":"","msgtype":"0"}],"retcode":"000000","retmsg":"OK"};
                    json["msg"][0]["ct"]={"id":userInfo['id']+"","user_nicename":userInfo['user_nicename'],"avatar":userInfo['avatar'],"level":userInfo['level']+""};
                    _this.process_msg(socket.roomnum, json);
                    var onlineKey =_this.config["keys"].online + parseInt(data.live_type) + ':' + parseInt(data.roomnum);
                    if (onlineKey && data.uid) {
                        _this.client.hset( onlineKey, data.uid, res);
                    }
                    json["msg"][0]={"_method_":"SystemNot","action":"1","ct":_this.warning,"msgtype":"4"};
                    socket.emit('broadcastingListen', [JSON.stringify(json)]);
                    return;
                } else {
                    socket.disconnect();
                }
            }socket.emit('conn',['no']);
        });
    },
    /**
     * 发消息
     * @param json
     * @param msg
     * @param socket
     */
    sendMsg:function(json,msg,socket){
        var isfilter= msg['isfilter']||"0";
        if(isfilter=="0"){
            msg['ct']=this.filterContent(msg['ct']);
        }
        var _this=this;
        _this.client.hget("super",socket.uid,function(err,res){
            if(err){return;}
            if(res){
                json["msg"][0]={"_method_":"SystemNot","action":"1","ct":msg["ct"],"msgtype":"4"};
                _this.process_msg(socket.roomnum,json);
            }else{
                var keyRoomGag=_this.config["keys"]["roomgag"]+ parseInt(socket.live_type) + ':' + parseInt(socket.live_id);
                _this.client.hget(keyRoomGag,socket.uid,function(err,res){
                    if(err) return;
                    if(res != null){
                        var time = Date.parse(new Date())/1000;
                        if((time < parseInt(res))){
                            json['retcode'] = '409002';
                            socket.emit('broadcastingListen',[JSON.stringify(json)]);
                        }else{//解除禁言
                            _this.client.del(keyRoomGag,socket.uid);
                            _this.process_msg(socket.roomnum||socket.live_id,json);
                        }
                    }else{
                        _this.process_msg(socket.roomnum,json);
                    }
                });
            }
        });
    },
    /**
     * 送礼
     * @param json
     * @param msg
     * @param socket
     */
    sendGift:function(json,msg,socket){
        var gifToken = msg['ct'];
        var _this=this;
        _this.client.get(gifToken,function(error,res){
            if(!error&&res != null){
                json['msg'][0]['ct'] = JSON.parse(res);
                _this.io.sockets.in(socket.roomnum).emit('broadcastingListen',[JSON.stringify(json)]);
                _this.client.del(gifToken);
            }
        });
    },
    /**
     * 弹幕
     * @param json
     * @param msg
     * @param socket
     */
    sendBarrage:function(json,msg,socket){
        var barragetoken = msg['ct'],_this=this;
        _this.client.get(barragetoken,function(error,res){
            if(!error&&res != null){
                //console.log(res);
                var resObj = JSON.parse(_this.filterContent(res));
                json['msg'][0]['ct'] = resObj;
                _this.process_msg(socket.roomnum,json);
                _this.client.del(barragetoken);
            }
        });
    },
    /**
     * 飞屏或抢座
     * @param action
     * @param json
     * @param msg
     * @param socket
     */
    flyOrSofaOrBroadcast:function(action,json,msg,socket){
        var key=socket.uid + action,_this=this;
        _this.client.get(key,function(error,res){
            if(!error&&res == '1'){
                _this.process_msg(socket.roomnum,json);
                _this.client.del(key);
            }
        });
    },
    /**
     * 下麦或上麦显示
     * @param action
     * @param json
     * @param msg
     * @param socket
     */
    videoDeal:function(action,json,socket){
        var isClose=(action=="CloseVideo"?true:false);
        var _this=this;
        _this.client.hget('ShowVideo',socket.roomnum,function(error,res){
            result= isClose?(socket.uid==res || socket.uid==socket.roomnum):(socket.uid==res);
            if(isClose){
                _this.client.hdel('ShowVideo',socket.roomnum);
            }
            if(!error && result){
                _this.process_msg(socket.roomnum,json);
            }
        });
    },
    /**
     * 开始关闭直播
     * @param action
     * @param json
     * @param msg
     * @param socket
     */
    startEndLive:function(action,json,msg,socket){
        if(socket.uType == 50 ){
            socket.broadcast.to(socket.roomnum).emit('broadcastingListen',[JSON.stringify(json)]);
        }else{
            this.client.get("LiveAuthority" + socket.uid,function(error,res){
                var status=parseInt(res||'0');
                if(!error  && (status== 5 ||status == 1 || status == 2)){
                    socket.broadcast.to(socket.roomnum).emit('broadcastingListen',[JSON.stringify(json)]);
                }
         });}
    },
    /**
     * 关注或请求粉丝
     * @param json
     * @param socket
     */
    requestFans:function(json,socket){
        var _this=this;
        _this.ajaxApi("?service=Live.getZombie&stream=" + socket.stream+"&uid=" + socket.uid,function(error, response, body){
            if(!error && body){
                var res = JSON.parse(body);
                if( response.statusCode == 200 && res && res.data && res.data.code== 0){
                    var result="{\"msg\":[{\"_method_\":\"requestFans\",\"action\":\"3\",\"ct\": "+ body + ",\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
                    _this.process_msg(socket.roomnum,result);
                }
            }
        });
    },
    /**
     * 游戏事件处理(炸金花游戏,转盘,开心牛仔,海盗船长,二八贝)
     * @param action
     * @param json
     * @param msg
     * @param socket
     */
    gamePlay:function(action,json,msg,socket){
        this.process_msg(socket.roomnum,json);
        var status=msg['action']||0, _this=this;
        if(status==4){
            var time=msg['time']*1000,gameid=msg['gameid'];
            var gameType={'startGame':1,'startRotationGame':1};
            var servType=gameType[action]?"Game.endGame":"Game.Cowboy_end";//仅startGame和startRotationGame游戏是Game.endGame类别
            setTimeout(function() {//定时发送结果
                var token = msg['token'];
                _this.client.get(token+"_Game",function(error,res){
                    if(!error&&res != null){
                        json['msg'][0]['ct'] = JSON.parse(res)||{};
                        json['msg'][0]['_method_'] = action;
                        json['msg'][0]['action']="6";
                        _this.process_msg(io,socket.roomnum,json);
                        /* 	this.client.del(Jinhuatoken+"_Game");  */
                        _this.ajaxApi("?service="+servType+"&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
                    }
                });
            }, time);
        }
    },
    /**
     * 事件路由器
     * @param data
     * @param socket
     * @param sockets
     */
    switchAction:function(data,socket,sockets){
        var json=(typeof data == 'object')?data:JSON.parse(data);
        var msg=json['msg'][0];
        var token= json['token'];
        var action=msg['_method_'];
        var _this=this;
        switch(action){
            case 'SendMsg':          //聊天
                _this.sendMsg(json,msg,socket);
            break;
            case 'SendGift':        //送礼物
                _this.sendGift(json,msg,socket);
            break;
            case 'SendBarrage':     //弹幕
                _this.sendBarrage(json,msg,socket);
            break;
            case 'SendRedPack':     //发红包
                _this.process_msg((json.roomnum||socket.roomnum),json);
            break;
            case 'StartEndLive':    //开始关闭直播
                _this.startEndLive(action,json,msg,socket);
            break;
            case 'requestFans':
                _this.requestFans(json,socket);
            break;
            case 'SendFly':            //飞屏
            case 'fetch_sofa':        //抢座
            case 'SubmitBroadcast':  //广播
                _this.flyOrSofaOrBroadcast(action,json,msg,socket);
            break;
            case 'CloseVideo':        //下麦
            case 'ShowVideo':         //上麦
                _this.videoDeal(action,json,socket);
            break;
            case 'SendTietiao':   //贴条
            case 'SendHb':         //送红包
            case 'VodSong':        //点歌
            case 'light':          //点亮
            case 'giftPK':         //礼物PK
            case 'GrabBench':      //抢板凳
            case 'Carouse':        //转盘游戏
            case 'lianmai':        //PC端连麦
            case 'NoticeMsg':      //全站礼物
            case 'KickUser':       //踢人
            case 'ShutUpUser' :    //禁言
            case 'channel_gap':    //直播间禁言
            case 'changeVest':      //更换马甲
            case 'SendPrvMsg':      //私聊
            case 'showuserinfo':      //在线用户
            case 'RowMike':            //排麦
            case 'OpenGuard':          //购买守护
            case 'SystemNot':          //系统通知
            case 'clearScreen':        //清屏
            case 'ConnectVideo':       //连麦
            case 'switchLive' :         //切换房间(供A02侧边栏使用)
            case 'mimadoor' :      //锁门
            case 'DisableAudio' :      //静音

                _this.process_msg(socket.roomnum,json);
            break;
            case 'stopLive':        //超管关播
                _this.client.hget("super",socket.uid,function(error,res){
                    if(!error && res) _this.process_msg(socket.roomnum,'stopplay');
                });
            break;
            case 'SetBackground': //设置背景
            case 'CancelBackground'://取消背景
            case 'AgreeSong':       //同意点歌
                if(socket.uType == 50){
                    _this.process_msg(socket.roomnum,json);
                }
            break;
            case 'MoveRoom':       //转移房间
            case 'SetchatPublic':  //开启关闭公聊
            case 'CloseLive':      //关闭直播
            case 'SetBulletin':    //房间公告
            case 'ResumeUser':     //恢复发言
                if(socket.uType == 50 || socket.uType == 40){
                    _this.process_msg(socket.roomnum,json);
                }
            break;
            case 'startGame':                //炸金花游戏,
            case 'startRotationGame':       //转盘
            case 'startCattleGame':          //开心牛仔
            case 'startLodumaniGame':        //海盗船长
            case 'startShellGame':            //二八贝
                _this.gamePlay(action,json,msg,socket);
            break;
        }
        msg=null;
        json=null;
    },

    switchActivity:function(data,socket,sockets){
        var json=(typeof data == 'object')?data:JSON.parse(data);
        var method=json["_method_"];
        var _this=this;
        switch(method){
            case 'publish':      //活动发布(后台)
            case 'ready':        //主播点击活动准备后，所有房间用户接收后展开活动展示页
            case 'begin':        //答题开始通知 所有房间用户收到后立即开始进入答题，倒计时十秒
            case 'next':         //下一题(用于通知所有用户拉取排行榜)
            case 'result':       //公布答案
            case 'end':          //活动结束(主播用于通知所有用户拉取排行榜)
              _this.io.sockets.in(socket.roomnum).emit('activityListen',json);
            break;
        }
    },

    /**
     * 资源释放
     * @param socket
     */
    disconnect:function(socket,domain,sockets,io) {
        var _this=this;
        _this.client.decr("totalNums", function (err, res) {
            if (!err && parseInt(res + "") < 0) {
                _this.client.set("totalNums", 0);
            }
        });
        if (socket.roomnum == null || socket.token == null) {
            _this.clearResouce();
            return;
        }
        domain.run(function () {
                var result='{"msg":[{"_method_":"CloseVideo","action":"0","msgtype":"20","uid":"'+socket.uid+'","uname":"'+socket.nicename+'"}],"retcode":"000000","retmsg":"OK"}';
                _this.videoDeal("CloseVideo",result,socket);
                console.log("用户:"+socket.uid+"离开房间:"+socket.roomnum);
                 if(socket.roomnum==socket.uid){/* 主播 */
                     if(socket.reusing==0){
                         var data = {"uid": socket.uid,"token": socket.token,"stream": socket.stream,"live_type": 0,"liveid": socket.uid};
                            _this.ajaxPost("?service=Live.stopRoom",data,function(error, response, body){});
                            var closeJob='{"retmsg":"ok","retcode":"000000","msg":[{"msgtype":"1","_method_":"StartEndLive","action":"18","ct":"直播关闭"}]}';
                            _this.process_msg(socket.roomnum,closeJob);
                        }
                 }else{/* 观众 */
                     var onlineKey = _this.config["keys"]["online"] + parseInt(socket.live_type) + ':' + parseInt(socket.live_id);
                     _this.client.hget(onlineKey,socket.uid,function(error,res){
                         if(error){return;}
                         _this.client.hdel(onlineKey,socket.uid);
                         if(res){
                             var user = JSON.parse(res);
                             var content='{"msg":[{"_method_":"disconnect","action":"flushOnlineList","ct":{"id":"'+user['id']+'","user_nicename":"'+user['user_nicename']+'","avatar":"'+user['avatar']+'","level":"'+user['level']+'"},"msgtype":"0","uid":"'+socket.uid+'","uname":"'+user.user_nicename+'"}],"retcode":"000000","retmsg":"OK"}';
                             _this.process_msg(socket.roomnum,content);
                         }
                     });
                 }
                 if(socket.roomnum && socket.uid){
                    socket.leave(socket.roomnum);
                    sockets[socket.uid] = null;
                     delete sockets[socket.uid];
                 }
                 delete _this.io.sockets.sockets[socket.id];
                 _this.clearResouce();
            }
        );
    }
}