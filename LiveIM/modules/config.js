/**
 * Current Version For LiveIM XSM
 * Author:Clement
 */
module.exports = {
    "name": "xsm",
    "redis": {
        "host": "localhost",
        "password": "tieweishivps",
        "port": 6379
    },
    "https": {
        "enabled": false,
        "cert": 'D:/Program Files/PHPHome/nginx/conf/vhost/keys/www.live.com.crt',
        "key": 'D:/Program Files/PHPHome/nginx/conf/vhost/keys/www.live.com.key'
    },
    "port": 3000,
    "iphash": false, /*IPhash*/
    "token": '1234567',
    "api": "https://www.testzhibo.com/api/public/index.php", /*API URL*/
    "io_options": {
        /*IO OPTIONS*/
        "pingTimeout": 20000,
        "pingInterval": 15000,
        "transports": [
            "polling", "websocket"
        ]
    },
    "keys": {
        /*Redus 在线用户前缀*/
        "online": "online:", //A02:'userlist_'
        "roomgag": 'roomgag:',//A02:'ssshutup'
        "warning": 'public_chat_room_system_msg'
    },
    "configure": {
        "appenders": {
            "console": {
                "type": "console"
            },
            "trace": {
                "type": "file",
                "filename": "logs/server-access.log",
                "maxLogSize": 31457280
            },
            "http": {
                "type": "logLevelFilter",
                "appender": "trace",
                "level": "trace",
                "maxLevel": "trace"
            },
            "info": {
                "type": "dateFile",
                "filename": "logs/server-info.log",
                "pattern": ".yyyy-MM-dd",
                "layout": {
                    "type": "pattern",
                    "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
                },
                "compress": true
            },
            "maxInfo": {
                "type": "logLevelFilter",
                "appender": "info",
                "level": "debug",
                "maxLevel": "info"
            },
            "error": {
                "type": "dateFile",
                "filename": "logs/server-error.log",
                "pattern": ".yyyy-MM-dd",
                "layout": {
                    "type": "pattern",
                    "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
                },
                "compress": true
            },
            "minError": {
                "type": "logLevelFilter",
                "appender": "error",
                "level": "error"
            }
        },
        "categories": {
            "default": {
                "appenders": [
                    "console",
                    "http",
                    "maxInfo",
                    "minError"
                ],
                "level": "all"
            }
        }
    }
};