module.exports = {
    http: null,
    fs: require('fs'),
    adapter: require('socket.io-redis'),
    event: require('./event.js'),
    redis: require('redis'),
    io: null,
    config: null,
    logger: null,
    client: null,

    createClient: function () {
        this.client = this.redis.createClient(this.config['redis'].port, this.config['redis'].host, {auth_pass: this.config['redis'].password});
        this.client.on("error", function (error) {
            this.logger.error(error);
        });
    },
    resBody: function () {
        return function (req, res) {
            res.writeHead(200, {'Content-type': 'text/html;charset=utf-8'});
            res.end();
        };
    },
    listenPort: function (port) {
        var status = {
            "Server Port": port,
            "Thread PID": process.pid ? process.pid : "Default",
            "ConfigName": this.config['name'],
            "Https": (this.config['https']['enabled']) ? "enabled" : "disabled"
        };
        this.logger.info(JSON.stringify(status));
    },
    create: function (config, port, isfork) {
        var domain = require('domain').create();
        domain.on("error", function (err) {
            var print = this.logger || console;
            print.error(err);
        });
        this.config = config;
        if (config) {
            this.createClient();
            this.http = (this.config["https"]["enabled"]) ? require('https') : require('http');
            this.http.globalAgent.maxSockets = Infinity;
            var server;
            if (this.config["https"]["enabled"]) {
                var options = {
                    key: this.fs.readFileSync(this.config["https"]["key"]),
                    cert: this.fs.readFileSync(this.config["https"]["cert"])
                };
                server = this.http.createServer(options, this.resBody()).listen(port, this.listenPort(port));
            } else {
                server = this.http.createServer(this.resBody()).listen(port, this.listenPort(port));
            }
            this.io = require('socket.io').listen(server, {
                pingTimeout: 60000,
                pingInterval: 25000
            });
            this.io.adapter(this.adapter({
                host: this.config['redis'].host,
                port: this.config['redis'].port,
                auth_pass: this.config['redis'].password
            }));
            if (isfork) {
                process.on('message', function (message, connection) {
                    if (message !== 'sticky-session') {
                        return;
                    }
                    server.emit('connection', connection);
                    connection.resume();
                });
            }
            this.event.socketUtil(this.io, this.client, domain, this.config);
        } else {
            this.logger.error("start params error.please check config.js params");
            return null;
        }
    }

};
