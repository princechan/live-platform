var config = require('./modules/config-a03'),
    servers = require('./modules/server.js'),
    log4js = require('log4js');

process.env.port = process.env.port || (config.port + parseInt(process.env.NODE_APP_INSTANCE || 0));
console.log("port:" + process.env.port);
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
log4js.configure(config["configure"]);
servers.logger = log4js.getLogger("INFO");
servers.create(config, process.env.port);
