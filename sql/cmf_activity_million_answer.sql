/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-21 17:58:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_activity_million_answer
-- ----------------------------
DROP TABLE IF EXISTS `cmf_activity_million_answer`;
CREATE TABLE `cmf_activity_million_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `name` varchar(30) NOT NULL COMMENT '活动名称',
  `start_at` int(11) NOT NULL COMMENT '活动开始时间',
  `question_list_id` int(11) NOT NULL COMMENT '关联题库ID',
  `base_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '奖金数额',
  `extra_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '加奖总额',
  `ranking_bonus_amount` varchar(300) DEFAULT NULL COMMENT '排名奖金(多个奖金数值集合(多个奖金数值间以英文逗号间隔存储))',
  `desct` text COMMENT '活动描述',
  `remind` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否启用预约提醒？是：1；否：0  默认0',
  `live_room_id` int(11) NOT NULL COMMENT '直播间ID',
  `location_room_id` varchar(1000) DEFAULT NULL COMMENT '发布位置(输入房间ID，多个ID用英文半角逗号隔开)',
  `countdown_at` int(11) NOT NULL COMMENT '活动准备倒计时',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '活动发布状态?已发布：1；未发布0；',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '活动进行状态？活动开始：1；答题准备：2；开始答题：3；关闭活动：4.',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '活动名称',
  KEY `start_at` (`start_at`) USING BTREE,
  KEY `question_list_id` (`question_list_id`) USING BTREE COMMENT '关联题库ID',
  KEY `live_room_id` (`live_room_id`) USING BTREE COMMENT '直播间ID',
  KEY `published` (`published`) USING BTREE COMMENT '活动发布状态?已发布：1；未发布0；',
  KEY `status` (`status`) USING BTREE COMMENT '活动进行状态？活动开始：1；答题准备：2；开始答题：3；关闭活动：4.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动百万答题活动表 @author Prince.S';
