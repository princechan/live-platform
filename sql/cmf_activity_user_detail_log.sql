/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-22 13:19:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_activity_user_detail_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_activity_user_detail_log`;
CREATE TABLE `cmf_activity_user_detail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `activity_id` int(10) unsigned NOT NULL COMMENT '活动ID',
  `live_room_id` int(10) unsigned NOT NULL COMMENT '直播房间ID',
  `question_name` varchar(500) NOT NULL COMMENT '题目名称',
  `right_answer` varchar(30) NOT NULL COMMENT '正确答案',
  `uid` int(10) unsigned NOT NULL,
  `u_answer` varchar(30) NOT NULL COMMENT '玩家答案',
  `u_answer_at` int(11) NOT NULL COMMENT '玩家答题时间',
  `u_answer_used_at` float NOT NULL COMMENT '玩家答题耗时',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='玩家活动详情记录日志表 @author Prince.S';
