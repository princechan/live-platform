CREATE TABLE `cmf_home_recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) DEFAULT 0 COMMENT '类型（0直播，1默认图片）',
  `show_id` int(11) DEFAULT 0 COMMENT '直播房间',
  `show_cover` varchar(500) NOT NULL DEFAULT '' COMMENT '直播封面图',
  `default_cover` varchar(500) NOT NULL DEFAULT '' COMMENT '默认封面图图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `cmf_announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '' COMMENT '名称',
  `link` varchar(500) DEFAULT '0' COMMENT '链接',
  `state` tinyint(1) DEFAULT 0 COMMENT '当前游戏状态（0生效，1下线）',
  `last_edit_uid` int(11) NOT NULL DEFAULT 0 COMMENT '最后一次修改用户',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '修改时间',
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


