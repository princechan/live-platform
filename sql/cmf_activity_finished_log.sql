/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-21 17:58:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_activity_finished_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_activity_finished_log`;
CREATE TABLE `cmf_activity_finished_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键自增长',
  `activity_id` int(11) NOT NULL COMMENT '关联活动ID',
  `join_total_num` int(11) NOT NULL COMMENT '总参加人数',
  `issue_amount` decimal(20,2) NOT NULL COMMENT '分配金额',
  `restraint_extend` varchar(255) DEFAULT NULL COMMENT '匹配条件(限制)(json格式化存储)',
  `created_at` int(11) NOT NULL COMMENT '创建者',
  `updated_at` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`) USING BTREE COMMENT '关联活动ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动结束日志表 @Author Prince.S';
