/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-22 13:51:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_question_relation
-- ----------------------------
DROP TABLE IF EXISTS `cmf_question_relation`;
CREATE TABLE `cmf_question_relation` (
  `question_list_id` int(10) unsigned NOT NULL COMMENT '题库ID',
  `question_id` int(10) unsigned NOT NULL COMMENT '题目ID',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL COMMENT '更改时间',
  KEY `question_list_id` (`question_list_id`) USING BTREE COMMENT '题库ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='题库题目关联表 @author Prince.S';
