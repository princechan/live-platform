/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.20-log : Database - phonelive
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phonelive` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `phonelive`;

/*Table structure for table `cmf_gift` */

DROP TABLE IF EXISTS `cmf_gift`;

CREATE TABLE `cmf_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1',
  `sid` int(11) DEFAULT '0',
  `giftname` varchar(50) DEFAULT '',
  `needcoin` decimal(11,2) DEFAULT '0.00',
  `gifticon_mini` varchar(255) DEFAULT '',
  `gifticon` varchar(255) DEFAULT '',
  `orderno` int(3) DEFAULT '0',
  `is_win_gift` tinyint(1) DEFAULT '0' COMMENT '是否为中奖礼物 0否 1是',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Data for the table `cmf_gift` */

insert  into `cmf_gift`(`id`,`type`,`sid`,`giftname`,`needcoin`,`gifticon_mini`,`gifticon`,`orderno`,`is_win_gift`,`addtime`) values 
(2,1,0,'红酒',8.00,'','/data/upload/20170417/58f43ada7ed6b.png',3,0,1458986319),
(3,1,0,'吻',0.20,'','/data/upload/20170417/58f43ab64e7be.png',0,0,1458986352),
(4,1,0,'一枝花',0.10,'','/data/upload/20170417/58f43a9ca4188.png',0,0,1458986374),
(6,1,0,'打火机',10.00,'','/data/upload/20170417/58f43a837c056.png',0,0,1458986425),
(8,1,0,'纸飞机',0.10,'','/data/upload/20170417/58f43a6c31a00.png',0,0,1458986485),
(9,0,0,'跑车',300.00,'','/data/upload/20170417/58f43a52efd8e.png',0,0,1458986509),
(19,0,0,'私人飞机',300.00,'','/data/upload/20170417/58f43a3b15ab1.png',0,0,1459210850),
(21,0,0,'游轮',1314.00,'','/data/upload/20170417/58f43a21cd847.png',0,0,1461550830),
(22,0,0,'烟花',666.00,'','/data/upload/20170417/58f439f3baec1.png',0,0,1461551137),
(23,1,0,'黄瓜',8.00,'','/data/upload/20170417/58f439d987b8e.png',0,0,1464137997),
(24,1,0,'茄子',0.10,'','/data/upload/20170417/58f439aaa16e1.png',0,0,1464139982),
(25,1,0,'香蕉',0.10,'','/data/upload/20170417/58f4398d70e60.png',0,0,1464142769),
(26,1,0,'棒棒糖',1.00,'','/data/upload/20170417/58f439631da93.png',0,0,1464142813),
(27,1,0,'啤酒',5.00,'','/data/upload/20170417/58f4394ad290f.png',0,0,1464142983),
(28,1,0,'气球',3.00,'','/data/upload/20170417/58f43926179c1.png',0,0,1464143007),
(29,0,0,'点赞',1.00,'','/data/upload/20170417/58f43903ac807.png',0,0,1464143028),
(30,1,0,'百合',3.00,'','/data/upload/20170417/58f438e929c8f.png',0,0,1464143324),
(31,1,0,'玫瑰花',2.00,'','/data/upload/20170417/58f438d06cff0.png',0,0,1464143342),
(32,1,0,'单身狗',11.00,'','/data/upload/20170417/58f438b581615.png',0,0,1464143356),
(33,1,0,'鼓掌',1.00,'','/data/upload/20170417/58f43895582bb.png',0,0,1464143371),
(34,1,0,'爱您',1.00,'','/data/upload/20170417/58f438735fc64.png',0,0,1464143597),
(35,1,0,'抱抱',2.00,'','/data/upload/20170417/58f4384a1f0e8.png',0,0,1464143619),
(36,1,0,'红包',0.60,'','/data/upload/20170417/58f437f69ee33.png',0,0,1464143639),
(37,1,0,'么么哒',0.70,'','/data/upload/20170417/58f437db95c2f.png',0,0,1464143661),
(38,1,0,'萌萌哒',0.50,'','/data/upload/20170417/58f437be8eb6f.png',0,0,1464143675),
(39,1,0,'棉花糖',0.50,'','/data/upload/20170417/58f4379ec12e2.png',0,0,1464143742),
(40,1,0,'亲爱的',0.10,'','/data/upload/20170417/58f4377748e5f.jpg',0,1,1464143763);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
