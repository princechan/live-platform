/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.20-log : Database - phonelive
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phonelive` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `phonelive`;

/*Table structure for table `cmf_ads_sort` */

DROP TABLE IF EXISTS `cmf_ads_sort`;

CREATE TABLE `cmf_ads_sort` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(20) DEFAULT '',
  `orderno` int(3) DEFAULT '0',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_name` (`sortname`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `cmf_ads_sort` */

insert  into `cmf_ads_sort`(`id`,`sortname`,`orderno`,`addtime`) values 
(1,'首页右边栏广告',0,1515467189);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
