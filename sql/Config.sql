alter table cmf_config add limit_talk_type TINYINT(3) not null default 0  COMMENT '禁止游客发言类型（0禁止发言，1限制发言）';
alter table cmf_config add limit_talk_num int(11) not null default 0  COMMENT '限制发言多少条/天';
alter table cmf_config add register_pop_ups_frequency int(11) not null default 0 COMMENT '注册弹窗频率/分钟';
