/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-22 14:28:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_activity_restraint
-- ----------------------------
DROP TABLE IF EXISTS `cmf_activity_restraint`;
CREATE TABLE `cmf_activity_restraint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键ID',
  `activity_id` int(11) NOT NULL COMMENT '活动关联ID',
  `name` varchar(30) NOT NULL COMMENT '限制条件名',
  `end_at` int(11) NOT NULL COMMENT '限制条件时间',
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '限制金额数',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动限制表 @Author Prince.S';
