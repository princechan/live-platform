/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:12:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_group
-- ----------------------------
DROP TABLE IF EXISTS `tb_group`;
CREATE TABLE `tb_group` (
  `id` bigint(30) NOT NULL DEFAULT '0' COMMENT '群号',
  `group_name` varchar(63) NOT NULL DEFAULT '' COMMENT '群名称',
  `dest` varchar(256) NOT NULL DEFAULT '' COMMENT '描述',
  `number` int(10) NOT NULL DEFAULT '0' COMMENT '人数',
  `approval` tinyint(1) NOT NULL DEFAULT '1' COMMENT '-1无需验证 1需要验证',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 2全体禁言',
  `belong` bigint(20) NOT NULL DEFAULT '0' COMMENT '群主',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='群组表';
