/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:13:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_my_friend
-- ----------------------------
DROP TABLE IF EXISTS `tb_my_friend`;
CREATE TABLE `tb_my_friend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `my_group_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '我的好友分组ID',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '好友id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='我的分组下的好友列表';
