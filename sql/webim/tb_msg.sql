/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:13:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_msg
-- ----------------------------
DROP TABLE IF EXISTS `tb_msg`;
CREATE TABLE `tb_msg` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `msg_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为请求添加用户2为系统消息（添加好友）3为请求加群 4为系统消息（添加群） 5 全体会员消息',
  `from` bigint(20) DEFAULT '0' COMMENT '消息发送者 0表示为系统消息',
  `to` bigint(20) DEFAULT '0' COMMENT '消息接收者 0表示全体会员',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1未读 2同意 3拒绝 4同意且返回消息已读 5拒绝且返回消息已读 6全体消息已读',
  `remark` varchar(128) NOT NULL DEFAULT '' COMMENT '附加消息',
  `send_time` int(11) NOT NULL DEFAULT '0' COMMENT '发送消息时间',
  `read_time` int(11) NOT NULL DEFAULT '0' COMMENT '读消息时间',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `admin_group` bigint(20) NOT NULL DEFAULT '0' COMMENT '接收消息的管理员',
  `handle` bigint(20) NOT NULL DEFAULT '0' COMMENT '处理该请求的管理员id',
  `my_group_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '好友分组',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='消息通知记录表';
