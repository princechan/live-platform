/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:12:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_chatlog
-- ----------------------------
DROP TABLE IF EXISTS `tb_chatlog`;
CREATE TABLE `tb_chatlog` (
  `id` bigint(30) NOT NULL AUTO_INCREMENT,
  `from` bigint(20) NOT NULL DEFAULT '0' COMMENT '谁发的',
  `to` bigint(20) NOT NULL DEFAULT '0' COMMENT '发给谁',
  `content` varchar(1024) NOT NULL DEFAULT '' COMMENT '发送内容',
  `send_time` int(11) NOT NULL DEFAULT '0' COMMENT '发送时间',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '收发关系1:friend;2:group;3:chatroom',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 可以正常访问 2禁止访问',
  PRIMARY KEY (`id`,`from`,`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天内容表';
