/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:13:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_skin
-- ----------------------------
DROP TABLE IF EXISTS `tb_skin`;
CREATE TABLE `tb_skin` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `url` varchar(32) NOT NULL DEFAULT '' COMMENT '皮肤图片路径',
  `is_user_upload` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 用户自定义皮肤 0默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='我的皮肤';
