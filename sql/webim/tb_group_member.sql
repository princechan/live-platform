/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:12:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_group_member
-- ----------------------------
DROP TABLE IF EXISTS `tb_group_member`;
CREATE TABLE `tb_group_member` (
  `id` bigint(30) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(30) NOT NULL DEFAULT '0' COMMENT '群id',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 2为该群黑名单',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '加群时间',
  `type` tinyint(1) NOT NULL DEFAULT '3' COMMENT '1群主 2 管理员 3会员',
  `gag_time` int(11) NOT NULL DEFAULT '0' COMMENT '禁言到某个时间',
  `nick_name` varchar(128) NOT NULL DEFAULT '' COMMENT '群员的群昵称',
  PRIMARY KEY (`id`,`group_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='群员表';
