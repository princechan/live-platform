/*
Navicat MySQL Data Transfer

Source Server         : 10.71.42.70
Source Server Version : 50720
Source Host           : 10.71.42.70:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-28 18:13:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_my_group
-- ----------------------------
DROP TABLE IF EXISTS `tb_my_group`;
CREATE TABLE `tb_my_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `my_group_name` varchar(128) DEFAULT '' COMMENT '分组名称',
  `weight` tinyint(2) DEFAULT '1' COMMENT '好友分组的排列顺序 越小越靠前',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='我的好友分组';
