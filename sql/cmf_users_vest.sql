/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.20-log : Database - phonelive
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phonelive` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `phonelive`;

/*Table structure for table `cmf_users_vest` */

DROP TABLE IF EXISTS `cmf_users_vest`;

CREATE TABLE `cmf_users_vest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vest_name` varchar(20) DEFAULT '' COMMENT '马甲名称',
  `vest_man_url` varchar(250) DEFAULT NULL COMMENT '男性马甲图片',
  `vest_woman_url` varchar(250) DEFAULT NULL COMMENT '女性马甲图片',
  `away_user` tinyint(1) DEFAULT '0' COMMENT '踢人',
  `away_admin` tinyint(1) DEFAULT '0' COMMENT '踢管理员',
  `gap` tinyint(1) DEFAULT '0' COMMENT '禁止说话',
  `gap_all` tinyint(1) DEFAULT '0' COMMENT '全频道禁言',
  `private_chat` tinyint(1) DEFAULT '0' COMMENT '私聊',
  `set_admin` tinyint(1) DEFAULT '0' COMMENT '设置管理',
  `limited_speak` tinyint(1) NOT NULL DEFAULT '0' COMMENT '受限发言',
  `live_video` tinyint(1) NOT NULL DEFAULT '0' COMMENT '视频直播',
  `live_voice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '语音直播',
  `private_chat_permit` varchar(255) NOT NULL DEFAULT '' COMMENT '聊天权限 （游客-1）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `cmf_users_vest` */

insert  into `cmf_users_vest`(`id`,`vest_name`,`vest_man_url`,`vest_woman_url`,`away_user`,`away_admin`,`gap`,`gap_all`,`private_chat`,`set_admin`,`limited_speak`,`live_video`,`live_voice`,`private_chat_permit`) values 
(1,'白马','/data/upload/20170629/59549fa0549f9.png','/data/upload/20170629/59549faedf7cb.png',0,0,0,0,1,0,0,0,0,'-1,1,6'),
(2,'绿马','/data/upload/20170629/5954a27710593.png','/data/upload/20170629/5954a2846344b.png',0,0,0,0,0,0,0,0,0,''),
(3,'蓝马','/data/upload/20170629/5954a29f53b6f.png','/data/upload/20170629/5954a2abceb5c.png',0,0,0,0,0,0,0,0,0,''),
(4,'黄马','/data/upload/20170629/5954a4117c841.png','/data/upload/20170629/5954a41dc0ecf.png',0,0,0,0,0,0,0,0,0,''),
(5,'橙马','/data/upload/20170629/5954a42d329c8.png','/data/upload/20170629/5954a4396b035.png',1,0,1,1,1,0,0,0,0,''),
(6,'紫马','/data/upload/20170629/5954a44869f62.png','/data/upload/20170629/5954a4548b462.png',1,1,1,1,1,0,0,0,0,''),
(7,'黑马','/data/upload/20170629/5954a476667bd.png','/data/upload/20170629/5954a48333ec0.png',1,1,1,1,1,1,0,0,0,'');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
