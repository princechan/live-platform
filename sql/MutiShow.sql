CREATE TABLE `cmf_muti_show` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '连麦房间ID',
  `live_name` varchar(150) NOT NULL COMMENT '连麦房间名称',
  `avatar` varchar(255) DEFAULT '' COMMENT '展示头像',
  `avatar_thumb` varchar(255) DEFAULT '' COMMENT '展示头像缩略图',
  `focus` varchar(255) DEFAULT NULL COMMENT '焦点图',
  `focus_thumb` varchar(255) DEFAULT NULL COMMENT '焦点图缩略图',
  `code` varchar(100) DEFAULT '' COMMENT '邀请码',
  `devide_family` varchar(100) DEFAULT '' COMMENT '家属邀请码',
  `status` tinyint(1) DEFAULT '0' COMMENT 'status (0 关闭,1开启)',
  `remark` varchar(1024) DEFAULT NULL COMMENT '描述',
  `stream` varchar(100) DEFAULT NULL COMMENT '流ID',
  `pull` varchar(1024) DEFAULT NULL COMMENT '播流地址',
  `last_edit_uid` int(11) NOT NULL DEFAULT 0 COMMENT '最后一次修改用户',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `cmf_muti_showmanager` (
  `mutiId` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `vest_type` varchar(20) DEFAULT NULL COMMENT '马甲类型（6主播，4麦手，0为管理员）',
  `sort_no` int(4) DEFAULT NULL COMMENT '序号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

