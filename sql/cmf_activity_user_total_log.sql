/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-22 13:19:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_activity_user_total_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_activity_user_total_log`;
CREATE TABLE `cmf_activity_user_total_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `activity_id` int(10) unsigned NOT NULL COMMENT '活动ID',
  `live_room_id` int(11) NOT NULL COMMENT '直播房间号',
  `uid` int(10) unsigned NOT NULL COMMENT '玩家ID',
  `base_bonus_amount` decimal(20,2) NOT NULL COMMENT '平分奖金',
  `extra_bonus_amount` decimal(20,2) NOT NULL COMMENT '加奖总额',
  `ranking_bonus_amount` decimal(20,2) NOT NULL COMMENT '排名奖金',
  `total_bonus_amount` decimal(20,2) NOT NULL COMMENT '总奖金数额',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='玩家活动奖金记录日志表 @author Prince.S';
