/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-21 17:58:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_question
-- ----------------------------
DROP TABLE IF EXISTS `cmf_question`;
CREATE TABLE `cmf_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题目ID',
  `question_list_id` int(11) NOT NULL COMMENT '题库ID',
  `type` tinyint(1) unsigned NOT NULL COMMENT '题目类型	1:3-1;2:4-1',
  `name` varchar(50) NOT NULL COMMENT '题目',
  `choose_answer` text NOT NULL COMMENT '多个答案集合(多个选择答案间以英文逗号间隔存储)',
  `right_answer` varchar(30) NOT NULL COMMENT '正确答案',
  `admin_id` int(11) NOT NULL COMMENT '创建者ID',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '题目',
  KEY `type` (`type`) USING BTREE COMMENT '题目类型	1:3-1;2:4-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题题目 @Author Prince.S';
