/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : phonelive

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-03-21 17:59:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_question_list
-- ----------------------------
DROP TABLE IF EXISTS `cmf_question_list`;
CREATE TABLE `cmf_question_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题库编号',
  `name` varchar(50) NOT NULL COMMENT '题库名称',
  `admin_id` int(11) NOT NULL COMMENT '创建者ID',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '题库名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='题库表 @author  Prince.S';
