alter table cmf_users_vest add limited_speak TINYINT(1) not null default 0  COMMENT '受限发言';
alter table cmf_users_vest add live_video TINYINT(1) not null default 0  COMMENT '视频直播';
alter table cmf_users_vest add live_voice TINYINT(1) not null default 0  COMMENT '语音直播';
alter table cmf_users_vest add private_chat_permit VARCHAR (255) not null default ''  COMMENT '聊天权限 （游客-1）';