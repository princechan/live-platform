<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Activity\Activity;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Live;
use Common\Lib\Auth\User;
use Common\Lib\Live\MultiLive;
use Common\Lib\LiveVideo;

/**
 * 直播页面
 */
class ShowController extends HomebaseController
{
    public function agora()
    {
        $this->display();
    }

    /**
     * 直播首页
     */
    public function index()
    {
        //主播ID
        $anchorid = trim(I('roomnum'));

        if (MultiLive::getInstance()->routeIsMultiLive($anchorid)) {
            return $this->multi();
        }

        $uid = $this->getUid();
        $config = $this->config;
        $getConfigPub = Func::getPublicConfig();
        if ($getConfigPub['maintain_switch'] == 1) {
            $this->assign('jumpUrl', __APP__);
            $this->error(nl2br($getConfigPub['maintain_tips']));
        }

        $getConfigPri = Func::getPrivateConfig();
        $config['daily_send_msg_num'] = Live::getInstance()->getDailySendMsgNum(0, $anchorid, $this->getUid());
        $this->assign("configj", json_encode($config));
        $this->assign("ConfigPub", json_encode($getConfigPub));
        $this->assign("getConfigPri", $getConfigPri);
        $this->assign("getConfigPub", $getConfigPub);
        $this->assign(User::VISITOR_COOKIE, User::getInstance()->getVisitorCookie());

        //读取礼物组
        $giftGroupLists = M("gift_group")->order("orderno")->select();
        $this->assign("giftGroupLists", $giftGroupLists);

        $User = M('users');
        $Gift = M('gift');

        $nowtime = time();
        $anchorinfo = User::getInstance()->getUserInfo($anchorid);
        if (!$anchorinfo) {
            $this->error("主播不存在");
        }

        $anchorinfo['level'] = User::getInstance()->getLevel($anchorinfo['consumption']);
        $anchorinfo['follows'] = User::getInstance()->getFollownums($anchorinfo['id']);
        $anchorinfo['fans'] = User::getInstance()->getFansnums($anchorinfo['id']);

        //设置stream
        if ($uid == $anchorinfo['id']) {
            $stream = $anchorid . "_" . time();
        } else {
            $stream = $anchorid . "_" . $anchorid;
            $userLive = M('users_live')->where(['uid' => $anchorid])->find();
            if ($userLive && $userLive['stream']) {
                $stream = $userLive['stream'];
            }
            Live::getInstance()->setRoomLivedNum(0, $anchorid);
        }

        //insert to online list
        Live::getInstance()->addOnlineUser(0, $anchorid, $uid, $anchorinfo);

        $anchorinfo['stream'] = $stream;
        $this->assign("anchorinfo", $anchorinfo);
        $this->assign("anchorinfoj", json_encode($anchorinfo));

        $liveinfo = M('users_live')->where("uid='{$anchorinfo['id']}' and islive=1")->order("islive desc")->limit(1)->find();
        if ($liveinfo) {
            $liveinfo['pull'] = Live::getInstance()->getStream('rtmp', $liveinfo['stream'], 0);
            $liveinfo['video_url'] = Live::getInstance()->getM3u8Stream($liveinfo['stream']);
            $liveinfo['stream'] = $stream;
            $liveinfo['manager'] = Live::getInstance()->getManager($anchorid);
            $liveinfo['ami_anchor'] = in_array(User::getInstance()->getUserIdentity($uid, $anchorid, 0), [40, 50, 60]);
        }

        $this->assign("liveinfo", $liveinfo);

        $liveinfoj = $liveinfo;
        if ($anchorid != $uid) {
            unset($liveinfoj['type_val']);
        }
        $this->assign('liveinfoj', json_encode($liveinfoj));

        $redis = CRedis::getInstance();
        //是否踢出房间  特殊处理ID
        if ($this->user['id']) {
            $kickTime = Live::getInstance()->getKick(0, $anchorid, $uid);
            $time = time();
            if ($kickTime > $time) {
                $surplus = $kickTime - $time;
                $this->assign('jumpUrl', __APP__);
                $this->error('您已被踢出房间，剩余' . $surplus . '秒');
            } else {
                Live::getInstance()->delKick(0, $anchorid, $uid);
            }
        }

        //用户身份
        $identity = 10;
        if ($uid > 0) {
            //判断该用户是否被禁用此IP
            //获取当前的IP
            $currentIP = $_SERVER['REMOTE_ADDR'];
            $Model = M("users_disable_ips");
            $count = $Model->where("uid='{$uid}'")->count();
            if ($count > 0) {
                $lists = $Model->where("uid='{$uid}'")->select();
                $isLimitIP = 0;
                foreach ($lists as $k => $v) {
                    if ($currentIP == $v['begin_ip']) {//符合单独IP被限制
                        $isLimitIP = 1;
                        break;
                    }

                    $info = $Model->where("uid='{$uid}' and inet_aton('{$currentIP}')  between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")->count();

                    $count1 = intval($info);
                    if ($count1 > 0) {
                        $isLimitIP = 1;
                        break;
                    }
                }

                if ($isLimitIP == 1) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error('您的IP已经被封禁,请联系管理员解封');
                }
            }

            //判断该房间是否被禁言
            $this->assign("ischannelShutUp", (int)Live::getInstance()->isChannelShutUp(0, $anchorid));

            //身份判断
            $identity = User::getInstance()->getUserIdentity($uid, $anchorid);
            $isBan = User::getInstance()->isBan($anchorinfo['id']);
            if ($isBan == 0) {
                $this->assign('jumpUrl', __APP__);
                $this->error('该主播已经被禁止直播');
            }
            $isBan = User::getInstance()->isBan($uid);
            if ($isBan == 0) {
                $this->assign('jumpUrl', __APP__);
                $this->error('你的账号已经被禁用');
            }

            //进入房间设置redis
            $userinfo = $User->where(['id' => $uid])->field("id,issuper")->find();
            if ($userinfo['issuper'] == 1) {
                $redis->hset('super', $userinfo['id'], '1');
            } else {
                $redis->hDel('super', $userinfo['id']);
            }
        }

        $this->assign('identity', $identity);

        //是否关注
        $isattention = User::getInstance()->isAttention($uid, $anchorinfo['id']);
        $this->assign("isattention", $isattention);
        $attention_type = $isattention ? "已关注" : "+关注";
        $this->assign("attention_type", $attention_type);
        $this->assign("anchorid", $anchorid);
        //礼物信息
        $giftinfo = $Gift->field("*")->order("orderno asc")->select();

        $this->assign("giftinfo", $giftinfo);
        $giftinfoj = [];
        foreach ($giftinfo as $k => $v) {
            $giftinfoj[$v['id']] = $v;
        }
        $this->assign("giftinfoj", json_encode($giftinfoj));

        //读取马甲信息
        $vests = Live::getInstance()->getVestLists();
        $this->assign('vests', $vests);

        $vestsj = [];
        foreach ($vests as $k => $v) {
            $vestsj[$v['id']] = $v;
        }

        $this->assign("vestsj", json_encode($vestsj));

        //读取后台配置的直播间轮播图效果
        $slideLists = M("slide")->where("slide_cid=2 and slide_status=1")->select();
        $this->assign('slidelists', $slideLists);

        $configpri = Func::getPrivateConfig();

        //答题活动
        $activityJson = Activity::getInstance()->getActivityBaseInfo(Activity::getInstance()->getActivityIdInLive(0, $anchorid));
        $activityJson['current_question'] = Activity::getInstance()->getCurrentQuestionByPlayer($activityJson['id']);
        $this->assign('activityJson', json_encode((object)$activityJson));

        //播流还是推流
        $isplay = 0;
        //播流(我是主播)
        if ($uid == $anchorinfo['id']) {
            if ($configpri['auth_islimit'] == 1) {
                $auth = M("users_auth")->field("status")->where("uid='{$uid}'")->find();
                if (!$auth || $auth['status'] != 1) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error("请先进行身份认证");
                }
            }

            if ($configpri['level_islimit'] == 1) {
                if ($anchorinfo['level'] < $configpri['level_limit']) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error('等级小于' . $configpri['level_limit'] . '级，不能直播');
                }
            }

            $token = User::getInstance()->getUserToken($uid);
            $this->assign('token', $token);
            //流地址
            $push = Live::getInstance()->getStream('rtmp', $stream, 1);
            $this->assign('push', $push);
            $this->assign('pushj', json_encode($push));
            $isplay = 1;
            $this->assign('isplay', $isplay);
            //M("users_connect_video")->where("liveid={$anchorinfo['id']} and (status=1 or status=2)")->setField("status",3);
            //主播礼物PK没有信息，置为空数组start
            $giftPkArr = [];
            $this->assign('giftPkInfo', json_encode($giftPkArr));
            //抢板凳没有信息，置为空数组start
            $grabBenchArr = [];
            $this->assign('grabBenchInfo', json_encode($grabBenchArr));
            //抢板凳没有信息，置为空数组
            //守护没有信息，置为空数组
            $guardInfo = [];
            $this->assign("guardInfoj", json_encode($guardInfo));
            $sendMsg_Num = '';
            $this->assign("sendMsg_Num", $sendMsg_Num);
        } else {
            $giftPkArr = [];
            $grabBenchArr = [];
            if ($uid) {
                //判断用户的马甲
                $isGapAll = 0;    //默认定义房间禁言权限为0
                $currentUserInfo = $User->where(['id' => $uid])->find();
                if ($currentUserInfo['vest_id'] > 1) {
                    //获取用户马甲身份权限
                    $currentUserVestInfo = Live::getInstance()->getUsersVest($currentUserInfo['vest_id']);
                    if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                        $isGapAll = 1;
                    }
                } else {
                    //从房间马甲列表里查询马甲
                    $currentUserVestInfo = Live::getInstance()->getUserVest($uid, 0, $anchorinfo['id']);
                    if ($currentUserVestInfo) {
                        $currentUserVestInfo = Live::getInstance()->getUsersVest($currentUserVestInfo['vestid']);
                        if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                            $isGapAll = 1;
                        }
                    }
                }

                $this->assign("isGapAll", $isGapAll);
                $redis = CRedis::getInstance();
                /*******获取礼物PK的redis信息start********/
                $giftPkInfo = $redis->get("giftPK_" . $anchorid);
                if ($giftPkInfo) {
                    $giftPkArr = json_decode($giftPkInfo, true);//转换为数组
                    if ($giftPkArr['isEnd'] == 0) {
                        //到PK结束的剩余时间
                        $effectiveTime = $giftPkArr['lastStopTime'] - time();
                        if ($effectiveTime <= 0) {//主播未正常停止游戏造成时间失效
                            $redis->delete("giftPK_" . $anchorid);
                        } else {
                            $giftPkArr['effectiveTime'] = $effectiveTime;
                            $guestUserInfo = User::getInstance()->getUserInfo($giftPkArr['guestID']);
                            $giftPkArr['guestAvatar'] = $guestUserInfo['avatar'];//客队头像
                            $giftPkArr['guestName'] = $guestUserInfo['user_nicename'];//客队昵称
                            $masterUserInfo = User::getInstance()->getUserInfo($giftPkArr['uid']);
                            $giftPkArr['masterAvatar'] = $masterUserInfo['avatar'];//主队头像
                            $giftPkArr['masterName'] = $masterUserInfo['user_nicename'];//主队昵称
                            $guestGiftInfo = Live::getInstance()->getGiftInfo($giftPkArr['guestGiftID']);
                            $giftPkArr['guestGiftImg'] = $guestGiftInfo['gifticon'];//客队礼物图片
                            $masterGiftInfo = Live::getInstance()->getGiftInfo($giftPkArr['masterGiftID']);
                            $giftPkArr['masterGiftImg'] = $masterGiftInfo['gifticon'];//主队礼物图片
                        }
                    }
                } else {
                    $giftPkArr = [];
                }

                /*******获取礼物PK的redis信息end********/
                /***********获取抢板凳游戏的redis信息start************/
                $grabBenchInfo = $redis->get("grabbench_" . $anchorid);
                if ($grabBenchInfo) {
                    $grabBenchArr = json_decode($grabBenchInfo, true);//解析成数组
                    if ($grabBenchArr['isEnd'] == 1) {//活动已经结束
                        $redis->delete("grabbench_" . $anchorid);//将redis删除
                        $info['grabbench'] = [];
                    } else {
                        $grabbenchID = $grabBenchArr['grabbenchID'];
                        //获取后台配置的连续点击时间间隔
                        $hits_space = $configpri['grabbench_hits_space'];
                        $effectiveTime = $grabBenchArr['effectiveTime'] - time();//到活动结束的剩余时间
                        if ($effectiveTime <= 0) {
                            $redis->delete("grabbench_" . $anchorid);
                            $grabBenchArr = [];
                        } else {
                            $grabBenchArr['hits_space'] = $hits_space;
                            $grabBenchArr['effectiveTime'] = strval($effectiveTime);
                        }
                    }
                } else {
                    $grabBenchArr = [];
                }

                /***********获取抢板凳游戏的redis信息end************/
                $userinfo = User::getInstance()->getUserPrivateInfo($uid);
                if ($userinfo['iswhite'] == 1) {//用户在白名单中
                    $sendMsg_Num = "";
                } else {
                    if ($uid) {
                        //判断该用户是否守护主播
                        $guardInfo = M("users_guard_lists")->where("uid='{$uid}' and liveuid={$anchorid} and effectivetime>{$nowtime}")->find();
                    }

                    if ($guardInfo) {   //守护
                        $sendMsg_Num = $config['guard_digits_num'];
                    } else if ($userinfo['chat_num'] > 0) {
                        $sendMsg_Num = $userinfo['chat_num'];
                    } else {
                        $info = M('users_live')->where("islive=1 and uid={$anchorid}")->order("starttime desc")->find();
                        if ($info['chat_num']) {
                            $sendMsg_Num = $info['chat_num'];
                        } else {
                            $sendMsg_Num = 30;
                        }
                    }
                }
            }

            $this->assign('giftPkInfo', json_encode($giftPkArr));
            $this->assign('grabBenchInfo', json_encode($grabBenchArr));
            $this->assign('isplay', $isplay);

            //流地址
            $push = Live::getInstance()->getStream('rtmp', $stream, 1);
            $this->assign('push', $push);
            $this->assign('pushj', json_encode($push));
            $this->assign('sendMsg_Num', $sendMsg_Num);
            $this->assign('guardInfoj', json_encode($guardInfo));
        }

        $this->display('index');
    }

    /**
     * 多人直播
     * /0+ID
     */
    private function multi()
    {
        //房间ID
        $anchorid = str_replace(MultiLive::ROUTE_PREFIX, '', trim(I('roomnum')));

        $uid = $this->getUid();
        $config = $this->config;
        $getConfigPub = Func::getPublicConfig();
        $getConfigPri = Func::getPrivateConfig();

        $config['daily_send_msg_num'] = Live::getInstance()->getDailySendMsgNum(1, $anchorid, $this->getUid());
        $this->assign("configj", json_encode($config));
        $this->assign("ConfigPub", json_encode($getConfigPub));
        $this->assign("getConfigPri", $getConfigPri);
        $this->assign("getConfigPub", $getConfigPub);
        $this->assign(User::VISITOR_COOKIE, User::getInstance()->getVisitorCookie());

        $currentUserInfo = User::getInstance()->getUserInfo($uid);
        //insert to online list
        if ($currentUserInfo) {
            Live::getInstance()->addOnlineUser(1, $anchorid, $uid, $currentUserInfo);
        }

        //读取礼物组
        $giftGroupLists = M("gift_group")->order("orderno")->select();
        $this->assign("giftGroupLists", $giftGroupLists);

        $nowtime = time();
        $anchorinfo = MultiLive::getInstance()->getRoomInfo($anchorid, $uid);
        if (!$anchorinfo) {
            $this->error('直播间不存在');
        }

        Live::getInstance()->setRoomLivedNum(1, $anchorid);

        $this->assign('multiLive', $anchorinfo);

        $multiLiveJ = $anchorinfo;
        if (!$multiLiveJ['ami_anchor']) {//非主播
            unset($multiLiveJ['room_password']);
        }
        $this->assign('multiLiveJ', json_encode($multiLiveJ));

        $anchorinfo['level'] = User::getInstance()->getLevel($anchorinfo['consumption']);
        $anchorinfo['follows'] = User::getInstance()->getFollownums($anchorinfo['id']);
        $anchorinfo['fans'] = User::getInstance()->getFansnums($anchorinfo['id']);
        $anchorinfo['user_nicename'] = $anchorinfo['live_name'];
        $this->assign("anchorinfo", $anchorinfo);
        $this->assign("anchorinfoj", json_encode($anchorinfo));
        $this->assign("liveinfo", []);

        //用户身份
        $identity = 10;
        if ($uid > 0) {
            //判断该用户是否被禁用此IP
            //获取当前的IP
            $currentIP = $_SERVER['REMOTE_ADDR'];
            $Model = M("users_disable_ips");
            $count = $Model->where("uid={$uid}")->count();
            if ($count > 0) {
                $lists = $Model->where("uid={$uid}")->select();
                $isLimitIP = 0;
                foreach ($lists as $k => $v) {
                    if ($currentIP == $v['begin_ip']) {//符合单独IP被限制
                        $isLimitIP = 1;
                        break;
                    }

                    $info = $Model->where("uid={$uid} and inet_aton('{$currentIP}')  between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")->count();

                    $count1 = intval($info);
                    if ($count1 > 0) {
                        $isLimitIP = 1;
                        break;
                    }
                }

                if ($isLimitIP == 1) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error('您的IP已经被封禁,请联系管理员解封');
                }
            }

            //是否踢出房间
            $redis = CRedis::getInstance();
            $kickTime = Live::getInstance()->getKick(1, $anchorid, $uid);
            $nowtime = time();
            if ($kickTime > $nowtime) {
                $surplus = $kickTime - $nowtime;
                $this->assign('jumpUrl', __APP__);
                $this->error('您已被踢出房间，剩余' . $surplus . '秒');
            } else {
                Live::getInstance()->delKick(1, $anchorid, $uid);
            }

            //判断该房间是否被禁言
            $this->assign("ischannelShutUp", (int)Live::getInstance()->isChannelShutUp(1, $anchorid));

            $isBan = User::getInstance()->isBan($uid);
            if ($isBan == 0) {
                $this->assign('jumpUrl', __APP__);
                $this->error('你的账号已经被禁用');
            }

            //进入房间设置redis
            $userinfo = M('users')->where(['id' => $uid])->field("id,issuper")->find();
            if ($userinfo['issuper'] == 1) {
                $redis->hset('super', $userinfo['id'], '1');
            } else {
                $redis->hDel('super', $userinfo['id']);
            }
            //身份判断
            $identity = User::getInstance()->getUserIdentity($uid, $anchorid, 1);
        }

        $this->assign('identity', $identity);

        //是否关注
        $isattention = User::getInstance()->isAttention($uid, $anchorinfo['id']);
        $this->assign("isattention", $isattention);
        $attention_type = $isattention ? "已关注" : "+关注";
        $this->assign("attention_type", $attention_type);
        $this->assign("anchorid", $anchorid);
        //礼物信息
        $giftinfo = M('gift')->order("orderno asc")->select();
        $this->assign("giftinfo", $giftinfo);
        $giftinfoj = [];
        foreach ($giftinfo as $k => $v) {
            $giftinfoj[$v['id']] = $v;
        }
        $this->assign("giftinfoj", json_encode($giftinfoj));

        //读取马甲信息
        $vests = Live::getInstance()->getVestLists();
        $this->assign('vests', $vests);

        $vestsj = [];
        foreach ($vests as $k => $v) {
            $vestsj[$v['id']] = $v;
        }

        $this->assign("vestsj", json_encode($vestsj));

        //读取后台配置的直播间轮播图效果
        $slideLists = M("slide")->where("slide_cid=2 and slide_status=1")->select();
        $this->assign('slidelists', $slideLists);

        $configpri = Func::getPrivateConfig();

        $giftPkArr = [];
        $grabBenchArr = [];
        $sendMsg_Num = '';
        $guardInfo = [];
        if ($uid) {
            //判断用户的马甲
            $isGapAll = 0;    //默认定义房间禁言权限为0
            if ($currentUserInfo['vest_id'] > 1) {
                //获取用户马甲身份权限
                $currentUserVestInfo = Live::getInstance()->getUsersVest($currentUserInfo['vest_id']);
                if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                    $isGapAll = 1;
                }
            } else {
                //从房间马甲列表里查询马甲
                $currentUserVestInfo = Live::getInstance()->getUserVest($uid, 1, $anchorinfo['id']);
                if ($currentUserVestInfo) {
                    $currentUserVestInfo = Live::getInstance()->getUsersVest($currentUserVestInfo['vestid']);
                    if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                        $isGapAll = 1;
                    }
                }
            }

            $this->assign("isGapAll", $isGapAll);
            $redis = CRedis::getInstance();
            /*******获取礼物PK的redis信息start********/
            $giftPkArr = [];

            /*******获取礼物PK的redis信息end********/
            /***********获取抢板凳游戏的redis信息start************/
            $grabBenchInfo = $redis->get("grabbench_" . $anchorid);
            if ($grabBenchInfo) {
                $grabBenchArr = json_decode($grabBenchInfo, true);//解析成数组
                if ($grabBenchArr['isEnd'] == 1) {//活动已经结束
                    $redis->delete("grabbench_" . $anchorid);//将redis删除
                    $info['grabbench'] = [];
                } else {
                    //获取后台配置的连续点击时间间隔
                    $hits_space = $configpri['grabbench_hits_space'];
                    $effectiveTime = $grabBenchArr['effectiveTime'] - time();//到活动结束的剩余时间
                    if ($effectiveTime <= 0) {
                        $redis->delete("grabbench_" . $anchorid);
                        $grabBenchArr = [];
                    } else {
                        $grabBenchArr['hits_space'] = $hits_space;
                        $grabBenchArr['effectiveTime'] = strval($effectiveTime);
                    }
                }
            } else {
                $grabBenchArr = [];
            }

            /***********获取抢板凳游戏的redis信息end************/
            $userinfo = User::getInstance()->getUserPrivateInfo($uid);
            if ($userinfo['iswhite'] == 1) {//用户在白名单中
                $sendMsg_Num = '';
            } else {
                //判断该用户是否守护主播
                $guardInfo = $uid ? M('users_guard_lists')->where("uid='{$uid}' and liveuid={$anchorid} and effectivetime>{$nowtime}")->find() : [];
                if ($guardInfo) {   //守护
                    $sendMsg_Num = $config['guard_digits_num'];
                } else if ($userinfo['chat_num'] > 0) {
                    $sendMsg_Num = $userinfo['chat_num'];
                } else {
                    $info = M('users_live')->where("islive=1 and uid={$anchorid}")->order("starttime desc")->find();
                    if ($info['chat_num']) {
                        $sendMsg_Num = $info['chat_num'];
                    } else {
                        $sendMsg_Num = 30;
                    }
                }
            }
        }

        $this->assign('giftPkInfo', json_encode($giftPkArr));
        $this->assign('grabBenchInfo', json_encode($grabBenchArr));
        $this->assign('sendMsg_Num', $sendMsg_Num);
        $this->assign('guardInfoj', json_encode($guardInfo));

        $this->display('multi');
    }

    /**
     * 二维码连接地址 value
     */
    public function qrcode()
    {
        $roomid = $_GET["roomid"];
        include 'simplewind/Lib/Util/phpqrcode.php';
        $a = new \QRcode();
        $value = "http://" . $_SERVER['SERVER_NAME'] . '/wxshare/index.php/Share/show?roomnum=' . $roomid;
        $errorCorrectionLevel = 'L';//容错级别
        $matrixPointSize = 6;//生成图片大小
        //生成二维码图片
        $basePath = 'public/images/';
        //准备好的logo图片
        $logo = $basePath . 'logo.png';
        $qrCodePath = $basePath . 'qrcode.png';
        $a->png($value, $qrCodePath, $errorCorrectionLevel, $matrixPointSize, 2);
        //已经生成的原始二维码图
        $QR = $qrCodePath;
        $QR = imagecreatefromstring(file_get_contents($QR));
        $QR_width = imagesx($QR);//二维码图片宽度
        $QR_height = imagesy($QR);//二维码图片高度
        $logo = imagecreatefromstring(file_get_contents($logo));
        $logo_width = imagesx($logo);//logo图片宽度
        $logo_height = imagesy($logo);//logo图片高度
        $logo_qr_width = $QR_width / 5;
        $scale = $logo_width / $logo_qr_width;
        $logo_qr_height = $logo_height / $scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        //重新组合图片并调整大小
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0,
            $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        //输出图片
        Header("Content-type: image/png");
        ImagePng($QR);
    }

    /**
     * 用户进入 写缓存 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前用户身份)
     */
    public function setNodeInfo()
    {
        $liveType = (int)I('live_type');
        $showid = (int)I('liveid');
        $stream = I('stream');

        if (!$showid || !in_array($liveType, [0, 1])) {
            return $this->renderJson([], 400, '参数错误');
        }

        $info = $this->user;
        $uid = $info['id'];
        $info['liveuid'] = $showid;
        $info['roomnum'] = $showid;
        $info['live_type'] = $liveType;
        $info['live_id'] = $showid;
        $info['sign'] = md5("{$liveType}:{$showid}:{$uid}");
        if ($uid > 0) {
            if ($uid == $showid) {
                $info['userType'] = 50;
            } else {
                $info['userType'] = 30;
            }
        } else {//游客
            $info['userType'] = 0;
            $info['avatar_thumb'] = $info['avatar'];
        }

        //判断该房间是否在直播
        $live = M('users_live')->where(['uid' => $showid, 'islive' => 1])->find();
        if ($live) {
            $info['stream'] = $live['stream'];
        } else {
            if ($uid == $showid) {
                $info['stream'] = $stream;
            } else {
                $info['stream'] = $showid . '_' . $showid;
            }
        }

        //游客数据只放3天
        $timeout = $uid > 0 ? null : 3 * 24 * 3600;
        CRedis::getInstance()->set($info['token'], json_encode($info), $timeout);

        $data = [
            'error' => 0,
            'userinfo' => $info,
            'iszombie' => Live::getInstance()->isZombie($showid),
        ];

        echo json_encode($data);
        die;
    }

    /**
     * 开播设置
     */
    public function createRoom()
    {
        $stream = I("stream");
        $title = I("title");
        $type = I("type");
        $type_val = I("stand");
        $thumb = I("thumb");
        $sendMsgNum = I("sendMsgNum");
        $sendMsgFrequency = I("sendMsgFrequency");

        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1002","data":"","msg":"请登录"}';
            exit;
        }

        $userinfo = M('users')->field('coin,token,expiretime,user_nicename,avatar,avatar_thumb')->where(['id' => $uid])->find();
        $getConfigPri = Func::getPrivateConfig();
        $orderid = explode("_", $stream);
        $showid = $orderid[1];
        $data = [
            'uid' => $uid,
            "avatar" => $userinfo['avatar'],
            "avatar_thumb" => $userinfo['avatar_thumb'],
            "user_nicename" => $userinfo['user_nicename'],
            "islive" => 1,
            "showid" => $showid,
            "starttime" => time(),
            "title" => $title,
            "province" => '',
            "city" => '好像在火星',
            "stream" => $stream,
            "pull" => "rtmp://" . $getConfigPri['pull_url'] . "/5showcam/" . $stream,
            "lng" => '',
            "lat" => '',
            "topicid" => '',
            "type" => $type,
            "thumb" => $thumb,
            "type_val" => $type_val,
            "ispc" => 1,
            "chat_num" => $sendMsgNum,
            "chat_frequency" => $sendMsgFrequency,
        ];

        $live = M("users_live");
        if ($live->where(['uid' => $uid])->find()) {
            $rs = $live->where(['uid' => $uid])->save($data);
        } else {
            $rs = $live->add($data);
        }

        if ($rs) {
            echo '{"state":"0","data":"","msg":""}';
        } else {
            echo '{"state":"1000","data":"","msg":"直播信息处理失败"}';
        }
        die;
    }

    /**
     * 用户列表弹出信息 TODO 临时用户没有info信息，需要处理
     * uid_admin 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前用户身份)
     * touid_admin 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前点击用户身份)
     */
    public function popupInfo()
    {
        $touid = I('touid');
        $liveId = I('liveid');
        $liveType = (int)I('live_type');

        $uid = $this->getUid();
        $info = User::getInstance()->getUserInfo($touid);
        if (!$info && User::getInstance()->isVisitor($touid)) {
            Live::getInstance()->delOnlineUser($liveType, $liveId, $touid);
        }

        if ($uid > 0 && $uid != null) {
            $isBlack = User::getInstance()->isBlack($uid, $touid);
        } else {
            $isBlack = '';
        }

        $uid_admin = User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
        $touid_admin = User::getInstance()->getUserIdentity($touid, $liveId, $liveType);

        $popupInfo = [
            "state" => "0",
            "uid_admin" => $uid_admin,
            "touid_admin" => $touid_admin,
            "info" => $info,
            "isBlack" => $isBlack,
        ];

        //获取当前用户的马甲
        $vest = Live::getInstance()->getUserVest($uid, $liveType, $liveId);
        $popupInfo['uid_vestid'] = $vest ? $vest['vestid'] : 1; //默认白马甲
        //判断后台是否设置紫马或黑马
        $userVestID = M('users')->where(['id' => $uid])->getField("vest_id");
        if ($userVestID > 1) {
            $popupInfo['uid_vestid'] = $userVestID;
        }

        //获取touid用户的马甲
        $vest = Live::getInstance()->getUserVest($touid, $liveType, $liveId);
        $popupInfo['touid_vestid'] = $vest ? $vest['vestid'] : 1; //默认白马甲
        //判断后台是否设置紫马或黑马
        $userVestID = M('users')->where(['id' => $touid])->getField('vest_id');
        if ($userVestID > 1) {
            $popupInfo['touid_vestid'] = $userVestID;
        }

        $popupInfo['private_chat'] = (int)Live::getInstance()->getPrivateChatPermission($uid, $touid, $liveType, $liveId);

        die(json_encode($popupInfo));
    }

    /**
     * 单直播数据
     */
    public function live()
    {
        $uid = I('uid');

        $liveinfo = M('users_live')->where(['uid' => $uid, 'islive' => 1])->find();
        $liveinfo['pull'] = Live::getInstance()->getStream('rtmp', $liveinfo['stream'], 0);
        $data = [
            'error' => 0,
            'data' => $liveinfo,
            'msg' => '',
        ];
        echo json_encode($data);
        die;
    }

    /**
     * 排行榜
     */
    public function rank()
    {
        $liveId = I('liveid');
        $liveType = (int)I('live_type');

        if (!$liveId || !in_array($liveType, [0, 1])) {
            return $this->renderJson([], 400, '参数错误');
        }

        $list = [];
        //本房间魅力榜
        $sql = "SELECT uid,sum(total_livecoin) as total_livecoin from cmf_users_coinrecord 
WHERE type='expend' AND liveid='{$liveId}' AND live_type='$liveType' 
GROUP BY uid ORDER BY total_livecoin DESC limit 0,10";
        $now = M('users_coinrecord')->query($sql);
        foreach ($now as $k => $v) {
            $userinfo = User::getInstance()->getUserInfo($v['uid']);
            $now[$k]['userinfo'] = $userinfo;
        }
        $list['now'] = $now;

        //全站魅力榜
        $sql = "SELECT uid,sum(total_livecoin) AS total_livecoin FROM cmf_users_coinrecord 
WHERE type='expend' AND live_type='$liveType' GROUP BY uid ORDER BY total_livecoin DESC LIMIT 0,10";
        $all = M('users_coinrecord')->query($sql);
        foreach ($all as $k => $v) {
            $userinfo = User::getInstance()->getUserInfo($v['uid']);
            $all[$k]['userinfo'] = $userinfo;
        }
        $list['all'] = $all;

        return $this->renderJson($list);
    }

    /**
     * 进入单直播间检查房间类型
     */
    public function checkLive()
    {
        $liveuid = I("liveuid");
        $stream = I("stream");

        $rs = ['code' => 0, 'msg' => '', 'info' => []];
        $uid = $this->getUserId();
        $config = $this->config;
        $rs['land'] = 0;
        $islive = M('users_live')->field('islive,type,type_val,starttime')
            ->where(['uid' => $liveuid, 'stream' => $stream])->find();
        if ($islive['type'] == 2) {//0-普通房间，1-密码房间，2-付费房间
            if ($uid > 0) {
                $rs['land'] = 0;
            } else {
                $rs['land'] = 1;
                $rs['type'] = $islive['type'];
                $rs['type_msg'] = '当前房间为付费房间，请先登陆';
                echo json_encode($rs);
                exit;
            }
        }
        if (!$islive || $islive['islive'] == 0) {
            $rs['code'] = 1005;
            $rs['msg'] = '直播已结束，请刷新';
            echo json_encode($rs);
            exit;
        } else {
            $rs['type'] = $islive['type'];
            $rs['type_msg'] = '';
            if ($islive['type'] == 1) {
                $rs['type_msg'] = $islive['type_val'];
            } else if ($islive['type'] == 2) {
                $userIdentity = User::getInstance()->getUserIdentity($uid, $liveuid, 0);
                if (in_array($userIdentity, [40, 50, 60])) {//主播等放过
                    echo json_encode($rs);
                    exit;
                } else {
                    if (Live::getInstance()->isRoomCharged($uid, $liveuid, 0, $liveuid, $stream)) {
                        echo json_encode($rs);
                        exit;
                    } else {
                        $rs['type_msg'] = '本房间为收费房间，需支付' . $islive['type_val'] . ' ' . $config['name_coin'];
                    }
                }
            } else if ($islive['type'] == 3) {
                $rs['type_msg'] = '本房间为计时房间，每分钟支付需支付' . $islive['type_val'] . ' ' . $config['name_coin'];
            }
        }
        echo json_encode($rs);
        die;
    }

    /* 直播/回放 结束后推荐 */
    public function endRecommend()
    {
        /* 推荐列表 */

        $list = M("users_live")->where("islive='1'")->order("rand()")->limit(0, 3)->select();
        foreach ($list as $k => $v) {
            $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
        }
        $data = [
            'error' => 0,
            'data' => $list,
        ];
        echo json_encode($data);
        die;
    }

    /* 关注 */
    public function attention()
    {
        /* 推荐列表 */
        $uid = $this->getUserId();
        if ($uid == 0 || $uid == '0' || $uid == "") {
            $data = [
                'error' => 1,
                'msg' => "请登录",
                'data' => "请登录",
            ];

        } else {
            $anchorid = (int)I("roomnum");
            if ($uid == $anchorid) {
                $data = [
                    'error' => 1,
                    'msg' => "不能关注自己",
                    'data' => "不能关注自己",
                ];
            } else {
                $add = [
                    'uid' => $uid,
                    'touid' => $anchorid,
                ];
                if (User::getInstance()->isAttention($uid, $anchorid)) {
                    $check = M('users_attention')->where($add)->delete();
                    if ($check !== false) {
                        $data = [
                            'error' => 0,
                            'msg' => "+关注",
                            'data' => User::getInstance()->getFansnums($anchorid),
                        ];
                    } else {
                        $data = [
                            'error' => 1,
                            'data' => '',
                            'msg' => "取消关注失败",
                        ];
                    }
                } else {
                    $check = M('users_attention')->add($add);
                    $black = M('users_black')->where($add)->delete();
                    if ($check !== false) {
                        $data = [
                            'error' => 0,
                            'msg' => "已关注",
                            'data' => User::getInstance()->getFansnums($anchorid),
                        ];
                    } else {
                        $data = [
                            'error' => 1,
                            'msg' => "关注失败",
                            'data' => "",
                        ];
                    }
                }
            }
        }
        echo json_encode($data);
        die;
    }

    /* 获取粉丝数量 */
    public function getattentionnums()
    {
        $anchorid = I('anchorid');
        $data = [
            'error' => 0,
            'msg' => "",
            'data' => User::getInstance()->getFansnums($anchorid),
        ];
        echo json_encode($data);
        die;
    }

    /*主播页面特殊直播弹窗*/
    public function selectplay()
    {
        $this->display();
    }

    public function settimes()
    {
        $this->display();
    }

    /**
     * 直播间在线用户
     */
    public function getUserList()
    {
        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');

        $nums = Live::getInstance()->getOnlineUserNum($liveType, $liveId);
        $lists = Live::getInstance()->getOnlineUserList($liveType, $liveId) ?: [];
        foreach ($lists as $k => $v) {
            $user = M('users')->where(['id' => $v['id']])->find();
            //use latest level
            $lists[$k]['vestIcon'] = $user ? User::getInstance()->getLevel($user['consumption']) : 0;
            //user vest info
            $vestID = $user ? $user['vest_id'] : 0;
            if ($vestID > 1) {
                $vestInfo = Live::getInstance()->getUsersVest($vestID);
                if ($v['sex'] == 2) {//用户性别为女
                    //根据用户的vestid获取对应的马甲图像
                    $lists[$k]['vestIcon'] = Func::getUrl($vestInfo['vest_woman_url']);//女性马甲图标
                } else {//其他默认性别为男
                    $lists[$k]['vestIcon'] = Func::getUrl($vestInfo['vest_man_url']);//男性马甲图标
                }

                $lists[$k]['vestid'] = $vestID;//设置马甲ID
            } else {
                //判断该用户是否被设置马甲
                $vestInfo = Live::getInstance()->getUserVest($v['id'], $liveType, $liveId);
                $lists[$k]['vestid'] = $vestInfo ? $vestInfo['vestid'] : 1;
                $vestInfo = Live::getInstance()->getUsersVest($lists[$k]['vestid']);
                if ($v['sex'] == 2) {//用户性别为女
                    //根据用户的vestid获取对应的马甲图像
                    $lists[$k]['vestIcon'] = Func::getUrl($vestInfo['vest_woman_url']);//女性马甲图标
                } else {//其他默认性别为男
                    $lists[$k]['vestIcon'] = Func::getUrl($vestInfo['vest_man_url']);//男性马甲图标
                }
            }

            if (User::getInstance()->isVisitor($v['id'])) {
                $lists[$k]['vestid'] = 8;
                $lists[$k]['vestIcon'] = Func::getUrl('', 3);
            }

            //判断该用户是否是该主播的守护
            $info = M('users_guard_lists')->where([
                'uid' => ['eq', $v['id']],
                'liveuid' => ['eq', $liveId],
                'effectivetime' => ['gt', time()],
            ])->find();
            if ($info) {
                $lists[$k]['isGuard'] = 1;
                $lists[$k]['guardLevel'] = $info['guard_level'];
            } else {
                $lists[$k]['isGuard'] = 0;
            }
        }

        echo json_encode(['list' => $lists, 'nums' => $nums,]);
        die;
    }

    public function getHall()
    {
        $p = I('times');
        $pnum = I('defnums');
        $start = ($p - 1) * $pnum;
        $nowtime = time();
        $result = M("channel c")
            ->field("l.uid,l.avatar,l.avatar_thumb,l.user_nicename,l.title,l.city,l.stream,l.pull,l.thumb,l.ispc,l.starttime")
            ->join("left join __USERS_LIVE__ l on l.uid=c.liveid")
            ->where("l.islive= '1'")
            ->order("c.orderno asc,starttime desc")
            ->limit($start, $pnum)
            ->select() ?: [];
        foreach ($result as $k => $v) {
            $result[$k]['nums'] = (string)Live::getInstance()->getOnlineUserNum(0, $v['uid']);
            if (!$v['thumb']) {
                $result[$k]['thumb'] = $v['avatar'];
            }
            $result[$k]['length'] = Func::formatTime($nowtime - $v['starttime']);
        }
        echo json_encode($result);
        die;
    }

    /*检测用户是否是主播的管理员*/
    public function checkManager()
    {
        $liveuid = I("liveuid");
        $uid = I("uid");
        $result = M("users_livemanager")
            ->where("liveuid={$liveuid} and uid={$uid}")
            ->find();

        $rs = [];
        if ($result) {
            $rs['code'] = 0;
            $rs['msg'] = "是管理员";
        } else {
            $rs['code'] = 1;
            $rs['msg'] = "不是管理员";
        }

        echo json_encode($rs);
        die;
    }

    /**
     * 检查是否是超管
     */
    public function checkSuper()
    {
        $uid = I("uid");

        $user = M("users")->where(['id' => $uid])->find();
        if ($user['issuper'] == 1) {
            $rs['code'] = 0;
            $rs['msg'] = "是超管";
        } else {
            $rs['code'] = 1;
            $rs['msg'] = "不是超管";
        }

        echo json_encode($rs);
        die;
    }

    /*转盘游戏判断用户是否存在*/
    public function checkUser()
    {
        $uid = I("uid");
        $stream = I('stream');
        $info = M("users")->where("id={$uid}")->find();

        $rs = ['code' => 0, 'msg' => '', 'info' => []];

        if (!$info) {
            $rs['code'] = 1001;
            $rs['msg'] = '该用户不存在';
            echo json_encode($rs);
            exit;
        } else {
            if ($info['iszombie'] == 1) {

                $rs['code'] = 1002;
                $rs['msg'] = '该用户无法开启游戏';
                echo json_encode($rs);
                exit;
            }

            $list = CRedis::getInstance()->hGetAll(Live::getInstance()->getOnlineUserKey(0, $uid)) ?: [];
            //判断该用户是否在本房间内
            $inRoom = 0;
            foreach ($list as $v) {
                $arr = json_decode($v, true);
                if (intval($arr['id']) == $uid) {
                    $inRoom = 1;
                    break;
                }
            }

            if ($inRoom == 1) { //在房间内
                $info = M("users_carouse")
                    ->where("id=1")
                    ->find();

                if ($info) {
                    $randNum = rand(1, $info['nums']);
                    $info['randNum'] = strval($randNum);
                    $info['imgUrl'] = Func::getUrl($info['url']);
                    unset($info['url']);
                    unset($info['addtime']);

                } else {
                    $info = -1;
                }

                if ($info == -1) {
                    $rs['msg'] = '暂无转盘游戏';
                    $rs['code'] = 1001;
                    echo json_encode($rs);
                    exit;
                } else {
                    $rs['code'] = 0;
                    $rs['info'] = $info;
                    echo json_encode($rs);
                    exit;
                }
            } else { //不在房间内
                $rs['code'] = 1001;
                $rs['msg'] = "该用户不在本房间";
                echo json_encode($rs);
                exit;
            }
        }

        echo json_encode($rs);
        die;
    }

    function getRandNum()
    {
        $info = M("users_carouse")
            ->where("id=1")
            ->find();

        $rs = [];

        if ($info) {
            $randNum = rand(1, $info['nums']);

            $rs['code'] = 0;
            $rs['randNum'] = $randNum;

        } else {
            $rs['code'] = 1001;
        }

        echo json_encode($rs);
        die;
    }

    function changeStatus()
    {
        $senduID = I("sendUid");
        $recordID = I("recordID");
        $status = I("status");
        $result = M("users_connect_video")->where("id={$recordID}")->setField('status', $status);
        $uid = $this->getUserId();

        if ($result !== false) {
            $stream = $uid . "_" . $senduID . "_" . time();
            $data = ['code' => 0, 'msg' => '修改成功', 'stream' => $stream];
            echo json_encode($data);
            exit;
        } else {
            $data = ['code' => 1001, 'msg' => '修改失败'];
            echo json_encode($data);
            exit;
        }
    }

    /**
     * 获取双方马甲ID
     */
    public function checkVest()
    {
        $touid = I('touid');
        $liveType = (int)I('live_type');
        $liveId = I('liveid');

        $uid = $this->getUserId();
        //判断当前用户是否被设置成黑马或紫马
        $VestID = M('users')->where(['id' => $uid])->getField('vest_id');
        if ($VestID > 1) {
            $userVestID = $VestID;
        } else {
            //从房间，马甲列表中查找
            $vest = Live::getInstance()->getUserVest($uid, $liveType, $liveId);
            $userVestID = $vest ? $vest['vestid'] : 1;
        }

        //判断touid的马甲id
        $VestID = M('users')->where(['id' => $touid])->getField('vest_id');
        if ($VestID > 1) {
            $touidVestID = $VestID;
        } else {
            //从房间，马甲列表中查找
            $vest = Live::getInstance()->getUserVest($touid, $liveType, $liveId);
            $touidVestID = $vest ? $vest['vestid'] : 1;
        }

        $rs = ['code' => 0, 'userVestID' => $userVestID, 'touidVestID' => $touidVestID];
        echo json_encode($rs);
        exit;
    }

    /**
     * 选择马甲类型
     */
    public function selectVest()
    {
        $touid = I("touid");
        $vestLists = I("vestLists");
        $this->assign("vestLists", $vestLists);
        $this->assign("touid", $touid);
        $this->display();
    }

    /**
     * 更换马甲（超管）
     */
    public function changeVest()
    {
        if (!IS_POST) {
            return $this->renderJson([], 400, '客户端非法调用');
        }

        $liveType = (int)I('live_type');
        $liveId = I('liveid');
        $touid = (int)I('touid');
        $vestid = (int)I('vestid');

        $rs = Live::getInstance()->changeVest($liveType, $liveId, $touid, $vestid);
        if (false === $rs) {
            return $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
        }

        $this->renderJson($rs);
    }

    function checkIsLive()
    {
        $uid = I("uid");
        $liveuid = I("liveuid");
        $rs = [];

        $liveInfo = M("users_live")->where("uid={$liveuid} and islive=1")->find();
        if (!$liveInfo) {
            $rs['code'] = 1001;
            $rs['msg'] = "当前主播未直播,无法发起连麦";
            echo json_encode($rs);
            exit;
        } else {
            $userInfo = M("users")->where("id={$uid}")->find();
            if ($userInfo) {
                //判断当前直播间是否有用户在连麦
                $Lianmai = M("users_connect_video");
                $connectLists = $Lianmai->where("liveid={$liveuid} and (status=0 or status=1) ")->select();
                $connectNum = count($connectLists);
                if ($connectNum > 0) {
                    $iscurrentUser = 0;
                    $isConnectVideo = 0;
                    //判断当前连麦的用户是否是该用户
                    foreach ($connectLists as $k => $v) {
                        if ($v['uid'] == $uid) {//当前连麦是当前用户

                            $iscurrentUser = 1;
                            $isConnectVideo = $v['status'];
                            break;
                        }
                    }

                    if ($iscurrentUser == 1) {
                        $rs['code'] = 1001;
                        if ($isConnectVideo == 1) {
                            $rs['msg'] = "您正在跟主播连麦";
                        } else {
                            $rs['msg'] = "您已经申请连麦";
                        }
                        echo json_encode($rs);
                        die;
                    } else {
                        $rs['code'] = 1001;
                        $rs['msg'] = "当前主播正在连麦";
                        echo json_encode($rs);
                        die;
                    }
                    exit;
                } else {
                    //判断是否有该用户和主播的连麦记录
                    $info = $Lianmai->where("liveid={$liveuid} and uid={$uid} ")->find();
                    if ($info) {
                        $data = [
                            'status' => 0,
                            'addtime' => time(),
                        ];

                        $result = $Lianmai->where("uid={$uid} and liveid={$liveuid}")->save($data);
                    } else {
                        $data = [
                            'uid' => $uid,
                            'liveid' => $liveuid,
                            'status' => 0,
                            'addtime' => time(),
                        ];

                        $result = $Lianmai->add($data);
                    }

                    $user_nicename = $userInfo['user_nicename'];

                    if ($result !== false) {
                        $rs['code'] = 0;
                        $rs['msg'] = '';
                        $rs['info'] = $user_nicename;
                        $rs['recordID'] = $result;
                        //$rs['stream']=$liveuid."_".$uid."_".time();
                    } else {
                        $rs['code'] = 1002;
                        $rs['msg'] = "连麦失败啦";
                    }

                    echo json_encode($rs);
                    exit;
                }
            } else {
                $rs['code'] = 1002;
                $rs['msg'] = "连麦失败啦,请确认您的身份";
                echo json_encode($rs);
                exit;
            }
        }
    }

    public function checkLianmaiStatus()
    {
        $userid = I("userid");
        $roomid = I("roomid");
        $rs = ['code' => 0, 'msg' => '', 'info' => ''];

        $info = M("users_connect_video")->where("uid={$userid} and liveid={$roomid}")->find();

        if ($info['status'] == 0) {//主播一直没有操作
            $rs['code'] = 0;
            $rs['msg'] = "主播未操作";
            //将对应记录状态改为连麦结束
            /*$data=array(
				"status"=>2,
			);
			M("users_connect_video")->where("uid={$userid} and liveid={$roomid}")->save($data);*/
        } else if ($info['status'] == 1) {//正在连麦
            $rs['code'] = 1001;
            $rs['msg'] = "您当前正在连麦";
        } else if ($info['status'] == 2) {//拒绝
            $rs['code'] = 1002;
            $rs['msg'] = "您的连麦已经被拒绝";
        } else if ($info['status'] == 3) {//您的连麦已经结束了
            $rs['code'] = 1003;
            $rs['msg'] = "您的连麦已经结束了";
        }

        echo json_encode($rs);
        exit;
    }

    //向直播记录里添加连麦用户的stream
    public function changeLianmaiStream()
    {
        $liveid = I("liveid");
        $stream = I("stream");

        $data = [
            'lianmai_stream' => $stream,
        ];

        $res = M("users_live")->where(['uid' => $liveid])->save($data);
        if ($res !== false) {
            $rs = [
                'code' => 0,
                'msg' => '更新连麦用户stream成功',
            ];
        } else {
            $rs = [
                'code' => 1001,
                'msg' => '更新连麦用户stream失败',
            ];
        }

        echo json_encode($rs);
        die;
    }

    public function getLianmaiStream()
    {
        $stream = I("stream");
        $rs = ['code' => 0, 'msg' => '', 'info' => ''];
        $url = Live::getInstance()->getStream('rtmp', $stream, 0);
        $rs['info'] = $url;
        echo json_encode($rs);
        die;
    }

    /*私信聊天判断用户是否被对方拉黑*/
    public function currentChatUid()
    {
        $uid = I("uid");
        $touid = I("touid");
        $info = M("users_black")->where("uid={$touid} and touid={$uid}")->find();

        $rs = ['code' => 0, 'msg' => '', 'info' => ''];
        if ($info) {
            $rs['msg'] = "被对方拉黑";
        } else {
            $rs['code'] = 1001;
            $rs['msg'] = "未被对方拉黑";
        }

        echo json_encode($rs);
        die;
    }

    /**
     * 用户私信权限
     */
    public function getUserPrivateChat()
    {
        $touid = I('touid');
        $liveId = (int)I('liveid');
        $liveType = (int)I('live_type');

        $uid = $this->getUid();
        $srv = Live::getInstance();
        $rs = $srv->getPrivateChatPermission($uid, $touid, $liveType, $liveId);
        if (false === $rs) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 检查是否黑名单
     */
    public function checkIsBlack()
    {
        $uid = I('uid');
        $touid = I('touid');

        if ($this->getUserId() && $uid && $touid) {
            $info = M('users_black')->where(['uid' => $touid, 'touid' => $uid])->find();
            if ($info) {
                $rs['code'] = 0;
                return $this->renderJson([], 400, '对方暂时拒绝接收您的消息');
            }
        }

        //新版不对未登录用户限制
        $this->renderJson();
    }

    public function stopLianmai()
    {
        $liveid = I("liveid");
        //将直播记录里的连麦信息删掉
        M("users_live")->where("uid={$liveid} and islive=1")->setField("lianmai_stream", "");
        //将连麦列表的数据修改
        M("users_connect_video")->where("liveid={$liveid} and (status=0 or status=1)")->setField("status", 3);
        $rs = ['code' => 0, 'msg' => '', 'info' => ''];
        echo json_encode($rs);
        die;
    }

    /*通过用户昵称获取用户ID*/
    public function searchInfoByNicename()
    {
        $rs = ["code" => 0, "msg" => '', 'info' => ''];
        $user_nicename = I("user_nicename");
        $where = [];
        $where['user_nicename'] = ["like", "%" . $user_nicename . "%"];
        $where['isvirtual'] = 0;

        $lists = M("users")->where($where)->select();
        if ($lists) {
            $rs['code'] = 0;
            $rs['info'] = $lists;
        } else {
            $rs['code'] = 1001;
            $rs['msg'] = "未查询到相关数据";
        }

        echo json_encode($rs);
        die;
    }

    /**
     * 房间在线充值管理员
     */
    public function getRoomAdminUserList()
    {
        $liveType = (int)I('live_type');
        $liveId = I('liveid');

        if (!$liveId) {
            $this->renderJson([], 400, '缺参数');
            return;
        }

        $list = Live::getInstance()->getOnlineUserList($liveType, $liveId) ?: [];
        $userIds = array_column($list, 'id');
        //紫马、黑马、橙马
        $list = $userIds ? M('users')->where([
            'id' => ['in', $userIds],
            'vest_id' => ['in', [5, 6, 7,]],
        ])->select() : [];

        $this->renderJson($list);
    }

    /**
     * delete
     */
    public function getMultiShowInfo()
    {
        $uid = $this->getUserId();
        $roomId = (int)I('room_id');
        if (!$roomId) {
            $this->renderJson([], 400, '缺参数room_id');
            return;
        }

        $data = MultiLive::getInstance()->getRoomInfo($roomId, $uid);
        if (!$data) {
            $this->renderJson([], 400, '直播间不存在');
            return;
        }

        $this->renderJson($data);

        if ($data['status'] == 0) {
            $this->renderJson([], 400, '直播间已关闭');
        } elseif ($data['status'] == 2) {
            $this->renderJson([], 400, '直播间休息中');
        } else {
            $this->renderJson($data);
        }
    }

    /**
     * 开始/关闭直播,开始/关闭喊麦API
     */
    public function setMultiShowManagerStatus()
    {
        if (!IS_POST) {
            $this->renderJson([], 400, '非法请求');
            return;
        }

        $multiId = (int)I('room_id');
        $vestType = trim(I('vest_type'));
        $status = (int)I('status');

        $uid = $this->getUserId();
        $data = MultiLive::getInstance()->setMultiShowManagerStatus($uid, $multiId, $vestType, $status);
        if (false === $data) {
            $this->renderJson([], 400, MultiLive::getInstance()->getLastErrMsg());
            return;
        }

        $this->renderJson();
    }

    /**
     * 多视频房间密码修改接口
     */
    public function setMultiShowPassword()
    {
        if (!IS_POST) {
            $this->renderJson([], 400, '非法请求');
            return;
        }

        $multiId = (int)I('room_id');
        $password = trim(I('password'));

        $uid = $this->getUserId();
        $data = MultiLive::getInstance()->setMultiShowPassword($uid, $multiId, $password);
        if (false === $data) {
            $this->renderJson([], 400, MultiLive::getInstance()->getLastErrMsg());
            return;
        }

        $this->renderJson();
    }

    /**
     * 进入多人直播间检查
     * @see checkLive()
     */
    public function checkMultiLive()
    {
        $liveId = (int)I('live_id');

        $uid = $this->getUserId();
        $config = $this->config;
        $live = MultiLive::getInstance()->getMultiShowInfo($liveId);
        if (!$live) {
            return $this->renderJson([], 400, '直播间不存在');
        }

        if ($live['status'] == 0) {
            return $this->renderJson([], 400, '直播间关闭中');
        } elseif ($live['status'] == 0) {
            return $this->renderJson([], 400, '直播间休息中');
        }

        $data = [
            'id' => $live['id'],
            'room_type' => $live['room_type'],
            'room_password' => $live['room_password'],
            'room_live_coin' => $live['room_live_coin'],
        ];
        if (!$uid) {
            if ($live['room_type'] == 1) {
                $data['msg'] = '当前房间为密码房间，请先登陆';
                return $this->renderJson($data);
            }
            if ($live['room_type'] == 2) {
                $data['msg'] = '当前房间为付费房间，请先登陆';
                return $this->renderJson($data);
            }
        }

        if ($live['room_type'] == 1) {
            $data['msg'] = '当前房间为密码房间，请输入密码';
            return $this->renderJson($data);
        } elseif ($live['room_type'] == 2) {
            $userIdentity = User::getInstance()->getUserIdentity($uid, $liveId, 1);
            if (in_array($userIdentity, [40, 50, 60])) {//主播等放过
                return $this->renderJson();
            } else {
                $isExist = Live::getInstance()->isRoomCharged($uid, 0, 1, $liveId);
                if ($isExist) {
                    return $this->renderJson();
                } else {
                    $data['msg'] = '本房间为收费房间，需支付' . $live['room_live_coin'] . $config['name_coin'];
                    return $this->renderJson($data);
                }
            }
        } else {
            return $this->renderJson();
        }
    }

    public function checkRoomPassword()
    {
        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');
        $password = trim(I('password'));

        $data = Live::getInstance()->checkRoomPassword($liveType, $liveId, $password);
        if (false === $data) {
            return $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
        }

        return $this->renderJson();
    }

    /**
     * 设置每日发送消息数量
     */
    public function setDailySendMsgNum()
    {
        if (!IS_POST) {
            $this->renderJson([], 400, '非法请求');
            return;
        }

        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');
        $uid = $this->getUid();
        $rs = Live::getInstance()->setDailySendMsgNum($liveType, $liveId, $uid);
        if (false === $rs) {
            $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
            return;
        }

        $num = Live::getInstance()->getDailySendMsgNum($liveType, $liveId, $uid);
        $this->renderJson([
            'num' => $num,
        ]);
    }

    public function getLanXunVideo()
    {
        $op = trim($this->getGet('op'));
        $url = trim($this->getGet('url'));
        $createtime = trim($this->getGet('createtime'));
        LiveVideo::getInstance()->getLanXunVideo($op, $url, $createtime);

        return $this->renderObject();
    }

}
