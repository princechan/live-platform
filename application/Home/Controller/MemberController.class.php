<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;
use Common\Lib\Helpers\Func;

/**
 * 会员登录
 */
class MemberController extends HomebaseController
{
    //首页
    public function login()
    {
        $rs = ['status' => 0, 'livestream_url' => ''];
        $username = I('username');
        $password = I('password');
        $level = I('level');
        $roomid = I('roomid');
        $token = I('token');
        //$configpri = Func::getPrivateConfig();
        //$key=$configpri['loginkey'];
        $key = file_get_contents('/data/config/ybkey.ini');
        $sign = md5("username={$username}password={$password}level={$level}" . $key);

        if ($sign != $token) {
            echo json_encode($rs);
            exit;
        }

        $Users = M('users');
        $username = User::getInstance()->encryptName($username);
        $userinfo = $Users->where("user_login='{$username}' and user_pass='{$password}' and user_type='2'")->find();
        if (!$userinfo) {
            $data = [
                'user_login' => $username,
                'user_nicename' => 'WEB用户' . substr($username, -4),
                'user_pass' => $password,
                'signature' => '这家伙很懒，什么都没留下',
                'avatar' => '/default.jpg',
                'avatar_thumb' => '/default_thumb.jpg',
                'create_time' => date("Y-m-d H:i:s"),
                'last_login_time' => date("Y-m-d H:i:s"),
                'user_status' => 1,
                "user_type" => 2,//会员
            ];

            $Users->add($data);
            $userinfo = $Users->where("user_login='{$username}' and user_pass='{$password}' and user_type='2'")->find();
        }

        if ($userinfo) {
            $userinfo['level'] = User::getInstance()->getLevel($userinfo['consumption']);
            if (!$userinfo['token'] || !$userinfo['expiretime']) {
                $token = md5(md5($userinfo['id'] . $userinfo['user_login'] . time()));
                $expiretime = time() + 60 * 60 * 24 * 300;
                $Users->where("id='{$userinfo['id']}'")->save(['token' => $token, 'expiretime' => $expiretime]);
                $userinfo['token'] = $token;
            }

            User::getInstance()->afterLogin($userinfo);
            $rs['status'] = 1;
        }

        if (!$roomid) {
            $Live = M('users_live');
            $liveid = M("channel")->field("liveid")->order('orderno asc')->find();
            if ($liveid) {
                $info = $Live
                    ->field("uid")
                    ->where("islive=1 and uid={$liveid['liveid']}")
                    ->order("starttime desc")
                    ->find();

            }
            if (!$info) {
                $info = $Live
                    ->field("uid")
                    ->where("islive=1")
                    ->order("starttime desc")
                    ->find();

            }
            if ($info) {
                $roomid = $info['uid'];
            } else {
                $roomid = $liveid['liveid'];
            }
        }
        $configpub = Func::getPublicConfig();

        $rs['livestream_url'] = $configpub['site'] . '/' . $roomid;
        header("Location:" . $rs['livestream_url']);
        exit;
    }

    /**
     * 登出
     */
    function logout()
    {
        $url = I("url");
        User::getInstance()->logout();
        header("Location:" . $url);
    }

}
