<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Live;
use Common\Lib\Auth\User;
use Common\Lib\Live\MultiLive;

/**
 * 消费相关
 */
class SpendController extends HomebaseController
{
    /**
     * 送礼物
     */
    public function sendGift()
    {
        if (!IS_POST) {
            echo '{"errno":"1000","data":"","msg":"客户端非法调用"}';
            exit;
        }

        $liveType = (int)I('liveType');
        $liveId = (int)I('liveid');
        $touid = (int)I('touid');
        $giftid = (int)I('giftid');
        $giftGroupNum = (int)I('giftGroupNum', 1);
        $sendType = (int)I('sendType');
        if (!$liveId || !$touid || !$giftid || !$giftGroupNum) {
            echo '{"errno":"1000","data":"","msg":"缺参数"}';
            exit;
        }

        $uid = $this->getUserId();
        if ($uid <= 0) {
            echo '{"errno":"1004","data":"","msg":"立即登录赠送礼物"}';
            exit;
        }
        if ($uid == $touid) {
            echo '{"errno":"1000","data":"","msg":"不允许给自己送礼物"}';
            exit;
        }

        $giftcount = 1;
        $stream = '';
        $showid = 0;
        if ($liveType == 0) {
            $liveInfo = M('users_live')->where(['uid' => $touid, 'islive' => 1])->find();
            $stream = $liveInfo ? $liveInfo['stream'] : '';
            $showid = $liveInfo ? $liveInfo['starttime'] : 0;
        }

        //礼物信息
        $giftinfo = M("gift")->field("giftname,gifticon,needcoin,type,is_win_gift")->where(['id' => $giftid])->find();
        $giftinfo['gifticon'] = Func::getUrl($giftinfo['gifticon']);
        $total = $giftinfo['needcoin'] * $giftcount * intval($giftGroupNum);
        $addtime = time();
        //直播券和钻石兑换比例
        $live_coin_percent = Live::getInstance()->getExchangeRate();
        $total_livecoin = $total * $live_coin_percent;
        //use info
        $User = M("users");
        $userinfo = $User->field('user_login,user_pass,coin,token,expiretime,user_nicename,avatar,livecoin')
            ->where(['id' => $uid])->find();
        //获取余额
        $userinfo['coin'] = Live::getInstance()->getUserCoin($userinfo['user_login'], $userinfo['user_pass']);

        if ($sendType == 0) {
            if ($userinfo['livecoin'] > 0 && ($userinfo['livecoin'] - $total_livecoin >= 0)) {
                $res = Live::getInstance()->deducteUserCoin($userinfo['user_login'], $userinfo['user_pass'], $total);
                if (is_array($res)) {
                    if ($res['code'] != 200) {
                        echo '{"errno":"1001","data":"","msg":"' . $res['message'] . '"}';
                        exit;
                    }
                } else {
                    echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
                    exit;
                }
            } else {
                echo '{"errno":"1001","data":"","msg":"余额不足"}';
                exit;
            }
        } else if ($sendType == 1) {
            //确认 [直播券不够，直接用钻石支付]
            //扣费
            $res = Live::getInstance()->deducteUserCoin($userinfo['user_login'], $userinfo['user_pass'], $total);
            if (is_array($res)) {
                if ($res['code'] != 200) {
                    echo '{"errno":"1001","data":"","msg":"' . $res['message'] . '"}';
                    exit;
                }
            } else {
                echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
                exit;
            }

            //主播增加映票和累计映票
            $User->where("id='{$touid}'")->setInc('votes', $total);
            $User->where("id='{$touid}'")->setInc('votestotal', $total);
        }

        //用户增加经验值
        $User->where(['id' => $uid])->setInc('consumption', $total);
        //向消费记录表里添加一条记录
        Live::getInstance()->addUserCoinRecord([
            "type" => 'expend',
            "action" => 'sendgift',
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "giftgroup_num" => $giftGroupNum,
            "total_livecoin" => $sendType ? 0 : $total_livecoin,
            "showid" => $showid,
            "addtime" => $addtime,
            'live_type' => $liveType,
            'liveid' => $liveId,
        ]);

        /*******判断中奖礼物start********/
        $iswin = 0;//默认不中奖
        $winCoin = 0;//中奖钻石数
        //判断该礼物是不是中奖礼物
        if (intval($giftinfo['is_win_gift']) == 1) {
            //获取后台配置信息
            $configPub = Func::getPublicConfig();
            //获取后台配置的中奖几率
            $gift_win_percent = $configPub['gift_win_percent'];
            //获取中奖倍率
            $gift_win_multiple = $configPub['gift_win_multiple'];
            if ($gift_win_percent >= 100) {
                $gift_win_percent = 100;
            } else if ($gift_win_percent < 0) {
                $gift_win_percent = 0;
            }

            if ($gift_win_percent > 0 && $gift_win_percent < 100) {
                $randNum = rand(1, 99);
                if ($randNum <= $gift_win_percent) {//中奖了

                    //计算中奖的金额
                    $winCoin = $gift_win_multiple * $total;
                    //计算中奖的直播券
                    $winLiveCoin = $live_coin_percent * $winCoin;

                    //向中奖纪录表里添加数据
                    $data = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'stream' => $stream,
                        'giftid' => $giftid,
                        'gift_coin' => $giftinfo['needcoin'],
                        'gift_win_multiple' => floatval($gift_win_multiple),
                        'win_coin' => $winCoin,
                        'addtime' => time(),
                        'win_livecoin' => $winLiveCoin
                    ];

                    M("users_sendgift_win")->add($data);
                    $iswin = 1;
                }
            } else if ($gift_win_percent == 100) {//直接中奖
                //计算中奖的金额
                $winCoin = $gift_win_multiple * $total;
                //计算中奖的直播券
                $winLiveCoin = $live_coin_percent * $winCoin;
                //向中奖纪录表里添加数据
                $data = [
                    'uid' => $uid,
                    'liveuid' => $liveId,
                    'stream' => $stream,
                    'giftid' => $giftid,
                    'gift_coin' => $giftinfo['needcoin'],
                    'gift_win_multiple' => floatval($gift_win_multiple),
                    'win_coin' => $winCoin,
                    'addtime' => time(),
                    'win_livecoin' => $winLiveCoin
                ];

                M("users_sendgift_win")->add($data);
                $iswin = 1;
            }
        }

        /*******判断中奖礼物end********/
        //判断用户如果中奖，给用户加直播券
        if ($iswin == 1) {
            $User->where("id='{$uid}'")->setInc('livecoin', $winLiveCoin); // 用户直播券增加
        }

        $redis = CRedis::getInstance();
        /*******主播发起送礼物PK时，用户送礼物给相应主播加数据start********/
        $giftPKinfo = $redis->get('giftPK_' . $liveId);

        //判断redis数据是否存在
        if ($giftPKinfo) {
            $pkArr = json_decode($giftPKinfo, true);//将json转为数组
            //获取pk的状态数据
            $pkIsEnd = $pkArr['isEnd'];
            if ($pkIsEnd == 1) {//代表PK已经结束
                $redis->delete("giftPK_" . $liveId);
            } else {//pk未结束
                //获取礼物PK数据的id
                $pkID = $pkArr['pkID'];
                //获取pk redis里的主队礼物id
                $masterGiftID = $pkArr['masterGiftID'];
                //获取pk redis里的客队礼物id
                $guestGiftID = $pkArr['guestGiftID'];
                //客队人id
                $guestID = $pkArr['guestID'];

                if ($masterGiftID == $giftid) {//赠送礼物同主队礼物相同
                    $pkArr['masterGiftNum'] += $giftcount * $giftGroupNum;
                    $pkArr['masterGiftTotalCoin'] += $total;

                    //写入数据库pk赠送记录表中
                    $dataMsg = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'giftid' => $giftid,
                        'num' => $giftcount,
                        'group_num' => $giftGroupNum,
                        'coin' => $total,
                        'guestid' => $guestID,
                        'send_type' => 0,
                        'pk_id' => $pkID,
                        'addtime' => time()
                    ];

                    M("giftpk_lists")->add($dataMsg);
                }

                if ($guestGiftID == $giftid) {//赠送的礼物同客队礼物相同
                    $pkArr['guestGiftNum'] += $giftcount * $giftGroupNum;
                    $pkArr['guestGiftTotalCoin'] += $total;
                    //写入数据库pk赠送记录表中
                    $dataMsg = [
                        'uid' => $uid,
                        'liveuid' => $liveId,
                        'giftid' => $giftid,
                        'num' => $giftcount,
                        'group_num' => $giftGroupNum,
                        'coin' => $total,
                        'guestid' => $guestID,
                        'send_type' => 1,
                        'pk_id' => $pkID,
                        'addtime' => time()
                    ];

                    M("giftpk_lists")->add($dataMsg);
                }

                //将数组重新打包成json串
                $giftPKinfo = json_encode($pkArr);
                $redis->set("giftPK_" . $liveId, $giftPKinfo);
            }
        }

        /*******主播发起送礼物PK时，用户送礼物给相应主播加数据end********/
        /* 更新用户余额 消费 */
        //M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total} where id='{$uid}'");
        /* 更新直播 映票 累计映票 */
        //M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$touid}'");

        $userinfo2 = $User->field("consumption,coin,votestotal,livecoin")->where("id='{$uid}'")->find();
        $level = User::getInstance()->getLevel($userinfo2['consumption']);

        //获取余额
        $userCoin = Live::getInstance()->getUserCoin($userinfo['user_login'], $userinfo['user_pass']);

        //清除缓存
        $redis->del("userinfo_{$uid}");
        $redis->del("userinfo_{$touid}");
        $gifttoken = md5(md5('sendGift' . $uid . $touid . $giftid . $giftcount . $total . $showid . $addtime));
        $result = [
            "uid" => (int)$uid,
            "giftid" => (int)$giftid,
            "giftcount" => (int)$giftcount,
            "totalcoin" => $total,
            'giftGroupNum' => $giftGroupNum,
            "giftname" => $giftinfo['giftname'],
            "gifticon" => $giftinfo['gifticon'],
            "level" => $level,
            "coin" => $userCoin,
            "votestotal" => $userinfo2['votestotal'],
            "iswin" => $iswin,
            "winCoin" => $winCoin,
            "winLiveCoin" => $winLiveCoin,
            "livecoin" => $userinfo2['livecoin'],
        ];

        $redis->set($gifttoken, json_encode($result));
        $evensend = "n";
        if ($giftinfo['type'] == 1) {
            $evensend = "y";
        }

        echo '{"errno":"0","uid":"' . $uid . '","level":"' . $level .
            '","giftGroupNum":"' . $giftGroupNum . '","iswin":"' . $iswin . '","winLiveCoin":"' . $winLiveCoin .
            '","evensend":"' . $evensend . '","coin":"' . $userCoin .
            '","gifttoken":"' . $gifttoken . '","msg":"赠送成功"}';
        exit;
    }

    /**
     * 弹幕
     */
    public function sendHorn()
    {
        if (!IS_POST) {
            $rs['code'] = 1001;
            $rs['msg'] = '客户端非法请求';
            echo json_encode($rs);
            exit;
        }

        $liveId = I('liveid');
        $liveType = (int)I('live_type');
        $touid = (int)I('touid');
        $content = I('content');

        if (!$liveId || !$touid || !in_array($liveType, [0, 1])) {
            echo '{"errno":"1002","data":"","msg":"参数错误"}';
            exit;
        }

        $users = M("users");
        $uid = $this->getUserId();
        $rs = ['code' => 0, 'msg' => '', 'info' => []];
        $userinfo = $users->field('user_login,user_pass,coin,token,expiretime,user_nicename,avatar,livecoin')
            ->where(['id' => $uid])->find();
        if ($uid <= 0) {
            echo '{"errno":"1002","data":"","msg":"请登录"}';
            exit;
        }

        //用户余额
        $userinfo['coin'] = Live::getInstance()->getUserCoin($userinfo['user_login'], $userinfo['user_pass']);
        $configpri = Func::getPrivateConfig();
        $giftid = 1;
        $giftcount = 1;
        $giftinfo = [
            "giftname" => '弹幕',
            "gifticon" => '',
            "needcoin" => $configpri['barrage_fee'],
        ];

        $total = $giftinfo['needcoin'] * $giftcount;
        $addtime = time();
        $action = 'sendbarrage';
        if ($userinfo['coin'] < $total) {
            $rs['code'] = 1001;
            $rs['msg'] = '余额不足';
            echo json_encode($rs);
            exit;
        }

        /*// 更新用户余额 消费
		M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total} where id='{$uid}'");
		// 更新直播主播 映票 累计映票
		M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$liveId}'");*/

        //扣费
        $res = Live::getInstance()->deducteUserCoin($userinfo['user_login'], $userinfo['user_pass'], $total);
        if (is_array($res)) {
            if ($res['code'] != 200) {
                echo '{"errno":"1001","data":"","msg":"' . $res['message'] . '"}';
                exit;
            }
        } else {
            echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
            exit;
        }

        Live::getInstance()->addUserCoinRecord([
            "type" => 'expend',
            "action" => $action,
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "showid" => $liveId,
            "addtime" => $addtime,
            'live_type' => $liveType,
            'liveid' => $liveId,
        ]);

        $userinfo2 = $users->field('consumption,coin')->where(['id' => $uid])->find();
        /*获取当前用户的等级*/
        $level = User::getInstance()->getLevel($userinfo2['consumption']);

        //清除缓存
        $redis = CRedis::getInstance();
        $redis->del("userinfo_" . $uid);
        $redis->del("userinfo_" . $liveId);
        //获取主播影票
        $votestotal = $users->field('votestotal,coin')->where(['id' => $liveId])->find();
        $barragetoken = md5(md5($action . $uid . $liveType . $liveId . $giftid . $giftcount . $total . $addtime . rand(100, 999)));

        //用户余额
        $userinfo['coin'] = Live::getInstance()->getUserCoin($userinfo['user_login'], $userinfo['user_pass']);
        $result = [
            "uid" => $uid,
            "content" => $content,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "giftname" => $giftinfo['giftname'],
            "gifticon" => $giftinfo['gifticon'],
            "level" => $level,
            "coin" => $userinfo['coin'],
            "votestotal" => $votestotal['votestotal'],
            "barragetoken" => $barragetoken,
        ];
        $rs['info'] = $result;
        unset($result['barragetoken']);
        $redis->set($barragetoken, json_encode($result));
        echo json_encode($rs);
    }

    /*设置 取消 管理员*/
    public function cancel()
    {
        $rs = ['code' => 0, 'msg' => '', 'info' => '操作成功'];
        $uid = $this->getUserId();
        $touid = I("touid");
        $showid = I("roomid");
        $users_livemanager = M("users_livemanager");
        if ($uid != $showid) {
            $rs['code'] = 1001;
            $rs['msg'] = '不是该房间主播';
            echo json_encode($rs);
            exit;
        }
        if ($uid == $touid) {
            $rs['code'] = 1002;
            $rs['msg'] = '自己无法管理自己';
            echo json_encode($rs);
            exit;
        }
        $admininfo = $users_livemanager->where("uid='{$touid}' and liveuid='{$showid}'")->find();
        $rs = M("users")->field("id,avatar,avatar_thumb,user_nicename")->where("id=" . $touid)->find();
        if ($admininfo) {
            $users_livemanager->where("uid='{$touid}' and liveuid='{$showid}'")->delete();
            $rs['isadmin'] = 0;
        } else {
            $count = $users_livemanager->where("liveuid='{$showid}'")->count();
            if ($count >= 5) {
                $rs['code'] = 1004;
                $rs['msg'] = '最多设置5个管理员';
                echo json_encode($rs);
                exit;
            }
            $users_livemanager->add(["uid" => $touid, "liveuid" => $showid]);
            $rs['isadmin'] = 1;
        }
        $rs['msg'] = "设置成功";
        echo json_encode($rs);
        exit;
    }

    /**
     * 禁言
     */
    public function gag()
    {
        if (!IS_POST) {
            $rs["code"] = 1001;
            $rs["msg"] = '客户端非法调用';
            echo json_encode($rs);
            exit;
        }

        $liveId = I('liveid');
        $liveType = (int)I('live_type');
        $touid = I('touid');
        $gagTime = I('gagTime');
        if (!$liveId || !in_array($liveType, [0, 1]) || !$touid || !$gagTime) {
            $rs["code"] = 1001;
            $rs["msg"] = '参数错误';
            echo json_encode($rs);
            exit;
        }

        $rs = ['code' => 0, 'msg' => '', 'info' => '禁言成功'];
        $uid = $this->getUserId();
        $uidtype = User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
        if ($uidtype == 30) {
            $rs["code"] = 1001;
            $rs["info"] = '你不是主播或者管理员';
            $rs["msg"] = '你不是主播或者管理员';
            echo json_encode($rs);
            exit;
        }

        $touidtype = User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
        if ($touidtype == 50) {
            $rs["code"] = 1002;
            $rs["info"] = '对方是主播，不能禁言';
            $rs["msg"] = '对方是主播，不能禁言';
            echo json_encode($rs);
            exit;
        } else if ($touidtype == 40) {
            $rs["code"] = 1002;
            $rs["info"] = '对方是管理员，不能禁言';
            $rs["msg"] = '对方是管理员，不能禁言';
            echo json_encode($rs);
            exit;
        } else if ($touidtype == 60) {
            $rs["code"] = 1002;
            $rs["info"] = '对方是超管，不能禁言';
            $rs["msg"] = '对方是超管，不能禁言';
            echo json_encode($rs);
            exit;
        }

        Live::getInstance()->gag($liveType, $liveId, $touid, $gagTime);

        echo json_encode($rs);
        exit;
    }

    /**
     * 是否被禁言
     */
    public function isShutUp()
    {
        $liveId = I('liveid');
        $liveType = (int)I('live_type');

        $uid = $this->getUserId();
        $rs = ['code' => 0, 'msg' => '', 'info' => '0'];
        if ($uid) {
            $admin = User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
            $rs['admin'] = $admin;
            $rs['info'] = Live::getInstance()->isGag($liveType, $liveId, $uid);
        }
        echo json_encode($rs);
        exit;
    }

    /**
     * 踢人
     */
    public function tiRen()
    {
        $liveId = (int)I('liveid');
        $liveType = (int)I('live_type');
        $touid = I('touid');
        $stream = I("stream");

        if (!IS_POST) {
            $rs['code'] = 1000;
            $rs['msg'] = '客户端非法调用';
            echo json_encode($rs);
            exit;
        }
        if (!$liveId || !$touid || !in_array($liveType, [0, 1])) {
            $rs['code'] = 1000;
            $rs['msg'] = '参数错误';
            echo json_encode($rs);
            exit;
        }

        $uid = $this->getUserId();
        $rs = ['code' => 0, 'msg' => '', 'info' => ''];
        if ($uid == $touid) {
            $rs['code'] = 1000;
            $rs['msg'] = '不能将自己踢出';
            echo json_encode($rs);
            exit;
        }

        $user_type = User::getInstance()->getUserIdentity($uid, $liveId, $liveType);
        if (!in_array($user_type, [40, 50, 60])) {
            $rs['code'] = 1000;
            $rs['msg'] = '您不是管理员，无权操作';
            echo json_encode($rs);
            exit;
        }

        $touser_type = User::getInstance()->getUserIdentity($touid, $liveId, $liveType);
        if ($touser_type == 50) {
            $rs['code'] = 1001;
            $rs['msg'] = '对方是主播，不能被踢出';
            echo json_encode($rs);
            exit;
        } else if ($touser_type == 40) {
            $rs['code'] = 1002;
            $rs['msg'] = '对方是管理员，不能被踢出';
            echo json_encode($rs);
            exit;
        } else if ($touser_type == 60) {
            $rs["code"] = 1002;
            $rs["info"] = '对方是超管，不能被踢出';
            $rs["msg"] = '对方是超管，不能被踢出';
            echo json_encode($rs);
            exit;
        }

        Live::getInstance()->setKick($liveType, $liveId, $touid);

        $rs['info']['msg'] = '操作成功';
        echo json_encode($rs);
        exit;
    }

    /**
     * 加入/取消 黑名单
     */
    public function black()
    {
        $rs = ['code' => 0, 'msg' => '', 'info' => '操作成功'];
        $uid = $this->getUserId();
        $touid = I("touid");
        if ($uid == $touid) {
            $rs['code'] = 0;
            $rs['msg'] = '无法将自己拉黑';
            echo json_encode($rs);
            exit;
        }
        $users_black = M('users_black');
        $isexist = $users_black->where("uid=" . $uid . " and touid=" . $touid)->find();
        if ($isexist) {
            $black = $users_black->where("uid=" . $uid . " and touid=" . $touid)->delete();
            if ($black) {
                $rs['code'] = 0;
                $rs['msg'] = '已将该用户移除黑名单';
                echo json_encode($rs);
                exit;
            } else {
                $rs['code'] = 1000;
                $rs['msg'] = '移除黑名单失败';
                echo json_encode($rs);
                exit;
            }
        } else {
            M('users_attention')->where("uid=" . $uid . " and touid=" . $touid)->delete();
            $black = $users_black->add(["uid" => $uid, "touid" => $touid]);
            if ($black) {
                $rs['code'] = 0;
                $rs['msg'] = '已将该用户添加到黑名单';
                echo json_encode($rs);
                exit;
            } else {
                $rs['code'] = 1000;
                $rs['msg'] = '添加黑名单失败';
                echo json_encode($rs);
                exit;
            }

        }
    }

    /**
     * 举报
     */
    public function report()
    {
        $tlleuid = I("tlleuid");
        $token = I("token");
        $liveId = I("liveuid");
        $content = I("content");

        $rs = ['code' => 0, 'msg' => '', 'info' => '操作成功'];
        $uid = $this->getUserId();
        if ($uid != $tlleuid) {
            $rs['code'] = 1000;
            $rs['msg'] = '未知信息错误';
            echo json_encode($rs);
            exit;
        }
        $checkToken = User::getInstance()->checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '登录信息过期，请重新登录';
            echo json_encode($rs);
            exit;
        }
        if ($content == "") {
            $rs['code'] = 1001;
            $rs['msg'] = '举报内容不能为空';
            echo json_encode($rs);
            exit;
        }
        $data = [
            "uid" => $uid,
            "touid" => $liveId,
            'content' => $content,
            'addtime' => time()
        ];
        $exist = M('users_report')->where(["uid" => $uid, "touid" => $liveId,])->find();
        if ($exist) {
            $users_report = M('users_report')->where(['id' => $exist['id']])->save($data);
        } else {
            $users_report = M('users_report')->add($data);
        }
        if ($users_report) {
            $rs['code'] = 0;
            $rs['msg'] = '举报成功';
            echo json_encode($rs);
            exit;
        } else {
            $rs['code'] = 1003;
            $rs['msg'] = '举报失败';
            echo json_encode($rs);
            exit;
        }
    }

    /**
     * 收费房间扣费API
     */
    public function roomCharge()
    {
        if (!IS_POST) {
            $rs['code'] = 1000;
            $rs['msg'] = '客户端非法请求';
            echo json_encode($rs);
            exit;
        }

        $liveType = (int)I('live_type');
        $liveId = I('liveid');
        $stream = trim(I('stream'));
        $touid = $liveType ? 0 : $liveId;

        $uid = $this->getUserId();
        $rs = ['code' => 0, 'msg' => '', 'info' => []];
        $userinfo = M('users')->where(['id' => $uid])->find();

        if ($uid <= 0) {
            $rs['code'] = 700;
            $rs['msg'] = 'Token错误或已过期，请重新登录';
            echo json_encode($rs);
            exit;
        }

        if ($liveType == 0) {//单人直播
            if (!$stream) {
                $rs['code'] = 1005;
                $rs['msg'] = '流地址不存在';
                echo json_encode($rs);
                exit;
            }
            $islive = M('users_live')->where(['uid' => $liveId])->find();
            if (!$islive || $islive['islive'] == 0) {
                $rs['code'] = 1005;
                $rs['msg'] = '直播已结束';
                echo json_encode($rs);
                exit;
            }
            if ($islive['type'] != 2) {
                $rs['code'] = 1006;
                $rs['msg'] = '该房间非扣费房间';
                echo json_encode($rs);
                exit;
            }
            //CNY
            $amount = $islive['type_val'];
            if ($amount <= 0) {
                $rs['code'] = 1007;
                $rs['msg'] = '房间费用有误';
                echo json_encode($rs);
                exit;
            }
            $showId = $islive['starttime'];
        } elseif ($liveType == 1) {//多人直播
            $multiLive = MultiLive::getInstance()->getMultiShowInfo($liveId);
            if ($multiLive['status'] == 0) {
                $rs['code'] = 1005;
                $rs['msg'] = '直播关闭';
                echo json_encode($rs);
                exit;
            } elseif ($multiLive['status'] == 2) {
                $rs['code'] = 1005;
                $rs['msg'] = '直播休息中';
                echo json_encode($rs);
                exit;
            }

            $amount = $multiLive['room_live_coin'];
            $showId = 0;
        } else {
            $rs['code'] = 1000;
            $rs['msg'] = '直播类型错误';
            echo json_encode($rs);
            exit;
        }

        if (Live::getInstance()->isRoomCharged($uid, $touid, $liveType, $liveId, $stream)) {
            $rs['code'] = 1005;
            $rs['msg'] = '已扣过费用';
            echo json_encode($rs);
            exit;
        }

        //获取用户的余额
        $userCoin = Live::getInstance()->getUserCoin($userinfo['user_login'], $userinfo['user_pass']);
        if ($userCoin < $amount) {
            $rs['code'] = 1008;
            $rs['msg'] = '余额不足';
            echo json_encode($rs);
            exit;
        }

        //扣费
        $res = Live::getInstance()->deducteUserCoin($userinfo['user_login'], $userinfo['user_pass'], $amount);
        if (is_array($res)) {
            if ($res['code'] != 200) {
                echo '{"errno":"1001","data":"","msg":"' . $res['message'] . '"}';
                exit;
            }
        } else {
            echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
            exit;
        }

        //更新直播 映票 累计映票
        $liveUser = $touid ? M('users')->where(['id' => $touid])->find() : [];
        if ($liveUser && $liveUser['votes'] > $amount) {
            M()->execute("UPDATE __PREFIX__users SET votes=votes-{$amount}, votestotal=votestotal+{$amount} WHERE id='{$touid}'");
        }

        //更新直播 映票 累计映票
        Live::getInstance()->addUserCoinRecord([
            "type" => 'expend',
            "action" => 'roomcharge',
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => 0,
            "giftcount" => 0,
            'total_livecoin' => Live::getInstance()->cnyToLiveCoin($amount),
            "showid" => $showId,
            "addtime" => time(),
            'live_type' => $liveType,
            'liveid' => $liveId,
            'stream' => $stream,
        ]);

        $user = M('users')->field(['coin', 'livecoin'])->where(['id' => $uid])->find();
        $rs['coin'] = $user['coin'];
        $rs['livecoin'] = $user['livecoin'];
        echo json_encode($rs);
    }

    /* 守护 */
    public function buyKeeper()
    {
        $touid = I('touid');
        $giftid = I('giftid');
        $buytype = I('buytype');
        $giftcount = I('buytime');
        $showid = I('showid');
        $liveType = (int)I('live_type');

        $config = $this->config;
        $User = M("users");
        $uid = $this->getUserId();
        $userinfo = $User->field('coin,token,expiretime,user_nicename,avatar')->where("id='{$uid}'")->find();
        if ($uid <= 0) {
            $data['errno'] = 1001;
            $data['data'] = '';
            $data['msg'] = 'Token过期，请重新登录';
            echo json_encode($data);
            exit;
        }
        if ($touid == $uid) {
            $data['errno'] = 1003;
            $data['data'] = '';
            $data['msg'] = '自己不能守护自己';
            echo json_encode($data);
            exit;
        }
        if ($buytype == 'month') {
            $giftid = 2;
            $type = 0;
            $add = 60 * 60 * 24 * 30 * $giftcount;
        } else {
            $giftid = 3;
            $type = 1;
            $add = 60 * 60 * 24 * 30 * 12 * $giftcount;
        }
        $action = 'buytool_' . $giftid;
        $keeperinfo = M("tools")->where("id='{$giftid}'")->find();

        $total = $giftcount * $keeperinfo['needcoin'];
        $totalexperience = $total * $config['experience_rate'];
        $addtime = time();
        if ($userinfo['coin'] < $total) {
            $data['errno'] = 1002;
            $data['data'] = '';
            $data['msg'] = '余额不足';
            echo json_encode($data);
            exit;
        }

        /* 更新用户余额 消费 */
        M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total},experience=experience+{$totalexperience} where id='{$uid}'");

        /* 更新直播 映票 累计映票 */
        M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$touid}'");

        /* 消费记录 */
        Live::getInstance()->addUserCoinRecord([
            "type" => 'expend',
            "action" => $action,
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "showid" => $showid,
            "addtime" => $addtime,
            'live_type' => $liveType,
            'liveid' => $touid,
        ]);

        /* 守护记录 */
        $keeperinfo = M("keeper")->where("uid='{$uid}' and touid='{$touid}'")->find();
        if ($keeperinfo) {
            if ($keeperinfo['endtime'] > $addtime) {
                /* 未过期 */
                M()->execute("update __PREFIX__keeper set endtime=endtime+{$add},type='{$type}' where id='{$keeperinfo['id']}'");
            } else {
                /* 已过期 */
                $endtime = $addtime + $add;
                M()->execute("update __PREFIX__keeper set endtime='{$endtime}',type='{$type}' where id='{$keeperinfo['id']}'");
            }
        } else {
            /* 未守护 */
            $endtime = $addtime + $add;
            M("keeper")->add(["uid" => $uid, "touid" => $touid, "buytime" => $addtime, "endtime" => $endtime, "type" => $type]);
        }

        $userinfo2 = $User->field("consumption,coin")->where("id='{$uid}'")->find();
        $level = User::getInstance()->getLevel($userinfo2['consumption']);
        $gifttoken = md5(md5($action . $uid . $touid . $giftid . $giftcount . $total . $showid . $addtime));
        $result = [
            "type" => 'expend',
            "action" => $action,
            "uid" => $uid,
            "touid" => $touid,
            "giftid" => $giftid,
            "giftcount" => $giftcount,
            "totalcoin" => $total,
            "showid" => $showid,
            "addtime" => $addtime,
            "gifttoken" => $gifttoken,
            "giftname" => $keeperinfo['title'],
            "nicename" => $userinfo['user_nicename'],
            "avatar" => $userinfo['avatar'],
            "level" => $level,
        ];
        $redis = CRedis::getInstance();
        $redis->set($result['gifttoken'], json_encode($result));

        $data['errno'] = 0;
        $data['data'] = [
            'level' => $level,
            'coin' => $userinfo2['coin'],
            'gifttoken' => $gifttoken,
        ];
        $data['msg'] = '购买成功';
        echo json_encode($data);
        exit;
    }

    /*判断用户是否是守护*/
    public function isGuard()
    {
        $uid = $this->getUserId();
        $showid = I("showid");
        $rs = ['code' => 0, 'msg' => '', 'isGurd' => '0'];

        //获取后台配置的守护发言文字
        $guard_digitsNum = M("config")->where("id=1")->getField("guard_digitsNum");
        $rs['guard_digitsNum'] = $guard_digitsNum;
        $nowtime = time();
        if ($uid) {
            $info = M("users_guard_lists")->where("uid={$uid} and liveuid={$showid} and effectivetime>{$nowtime}")->find();
            if ($info) {
                $rs['isGuard'] = 1;
            } else {
                $rs['isGuard'] = 0;
            }
        }

        echo json_encode($rs);
        exit;
    }

    /**
     * 判断频道是否禁言
     */
    public function checkChannelShutUp()
    {
        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');

        $data = Live::getInstance()->isChannelShutUp($liveType, $liveId);
        if (false === $data) {
            return $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
        }

        $this->renderJson([
            'is_shut_up' => (bool)$data,
        ]);
    }

    /**
     * 频道禁言
     */
    public function channelShutUp()
    {
        if (!IS_POST) {
            return $this->renderJson([], 400, '客户端非法调用');
        }

        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');
        $userId = $this->getUserId();

        $rs = Live::getInstance()->channelShutUp($liveType, $liveId, $userId);
        if (false === $rs) {
            return $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
        }

        $this->renderJson($rs);
    }

    /**
     * 删除禁言
     */
    public function delChannelShutUp()
    {
        if (!IS_POST) {
            return $this->renderJson([], 400, '客户端非法调用');
        }

        $liveType = (int)I('live_type');
        $liveId = (int)I('liveid');
        $userId = $this->getUserId();

        $rs = Live::getInstance()->delChannelShutUp($liveType, $liveId, $userId);
        if (false === $rs) {
            return $this->renderJson([], 400, Live::getInstance()->getLastErrMsg());
        }

        $this->renderJson($rs);
    }

}
