<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Helpers\Func;

class PlaybackController extends HomebaseController
{
    public function index()
    {
        $touid = $_GET['touid'];
        $liverecord = M("users_liverecord")->where("uid=" . $touid)->select() ?: [];
        foreach ($liverecord as $k => $v) {
            $time = $liverecord['endtime'] - $liverecord['starttime'];
            $liverecord[$k]['time'] = Func::formatTime($time);
        }

        $this->assign("liverecord", $liverecord);
        $this->display();
    }

}
