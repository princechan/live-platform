<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Live\Ads;
use Common\Lib\Live\Live;
use Common\Lib\Live\MultiLive;

class IndexController extends HomebaseController
{
    public function index()
    {
        //直播公告
        $liveAnnouncement = M('announcement')
            ->where(['state' => 0])
            ->order(['sort' => 'asc', 'id' => 'desc'])
            ->limit(0, 3)
            ->select() ?: [];
        $groupMap = Live::$liveAnnouncementGroupMap;
        foreach ($liveAnnouncement as &$liveAn) {
            $liveAn['date'] = date('m/d', strtotime($liveAn['updated_at']));
            $liveAn['title'] = mb_substr($liveAn['title'], 0, 15);
            $liveAn['sm_title'] = isset($groupMap[$liveAn['category']]) ? $groupMap[$liveAn['category']] : '活动';
        }

        //首页右边栏广告
        $indexRightAds = Ads::getInstance()->getIndexRightAds();

        //multi live
        $multiLive = MultiLive::getInstance()->getList() ?: [];
        foreach ($multiLive as &$value) {
            $value['live_id'] = MultiLive::ROUTE_PREFIX . $value['id'];
        }

        $this->assign([
            'current' => 'index',
            'liveAnnouncement' => $liveAnnouncement,
            'indexRightAds' => $indexRightAds,
            'multiLive' => $multiLive,
        ]);

        $this->display('index');
    }

    /**
     * 获取直播列表，在线->不在线，时间倒序
     */
    public function getSingleLives()
    {
        $page = (int)I('page', 8);
        $page = $page <= 1 ?: $page;
        $limit = (int)I('limit');
        if ($limit <= 0) {
            $limit = 8;
        }

        $offset = ($page - 1) * $limit;
        $list = M('users_live')
            ->order(['islive' => 'DESC', 'starttime' => 'DESC'])
            ->limit($offset, $limit)->select() ?: [];

        foreach ($list as &$v) {
            $v['nums'] = Live::getInstance()->getRoomLivedNum(0, $v['uid']);
        }

        $this->renderJson([
            'has_more' => $limit == count($list),
            'list' => $list,
        ]);
    }

    public function translate()
    {
        $prefix = C("DB_PREFIX");

        if ($_REQUEST['keyword'] != '') {
            $where = "user_type='2'";
            $keyword = $_REQUEST['keyword'];
            $where .= " and (id='{$keyword}' OR user_nicename like '%{$keyword}%')";
            $_GET['keyword'] = $_REQUEST['keyword'];
        } else {
            $where = "u.user_type='2' and l.islive='1' ";
        }
        $auth = M("users");
        $pagesize = 18;
        if ($_REQUEST['keyword'] == "") {
            $count = M("users_live l")
                ->field("l.user_nicename,l.avatar,l.uid,l.stream,l.title,l.city,l.islive")
                ->join("left join {$prefix}users u on u.id=l.uid")
                ->where($where)
                ->order("l.starttime desc")
                ->count();
            $Page = new \Page2($count, $pagesize);
            $show = $Page->show();
            $lists = M("users_live l")
                ->field("l.user_nicename,l.avatar,l.uid,l.stream,l.title,l.city,l.islive")
                ->join("left join {$prefix}users u on u.id=l.uid")
                ->where($where)
                ->order("l.starttime desc")
                ->limit($Page->firstRow . ',' . $Page->listRows)
                ->select();
            $msg["info"] = '抱歉,没有找到关于"';
            $msg["name"] = '';
            $msg["result"] = '"的搜索结果';
            $msg["type"] = '0';
        } else {
            $count = $auth->where($where)->count();
            $Page = new \Page2($count, $pagesize);
            $show = $Page->show();
            $lists = $auth->where($where)->order("consumption desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
            $msg["info"] = '共找到' . $count . '个关于"';
            $msg["name"] = $_REQUEST['keyword'];
            $msg["result"] = '"的搜索结果';
            $msg["type"] = '1';
        }

        $this->assign('lists', $lists);
        $this->assign('msg', $msg);
        $this->assign('page', $show);
        $this->assign('formget', $_GET);

        $this->display();
    }

    /**
     * 直播公告列表页
     */
    public function liveAnnouncement()
    {
        $category = trim(I('category', 'huodong'));
        $p = (int)I('p');
        if ($p < 1) {
            $p = 1;
        }
        $limit = 20;
        $offset = ($p - 1) * $limit;
        $count = (int)M('announcement')->where(['state' => 0, 'category' => $category])->count();
        $lists = M('announcement')->where(['state' => 0, 'category' => $category])->order(['sort' => 'asc', 'id' => 'desc'])
            ->limit($offset, $limit)
            ->select() ?: [];

        $Page = new \Page2($count, $limit);
        $pagination = $Page->show();

        foreach ($lists as &$li) {
            $li['date'] = date('Y/m/d', strtotime($li['updated_at']));
            $li['title'] = mb_substr($li['title'], 0, 15);
        }

        $groupMap = Live::$liveAnnouncementGroupMap;

        $this->assign([
            'lists' => $lists,
            'pagination' => $pagination,
            'category' => $category,
            'categoryName' => isset($groupMap[$category]) ? $groupMap[$category] : '活动',
            'groupMap' => Ads::getInstance()->getLiveAnnouncementGroupMap(),
        ]);

        $this->display('liveAnnouncement');
    }

    public function viewAnnouncement()
    {
        $id = (int)I('id');

        $data = $id ? M('announcement')->where(['id' => $id])->find() : [];
        if (!$data) {
            $this->error('找不到', '/');
        }

        $data['date'] = date('Y/m/d', strtotime($data['updated_at']));

        $this->assign([
            'data' => $data,
        ]);

        $this->display('viewAnnouncement');
    }

    /**
     * 获取环境
     */
    public function getEnv()
    {
        $this->renderJson([
            'env' => ENV,
            'description' => 'dev-开发环境，prod-线上环境',
        ]);
    }

}
