<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Live;

class PersonalController extends HomebaseController
{
    /**
     * 个人中心-首页方法
     */
    public function index()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $getgif = Live::getInstance()->getGif($uid);
        //累计经验
        $experience = M('users')->where(['id' => $uid])->getField('consumption');
        //当前等级
        $level = User::getInstance()->getLevel($experience);
        $level_up = $level + 1;
        $experlevel = M("experlevel")->field("level_up")->where(['levelid' => $level_up])->find();
        $cha = $experlevel['level_up'] - $experience;

        //直播券账变
        $pagesize = 20;
        $count = M('users_coinrecord')->where("uid=$uid OR touid=$uid")->count()
            + M('users_chargelivecoin_admin')->where(['touid' => $uid])->count()
            + M('users_sendgift_win')->where(['uid' => $uid])->count();

        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();

        $sql = <<<SQL
(SELECT `id`, `uid`, `touid`, `addtime`, '' `content`, `total_livecoin` `livecoin`, `type`, `action`, 1 `from`
FROM `cmf_users_coinrecord` WHERE (uid=$uid OR touid=$uid))
UNION
(SELECT `id`, `touid`, `touid` `uid`, `addtime`, '直播充券' `content`, `livecoin`, '' `type`, '' `action`, 2 `from` 
FROM `cmf_users_chargelivecoin_admin` WHERE touid=$uid)
UNION
(SELECT `id`, `uid`, `uid` `touid` , `addtime`, '中奖' `content`, `win_livecoin` `livecoin`, '' `type`, '' `action`, 3 `from` 
FROM `cmf_users_sendgift_win` WHERE uid=$uid)
ORDER BY addtime DESC LIMIT $Page->listRows OFFSET $Page->firstRow
SQL;

        $model = M();
        $model->query($sql);
        $coinRecord = $model->query($sql);

        $remarkMap = [
            'sendgift' => '赠送礼物',
            'sendbarrage' => '弹幕扣费',
            'roomcharge' => '房间扣费',
            'loginbonus' => '登录奖励',
            'buyvip' => '购买VIP',
            'buycar' => '购买坐骑',
            'buyliang' => '购买靓号',
            'sharereward' => '分享奖励',
            'game_bet' => '游戏下注',
            'game_return' => '游戏返还',
            'game_win' => '游戏获胜',
            'game_banker' => '庄家收益',
            'timecharge' => '计时扣费',
            'bid_price' => '竞拍费用',
            'price_bond' => '缴纳保证金',
            'price_bond_return' => '退还保证金',
        ];

        $toRemarkMap = [
            'sendgift' => '收到礼物',
            'sendbarrage' => '收到弹幕扣费',
            'roomcharge' => '收到房间扣费',
            'loginbonus' => '登录奖励',
            'buyvip' => '购买VIP',
            'buycar' => '购买坐骑',
            'buyliang' => '购买靓号',
            'sharereward' => '分享奖励',
            'game_bet' => '游戏下注',
            'game_return' => '游戏返还',
            'game_win' => '游戏获胜',
            'game_banker' => '庄家收益',
            'timecharge' => '计时扣费',
            'bid_price' => '竞拍费用',
            'price_bond' => '缴纳保证金',
            'price_bond_return' => '退还保证金',
        ];

        foreach ($coinRecord as &$coinR) {
            $coinR['addtime'] = date('Y/m/d H:i', $coinR['addtime']);
            if ($coinR['from'] == 1) {
                if ($coinR['uid'] == $uid) {
                    $coinR['content'] = isset($remarkMap[$coinR['action']]) ? $remarkMap[$coinR['action']] : '系统扣减';
                } else {
                    $coinR['content'] = isset($toRemarkMap[$coinR['action']]) ? $toRemarkMap[$coinR['action']] : '系统增加';
                }
            }
        }

        $this->assign("info", $info);
        $this->assign("getgif", $getgif[0]);
        $this->assign("experience", $experience);
        $this->assign("level", $level);
        $this->assign("cha", $cha);
        $this->assign('coinRecord', $coinRecord);
        $this->assign('page', $show);

        $this->display();
    }

    /**
     * 个人中心-基本资料展示
     **/
    public function modify()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $this->assign("personal", 'Set');
        $this->display();
    }

    /**
     * 个人中心-基本资料修改
     **/
    public function edit_modify()
    {
        $sex = I('sex');
        $birthday = trim(I('birthday'));
        $userNicename = trim(I('user_nicename'));
        $signature = trim(I('signature'));

        if (!IS_POST) {
            return $this->renderJson([], 400, '客户端非法调用');
        }

        $uid = $this->getUid();
        if ($uid <= 0) {
            return $this->renderJson([], 400, '登录失效,请重新登录');
        }

        $data = User::getInstance()->editModify($uid, $sex, $birthday, $userNicename, $signature);
        if (false === $data) {
            return $this->renderJson([], 400, User::getInstance()->getLastErrMsg());
        }

        return $this->renderJson();
    }

    /**
     * 个人中心-头像展示
     **/
    public function photo()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $this->assign("personal", 'Set');
        $this->display();
    }

    /**
     * 个人中心-修改头像
     */
    public function edit_photo()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $callback = [
                'error' => 0,
                'type' => "登录失效,请重新登录"
            ];
            echo json_encode($callback);
            exit;
        }
        $url = urldecode($_GET['avatar']);
        if (!empty($url)) {
            $avatar = $url . '?imageView2/2/w/600/h/600'; //600 X 600
            $avatar_thumb = $url . '?imageView2/2/w/200/h/200'; // 200 X 200
            $data = [
                "id" => $uid,
                "avatar" => $avatar,
                "avatar_thumb" => $avatar_thumb,
            ];
            $result = M('users')->save($data);
            if ($result) {
                $callback = [
                    'error' => 1,
                    'type' => "头像修改成功"
                ];
            } else {
                $callback = [
                    'error' => 0,
                    'type' => "头像修改失败"
                ];
            }
        } else {
            $callback = [
                'error' => 0,
                'type' => "图片处理失败"
            ];
        }
        echo json_encode($callback);
        die;
    }

    /**
     * 个人中心-我的认证
     */
    public function card()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $this->assign("uid", $uid);
        $auth = User::getInstance()->getAuthStatus($uid);
        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $this->assign("auth", $auth);
        $this->assign("personal", 'card');
        $this->display();
    }

    /**
     * 个人中心-我的认证-身份证上传
     */
    function upload()
    {
        $saveName = I('saveName') . "_" . time();
        $config = [
            'replace' => true,
            'rootPath' => './' . C("UPLOADPATH"),
            'savePath' => '/rz/',
            'maxSize' => 0,//500K
            'saveName' => $saveName,
            //'exts'       =>    array('jpg', 'png', 'jpeg'),
            'autoSub' => false,
        ];
        $upload = new \Think\Upload($config);//
        $info = $upload->upload();
        //开始上传
        if ($info) {
            //上传成功
            //写入附件数据库信息
            $first = array_shift($info);

            if (!empty($first['url'])) {
                $url = $first['url'];
            } else {
                $url = C("TMPL_PARSE_STRING.__UPLOAD__") . 'rz/' . $first['savename'];
            }

            echo json_encode(["ret" => 200, 'data' => ["url" => $url], 'msg' => $saveName]);
            //$this->ajaxReturn(sp_ajax_return(array("file"=>$file),"上传成功！",1),"AJAX_UPLOAD");
        } else {
            //上传失败，返回错误
            //$this->ajaxReturn(sp_ajax_return(array(),$upload->getError(),0),"AJAX_UPLOAD");
            echo json_encode(["ret" => 0, 'file' => '', 'msg' => $upload->getError()]);
        }
    }

    /**
     * 个人中心-我的认证-认证信息写入数据库
     **/
    function authsave()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo json_encode(["ret" => 0, 'data' => [], 'msg' => '您尚未登录']);
            die;
        }

        $data['uid'] = $uid;
        $data['real_name'] = I("real_name");
        $data['mobile'] = I("mobile");
        $data['card_no'] = I("card_no");
        $data['bank_name'] = I("bank_name");
        $data['accounts_province'] = I("accounts_province");
        $data['accounts_city'] = I("accounts_city");
        $data['sub_branch'] = I("sub_branch");
        $data['cer_type'] = I("cer_type");
        $data['cer_no'] = I("cer_no");
        $data['front_view'] = I("front_view");
        $data['back_view'] = I("back_view");
        $data['handset_view'] = I("handset_view");
        $data['status'] = 0;
        $data['addtime'] = time();
        $authid = M("users_auth")->where("uid='{$data['uid']}'")->getField('uid');
        if ($authid) {
            $result = M("users_auth")->where("uid='{$authid}'")->save($data);
        } else {
            $result = M("users_auth")->add($data);
        }
        if ($result !== false) {
            echo json_encode(["ret" => 200, 'data' => [], 'msg' => '']);
        } else {
            echo json_encode(["ret" => 0, 'data' => [], 'msg' => '提交失败，请重新提交']);
        }
        die;
    }

    /**
     * 个人中心-我关注的
     **/
    public function follow()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $live = M("users_attention");
        $attention = $live->where("uid=$uid")->select();
        foreach ($attention as $k => $v) {
            $users = User::getInstance()->getUserInfo($v['touid']);
            $attention[$k]['users'] = $users;
            $attention[$k]['follow'] = User::getInstance()->getFollownums($v['touid']);
            $attention[$k]['fans'] = User::getInstance()->getFansnums($v['touid']);
        }

        $this->assign("attention", $attention);
        $this->assign("personal", 'follow');
        $this->display();
    }

    /**
     * 个人中心-我关注的-取消关注
     **/
    public function follow_dal()
    {
        $touid = $_GET['followID'];
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"0","msg":"您尚未登录"}';
            die;
        }

        $del_follow = M("users_attention")->where("touid=$touid and uid=$uid")->delete();
        if ($del_follow !== false) {
            echo '{"state":"0","msg":"取消关注"}';
            die;
        } else {
            echo '{"state":"1","msg":"取消失败"}';
            die;
        }
    }

    public function follow_add()
    {
        $touid = $_GET['touid'];
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1","msg":"您尚未登录"}';
            die;
        }

        $data = [
            "uid" => $uid,
            "touid" => $touid
        ];
        $result = M("users_attention")->add($data);
        if ($result !== false) {
            M('users_black')->where("touid=$touid and uid=$uid")->delete();
            echo '{"state":"0","msg":"关注成功"}';
        } else {
            echo '{"state":"1","msg":"关注失败"}';
        }
        die;
    }

    /**
     * 个人中心-我的粉丝
     **/
    public function fans()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $live = M("users_attention");
        $attention = $live->where("touid=$uid")->select();
        foreach ($attention as $k => $v) {
            $users = User::getInstance()->getUserInfo($v['uid']);
            $attention[$k]['users'] = $users;
            $attention[$k]['follow'] = User::getInstance()->getFollownums($v['uid']);
            $attention[$k]['fans'] = User::getInstance()->getFansnums($v['uid']);
            $isAttention = User::getInstance()->isAttention($uid, $v['uid']);
            $attention[$k]['attention'] = $isAttention;
        }

        $this->assign("attention", $attention);
        $this->assign("personal", 'follow');
        $this->display();
    }

    /**
     * 黑名单
     */
    public function namelist()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $live = M("users_black");
        $attention = $live->where("uid=$uid")->select();
        foreach ($attention as $k => $v) {
            $users = User::getInstance()->getUserInfo($v['touid']);
            $attention[$k]['users'] = $users;
            $attention[$k]['follow'] = User::getInstance()->getFollownums($v['touid']);
            $attention[$k]['fans'] = User::getInstance()->getFansnums($v['touid']);
            $attention[$k]['attention'] = User::getInstance()->isAttention($uid, $v['touid']);;
        }

        $this->assign("attention", $attention);
        $this->assign("personal", 'follow');
        $this->display();
    }

    /**
     * 删除黑名单
     */
    public function list_del()
    {
        $touid = $_GET['touid'];
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1001","msg":"您尚未登录"}';
            die;
        }

        $isBlack = isBlack($uid, $touid);
        if ($isBlack == 0) {
            echo '{"state":"1000","msg":"该用户不在你的黑名单内"}';
            exit;
        } else {
            $attention = M('users_black')->where("touid=$touid and uid=$uid")->delete();
            if ($attention) {
                echo '{"state":"0","msg":"移除成功"}';
                exit;
            } else {
                echo '{"state":"1001","msg":"移除失败"}';
                exit;
            }
        }
    }

    /**
     * 拉黑操作 如果我已经关注这个主播 同时会删除关注状态但是不会清除粉丝
     */
    public function blacklist()
    {
        $touid = $_GET['touid'];
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1001","msg":"您尚未登录"}';
            die;
        }

        $isBlack = User::getInstance()->isBlack($uid, $touid);
        if ($isBlack == 1) {
            echo '{"state":"1000","msg":"你已经将该用户拉黑"}';
            exit;
        } else {
            $isAttention = User::getInstance()->isAttention($uid, $touid);
            if ($isAttention) {
                M('users_attention')->where("touid=$touid and uid=$uid")->delete();
            }
            $data = [
                "uid" => $uid,
                "touid" => $_GET['touid']
            ];
            $result = M('users_black')->add($data);
            if ($result) {
                echo '{"state":"0","msg":"拉黑成功"}';
                exit;
            } else {
                echo '{"state":"1001","msg":"拉黑失败"}';
                exit;
            }
        }
    }

    /**
     * 个人中心-管理员管理中心
     **/
    public function admin()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $admin = M('users_livemanager')->where("liveuid=$uid")->select();
        foreach ($admin as $k => $v) {
            $users = User::getInstance()->getUserInfo($v['uid']);
            $admin[$k]['users'] = $users;
            $admin[$k]['follow'] = User::getInstance()->getFollownums($v['uid']);
            $admin[$k]['fans'] = User::getInstance()->getFansnums($v['uid']);
            $admin[$k]['attention'] = User::getInstance()->isAttention($uid, $v['uid']);
        }

        $this->assign("admin", $admin);
        $this->assign("personal", 'follow');
        $this->display();
    }

    /**
     * 个人中心-管理员管理中心-取消管理员
     **/
    function admin_del()
    {
        $touid = $_GET['touid'];
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1000","msg":"您尚未登录"}';
            die;
        }

        if ($touid) {
            $rst = M("users_livemanager")->where("uid=" . $touid . " and liveuid=" . $uid)->delete();
            if ($rst) {
                echo '{"state":"0","msg":"管理取消成功"}';
                exit;
            } else {
                echo '{"state":"1000","msg":"管理取消失败"}';
                exit;
            }
        } else {
            echo '{"state":"1001","msg":"数据传入失败"}';
            exit;
        }
    }

    /**
     * 个人中心-提现中心
     **/
    public function exchange()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $level = User::getInstance()->getLevel($info['consumption']);
        //等级限制金额
        $limitcash = User::getInstance()->getLevelSection($level);
        $config = Func::getPrivateConfig();
        //提现比例
        $cash_rate = $config['cash_rate'];
        //剩余票数
        $votes = $info['votes'];

        //总可提现数
        $total = floor($votes / $cash_rate);
        $nowtime = time();
        //当天0点
        $today = date("Ymd", $nowtime);
        $today_start = strtotime($today) - 1;
        //当天 23:59:59
        $today_end = strtotime("{$today} + 1 day");
        //已提现
        $users_cashrecord = M("users_cashrecord")->query('select uid,sum(money) as hascash from cmf_users_cashrecord where uid=' . $uid . ' and addtime>' . $today_start . ' and addtime<' . $today_end . ' and status!=2');

        $hascash = $users_cashrecord[0]['hascash'];

        if (!$hascash) {
            $hascash = 0;
        }
        //今天可体现等级提现区间 - 今日提过的
        $todaycancash = (string)$limitcash - $hascash;

        //今天能提
        if ($todaycancash < $total) {
            $todaycash = $todaycancash;
        } else {
            $todaycash = $total;
        }

        $rs = [
            "votes" => $votes,
            "todaycash" => $todaycash,
            "total" => $total,
        ];
        $this->assign("info", $info);
        $this->assign("rs", $rs);
        $this->assign('exchange_rate', Live::getInstance()->getExchangeRate());
        $this->assign("personal", 'card');
        $this->display();
    }

    /**
     * 个人中心-提现中心开始提现
     **/
    public function edit_exchange()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            echo '{"state":"1003","msg":"登录失效，请重新登录"}';
            exit;
        }

        $isrz = M("users_auth")->field("status")->where("uid=" . $uid)->find();
        if (!$isrz || $isrz['status'] != 1) {
            echo '{"state":"1003","msg":"请先进行身份认证"}';
            exit;
        }
        $info = User::getInstance()->getUserPrivateInfo($uid);
        $level = User::getInstance()->getLevel($info['consumption']);
        //等级限制金额
        $limitcash = User::getInstance()->getLevelSection($level);
        $config = Func::getPrivateConfig();

        //提现比例
        $cash_rate = $config['cash_rate'];
        //剩余票数
        $votes = $info['votes'];
        //总可提现数
        $total = floor($votes / $cash_rate);

        //已提现
        $nowtime = time();
        //当天0点
        $today = date("Ymd", $nowtime);
        $today_start = strtotime($today) - 1;
        //当天 23:59:59
        $today_end = strtotime("{$today} + 1 day");
        $users_cashrecord = M("users_cashrecord")->query('select uid,sum(money) as hascash from cmf_users_cashrecord where uid=' . $uid . ' and addtime>' . $today_start . ' and addtime<' . $today_end . ' and status!=2');
        $hascash = $users_cashrecord[0]['hascash'];
        if (!$hascash) {
            $hascash = 0;
        }
        //今天可体现
        $todaycancash = $limitcash - $hascash;

        //今天能提
        if ($todaycancash < $total) {
            $todaycash = $todaycancash;
        } else {
            $todaycash = $total;
        }

        if ($todaycash <= 0) {
            echo '{"state":"1001","msg":"今日提现已达上限"}';
            exit;
        }

        $cashvotes = $todaycash * $cash_rate;

        $nowtime = time();

        $data = [
            "uid" => $uid,
            "money" => $todaycash,
            "votes" => $cashvotes,
            "orderno" => $uid . '_' . $nowtime . rand(100, 999),
            "status" => 0,
            "addtime" => $nowtime,
            "uptime" => $nowtime,
        ];
        $rs = M("users_cashrecord")->add($data);
        if ($rs) {
            M()->execute("update __PREFIX__users set votes=votes-{$cashvotes},consumption=consumption+{$total} where id='{$uid}'");
            echo '{"state":"0","msg":"提现成功"}';
            exit;
        } else {
            echo '{"state":"1002","msg":"提现失败，请重试"}';
            exit;
        }
    }

    /**
     * 修改密码
     */
    public function updatepass()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $this->assign("personal", 'Set');
        $this->display();
    }

    /**
     * 执行密码修改
     */
    public function savepass()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $rs['code'] = 1001;
            $rs['msg'] = '您尚未登录';
            echo json_encode($rs);
            exit;
        }

        //旧密码
        $oldpass = I('oldpass');
        //新密码
        $newpass = I('newpass');
        //确认密码
        $repass = I('repass');
        $rs = [];
        if ($newpass !== $repass) {
            $rs['code'] = 800;
            $rs['msg'] = '两次密码不一致';
            echo json_encode($rs);
            exit;
        }
        $authcode = 'rCt52pF2cnnKNB3Hkp';
        $oldpass = "###" . md5(md5($authcode . $oldpass));
        $pwd = "###" . md5(md5($authcode . $newpass));
        $check = $this->passcheck($newpass);
        if ($check == 0) {
            $rs['code'] = 1001;
            $rs['msg'] = '密码6-12位数字与字母';
            echo json_encode($rs);
            exit;
        }
        if ($check == 2) {
            $rs['code'] = 1002;
            $rs['msg'] = '密码6-12位数字与字母';
            echo json_encode($rs);
            exit;
        }
        /* 密码判定 */
        $rt = M("users")->where("id='$uid' and user_pass='$oldpass' and user_type='2'")->find();
        if (empty($rt)) {
            $rs['code'] = 103;
            $rs['msg'] = '旧密码错误';
            echo json_encode($rs);
            exit;
        }
        $data = [];
        $User = M("users");
        //要修改的数据对象属性赋值
        $data['user_pass'] = $pwd;
        $map['id'] = $uid;
        //保存昵称到数据库
        $result = $User->where($map)->save($data);
        if ($result !== false) {
            $rs['code'] = 0;
            $rs['msg'] = '修改成功';
            echo json_encode($rs);
            exit;
        } else {
            $rs['code'] = 0;
            $rs['msg'] = '修改失败';
            echo json_encode($rs);
            exit;
        }
    }

    /**
     * 个人中心-直播记录
     **/
    public function live()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        if ($_REQUEST['start_time'] != '') {
            $where['starttime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['starttime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['starttime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        $this->assign('formget', $_GET);
        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $pagesize = 20;
        $User = M('users_liverecord');
        $Live = M('users');
        $coin = $Live->where("id=$uid")->getField("coin");
        $this->assign('coin', $coin);
        $count = $User->where($where)->count();
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();
        $lists = $User->field($this->field)->where($where)->order("showid desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('lists', $lists);
        $this->assign('page', $show);
        $this->assign('uid', $uid);
        $this->assign("personal", 'follow');
        $this->display();
    }

    /**
     * 密码检查
     * @param $user_pass
     * @return int
     */
    public function passcheck($user_pass)
    {
        $num = ereg("^[a-zA-Z]{6,12}$", $user_pass);
        $word = ereg("^[0-9]{6,12}$", $user_pass);
        $check = ereg("^[a-zA-Z0-9]{6,12}$", $user_pass);
        if ($num || $word) {
            return 2;
        }
        if (!$check) {
            return 0;
        }
        return 1;
    }

    /**
     * 个人中心-充值记录
     **/
    public function charge()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['touid'] = $uid;
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'] . ' 23:59:59')];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'] . ' 23:59:59')]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        $pagesize = 20;
        $count = M('users_chargelivecoin_admin')->where($where)->count();
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();
        $lists = M('users_chargelivecoin_admin')->where($where)
            ->order('id DESC')
            ->limit($Page->firstRow . ',' . $Page->listRows)->select() ?: [];

        $this->assign('formget', $_GET);
        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);
        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $this->assign('lists', $lists);
        $this->assign('page', $show);
        $this->assign("personal", 'transaction');

        $this->display();
    }

    /**
     * 个人中心-消费记录
     * $count查询当前用户的充值记录条数
     * $users获取被关注主播的信息
     * $_REQUEST['start_time']查询开始时间
     * $_REQUEST['end_time']查询结束时间
     * $Live实例化个人信息
     * $gift实例化礼物信息
     * $count查询满足要求的总记录数
     * $Page实例化分页类 传入总记录数和每页显示的记录数(25)
     * $show分页显示输出
     **/
    public function spend()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        $this->assign('formget', $_GET);

        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);
        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);
        $pagesize = 20;
        $Live = M('users');
        $gift = M('gift');

        $coin = $Live->where("id=$uid")->getField("coin");
        $this->assign('coin', $coin);
        $count = M('users_coinrecord')->where($where)->count();
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();
        $lists = M('users_coinrecord')->field($this->field)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)
            ->order("addtime desc")->select() ?: [];

        foreach ($lists as $k => $v) {
            $lists[$k]['liveinfo'] = $Live->where("id='{$v['touid']}'")->find();
            $lists[$k]['giftname'] = $gift->where("id={$v['giftid']}")->getField('giftname');
        }

        $this->assign('lists', $lists);
        $this->assign('page', $show);
        $this->assign("personal", 'transaction');

        $this->display();
    }

    /**
     * 个人中心-中奖记录
     * $count查询当前用户的充值记录条数
     * $_REQUEST['start_time']查询开始时间
     * $_REQUEST['end_time']查询结束时间
     * $Live实例化个人信息
     * $gift实例化礼物信息
     * $count查询满足要求的总记录数
     * $Page实例化分页类 传入总记录数和每页显示的记录数(25)
     * $show分页显示输出
     **/
    public function giftwin()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        //$where['action']=array("neq",'sendgift');
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        $this->assign('formget', $_GET);

        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);

        $pagesize = 20;
        $coin = M('users')->where(['id' => $uid])->getField("coin");
        $this->assign('coin', $coin);

        $count = M('users_sendgift_win')->where($where)->count();
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();
        $lists = M('users_sendgift_win')->field($this->field)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)
            ->order("addtime desc")->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['live_user_nicename'] = M('users')->where(['id' => $v['liveuid']])->getField('user_nicename');
            $lists[$k]['giftname'] = M('gift')->where(['id' => $v['giftid']])->getField('giftname');
        }

        $this->assign('lists', $lists);
        $this->assign('page', $show);
        $this->assign("personal", 'transaction');
        $this->display();
    }

    /**
     * 抢红包记录
     */
    public function getpacket()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        //$where['action']=array("neq",'sendgift');
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }


        $this->assign('formget', $_GET);

        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);

        $User = M("users");

        $pagesize = 20;
        $Redpacket = M('users_rob_redpackets'); // 实例化User对象
        $count = $Redpacket->where($where)->count();// 查询满足要求的总记录数
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $Redpacket->where($where)->order('addtime desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();

        foreach ($list as $k => $v) {
            $list[$k]['sendUserName'] = $User->where(['id' => $v['senduid']])->getField("user_nicename");
            $list[$k]['liveUserName'] = $User->where(['id' => $v['liveuid']])->getField("user_nicename");
        }

        $this->assign('list', $list);// 赋值数据集
        $this->assign('page', $show);// 赋值分页输出
        $this->assign("personal", 'transaction');
        $this->display(); // 输出模板
    }

    /**
     * 退红包记录
     */
    public function returnpacket()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        //$where['action']=array("neq",'sendgift');
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }


        $this->assign('formget', $_GET);

        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);

        $User = M("users");
        $backPacket = M("redpcakets_back");
        $Redpacket = M("users_send_redpackets");

        $pagesize = 20;

        $count = $backPacket->where("uid='{$uid}'")->where($where)->count();// 查询满足要求的总记录数
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $backPacket->where("uid='{$uid}'")->where($where)->order('addtime desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();

        foreach ($list as $k => $v) {
            $list[$k]['packetInfo'] = $Redpacket->where("id='{$v['packet_id']}'")->find();
            $list[$k]['liveUserName'] = $User->where("id='{$list[$k]['packetInfo']['roomid']}'")->getField("user_nicename");
        }


        $this->assign('list', $list);// 赋值数据集
        $this->assign('page', $show);// 赋值分页输出
        $this->assign("personal", 'transaction');
        $this->display(); // 输出模板
    }

    /**
     * 发红包记录
     */
    public function sendpacket()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->assign('jumpUrl', __APP__);
            $this->error('您尚未登录');
        }

        $where = [];
        $where['uid'] = $uid;
        //$where['action']=array("neq",'sendgift');
        if ($_REQUEST['start_time'] != '') {
            $where['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $where['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $where['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        $this->assign('formget', $_GET);

        $auth = User::getInstance()->getUserAuth($uid);
        $this->assign("auth", $auth);

        $info = User::getInstance()->getUserPrivateInfo($uid);
        $this->assign("info", $info);

        $User = M("users");
        $Redpacket = M("users_send_redpackets");

        $pagesize = 20;

        $count = $Redpacket->where("uid='{$uid}'")->where($where)->count();// 查询满足要求的总记录数
        $Page = new \Page2($count, $pagesize);
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $Redpacket->where("uid='{$uid}'")->where($where)->order('addtime desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();

        foreach ($list as $k => $v) {

            $list[$k]['liveUserName'] = $User->where("id='{$v['roomid']}'")->getField("user_nicename");
            if (!$list[$k]['liveUserName']) {
                unset($list[$k]['liveUserName']);
            }
        }

        $this->assign('list', $list);// 赋值数据集
        $this->assign('page', $show);// 赋值分页输出
        $this->assign("personal", 'transaction');
        $this->display(); // 输出模板
    }

}
