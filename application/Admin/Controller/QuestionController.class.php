<?php

/**
* 题库/目管理类
 */

namespace Admin\Controller;
use Common\Controller\AdminbaseController;
use Common\Lib\Activity\Activity;
use Common\Lib\Auth\Admin;
use Common\Lib\Activity\Question;

class QuestionController extends AdminbaseController
{


    function _initialize()
    {
        parent::_initialize();

    }

    /**
     * 题目管理列表
     */
    public function quesManager()
    {
        if(IS_POST)
        {
            $type           = I('post.html_type');
            $name           = I('post.html_name');
            $answer         = I('post.html_answer');
            $right_answer   = I('post.html_right_answer');

            if(empty($type))
                $this->error('题目类型不能为空');
            if(empty($name))
                $this->error('题目名不能为空');
            if(empty($answer))
                $this->error('选择答案不能为空');
            if(empty($right_answer))
                $this->error('正确答案不能为空');

            $arr = [
                'type'          => $type,
                'name'          => $name,
                'choose_answer' => json_encode($answer, JSON_UNESCAPED_UNICODE),
                'right_answer'  => $right_answer,
                'admin_id'      => $this->adminId,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            if(Question::getInstance()->addQuestion($arr))
            {
                $this->success(L('ADD_SUCCESS'), U("Question/quesManager"));
            }else{
                $this->error(L('ADD_FAILED'));
            }
        }

        $this->display('Activity/ques_add');
    }

    /**
     * 编辑题目
     */
    public function quesInfo()
    {
        if(IS_POST)
        {
            $id             = intval(I('post.html_id'));
            $type           = I('post.html_type');
            $name           = I('post.html_name');
            $answer         = I('post.html_answer');
            $right_answer   = I('post.html_right_answer');

            if(empty($id))
                $this->error('题目编号不能为空');
            if(empty($type))
                $this->error('题目类型不能为空');
            if(empty($name))
                $this->error('题目名不能为空');
            if(empty($answer))
                $this->error('选择答案不能为空');
            if(empty($right_answer))
                $this->error('正确答案不能为空');

            $arr = [
                'type'          => $type,
                'name'          => $name,
                'choose_answer' => json_encode($answer, JSON_UNESCAPED_UNICODE),
                'right_answer'  => $right_answer,
                'admin_id'      => $this->adminId,
                'updated_at'    => time(),
            ];

            if(Question::getInstance()->updateQuestion($id, $arr))
            {
                $this->success(L('UPDATE_SUCCESS'), U("Question/quesManager"));
            }else{
                $this->error(L('UPDATE_FAILED'));
            }
        }

        $id         = intval(I('get.id'));
        $ret_arr    = Question::getInstance()->getQuestionInfo($id);
        $info       = $ret_arr ? $ret_arr : [];

        $this->assign("info", $info);
        $this->display('Activity/ques_edit');
    }

    /**
     * 删除题目
     * @return mixed
     */
    public function quesDel()
    {
         $id = intval(I('get.id'));
        if(empty($id))
            $this->error('题目编号不能为空');

        if(Question::getInstance()->delQuestion($id))
        {
            $this->success(L('DEL_SUCCESS'), U("Question/quesManager")); //跳转到题库管理,地址待定
        }else{
            $this->error(L('DEL_FAILED'));
        }
    }

    /**
     * 题单管理Ajax
     */
    public function quesListManagerAjax()
    {
        $page    = intval(I('post.page'));
        $ret_arr = Question::getInstance()->quesListManagerAjax($page);
        $lists   = $ret_arr['lists'] ? $ret_arr : [];

        $this->ajaxReturn($lists);
    }




    /**
     * 题单管理
     */
    public function quesListManager()
    {

        $ret_arr = Question::getInstance()->quesListManager();
        $lists   = $ret_arr['lists'] ? $ret_arr['lists'] : [];

        $this->assign("lists", $lists);
        $this->assign("page", $ret_arr['page']);
        $this->display('Activity/ques_list_manager');
    }

    /**
     * 添加已存在题单
     */
     public function addExistQuesListAjax()
     {
        $ques_list_arr  = I('post.question_list_id');
        $ques_arr       = I('post.question_id');

        if(empty($ques_list_arr))
        {
            $this->error("请选择题单");
        }
         if(empty($ques_arr))
         {
             $this->error("请选择题目");
         }

        $ques_list_arr = array_filter($ques_list_arr);
        $ques_str      = implode(',', array_filter($ques_arr));

        if(Question::getInstance()->addExistQuesListAjax($ques_list_arr, $ques_str))
        {
            $this->success('添加成功!', U("Question/quesWareroomManager"));
        }else{
            $this->error("添加失败", U("Question/quesWareroomManager"));
        }
     }

    /**
     * 添加题单
     */
    public function quesListAdd()
    {
        if(IS_POST) {

            $quesion_id_arr = I('post.question_id');
            $question_ids = $quesion_id_arr ? implode(',', array_filter($quesion_id_arr)) : '';

            $name =  I('post.html_name');
            if(empty($name))
                $this->error(L('题单名不能为空'));

            $arr = [
                'name'          => $name,
                'admin_id'      => $this->adminId,
                'question_ids'  => $question_ids,
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            if(Question::getInstance()->addQuesList($arr))
            {
                $this->success(L('ADD_SUCCESS'), U("Question/quesListManager"));
            }else{
                $this->error(L('ADD_FAILED'));
            }
        }

        $this->display('Activity/ques_list_add');
    }

    /**
     *  编辑修改题单
     */
    public function getQuesListInfo()
    {

        if(IS_POST)
        {
            $id             = intval(I('post.html_id'));
            $quesion_id_arr = I('post.html_question_id');
            $question_ids   = $quesion_id_arr ? implode(',', array_filter($quesion_id_arr)) : '';

            $name =  I('post.html_name');
            if(empty($name))
                $this->error(L('题单名不能为空'));

            $arr = [
                'name'          => $name,
                'admin_id'      => $this->adminId,
                'question_ids'  => $question_ids,
                'updated_at'    => time(),
            ];

            if(Question::getInstance()->updateQuesList($id, $arr))
            {
                $this->success(L('UPDATE_SUCCESS'), U("Question/quesListManager"));
            }else{
                $this->error(L('UPDATE_FAILED'));
            }
        }

        $id = intval(I('get.id'));
        $ret_arr = Question::getInstance()->getQuesListInfo($id);
        $info       = $ret_arr ? $ret_arr : [];

        $this->assign("info", $info);
        $this->display('Activity/ques_list_edit');
    }

    /**
     * 删除题单
     */
    public function quesListDel()
    {
        $id = intval(I('get.id'));
        if(empty($id))
            $this->error('题目单编号不能为空');

        if(Question::getInstance()->delQuesList($id))
        {
            $this->success(L('DEL_SUCCESS'), U("Question/quesListManager")); //跳转到题库管理,地址待定
        }else{
            $this->error(L('DEL_FAILED'));
        }
    }

    /**
     * 题库管理
     */
    public function quesWareroomManager()
    {
        /*
        1、支持搜索题目类型筛选
        2、列出所有题目列表并分页

         */
        $type = I('get.type');

        $arr = [];
        if($type > 0 && $type < 3) //限制题目类型
        {
            $arr['type'] = $type;
        }

        $ret_arr = Question::getInstance()->quesGetList($arr);
        $lists   = $ret_arr['lists'] ? $ret_arr['lists'] : [];

        $this->assign("type", $type);
        $this->assign("lists", $lists);
        $this->assign("page", $ret_arr['page']);
        $this->display('Activity/ques_wareroom_manager');
    }

    /**
     * 题目批量导入
     */
    public function import()
    {
        $this->display('Activity/import_question');
    }

    /**
     * 答题Excel导入模板导入Mysql
     */
    public function excelImport()
    {
        $file = $_FILES['file'];

        $lib = Question::getInstance();
        $rs = $lib->excelImport($file);

        if(false == $rs){
            $this->error($lib->getLastErrMsg());
        }

        $this->success($rs);
    }










}