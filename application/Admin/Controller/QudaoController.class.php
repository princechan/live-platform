<?php

/**
 * 渠道
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class QudaoController extends AdminbaseController
{


    function index()
    {
        $channel = M("qudao");
        $count = $channel->count();
        $page = $this->page($count, 20);
        $lists = $channel
            ->where()
            ->order("orderno asc")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();
        $this->assign('lists', $lists);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }


    function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $result = M("qudao")->delete($id);
            if ($result) {
                //更新用户列表的渠道值
                $data['qudao_id'] = 0;
                M("users")->where("qudao_id={$id}")->save($data);
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }


    //排序
    public function listorders()
    {

        $ids = $_POST['listorders'];
        foreach ($ids as $key => $r) {
            $data['orderno'] = $r;
            M("qudao")->where(['id' => $key])->save($data);
        }

        $status = true;
        if ($status) {
            $this->success("排序更新成功！");
        } else {
            $this->error("排序更新失败！");
        }
    }


    function add()
    {
        $this->display();
    }


    function add_post()
    {
        if (IS_POST) {
            $channel = M("qudao");
            $title = $_POST['title'];
            $orderno = $_POST['orderno'];
            $isexist = $channel->where("title='{$title}'")->find();
            if ($isexist) {
                $this->error('该渠道已存在');
            }
            $channel->create();
            $channel->addtime = time();
            $result = $channel->add();
            if ($result) {
                $this->success('添加成功');
                $this->display("index");
            } else {
                $this->error('添加失败');
            }
        }

    }


    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $channel = M("qudao")->find($id);
            $this->assign('channel', $channel);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }


    function edit_post()
    {
        if (IS_POST) {
            $channel = M("qudao");
            $id = $_POST['id'];
            $orderno = $_POST['orderno'];
            $title = $_POST['title'];
            $isexist = $channel->where("title='{$title}' and id!={$id}")->find();
            if ($isexist) {
                $this->error('该渠道已存在');
            }
            $channel->create();
            $channel->updatetime = time();
            $result = $channel->save();
            if ($result !== false) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }


}
