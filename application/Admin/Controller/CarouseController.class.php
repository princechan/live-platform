<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class CarouseController extends AdminbaseController
{
    function index()
    {
        $info = M('users_carouse')->where(['id' => 1])->find();
        $this->assign('info', $info);
        $this->display();
    }

    function edit()
    {
        $info = M('users_carouse')->where(['id' => 1])->find();
        $this->assign('info', $info);
        $this->display();
    }

    function edit_post()
    {
        if (IS_POST) {
            $user = M('users_carouse');
            $user->create();
            $user->addtime = time();
            if (empty($_POST['id'])) {
                $result = $user->add();
            } else {
                $result = $user->save();
            }
            if ($result) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }

}
