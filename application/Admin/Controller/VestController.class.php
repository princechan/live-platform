<?php

/**
 * 马甲权限
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Live\Live;

class VestController extends AdminbaseController
{
    function index()
    {
        $vests = M("users_vest")->order("id")->select();
        $this->assign('vests', $vests);
        $this->display();
    }

    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $vests = M("users_vest")->select();
            $vest = M("users_vest")->find($id);
            $this->assign('visitor', -1);
            $this->assign('vest', $vest);
            $this->assign('vests', $vests);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    function edit_post()
    {
        if (IS_POST) {
            $data = I('post.');
            if (empty($data['private_chat_permit'])) {
                $data['private_chat_permit'] = '';
            } else {
                $data['private_chat_permit'] = implode(',', $data['private_chat_permit']);
            }

            $where['id'] = $data['id'];
            unset($data['id']);
            $result = M("users_vest")->where($where)->save($data);
            Live::getInstance()->getUsersVest($data['id'], true);
            if ($result) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }
}
