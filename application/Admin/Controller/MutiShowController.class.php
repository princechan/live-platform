<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\Admin;
use Common\Lib\Auth\User;
use Common\Lib\Live\MultiLive;

class MutiShowController extends AdminbaseController
{
    public function index()
    {
        $this->display();
    }

    public function single_edit()
    {

    }

    public function single_edit_post()
    {

    }

    public function index_multiple()
    {
        $user = M('users');
        $muti_show = M('muti_show');
        $muti_showmanager = M('muti_showmanager');
        $map = [];

        if ($_REQUEST['live_name'] != '') {
            $map['live_name'] = $_REQUEST['live_name'];
            $_GET['live_name'] = $_REQUEST['live_name'];
        }

        if ($map) {
            $count = $muti_show->where($map)->count();
            $page = $this->page($count, 20);
            $lists = $muti_show
                ->where($map)
                ->limit($page->firstRow, $page->listRows)
                ->select();
        } else {
            $count = $muti_show->count();
            $page = $this->page($count, 20);
            $lists = $muti_show
                ->limit($page->firstRow, $page->listRows)
                ->select();
        }

        $muti_id = array_column($lists, 'id');
        $muti_showmanager_temp = $muti_showmanager->where([
            'muti_id' => ['in', implode(',', $muti_id)],
        ])->select();

        $muti_showmanager_list = [];
        foreach ($muti_showmanager_temp as $tmp) {
            $muti_showmanager_list[$tmp['muti_id']][$tmp['vest_type']][$tmp['uid']] = $tmp;
        }

        $user_id = array_column($muti_showmanager_temp, 'uid');
        $last_edit_uid = array_column($lists, 'last_edit_uid');
        $user_id = array_merge(array_unique($user_id), array_unique($last_edit_uid));
        $user_temp = $user->where(['id' => ['in', implode(',', $user_id)]])->select();

        $user = [];
        foreach ($user_temp as $tmp) {
            $user[$tmp['id']] = $tmp;
        }

        $status = [0 => '关闭', 1 => '开启', 2 => '休息'];
        foreach ($lists as $key => $list) {
            $list['user'] = $user[$list['last_edit_uid']];
            $list['status'] = $status[$list['status']];
            $list['anchor'] = isset($muti_showmanager_list[$list['id']][User::VEST_TYPE_ANCHOR]) ? $muti_showmanager_list[$list['id']][User::VEST_TYPE_ANCHOR] : [];
            $list['dj'] = isset($muti_showmanager_list[$list['id']][User::VEST_TYPE_DJ]) ? $muti_showmanager_list[$list['id']][User::VEST_TYPE_DJ] : [];

            foreach ($list['anchor'] as $uid => $anchor) {
                $anchor['user'] = $user[$uid];
                $list['anchor'][$uid] = $anchor;
            }

            foreach ($list['dj'] as $uid => $dj) {
                $dj['user'] = $user[$uid];
                $list['dj'][$uid] = $dj;
            }

            $lists[$key] = $list;
        }

        $this->assign('lists', $lists);
        $this->display();
    }

    public function multiple_edit()
    {
        if ($id = $_REQUEST['id']) {
            $data = MultiLive::getInstance()->getMultiShowInfo($id);
            $muti = M('muti_showmanager')->where(['muti_id' => $data['id']])->order(['sort_no' => 'DESC'])->select();
            $user_id = array_column($muti, 'uid');
            $user_temp = M('users')->where(['id' => ['in', implode(',', $user_id)]])->select();
            $users = [];
            foreach ($user_temp as $ut) {
                $users[$ut['id']] = $ut;
            }

            foreach ($muti as $m) {
                $m['user'] = $users[$m['uid']];
                if ($m['vest_type'] == User::VEST_TYPE_ANCHOR) {
                    $data['anchor'][] = $m;
                } else if ($m['vest_type'] == User::VEST_TYPE_DJ) {
                    $data['dj'][] = $m;
                }
            }
        } else {
            $data = [];
        }

        $this->assign('data', $data);
        $this->display();
    }

    public function multiple_edit_post()
    {
        $admin_id = Admin::getInstance()->getId();
        $muti_show = M('muti_show');
        $muti_showmanager = M('muti_showmanager');
        $data = I('post.');

        $anchor = !empty($data['anchor']) ? $data['anchor'] : [];
        $dj = !empty($data['dj']) ? $data['dj'] : [];
        unset($data['anchor']);
        unset($data['dj']);
        $data['last_edit_uid'] = $admin_id;

        if (!empty($data)) {
            $id = $data['id'];
            $data['avatar_thumb'] = $data['avatar'];
            $data['focus_thumb'] = $data['focus'];

            $roomType = (int)$data['room_type'];
            $roomPassword = trim($data['room_password']);
            $roomLiveCoin = trim($data['room_live_coin']);
            unset($data['id']);
            unset($data['room_password']);
            unset($data['room_live_coin']);

            if ($roomType == MultiLive::ROOM_TYPE_LOCK) {
                if (!MultiLive::getInstance()->validateRoomPassword($roomPassword)) {
                    $this->error(MultiLive::getInstance()->getLastErrMsg());
                    return;
                }
            }

            if ($roomType > 0 && (empty($roomPassword) && empty($roomLiveCoin))) {
                $this->error('请填写直播间类型对应的数据');
            }

            if ($data['speak_num'] <= 0 || $data['speak_num'] > 100) {
                $data['speak_num'] = 30;
            }
            if ($data['speak_frequency'] <= 0 || $data['speak_frequency'] > 60) {
                $data['speak_frequency'] = 0;
            }

            $data['updated_at'] = time();
            if ($id) {
                $muti_show->where(['id' => $id])->save($data);
            } else {
                $data['created_at'] = time();
                $id = $muti_show->add($data);
            }

            if ($id) {
                if ($anchor && count($anchor) > MultiLive::ANCHOR_LIMIT) {
                    $this->error('最多只能添加50位主播');
                    return;
                }

                $muti_showmanager->where(['muti_id' => $id])->delete();
                $time = time();
                if ($anchor) {
                    foreach ($anchor as $ak => $a) {
                        if ($a) {
                            $muti_showmanager->add([
                                'vest_type' => User::VEST_TYPE_ANCHOR,
                                'uid' => $a,
                                'muti_id' => $id,
                                'sort_no' => $time + $ak,
                                'status' => $data['status'],
                            ]);
                        }
                    }
                }
                if ($dj) {
                    foreach ($dj as $djk => $d) {
                        if ($d) {
                            $muti_showmanager->add([
                                'vest_type' => User::VEST_TYPE_DJ,
                                'uid' => $d,
                                'muti_id' => $id,
                                'sort_no' => $time + $djk,
                                'status' => $data['status'],
                            ]);
                        }
                    }
                }

                //设置属性
                $rs = MultiLive::getInstance()->setMultiShowExtData($id, [
                    'room_type' => $roomType,
                    'room_password' => $roomPassword,
                    'room_live_coin' => $roomLiveCoin,
                ]);
                if (!$rs) {
                    $this->error(MultiLive::getInstance()->getLastErrMsg());
                    return;
                }
            }
            $result = true;
        } else {
            $result = false;
        }

        if ($result) {
            $this->success('修改成功');
        } else {
            $this->error('修改失败');
        }
    }

    /**
     * 查询主播、麦手是否合法
     */
    public function get_anchor_and_dj()
    {
        $type = I('type');
        $username = I('username');

        $username = $username ? explode(',', $username) : [];
        if (!$username) {
            return $this->renderJson([], 400, '用户名为空');
        }

        $newUsername = [];
        foreach ($username as $u) {
            $newUsername[] = User::getInstance()->encryptName($u);
        }
        $username = $newUsername;

        $found = [];
        $result = [];
        if (in_array($type, ['anchor', 'dj'])) {
            if ($type == 'anchor') {
                $vest_id = User::VEST_TYPE_ANCHOR;
            } else {
                $vest_id = User::VEST_TYPE_DJ;
            }

            $user = M('users')->where([
                'vest_id' => $vest_id,
                'user_login' => ['in', implode(',', $username)]
            ])->select() ?: [];

            foreach ($user as $u) {
                $result[] = [
                    'id' => $u['id'],
                    'user_login' => $u['user_login'],
                    'user_nicename' => $u['user_nicename']
                ];

                $found[] = $u['user_login'];
            }
        }

        $notfound = array_diff($username, $found);
        if ($notfound) {
            $notfound = implode(',', $notfound);
            $this->renderJson([], 400, '不存在的账号: ' . $notfound);
            return;
        }

        $this->renderJson([
            'type' => $type,
            'result' => $result,
        ]);
    }

}
