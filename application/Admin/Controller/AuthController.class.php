<?php

/**
 * 提现
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\User;

class CashController extends AdminbaseController
{
    function index()
    {

        if ($_REQUEST['status'] != '') {
            $map['status'] = $_REQUEST['status'];
            $_GET['status'] = $_REQUEST['status'];
        }
        if ($_REQUEST['start_time'] != '') {
            $map['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }

        if ($_REQUEST['end_time'] != '') {

            $map['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {

            $map['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        if ($_REQUEST['keyword'] != '') {
            $map['uid|orderno|trade_no'] = ["like", "%" . $_REQUEST['keyword'] . "%"];
            $_GET['keyword'] = $_REQUEST['keyword'];
        }

        $cashrecord = M("users_cashrecord");
        $count = $cashrecord->where($$map)->count();
        $page = $this->page($count, 20);
        $lists = $cashrecord
            ->where($map)
            ->order("addtime DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select() ?: [];

        foreach ($lists as $k => $v) {
            $user = User::getInstance()->getUserInfo($v['uid']);
            $lists[$k]['userinfo'] = [
                'user_nicename' => $user ? $user['user_nicename'] : '',
            ];
        }

        $this->assign('lists', $lists);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $result = M("users_cashrecord")->delete($id);
            if ($result) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }


    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $cash = M("users_cashrecord")->find($id);
            $user = $cash ? User::getInstance()->getUserInfo($cash['uid']) : [];
            $cash['userinfo'] = [
                'user_nicename' => $user ? $user['user_nicename'] : '',
            ];
            $this->assign('cash', $cash);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    function edit_post()
    {
        if (IS_POST) {
            if ($_POST['status'] == '0') {
                $this->error('未修改订单状态');
            }

            $uid = $_POST['uid'];
            $cash = M("users_cashrecord");
            $cash->create();
            $cash->uptime = time();
            $result = $cash->save();
            if ($result) {
                if ($_POST['status'] == '2') {
                    M("users")->where(['id' => $uid])->setInc("votes", $_POST['votes']);
                    User::getInstance()->getUserInfo($uid, true);
                }
                $this->success('修改成功', U('Cash/index'));
            } else {
                $this->error('修改失败');
            }
        }
    }

}
