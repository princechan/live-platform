<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Ads;
use Common\Lib\Auth\Admin;
use Common\Lib\Live\Live;

class HomeSetController extends AdminbaseController
{
    function index()
    {
        $liveList = [];
        $multiList = M('muti_show')->where(['status' => 1])->order(['updated_at' => 'DESC'])->select() ?: [];
        foreach ($multiList as &$li) {
            $li['n_id'] = '1-' . $li['id'];
            $li['user_nicename'] = "(多人){$li['live_name']}";
            $liveList[] = $li;
        }

        $userLive = M('users_live')->field(['uid'])->where(['islive' => 1])->select() ?: [];
        $singleList = $userLive ? M('users')->where(['id' => ['in', array_column($userLive, 'uid')]])->select() : [];
        foreach ($singleList as &$li) {
            $li['n_id'] = '0-' . $li['id'];
            $liveList[] = $li;
        }

        $data = M('home_recommend')->where(['id' => ['lt', 5]])->select() ?: [];
        foreach ($data as &$d) {
            $d['n_id'] = $d['show_type'] . '-' . $d['show_id'];
        }
        $data = Func::index($data);

        $this->assign('liveList', $liveList);
        $this->assign('data', $data);

        $this->display();
    }

    function home_live_post()
    {
        $home_recommend = M("home_recommend");

        for ($i = 1; $i < 5; $i++) {
            $flag = (string)$i;
            $where = [];
            $data = [];
            $where['id'] = $_REQUEST['id_' . $flag];
            $data['type'] = $_REQUEST['type_' . $flag];
            $showId = explode('-', $_REQUEST['show_id_' . $flag]);
            $data['show_type'] = (int)$showId[0];
            $data['show_id'] = (int)$showId[1];
            $data['show_cover'] = $_REQUEST['show_cover_' . $flag];
            $data['default_cover'] = $_REQUEST['default_cover_' . $flag];

            if ($data['type'] == 0 && $data['show_id'] && empty($data['show_cover'])) {
                $this->error('直播封面图为空');
            }
            if ($data['type'] == 1 && $data['show_id'] && empty($data['default_cover'])) {
                $this->error('默认封面图为空');
            }

            if ($home_recommend->where($where)->count() == 0) {
                $data['id'] = $where['id'];
                $home_recommend->add($data);
            } else {
                $home_recommend->where($where)->save($data);
            }
        }

        Live::getInstance()->delSlideLiveList();

        $this->success("编辑成功！");
    }

    function announcement()
    {
        $user = D("Common/Users");

        $map = [
            'id' => ['gt', 0]
        ];
        $announcement = M("announcement");
        $count = $announcement->where($map)->count();

        $page = $this->page($count, 20);

        $lists = $announcement
            ->where($map)
            ->order(['sort' => 'asc'])
            ->limit($page->firstRow, $page->listRows)
            ->select();

        $last_edit_uid = array_column($lists, 'last_edit_uid');
        $admin_user_temp = $user->where(
            [
                'id' => ['in', implode(',', $last_edit_uid)],
                'user_type' => 1
            ]
        )->select();

        $admin_user = [];
        foreach ($admin_user_temp as $tmp) {
            $admin_user[$tmp['id']] = $tmp;
        }
        $state = [0 => '生效', 1 => '下线'];
        foreach ($lists as $key => $list) {
            $list['user'] = $admin_user[$list['last_edit_uid']];
            $list['state'] = $state[$list['state']];

            $lists[$key] = $list;
        }

        $this->assign('lists', $lists);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));
        $this->display();
    }

    function announcement_edit()
    {
        $data = [];
        if ($id = $_REQUEST['id']) {
            $data = M("announcement")->find($id);
        }

        $this->assign('data', $data);
        $this->assign('group', Ads::getInstance()->getLiveAnnouncementGroupMap());

        $this->display();
    }

    function announcement_edit_post()
    {
        $announcement = M("announcement");
        $admin_id = Admin::getInstance()->getId();
        $map = [];
        if ($_REQUEST['title'] != '') {
            $map['title'] = $_REQUEST['title'];
        }
        if ($_REQUEST['link'] != '') {
            $map['link'] = $_REQUEST['link'];
        }
        if ($_REQUEST['state'] != '') {
            $map['state'] = $_REQUEST['state'];
        }
        $map['content'] = $_REQUEST['content'];

        $map['last_edit_uid'] = $admin_id;
        $map['category'] = trim($_REQUEST['category']);

        if (!empty($map)) {
            if ($_REQUEST['id'] != '') {
                $id = $_REQUEST['id'];
                $result = $announcement->where(['id' => $id])->save($map);
            } else {
                $map['sort'] = $announcement->count();
                $result = $announcement->add($map);
            }
        } else {
            $result = false;
        }

        if ($result) {
            $this->success('添加成功', U('HomeSet/announcement'));
        } else {
            $this->success('添加失败', U('HomeSet/announcement'));
        }

    }

    function announcement_orders()
    {
        $announcement = M("announcement");
        $admin_id = Admin::getInstance()->getId();

        if (!empty($_REQUEST['listorders'])) {
            $map = $_REQUEST['listorders'];
        }

        foreach ($map as $id => $sort) {
            $data = [];
            $data['sort'] = $sort;
            $data['last_edit_uid'] = $admin_id;
            $announcement->where(['id' => $id])->save($data);
        }

        $this->success("排序更新成功！");
    }

    /**
     * 上传图片
     */
    public function uploadPic()
    {
        $data = [];

        if (IS_POST) {
            if (isset($_FILES['picture']) && empty($_FILES['picture']['size'])) {
                $this->error('请选择图片');
            }

            $savepath = date('Ymd') . '/';
            //上传处理类
            $config = [
                'rootPath' => './' . C("UPLOADPATH"),
                'savePath' => $savepath,
                'maxSize' => 11048576,
                'saveName' => ['uniqid', ''],
                'exts' => ['jpg', 'gif', 'png', 'jpeg', "txt", 'zip'],
                'autoSub' => false,
            ];

            $upload = new \Think\Upload($config);
            $info = $upload->upload();
            //开始上传
            if (isset($info['picture']) && is_array($info['picture'])) {
                $data = $info['picture'];
                if (!empty($data['url'])) {
                    $url = $data['url'];
                } else {
                    $url = C("TMPL_PARSE_STRING.__UPLOAD__") . $savepath . $data['savename'];
                }
                $data['url'] = $url;
            } else {
                $this->error('上传失败');
            }
        }

        $this->assign('data', $data);

        $this->display('uploadPic');
    }

}
