<?php

/**
 * 红包
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;

class RedPacketController extends AdminbaseController
{
    function index()
    {

        $packets = M("users_send_redpackets");
        $count = $packets->count();
        $page = $this->page($count, 20);
        $lists = $packets
            ->where()
            ->order("addtime DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();

        foreach ($lists as $k => $v) {
            $lists[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
            $lists[$k]['roominfo'] = User::getInstance()->getUserInfo($v['roomid']);
        }
        $this->assign('lists', $lists);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }


    /*抢红包记录列表*/

    function robLists()
    {
        $id = I("id");
        $sendid = I("sendid");
        $type = I("type");
        $roomid = I("roomid");
        $sendtime = I("sendtime");
        $list = M("users_rob_redpackets")->where("senduid={$sendid} and type={$type} and liveuid={$roomid} and sendtime={$sendtime}")->order("livecoin desc,addtime asc")->select();
        $rs = [];
        if ($list) {
            foreach ($list as $k => $v) {
                $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
                $list[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);

                //获取红包退还记录
                $backMsg = M("redpcakets_back")->where("packet_id={$id}")->find();

                if ($backMsg) {
                    $backMsg['addtime'] = date('Y-m-d H:i:s', $backMsg['addtime']);
                    $rs['backMsg'] = $backMsg;
                } else {
                    $rs['backMsg'] = "";
                }

            }
            $rs['code'] = 0;
            $rs['info'] = $list;
            $rs['msg'] = '';
        } else {
            $rs['code'] = 1;
            $rs['msg'] = "没有找到相关记录";
        }

        echo json_encode($rs);

    }


    function add()
    {
        $config = M("config_private")->where("id=1")->find();
        $this->assign('config', $config);
        $this->display();
    }


    function add_post()
    {

        $type = I("type");
        $title = I("title");
        $liveid = I("liveid");
        $num = I("num");
        $totalMoney = I("totalMoney");


        $rs = [];

        $info = M("users")->where("id={$liveid}")->find();

        if (!$info) {
            $rs['code'] = 1001;
            $rs['msg'] = "";

            echo json_encode($rs);
            exit;
        }

        $config = M("config")->where("id=1")->find();


        $datared = ['uid' => 1, 'roomid' => $liveid, 'num' => $num, 'total_money' => $totalMoney, 'title' => $title, 'type' => $type, 'addtime' => time()];
        M('users_send_redpackets')->add($datared);


        $total = $totalMoney;//红包总金额
        $min = 0.01;//每个人最少能收到0.01元 
        for ($i = 1; $i < $num; $i++) {
            $safe_total = ($total - ($num - $i) * $min) / ($num - $i);//随机安全上限 
            $money = mt_rand($min * 100, $safe_total * 100) / 100;
            $total = $total - $money;
            //echo '第'.$i.'个红包：'.$money.' 元，余额：'.$total.' 元 '; 
            $rs[] = $money;
        }
        //echo '第'.$num.'个红包：'.$total.' 元，余额：0 元';
        $rs[] = $total;


        $redis = CRedis::getInstance();
        $redis->hSet('redpackets_' . $liveid, '1_' . time(), json_encode($rs));

        //将红包口令存入redis中
        if ($type == 1) {
            $redis->hSet('redpacketstitle_' . $liveid, '1_' . time(), $title);
        }

        $avatar = $config['site'] . '/default.jpg';

        $rs['code'] = 0;
        $rs['msg'] = "红包发送成功";
        $rs['info'] = ['sendtime' => time(), 'avatar' => $avatar];


        echo json_encode($rs);
        exit;
    }


}
