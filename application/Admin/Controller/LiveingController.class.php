<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Live\Live;

class LiveingController extends AdminbaseController
{
    public function index()
    {
        $config = M("config")->where("id='1'")->find();
        $map = [];
        $map['islive'] = 1;
        if ($_REQUEST['start_time'] != '') {
            $map['starttime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }

        if ($_REQUEST['end_time'] != '') {

            $map['starttime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {

            $map['starttime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        if ($_REQUEST['keyword'] != '') {
            $map['uid'] = $_REQUEST['keyword'];
            $_GET['keyword'] = $_REQUEST['keyword'];
        }

        $count = M('users_live')->where($map)->count();
        $page = $this->page($count, 20);
        $lists = M("users_live")->where($map)
            ->order("starttime DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select() ?: [];

        foreach ($lists as &$v) {
            $v['pull'] = Live::getInstance()->getStream('rtmp', $v['stream'], 0);
            $userinfo = M("users")->field("user_nicename")->where("id='{$v['uid']}'")->find();
            $v['userinfo'] = $userinfo;
        }

        $this->assign([
            'config' => $config,
            'lists' => $lists,
            'formget' => $_GET,
            'page' => $page->show('Admin'),
        ]);

        $this->display();
    }

}
