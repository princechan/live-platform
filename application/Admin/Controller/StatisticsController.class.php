<?php

/**
 * 礼物
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class StatisticsController extends AdminbaseController
{
    function indexcnzz()
    {
        $cnzz_id = M("config_private")->where("id=1")->getField("cnzz_id");
        if ($cnzz_id) {
            $this->assign("cnzz_id", $cnzz_id);
        } else {
            $this->error("请先填写cnzz的ID");
        }

        $this->display();
    }


    function index()
    {


        //获取所有的渠道列表
        $channels = M("qudao")->order("orderno")->select();


        $User = M("users");

        $coinRecord = M("users_coinrecord");

        //今日开始时间
        $todayBeginDate = date('Y-m-d 00:00:00');//今天，格式成时间戳
        $todayBeginDateInt = strtotime($todayBeginDate);

        $todayLoginCount = $User->where("last_login_time >'{$todayBeginDate}'")->count();
        $this->assign("todayLoginCount", $todayLoginCount);

        $todayRegCount = $User->where("create_time >'{$todayBeginDate}'")->count();
        $this->assign("todayRegCount", $todayRegCount);

        $todayWxRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='wx'")->count();
        $this->assign("todayWxRegCount", $todayWxRegCount);

        $todayQQRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='qq'")->count();
        $this->assign("todayQQRegCount", $todayQQRegCount);

        $todayPhoneRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='phone'")->count();
        $this->assign("todayPhoneRegCount", $todayPhoneRegCount);


        //获取今日渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['todayChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$todayBeginDate}'")->count();

            //今日渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$todayBeginDateInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['todayChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['todayChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['todayChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['todayChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['todayChannelConsumption'][0]['total_livecoin'] = 0;
            }


        }


        //获取当前日期
        $now = time();
        $todayLingDate = strtotime(date('Y-m-d 00:00:00'));//今天零点，格式成时间戳
        $todayEightDate = strtotime(date('Y-m-d 08:00:00'));//今天八点，格式成时间戳
        $todaySixteenDate = strtotime(date('Y-m-d 16:00:00'));//今天十六点，格式成时间戳
        $todayTwentyfourDate = strtotime(date('Y-m-d 23:59:59'));//今天二十四点，格式成时间戳

        if ($now <= $todayEightDate) {
            $lists1 = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->select();

            $lists1Max = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->max("num");

            $this->assign("lists1", $lists1);
            $this->assign("lists1Max", $lists1Max);
        }

        if ($now <= $todaySixteenDate) {

            $lists1 = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->select();
            $lists2 = M("online_nums")->where(" addtime>={$todayEightDate} and addtime<={$todaySixteenDate}")->select();

            $lists1Max = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->max("num");
            $lists2Max = M("online_nums")->where("addtime>={$todayEightDate} and addtime<={$todaySixteenDate}")->max("num");

            $this->assign("lists1", $lists1);
            $this->assign("lists2", $lists2);


            $this->assign("lists1Max", $lists1Max + 30);
            $this->assign("lists2Max", $lists2Max + 30);
        }

        if ($now <= $todayTwentyfourDate) {
            $lists1 = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->select();
            $lists2 = M("online_nums")->where("addtime>={$todayEightDate} and addtime<={$todaySixteenDate}")->select();
            $lists3 = M("online_nums")->where("addtime>={$todaySixteenDate} and addtime<={$todayTwentyfourDate}")->select();

            $lists1Max = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayEightDate}")->max("num");
            $lists2Max = M("online_nums")->where("addtime>={$todayEightDate} and addtime<={$todaySixteenDate}")->max("num");
            $lists3Max = M("online_nums")->where("addtime>={$todaySixteenDate} and addtime<={$todayTwentyfourDate}")->max("num");

            $this->assign("lists1", $lists1);
            $this->assign("lists2", $lists2);
            $this->assign("lists3", $lists3);

            $this->assign("lists1Max", $lists1Max + 30);
            $this->assign("lists2Max", $lists2Max + 30);
            $this->assign("lists3Max", $lists3Max + 30);


        }


        $todayListsMax = M("online_nums")->where("addtime>={$todayLingDate} and addtime<={$todayTwentyfourDate}")->max("num");
        if ($todayListsMax) {
            $todayListsMaxDate = M("online_nums")->where("num={$todayListsMax}")->getField("addtime");
        } else {
            $todayListsMax = 0;
            $todayListsMaxDate = "";
        }

        $this->assign("todayListsMax", $todayListsMax);
        $this->assign("todayListsMaxDate", $todayListsMaxDate);


        $yestodayBeginDate = date('Y-m-d 00:00:00', strtotime("-1 day"));//昨天，格式成时间戳
        $yestodayEndDate = date('Y-m-d 23:59:59', strtotime("-1 day"));//昨天，格式成时间戳

        $yestodayBeginDateInt = strtotime($yestodayBeginDate);
        $yestodayEndDateInt = strtotime($yestodayEndDate);


        $yestodayLoginCount = $User->where("last_login_time >'{$yestodayBeginDate}' and last_login_time< '{$yestodayEndDate}'")->count();
        $this->assign("yestodayLoginCount", $yestodayLoginCount);

        $yestodayRegCount = $User->where("create_time >'{$yestodayBeginDate}' and create_time< '{$yestodayEndDate}'")->count();
        $this->assign("yestodayRegCount", $yestodayRegCount);

        $yestodayWxRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='wx' and create_time< '{$yestodayEndDate}' ")->count();
        $this->assign("yestodayWxRegCount", $yestodayWxRegCount);

        $yestodayQQRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='qq' and create_time< '{$yestodayEndDate}' ")->count();
        $this->assign("yestodayQQRegCount", $yestodayQQRegCount);

        $yestodayPhoneRegCount = $User->where("create_time >'{$todayBeginDate}' and login_type='phone' and create_time< '{$yestodayEndDate}'")->count();

        $this->assign("yestodayPhoneRegCount", $yestodayPhoneRegCount);

        //获取昨日渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['yestodayChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$yestodayBeginDate}' and create_time< '{$yestodayEndDate}'")->count();

            //昨日渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$yestodayBeginDateInt} and r.addtime<{$yestodayEndDateInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['yestodayChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['yestodayChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['yestodayChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['yestodayChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['yestodayChannelConsumption'][0]['total_livecoin'] = 0;
            }
        }


        $thisWeekStartDate = date('Y-m-d 00:00:00', (strtotime('last day this week') + 24 * 60 * 60));//本周第一天
        $thisWeekEndDate = date('Y-m-d 23:59:59', strtotime('last day next week'));//本周最后一天

        $thisWeekStartDateInt = strtotime($thisWeekStartDate);
        $thisWeekEndDateInt = strtotime($thisWeekEndDate);

        $thisWeekLoginCount = $User->where("last_login_time >'{$thisWeekStartDate}' and last_login_time< '{$thisWeekEndDate}'")->count();
        $this->assign("thisWeekLoginCount", $thisWeekLoginCount);

        $thisWeekRegCount = $User->where("create_time >'{$thisWeekStartDate}' and create_time< '{$thisWeekEndDate}'")->count();
        $this->assign("thisWeekRegCount", $thisWeekRegCount);

        $thisWeekWxRegCount = $User->where("create_time >'{$thisWeekStartDate}' and login_type='wx' and create_time< '{$thisWeekEndDate}' ")->count();
        $this->assign("thisWeekWxRegCount", $thisWeekWxRegCount);

        $thisWeekQQRegCount = $User->where("create_time >'{$thisWeekStartDate}' and login_type='qq' and create_time< '{$thisWeekEndDate}' ")->count();
        $this->assign("thisWeekQQRegCount", $thisWeekQQRegCount);

        $thisWeekPhoneRegCount = $User->where("create_time >'{$thisWeekStartDate}' and login_type='phone' and create_time< '{$thisWeekEndDate}'")->count();
        $this->assign("thisWeekPhoneRegCount", $thisWeekPhoneRegCount);

        //获取本周渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['thisWeekChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$thisWeekStartDate}' and create_time< '{$thisWeekEndDate}'")->count();

            //本周渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$thisWeekStartDateInt} and r.addtime<{$thisWeekEndDateInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['thisWeekChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['thisWeekChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['thisWeekChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['thisWeekChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['thisWeekChannelConsumption'][0]['total_livecoin'] = 0;
            }


        }


        $beginLastweek = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
        $beginLastweek = date('Y-m-d H:i:s', $beginLastweek);
        $beginLastweekInt = strtotime($beginLastweek);

        $endLastweek = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
        $endLastweek = date('Y-m-d H:i:s', $endLastweek);
        $endLastweekInt = strtotime($endLastweek);


        $lastWeekLoginCount = $User->where("last_login_time >'{$beginLastweek}' and last_login_time< '{$endLastweek}'")->count();
        $this->assign("lastWeekLoginCount", $lastWeekLoginCount);

        //var_dump($User->getLastSql());

        $lastWeekRegCount = $User->where("create_time >'{$beginLastweek}' and create_time< '{$endLastweek}'")->count();
        $this->assign("lastWeekRegCount", $lastWeekRegCount);

        $lastWeekWxRegCount = $User->where("create_time >'{$beginLastweek}' and login_type='wx' and create_time< '{$endLastweek}' ")->count();
        $this->assign("lastWeekWxRegCount", $lastWeekWxRegCount);

        $lastWeekQQRegCount = $User->where("create_time >'{$beginLastweek}' and login_type='qq' and create_time< '{$endLastweek}' ")->count();
        $this->assign("lastWeekQQRegCount", $lastWeekQQRegCount);

        $lastWeekPhoneRegCount = $User->where("create_time >'{$beginLastweek}' and login_type='phone' and create_time< '{$endLastweek}'")->count();
        $this->assign("lastWeekPhoneRegCount", $lastWeekPhoneRegCount);


        //获取上周渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['lastWeekChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$beginLastweek}' and create_time< '{$endLastweek}'")->count();

            //上周渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginLastweekInt} and r.addtime<{$endLastweekInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['lastWeekChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['lastWeekChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['lastWeekChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['lastWeekChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['lastWeekChannelConsumption'][0]['total_livecoin'] = 0;
            }


        }


        $beginThisMonth = date('Y-m-01 00:00:00');//本月第一天，格式成时间戳
        $endThisMonth = strtotime(date('Y-m-d H:i:s', mktime(23, 59, 59, date('n'), date('t'), date('Y'))));//本月最后一天，格式成时间戳
        $endThisMonth = date('Y-m-d H:i:s', $endThisMonth);

        $beginThisMonthInt = strtotime($beginThisMonth);
        $endThisMonthInt = strtotime($endThisMonth);

        $thisMonthLoginCount = $User->where("last_login_time >'{$beginThisMonth}' and last_login_time< '{$endThisMonth}'")->count();
        $this->assign("thisMonthLoginCount", $thisMonthLoginCount);

        $thisMonthRegCount = $User->where("create_time >'{$beginThisMonth}' and create_time< '{$endThisMonth}'")->count();
        $this->assign("thisMonthRegCount", $thisMonthRegCount);

        $thisMonthWxRegCount = $User->where("create_time >'{$beginThisMonth}' and login_type='wx' and create_time< '{$endThisMonth}' ")->count();
        $this->assign("thisMonthWxRegCount", $thisMonthWxRegCount);

        $thisMonthQQRegCount = $User->where("create_time >'{$beginThisMonth}' and login_type='qq' and create_time< '{$endThisMonth}' ")->count();
        $this->assign("thisMonthQQRegCount", $thisMonthQQRegCount);

        $thisMonthPhoneRegCount = $User->where("create_time >'{$beginThisMonth}' and login_type='phone' and create_time< '{$endThisMonth}' ")->count();
        $this->assign("thisMonthPhoneRegCount", $thisMonthPhoneRegCount);

        //获取本月渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['thisMonthChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$beginThisMonth}' and create_time< '{$endThisMonth}'")->count();


            //本月渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginThisMonthInt} and r.addtime<{$endThisMonthInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['thisMonthChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['thisMonthChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['thisMonthChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['thisMonthChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['thisMonthChannelConsumption'][0]['total_livecoin'] = 0;
            }


        }


        $beginLastMonth = date('Y-m-01 00:00:00', strtotime('-1 month'));
        $endLastMonth = date('Y-m-t 23:59:59', strtotime('-1 month'));

        $beginLastMonthInt = strtotime($beginLastMonth);
        $endLastMonthInt = strtotime($endLastMonth);


        $lastMonthLoginCount = $User->where("last_login_time >'{$beginLastMonth}' and last_login_time< '{$endLastMonth}'")->count();
        $this->assign("lastMonthLoginCount", $lastMonthLoginCount);

        $lastMonthRegCount = $User->where("create_time >'{$beginLastMonth}' and create_time< '{$endLastMonth}'")->count();
        $this->assign("lastMonthRegCount", $lastMonthRegCount);

        $lastMonthWxRegCount = $User->where("create_time >'{$beginLastMonth}' and login_type='wx' and create_time< '{$endLastMonth}' ")->count();
        $this->assign("lastMonthWxRegCount", $lastMonthWxRegCount);

        $lastMonthQQRegCount = $User->where("create_time >'{$beginLastMonth}' and login_type='qq' and create_time< '{$endLastMonth}' ")->count();
        $this->assign("lastMonthQQRegCount", $lastMonthQQRegCount);

        $lastMonthPhoneRegCount = $User->where("create_time >'{$beginLastMonth}' and login_type='phone' and create_time< '{$endLastMonth}' ")->count();
        $this->assign("lastMonthPhoneRegCount", $lastMonthPhoneRegCount);

        //获取本月渠道注册信息
        foreach ($channels as $k => $v) {

            $channels[$k]['lastMonthChannelRegNum'] = $User->where("qudao_id={$v['id']} and create_time >'{$beginLastMonth}' and create_time< '{$endLastMonth}'")->count();

            //上月渠道消费
            $sql = "select count(distinct r.uid) as totalcount,sum(r.totalcoin) as totalcoin,sum(r.total_livecoin) as total_livecoin from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginLastMonthInt} and r.addtime<{$endLastMonthInt} and u.qudao_id={$v['id']} order by r.uid";
            $channels[$k]['lastMonthChannelConsumption'] = $coinRecord->query($sql);

            //var_dump($coinRecord->getLastSql());

            if ($channels[$k]['lastMonthChannelConsumption'][0]['totalcoin'] == NULL) {
                $channels[$k]['lastMonthChannelConsumption'][0]['totalcoin'] = 0;
            }

            if ($channels[$k]['lastMonthChannelConsumption'][0]['total_livecoin'] == NULL) {
                $channels[$k]['lastMonthChannelConsumption'][0]['total_livecoin'] = 0;
            }

        }


        //今天的开始时间
        $todaystartDate = date('Y-m-d 00:00:00');//今天零点，格式成时间戳
        $todayendDate = date('Y-m-d 23:59:59');//今天零点，格式成时间戳


        $todaystartDateInt = strtotime(date('Y-m-d 00:00:00'));//今天零点，格式成时间戳
        $todayendDateInt = strtotime(date('Y-m-d 23:59:59'));//今天零点，格式成时间戳


        //昨天的时间
        $yestodayDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))));
        $yestodaystartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))));
        $yestodayendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))));
        $this->assign("yestodayDate", $yestodayDate);

        //前天
        $qianDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))));
        $qianstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))));
        $qianendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))));
        $this->assign("qianDate", $qianDate);

        //前四天
        $qianFourDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y"))));
        $qianFourstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y"))));
        $qianFourendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y"))));
        $this->assign("qianFourDate", $qianFourDate);

        //前五天
        $qianFiveDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y"))));
        $qianFivestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y"))));
        $qianFiveendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y"))));
        $this->assign("qianFiveDate", $qianFiveDate);

        //前六天
        $qianSixDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y"))));
        $qianSixstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y"))));
        $qianSixendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y"))));
        $this->assign("qianSixDate", $qianSixDate);

        //前七天
        $qianSevenDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 6, date("Y"))));
        $qianSevenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 6, date("Y"))));
        $qianSevenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 6, date("Y"))));
        $this->assign("qianSevenDate", $qianSevenDate);

        //前八天
        $qianEightDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"))));
        $qianEightstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"))));
        $qianEightendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"))));
        $this->assign("qianEightDate", $qianEightDate);


        //前九天
        $qianNineDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 8, date("Y"))));
        $qianNinestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 8, date("Y"))));
        $qianNineendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 8, date("Y"))));
        $this->assign("qianNineDate", $qianNineDate);

        //前十天
        $qianTenDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 9, date("Y"))));
        $qianTenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 9, date("Y"))));
        $qianTenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 9, date("Y"))));
        $this->assign("qianTenDate", $qianTenDate);


        //前十一天
        $qianElevenDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 10, date("Y"))));
        $qianElevenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 10, date("Y"))));
        $qianElevenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 10, date("Y"))));

        $this->assign("qianElevenDate", $qianElevenDate);

        //前十二天
        $qianTwelveDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 11, date("Y"))));
        $qianTwelvestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 11, date("Y"))));
        $qianTwelveendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 11, date("Y"))));

        $this->assign("qianTwelveDate", $qianTwelveDate);

        //前十三天
        $qianThirteenDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 12, date("Y"))));
        $qianThirteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 12, date("Y"))));
        $qianThirteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 12, date("Y"))));

        $this->assign("qianThirteenDate", $qianThirteenDate);

        //前十四天
        $qianFourteenDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 13, date("Y"))));
        $qianFourteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 13, date("Y"))));
        $qianFourteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 13, date("Y"))));

        $this->assign("qianFourteenDate", $qianFourteenDate);


        //前十五天
        $qianFifteenDate = strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))));
        $qianFifteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))));
        $qianFifteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))));

        $this->assign("qianFifteenDate", $qianFifteenDate);


        //前十六天
        $qianSixteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 15, date("Y"))));
        $qianSixteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 15, date("Y"))));

        //前十七天
        $qianSeventeenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 16, date("Y"))));
        $qianSeventeenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 16, date("Y"))));

        //前十八天
        $qianEighteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 17, date("Y"))));
        $qianEighteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 17, date("Y"))));

        //前十九天
        $qianNineteenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 18, date("Y"))));
        $qianNineteenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 18, date("Y"))));

        //前二十天
        $qianTwentystartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 19, date("Y"))));
        $qianTwentyendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 19, date("Y"))));

        //前二十一天
        $qianTwentyOnestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 20, date("Y"))));
        $qianTwentyOneendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 20, date("Y"))));

        //前二十二天
        $qianTwentyTwostartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 21, date("Y"))));
        $qianTwentyTwoendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 21, date("Y"))));

        //前二十三天
        $qianTwentyThreestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 22, date("Y"))));
        $qianTwentyThreeendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 22, date("Y"))));

        //前二十四天
        $qianTwentyFourstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 23, date("Y"))));
        $qianTwentyFourendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 23, date("Y"))));

        //前二十五天
        $qianTwentyFivestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 24, date("Y"))));
        $qianTwentyFiveendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 24, date("Y"))));

        //前二十六天
        $qianTwentySixstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 25, date("Y"))));
        $qianTwentySixendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 25, date("Y"))));

        //前二十七天
        $qianTwentySevenstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 26, date("Y"))));
        $qianTwentySevenendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 26, date("Y"))));

        //前二十八天
        $qianTwentyEightstartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 27, date("Y"))));
        $qianTwentyEightendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 27, date("Y"))));

        //前二十九天
        $qianTwentyNinestartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 28, date("Y"))));
        $qianTwentyNineendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 28, date("Y"))));

        //前三十天
        $qianThirtystartDate = strtotime(date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"), date("d") - 29, date("Y"))));
        $qianThirtyendDate = strtotime(date("Y-m-d 23:59:59", mktime(0, 0, 0, date("m"), date("d") - 29, date("Y"))));


        //获取十五天之前的注册人数
        $RegLists = M("users")->where("create_time>='{$qianThirteenstartDate}' and create_time<='{$qianThirteenendDate}'")->select();
        $RegNum = count($RegLists);


        //var_dump($RegLists);
        if ($RegNum == 0) {

            $LiuNum = 0;
            $DiuNum = 0;

        } else {

            $str = "";
            foreach ($RegLists as $k => $v) {
                $str = $str . $v['id'] . ",";
            }

            $str = substr($str, 0, strlen($str) - 1);//注册用户id字符串


            //前十五天留存和丢失
            $FifteenLists = M("users_login_lists")->where("logintime>={$qianFifteenstartDate} and logintime<={$qianFifteenendDate} and uid in({$str}) ")->select();
            $FifteenLiuNum = count($FifteenLists);
            $FifteenLiuPercent = round($FifteenLiuNum / $RegNum, 2) * 100;
            $FifteenDiuPercent = round(($RegNum - $FifteenLiuNum) / $RegNum, 2) * 100;

            $this->assign("FifteenLiuPercent", $FifteenLiuPercent);
            $this->assign("FifteenDiuPercent", $FifteenDiuPercent);

            //前十四天留存和丢失
            $FourteenLists = M("users_login_lists")->where("logintime>={$qianFourteenstartDate} and logintime<={$qianFourteendDate} and uid in({$str}) ")->select();
            $FourteenLiuNum = count($FourteenLists);
            $FourteenLiuPercent = round($FourteenLiuNum / $RegNum, 2) * 100;
            $FourteenDiuPercent = round(($RegNum - $FourteenLiuNum) / $RegNum, 2) * 100;

            $this->assign("FourteenLiuPercent", $FourteenLiuPercent);
            $this->assign("FourteenDiuPercent", $FourteenDiuPercent);

            //前十三天留存和丢失
            $ThirteenLists = M("users_login_lists")->where("logintime>={$qianThirteenstartDate} and logintime<={$qianThirteendDate} and uid in({$str}) ")->select();
            $ThirteenLiuNum = count($ThirteenLists);
            $ThirteenLiuPercent = round($ThirteenLiuNum / $RegNum, 2) * 100;
            $ThirteenDiuPercent = round(($RegNum - $ThirteenLiuNum) / $RegNum, 2) * 100;

            $this->assign("ThirteenLiuPercent", $ThirteenLiuPercent);
            $this->assign("ThirteenDiuPercent", $ThirteenDiuPercent);

            //前十二天留存和丢失
            $TwelveenLists = M("users_login_lists")->where("logintime>={$qianTwelvestartDate} and logintime<={$qianTwelveendDate} and uid in({$str}) ")->select();
            $TwelveenLiuNum = count($TwelveenLists);
            $TwelveenLiuPercent = round($TwelveenLiuNum / $RegNum, 2) * 100;
            $TwelveenDiuPercent = round(($RegNum - $TwelveenLiuNum) / $RegNum, 2) * 100;

            $this->assign("TwelveenLiuPercent", $TwelveenLiuPercent);
            $this->assign("TwelveenDiuPercent", $TwelveenDiuPercent);


            //前十一天留存和丢失
            $ElevenLists = M("users_login_lists")->where("logintime>={$qianElevenstartDate} and logintime<={$qianElevenendDate} and uid in({$str}) ")->select();
            $ElevenLiuNum = count($ElevenLists);
            $ElevenLiuPercent = round($ElevenLiuNum / $RegNum, 2) * 100;
            $ElevenDiuPercent = round(($RegNum - $ElevenLiuNum) / $RegNum, 2) * 100;

            $this->assign("ElevenLiuPercent", $ElevenLiuPercent);
            $this->assign("ElevenDiuPercent", $ElevenDiuPercent);

            //前十天留存和丢失
            $TenLists = M("users_login_lists")->where("logintime>={$qianTenstartDate} and logintime<={$qianTenendDate} and uid in({$str}) ")->select();
            $TenLiuNum = count($TenLists);
            $TenLiuPercent = round($TenLiuNum / $RegNum, 2) * 100;
            $TenDiuPercent = round(($RegNum - $TenLiuNum) / $RegNum, 2) * 100;

            $this->assign("TenLiuPercent", $TenLiuPercent);
            $this->assign("TenDiuPercent", $TenDiuPercent);

            //前九天留存和丢失
            $NineLists = M("users_login_lists")->where("logintime>={$qianNinestartDate} and logintime<={$qianNineendDate} and uid in({$str}) ")->select();
            $NineLiuNum = count($NineLists);
            $NineLiuPercent = round($NineLiuNum / $RegNum, 2) * 100;
            $NineDiuPercent = round(($RegNum - $NineLiuNum) / $RegNum, 2) * 100;

            $this->assign("NineLiuPercent", $NineLiuPercent);
            $this->assign("NineDiuPercent", $NineDiuPercent);

            //前八天留存和丢失
            $EightLists = M("users_login_lists")->where("logintime>={$qianEightstartDate} and logintime<={$qianEightendDate} and uid in({$str}) ")->select();
            $EightLiuNum = count($EightLists);
            $EightLiuPercent = round($EightLiuNum / $RegNum, 2) * 100;
            $EightDiuPercent = round(($RegNum - $EightLiuNum) / $RegNum, 2) * 100;

            $this->assign("EightLiuPercent", $EightLiuPercent);
            $this->assign("EightDiuPercent", $EightDiuPercent);

            //前七天留存和丢失
            $SevenLists = M("users_login_lists")->where("logintime>={$qianSevenstartDate} and logintime<={$qianSevenendDate} and uid in({$str}) ")->select();
            $SevenLiuNum = count($SevenLists);
            $SevenLiuPercent = round($SevenLiuNum / $RegNum, 2) * 100;
            $SevenDiuPercent = round(($RegNum - $SevenLiuNum) / $RegNum, 2) * 100;

            $this->assign("SevenLiuPercent", $SevenLiuPercent);
            $this->assign("SevenDiuPercent", $SevenDiuPercent);

            //前六天留存和丢失
            $SixLists = M("users_login_lists")->where("logintime>={$qianSixstartDate} and logintime<={$qianSixendDate} and uid in({$str}) ")->select();
            $SixLiuNum = count($SixLists);
            $SixLiuPercent = round($SixLiuNum / $RegNum, 2) * 100;
            $SixDiuPercent = round(($RegNum - $SixLiuNum) / $RegNum, 2) * 100;

            $this->assign("SixLiuPercent", $SixLiuPercent);
            $this->assign("SixDiuPercent", $SixDiuPercent);

            //前五天留存和丢失
            $FiveLists = M("users_login_lists")->where("logintime>={$qianFivestartDate} and logintime<={$qianFiveendDate} and uid in({$str}) ")->select();
            $FiveLiuNum = count($FiveLists);
            $FiveLiuPercent = round($FiveLiuNum / $RegNum, 2) * 100;
            $FiveDiuPercent = round(($RegNum - $FiveLiuNum) / $RegNum, 2) * 100;

            $this->assign("FiveLiuPercent", $FiveLiuPercent);
            $this->assign("FiveDiuPercent", $FiveDiuPercent);

            //前四天留存和丢失
            $FourLists = M("users_login_lists")->where("logintime>={$qianFourstartDate} and logintime<={$qianFourendDate} and uid in({$str}) ")->select();
            $FourLiuNum = count($FourLists);
            $FourLiuPercent = round($FourLiuNum / $RegNum, 2) * 100;
            $FourDiuPercent = round(($RegNum - $FourLiuNum) / $RegNum, 2) * 100;

            $this->assign("FourLiuPercent", $FourLiuPercent);
            $this->assign("FourDiuPercent", $FourDiuPercent);

            //前天留存和丢失
            $ThreeLists = M("users_login_lists")->where("logintime>={$qianstartDate} and logintime<={$qianendDate} and uid in({$str}) ")->select();
            $ThreeLiuNum = count($ThreeLists);
            $ThreeLiuPercent = round($ThreeLiuNum / $RegNum, 2) * 100;
            $ThreeDiuPercent = round(($RegNum - $ThreeLiuNum) / $RegNum, 2) * 100;

            $this->assign("ThreeLiuPercent", $ThreeLiuPercent);
            $this->assign("ThreeDiuPercent", $ThreeDiuPercent);

            //昨天留存和丢失
            $TwoLists = M("users_login_lists")->where("logintime>={$yestodaystartDate} and logintime<={$yestodayendDate} and uid in({$str}) ")->select();
            $TwoLiuNum = count($TwoLists);
            $TwoLiuPercent = round($TwoLiuNum / $RegNum, 2) * 100;
            $TwoDiuPercent = round(($RegNum - $TwoLiuNum) / $RegNum, 2) * 100;

            $this->assign("TwoLiuPercent", $TwoLiuPercent);
            $this->assign("TwoDiuPercent", $TwoDiuPercent);

            //今日留存和丢失

            $todayLists = M("users_login_lists")->where("logintime>={$todaystartDateInt} and logintime<={$todayendDateInt} and uid in({$str}) ")->select();
            $todayLiuNum = count($todayLists);
            $todayLiuPercent = round($todayLiuNum / $RegNum, 2) * 100;
            $todayDiuPercent = round(($RegNum - $todayLiuNum) / $RegNum, 2) * 100;

            $this->assign("todayLiuPercent", $todayLiuPercent);//今日留存
            $this->assign("todayDiuPercent", $todayDiuPercent);//今日丢失
            $this->assign("RegNum", $RegNum);

        }


        /*三十日老用户流失*/
        $ThirtyCount = M("users_login_lists")->where("logintime>={$qianThirtystartDate} and logintime<={$qianThirtyendDate}")->count();
        $TwentyNineCount = M("users_login_lists")->where("logintime>={$qianTwentyNinestartDate} and logintime<={$qianTwentyNineendDate}")->count();
        $TwentyEightCount = M("users_login_lists")->where("logintime>={$qianTwentyEightstartDate} and logintime<={$qianTwentyEightendDate}")->count();
        $TwentySevenCount = M("users_login_lists")->where("logintime>={$qianTwentySevenstartDate} and logintime<={$qianTwentySevenendDate}")->count();
        $TwentySixCount = M("users_login_lists")->where("logintime>={$qianTwentySixstartDate} and logintime<={$qianTwentySixendDate}")->count();
        $TwentyFiveCount = M("users_login_lists")->where("logintime>={$qianTwentyFivestartDate} and logintime<={$qianTwentyFiveendDate}")->count();
        $TwentyFourCount = M("users_login_lists")->where("logintime>={$qianTwentyFourstartDate} and logintime<={$qianTwentyFourendDate}")->count();
        $TwentyThreeCount = M("users_login_lists")->where("logintime>={$qianTwentyThreestartDate} and logintime<={$qianTwentyThreeendDate}")->count();
        $TwentyTwoCount = M("users_login_lists")->where("logintime>={$qianTwentyTwostartDate} and logintime<={$qianTwentyTwoendDate}")->count();
        $TwentyOneCount = M("users_login_lists")->where("logintime>={$qianTwentyOnestartDate} and logintime<={$qianTwentyOneendDate}")->count();
        $TwentyCount = M("users_login_lists")->where("logintime>={$qianTwentystartDate} and logintime<={$qianTwentyendDate}")->count();
        $NineteenCount = M("users_login_lists")->where("logintime>={$qianNineteenstartDate} and logintime<={$qianNineteenendDate}")->count();

        $EighteenCount = M("users_login_lists")->where("logintime>={$qianEighteenstartDate} and logintime<={$qianEighteenendDate}")->count();
        $SeventeenCount = M("users_login_lists")->where("logintime>={$qianSeventeenstartDate} and logintime<={$qianSeventeenendDate}")->count();
        $SixteenCount = M("users_login_lists")->where("logintime>={$qianSixteenstartDate} and logintime<={$qianSixteenendDate}")->count();
        $FifteenCount = M("users_login_lists")->where("logintime>={$qianFifteenstartDate} and logintime<={$qianFifteenendDate}")->count();
        $FourteenCount = M("users_login_lists")->where("logintime>={$qianFourteenstartDate} and logintime<={$qianFourteenendDate}")->count();

        $ThirteenCount = M("users_login_lists")->where("logintime>={$qianThirteenstartDate} and logintime<={$qianThirteenendDate}")->count();
        $TwelveenCount = M("users_login_lists")->where("logintime>={$qianTwelvestartDate} and logintime<={$qianTwelveendDate}")->count();
        $ElevenCount = M("users_login_lists")->where("logintime>={$qianElevenstartDate} and logintime<={$qianElevenendDate}")->count();
        $TenCount = M("users_login_lists")->where("logintime>={$qianTenstartDate} and logintime<={$qianTenendDate}")->count();

        $NineCount = M("users_login_lists")->where("logintime>={$qianNinestartDate} and logintime<={$qianNineendDate}")->count();
        $EightCount = M("users_login_lists")->where("logintime>={$qianEightstartDate} and logintime<={$qianEightendDate}")->count();
        $SevenCount = M("users_login_lists")->where("logintime>={$qianSevenstartDate} and logintime<={$qianSevenendDate}")->count();
        $SixCount = M("users_login_lists")->where("logintime>={$qianSixstartDate} and logintime<={$qianSixendDate}")->count();


        $FiveCount = M("users_login_lists")->where("logintime>={$qianFivestartDate} and logintime<={$qianFiveendDate}")->count();
        $FourCount = M("users_login_lists")->where("logintime>={$qianFourstartDate} and logintime<={$qianFourendDate}")->count();
        $ThreeCount = M("users_login_lists")->where("logintime>={$qianstartDate} and logintime<={$qianendDate}")->count();
        $TwoCount = M("users_login_lists")->where("logintime>={$yestodaystartDate} and logintime<={$yestodayendDate}")->count();
        $todayCount = M("users_login_lists")->where("logintime>={$todaystartDateInt} and logintime<={$todayendDateInt}")->count();

        $lists4 = [$ThirtyCount, $TwentyNineCount, $TwentyEightCount, $TwentySevenCount, $TwentySixCount, $TwentyFiveCount, $TwentyFourCount, $TwentyThreeCount, $TwentyTwoCount, $TwentyOneCount, $TwentyCount, $NineteenCount, $EighteenCount, $SeventeenCount, $SixteenCount, $FifteenCount, $FourteenCount, $ThirteenCount, $TwelveenCount, $ElevenCount, $TenCount, $NineCount, $EightCount, $SevenCount, $SixCount, $FiveCount, $FourCount, $ThreeCount, $TwoCount, $todayCount];
        //var_dump($lists4);

        $pos = array_search(max($lists4), $lists4);

        $lists4Max = $lists4[$pos] + 20;

        $this->assign("lists4", $lists4);
        $this->assign("lists4Max", $lists4Max);


        $this->assign("channels", $channels);

        $this->display();
    }


    function getUserLists()
    {
        $qudaoID = I("qudaoID");
        $type = I("type");//查询类型1：今天

        $coinRecord = M("users_coinrecord");

        if ($type == 1) {//今天
            //今日开始时间
            $todayBeginDate = date('Y-m-d 00:00:00');//今天，格式成时间戳
            $todayBeginDateInt = strtotime($todayBeginDate);

            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$todayBeginDateInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);
        }

        if ($type == 2) {//昨天

            $yestodayBeginDate = date('Y-m-d 00:00:00', strtotime("-1 day"));//昨天，格式成时间戳
            $yestodayEndDate = date('Y-m-d 23:59:59', strtotime("-1 day"));//昨天，格式成时间戳

            $yestodayBeginDateInt = strtotime($yestodayBeginDate);
            $yestodayEndDateInt = strtotime($yestodayEndDate);

            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$yestodayBeginDateInt} and r.addtime<{$yestodayEndDateInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);
        }


        if ($type == 3) {
            $thisWeekStartDate = date('Y-m-d 00:00:00', (strtotime('last day this week') + 24 * 60 * 60));//本周第一天
            $thisWeekEndDate = date('Y-m-d 23:59:59', strtotime('last day next week'));//本周最后一天

            $thisWeekStartDateInt = strtotime($thisWeekStartDate);
            $thisWeekEndDateInt = strtotime($thisWeekEndDate);

            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$thisWeekStartDateInt} and r.addtime<{$thisWeekEndDateInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);

        }

        if ($type == 4) {

            $beginLastweek = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
            $beginLastweek = date('Y-m-d H:i:s', $beginLastweek);
            $beginLastweekInt = strtotime($beginLastweek);

            $endLastweek = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
            $endLastweek = date('Y-m-d H:i:s', $endLastweek);
            $endLastweekInt = strtotime($endLastweek);

            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginLastweekInt} and r.addtime<{$endLastweekInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);


        }

        if ($type == 5) {
            $beginThisMonth = date('Y-m-01 00:00:00');//本月第一天，格式成时间戳
            $endThisMonth = strtotime(date('Y-m-d H:i:s', mktime(23, 59, 59, date('n'), date('t'), date('Y'))));//本月最后一天，格式成时间戳
            $endThisMonth = date('Y-m-d H:i:s', $endThisMonth);

            $beginThisMonthInt = strtotime($beginThisMonth);
            $endThisMonthInt = strtotime($endThisMonth);


            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginThisMonthInt} and r.addtime<{$endThisMonthInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);
        }


        if ($type == 6) {
            $beginLastMonth = date('Y-m-01 00:00:00', strtotime('-1 month'));
            $endLastMonth = date('Y-m-t 23:59:59', strtotime('-1 month'));

            $beginLastMonthInt = strtotime($beginLastMonth);
            $endLastMonthInt = strtotime($endLastMonth);

            $sql = "select distinct r.uid,u.user_nicename as user_nicename from cmf_users_coinrecord as r left join cmf_users as u on r.uid=u.id where r.addtime> {$beginLastMonthInt} and r.addtime<{$endLastMonthInt} and u.qudao_id={$qudaoID} order by r.uid";

            $userlists = $coinRecord->query($sql);


            echo json_encode($userlists);
        }


    }


}
