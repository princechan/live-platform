<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class LiveController extends AdminbaseController
{
    public function index()
    {
        $config = M("config")->where("id='1'")->find();
        $map = [];

        if ($_REQUEST['start_time'] != '') {
            $map['starttime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {

            $map['starttime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {

            $map['starttime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['keyword'] != '') {
            $map['uid'] = $_REQUEST['keyword'];
            $_GET['keyword'] = $_REQUEST['keyword'];
        }

        $live = M("users_liverecord");
        $count = $live->where($map)->count();
        $page = $this->page($count, 20);
        $lists = $live
            ->where($map)
            ->order("id DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();

        foreach ($lists as $k => $v) {
            $userinfo = M("users")->field("user_nicename")->where("id='{$v['uid']}'")->find();
            $lists[$k]['userinfo'] = $userinfo;
            $lists[$k]['liveMinutes'] = $v['endtime'] - $v['starttime'];
        }

        $this->assign('config', $config);
        $this->assign('lists', $lists);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    public function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $result = M("users_liverecord")->delete($id);
            if ($result) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    /**
     * video list
     */
    public function videoList()
    {
        $map = [];
        if ($key = $_REQUEST['keyword']) {
            if (is_numeric($key)) {
                $map['uid'] = $key;
            } else {
                $map['user_nicename'] = ['like', "%$key%"];
            }
            $_GET['keyword'] = $key;
        }

        $count = M('live_video')->where($map)->count();
        $page = $this->page($count, 20);
        $lists = M('live_video')->where($map)->order("id DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select() ?: [];
        foreach ($lists as &$v) {
            $v['created_at'] = date('Y年m月d日 H:i');
        }

        $this->assign('lists', $lists);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));

        $this->display('videoList');
    }

}
