<?php

/**
 * 管理员手动充值记录
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\Admin;
use Common\Lib\Auth\User;

class ManualController extends AdminbaseController
{
    function index()
    {
        if ($_REQUEST['start_time'] != '') {
            $map['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $map['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $map['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['keyword'] != '') {
            $map['touid'] = ["like", "%" . $_REQUEST['keyword'] . "%"];
            $_GET['keyword'] = $_REQUEST['keyword'];
        }

        $live = M("users_charge_admin");
        $count = $live->where($map)->count();
        $page = $this->page($count, 20);
        $lists = $live
            ->where($map)
            ->order("id DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();
        $coin = $live->where($map)->sum("coin");
        foreach ($lists as $k => $v) {
            $userinfo = M("users")->field("user_login,user_nicename")->where("id='$v[touid]'")->find();
            $lists[$k]['user_login'] = $userinfo['user_login'];
            $lists[$k]['user_nicename'] = $userinfo['user_nicename'];
        }
        $this->assign('lists', $lists);
        $this->assign('coin', $coin);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    public function livecoin_list()
    {
        if ($_REQUEST['start_time'] != '') {
            $map['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }
        if ($_REQUEST['end_time'] != '') {
            $map['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $map['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['keyword'] != '') {
            $map['touid'] = ["like", "%" . $_REQUEST['keyword'] . "%"];
            $_GET['keyword'] = $_REQUEST['keyword'];
        }

        $live = M("users_chargelivecoin_admin");
        $count = $live->where($map)->count();
        $page = $this->page($count, 20);
        $lists = $live
            ->where($map)
            ->order("id DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();
        $livecoin = $live->where($map)->sum("livecoin");
        foreach ($lists as $k => $v) {
            $userinfo = M("users")->field("user_login,user_nicename")->where("id='$v[touid]'")->find();
            $lists[$k]['user_login'] = $userinfo['user_login'];
            $lists[$k]['user_nicename'] = $userinfo['user_nicename'];

        }
        $this->assign('lists', $lists);
        $this->assign('livecoin', $livecoin);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    function add()
    {
        $this->display();
    }

    public function add_post()
    {
        $user_login = $_POST['user_login'];
        $coin = $_POST['coin'];
        $users_obj = M("Users");
        if ($user_login == '' || $coin == '') {
            $this->error("信息不全，请填写完整");

        }

        $userlogin = User::getInstance()->encryptName($user_login);
        $uid = $users_obj->where("user_login='$userlogin'")->getField("id");
        if (!$uid) {
            $this->error("会员不存在，请更正");

        }

        $id = Admin::getInstance()->getId();
        $user = $users_obj->where("id=$id")->find();

        $result = M("users_charge_admin")->add([
            "touid" => $uid,
            "coin" => $coin,
            "addtime" => time(),
            "admin" => $user['user_login'],
            "ip" => $_SERVER['REMOTE_ADDR'],
        ]);

        if ($result) {
            M("users")->where("user_login='$userlogin'")->setInc("coin", $coin);

            $this->success("充值成功！");
        } else {
            $this->error("充值失败！");
        }
    }


    public function add_livecoin()
    {
        $this->display();
    }

    public function add_livecoin_post()
    {
        $user_login = $_POST['user_login'];
        $coin = $_POST['coin'];
        $users_obj = M("Users");
        if ($user_login == '' || $coin == '') {
            $this->error("信息不全，请填写完整");
        }
        if ($coin > 2147483647) {
            $this->error('最多只能充值21亿');
        }

        $userlogin = User::getInstance()->encryptName($user_login);
        $uid = $users_obj->where("user_login='$userlogin'")->getField("id");
        if (!$uid) {
            $this->error("会员不存在，请更正");
        }

        $id = Admin::getInstance()->getId();
        $user = $users_obj->where("id=$id")->find();
        $result = M("users_chargelivecoin_admin")->add([
            "touid" => $uid,
            "livecoin" => $coin,
            "addtime" => time(),
            "admin" => $user['user_login'],
            "ip" => $_SERVER['REMOTE_ADDR'],
        ]);

        if ($result) {
            M("users")->where("user_login='$userlogin'")->setInc("livecoin", $coin);
            $this->success("充值成功！");
        } else {
            $this->error("充值失败！");
        }
    }

    function export()
    {
        if ($_REQUEST['start_time'] != '') {
            $map['addtime'] = ["gt", strtotime($_REQUEST['start_time'])];
        }
        if ($_REQUEST['end_time'] != '') {
            $map['addtime'] = ["lt", strtotime($_REQUEST['end_time'])];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $map['addtime'] = ["between", [strtotime($_REQUEST['start_time']), strtotime($_REQUEST['end_time'])]];
        }
        if ($_REQUEST['keyword'] != '') {
            $map['touid'] = ["like", "%" . $_REQUEST['keyword'] . "%"];
        }
        $xlsName = "Excel";
        $live = M("users_charge_admin");
        $xlsData = $live->where($map)->order("addtime DESC")->select();
        foreach ($xlsData as $k => $v) {
            $userinfo = M("users")->field("user_login,user_nicename")->where("id='$v[touid]'")->find();
            $xlsData[$k]['user_nicename'] = $userinfo['user_nicename'] . "(" . $v['touid'] . ")";
            $xlsData[$k]['addtime'] = date("Y-m-d H:i:s", $v['addtime']);
        }
        $cellName = ['A', 'B', 'C', 'D', 'E', 'F'];
        $xlsCell = [
            ['id', '序号'],
            ['admin', '管理员'],
            ['user_nicename', '会员 (账号)(ID)'],
            ['coin', '充值点数'],
            ['ip', 'IP'],
            ['addtime', '时间'],
        ];
        exportExcel($xlsName, $xlsCell, $xlsData, $cellName);
    }


}
