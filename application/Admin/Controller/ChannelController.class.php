<?php

/**
 * 频道
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class ChannelController extends AdminbaseController
{
    function index()
    {
        $channel = M("channel");
        $count = $channel->count();
        $page = $this->page($count, 20);
        $lists = $channel
            ->where()
            ->order("orderno asc")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();
        $this->assign('lists', $lists);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $result = M("channel")->delete($id);
            if ($result) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    //排序
    public function listorders()
    {

        $ids = $_POST['listorders'];
        foreach ($ids as $key => $r) {
            $data['orderno'] = $r;
            M("channel")->where(['id' => $key])->save($data);
        }

        $status = true;
        if ($status) {
            $this->success("排序更新成功！");
        } else {
            $this->error("排序更新失败！");
        }
    }


    function add()
    {
        $this->display();
    }

    function add_post()
    {
        if (IS_POST) {
            $channel = M("channel");
            $liveid = $_POST['liveid'];
            $isexist = $channel->where("liveid='{$liveid}'")->find();
            if ($isexist) {
                $this->error('该直播间已存在');
            }
            $channel->create();
            $result = $channel->add();
            if ($result) {
                $this->success('添加成功');
            } else {
                $this->error('添加失败');
            }
        }
    }

    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $channel = M("channel")->find($id);
            $this->assign('channel', $channel);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    function edit_post()
    {
        if (IS_POST) {
            $channel = M("channel");
            $id = $_POST['id'];
            $liveid = $_POST['liveid'];
            $isexist = $channel->where("liveid='{$liveid}' and id!={$id}")->find();
            if ($isexist) {
                $this->error('该直播间已存在');
            }
            $channel->create();
            $result = $channel->save();
            if ($result !== false) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }
}
