<?php
return [
    "TITLE" => 'Title',
    "DESCRIPTION" => 'Description',
    "LINK" => 'Link',
    "IMAGE" => 'Image',
    "VIEW" => 'View',
    "ALL" => 'All'
];