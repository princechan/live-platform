<?php
return [
    "Name" => "Name",
    "TEXT_DOMAIN" => 'Text Domain',
    "HOOKS" => 'Hooks',
    "DESCRIPTION" => 'Description',
    "AUTHOR" => 'Author',
    "UNINSTALLED" => 'Uninstalled',
    "PLUGIN_DISCUSSION" => 'Plugin Discussion',
    "PLUGIN_DOCUMENT" => 'Document'
];