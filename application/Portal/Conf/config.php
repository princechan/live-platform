<?php

return [
    'TAGLIB_BUILD_IN' => THINKCMF_CORE_TAGLIBS . ',Portal\Lib\Taglib\Portal',
    //模板文件MODULE_NAME与ACTION_NAME之间的分割符
    'TMPL_FILE_DEPR' => '/',
    'HTML_CACHE_RULES' => [
        // 定义静态缓存规则
        'article:index' => ['portal/article/{id}', 600],
        'index:index' => ['portal/index', 600],
        'list:index' => ['portal/list/{id}_{p}', 60]
    ],
];
