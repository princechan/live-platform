<?php
return array(
		"USERNAME" => 'Username',
		"USER_FROM" => 'From',
		"AVATAR" => 'Avatar',
		"BINGDING_ACCOUNT" => 'Binding Account',
		"FIRST_LOGIN_TIME" => 'First Login Time',
		"LAST_LOGIN_TIME" => "Last Login Time",
		"LOGIN_TIMES" => 'Login Times',
		"STATUS" => 'Status',
		"ACTIONS" => "Actions",
);