<?php

/**
 * 会员注册登录
 */

namespace User\Controller;

use Common\Controller\HomebaseController;

class IndexController extends HomebaseController
{
    //登录
    public function index()
    {
        $id = I("get.id");

        $users_model = M("Users");

        $user = $users_model->where(["id" => $id])->find();

        if (empty($user)) {
            $this->error("查无此人！");
        }
        $this->assign($user);
        $this->display(":index");

    }

    function is_login()
    {
        if ($this->isUserLogin()) {
            $this->ajaxReturn(["status" => 1, "user" => $this->user]);
        } else {
            $this->ajaxReturn(["status" => 0, "info" => "此用户未登录！"]);
        }
    }

    public function logout()
    {
        $ucenter_syn = C("UCENTER_ENABLED");
        if ($ucenter_syn) {
            include UC_CLIENT_ROOT . "client.php";
            echo uc_user_synlogout();
        }
        redirect(__ROOT__ . "/");
    }

    public function logout2()
    {
        $ucenter_syn = C("UCENTER_ENABLED");
        if ($ucenter_syn) {
            include UC_CLIENT_ROOT . "client.php";
            echo uc_user_synlogout();
        }
        if ($this->getUserId()) {
            $referer = $_SERVER["HTTP_REFERER"];
            $_SESSION['login_http_referer'] = $referer;
            $this->success("退出成功！", __ROOT__ . "/");
        } else {
            redirect(__ROOT__ . "/");
        }
    }

}
