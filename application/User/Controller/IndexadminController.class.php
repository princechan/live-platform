<?php

/**
 * 会员
 */

namespace User\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Live\Live;

class IndexadminController extends AdminbaseController
{

    protected $users_model;

    function _initialize()
    {
        parent::_initialize();
        $this->users_model = D("Common/Users");
    }


    function index()
    {

        $map = [];
        $map['user_type'] = 2;
        $map['isvirtual'] = 0;

        if ($_REQUEST['iszombie'] != '') {
            $map['iszombie'] = $_REQUEST['iszombie'];
            $_GET['iszombie'] = $_REQUEST['iszombie'];
        }

        if ($_REQUEST['isban'] != '') {
            $map['user_status'] = $_REQUEST['isban'];
            $_GET['isban'] = $_REQUEST['isban'];
        }

        if ($_REQUEST['issuper'] != '') {

            $map['issuper'] = $_REQUEST['issuper'];
            $_GET['issuper'] = $_REQUEST['issuper'];
        }


        if ($_REQUEST['ishot'] != '') {
            $map['ishot'] = $_REQUEST['ishot'];
            $_GET['ishot'] = $_REQUEST['ishot'];
        }

        if ($_REQUEST['iszombiep'] != '') {
            $map['iszombiep'] = $_REQUEST['iszombiep'];
            $_GET['iszombiep'] = $_REQUEST['iszombiep'];
        }

        if ($_REQUEST['start_time'] != '') {
            $map['create_time'] = ["gt", $_REQUEST['start_time']];
            $_GET['start_time'] = $_REQUEST['start_time'];
        }

        if ($_REQUEST['end_time'] != '') {
            $map['create_time'] = ["lt", $_REQUEST['end_time']];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }
        if ($_REQUEST['start_time'] != '' && $_REQUEST['end_time'] != '') {
            $map['create_time'] = ["between", [$_REQUEST['start_time'], $_REQUEST['end_time']]];
            $_GET['start_time'] = $_REQUEST['start_time'];
            $_GET['end_time'] = $_REQUEST['end_time'];
        }

        if ($_REQUEST['keyword'] != '') {
            $where['id|user_login|user_nicename'] = ["like", "%" . $_REQUEST['keyword'] . "%"];
            $where['_logic'] = "or";
            $map['_complex'] = $where;

            $_GET['keyword'] = $_REQUEST['keyword'];
        }


        $users_model = $this->users_model;
        $count = $users_model->where($map)->count();
        $page = $this->page($count, 20);
        $lists = $users_model
            ->where($map)
            ->order("id DESC")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();


        $this->assign('lists', $lists);
        $this->assign('formget', $_GET);
        $this->assign("page", $page->show("Admin"));

        $this->display(":index");
    }

    function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->delete();
            if ($rst !== false) {
                /* 删除直播记录 */
                M("users_live")->where("uid='{$id}'")->delete();
                /* 删除直播记录 */
                M("users_liverecord")->where("uid='{$id}'")->delete();
                /* 删除房间管理员 */
                M("users_livemanager")->where("uid='{$id}' or liveuid='{$id}'")->delete();
                /* 删除兑换记录 */
                M("users_exchange")->where("uid='{$id}'")->delete();
                /* 删除消费记录 */
                M("users_coinrecord")->where("uid='{$id}' or touid='{$id}'")->delete();
                /*  删除黑名单*/
                M("users_black")->where("uid='{$id}' or touid='{$id}'")->delete();
                /* 删除关注记录 */
                M("users_attention")->where("uid='{$id}' or touid='{$id}'")->delete();
                /* 删除手动充值记录 */
                M("users_charge_admin")->where("touid='{$id}'")->delete();
                /* 删除举报记录记录 */
                M("users_report")->where("uid='{$id}' or touid='{$id}'")->delete();
                /* 删除反馈记录 */
                M("feedback")->where("uid='{$id}'")->delete();
                /* 删除僵尸 */
                M("users_zombie")->where("uid='{$id}'")->delete();
                /* 删除超管 */
                M("users_super")->where("uid='{$id}'")->delete();
                /*删除赠送礼物中奖*/
                M("users_sendgift_win")->where("uid='{$id}'")->delete();
                /*删除马甲列表*/
                M("users_vest_lists")->where("uid='{$id}' or (liveid='{$id}' and live_type='0')")->delete();
                /*删除登录信息*/
                M("users_login_lists")->where("uid='{$id}'")->delete();
                /*删除认证信息*/
                M("users_auth")->where("uid='{$id}'")->delete();
                /*删除坐骑信息*/
                M("users_car")->where("uid='{$id}'")->delete();
                /*删除提现记录*/
                M("users_cashrecord")->where("uid='{$id}'")->delete();
                /*删除充值记录*/
                M("users_cashrecord")->where("uid='{$id}' or touid='{$id}'")->delete();
                /*删除手动删除直播券记录*/
                M("users_chargelivecoin_admin")->where("touid='{$id}'")->delete();
                /*删除连麦记录*/
                M("users_connect_video")->where("uid='{$id}' or liveid='{$id}'")->delete();
                /*删除IP禁用记录*/
                M("users_disable_ips")->where("uid='{$id}'")->delete();
                /*删除抢板凳记录*/
                M("users_grabbench")->where("uid='{$id}'")->delete();
                /*删除守护记录*/
                M("users_guard_lists")->where("uid='{$id}' or liveuid='{$id}'")->delete();
                /*删除抢红包记录*/
                M("users_rob_redpackets")->where("uid='{$id}' or senduid='{$id}' or liveuid='{$id}'")->delete();
                /*删除发红包记录*/
                M("users_send_redpackets")->where("uid='{$id}' or roomid='{$roomid}'")->delete();
                User::getInstance()->getUserInfo($id, 1);
                $this->success("会员删除成功！");
            } else {
                $this->error('会员删除失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    public function ban()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('user_status', '0');
            if ($rst !== false) {
                $nowtime = time();
                $time = $nowtime + 60 * 60 * 1;
                $live = M("users_live")->field("uid")->where("islive='1'")->select();
                foreach ($live as $k => $v) {
                    Live::getInstance()->setGag(0, $v['uid'], $id, $time);
                }
                User::getInstance()->getUserInfo($id, 1);
                $this->success("会员拉黑成功！", U("indexadmin/index"));
            } else {
                $this->error('会员拉黑失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelban()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('user_status', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("会员启用成功！");
            } else {
                $this->error('会员启用失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelsuper()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("users_super")->where("uid='{$id}'")->delete();
            if ($rst !== false) {
                $redis = CRedis::getInstance();
                $redis->hDel('super', $id);
                User::getInstance()->getUserInfo($id, 1);
                $this->success("会员取超管成功！");
            } else {
                $this->error('会员取消超管失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function super()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('issuper', '1');
            $rst = M("users_super")->add(['uid' => $id, 'addtime' => time()]);
            if ($rst !== false) {
                $redis = CRedis::getInstance();
                $redis->hset('super', $id, '1');
                User::getInstance()->getUserInfo($id, 1);
                $this->success("会员设置超管成功！");
            } else {
                $this->error('会员设置超管失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelhot()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('ishot', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("会员取消热门成功！");
            } else {
                $this->error('会员取消热门失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function hot()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('ishot', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("会员设置热门成功！");
            } else {
                $this->error('会员设置热门失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelrecommend()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('isrecommend', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("会员取消推荐成功！");
            } else {
                $this->error('会员取消推荐失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function recommend()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('isrecommend', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("会员推荐成功！");
            } else {
                $this->error('会员推荐失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelzombie()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iszombie', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("关闭成功！");
            } else {
                $this->error('关闭失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function zombie()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iszombie', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("开启成功！");
            } else {
                $this->error('开启失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function zombieall()
    {
        $iszombie = intval($_GET['iszombie']);

        $rst = M("Users")->where("user_type='2'")->setField('iszombie', $iszombie);
        User::getInstance()->getUserInfo($id, 1);
        if ($rst !== false) {
            $this->success("操作成功！");
        } else {
            $this->error('操作失败！');
        }

    }

    function cancelzombiep()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iszombiep', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                M("users_zombie")->where("uid='{$id}'")->delete();
                $this->success("关闭成功！");
            } else {
                $this->error('关闭失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function zombiep()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iszombiep', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                M("users_zombie")->add(["uid" => $id]);
                $this->success("开启成功！");
            } else {
                $this->error('开启失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    //批量设置僵尸粉
    public function zombiepbatch()
    {
        $iszombiep = intval($_GET['iszombiep']);
        $ids = $_POST['ids'];
        $tids = join(",", $_POST['ids']);
        $rst = M("Users")->where("id in ({$tids}) and user_type='2'")->setField('iszombiep', $iszombiep);
        User::getInstance()->getUserInfo($id, 1);
        if ($rst !== false) {
            if ($iszombiep == 1) {
                foreach ($ids as $k => $v) {
                    M("users_zombie")->add(["uid" => $v]);
                }

            } else {
                M("users_zombie")->where("id in ({$tids}")->delete();
            }
            $this->success("设置成功！");
        } else {
            $this->error('设置失败！');
        }
    }

    function cancelrecord()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('isrecord', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("关闭成功！");
            } else {
                $this->error('关闭失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function record()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('isrecord', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("开启成功！");
            } else {
                $this->error('开启失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function white()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iswhite', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("成功加入白名单！");
            } else {
                $this->error('加入白名单失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }


    function cancelwhite()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('iswhite', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("成功移出白名单！");
            } else {
                $this->error('移出白名单失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function sendimg()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('issendimg', '1');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("成功开启发图功能！");
            } else {
                $this->error('开启发图功能失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelsendimg()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(["id" => $id, "user_type" => 2])->setField('issendimg', '0');
            User::getInstance()->getUserInfo($id, 1);
            if ($rst !== false) {
                $this->success("成功关闭发图功能！");
            } else {
                $this->error('关闭发图功能失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }


    function recordall()
    {
        $isrecord = intval($_GET['isrecord']);

        $rst = M("Users")->where("user_type='2'")->setField('isrecord', $isrecord);
        User::getInstance()->getUserInfo($id, 1);
        if ($rst !== false) {
            $this->success("操作成功！");
        } else {
            $this->error('操作失败！');
        }
    }

    function add()
    {
        $this->display(":add");
    }

    function add_post()
    {
        if (IS_POST) {
            $user = $this->users_model;
            $user_login = I('user_login');
            $user_nicename = I('user_nicename');
            if (!$user_login || !$user_nicename) {
                $this->error('请填写用户名、昵称');
            }

            $user->user_login = User::getInstance()->encryptName($user_login);

            if ($user->create()) {
                $user->user_type = 2;
                $user->user_status = 1;
                $user->user_pass = sp_password($_POST['user_pass']);
                $avatar = $_POST['avatar'];

                if ($avatar == '') {
                    $user->avatar = '/default.jpg';
                    $user->avatar_thumb = '/default_thumb.jpg';
                } else if (strpos($avatar, 'http') === 0) {
                    /* 绝对路径 */
                    $user->avatar = $avatar;
                    $user->avatar_thumb = $avatar;
                } else if (strpos($avatar, '/') === 0) {
                    /* 本地图片 */
                    $user->avatar = $avatar;
                    $user->avatar_thumb = $avatar;
                } else {
                    /* 七牛 */
                    //$user->avatar=  $avatar.'?imageView2/2/w/600/h/600'; //600 X 600
                    //$user->avatar_thumb=  $avatar.'?imageView2/2/w/200/h/200'; // 200 X 200
                }

                $user->last_login_time = date('Y-m-d H:i:s', time());
                $user->create_time = date('Y-m-d H:i:s', time());
                $result = $user->add();
                if ($result !== false) {
                    User::getInstance()->getUserInfo($user->id, 1);
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }
            } else {
                $this->error($this->users_model->getError());
            }
        }
    }

    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $userinfo = M("users")->find($id);
            $this->assign('userinfo', $userinfo);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display(":edit");
    }

    function edit_post()
    {
        if (IS_POST) {
            $user = M("users");
            $user->create();
            $avatar = $_POST['avatar'];
            if ($avatar == '') {
                $user->avatar = '/default.jpg';
                $user->avatar_thumb = '/default_thumb.jpg';
            } else if (strpos($avatar, 'http') === 0) {
                /* 绝对路径 */
                $user->avatar = $avatar;
                $user->avatar_thumb = $avatar;
            } else if (strpos($avatar, '/') === 0) {
                /* 本地图片 */
                $user->avatar = $avatar;
                $user->avatar_thumb = $avatar;
            } else {
                /* 七牛 */
                //$user->avatar=  $avatar.'?imageView2/2/w/600/h/600'; //600 X 600
                //$user->avatar_thumb=  $avatar.'?imageView2/2/w/200/h/200'; // 200 X 200
            }
            $user->updated_at = time();
            $result = $user->save();
            User::getInstance()->getUserInfo($_POST['id'], true);
            if ($result !== false) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }

    function importuser()
    {
        $this->display(":importuser");
    }

    public function excel_import()
    {

        $file = $_FILES['file'];

        if (IS_POST) {
            $savepath = date('Ymd') . '/';
            //上传处理类
            $config = [
                'rootPath' => './' . C("UPLOADPATH"),
                'savePath' => $savepath,
                'maxSize' => 11048576,
                'saveName' => ['uniqid', ''],
                'exts' => ['xls', 'xlsx'],
                'autoSub' => false,
            ];
            $upload = new \Think\Upload($config);//
            $info = $upload->upload();

            //开始上传
            if ($info) {
                //上传成功
                //写入附件数据库信息
                $first = array_shift($info);
                if (!empty($first['url'])) {
                    $url = $first['url'];

                } else {
                    $url = C("TMPL_PARSE_STRING.__UPLOAD__") . $savepath . $first['savename'];

                }

                Vendor("PHPExcel.PHPExcel.IOFactory");

                if (substr($url, -5) == ".xlsx") {
                    $objReader = \PHPExcel_IOFactory::createReader('Excel2007');

                } else {
                    $objReader = \PHPExcel_IOFactory::createReader('Excel5');

                }

                //$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
                $objPHPExcel = $objReader->load('./' . C("UPLOADPATH") . $savepath . $first['savename']); //$filename可以是上传的文件，或者是指定的文件

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow(); // 取得总行数
                $highestColumn = $sheet->getHighestColumn(); // 取得总列数


                $data = [];
                $time = time();
                //循环读取excel文件,读取一条,插入一条
                for ($num = 2, $isi = 0, $isn = 0; $num <= $highestRow; $num++) {
                    $user_login = $objPHPExcel->getActiveSheet()->getCell("A" . $num)->getValue();//获取A列的值:登录名称
                    $user_pass = $objPHPExcel->getActiveSheet()->getCell("B" . $num)->getValue();//获取B列的值：登录密码// "123456"
                    $avatar = $objPHPExcel->getActiveSheet()->getCell("C" . $num)->getValue();//获取C列的值：头像
                    $sex = $objPHPExcel->getActiveSheet()->getCell("D" . $num)->getValue();//获取D列的值：性别
                    $user_status = $objPHPExcel->getActiveSheet()->getCell("E" . $num)->getValue();//获取E列的值：用户状态 0：禁用； 1：正常 ；2：未验证
                    $user_type = $objPHPExcel->getActiveSheet()->getCell("F" . $num)->getValue();//获取F列的值：用户类型，1:admin ;2:会员


                    $userinfo = [
                        'user_login' => $user_login,
                        'user_pass' => sp_password($password),
                        'user_nicename' => "user_" . substr($user_login, -4),//$time.$user_login,//昵称
                        'avatar' => $avatar,
                        'sex' => $sex,
                        'user_status' => $user_status,
                        'user_type' => $user_type,
                        'create_time' => date("Y-m-d H:i:s", $time),
                    ];
                    $isexist = M("users")->where("user_login='$user_login'")->find();

                    if (!$isexist) {
                        $data[$isi] = $userinfo;
                        $isi++;
                    } else {
                        $isn++;
                    }
                }
                $result = M("users")->addAll($data);
                if ($result) {
                    $this->success("导入成功！有{$isn}条重复信息未导入");
                } else {
                    $this->error("导入失败！所有手机号已存在");
                }
            } else {
                //上传失败，返回错误
                $this->error($upload->getError());
            }
        }

    }

}
