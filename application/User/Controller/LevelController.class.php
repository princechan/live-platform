<?php

namespace User\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;

class LevelController extends HomebaseController
{
    public function index()
    {
        $uid = (int)I("uid");

        $experience = M("users")->where(['id' => $uid])->getField("consumption");
        $levelId = User::getInstance()->getLevel($experience);
        $level = M("experlevel")->where(['levelid' => $levelId])->find();
        $cha = $level['level_up'] + 1 - $experience;
        $rate = floor(($experience - $level['level_low']) / ($level['level_up'] + 1 - $level['level_low']) * 100);

        $this->assign("experience", $experience);
        $this->assign("level", $level);
        $this->assign("cha", $cha);
        $this->assign("rate", $rate);

        $this->display();
    }

}
