<?php
/**
 * 消费和充值记录
 */

namespace User\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;

class RecordController extends HomebaseController
{
    /**
     * 消费记录
     */
    public function spend()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('users_coinrecord')->where(['uid' => $uid])->count();
        $spendList = M('users_coinrecord')->where(['uid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];
        $Car = M('car');
        $gift = M('gift');
        foreach ($spendList as $k => $v) {
            $spendList[$k]['userinfo'] = User::getInstance()->getUserInfo($v['touid']);
            $action = $v['action'];
            if (substr($action, 0, 6) == "buycar") {//买座驾
                $spendList[$k]['giftName'] = $Car->where("id={$v['giftid']}")->getField('name');
                $spendList[$k]['use'] = "购买座驾";
            }

            if ($action == "sendbighorn") {//大喇叭
                $spendList[$k]['giftName'] = "大喇叭";
                $spendList[$k]['use'] = "购买喇叭";
            }

            if ($action == "sendbarrage") {//发弹幕
                $spendList[$k]['giftName'] = "发弹幕";
                $spendList[$k]['use'] = "发弹幕";
            }

            if ($action == "buyguard") {//购买守护
                $spendList[$k]['giftName'] = "购买守护";
                $spendList[$k]['use'] = "购买守护";
            }

            if ($action == "roomcharge") {//收费房间进入
                $spendList[$k]['giftName'] = "付费房间";
                $spendList[$k]['use'] = "付费房间";
            }

            if ($action == "sendgift") {//送礼物
                $spendList[$k]['giftName'] = $gift->where("id={$v['giftid']}")->getField("giftname");
                $spendList[$k]['use'] = "送礼物";
            }
        }

        $this->assign("spendList", $spendList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

    /**
     * 充值记录
     */
    public function charge()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('users_chargelivecoin_admin')->where(['touid' => $uid])->count();
        $chargeList = M('users_chargelivecoin_admin')
            ->where(['touid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];

        $this->assign("chargeList", $chargeList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

    /**
     * 发红包记录
     */
    public function sendpacket()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('users_send_redpackets')->where(['uid' => $uid])->count();
        $spendList = M('users_send_redpackets')
            ->where(['uid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];
        foreach ($spendList as &$v) {
            $v['liveUserName'] = '';
            if ($v['live_type'] == 0) {
                $v['liveUserName'] = M('users')->where(['id' => $v['liveid']])->getField("user_nicename");
            } else {
                $live = M('muti_show')->where(['id' => $v['liveid']])->find();
                $v['liveUserName'] = $live ? $live['live_name'] : '';
            }
        }

        $this->assign("spendList", $spendList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

    /**
     * 抢红包记录
     */
    public function getpacket()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('users_rob_redpackets')->where(['uid' => $uid])->count();
        $getList = M('users_rob_redpackets')
            ->where(['uid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];
        foreach ($getList as &$v) {
            $v['liveUserName'] = '';
            if ($v['live_type'] == 0) {
                $v['liveUserName'] = M('users')->where(['id' => $v['liveid']])->getField("user_nicename");
            } else {
                $live = M('muti_show')->where(['id' => $v['liveid']])->find();
                $v['liveUserName'] = $live ? $live['live_name'] : '';
            }
            $v['sendUserName'] = M('users')->where(['id' => $v['senduid']])->getField("user_nicename");
        }

        $this->assign("getList", $getList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

    /**
     * 中奖记录
     */
    public function wingift()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('users_sendgift_win')->where(['uid' => $uid])->count();
        $winList = M('users_sendgift_win')
            ->where(['uid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];
        foreach ($winList as $k => $v) {
            $winList[$k]['liveUserName'] = M("users")->where("id='{$v['liveuid']}'")->getField("user_nicename");
            $winList[$k]['giftName'] = M("gift")->where("id='{$v['giftid']}'")->getField("giftname");
        }

        $this->assign("winList", $winList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

    public function backpacket()
    {
        $uid = I("uid");
        $token = I("token");
        $start = I("start");
        $end = I("end");

        //判断用户是否登录
        $userInfo = M("users")->where(['id' => $uid, 'token' => $token])->select();
        if (!$uid || !$token || !$userInfo || !$end) {
            $this->assign("woringMsg", "用户信息验证失败，请重新登录");
            $this->display();
            exit;
        }

        $count = M('redpcakets_back')->where(['uid' => $uid])->count();
        $backList = M('redpcakets_back')
            ->where(['uid' => $uid])
            ->order('id desc')
            ->limit($start, $end)
            ->select() ?: [];
        foreach ($backList as &$v) {
            $redpacket = M("users_send_redpackets")->where(['id' => $v['packet_id']])->find();
            $v['packetInfo'] = $redpacket;
            $v['liveUserName'] = '';
            if ($redpacket['live_type'] == 0) {
                $v['liveUserName'] = M('users')->where(['id' => $redpacket['liveid']])->getField("user_nicename");
            } else {
                $live = M('muti_show')->where(['id' => $redpacket['liveid']])->find();
                $v['liveUserName'] = $live ? $live['live_name'] : '';
            }
        }

        $this->assign("backList", $backList);
        $this->assign("uid", $uid);
        $this->assign("start", $start);
        $this->assign("end", $end);
        $this->assign("count", $count);
        $this->assign("token", $token);

        $this->display();
    }

}
