<?php

/**
 * 附件上传
 */

namespace Asset\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Auth\Admin;
use Common\Lib\Auth\User;

class AssetController extends AdminbaseController
{
    function _initialize()
    {
        $adminId = Admin::getInstance()->getId();
        $userId = User::getInstance()->getUserId();
        if (empty($adminId) && empty($userId)) {
            exit("非法上传！");
        }
    }

    /**
     * 上传
     */
    public function swfupload()
    {
        if (IS_POST) {
            $savepath = date('Ymd') . '/';
            $config = [
                'rootPath' => './' . C("UPLOADPATH"),
                'savePath' => $savepath,
                'maxSize' => 11048576,
                'saveName' => ['uniqid', ''],
                'exts' => ['jpg', 'gif', 'png', 'jpeg', "txt", 'zip'],
                'autoSub' => false,
            ];
            $upload = new \Think\Upload($config);
            $info = $upload->upload();
            if ($info) {
                //上传成功
                $first = array_shift($info);
                if (!empty($first['url'])) {
                    $url = $first['url'];
                } else {
                    $url = C("TMPL_PARSE_STRING.__UPLOAD__") . $savepath . $first['savename'];
                }

                echo "1," . $url . "," . '1,' . $first['name'];
                exit;
            } else {
                //上传失败，返回错误
                exit("0," . $upload->getError());
            }
        } else {
            $this->display(':swfupload');
        }
    }

    /**
     * 上传APK文件
     */
    public function swfuploadAPK()
    {
        set_time_limit(300);
        if (IS_POST) {
            $savepath = 'apk/'.date('Ymd') . '/';
            $config = [
                'rootPath' => './' . C("UPLOADPATH"),
                'savePath' => $savepath,
                'maxSize' => 52428800,
                'saveName' => ['uniqid', ''],
                'exts' => [ 'apk','zip'],
                'autoSub' => false,
            ];
            $upload = new \Think\Upload($config);
            $info = $upload->upload();
            if ($info) {
                //上传成功
                $first = array_shift($info);
                if (!empty($first['url'])) {
                    $url = $first['url'];
                } else {
                    $url = C("TMPL_PARSE_STRING.__UPLOAD__") . $savepath . $first['savename'];
                }

                echo "1," . $url . "," . '1,' . $first['name'];
                exit;
            } else {
                //上传失败，返回错误
                exit("0," . $upload->getError());
            }
        } else {
            $this->display(':swfuploadAPK');
        }
    }

}
