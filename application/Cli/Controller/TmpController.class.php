<?php

namespace Cli\Controller;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;
use Think\Controller;

class TmpController extends Controller
{
    /**
     * php index.php cli/tmp/updateUserLogin
     */
    public function updateUserLogin()
    {
        echo PHP_EOL, 'clear user cache', PHP_EOL;
        $keys = CRedis::getInstance()->keys(User::KEY_CACHE_USER . '*');
        foreach ($keys as $key) {
            echo $key, PHP_EOL;
            CRedis::getInstance()->del($key);
        }
        echo PHP_EOL, 'clear user cache', PHP_EOL;

        $users = M('users')->order('id asc')->select();
        foreach ($users as $user) {
            echo $user['id'], PHP_EOL;
            if (strlen($user['user_login']) == 32) {
                continue;
            }

            //admin
            if ($user['user_type'] == 1) {

            } else {
                $data = [
                    'user_login' => User::getInstance()->encryptName($user['user_login']),
                ];
                M('users')->where(['id' => $user['id']])->save($data);
            }

            User::getInstance()->getUserInfo($user['id'], true);
        }
    }

    //
    public function getUserList()
    {
        $users = M('users')->field('id,user_nicename,create_time,last_login_time,login_type,vest_id')->order('id asc')->select();
        $file = '/tmp/cmf_user';
        file_put_contents($file, 'id,user_nicename,create_time,last_login_time,login_type,vest_id');
        foreach ($users as $user) {
            $username = str_replace('*', '', $user['user_nicename']);
            if (preg_match('/^[0-9]+$/', $username)) {
                file_put_contents($file, "{$user['id']},{$user['user_nicename']},{$user['create_time']},{$user['last_login_time']},{$user['login_type']},{$user['vest_id']}" . PHP_EOL, FILE_APPEND);
            }
        }
    }

}
