<?php

namespace Cli\Controller;

use Think\Controller;
use Common\Lib\Auth\User;

class EasemobController extends Controller
{
    /**
     * 游客账号清理：定时发出环形REST接口请求删除5天前的环信用户数据
     * php index.php cli/easemob/delTmpUser
     */
    public function delTmpUser()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        User::getInstance()->delTmpEasemobUsers();
    }

    /**
     * 创建环信临时账号，用户清理环信临时账号
     */
    public function createTmpUser()
    {
        $name = I('post.name');
        User::getInstance()->createTmpEasemobUser($name);
    }

}
