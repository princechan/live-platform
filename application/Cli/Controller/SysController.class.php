<?php

namespace Cli\Controller;

use Common\Lib\Helpers\CRedis;
use Think\Controller;
use Common\Lib\Helpers\Func;

class SysController extends Controller
{
    public function clearCache()
    {
        Func::clearCache();
        die('clear cache finished');
    }

    public function clearTmpUser()
    {
        $redis = CRedis::getInstance();
        $keys = $redis->keys('temp_*');
        print_r($keys);
        foreach ($keys as $key) {
            $redis->del($key);
        }
    }

}
