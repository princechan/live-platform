<?php
/**
 * 贡献榜
 */

namespace Appapi\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;

class ContributeController extends HomebaseController
{
    public function index()
    {
        $uid = (int)I("uid");
        if (!$uid) {
            return $this->renderJson([], 400, '缺参数uid');
        }

        $p = 1;
        $page_nums = 20;
        $start = ($p - 1) * $page_nums;
        $list = M("users_coinrecord")
            ->field("uid,sum(total_livecoin) as total")
            ->where(" action in ('sendgift','sendbarrage') and touid='{$uid}'")
            ->group("uid")
            ->order("total desc")
            ->limit($start, $page_nums)
            ->select() ?: [];
        foreach ($list as $k => $v) {
            $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
        }

        $this->assign("uid", $uid);
        $this->assign("p", $p + 1);
        $this->assign("list", $list);

        $this->display();
    }

    public function getmore()
    {
        $uid = (int)I("uid");
        $p = (int)I("page");
        if (!$uid) {
            return $this->renderJson([], 400, '缺参数uid');
        }

        $page_nums = 20;
        if ($p <= 1) {
            $p = 1;
        }
        $start = ($p - 1) * $page_nums;
        $list = M("users_coinrecord")
            ->field("uid,sum(total_livecoin) as total")
            ->where(" action in ('sendgift','sendbarrage') and touid='{$uid}'")
            ->group("uid")
            ->order("total desc")
            ->limit($start, $page_nums)
            ->select() ?: [];
        foreach ($list as $k => $v) {
            $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
        }

        $nums = count($list);
        if ($nums < $page_nums) {
            $isscroll = 0;
        } else {
            $isscroll = 1;
        }

        return $this->renderJson([
            'data' => $list,
            'nums' => $nums,
            'start' => $start,
            'isscroll' => $isscroll,
        ]);
    }

    public function order()
    {
        $uid = I("uid");
        $type = I("type");
        if (!$uid) {
            die('缺参数uid');
        }

        if ($type == 'week') {
            $nowtime = time();
            $w = date('w', $nowtime);
            //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
            $first = 1;
            $week = date('Y-m-d H:i:s', strtotime(date("Ymd") . "-" . ($w ? $w - $first : 6) . ' days'));
            $week_start = strtotime(date("Ymd") . "-" . ($w ? $w - $first : 6) . ' days');
            //本周结束日期
            $week_end = strtotime("{$week} +1 week") - 1;
            $list = M("users_coinrecord")
                ->field("uid,sum(total_livecoin) as total")
                ->where(" action in ('sendgift','sendbarrage') and touid='{$uid}' and addtime>{$week_start} and addtime<{$week_end}")
                ->group("uid")
                ->order("total desc")
                ->limit(0, 20)
                ->select() ?: [];
            foreach ($list as $k => $v) {
                $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
            }
        } else {
            $list = M("users_coinrecord")
                ->field("uid,sum(total_livecoin) as total")
                ->where(" action in ('sendgift','sendbarrage') and touid='{$uid}'")
                ->group("uid")
                ->order("total desc")
                ->limit(0, 20)
                ->select() ?: [];
            foreach ($list as $k => $v) {
                $list[$k]['userinfo'] = User::getInstance()->getUserInfo($v['uid']);
                if ($v['total'] == 0) {
                    unset($list[$k]);
                }
            }
        }

        $this->assign("list", $list);

        $this->display();
    }

}
