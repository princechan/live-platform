<?php
/**
 * 会员等级
 */

namespace Appapi\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Auth\User;

class LevelController extends HomebaseController
{
    public function index()
    {
        $uid = (int)I("uid");

        $experience = M("users")->where(['id' => $uid])->getField("consumption");
        $level = M('experlevel')->where("level_up>='$experience'")->order("levelid asc")->find();
        $cha = $level['level_up'] + 1 - $experience;
        if ($level) {
            $baifen = ($experience / ($cha + $experience)) * 100;
            $type = "1";
        } else {
            $baifen = 0;
            $type = "0";
            $level = 1;
        }

        $info = User::getInstance()->getUserInfo($uid);

        $this->assign("experience", $experience);
        $this->assign("baifen", $baifen);
        $this->assign("level", $level);
        $this->assign("info", $info);
        $this->assign("cha", $cha);
        $this->assign("type", $type);

        $this->display();
    }

}
