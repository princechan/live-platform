<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Api\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Activity\Activity;

class ActivityController extends HomebaseController
{
    /**
     * 获取活动
     */
    public function getActivity()
    {
        $id = (int)$this->getGet('id');

        $srv = Activity::getInstance();
        $data = $srv->getActivity($id);
        if (false === $data) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject($data);
    }

    /**
     * 报名
     */
    public function enroll()
    {
            if (!IS_POST) {
            return $this->renderError('非法请求');
        }

        $activityId = (int)$this->getPost('activity_id');
        $uid = (int)$this->getUid();

        $srv = Activity::getInstance();
        $rs = $srv->enroll($uid, $activityId);
        if (false === $rs) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

    /**
     * 活动报名用户列表
     */
    public function getEnrollList()
    {
        $activityId = (int)$this->getGet('activity_id');
        $page = (int)$this->getGet('page');
        if ($page <= 1) {
            $page = 1;
        }

        $rs = Activity::getInstance()->getEnrollList($activityId, $page);
        return $this->renderArray($rs);
    }

    /**
     * 活动出局用户列表
     */
    public function getOutList()
    {
        $activityId = (int)$this->getGet('activity_id');
        $page = (int)$this->getGet('page');
        if ($page <= 1) {
            $page = 1;
        }

        $rs = Activity::getInstance()->getOutList($activityId, $page);
        return $this->renderArray($rs);
    }

    /**
     * 抢答排行榜
     */
    public function getPrizeList()
    {
        $activityId = (int)$this->getGet('activity_id');
        $page = (int)$this->getGet('page', 1);
        $limit = (int)$this->getGet('limit', 10);
        if ($page <= 1) {
            $page = 1;
        }
        if ($limit <= 0) {
            $limit = 10;
        }

        $uid = (int)$this->getUid();

        $srv = Activity::getInstance();
        $rs = $srv->getPrizeList($uid, $activityId, $page, $limit);
        if (false === $rs) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject($rs);
    }

    /**
     * 加奖抢答排行榜
     */
    public function getExtPrizeList()
    {
        $activityId = (int)$this->getGet('activity_id');
        $page = (int)$this->getGet('page', 1);
        $limit = (int)$this->getGet('limit', 10);
        if ($page <= 1) {
            $page = 1;
        }
        if ($limit <= 0) {
            $limit = 10;
        }

        $uid = (int)$this->getUid();

        $srv = Activity::getInstance();
        $rs = $srv->getExtPrizeList($uid, $activityId, $page, $limit);
        if (false === $rs) {
            return $this->renderError($srv->getLastErrMsg());
        }
        return $this->renderArray($rs);
    }

    /**
     * 抢答排行总榜
     */
    public function getTotalPrizeList()
    {
        $page = (int)$this->getGet('page');
        if ($page <= 1) {
            $page = 1;
        }

        $list = Activity::getInstance()->getTotalPrizeList($page, 10);
        return $this->renderArray($list);
    }

    /**
     * 获取当前问题详情及备选答案,关联当前主播房间ID
     * @return mixed
     */
    public function getCurrentQuestion()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $live_id = (int)$this->getPost('live_id');
        $uid = (int)$this->getPost('uid');

        if (empty($activity_id) || empty($live_id) || empty($uid)) {
            return $this->renderError('非法请求');
        }

        $lib = Activity::getInstance();
        $ret = $lib->getCurrentQuestion($activity_id, $live_id, $uid);
        if (false === $ret) {
            return $this->renderError($lib->getLastErrMsg());
        }
        return $this->renderArray($ret);
    }

    /**
     * 用户提交答案并验证返回结果
     * @return mixed
     */
    public function pushVerifyAnswer()
    {
        $activity_id    = (int)$this->getPost('activity_id');
        $live_id        = (int)$this->getPost('live_id');
        $answer         = $this->getPost('answer');
        $uid            = (int)$this->getUid();

        //接入业务逻辑
        $lib = Activity::getInstance();
        $ret = $lib->pushVerifyAnswer($activity_id, $live_id, $uid, $answer);

        if (false === $ret) {
            return $this->renderError($lib->getLastErrMsg());
        }
        return $this->renderArray();
    }

    /**
     * 主播公布结果,获取当前问题各个答案人数和
     * @return mixed
     */
    public function answerTotalNum()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $live_id     = (int)$this->getPost('live_id');

        //主播ID
        $uid = (int)$this->getPost('uid');

        //接入业务逻辑
        $lib = Activity::getInstance();
        $ret = $lib->announceResults($activity_id, $live_id, $uid);
        if (false === $ret) {
            return $this->renderError($lib->getLastErrMsg());
        }

        return $this->renderArray($ret);
    }

    //预约提醒待定

    /**
     * 玩家是否已报名
     * @return mixed
     */
    public function isEnroll()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $uid         = (int)$this->getUid();

        //接入业务逻辑
        $lib = Activity::getInstance();
        $ret = $lib->isEnroll($activity_id, $uid);
        $ret = $ret ? 1 : -1;

        return $this->renderArray($ret);
    }

    /**
     * 获取玩家当前答题状态
     * @param $activityId
     * @return mixed
     */
    public function getPlayerStatus()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $live_id     = (int)$this->getPost('live_id');
        $uid         = (int)$this->getUid();

        $lib = Activity::getInstance();
        $ret = $lib->getPlayerStatus($activity_id, $live_id, $uid);

        if (false === $ret) {
            return $this->renderError($lib->getLastErrMsg());
        }
        return $this->renderArray($ret);
    }

    /**
     * 获取当前答题人数
     * @return mixed
     */
    public function getRightAnswerNum()
    {
        $activity_id = (int)$this->getPost('activity_id');

        $lib = Activity::getInstance();
        $ret = $lib->getLastRightAnswerNum($activity_id);
        $num = $ret ? $ret : 0;
        return $this->renderArray($num);
    }

    /**
     * 设置活动状态为开始/活动进行中
     * @return mixed
     */
    public function setActivityStatus()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $live_id     = (int)$this->getPost('live_id');
        $uid         = (int)$this->getPost('uid'); //主播ID
        $status      = (int)$this->getPost('status');

        $lib = Activity::getInstance();
        $ret = $lib->setActivityStatus($activity_id, $live_id, $uid, $status);
        if(false == $ret){
            return $this->renderError($lib->getLastErrMsg());
        }

        return $this->renderArray();
    }

    /**
     * 返回服务器时间
     * @return mixed
     */
    public function getServerTime()
    {
        return $this->renderArray(time());
    }

}
