<?php

namespace Api\Controller;

use Common\Lib\Auth\User;
use Common\Controller\AppframeController;
use Common\Lib\Helpers\Func;

class ThirdpartyuserController extends AppframeController
{
    public function get_captcha()
    {
        $data = ['captcha' => '/index.php?g=Api&m=Thirdpartyuser&a=code'];
        echo json_encode($data);
        exit;
    }

    public function code()
    {
        $width = I('width', 120);
        $height = I('height', 40);

        if ($width < 120) {
            $width = 120;
        }
        if ($height < 40) {
            $height = 40;
        }

        $fontSize = 22;
        $img = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
        $color = imagecolorallocate($img, mt_rand(1, 150), mt_rand(1, 150), mt_rand(1, 150));
        imagefill($img, 0, 0, $white);

        $code = '';
        $codeS = 'abcdefghijklmnpqrtuvwxyz234679ACDEFGHIJKLMNPQRTUVWXYZ';
        for ($i = 0; $i < 4; $i++) {
            $code .= $codeS[rand(0, strlen($codeS) - 1)];
        }

        // 验证码使用随机字体
        $ttfPath = dirname(__FILE__) . '/Verify/ttfs/';

        $dir = dir($ttfPath);
        $ttfs = [];
        while (false !== ($file = $dir->read())) {
            if ($file[0] != '.' && substr($file, -4) == '.ttf') {
                $ttfs[] = $file;
            }
        }
        $dir->close();

        $fontttf = $ttfs[0];//$ttfs[array_rand($ttfs)];
        $fontttf = $ttfPath . $fontttf;
        $codeSet = '2345678abcdefhijkmnpqrstuvwxyz';
        for ($i = 0; $i < 10; $i++) {
            //杂点颜色
            $noiseColor = imagecolorallocate($img, mt_rand(150, 225), mt_rand(150, 225), mt_rand(150, 225));
            for ($j = 0; $j < 5; $j++) {
                // 绘杂点
                imagestring($img, 5, mt_rand(-10, $width), mt_rand(-10, $height), $codeSet[mt_rand(0, 29)], $noiseColor);
            }
        }

        /*
        $py = 0;
        // 曲线前部分
        $A = mt_rand(1, $height / 2);                  // 振幅
        $b = mt_rand(-$height / 4, $height / 4);   // Y轴方向偏移量
        $f = mt_rand(-$height / 4, $height / 4);   // X轴方向偏移量
        $T = mt_rand($height, $width * 2);  // 周期
        $w = (2 * M_PI) / $T;
        $px1 = 0;  // 曲线横坐标起始位置
        $px2 = mt_rand($width / 2, $width * 0.8);  // 曲线横坐标结束位置
        for ($px = $px1; $px <= $px2; $px = $px + 1) {
            if ($w != 0) {
                $py = $A * sin($w * $px + $f) + $b + $height / 2;  // y = Asin(ωx+φ) + b
                $i = (int)($fontSize / 5);
                while ($i > 0) {
                    // 这里(while)循环画像素点比imagettftext和imagestring用字体大小一次画出（不用这while循环）性能要好很多
                    imagesetpixel($img, $px + $i, $py + $i, $color);
                    $i--;
                }
            }
        }

        // 曲线后部分
        $A = mt_rand(1, $height / 2);                  // 振幅
        $f = mt_rand(-$height / 4, $height / 4);   // X轴方向偏移量
        $T = mt_rand($height, $width * 2);  // 周期
        $w = (2 * M_PI) / $T;
        $b = $py - $A * sin($w * $px + $f) - $height / 2;
        $px1 = $px2;
        $px2 = $width;
        for ($px = $px1; $px <= $px2; $px = $px + 1) {
            if ($w != 0) {
                $py = $A * sin($w * $px + $f) + $b + $height / 2;  // y = Asin(ωx+φ) + b
                $i = (int)($fontSize / 5);
                while ($i > 0) {
                    imagesetpixel($img, $px + $i, $py + $i, $color);
                    $i--;
                }
            }
        }
        */

        $codeNX = -5;
        for ($i = 0; $i < 4; $i++) {
            $codeNX += $fontSize;
            imagettftext($img, $fontSize, mt_rand(-15, 15), $codeNX, $fontSize * 1.6, $color, $fontttf, $code[$i]);
        }

        session('code', strtolower($code));
        header("content-type: image/png");
        imagepng($img);
        imagedestroy($img);
        exit;
    }

    /**
     * 注册
     */
    public function register()
    {
        $username = trim(I('username'));
        $password = trim(I('password'));
        $code = trim(strtolower(I('code')));
        $agreement = trim(I('agreement'));

        if (!$agreement) {
            $this->error('请勾选用户协议', '', true);
            exit;
        }

        $session_code = session('code');
        session('code', null);
        if ($code != $session_code) {
            $this->error('验证码错误', '', true);
            exit;
        }

        if (!$username || !$password) {
            $this->error("用户名或密码为空！", '', true);
        }

        $srv = User::getInstance();
        $result = $srv->newRegister($username, $password);
        if ($result) {
            $rst = [
                'status' => 1,
                'msg' => '成功',
            ];
            echo json_encode($rst);
            exit;
        } else {
            $rst = [
                'status' => 0,
                'msg' => $srv->getLastErrMsg(),
            ];
            echo json_encode($rst);
            exit;
        }
    }

    /**
     * 登陆
     */
    public function login()
    {
        $username = trim(I('username'));
        $password = trim(I('password'));

        if (!$username || !$password) {
            $this->error("用户名或密码为空！", '', true);
        }

        $srv = User::getInstance();
        $rs = $srv->login($username, $password);
        if (false === $rs) {
            $this->error($srv->getLastErrMsg(), '', true);
        } else {
            $this->success("登录成功！", '', true);
        }
    }

    /**
     * 检查用户名是否合法
     */
    public function checkLoginName()
    {
        $username = trim(I('username'));

        $prefix = Func::getLoginNamePrefix();
        if (!trim($username, $prefix)) {
            return $this->renderError('用户名为空');
        }

        $srv = User::getInstance();
        $status = $srv->checkLoginName($username);
        if (false === $status) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject([
            'is_valid' => (int)$status, // 1-合法 0-不合法
            'msg' => '',
        ]);
    }

    /**
     * 注销
     */
    public function logout()
    {
        User::getInstance()->logout();
        $this->redirect('/');
    }

}
