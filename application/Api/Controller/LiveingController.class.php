<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Api\Controller;

use Common\Controller\AppframeController;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Live;
use Common\Lib\Live\MultiLive;

class LiveingController extends AppframeController
{
    /**
     * 首页推荐主播
     * $slide = M("slide")->where("slide_status='1' and slide_cid='1'")->order("listorder asc")->select();
     */
    public function recommend()
    {
        $list = M('home_recommend')->select() ?: [];
        if (!$list) {
            $this->renderJson();
            return;
        }

        $liveList = M('users_live')->where([
            'uid' => ['in', array_column($list, 'show_id')],
            'islive' => 1,
        ])->select() ?: [];
        $liveList = Func::index($liveList, 'uid');
        foreach ($list as &$li) {
            $li['route_id'] = $li['show_id'];
            if ($li['show_type'] == 1) {
                $li['route_id'] = MultiLive::ROUTE_PREFIX . $li['show_id'];
            }

            //type：0-直播，1-默认图片
            //$live['type'] = 1;
            //直播状态：0-直播结束 1-直播中
            $li['islive'] = 0;
            $li['video_url'] = '';
            $li['show_cover'] = Func::getUrl($li['show_cover'], 2);
            $li['default_cover'] = Func::getUrl($li['default_cover'], 2);
            $userLive = isset($liveList[$li['show_id']]) ? $liveList[$li['show_id']] : [];
            if (!$userLive) {
                continue;
            }

            if ($userLive['islive'] == 1) {
                $li['islive'] = 1;
            }

            $li['video_url'] = Live::getInstance()->getM3u8Stream($userLive['stream']);
        }

        $this->renderJson([
            'live_list' => $list,
        ]);
    }

    /**
     * 直播公告列表
     */
    public function liveAnnouncement()
    {
        $page = (int)I('get.page', 1);
        $page = $page <= 1 ? 1 : $page;
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $liveAnnouncement = M('announcement')
            ->where(['state' => 0])
            ->order(['sort' => 'asc', 'id' => 'desc'])
            ->limit($offset, $limit)
            ->select() ?: [];
        foreach ($liveAnnouncement as &$liveAn) {
            $liveAn['date'] = date('m/d', strtotime($liveAn['updated_at']));
            $liveAn['title'] = mb_substr($liveAn['title'], 0, 15);
        }

        $this->renderJson($liveAnnouncement);
    }

}
