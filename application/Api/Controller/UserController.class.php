<?php

/**
 * 用户相关接口
 */

namespace Api\Controller;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\Func;
use Common\Lib\Live\Live;
use Think\Controller;

class UserController extends Controller
{
    function get_vest()
    {
        $desc = I('desc');

        $uid = (int)User::getInstance()->getUserId();
        $private_chat_permit = [];

        $userinfo = User::getInstance()->getUserInfo($uid);
        if ($userinfo['vest_id'] > 0) {
            $uservest = Live::getInstance()->getUsersVest($userinfo['vest_id']);
            $chat = explode(',', $uservest['private_chat_permit']);
            $key = array_search(-1, $chat);
            if ($key !== false) {
                unset($chat[$key]);
                $private_chat_permit[] = [
                    'key' => -1,
                    'name' => '游客'
                ];
            }

            $v = M("users_vest")->where(['id' => ['in', $chat]])->select();
            foreach ($v as $val) {
                $private_chat_permit[] = [
                    'key' => $val['id'],
                    'name' => $val['vest_name']
                ];
            }
            $uservest['private_chat_permit'] = $private_chat_permit;
        } else {
            $uservest = [];
        }

        if ($desc) {
            $uservest['desc'] = [
                'vest_name' => '马甲名称',
                'limited_speak' => '受限发言',
                'live_video' => '视频直播',
                'live_voice' => '语音直播',
                'away_user' => '踢人',
                'away_admin' => '踢管理员',
                'gap' => '禁止说话',
                'gap_all' => '全频道禁言',
                'private_chat' => '私聊',
                'private_chat_permit' => '私聊权限',
                //'set_admin' => '设置管理',
            ];
        }

        echo json_encode($uservest);
        exit;
    }

    function permission()
    {
        $result = [];
        $private_chat_permit = [];

        $uid = (int)User::getInstance()->getUserId();
        $data = Func::getPublicConfig();
        $result['global']['limit_talk_type'] = $data['limit_talk_type'];
        $result['global']['limit_talk_num'] = $data['limit_talk_num'];
        $result['global']['register_pop_ups_frequency'] = $data['register_pop_ups_frequency'];

        $liveinfo = M('users_live')->where(['uid' => $uid])->find();
        $result['live']['chat_num'] = $liveinfo['chat_num'];
        $result['live']['chat_frequency'] = $liveinfo['chat_frequency'];
        $result['live']['lianmai_stream'] = $liveinfo['lianmai_stream'];

        $userinfo = User::getInstance()->getUserInfo($uid);
        if ($userinfo['vest_id'] > 0) {
            $uservest = M("users_vest")->where(['id' => $userinfo['vest_id']])->find();

            $chat = explode(',', $uservest['private_chat_permit']);
            $key = array_search(-1, $chat);
            if ($key !== false) {
                unset($chat[$key]);
                $private_chat_permit[] = [
                    'key' => -1,
                    'name' => '游客'
                ];
            }

            $v = M("users_vest")->where(['id' => ['in', $chat]])->select();

            foreach ($v as $val) {
                $private_chat_permit[] = [
                    'key' => $val['id'],
                    'name' => $val['vest_name']
                ];
            }
            $uservest['private_chat_permit'] = $private_chat_permit;
        } else {
            $uservest = [];
        }

        $result['user'] = $uservest;

        echo json_encode($result);
        exit;
    }
}