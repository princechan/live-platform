<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Api\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Live\IM;
use Common\Lib\Vendor\Easemob;

class ImController extends HomebaseController
{
    private function render(array $value)
    {
        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($value, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 首页数据
     */
    public function getUserData()
    {
        $uid = $this->getUid();
        return $this->render(IM::getInstance()->getUserData($uid));
    }

    /**
     * 请求添加，好友请求
     */
    public function addMsg()
    {
        $touid = $this->getGet('to');
        $msgType = $this->getGet('msgType');
        $remark = $this->getGet('remark');
        $mygroupIdx = $this->getGet('mygroupIdx');

        $uid = $this->getUid();
        $rs = IM::getInstance()->addMsg($uid, $touid, $msgType, $mygroupIdx, $remark);
        return $this->render($rs);
    }

    /**
     * 好友请求已通过
     */
    public function subscribed()
    {
        $touid = $this->getGet('memberIdx');
        $uid = $this->getUid();
        $rs = IM::getInstance()->subscribed($uid, $touid);
        return $this->render($rs);
    }

    /**
     * 记录聊天记录
     */
    public function addChatLog()
    {
        $touid = $this->getGet('to');
        $content = $this->getGet('content');
        $sendTime = $this->getGet('sendTime');
        $type = $this->getGet('type');
        $sysLog = $this->getGet('sysLog');

        $uid = $this->getUid();

        return $this->render(IM::getInstance()->addChatLog($uid, $touid, $content, $sendTime, $type, $sysLog));
    }

    /**
     * 获取聊天总的条数
     */
    public function getChatLogTotal()
    {
        $id = (int)$this->getGet('?id');
        $type = $this->getGet('type');
        $uid = $this->getUid();
        $limit = 20;

        return $this->render(IM::getInstance()->getChatLogTotal($uid, $id, $type, $limit));
    }

    /**
     * 聊天记录
     */
    public function getChatLog()
    {
        $id = (int)$this->getGet('?id');
        $type = $this->getGet('type');
        $page = $this->getGet('page');
        $uid = $this->getUid();
        $limit = 20;
        if ($page <= 1) {
            $page = 1;
        }

        return $this->render(IM::getInstance()->getChatLog($uid, $id, $type, $page, $limit));
    }

    /**
     * 好友或群总的条数
     */
    public function findFriendTotal()
    {
        $type = $this->getGet('type');
        $value = $this->getGet('value');
        $limit = 16;

        return $this->render(IM::getInstance()->findFriendTotal($type, $value, $limit));
    }

    /**
     * 查找好友或群
     */
    public function findFriend()
    {
        $type = $this->getGet('type');
        $value = $this->getGet('value');
        $page = $this->getGet('page');
        $limit = 16;
        if ($page <= 1) {
            $page = 1;
        }

        return $this->render(IM::getInstance()->findFriend($type, $value, $page, $limit));
    }

    /**
     * 删除好友
     */
    public function removeFriends()
    {
        $uid = $this->getUid();
        $friendId = $this->getGet('friend_id');
        return $this->render(IM::getInstance()->removeFriends($uid, $friendId));
    }

    /**
     * 获取好友信息
     */
    public function getOneUserData()
    {
        $uid = $this->getGet('memberIdx');
        return $this->render(IM::getInstance()->getOneUserData($uid));
    }

    public function clearTmpUser()
    {
        $redis = CRedis::getInstance();
        $keys = $redis->keys('temp_*');
        print_r($keys);
        foreach ($keys as $key) {
            $redis->del($key);
        }
        $keys = $redis->keys('pc_token*');
        print_r($keys);
        foreach ($keys as $key) {
            $redis->del($key);
        }
    }

    /**
     * 创建环信用户
     */
    public function createEasemobUser()
    {
        $uid = $this->getPost('uid');
        $token = $this->getPost('token');
        $srv = Easemob::getInstance();
        $rs = $srv->createEasemobUser($uid, $token);
        if (false === $rs) {
            return $this->renderError($srv->getLastErrMsg());
        }

        return $this->renderObject();
    }

}
