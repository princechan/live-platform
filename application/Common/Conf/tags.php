<?php

return [
    'app_init' => ['Common\Behavior\InitHookBehavior'],
    'app_begin' => ['Behavior\CheckLangBehavior'],
    'view_filter' => ['Common\Behavior\TmplStripSpaceBehavior'],
];
