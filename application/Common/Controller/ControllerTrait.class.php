<?php

namespace Common\Controller;

trait ControllerTrait
{
    /**
     * @deprecated
     * @param array $data
     * @param int $code 200-正常，非200-异常
     * @param string $msg
     * @return mixed
     */
    public function renderJson($data = [], $code = 200, $msg = '')
    {
        $value = [
            'code' => (int)$code,
            'message' => "$msg",
            'data' => $data,
        ];

        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($value, JSON_UNESCAPED_UNICODE));
    }

    protected function renderObject($info = [])
    {
        if (empty($info)) {
            $info = (object)[];
        }

        return $this->renderJson($info);
    }

    protected function renderArray($info = [])
    {
        /*
        if (empty($info)) {
            $info = [];
        }
        */

        return $this->renderJson($info);
    }

    protected function renderError($msg, $code = 400)
    {
        return $this->renderJson([], $code, $msg);
    }

}
