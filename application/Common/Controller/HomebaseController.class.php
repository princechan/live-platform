<?php

namespace Common\Controller;

use Common\Lib\Helpers\Func;
use Common\Lib\Auth\User;
use Common\Lib\Live\Live;

class HomebaseController extends AppframeController
{
    protected $user;
    protected $config;
    protected $configpri;

    public function __construct()
    {
        parent::__construct();
        $this->set_action_success_error_tpl();
    }

    /**
     * 后期需要去掉(int)，以兼容游客
     * @deprecated
     * @return int
     */
    protected function getUserId()
    {
        return (int)$this->getUid();
    }

    /**
     * 获取用户ID
     * @return int|string
     */
    protected function getUid()
    {
        return User::getInstance()->getUserId();
    }

    protected function isUserLogin()
    {
        return $this->getUserId() > 0;
    }

    public function _initialize()
    {
        parent::_initialize();
        $uid = $this->getUserId();
        if ($uid > 0) {
            $user = User::getInstance()->getUserPrivateInfo($uid);
            User::getInstance()->delVisitorCookie();
        } else {
            $user = User::getInstance()->setVisitorInfo();
            if (!$user) {
                $this->error('服务异常');
            }
        }
        $this->user = $user;

        //public config
        $config = Func::getPublicConfig();
        $this->config = $config;
        $serverQQ = isset($config['server_qq']) ? $config['server_qq'] : '';
        $serverQQ = explode(',', $serverQQ);
        //private config
        $configpri = Func::getPrivateConfig();
        $this->configpri = $configpri;
        //ext info
        $config['app_key'] = Func::getAppKey();
        $config['favicon_ico'] = Func::getFaviconIco();
        $config['login_name_prefix'] = Func::getLoginNamePrefix();

        $this->assign(get_site_options());
        $this->assign('getConfigName', Func::getPublicConfig());
        $this->assign('config', $config);
        $this->assign('configpri', $configpri);
        $this->assign('site_name', $config['sitename']);
        $this->assign('user', $user);
        $this->assign('userinfo', json_encode($user));
        $this->assign('users_auth', User::getInstance()->getUserAuth($uid));
        $this->assign('sversion', Func::getSetStaticVersion());
        $this->assign('formatLiveCoin', @Live::getInstance()->formatLiveCoin($this->user['livecoin']));
        $this->assign('serverQQ', $serverQQ);
    }

    /**
     * 检查用户登录
     */
    protected function check_login()
    {
        $uid = $this->getUid();
        if ($uid <= 0) {
            $this->error('您还没有登录！', __ROOT__ . "/");
        }
    }

    /**
     * 检查用户状态
     */
    protected function check_user()
    {
        $user_status = M('Users')->where(['id' => $this->getUserId()])->getField("user_status");
        if ($user_status == 2) {
            $this->error('您还没有激活账号，请激活后再使用！', U("user/login/active"));
        }

        if ($user_status == 0) {
            $this->error('此账号已经被禁止使用，请联系管理员！', __ROOT__ . "/");
        }
    }

    /**
     * 发送注册激活邮件
     */
    protected function _send_to_active()
    {
        $option = M('Options')->where(['option_name' => 'member_email_active'])->find();
        if (!$option) {
            $this->error('网站未配置账号激活信息，请联系网站管理员');
        }
        $options = json_decode($option['option_value'], true);
        //邮件标题
        $title = $options['title'];
        $uid = $this->user['id'];
        $username = $this->user['user_login'];

        $activekey = md5($uid . time() . uniqid());
        $users_model = M("Users");

        $result = $users_model->where(["id" => $uid])->save(["user_activation_key" => $activekey]);
        if (!$result) {
            $this->error('激活码生成失败！');
        }
        //生成激活链接
        $url = U('user/register/active', ["hash" => $activekey], "", true);
        //邮件内容
        $template = $options['template'];
        $content = str_replace(['http://#link#', '#username#'], [$url, $username], $template);

        $send_result = sp_send_email($this->user['user_email'], $title, $content);

        if ($send_result['error']) {
            $this->error('激活邮件发送失败，请尝试登录后，手动发送激活邮件！');
        }
    }

    /**
     * 加载模板和页面输出 可以返回输出内容
     * @access public
     * @param string $templateFile 模板文件名
     * @param string $charset 模板输出字符集
     * @param string $contentType 输出类型
     * @param string $content 模板输出内容
     * @return mixed
     */
    public function display($templateFile = '', $charset = '', $contentType = '', $content = '', $prefix = '')
    {
        //echo $this->parseTemplate($templateFile);
        parent::display($this->parseTemplate($templateFile), $charset, $contentType);
    }

    /**
     * 获取输出页面内容
     * 调用内置的模板引擎fetch方法，
     * @access protected
     * @param string $templateFile 指定要调用的模板文件
     * 默认为空 由系统自动定位模板文件
     * @param string $content 模板输出内容
     * @param string $prefix 模板缓存前缀*
     * @return string
     */
    public function fetch($templateFile = '', $content = '', $prefix = '')
    {
        $templateFile = empty($content) ? $this->parseTemplate($templateFile) : '';
        return parent::fetch($templateFile, $content, $prefix);
    }

    /**
     * 自动定位模板文件
     * @access protected
     * @param string $template 模板文件规则
     * @return string
     */
    public function parseTemplate($template = '')
    {

        $tmpl_path = C("SP_TMPL_PATH");
        define("SP_TMPL_PATH", $tmpl_path);
        // 获取当前主题名称
        $theme = C('SP_DEFAULT_THEME');
        if (C('TMPL_DETECT_THEME')) {// 自动侦测模板主题
            $t = C('VAR_TEMPLATE');
            if (isset($_GET[$t])) {
                $theme = $_GET[$t];
            } elseif (cookie('think_template')) {
                $theme = cookie('think_template');
            }
            if (!file_exists($tmpl_path . "/" . $theme)) {
                $theme = C('SP_DEFAULT_THEME');
            }
            cookie('think_template', $theme, 864000);
        }

        $theme_suffix = "";

        if (C('MOBILE_TPL_ENABLED') && sp_is_mobile()) {//开启手机模板支持

            if (C('LANG_SWITCH_ON', null, false)) {
                if (file_exists($tmpl_path . "/" . $theme . "_mobile_" . LANG_SET)) {//优先级最高
                    $theme_suffix = "_mobile_" . LANG_SET;
                } elseif (file_exists($tmpl_path . "/" . $theme . "_mobile")) {
                    $theme_suffix = "_mobile";
                } elseif (file_exists($tmpl_path . "/" . $theme . "_" . LANG_SET)) {
                    $theme_suffix = "_" . LANG_SET;
                }
            } else {
                if (file_exists($tmpl_path . "/" . $theme . "_mobile")) {
                    $theme_suffix = "_mobile";
                }
            }
        } else {
            $lang_suffix = "_" . LANG_SET;
            if (C('LANG_SWITCH_ON', null, false) && file_exists($tmpl_path . "/" . $theme . $lang_suffix)) {
                $theme_suffix = $lang_suffix;
            }
        }

        $theme = $theme . $theme_suffix;

        C('SP_DEFAULT_THEME', $theme);

        $current_tmpl_path = $tmpl_path . $theme . "/";
        // 获取当前主题的模版路径
        define('THEME_PATH', $current_tmpl_path);

        C("TMPL_PARSE_STRING.__TMPL__", __ROOT__ . "/" . $current_tmpl_path);

        C('SP_VIEW_PATH', $tmpl_path);
        C('DEFAULT_THEME', $theme);

        define("SP_CURRENT_THEME", $theme);

        if (is_file($template)) {
            return $template;
        }
        $depr = C('TMPL_FILE_DEPR');
        $template = str_replace(':', $depr, $template);

        // 获取当前模块
        $module = MODULE_NAME;
        if (strpos($template, '@')) { // 跨模块调用模版文件
            list($module, $template) = explode('@', $template);
        }


        // 分析模板文件规则
        if ('' == $template) {
            // 如果模板文件名为空 按照默认规则定位
            $template = "/" . CONTROLLER_NAME . $depr . ACTION_NAME;
        } elseif (false === strpos($template, '/')) {
            $template = "/" . CONTROLLER_NAME . $depr . $template;
        }

        $file = sp_add_template_file_suffix($current_tmpl_path . $module . $template);
        $file = str_replace("//", '/', $file);
        if (!file_exists_case($file)) E(L('_TEMPLATE_NOT_EXIST_') . ':' . $file);
        return $file;
    }

    /**
     * 设置错误，成功跳转界面
     */
    private function set_action_success_error_tpl()
    {
        $theme = C('SP_DEFAULT_THEME');
        if (C('TMPL_DETECT_THEME')) {// 自动侦测模板主题
            if (cookie('think_template')) {
                $theme = cookie('think_template');
            }
        }
        //by ayumi手机提示模板
        $tpl_path = '';
        if (C('MOBILE_TPL_ENABLED') && sp_is_mobile() && file_exists(C("SP_TMPL_PATH") . "/" . $theme . "_mobile")) {//开启手机模板支持
            $theme = $theme . "_mobile";
            $tpl_path = C("SP_TMPL_PATH") . $theme . "/";
        } else {
            $tpl_path = C("SP_TMPL_PATH") . $theme . "/";
        }

        //by ayumi手机提示模板
        $defaultjump = THINK_PATH . 'Tpl/dispatch_jump.tpl';
        $action_success = sp_add_template_file_suffix($tpl_path . C("SP_TMPL_ACTION_SUCCESS"));
        $action_error = sp_add_template_file_suffix($tpl_path . C("SP_TMPL_ACTION_ERROR"));
        if (file_exists_case($action_success)) {
            C("TMPL_ACTION_SUCCESS", $action_success);
        } else {
            C("TMPL_ACTION_SUCCESS", $defaultjump);
        }

        if (file_exists_case($action_error)) {
            C("TMPL_ACTION_ERROR", $action_error);
        } else {
            C("TMPL_ACTION_ERROR", $defaultjump);
        }
    }


}