<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Auth;

use Common\Lib\Helpers\Func;
use Common\Lib\Service;
use Exception;

/**
 * 后台用户
 */
class Admin extends Service
{
    /**
     * 获取当前登录的管事员ID
     * @return int
     */
    public function getId()
    {
        return isset($_SESSION['ADMIN_ID']) ? $_SESSION['ADMIN_ID'] : 0;
    }

    /**
     * 获取管理员列表
     * @return mixed
     */
    public function getAdminUserList()
    {
        $list = M('users')
            ->field(['id', 'user_login', 'user_nicename'])
            ->where(['user_type' => 1])
            ->order(['id' => 'DESC'])
            ->select() ?: [];
        foreach ($list as &$li) {
            $li['avatar'] = Func::getUrl($li['avatar']);
            $li['avatar_thumb'] = Func::getUrl($li['avatar_thumb']);
        }

        return $list;
    }

}
