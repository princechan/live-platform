<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Auth;

use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;
use Common\Lib\Vendor\Easemob;

class User extends Service
{
    const PC_TOKEN = 'pc_token';
    const KEY_CACHE_USER = 'cache:user:';
    const KEY_VISITOR_SENDER = 'vistor:senderid';
    const EXPIRE_TIME = 3600;
    const KEY_CACHE_TIME = 10 * 60;

    /**
     * A6配置
     */
    const A6_PRODUCT_ID = 'A06';
    const A6_PRIVATE_KEY = '#@980live';

    /**
     * 游客信息
     */
    const VISITOR_ID_PREFIX = 'temp_';
    const VISITOR_COOKIE = 'ybzb_tmp_id';
    const VISITOR_NAME_PREFIX = '游客';

    /**
     * 5-麦手，6-主播
     */
    const VEST_TYPE_DJ = 5;
    const VEST_TYPE_ANCHOR = 6;

    public static $quDao = [
        'a01' => '102',
        'xsm' => '301',
    ];

    protected $_config = [];

    /**
     * 检查权限
     * @param $name string|array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param $uid  int           认证用户的id
     * @param $relation string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean           通过验证返回true;失败返回false
     */
    public function check($uid, $name, $relation = 'or')
    {
        if (empty($uid)) {
            return false;
        }
        if ($uid == 1) {
            return true;
        }
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = [$name];
            }
        }

        $list = []; //保存验证通过的规则名
        $role_user_model = M("RoleUser");
        $role_user_join = C('DB_PREFIX') . 'role as b on a.role_id =b.id';
        $groups = $role_user_model->alias("a")->join($role_user_join)
            ->where(["a.user_id" => $uid, "b.status" => 1])->getField("role_id", true) ?: [];

        if (in_array(1, $groups)) {
            return true;
        }

        if (empty($groups)) {
            return false;
        }

        $auth_access_model = M("AuthAccess");
        $join = C('DB_PREFIX') . 'auth_rule as b on a.rule_name =b.name';
        $rules = $auth_access_model
            ->alias("a")
            ->join($join)
            ->where([
                "a.role_id" => ["in", $groups],
                "b.name" => ["in", $name],
            ])->select();

        //下面用到的变量$user
        $user = M('users')->where(['id' => $uid])->find();
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) { //根据condition进行验证
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $list[] = strtolower($rule['name']);
                }
            } else {
                $list[] = strtolower($rule['name']);
            }
        }

        if ($relation == 'or' and !empty($list)) {
            return true;
        }
        $diff = array_diff($name, $list);
        if ($relation == 'and' and empty($diff)) {
            return true;
        }

        return false;
    }

    /**
     * 统一获取用户信息方法
     */
    public function getUserInfo($uid, $update = false)
    {
        if (!$uid) {
            $this->setError('缺参数');
            return false;
        }

        if ($uid < 0) {
            $uid = self::VISITOR_ID_PREFIX . abs($uid);
            return $this->getVisitorInfo($uid);
        }

        if ($this->isVisitor($uid)) {
            return $this->getVisitorInfo($uid);
        }

        $users = $this->getUsersInfo([$uid], $update);
        if (empty($users[$uid])) {
            $this->setError('用户不存在');
            return false;
        }

        return $users[$uid];
    }

    /**
     * 用户信息
     */
    public function getUsersInfo(array $ids, $update = false)
    {
        if (!$ids) {
            return false;
        }

        $redis = CRedis::getInstance();
        $key = $this->getUserCacheKey($ids);
        $users = $redis->get($key);
        if ($users && !$update) {
            return json_decode($users, true);
        }

        $users = M('users')->where(['id' => ['in', $ids]])->select() ?: [];
        foreach ($users as &$li) {
            $li['avatar'] = Func::getUrl($li['avatar']);
            $li['avatar_thumb'] = Func::getUrl($li['avatar_thumb']);
            $li['level'] = $this->getLevel($li['consumption']);
        }

        $users = $users ? Func::index($users, 'id') : [];
        if ($users) {
            $redis->set($key, json_encode($users), self::KEY_CACHE_TIME);
        }

        return $users;
    }

    private function getUserCacheKey($id)
    {
        return self::KEY_CACHE_USER . (is_array($id) ? json_encode($id) : $id);
    }

    /**
     * 用户信息 含有私密信息
     * @param $uid
     * @return mixed
     */
    public function getUserPrivateInfo($uid)
    {
        $info = $this->getUserInfo($uid);
        if ($info) {
            $info['lighttime'] = 0;
            $info['light'] = 0;
            $info['level'] = $this->getLevel($info['consumption']);
            $info['avatar'] = Func::getUrl($info['avatar']);
            $info['avatar_thumb'] = Func::getUrl($info['avatar_thumb']);

            //查询用户是否认证通过
            $isauth = M("users_auth")->where(['uid' => $uid])->find();
            if ($isauth['status'] == 1) {
                $info['isauth'] = 1;
            } else {
                $info['isauth'] = 0;
            }
            //是否是登陆用户
            $info['logged'] = 1;
        }

        return $info;
    }

    private function register($username, $password)
    {
        switch (Func::getAppKey()) {
            case 'a01':
                $srv = UserBttZb::getInstance();
                break;
            case 'a04':
                $srv = UserA04::getInstance();
                break;
            case 'xsm':
                $srv = UserScXsm::getInstance();
                break;
            default:
                $this->setError('app_key not exists');
                return false;
        }

        $rs = $srv->register($username, $password);
        if (false === $rs) {
            $this->setError($srv->getLastErrMsg());
        }

        return $rs;
    }

    public function encryptName($username)
    {
        return md5($username);
    }

    /**
     * 新的注册接口
     * @param $username
     * @param $password
     * @return mixed
     */
    public function newRegister($username, $password)
    {
        $exist = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if ($exist) {
            $this->setError('用户已存在');
            return false;
        }

        $rst = $this->register($username, $password);
        if (false === $rst) {
            return $rst;
        }

        $userId = $this->addUser($username);
        if (!$userId) {
            $this->setError('保存用户失败');
            return false;
        }

        $userInfo = M('users')->where(['id' => $userId])->find();
        $userInfo['level'] = User::getInstance()->getLevel($userInfo['consumption']);
        $this->afterLogin($userInfo);

        return true;
    }

    /**
     * 新增用户
     * @param $username
     * @return mixed
     */
    private function addUser($username)
    {
        $data = [
            'user_login' => $this->encryptName($username),
            'user_email' => '',
            'mobile' => '',
            'user_nicename' => $this->genUserNicename($username),
            'user_pass' => '',
            'signature' => '这家伙很懒，什么都没留下',
            'avatar' => '/default.jpg',
            'avatar_thumb' => '/default_thumb.jpg',
            'create_time' => date("Y-m-d H:i:s"),
            'last_login_time' => date("Y-m-d H:i:s"),
            'user_status' => 1,
            "user_type" => 2, //会员
            "qudao_id" => empty(self::$quDao[Func::getAppKey()]) ?: 102,
            'token' => $this->buildToken($username),
            'expiretime' => time() + 60 * 60 * 24 * 300,
        ];

        return $userId = M('users')->add($data);
    }

    private function buildToken($username)
    {
        return md5(md5($username . microtime(true) . uniqid()));
    }

    public function afterLogin($user)
    {
        $token = $this->buildToken($user['user_login']);
        M('users')->where(['id' => $user['id']])->save([
            'token' => $token,
            'expiretime' => time() + 60 * 60 * 24 * 300,
        ]);
        $user = $this->getUserInfo($user['id'], true);

        return $this->setPcToken($token, $user);
    }

    /**
     * PC获取用户ID方法，后面需要注意游客身份
     * @return int|string
     */
    public function getUserId()
    {
        $user = $this->getPcToken();
        if (!$user) {
            return 0;
        }

        return $user['id'];
    }

    /**
     * 前台个人中心判断是否登录
     * @deprecated
     */
    public function checkLogin()
    {
        $uid = $this->getUserId();
        if ($uid <= 0) {
            $url = Func::getHost();
            header("Location:http://" . $url);
            exit;
        }
    }

    private function setPcToken($token, array $user)
    {
        if (!$token || !$user) {
            return false;
        }

        //cookie
        $cookieTimeout = ($user['vest_id'] <= 1 ? 1 : 24) * self::EXPIRE_TIME;
        cookie('token', $token, $cookieTimeout);
        //session
        $tokenTimeout = $this->isVisitor($user['id']) ? self::EXPIRE_TIME : null;
        return CRedis::getInstance()->set($this->getPcTokenKey(), json_encode($user), $tokenTimeout);
    }

    private function getPcToken()
    {
        $token = $this->getPcTokenKey();
        if (!$token) {
            return false;
        }

        return json_decode(CRedis::getInstance()->get($token), true);
    }

    private function delPcToken()
    {
        return CRedis::getInstance()->del($this->getPcTokenKey());
    }

    public function getToken()
    {
        return cookie('token');
    }

    private function getPcTokenKey()
    {
        $token = $this->getToken();
        if (!$token) {
            return null;
        }

        return self::PC_TOKEN . $token;
    }

    /**
     * 注销
     */
    public function logout()
    {
        session(null);
        cookie(null);
        $this->delPcToken();
    }

    /**
     * 登陆
     * @param $username
     * @param $password
     * @return mixed
     */
    public function login($username, $password)
    {
        switch (Func::getAppKey()) {
            case 'xsm':
                return $this->loginScXsm($username, $password);
                break;
            case 'a04':
                return $this->loginA04($username, $password);
                break;
            case 'a01':
                return $this->loginA01($username, $password);
                break;
            default:
                $this->setError('app_key not exists');
                return false;
        }
    }

    private function loginScXsm($username, $password)
    {
        $user = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if (!$user) {
            $this->setError('用户不存在');
            return false;
        }

        if ($user['user_status'] == 0) {
            $this->setError('该账号已被禁用');
            return false;
        }

        $rs = UserScXsm::getInstance()->login($username, $password);
        if ($rs) {
            $this->afterLogin($user);
        } else {
            $this->setError(UserScXsm::getInstance()->getLastErrMsg());
            return false;
        }

        return $rs;
    }

    private function loginA01($username, $password)
    {
        $srv = UserBttZb::getInstance();
        $rs = $srv->login($username, $password);
        if (!$rs) {
            $this->setError($srv->getLastErrMsg());
            return false;
        }

        $user = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if (!$user) {
            $this->addUser($username);
        }
        $user = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if (!$user) {
            $this->setError('用户不存在');
            return false;
        }

        $this->afterLogin($user);

        return $rs;
    }

    private function loginA04($username, $password)
    {
        $srv = UserA04::getInstance();
        $rs = $srv->login($username, $password);
        if (!$rs) {
            $this->setError($srv->getLastErrMsg());
            return false;
        }

        $user = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if (!$user) {
            $this->addUser($username);
        }
        $user = M('users')->where(['user_login' => $this->encryptName($username), 'user_type' => 2])->find();
        if (!$user) {
            $this->setError('用户不存在');
            return false;
        }

        $this->afterLogin($user);

        return $rs;
    }

    /**
     * @param $username
     * @return boolean false-user_name has already existed, true-user_name does not exist
     */
    public function checkLoginName($username)
    {
        $user = M('users')->where(['user_login' => $this->encryptName($username)])->find();
        if ($user) {
            $this->setError('用户已经存在');
            return false;
        }

        switch (Func::getAppKey()) {
            case 'xsm':
                $srv = UserScXsm::getInstance();
                break;
            case 'a04':
                $srv = UserA04::getInstance();
                break;
            case 'a01':
                $srv = UserBttZb::getInstance();
                break;
            default:
                $this->setError('app_key not exists');
                return false;
        }

        $rs = $srv->checkLoginName($username);

        if (false === $rs) {
            $this->setError($srv->getLastErrMsg());
        }

        return $rs;
    }

    /**
     * visitor info
     * @return mixed|null
     */
    public function getVisitorCookie()
    {
        return cookie(self::VISITOR_COOKIE);
    }

    public function delVisitorCookie()
    {
        return cookie(self::VISITOR_COOKIE, null, -1);
    }

    private function genVisitorId($hash)
    {
        $hash = str_replace([self::VISITOR_NAME_PREFIX, self::VISITOR_ID_PREFIX], '', $hash);
        return self::VISITOR_ID_PREFIX . $hash;
    }

    public function setVisitorInfo()
    {
        $user = $this->getPcToken();
        if ($user) {
            return $user;
        }

        $hash = $this->genVisitorSenderId();
        //log
        file_put_contents('/tmp/visitor-' . Func::getAppKey() . '-' . date('Y-m-d'), json_encode([
                'id' => $hash,
                'date' => date('Y-m-d H:i:s'),
            ]) . PHP_EOL, FILE_APPEND);

        cookie(self::VISITOR_COOKIE, $hash);
        $user = $this->genVisitorInfo($hash);

        return $user;
    }

    private function genVisitorSenderId()
    {
        return CRedis::getInstance()->incr(self::KEY_VISITOR_SENDER);
    }

    private function genVisitorInfo($hash)
    {
        $id = self::VISITOR_ID_PREFIX . $hash;
        $hashName = self::VISITOR_NAME_PREFIX . $hash;
        $token = md5($id . self::A6_PRIVATE_KEY);

        $user = [
            'id' => $id,
            'avatar' => Func::getUrl('', 3),
            'user_nicename' => $hashName,
            'token' => $token,
            'logged' => 0, //是否是登陆用户
            'issuper' => 0,
            'iswhite' => 0,
            'sex' => 0,
            'signature' => '',
            'consumption' => 0,
            'votestotal' => 0,
            'province' => '',
            'city' => '',
            'level' => 0,
            'vest_id' => 0,
        ];

        $this->setPcToken($user['token'], $user);
        $this->setVisitorInfoById($id, $user);

        return $user;
    }

    private function setVisitorInfoById($id, $user)
    {
        return CRedis::getInstance()->set($id, json_encode($user), self::EXPIRE_TIME);
    }

    /**
     * 游客信息
     * @param $id
     * @return mixed
     */
    public function getVisitorInfo($id)
    {
        $id = $this->genVisitorId($id);
        return json_decode(CRedis::getInstance()->get($id), true);
    }

    /**
     * 是否是游客，true-是，false-否
     * @param $uid
     * @return bool
     */
    public function isVisitor($uid)
    {
        return 0 === stripos($uid, self::VISITOR_ID_PREFIX);
    }

    /**
     * 是否拉黑
     * @param $uid
     * @param $touid
     * @return int
     */
    public function isBlack($uid, $touid)
    {
        $black = M('users_black')->where(['uid' => $uid, 'touid' => $touid])->find();
        if ($black) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 用户权限
     * @param $uid
     * @param $liveId
     * @param int $liveType 0-单 1-多
     * @return int 10-游客 30-观众 40-管理员 50-房间主播 60-超管 70-麦手
     */
    public function getUserIdentity($uid, $liveId, $liveType = 0)
    {
        if ($this->isVisitor($uid)) {
            return 10;
        }
        if ($this->isSuper($uid)) {
            return 60;
        }

        if ($liveType == 0) {
            if ($uid == $liveId) {
                return 50;
            }

            $manager = M('users_livemanager')->where(['uid' => $uid, 'liveuid' => $liveId])->find();
            if ($manager) {
                return 40;
            }
        } else {
            $list = M('muti_showmanager')->where(['muti_id' => $liveId])->select() ?: [];
            foreach ($list as $value) {
                if ($value['uid'] == $uid) {
                    if ($value['vest_type'] == self::VEST_TYPE_DJ) {
                        return 70;
                    }
                    if ($value['vest_type'] == self::VEST_TYPE_ANCHOR) {
                        return 50;
                    }
                    break;
                }
            }
        }

        return 30;
    }

    /**
     * 判断账号是否超管
     * @param $uid
     * @return int
     */
    public function isSuper($uid)
    {
        return M('users_super')->where(['uid' => $uid])->find();
    }

    /**
     * 创建easemob临时用户
     * @param $name
     * @return bool
     */
    public function createTmpEasemobUser($name)
    {
        if (!$this->isVisitor($name)) {
            return false;
        }

        if ($name != $this->genVisitorId($this->getVisitorCookie())) {
            return false;
        }

        $model = M('tmp_easemob_user');
        if ($model->where(['name' => $name])->find()) {
            return true;
        }

        return $model->add([
            'name' => $name,
            'created_at' => time(),
        ]);
    }

    /**
     * 删除环信临时用户
     * @param $name
     * @return bool
     */
    public function delTmpEasemobUser($name)
    {
        if (!$this->isVisitor($name)) {
            return false;
        }

        $rs = Easemob::getInstance()->deleteUser($name);
        if (empty($rs['error'])) {
            M('tmp_easemob_user')->where(['name' => $name])->delete();
        }
        print_r($rs);

        return true;
    }

    /**
     * 删除2天前的环信用户数据
     */
    public function delTmpEasemobUsers()
    {
        $time = time() - 2 * self::EXPIRE_TIME;
        $users = M('tmp_easemob_user')->where(['created_at' => ['LT', $time]])
            ->limit(0, 10000)->order(['id' => 'ASC'])->select() ?: [];
        foreach ($users as $user) {
            $this->delTmpEasemobUser($user['name']);
        }
    }

    /**
     * 认证信息
     * @param $uid
     * @return int|mixed
     */
    public function getUserAuth($uid)
    {
        $auth = M('users_auth')->where(['uid' => $uid])->find();
        return $auth ?: 0;
    }

    /**
     * 判断该用户是否已经认证
     * @param $uid
     * @return int
     */
    public function getAuthStatus($uid)
    {
        $auth = $this->getUserAuth($uid);
        return $auth ? $auth['status'] : 3;
    }

    /**
     * 密码检查
     * @param $user_pass
     * @return int
     */
    function passCheck($user_pass)
    {
        $num = preg_match("/^[a-zA-Z]+$/", $user_pass);
        $word = preg_match("/^[0-9]+$/", $user_pass);
        $check = preg_match("/^[a-zA-Z0-9]{6,12}$/", $user_pass);
        if ($num || $word) {
            return 2;
        } else if (!$check) {
            return 0;
        }

        return 1;
    }

    /**
     * 等级区间限额
     * @param $level
     * @return mixed
     */
    public function getLevelSection($level)
    {
        $key = 'experlevel_limit';
        $redis = CRedis::getInstance();
        $list = json_decode($redis->get($key), true) ?: [];
        if (!$list) {
            $list = M('experlevel_limit')->field('withdraw,level_up')->order('level_up asc')->select() ?: [];
            $redis->set($key, json_encode($list), 60);
        }

        $withdraw = 0;
        foreach ($list as $v) {
            if ($v['level_up'] >= $level) {
                $withdraw = $v['withdraw'];
                break;
            }
        }

        return $withdraw;
    }

    /**
     * 修改用户资料
     * @param $uid
     * @param $sex
     * @param $birthday
     * @param $userNicename
     * @param $signature
     * @return bool
     */
    public function editModify($uid, $sex, $birthday, $userNicename, $signature)
    {
        if (!$sex && !$birthday && !$userNicename && !$signature) {
            $this->setError('参数错误');
            return false;
        }

        if ($userNicename) {
            if (M('users')->where(['user_nicename' => $userNicename])->find()) {
                $this->setError('用户名重复');
                return false;
            }
        }

        $data = [];
        $data['id'] = $uid;
        if ($sex) {
            $data['sex'] = $sex;
        }
        if ($birthday) {
            $data['birthday'] = $birthday;
        }
        $userNicename = Func::filterWord($userNicename);
        if ($userNicename) {
            $data['user_nicename'] = $userNicename;
        }
        $signature = Func::filterWord($signature);
        if ($signature) {
            $data['signature'] = $signature;
        }

        $result = M('users')->save($data);
        if ($result) {
            if ($userNicename) {
                if (M('users_live')->where(['uid' => $uid])->find()) {
                    M('users_live')->where(['uid' => $uid])->setField('user_nicename', $userNicename);
                }
            }
            return true;
        }

        $this->setError('修改失败');
        return false;
    }

    /**
     * 生成nicename
     * @param $username
     * @return string
     */
    public function genUserNicename($username)
    {
        $nick = '***' . substr($username, 3);
        if (!M('users')->where(['user_nicename' => $nick])->find()) {
            return $nick;
        }

        for ($i = 1; $i < 1000; $i++) {
            $nick .= $this->getRandChar();
            if (!M('users')->where(['user_nicename' => $nick])->find()) {
                return $nick;
            }
            $nick .= date('mdHi');
        }

        return $nick;
    }

    private function getRandChar($num = 6)
    {
        $str = '';
        for ($i = 1; $i <= $num; $i++) {
            $str .= chr(rand(97, 122));
        }

        return $str;
    }

    /**
     * 判断是否关注
     * @param $uid
     * @param $touid
     * @return int
     */
    public function isAttention($uid, $touid)
    {
        $data = M('users_attention')->where(['uid' => $uid, 'touid' => $touid])->find();
        return $data ? 1 : 0;
    }

    /**
     * 关注人数
     * @param $uid
     * @return mixed
     */
    public function getFollownums($uid)
    {
        return M('users_attention')->where(['uid' => $uid])->count();
    }

    /**
     * 粉丝人数
     * @param $uid
     * @return mixed
     */
    public function getFansnums($uid)
    {
        return M('users_attention')->where(['touid' => $uid])->count();
    }

    /**
     * @param $uid
     * @return mixed
     */
    public function getUserToken($uid)
    {
        $data = M('users')->field('token')->where(['id' => $uid])->find();
        return $data ? $data['token'] : '';
    }

    /**
     * 判断token是否过期
     * @deprecated
     * @param $uid
     * @param $token
     * @return int
     */
    public function checkToken($uid, $token)
    {
        $userinfo = M('users')->field('token,expiretime')->where(['id' => $uid, 'user_type' => 2])->find();
        if ($userinfo['token'] != $token || $userinfo['expiretime'] < time()) {
            return 700;
        }

        return 0;
    }

    /**
     * 获取等级
     * @param $experience
     * @return int
     */
    public function getLevel($experience)
    {
        $levelId = 1;
        $levelList = M('experlevel')->field('levelid,level_up')->order("level_up asc")->select() ?: [];
        if (!$levelList) {
            return $levelId;
        }

        $max = $levelList[count($levelList) - 1];
        if ($experience >= $max['level_up']) {
            $levelId = $max['levelid'];
        } else {
            foreach ($levelList as $k => $v) {
                if ($v['level_up'] >= $experience) {
                    $levelId = $v['levelid'];
                    break;
                }
            }
        }

        return $levelId;
    }

    /**
     * 判断账号是被禁用
     * @param $uid
     * @return int 0-禁用，1-未禁用
     */
    public function isBan($uid)
    {
        if ($this->isVisitor($uid)) {
            return 1;
        }

        $status = M('users')->field('user_status')->where(['id' => $uid])->find();
        if (!$status || $status['user_status'] == 0) {
            return 0;
        }

        return 1;
    }

    /**
     * 获取指定长度的随机字符串
     * @param int $length
     * @param int $numeric
     * @return string
     */
    public function random($length = 6, $numeric = 0)
    {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if ($numeric) {
            $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max = strlen($chars) - 1;
            for ($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }

        return $hash;
    }

    /**
     * @deprecated
     * @param $pass
     * @return string
     */
    public function setPass($pass)
    {
        $code = 'rCt52pF2cnnKNB3Hkp';
        return '###' . md5(md5($code . $pass));
    }

    /**
     * 获取用户实际ID，注册用户正整数，游客负整数
     * @param $uid
     * @return string
     */
    public function getRUid($uid)
    {
        return $this->isVisitor($uid) ? -(int)str_replace(self::VISITOR_ID_PREFIX, '', $uid) : (int)$uid;
    }

}
