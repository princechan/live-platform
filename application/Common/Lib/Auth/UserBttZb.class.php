<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Auth;

use Common\Lib\Service;
use Common\Lib\Helpers\Curl;

class UserBttZb extends Service
{
    public static function getHost()
    {
        if (isset($_SERVER['SERVER_ADDR'])) {
            switch ($_SERVER['SERVER_ADDR']) {
                case '10.71.42.70':
                    return 'http://php.a01webapi.com';//dev
                    break;
                case '45.199.97.115':
                    return 'http://a01webapi.fastgoapi.com';//prod
                    return 'http://webapi.918rr.com'; //运测环境
                    break;
            }
        }

        return 'http://a01webapi.fastgoapi.com';//prod
    }

    public function login($username, $password)
    {
//        if (ENV_DEV) {
//            return true;
//        }

        $url = self::getHost() . '/api/livestream/login';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['token'] = $this->buildToken($username, $timestamp);
        $data['login_name'] = $username;
        $data['password'] = $password;

        $send_request_time = date('Y-m-d H:i:s');
        $start = microtime(true);
        $rs = Curl::getInstance()->post($url, $data);
        $end = microtime(true);
        $duration = round($end - $start, 5);
        $this->recordInfo($send_request_time, $url, $data, $rs, $duration);

        if (isset($rs['status']) && $rs['status'] == 200) {
            return true;
        }

        $this->setError($rs['msg']);
        return false;
    }

    public function register($username, $password)
    {
//        if (ENV_DEV) {
//            return true;
//        }

        $url = self::getHost() . '/api/livestream/register';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['token'] = $this->buildToken($username, $timestamp);
        $data['login_name'] = $username;
        $data['password'] = $password;
        $data['ip_address'] = $_SERVER['REMOTE_ADDR'] ?: '0.0.0.0';
        $data['domain_name'] = 'www.918aav.com';
        $send_request_time = date('Y-m-d H:i:s');
        $start = microtime(true);
        $rs = Curl::getInstance()->post($url, $data);
        $end = microtime(true);
        $duration = round($end - $start, 5);
        $this->recordInfo($send_request_time, $url, $data, $rs, $duration);

        if (isset($rs['status']) && $rs['status'] == 200) {
            return true;
        }

        $this->setError($rs['msg']);
        return false;
    }

    public function checkLoginName($username)
    {
//        if (ENV_DEV) {
        // return true;
//        }

        $url = self::getHost() . '/api/livestream/checkLoginName';
        $timestamp = time();
        $data['timestamp'] = $timestamp;
        $data['token'] = $this->buildToken($username, $timestamp);
        $data['login_name'] = $username;
        $send_request_time = date('Y-m-d H:i:s');
        $start = microtime(true);
        $rs = Curl::getInstance()->post($url, $data);
        $end = microtime(true);
        $duration = round($end - $start, 5);
        $this->recordInfo($send_request_time, $url, $data, $rs, $duration);

        if (isset($rs['status'])) {
            if ($rs['status'] == 200) {
                return true;
            } else {
                $this->setError($rs['msg']);
                return false;
            }
        }

        $this->setError('failed to connect');
        return false;
    }

    private function buildToken($username, $timestamp)
    {
        return md5('A01' . $username . $timestamp . '#@980live');
    }

    /**
     * 预约限制
     * @param $data
     * @return bool
     */
    public function checkJoinRequirements($data)
    {
        return true;
    }


    /**
     * 记录博天堂接口请求日志
     *
     * @param  string $time [发起请求时间]
     * @param  string $url [请求地址]
     * @param  string $data [请求数据]
     * @param  string $response [相应内容]
     * @param  string $duration [耗时]
     * @return [type]           [description]
     */
    private function recordInfo($time = '', $url = '', $data = '', $response = '', $duration = '')
    {
        $json = [
            'send_request' => $time,
            'request' => [
                'url' => $url,
                'data' => $data,
            ],
            'response' => $response,
            'duration' => $duration,
        ];

        $message = json_encode($json);
        return \Think\Log::write($message, 'INFO');
    }

}
