<?php

namespace Common\Lib;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\Log;

class LiveVideo extends Service
{
    public function add()
    {

    }

    public function save()
    {

    }

    public function getList()
    {

    }

    public function getLanXunVideo($op, $url, $createAt)
    {
        Log::getInstance()->logFile(__FUNCTION__, json_encode($_GET) . PHP_EOL);
        if ($op == 'uploadsuccess') {
            $createdAt = strtotime($createAt);
            $urlArr = explode('/', $url);
            $stream = $urlArr[4];
            $streamArr = explode('_', $stream);
            $uid = $streamArr[0];
            $updatedAt = $streamArr[1];
            $user = User::getInstance()->getUserInfo($uid);
            M('live_video')->add([
                'uid' => $uid,
                'user_nicename' => $user['user_nicename'],
                'stream' => $stream,
                'url' => $url,
                'created_at' => $createdAt,
                'updated_at' => $updatedAt,
            ]);
        }
    }

}
