<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib;

use Exception;

/**
 * Lib下的service都需要extend该类
 * Class Service
 * @package Common\Lib
 */
class Service
{
    private $errCode = 0;
    private $errMsg = '';
    protected static $instance = [];

    /**
     * @return static|mixed
     */
    public static function getInstance()
    {
        $class = get_called_class();
        $key = serialize(func_get_args()) . $class;
        if (empty(self::$instance[$key])) {
            self::$instance[$key] = new $class;
        }
        return self::$instance[$key];
    }

    protected function __construct()
    {
    }

    public function setError($error = '', $errCode = 400)
    {
        $this->errMsg = $error;
        $this->errCode = $errCode;
    }

    public function getLastErrCode()
    {
        return (int)$this->errCode;
    }

    public function getLastErrMsg()
    {
        return $this->errMsg;   //?: ($this->errCode ? LibFunc::getText($this->errCode) : '');
    }

    public function getLastErrorInfo()
    {
        return ['errcode' => $this->errCode, 'errmsg' => $this->errMsg,];
    }

    /**
     * @throws Exception
     */
    public function throwException()
    {
        throw new Exception($this->errMsg, $this->errCode);
    }

}
