<?php
/**
 * 题单业务逻辑类
 */

namespace Common\Lib\Activity;

use Common\Lib\Auth\Admin;
use Common\Lib\Service;


class Question extends Service
{

    public function __construct()
    {
    }


    /**
     * 获取题目列表
     * @param $arr
     * @return mixed
     */
    public function quesGetList($arr)
    {

        $map = [];
        if ($arr) {
            $map['type'] = $arr['type'];
        }

        $field = [
            'id',
            'name',
            'choose_answer',
        ];

        $ques_model = M('question');
        $count = $ques_model->where($map)->count();
        $page = $this->page($count, 20);
        $ret_arr = $ques_model->field($field)->where($map)->order("id DESC")->limit($page->firstRow . ',' . $page->listRows)->select();

        if ($ret_arr) {
            foreach ($ret_arr as &$v) {
                $v['choose_answer'] = json_decode($v['choose_answer'], JSON_UNESCAPED_UNICODE);
            }
        }

        return [
            'lists' => $ret_arr,
            'page' => $page->show("Admin"),
        ];
    }

    /**
     * 后台分页
     */
    protected function page($total_size = 1, $page_size = 0, $current_page = 1, $listRows = 6, $pageParam = '', $pageLink = '', $static = FALSE)
    {
        if ($page_size == 0) {
            $page_size = C("PAGE_LISTROWS");
        }

        if (empty($pageParam)) {
            $pageParam = C("VAR_PAGE");
        }

        $Page = new \Page($total_size, $page_size, $current_page, $listRows, $pageParam, $pageLink, $static);
        $Page->SetPager('Admin', '{first}{prev}&nbsp;{liststart}{list}{listend}&nbsp;{next}{last}', ["listlong" => "9", "first" => "首页", "last" => "尾页", "prev" => "上一页", "next" => "下一页", "list" => "*", "disabledclass" => ""]);
        return $Page;
    }

    /**
     * 添加题目
     * @param $arr
     * @return bool
     */
    public function addQuestion($arr)
    {
        return M('question')->add($arr);
    }

    /**
     * 获取当个题目数据
     * @param $id
     * @return bool
     */
    public function getQuestionInfo($id)
    {

        $field = [
            'id',
            'type',
            'name',
            'choose_answer',
            'right_answer',
        ];
        $arr = M('question')->field($field)->where('id=' . $id)->find();

        if ($arr) {
            $arr['choose_answer'] = json_decode($arr['choose_answer'], JSON_UNESCAPED_UNICODE);
        }
        return $arr;
    }

    /**
     * 更新题目
     * @param $id
     * @param $arr
     * @return bool
     */
    public function updateQuestion($id, $arr)
    {
        return M('question')->where('id=' . $id)->save($arr);
    }

    /**
     * 删除题目
     * @param $id
     * @return mixed
     */
    public function delQuestion($id)
    {
        return M('question')->where('id=' . $id)->delete();
    }

    /**
     * 获取所有题单列表Ajax
     * @return array
     */
    public function quesListManagerAjax($page)
    {
        $size = 20;
        $field = [
            'id',
            'name',
        ];

        $ques_list_model = M('question_list');
        $count = $ques_list_model->count();
        $prev_page = 0;
        $current_page = $page;
        $next_page = intval($page) + 1;
        if ($current_page < 1) {
            $current_page = 1;
            $next_page = intval($current_page) + 1;
        }

        $total_page = ceil($count / $size);

        if ($current_page >= $total_page) {
            $current_page = $next_page = $total_page;
        }
        $prev_page = intval($current_page) - 1;

        $limit = $size * ($current_page - 1);
        $arr = $ques_list_model->field($field)->order("id DESC")->limit($limit . ',' . $size)->select();

        return [
            'lists' => $arr,
            'page' => [
                'prev_page' => $prev_page,
                'current_page' => $current_page,
                'next_page' => $next_page,
                'total_page' => $total_page,
            ],
        ];
    }


    /**
     * 获取所有题单列表
     * @return array
     */
    public function quesListManager()
    {

        $field = [
            'id',
            'name',
            'question_ids',
            'admin_id',
            'created_at',
        ];

        $ques_list_model = M('question_list');
        $count = $ques_list_model->count();
        $page = $this->page($count, 20);
        $arr = $ques_list_model->field($field)->order("id DESC")->limit($page->firstRow . ',' . $page->listRows)->select();

        if ($arr) {
            $admin_ids = [];
            foreach ($arr as $v) {
                $admin_ids[] = $v['admin_id'];
            }

            if ($admin_ids) {
                $admin_strs = implode(',', array_unique($admin_ids));
                $admin_user_arr = M('users')->field('id, user_login')->where('user_type=1 AND id in(' . $admin_strs . ')')->select();

                $admin_users = [];
                if ($admin_user_arr) {
                    foreach ($admin_user_arr as $v) {
                        $admin_users[$v['id']] = $v['user_login'];
                    }
                    foreach ($arr as &$v) {
                        if (isset($admin_users[$v['admin_id']])) {
                            $v['admin_user_name'] = $admin_users[$v['admin_id']];
                        }
                    }
                }
            }
        }

        return [
            'lists' => $arr,
            'page' => $page->show("Admin"),
        ];
    }


    /**
     * 添加已有题单ajax
     * @param $arr
     * @return mixed
     */
    public function addExistQuesListAjax($ques_list_arr, $ques_str)
    {

        if ((!is_array($ques_list_arr)) || (!is_string($ques_str))) return false;
        $ques_list_str = implode(',', $ques_list_arr);

        $ques_list_ret = M('question_list')->field(['id', 'question_ids'])->where('id IN(' . $ques_list_str . ')')->select();

        if ($ques_list_ret) {
            foreach ($ques_list_ret as &$v) {
                $v['question_ids'] = implode(',', array_unique(array_filter(explode(',', $v['question_ids'] . ',' . $ques_str))));
            }

            $ques_list_model = M('question_list');
            foreach ($ques_list_ret as $val) {
                $ques_list_model->where('id=' . $val['id'])->setField('question_ids', $val['question_ids']);
            }
        }
        return true;
    }

    /**
     * 添加题单
     * @param $arr
     * @return mixed
     */
    public function addQuesList($arr)
    {
        return M('question_list')->add($arr);
    }

    /**
     * 获取单条题单信息
     * @param $id
     * @return mixed
     */
    public function getQuesListInfo($id)
    {
        $field = [
            'id',
            'name',
            'question_ids',
        ];
        $arr = M('question_list')->field($field)->where('id=' . $id)->find();

        if ($arr) {
            $arr['question_arr'] = [];
            if ($arr['question_ids']) {
                $arr['question_arr'] = M('question')->field("id, name")->where("id in(" . $arr['question_ids'] . ")")->select();
            }
            unset($arr['question_ids']);
        }
        return $arr;
    }

    /**
     * 修改题单
     * @param $id
     * @param $arr
     */
    public function updateQuesList($id, $arr)
    {
        return M('question_list')->where(['id' => $id])->save($arr);
    }

    /**
     * 删除题单
     * @param $id
     * @return mixed
     */
    public function delQuesList($id)
    {
        //判断活动数据表是否有关联
        $arr = M('activity_answer')->field('id')->where('question_list_id=' . $id)->find();
        if ($arr) {
            $this->setError(400, '该提单已关联活动');
            return false;
        }

        return M('question_list')->where(['id' => $id])->delete();
    }

    /**
     * 答题Excel导入模板导入Mysql
     */
    public function excelImport($file)
    {

        if(empty($file)){
            $this->setError('未上传');
            return false;
        }


        $savepath = date('Ymd') . '/';
        //上传处理类
        $config = [
            'rootPath' => './' . C("UPLOADPATH"),
            'savePath' => $savepath,
            'maxSize' => 11048576,
            'saveName' => ['uniqid', ''],
            'exts' => ['xls', 'xlsx'],
            'autoSub' => false,
        ];
        $upload = new \Think\Upload($config);//
        $info = $upload->upload();

        if(!$info){
            $this->setError($upload->getError());
            return false;
        }

        //上传成功
        //写入附件数据库信息
        $first = array_shift($info);
        if (!empty($first['url'])) {
            $url = $first['url'];

        } else {
            $url = C("TMPL_PARSE_STRING.__UPLOAD__") . $savepath . $first['savename'];
        }

        Vendor("PHPExcel.PHPExcel.IOFactory");

        if (substr($url, -5) == ".xlsx") {
            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');

        } else {
            $objReader = \PHPExcel_IOFactory::createReader('Excel5');

        }

        //$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
        $objPHPExcel = $objReader->load('./' . C("UPLOADPATH") . $savepath . $first['savename']); //$filename可以是上传的文件，或者是指定的文件

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow(); // 取得总行数

        $data = [];
        $time = time();

        //循环读取excel文件,读取一条,插入一条
        for ($num = 2, $isn=0; $num <= $highestRow; $num++) {
            $question_name = trim($objPHPExcel->getActiveSheet()->getCell("A" . $num)->getValue());//获取A列的值:题目名称

            $model = M("question");
            $is_exist = $model->where("name='$question_name'")->find();
            if($is_exist){
                $isn ++;
                continue;
            }

            $right_answer  = trim($objPHPExcel->getActiveSheet()->getCell("B" . $num)->getValue());//获取B列的值：正确答案
            $answer_1      = trim($objPHPExcel->getActiveSheet()->getCell("C" . $num)->getValue());//获取C列的值：答案1
            $answer_2      = trim($objPHPExcel->getActiveSheet()->getCell("D" . $num)->getValue());//获取D列的值：答案2
            $answer_3      = trim($objPHPExcel->getActiveSheet()->getCell("E" . $num)->getValue());//获取E列的值：答案3
            $answer_4      = trim($objPHPExcel->getActiveSheet()->getCell("F" . $num)->getValue());//获取F列的值：答案4

            $chooser_answer = json_encode([$answer_1, $answer_2, $answer_3, $answer_4], JSON_UNESCAPED_UNICODE);
            $type           = $answer_4 ? 2 : 1;
            $time           = time();

            $data[] = [
                'name'           => $question_name,
                'type'           => $type,
                'choose_answer'  => $chooser_answer,
                'right_answer'   => $right_answer,
                'admin_id'       => Admin::getInstance()->getId(),
                'created_at'     => $time,
                'updated_at'     => $time,
            ];
        }

        $result = $model->addAll($data);

        if(!$result){
            $this->setError("导入失败！");
            return false;
        }

        return "导入成功！有{$isn}条重复信息未导入";
    }

}