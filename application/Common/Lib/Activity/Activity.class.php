<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Activity;

use Common\Lib\Auth\User;
use Common\Lib\Auth\UserBttZb;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;

class Activity extends Service
{
    const KEY_ACTIVITY = 'cache:activity:';
    const KEY_ENROLL = 'activity:{id}:enroll';
    const KEY_OUT = 'activity:{id}:out';
    const KEY_DYNAMIC_DATA = 'activity:{id}:dynamic_data';
    const KEY_PLAYER_DATA = 'activity:{id}:player_data{uid}';
    const KEY_PRIZE_RANK = 'activity:{id}:prize_rank';
    const KEY_TOTAL_PRIZE_RANK = 'activity:total_prize_rank';

    //当前活动题单问题集合
    const KEY_ACTIVITY_QUESTION = 'activity:{id}:question_list';
    //记录玩家答案归类总数
    const KEY_NUM_OF_ANSWER = 'activity:{id}:{current_num}:num_of_answer';
    //记录玩家日志答题日志
    const KEY_PLAYER_LOG = 'activity:{id}:{current_num}:player_log';
    //记录被过滤的玩家日志
    const KEY_PLAYER_FILTER_LOG = 'activity:{id}:{current_num}:player_log_filter';
    //正确答案提交者集合
    const KEY_RIGHT_ANSWER = 'activity:{id}:right_answer_user';
    //上道题正确答案提交者集合
    const KEY_LAST_RIGHT_ANSWER = 'activity:{id}:right_answer_user_temp';
    //记录活动每个玩家的答题记录
    const KEY_PLAYER_DETAIL_LOG_PATTERN = 'activity:player_detail_log:*';
    const KEY_PLAYER_DETAIL_LOG = 'activity:player_detail_log:{id}:{uid}:{current_num}';
    //玩家总奖金日志
    const KEY_PLAYER_TOTAL_LOG_PATTERN = 'activity:player_total_log:*';
    const KEY_PLAYER_TOTAL_LOG = 'activity:player_total_log:{id}:{uid}';


    //设置玩家hashKey
    public static $playDataHashKey = [
        'each_consuming_time' => 'each_consuming_time',     //每道题答题时间
        'total_consuming_time' => 'total_consuming_time',   //答题总时间
        'answer_log' => 'answer_log',                       //回答过的问题ID集合
        'current_status' => 'current_status',               //当前玩家状态
        'kaishi_token' => 'kaishi_token',                   //限制条件时绑定凯时平台用户token
        'right_answer' => 'right_answer',                   //回答正确答案日志
    ];

    //活动状态
    public static $statusKeyMap = [
        'unpublished' => 0,   //'活动未发布',
        'published' => 1,   //'活动发布',
        'ready' => 2,   //'活动准备',
        'going' => 3,   //'活动进行中',
        'finished' => 4,   //'活动结束',
        'deleted' => 5,   //'活动删除',
    ];

    //活动状态
    public static $statusMap = [
        0 => '活动未发布',
        1 => '活动发布',
        2 => '活动准备',
        3 => '活动进行中',
        4 => '活动结束',
        5 => '活动删除',
    ];

    public static $joinRequirements = [
        //流水限制
        'cash_flow_limit_type' => 1,
        'cash_flow_limit_time' => '', //流水截止时间
        'cash_flow_limit_quota' => 0,//流水额度
        //充值限制
        'recharge_limit_type' => 1,
        'recharge_limit_time' => '',  //充值截止时间
        'recharge_limit_quota' => 0, //充值额度
    ];

    public static $ActivityDynamicDataStatusMap = [
        'begin' => 1,  //未公布结果
        'finished' => -1, //已公布结果
    ];

    public function delActivity($id)
    {
        if (!$id) {
            $this->setError('缺参数id');
            return false;
        }

        $rs = M('activity_answer')->where(['id' => $id])->save([
            'status' => 5,
            'updated_at' => time(),
        ]);
        if (false === $rs) {
            $this->setError('删除失败');
            return false;
        }

        CRedis::getInstance()->del($this->getActivityKey($id));

        return $rs;
    }

    /**
     * 获取活动
     */
    public function getActivity($id, $updateCache = false)
    {
        if (!$id) {
            $this->setError('缺参数id');
            return false;
        }

        $rs = $updateCache ? null : CRedis::getInstance()->get($this->getActivityKey($id));
        if ($rs) {
            return json_decode($rs, true);
        } else {
            $rs = M('activity_answer')->where(['id' => $id])->find();
            if (!$rs) {
                $this->setError('活动不存在');
                return false;
            }

            $rs['icon_image'] = $rs['icon'] ? Func::getUrl($rs['icon']) : '';
            $rs['join_requirements'] = array_merge(self::$joinRequirements, json_decode($rs['join_requirements'], true) ?: []);
            CRedis::getInstance()->set($this->getActivityKey($id), json_encode($rs));
            return $rs;
        }
    }

    public function getActivityBaseInfo($activityId)
    {
        $activity = $this->getActivity($activityId);
        if (!$activity) {
            return [];
        }

        return [
            'id' => $activity['id'],
            'name' => $activity['name'],
            'start_at' => $activity['start_at'],
            'extra_bonus_type' => $activity['extra_bonus_type'],
            'base_bonus_amount' => (int)$activity['base_bonus_amount'],
            'desct' => $activity['desct'],
            'status' => $activity['status'],
            'icon' => $activity['icon_image'],
            'answer_countdown_time' => $activity['answer_countdown_at'],
            'enroll_num' => $this->sCardLastRightAnswerNum($activityId),
            'server_time' => time(),
        ];
    }

    /**
     * 保存活动
     */
    public function saveActivity($data)
    {
        if (!$data) {
            $this->setError('数据为空');
            return false;
        }

        $model = M('activity_answer');
        $time = time();
        $data['updated_at'] = $time;
        if (empty($data['id'])) {
            $data['created_at'] = $time;
            $id = $rs = $model->add($data);
        } else {
            $rs = $model->where(['id' => $data['id']])->save($data);
            $id = $data['id'];
        }

        if ($rs) {
            $this->getActivity($id, true);
        }

        return $rs;
    }

    private function getActivityKey($id)
    {
        return self::KEY_ACTIVITY . $id;
    }

    public function getCount($where = [])
    {
        return M('activity_answer')->where($where)->count();
    }

    public function getAdminList($where = [], $offset = 0, $limit = 20)
    {
        $list = M('activity_answer')->where($where)->order('id DESC')->limit($offset, $limit)->select() ?: [];
        if (!$list) {
            return [];
        }

        $adminIds = array_column($list, 'admin_id');
        $adminList = M('users')->field('id,user_nicename,user_login')->where(['id' => ['IN', $adminIds]])->select() ?: [];
        $adminList = Func::index($adminList, 'id');
        $questionListIds = array_column($list, 'question_list_id');
        $questionList = M('question_list')->field('id,name')->where(['id' => ['IN', $questionListIds]])->select() ?: [];
        $questionList = Func::index($questionList, 'id');
        foreach ($list as &$li) {
            $li['created_at_f'] = date('Y/m/d', $li['created_at']);
            $li['start_at_f'] = date('Y年m月d日 H:i', $li['start_at']);
            $li['status_f'] = isset(self::$statusMap[$li['status']]) ? self::$statusMap[$li['status']] : '-';
            $li['user_nicename'] = '';
            $li['question_list_name'] = '';
            if (!empty($adminList[$li['admin_id']])) {
                $li['user_nicename'] = $adminList[$li['admin_id']]['user_nicename'] ?: $adminList[$li['admin_id']]['user_login'];
            }
            if (!empty($questionList[$li['question_list_id']])) {
                $li['question_list_name'] = $questionList[$li['question_list_id']]['name'];
            }
        }

        return $list;
    }

    /**
     * 题单列表
     * @return mixed
     */
    public function getQuestionList()
    {
        return M('question_list')->order('id DESC')->select();
    }

    /**
     * 报名
     * @param $uid
     * @param $activityId
     * @return bool
     */
    public function enroll($uid, $activityId)
    {
        if (!$uid) {
            $this->setError('未登录，不允许参加活动');
            return false;
        }
        if (!$activityId) {
            $this->setError('活动ID不存在');
            return false;
        }

        $activity = $this->getActivity($activityId);
        if (!$activity) {
            $this->setError('活动不存在');
            return false;
        }

        if (!in_array($activity['status'], [self::$statusKeyMap['published'], self::$statusKeyMap['ready']])) {
            $this->setError('非法请求, 活动非报名状态');
            return false;
        }

        if ((isset($activity['join_requirements']['cash_flow_limit_type'])
                && (self::$joinRequirements['cash_flow_limit_type'] == isset($activity['join_requirements']['cash_flow_limit_type'])))
            || (isset($activity['join_requirements']['cash_flow_limit_type'])
                && (self::$joinRequirements['cash_flow_limit_type'] == isset($activity['join_requirements']['cash_flow_limit_type'])))) {

            //获取用户名
            $user_info = User::getInstance()->getUserInfo($uid);

            if (!empty($activity['join_requirements']['cash_flow_limit_begintime'])
                && !empty($activity['join_requirements']['cash_flow_limit_endtime'])
                && !empty($activity['join_requirements']['cash_flow_limit_quota'])) {

                //检测用户是否符合限制条件API
                $map1 = [
                    'login_name' => $user_info['user_login'],
                    'flow_limit' => [
                        'begindate' => $activity['join_requirements']['cash_flow_limit_begintime'],
                        'enddate' => $activity['join_requirements']['cash_flow_limit_endtime'],
                        'amount' => $activity['join_requirements']['cash_flow_limit_quota'],
                    ]
                ];

                if (false == UserBttZb::getInstance()->checkJoinRequirements($map1)) {
                    $this->setError('流水额度不足，请充值');
                    return false;
                }
            }

            if (!empty($activity['join_requirements']['recharge_limit_begintime'])
                && !empty($activity['join_requirements']['recharge_limit_endtime'])
                && !empty($activity['join_requirements']['recharge_limit_quota'])) {

                //检测用户是否符合限制条件API
                $map2 = [
                    'login_name' => $user_info['user_login'],
                    'recharge_limit' => [
                        'begindate' => $activity['join_requirements']['recharge_limit_begintime'],
                        'enddate' => $activity['join_requirements']['recharge_limit_endtime'],
                        'amount' => $activity['join_requirements']['recharge_limit_quota'],
                    ]
                ];
                if (false == UserBttZb::getInstance()->checkJoinRequirements($map2)) {
                    $this->setError('充值额度不足，请充值');
                    return false;
                }
            }
        }

        $this->zAddEnroll($activityId, $uid);

        //添加到提交上次正确答案集合
        $this->sAddLastRightAnswer($activityId, $uid);
        return true;
    }

    private function getEnrollKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_ENROLL);
    }

    /**
     * 添加到报名列表
     */
    public function zAddEnroll($activity_id, $uid)
    {
        return CRedis::getInstance()->zAdd($this->getEnrollKey($activity_id), microtime(true), $uid);
    }

    /**
     * 是否报名
     * @param $activity_id
     * @param $uid
     * @return float
     */
    public function getEnrollScore($activity_id, $uid)
    {
        return CRedis::getInstance()->zScore($this->getEnrollKey($activity_id), $uid);
    }

    /**
     * 删除某个key
     * @param $key
     * @return int
     */
    public function delKey($key)
    {
        return CRedis::getInstance()->del($key);
    }

    /**
     * 正确答案回答者集合Key
     * @param $activityId
     * @return mixed
     */
    public function getRightAnswerKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_RIGHT_ANSWER);
    }

    /**
     * 添加到提交答案集合
     * @param $activity_id
     * @param $uid
     * @return int
     */
    public function sAddRightAnswer($activity_id, $uid)
    {
        return CRedis::getInstance()->sAdd($this->getRightAnswerKey($activity_id), $uid);
    }

    /**
     * 获取正确答案集合所有成员
     * @param $activity_id
     * @return array
     */
    public function getRightAnswer($activity_id)
    {
        return CRedis::getInstance()->sMembers($this->getRightAnswerKey($activity_id));
    }

    /**
     * 移除答案集合某个用户
     * @param $activity_id
     * @param $uid
     * @return int
     */
    public function sRemRightAnswer($activity_id, $uid)
    {
        return CRedis::getInstance()->sRem($this->getRightAnswerKey($activity_id), $uid);
    }

    /**
     * 获取当前答题人数
     * @param $activity_id
     * @return int
     */
    private function sCardLastRightAnswerNum($activity_id)
    {
        return CRedis::getInstance()->sCard($this->getLastRightAnswerKey($activity_id));
    }

    /**
     * 上道题正确答案回答者集合Key
     */
    public function getLastRightAnswerKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_LAST_RIGHT_ANSWER);
    }

    /**
     * 添加到上次正确答案集合
     * @param $activity_id
     * @param $uid
     * @return int
     */
    public function sAddLastRightAnswer($activity_id, $uid)
    {
        return CRedis::getInstance()->sAdd($this->getLastRightAnswerKey($activity_id), $uid);
    }

    /**
     * 获取上次正确答案集合所有成员
     * @param $activity_id
     * @return array
     */
    public function getLastRightAnswer($activity_id)
    {
        return CRedis::getInstance()->sMembers($this->getLastRightAnswerKey($activity_id));
    }

    /**
     * 是否回答正确答案
     * @param $activityId
     * @param $uid
     * @return bool
     */
    public function isLastRightAnswerMember($activityId, $uid)
    {
        return CRedis::getInstance()->sIsMember($this->getLastRightAnswerKey($activityId), $uid);
    }

    //返回正确答案集合两个成员间的差集
    public function getRightAnswerDiff($activity_id)
    {
        return CRedis::getInstance()->sDiff($this->getLastRightAnswerKey($activity_id), $this->getRightAnswerKey($activity_id));
    }


    /**
     * 报名人数
     * @param $activityId
     * @return int
     */
    public function getEnrollNum($activityId)
    {
        return (int)CRedis::getInstance()->zCard($this->getEnrollKey($activityId));
    }

    /**
     * 活动报名用户列表
     */
    public function getEnrollList($activityId, $page = 1, $limit = 20)
    {
        $start = ($page - 1) * $limit;
        $end = $page * $limit;
        $rs = CRedis::getInstance()->zRange($this->getEnrollKey($activityId), $start, $end, true) ?: [];
        if (!$rs) {
            return [];
        }

        $users = User::getInstance()->getUsersInfo(array_keys($rs));
        $list = [];
        foreach ($rs as $uid => $time) {
            if (!empty($users[$uid])) {
                $user = $users[$uid];
                $list[] = [
                    'uid' => $uid,
                    'user_nicename' => $user['user_nicename'],
                    'time' => $time,
                ];
            }
        }

        return $list;
    }

    /**
     * 出局
     */
    public function out($activityId, $uid)
    {
        return CRedis::getInstance()->zAdd($this->getOutKey($activityId), microtime(true), $uid);
    }

    private function getOutKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_OUT);
    }

    /**
     * 是否出局
     */
    public function getOutScore($activityId, $uid)
    {
        return CRedis::getInstance()->zScore($this->getOutKey($activityId), $uid);
    }

    /**
     * 活动出局用户列表
     */
    public function getOutList($activityId, $page = 1, $limit = 20)
    {
        $start = ($page - 1) * $limit;
        $end = $page * $limit;
        $rs = CRedis::getInstance()->zRange($this->getOutKey($activityId), $start, $end, true) ?: [];
        if (!$rs) {
            return [];
        }

        $users = User::getInstance()->getUsersInfo(array_keys($rs));
        $list = [];
        foreach ($rs as $uid => $time) {
            if (!empty($users[$uid])) {
                $user = $users[$uid];
                $list[] = [
                    'uid' => $uid,
                    'user_nicename' => $user['user_nicename'],
                    'time' => $time,
                ];
            }
        }

        return $list;
    }

    /**
     * 活动动态数据
     * @param $activityId
     * @param array $data current_question_no-当前题号;join_num-参与人数；start_at-活动实际开始时间；timestamp-记录返回问题时间(精确到小数点后8位数)
     * @return bool
     */
    public function setActivityDynamicData($activityId, $data)
    {
        if (!$activityId || !is_array($data)) {
            return false;
        }

        $key = $this->getDynamicDataKey($activityId);
        foreach ($data as $hashKey => $value) {
            CRedis::getInstance()->hSet($key, $hashKey, $value);
        }

        return true;
    }

    /**
     * 设置活动动态数据单个hashKey的值
     * @param $activity_id
     * @param $hashKey
     * @param $value
     * @return int
     */
    public function setActivityDynamicDataOneKey($activity_id, $hashKey, $value)
    {
        return CRedis::getInstance()->hSet($this->getDynamicDataKey($activity_id), $hashKey, $value);
    }

    private function getDynamicDataKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_DYNAMIC_DATA);
    }


    /**
     * 记录获奖玩家奖金日志key
     * @param $activity_id
     * @param $uid
     * @return mixed
     */
    private function getPlayerTotalLogKey($activity_id, $uid)
    {
        return str_replace(['{id}', '{uid}'], [$activity_id, $uid], self::KEY_PLAYER_TOTAL_LOG);
    }

    /**
     * 记录玩家每道题日志key
     * @param $activity_id
     * @param $uid
     * @param $current_num
     * @return mixed
     */
    private function getPlayerDetailLogKey($activity_id, $uid, $current_num)
    {
        return str_replace(['{id}', '{uid}', '{current_num}'], [$activity_id, $uid, $current_num], self::KEY_PLAYER_DETAIL_LOG);
    }

    /**
     * 记录获奖玩家奖金日志
     * @param $activity_id
     * @param $uid
     * @param $current_num
     * @param $hashKey
     * @param $value
     * @return int
     */
    public function setPlayerTotalLog($activity_id)
    {
        $prize_num = $this->getPrizedNum($activity_id);

        if ($prize_num) {
            $activity = $this->getActivity($activity_id);

            $total_bonus_amount   = $bonus = (int)round($activity['base_bonus_amount']/$prize_num);  //平分奖金
            $ranking_bonus_amount = $index = 0;
            $extBonus = [];

            if (1 == $activity['extra_bonus_type']) {
                //有加奖
                $extBonus = explode(',', $activity['extra_bonus_by_ranking']);
            }

            $uid_arr  = array_keys($this->getPrizeRank($activity_id));
            foreach($uid_arr as $uid){
                if($extBonus){
                    $ranking_bonus_amount = $extBonus[$index];
                    $total_bonus_amount = bcadd($bonus, $ranking_bonus_amount);
                }
                $data = [
                    'activity_id' => $activity['id'],
                    'live_id' => $activity['live_id'],
                    'uid' => $uid,
                    'base_bonus_amount' => $bonus,
                    'extra_bonus_amount' => $activity['extra_bonus_amount'],
                    'ranking_bonus_amount' => $ranking_bonus_amount,
                    'total_bonus_amount' => $total_bonus_amount,
                ];
                $index++;
                CRedis::getInstance()->sAdd($this->getPlayerTotalLogKey($activity_id, $uid), json_encode($data, JSON_UNESCAPED_UNICODE));
            }
        }
        return true;
    }

    /**
     * 获取获奖玩家奖金日志
     * @param $activity_id
     * @param $uid
     * @return array
     */
    public function getPlayerTotalLogAll()
    {
        $rs = [];
        $keys = $this->getKeys(self::KEY_PLAYER_TOTAL_LOG_PATTERN);

        if ($keys) {
            foreach ($keys as $key) {
                $ret = CRedis::getInstance()->sMembers($key);
                if($ret){
                    foreach($ret as $val){
                        $rs[] = json_decode($val, true);
                    }
                }
                $this->delKey($key);
            }
        }
        return $rs;
    }

    /**
     * 记录玩家每道题日志
     * @param $activity_id
     * @param $uid
     * @param $current_num
     * @param $hashKey
     * @param $value
     * @return int
     */
    public function setPlayerDetailLog($activity_id, $uid, $current_num, $hashKey, $value)
    {
        return CRedis::getInstance()->hSet($this->getPlayerDetailLogKey($activity_id, $uid, $current_num), $hashKey, $value);
    }

    /**
     * 记录玩家每道题日志
     * @param $activity_id
     * @param $uid
     * @param $current_num
     * @param $data
     * @return bool
     */
    public function setPlayerDetailLogAll($activity_id, $uid, $current_num, $data)
    {
        if (!$activity_id || !is_array($data)) {
            return false;
        }

        $key = $this->getPlayerDetailLogKey($activity_id, $uid, $current_num);
        foreach ($data as $hashKey => $value) {
            CRedis::getInstance()->hSet($key, $hashKey, $value);
        }
        return true;
    }

    /**
     * 获取玩家每道题日志
     * @param $activityId
     * @return array
     */
    public function getPlayerDetailLog($activity_id, $uid, $current_num)
    {
        return CRedis::getInstance()->hGetAll($this->getPlayerDetailLogKey($activity_id, $uid, $current_num));
    }

    /**
     * 获取所有玩家每道题日志
     * @param $activityId
     * @return array
     */
    public function getPlayerDetailLogAll()
    {
        $rs = [];
        $keys = $this->getKeys(self::KEY_PLAYER_DETAIL_LOG_PATTERN);
        if ($keys) {
            foreach ($keys as $key) {
                $rs[] = CRedis::getInstance()->hGetAll($key);
                $this->delKey($key);
            }
        }
        return $rs;
    }


    /**
     * 获取活动动态数据
     * @param $activityId
     * @return array
     */
    public function getAllActivityDynamic($activityId)
    {
        return CRedis::getInstance()->hGetAll($this->getDynamicDataKey($activityId));
    }

    public function getActivityDynamic($activityId, $hashKey)
    {
        return CRedis::getInstance()->hGet($this->getDynamicDataKey($activityId), $hashKey);
    }

    /**
     * 玩家数据
     * @param $activityId
     * @param $uid
     * @return array
     */
    public function getAllPlayerData($activityId, $uid)
    {
        return CRedis::getInstance()->hGetAll($this->getPlayerDataKey($activityId, $uid));
    }

    public function getPlayerData($activityId, $uid, $hashKey)
    {
        return CRedis::getInstance()->hGet($this->getPlayerDataKey($activityId, $uid), $hashKey);
    }

    private function getPlayerDataKey($activityId, $uid)
    {
        return str_replace(['{id}', '{uid}'], [$activityId, $uid], self::KEY_PLAYER_DATA);
    }

    /**
     * 设置玩家答题状态
     * @param int $activityId
     * @param int $uid
     * @param int $status -1-出局，1-可继续答题
     * @return int
     */
    public function setPlayerCurrentStatus($activityId, $uid, $status)
    {
        return CRedis::getInstance()->hSet($this->getPlayerDataKey($activityId, $uid), self::$playDataHashKey['current_status'], $status);
    }

    /**
     * 设置玩家答题总耗时
     * @param int $activityId
     * @param int $uid
     * @param float $microTime
     * @return float
     */
    public function setPlayerTotalConsumingTime($activityId, $uid, $microTime)
    {
        return CRedis::getInstance()->hIncrByFloat($this->getPlayerDataKey($activityId, $uid), self::$playDataHashKey['total_consuming_time'], $microTime);
    }

    /**
     * 设置玩家答题每道题耗时
     * @param int $activityId
     * @param int $uid
     * @param float $microTime
     * @return int
     */
    public function setPlayerEachConsumingTime($activityId, $uid, $microTime)
    {
        $key = $this->getPlayerDataKey($activityId, $uid);
        $data = CRedis::getInstance()->hGet($key, self::$playDataHashKey['each_consuming_time']);
        $value = $data ? "{$data},{$microTime}" : $microTime;
        return CRedis::getInstance()->hSet($key, self::$playDataHashKey['each_consuming_time'], $value);
    }

    /**
     * 设置玩家答题每道题答案
     * @param $activityId
     * @param $uid
     * @param $answer
     * @return int
     */
    public function setPlayerEachConsumingAnswer($activityId, $uid, $answer)
    {
        $key = $this->getPlayerDataKey($activityId, $uid);
        $data = CRedis::getInstance()->hGet($key, self::$playDataHashKey['right_answer']);
        $value = $data ? "{$data},{$answer}" : $answer;
        return CRedis::getInstance()->hSet($key, self::$playDataHashKey['right_answer'], $value);
    }

    /**
     * 设置玩家已答题记录
     * @param $activityId
     * @param $uid
     * @param $microTime
     * @return int
     */
    public function setPlayerAnswerLog($activityId, $uid, $question_id)
    {
        $key = $this->getPlayerDataKey($activityId, $uid);
        $data = CRedis::getInstance()->hGet($key, self::$playDataHashKey['answer_log']);
        $value = $data ? "{$data},{$question_id}" : $question_id;
        return CRedis::getInstance()->hSet($key, self::$playDataHashKey['answer_log'], $value);
    }

    /**
     * 设置玩家绑定凯时限制条件提供token
     * @param $activity_id
     * @param $uid
     * @param $token
     * @return int
     */
    private function setPlayerBindKaishiToken($activity_id, $uid, $token)
    {
        return CRedis::getInstance()->hSet($this->getPlayerDataKey($activity_id, $uid), self::$playDataHashKey['kaishi_token'], $token);
    }

    /**
     * 抢答排行榜单分页
     * @param $activityId
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPrizeRankPage($activityId, $page = 1, $limit = 20)
    {
        return CRedis::getInstance()->zRangeByScore($this->getPrizeRankKey($activityId), 0, '+inf', [
            'withscores' => true,
            'limit' => [($page - 1) * $limit, $limit],
        ]);
    }

    /**
     * 抢答排行榜单全部
     * @param $activityId
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPrizeRank($activityId)
    {
        return CRedis::getInstance()->zRange($this->getPrizeRankKey($activityId), 0, -1, true);
    }


    /**
     * 获取新抢答榜榜单分页
     * @param $activity_id
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getNewPrizeRank($activity_id, $page = 1, $limit = 20)
    {
        $current_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        $rs = $this->getEveryOneAnswerTime($activity_id, $current_num, $page, $limit);
        if ($rs) {
            //获取后台过滤的玩家集合
            $filter_users = $this->getEveryOneAnswerTimeFilter($activity_id, $current_num);
            if ($filter_users) {
                $filter_users_keys = array_keys($filter_users);
                foreach ($filter_users as $uid) {
                    if (array_key_exists($uid, $rs)) {
                        unset($rs[$uid]);
                    }
                }
            }
        }
        return $rs;
    }

    /**
     * 获取新排行榜玩家排名数据
     * @param $activity_id
     * @param $current_num
     * @return array
     */
    public function getPrizeRankAll($activity_id, $current_num)
    {
        $rs   = [];
        $rank = 1;
        $ret  = $this->getEveryOneAnswerTimeAll($activity_id, $current_num);
        if ($ret) {
            //获取后台过滤的玩家集合
            $filter_users = $this->getEveryOneAnswerTimeFilter($activity_id, $current_num);
            $filter_users = $filter_users ? array_flip($filter_users) : [];

            //所有用户集合
            $users = User::getInstance()->getUsersInfo(array_keys($ret)) ?: [];

            foreach($ret as $uid => $score){
                $rs[] = [
                    'rank'          => $rank,
                    'uid'           => $uid,
                    'user_nickname' => $users[$uid]['user_nicename'],
                    'score'         => $score,
                    'status'        => array_key_exists($uid, $filter_users) ? '过滤' : '正常',
                ];

                $rank ++;
            }
        }
        return $rs;
    }

    private function getPrizeRankKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_PRIZE_RANK);
    }

    public function getPrizedNum($activityId)
    {
        return (int)CRedis::getInstance()->zCard($this->getPrizeRankKey($activityId));
    }

    public function getPrize($activityId, $uid)
    {
        return CRedis::getInstance()->zScore($this->getPrizeRankKey($activityId), $uid);
    }

    //获取所有匹配模式的key
    public function getKeys($pattern)
    {
        return CRedis::getInstance()->keys($pattern);
    }

    /**
     * 移除排行榜
     * @param $activity_id
     * @param $uid
     * @return int
     */
    public function delPrizeRank($activity_id, $uid)
    {
        return CRedis::getInstance()->zRem($this->getPrizeRankKey($activity_id), $uid);
    }

    /**
     * 设置榜单
     * @param $activityId
     * @param $uid
     * @param $microTime
     * @return int
     */
    public function setPrizeRank($activityId, $uid, $microTime)
    {
        return CRedis::getInstance()->zIncrBy($this->getPrizeRankKey($activityId), $microTime, $uid);
    }

    /**
     * 抢答排行榜
     */
    public function getPrizeList($uid, $activityId, $page = 1, $limit = 20)
    {
        if (!$activityId) {
            $this->setError('活动ID不存在');
            return false;
        }
        $activity = $this->getActivity($activityId);
        if (!$activity) {
            $this->setError('活动不存在');
            return false;
        }

        $list = [];
        $data = [
            'prized_num' => 0,   //中奖总人数
            'base_bonus_amount' => 0,   //总奖金
            'each_person_bonus' => 0,   //每个人中奖
            'ami_prize' => 0,   //我是否中奖 0-未中奖，1-中奖
        ];

        //$rs = $this->getPrizeRankPage($activityId, $page, $limit);
        $rs = $this->getNewPrizeRank($activityId, $page, $limit);
        if ($rs) {
            $activityEnd = in_array($activity['status'], [self::$statusKeyMap['finished'], self::$statusKeyMap['deleted']]);

            $prizedNum = $this->getPrizedNum($activityId);
            $bonus = $prizedNum ? $activity['base_bonus_amount'] / $prizedNum : 0;
            if ($activityEnd) {
                $data = [
                    'prized_num' => $prizedNum,                                  //中奖总人数
                    'base_bonus_amount' => $activity['base_bonus_amount'],              //总奖金
                    'each_person_bonus' => (int)round($bonus),                          //每个人中奖
                    'ami_prize' => $this->getPrize($activityId, $uid) ? 1 : 0, //我是否中奖 0-未中奖，1-中奖
                ];
            }

            $users = User::getInstance()->getUsersInfo(array_keys($rs)) ?: [];
            foreach ($rs as $uid => $time) {
                if (!empty($users[$uid])) {
                    $user = $users[$uid];
                    $value = [
                        //'uid' => $uid,
                        'user_nicename' => $user['user_nicename'],
                        'time' => $this->timeFormat($time),
                    ];
                    if ($activityEnd) {
                        $value['bonus'] = $bonus;
                    }
                    $list[] = $value;
                }
            }
        }
        return [
            'data' => $data,
            'list' => $list,
        ];
    }

    /**
     * 额外抢答排行榜
     */
    public function getExtPrizeList($uid, $activityId, $page = 1, $limit = 20)
    {
        if (!$activityId) {
            $this->setError('活动ID不存在');
            return false;
        }
        $activity = $this->getActivity($activityId);
        if (!$activity) {
            $this->setError('活动不存在');
            return false;
        }

        //奖金
        $extBonus = $activity['extra_bonus_by_ranking'] ? explode(',', $activity['extra_bonus_by_ranking']) : [];
        if (!$extBonus) {
            return null;
        }

        //$limit = count($extBonus);
        //$rs = $this->getPrizeRankPage($activityId, $page, $limit);
        $rs = $this->getNewPrizeRank($activityId, $page, $limit);
        if (!$rs) {
            return [];
        }

        $index = 0;
        $list = [];
        $users = User::getInstance()->getUsersInfo(array_keys($rs)) ?: [];
        foreach ($rs as $uid => $time) {
            if (!empty($users[$uid])) {
                $user = $users[$uid];
                $list[] = [
                    //'uid' => $uid,
                    'user_nicename' => $user['user_nicename'],
                    'time' => $this->timeFormat($time),
                    'bonus' => (float)$extBonus[$index],
                ];
                $index += 1;
            }
        }

        return $list;
    }

    /**
     * 抢答排行总榜单
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getTotalPrizeList($page = 1, $limit = 20)
    {
        $rs = CRedis::getInstance()->zRevRangeByScore($this->getTotalPrizeRankKey(), '+inf', 0, [
            'withscores' => true,
            'limit' => [($page - 1) * $limit, $limit],
        ]);
        if (!$rs) {
            return [];
        }

        $list = [];
        $users = User::getInstance()->getUsersInfo(array_keys($rs));
        foreach ($rs as $uid => $bonus) {
            if (!empty($users[$uid])) {
                $user = $users[$uid];
                $list[] = [
                    //'uid' => $uid,
                    'user_nicename' => $user['user_nicename'],
                    'bonus' => $bonus,
                ];
            }
        }

        return $list;
    }

    private function getTotalPrizeRankKey()
    {
        return self::KEY_TOTAL_PRIZE_RANK;
    }

    /**
     * 设置总榜单
     * @param int $uid
     * @param float $bonus
     * @return float
     */
    public function setTotalPrizeRank($uid, $bonus)
    {
        return CRedis::getInstance()->zIncrBy($this->getTotalPrizeRankKey(), $bonus, $uid);
    }

    /**
     * 在房间获取活动信息
     * @param $liveType
     * @param $liveId
     * @return int|null
     */
    public function getActivityIdInLive($liveType, $liveId)
    {
        $map = [
            self::$statusKeyMap['published'],
            self::$statusKeyMap['ready'],
            self::$statusKeyMap['going'],
            self::$statusKeyMap['finished'],
        ];
        $activities = M('activity_answer')->field('id,live_type,live_id,location_live_id')->where(['status' => ['in', $map]])->order('id DESC')->select();
        if (!$activities) {
            return null;
        }

        $activityId = 0;
        foreach ($activities as $act) {
            if ($act['live_type'] == $liveType && $act['live_id'] == $liveId) {
                $activityId = $act['id'];
                break;
            } else {
                $liveIds = $act['location_live_id'] ? explode(',', $act['location_live_id']) : [];
                if (in_array($liveId, $liveIds)) {
                    $activityId = $act['id'];
                    break;
                }
            }
        }
        return $activityId;
    }

    /**
     * 当前活动问题集合key
     * @param $activityId
     * @return mixed
     */
    private function getQuestionListKey($activityId)
    {
        return str_replace('{id}', $activityId, self::KEY_ACTIVITY_QUESTION);
    }

    /**
     * 当前活动每道题玩家答题时间key
     * @param $activity_id
     * @param $current_num
     * @return mixed
     */
    private function getEveryOneAnswerTimeKey($activity_id, $current_num)
    {
        return str_replace(['{id}', '{current_num}'], [$activity_id, $current_num], self::KEY_PLAYER_LOG);
    }

    /**
     * 当前活动过滤每道题玩家key
     * @param $activity_id
     * @param $current_num
     * @return mixed
     */
    private function getEveryOneAnswerTimeFilterKey($activity_id, $current_num)
    {
        return str_replace(['{id}', '{current_num}'], [$activity_id, $current_num], self::KEY_PLAYER_FILTER_LOG);
    }

    /**
     * 设置每道问题玩家回答时间
     * @param $activity_id
     * @param $current_num
     * @param $uid
     * @param $microTime
     * @return float
     */
    private function setEveryOneAnswerTime($activity_id, $current_num, $uid, $microTime)
    {
        return CRedis::getInstance()->zIncrBy($this->getEveryOneAnswerTimeKey($activity_id, $current_num), $microTime, $uid);
    }

    /**
     * 设置过滤每道问题玩家
     * @param $activity_id
     * @param $current_num
     * @param $uid
     * @return int
     */
    public function setEveryOneAnswerTimeFilter($activity_id, $current_num, $uid)
    {
        return CRedis::getInstance()->sAdd($this->getEveryOneAnswerTimeFilterKey($activity_id, $current_num), $uid);
    }

    /**
     * 获取过滤每道问题玩家
     * @param $activity_id
     * @param $current_num
     * @param $uid
     * @return array
     */
    private function getEveryOneAnswerTimeFilter($activity_id, $current_num)
    {
        return CRedis::getInstance()->sMembers($this->getEveryOneAnswerTimeFilterKey($activity_id, $current_num));
    }

    /**
     * 移除过滤玩家uid
     * @param $activity_id
     * @param $current_num
     * @param $uid
     * @return int
     */
    private function delEveryOneAnswerTimeFilter($activity_id, $current_num, $uid)
    {
        return CRedis::getInstance()->sRem($this->getEveryOneAnswerTimeFilterKey($activity_id, $current_num), $uid);
    }

    /**
     * 获取每道问题玩家回答时间
     * @param $activity_id
     * @param $current_num
     * @return array
     */
    public function getEveryOneAnswerTime($activity_id, $current_num, $page = 1, $limit = 20)
    {
        return CRedis::getInstance()->zRangeByScore($this->getEveryOneAnswerTimeKey($activity_id, $current_num), 0, '+inf', [
            'withscores' => true,
            'limit' => [($page - 1) * $limit, $limit],
        ]);
    }

    /**
     * 获取新排行榜每道问题玩家回答时间
     * @param $activity_id
     * @param $current_num
     * @return array
     */
    public function getEveryOneAnswerTimeAll($activity_id, $current_num)
    {
        return CRedis::getInstance()->zRange($this->getEveryOneAnswerTimeKey($activity_id, $current_num), 0, -1, true);
    }


    /**
     * 当前活动每道问题答案数量集合key
     * @param $activity_id
     * @param $current_num
     * @return mixed
     */
    private function getEveryOneAnswerNumKey($activity_id, $current_num)
    {
        return str_replace(['{id}', '{current_num}'], [$activity_id, $current_num], self::KEY_NUM_OF_ANSWER);
    }

    /**
     * 设置每道问题答案数量自增
     * @param $activity_id
     * @param $current_num
     * @param $hash_key
     * @param $value
     * @return int
     */
    private function setEveryOneAnswerNumAdd($activity_id, $current_num, $hash_key, $value)
    {
        return CRedis::getInstance()->hIncrBy($this->getEveryOneAnswerNumKey($activity_id, $current_num), $hash_key, $value);
    }

    /**
     * 获取答案数量总数
     * @param $activity_id
     * @param $current_num
     * @return array
     */
    public function getEveryOneAnswerNum($activity_id, $current_num)
    {
        return CRedis::getInstance()->hGetAll($this->getEveryOneAnswerNumKey($activity_id, $current_num));
    }

    /**
     * 主播获取问题及备选答案
     * @param $activity_id  int 活动ID
     * @param $live_id      int 主播房间号
     * @param $uid          int 主播用户ID
     * @return array|bool
     */
    public function getCurrentQuestion($activity_id, $live_id, $uid)
    {
        //校验主播token
        //判断活动是否进行中
        if (!(($this->verifyAnchorToken($uid, $live_id)) && ($this->isActivityGoing($activity_id, $live_id)))) {
            return false;
        }

        //获取当前问题信息
        return $this->getCurrentQuestionInfo($activity_id);
    }

    /**
     * 用户获取当前题库问题
     * @param $activity_id
     * @return array
     */
    public function getCurrentQuestionByPlayer($activity_id)
    {

        $ret = [];

        //判断当前用户是否在报名或正确答案提交者集合中
        //if(!($this->isEnroll($activity_id, $uid)) || !($this->isLastRightAnswerMember($activity_id, $uid))){

        //判断活动是否开始
        $activity = $this->getActivity($activity_id);
        if ($activity && (((self::$statusKeyMap['going'] != $activity['status']) && (self::$statusKeyMap['finished'] != $activity['status'])) || (time() < $activity['start_at']))) return $ret;

        $current_num = 0;
        $current_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        if ($current_ques_num) {
            $current_num = $current_ques_num - 1;
        }
        //取得当前问题备选备案
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        $total_num = count($activity_answer_arr);
        $time = microtime(true);

        if (isset($activity_answer_arr[$current_num])) {
            $answer = [];
            $answer_arr = (is_array($activity_answer_arr[$current_num]['choose_answer'])) ? $activity_answer_arr[$current_num]['choose_answer'] : json_decode($activity_answer_arr[$current_num]['choose_answer']);
            array_walk($answer_arr, function ($v, $k) use (&$answer) {
                $key = $k + 1;
                $answer[$k] = [
                    'key' => $key,
                    'text' => $v,
                ];
            });
            $ret = [
                'id' => $current_num + 1,
                'content' => [
                    'question' => $activity_answer_arr[$current_num]['name'],
                    'answer' => $answer,
                ],
                'timestamp' => $time,
                'total_num' => $total_num,
            ];
        }
        //}
        return $ret;
    }


    /**
     * 获取当前当前问题
     * @param $activity_id int 活动ID
     * @return array|bool
     */
    private function getCurrentQuestionInfo($activity_id)
    {
        //获取当前活动存储的题目代号：
        //判断是否有keyNo, 没有则返回第一题答案, 并重置下一题的id为当前No.,

        //redis存储当期的问题代号
        $current_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');

        //开始答题,current_question_no初始化,反之下一题
        $status = (!$current_ques_num) ? 'init' : 'next';

        //取得当前问题备选备案
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        if ($activity_answer_arr === false) {
            return false;
        }

        $total_num = count($activity_answer_arr);
        $time = microtime(true);
        $ret = $answer = [];
        $id = 0;
        $question = '';

        if ('init' == $status) {
            $id = 1;
            $question = $activity_answer_arr[0]['name'];
            $answer_arr = (is_array($activity_answer_arr[0]['choose_answer'])) ? $activity_answer_arr[0]['choose_answer'] : json_decode($activity_answer_arr[0]['choose_answer']);
            array_walk($answer_arr, function ($v, $k) use (&$answer) {
                $key = $k + 1;
                $answer[$k] = [
                    'key' => $key,
                    'text' => $v,
                ];
            });

            //设置活动状态为进行中
            $map = [
                'id' => $activity_id,
                'status' => self::$statusKeyMap['going'],
            ];
            $this->saveActivity($map);

        } else {
            if (isset($activity_answer_arr[$current_ques_num])) {

                //对比上一题获取时间如果<=后台设置时长,则非法请求
                $activity_info = $this->getActivity($activity_id);
                if (bcsub($time, $this->getActivityDynamic($activity_id, 'timestamp')) < $activity_info['answer_countdown_at']) {
                    $this->setError('频繁操作');//距离上次答题不到10s
                    return false;
                }

                $id = $current_ques_num + 1;
                $question = $activity_answer_arr[$current_ques_num]['name'];
                $answer_arr = (is_array($activity_answer_arr[$current_ques_num]['choose_answer'])) ? $activity_answer_arr[$current_ques_num]['choose_answer'] : json_decode($activity_answer_arr[$current_ques_num]['choose_answer']);
                array_walk($activity_answer_arr[$current_ques_num]['choose_answer'], function ($v, $k) use (&$answer) {
                    $key = $k + 1;
                    $answer[$k] = [
                        'key' => $key,
                        'text' => $v,
                    ];
                });

            } else {
                $this->setError('题库结束');//题库结束
                return false;
            }
        }

        $ret = [
            'id' => $id,
            'content' => [
                'question' => $question,
                'answer' => $answer,
            ],
            'timestamp' => $time,
            'total_num' => $total_num,
        ];

        //存储当前题号

        $data = [
            'current_question_no' => $id,
            'timestamp' => $time,
            'published_status' => self::$ActivityDynamicDataStatusMap['begin'],//设置当前问题的状态为1,
        ];
        $this->setActivityDynamicData($activity_id, $data);

        return $ret;
    }

    /**
     * 校验主播token
     * @param $uid          int 主播用户标识
     * @param $live_id      int 房间ID
     * @return bool
     */
    private function verifyAnchorToken($uid, $live_id)
    {
        $anchorinfo = User::getInstance()->getUserInfo($live_id);
        if (!$anchorinfo) {
            $this->setError('主播不存在');
            return false;
        }
        if ($uid != $anchorinfo['id']) {
            $this->setError('非当前主播,权限不足');
            return false;
        }
        return true;
    }

    /**
     * 用户提交答案并验证返回结果
     * @param $activity_id  int 活动ID
     * @param $live_id      int 主播房间ID
     * @param $uid          int 用户ID
     * @param $answer
     */
    public function pushVerifyAnswer($activity_id, $live_id, $uid, $answer)
    {
        if (!$activity_id || !$live_id || !$uid || !$answer) {
            $this->setError('非法请求');
            return false;
        }

        /*
            1、验证用户是否报名
            2、redis获取当前问题及答案匹配用户答案
            3、匹配失败移除报名集合并写入出局集合，返回失败状态
            4、匹配成功返回状态并继续答题
         */

        //判断用户是否登录; 判断活动是否进行中; 判断用户是否在用户集合中; 判断是否出局.
        if (!($this->isLogin($uid) && $this->isActivityGoing($activity_id, $live_id) && $this->isEnroll($activity_id, $uid))) {
            return false;
        }

        //判断是否答对上一题(考虑用户不答题，超时)
        if (!$this->isLastRightAnswerMember($activity_id, $uid)) {
            $this->setError('非法请求, 已出局right-answer');
            return false;
        }

        //是否已回答当前问题
        if (false === $this->isUnAnswer($activity_id, $uid)) {
            return false;
        }

        //是否匹配答案
        if (false === $this->verifyAnswer($activity_id, $uid, $answer)) {
            $this->setError('回答错误');
            return false;
        }

        //记录用户答题时间
        if (false === $this->noteOfUserAnswer($activity_id, $uid)) {
            return false;
        }

        //正确答案集合
        $this->sAddRightAnswer($activity_id, $uid);
        $this->setPlayerCurrentStatus($activity_id, $uid, 1);
        return true;
    }

    /**
     * 判断用户是否未答题
     * @param $activity_id  int 活动ID
     * @param $uid          int 用户ID
     * @return bool
     */
    public function isUnAnswer($activity_id, $uid)
    {
        $current_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        if (!$current_ques_num) {
            $this->setError('活动未开始');
            return false;
        }

        $log_str = $this->getPlayerData($activity_id, $uid, self::$playDataHashKey['answer_log']);
        if (!$log_str) {
            return true;
        }

        if (array_key_exists($current_ques_num, array_flip(explode(',', $log_str)))) {
            $this->setError('您已参与过');
            return false;
        }
    }

    /**
     * 记录用户答题时间(玩家回答正确的前提下)
     * @param $activity_id  int 活动ID
     * @param $uid          int 用户ID
     * @return bool
     */
    private function noteOfUserAnswer($activity_id, $uid)
    {
        //取出主播提取当前问题的时间
        $timestamp = $this->getActivityDynamic($activity_id, 'timestamp');
        $user_answer_time = microtime(true) - $timestamp;

        //设置玩家答题时间
        $this->setPlayerEachConsumingTime($activity_id, $uid, $user_answer_time);
        $this->setPlayerTotalConsumingTime($activity_id, $uid, $user_answer_time);

        #########后台日志统计需求 start###########
        $current_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        $this->setEveryOneAnswerTime($activity_id, $current_ques_num, $uid, $user_answer_time);
        #########后台日志统计需求 end###########

        //写入榜单
        $this->setPrizeRank($activity_id, $uid, $user_answer_time);

        return true;
    }


    /**
     * 获取答案数量总数
     * @param $activity_id
     * @return array
     */
    public function getEveryOneAnswerNumData($activity_id)
    {
        $next_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        return $this->getEveryOneAnswerNum($activity_id, $next_ques_num);
    }

    /**
     * 匹配答案
     * @param $activity_id  int 活动ID
     * @param $uid          int 用户ID
     * @param $answer       string 用户答案
     * @param $right_answer string 正确答案
     * @return bool
     */
    public function verifyAnswer($activity_id, $uid, $answer)
    {
        $next_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        if (!$next_ques_num) {
            $this->setError('活动未开始');
            return false;
        }
        $current_ques_num = $next_ques_num - 1;
        $activity         = $this->getActivity($activity_id);
        $time             = microtime(true);
        $user_answer_time = $time - $this->getActivityDynamic($activity_id, 'timestamp');

        //答题是否超时,对比上一题获取时间如果<=答案设置,则非法请求
        if ($user_answer_time > $activity['answer_countdown_at']) {
            $this->setError('答题超时');
            return false;
        }

        //取得当前问题备选备案
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        if ($activity_answer_arr === false) {
            return false;
        }

        //当前问题是否存在
        if (!isset($activity_answer_arr[$current_ques_num])) {
            $this->setError('非法请求, 无此问题');
            return false;
        }

        //记录玩家答题记录
        $this->setPlayerAnswerLog($activity_id, $uid, $next_ques_num);

        $data = [
            'activity_id'           => $activity_id,
            'live_id'               => $activity['live_id'],
            'question_name'         => $activity_answer_arr[$current_ques_num]['name'],
            'right_answer'          => $activity_answer_arr[$current_ques_num]['right_answer'],
            'uid'                   => $uid,
            'u_answer'              => $answer,
            'u_answer_time'         => $time,
            'u_answer_used_time'    => $user_answer_time,
        ];
        $this->setPlayerDetailLogAll($activity_id, $uid, $next_ques_num, $data);


        //记录每个玩家答案归类
        $this->setEveryOneAnswerNumAdd($activity_id, $next_ques_num, $answer, 1);
        //记录每个玩家正确答案日志
        $this->setPlayerEachConsumingAnswer($activity_id, $uid, $answer);

        $question_info = $activity_answer_arr[$current_ques_num];

        if ($question_info['right_answer'] != $answer) {
            $this->setError('答案错误');
            $this->setPlayerDetailLog($activity_id, $uid, $next_ques_num, 'status', 0);
            return false;
        }

        $this->setPlayerDetailLog($activity_id, $uid, $next_ques_num, 'status', 1);
        return true;
    }

    /**
     * 主播公布结果,获取当前问题各个答案人数和
     */
    public function announceResults($activity_id, $live_id, $uid)
    {
        if (!$activity_id || !$live_id || !$uid) {
            $this->setError('非法请求');
            return false;
        }

        //校验主播token
        //判断活动是否进行中
        if (!(($this->verifyAnchorToken($uid, $live_id)) && ($this->isActivityGoing($activity_id, $live_id)))) {
            return false;
        }

        //当前答对结果集与上次问题答对结果集(临时)做差集，差集结全部到出局集合,并将对差集用户成员的状态全部置为-1出局, 并将临时结果集置为当前答对答案结果集
        $this->getFailPlayUserStatus($activity_id);

        //当公布答案时状态更改为-1
        $this->setActivityDynamicDataOneKey($activity_id, 'published_status', self::$ActivityDynamicDataStatusMap['finished']);

        //判断当前题目是否最后一题,如是最后一题则操作活动状态为结束
        $this->setFinishedActivityAction($activity_id);

        return $this->answerTotalNum($activity_id);
    }

    /**
     * 处理当前答题非正确玩家状态集合
     * @param $activity_id
     */
    public function getFailPlayUserStatus($activity_id)
    {
        //当前答对结果集与上次问题答对结果集(临时)做差集，差集结全部到出局集合,并将对差集用户成员的状态全部置为-1出局, 并将临时结果集置为当前答对答案结果集

        //获取差集并重置用户状态
        $out_player = $this->getRightAnswerDiff($activity_id);
        if ($out_player) {
            //加入出局集合并将状态置为-1
            foreach ($out_player as $uid) {

                //写入出局集合
                $this->out($activity_id, $uid);

                //设置玩家状态为不可答
                $this->setPlayerCurrentStatus($activity_id, $uid, -1);

                //移除排行榜
                $this->delPrizeRank($activity_id, $uid);
            }

            //删除上道题答案正确者
            $this->delKey($this->getLastRightAnswerKey($activity_id));

            //并把当前回答问题正确者集合放入上道题集合
            $current_right_answer_arr = $this->getRightAnswer($activity_id);
            if ($current_right_answer_arr) {
                foreach ($current_right_answer_arr as $uid) {
                    $this->sAddLastRightAnswer($activity_id, $uid);

                }
            }
        }
        //删除当前回答问题正确者
        $this->delKey($this->getRightAnswerKey($activity_id));
        return true;
    }

    /**
     * 获取当前问题各个答案人数和
     * @param $activity_id
     * @return array|bool
     */
    public function answerTotalNum($activity_id)
    {

        $next_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        if (!$next_ques_num) {
            $this->setError('活动未开始');
            return false;
        }

        //取得当前问题备选备案
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        if ($activity_answer_arr === false) {
            return false;
        }

        //当前问题是否存在
        $current_ques_num = $next_ques_num - 1;
        if (!isset($activity_answer_arr[$current_ques_num])) {
            $this->setError('非法请求, 无此问题');
            return false;
        }

        $question_info = $activity_answer_arr[$current_ques_num];

        //取答案集合
        $data = [
            'contents' => [],
            'right_answer' => $question_info['right_answer'],
        ];
        $ret = $this->getEveryOneAnswerNumData($activity_id);

        foreach ($question_info['choose_answer'] as $v) {
            $num = isset($ret[$v]) ? $ret[$v] : 0;
            $data['contents'][] = [
                'answer' => $v,
                'num' => $num,
            ];
        }
        return $data;
    }

    /**
     * 获取当前活动答题集合并存储在redis
     * @param $activity_id  int 活动ID
     * @return bool|mixed
     */
    public function getActivityAnswer($activity_id)
    {

        $activity = $this->getActivity($activity_id);
        if (!$activity) {
            $this->setError('活动不存在');
            return false;
        }

        //获取当前活动redis答题答案
        $res = CRedis::getInstance()->get($this->getQuestionListKey($activity_id));
        if ($res) {
            return json_decode($res, true);
        } else {

            $answer_str = M('question_list')->where(['id' => $activity['question_list_id']])->getField('question_ids');

            if ($answer_str) {
                $question_arr = M('question')->field(['id', 'name', 'choose_answer', 'right_answer'])->where('id IN(' . $answer_str . ') ')->select();

                if ($question_arr) {

                    //题目打乱
                    $arr = $arr_temp = [];
                    $arr_key = [];

                    foreach ($question_arr as $k => $v) {
                        $arr_key[] = $v['id'];
                        $temp = json_decode($v['choose_answer'], true);
                        array_walk($temp, function ($v, $k) use (&$temp) {
                            if (empty($v)) {
                                unset($temp[$k]);
                            }
                        });
                        shuffle($temp);
                        $arr_temp[$v['id']] = $v;
                        $arr_temp[$v['id']]['choose_answer'] = $temp;
                    }
                    shuffle($arr_key);
                    foreach ($arr_key as $val) {
                        if (isset($arr_temp[$val])) {
                            $arr[] = $arr_temp[$val];
                        }
                    }
                    CRedis::getInstance()->set($this->getQuestionListKey($activity_id), json_encode($arr, JSON_UNESCAPED_UNICODE));
                    return $arr;
                }
            }
            $this->setError('问题配置不存在');
            return false;
        }
    }

    /**
     * 设置活动结束状态, 及存入日志操作
     * @param $activity_id
     * @return bool
     */
    public function setFinishedActivityAction($activity_id)
    {

        //获取当前活动题号
        $current_ques_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');

        //取得当前问题备选备案
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        if ($activity_answer_arr === false) {
            return false;
        }

        $total_num = count($activity_answer_arr);

        if ($total_num != $current_ques_num) {
            $this->setError('非法请求, 非答题结束');
            return false;
        }

        //当前最后一道题则活动状态结束, 并做操作日志入库操作
        //1、活动结束活动数据入库
        $this->setActivityRsToDB($activity_id);
        //2、活动结束玩家最后奖金日志入库
        $this->setPlayerTotalLog($activity_id);

        $map = [
            'id' => $activity_id,
            'status' => self::$statusKeyMap['finished'],
        ];
        $ret = $this->saveActivity($map);

        if ($ret) {
            return true;
        } else {
            $this->setError('数据更新失败');
            return false;
        }
    }

    /**
     * 判断是否有用户是否登录
     * @param $uid      int 用户ID
     * @return bool
     */
    public function isLogin($uid)
    {
        if (empty($uid)) {
            $this->setError('未登录，不允许参加活动');
            return false;
        }
        return true;
    }

    /**
     * 判断活动是否可进行中
     * @param $activity_id  int 活动ID
     * @param $live_id      int 主播房间ID
     * @return bool
     */
    public function isActivityGoing($activity_id, $live_id)
    {
        $activity = $this->getActivity($activity_id);
        if (!$activity) {
            $this->setError('活动不存在');
            return false;
        }

        if ((self::$statusKeyMap['going'] != $activity['status']) && (self::$statusKeyMap['ready'] != $activity['status'])) {
            $this->setError('非法请求, 活动非进行中');
            return false;
        }

        if ($live_id != $activity['live_id']) {
            $this->setError('非法请求, 非此直播间');
            return false;
        }
        return true;
    }

    /**
     * 判断用户是否在报名集合中
     * @param $activity_id  int 活动ID
     * @param $uid          int 用户ID
     * @return bool
     */
    public function isEnroll($activity_id, $uid)
    {
        if (!$this->getEnrollScore($activity_id, $uid)) {
            $this->setError('非法请求, 未报名或已出局');
            return false;
        }
        return true;
    }

    /**
     * 获取玩家当前答题状态
     * @param $activity_id
     * @param $live_id
     * @param $uid
     * @return bool|int|string
     */
    public function getPlayerStatus($activity_id, $live_id, $uid)
    {
        if (!$activity_id || !$live_id || !$uid) {
            $this->setError('非法请求, 参数为空');
            return false;
        }

        //是否未报名
        if (!$this->getEnrollScore($activity_id, $uid)) {
            return 0;
        }

        //是否出局
        return $this->isLastRightAnswerMember($activity_id, $uid) ? 1 : -1;
    }

    /**
     * 获取当前答题人数
     * @param $activity_id
     * @return int
     */
    public function getLastRightAnswerNum($activity_id)
    {
        if (!$activity_id) {
            $this->setError('非法请求, 参数为空');
            return false;
        }
        return $this->sCardLastRightAnswerNum($activity_id);
    }

    /**
     * 排行榜时间转换
     * @param $time
     * @return string
     */
    public function timeFormat($time)
    {
        $time = sprintf('%.2f', $time);
        list($second, $micro_second) = explode('.', $time);
        if ($second < 60) {
            $second = ($second < 10) ? '0' . $second : $second;
            return '00:' . $second . '"' . $micro_second;
        } elseif (($second > 60)) {
            $min = (int)($second / 60);
            $second = (int)($second % 60);
            $min = ($second < 10) ? '0' . $min : $min;
            $second = ($second < 10) ? '0' . $second : $second;
            return $min . ':' . $second . "'" . $micro_second;
        }
    }

    /**
     * 设置活动状态为开始/活动进行中
     * @param $activity_id
     * @param $live_id
     * @param $uid
     * @param $status
     * @return bool
     */
    public function setActivityStatus($activity_id, $live_id, $uid, $status)
    {
        if (!$activity_id || !$live_id || !$uid || !$status) {
            $this->setError('非法请求, 参数为空');
            return false;
        }

        //验证主播权限
        if (false == $this->verifyAnchorToken($uid, $live_id)) {
            return false;
        }
        $activity_info = $this->getActivity($activity_id);
        if (false == $activity_info) {
            return false;
        }

        //活动结束不可设置
        if (self::$statusKeyMap['going'] < $activity_info['status']) {
            $this->setError('非法设置, 活动已结束');
            return false;
        }

        $status_arr = [self::$statusKeyMap['ready']];
        if (!in_array($status, $status_arr)) {
            $this->setError('非法设置');
            return false;
        }

        $map = [
            'id' => $activity_id,
            'status' => $status,
        ];
        $ret = $this->saveActivity($map);
        if (false == $ret) {
            return false;
        }
        return true;
    }

    /**
     * 获取当前活动日志答题信息
     * @param $activity_id
     * @param int $page
     * @param int $limit
     * @return array|bool
     */
    public function getAnswerLogs($activity_id)
    {

        if (!$activity_id) {
            $this->setError('非法请求, 参数为空');
            return false;
        }

        $ret = [];
        //当前活动
        $activity = $this->getActivity($activity_id);
        $ret['activity'] = (false == $activity) ? [] : [
            'activity_id' => $activity['id'],
            'activity_name' => $activity['name'],
        ];

        //当前活动问题集合
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        $ret['question'] = (false === $activity_answer_arr) ? [] : $activity_answer_arr;

        //默认获取总排行
        $ret['rank'] = $this->getAnswerLogsRank($activity_id);
        return $ret;
    }

    /**
     * 获取当前活动每道题玩家答题日志
     * @param $activity_id
     * @param $question_id
     * @return array|bool
     */
    public function getAnswerUserLogs($activity_id, $question_id)
    {
        if (!$activity_id || !$question_id) {
            $this->setError('非法请求, 参数为空');
            return false;
        }

        $rs = [
            'question' => [
                'name'   => '',
                'answer' => [],
            ],
            'rank' => [],
        ];

        $current_key = (int)$question_id-1;
        //获取当前题目的问题和答案
        //当前活动问题集合
        $activity_answer_arr = $this->getActivityAnswer($activity_id);
        if($activity_answer_arr && isset($activity_answer_arr[$current_key])){
            $rs['question'] = [
                'name'   => $activity_answer_arr[$current_key]['name'],
                'answer' => $activity_answer_arr[$current_key]['choose_answer'],
            ];
        }
        //获取每道题玩家的排名
        $rs['rank'] = $this->getPrizeRankAll($activity_id, $question_id);
        return $rs;
    }

    /**
     * 后台获取玩家排行榜数据
     * @param $activity_id
     * @return array
     */
    public function getAnswerLogsRank($activity_id)
    {
        $ret = [];
        $activity = $this->getActivity($activity_id);
        if (in_array($activity['status'], [self::$statusKeyMap['finished'], self::$statusKeyMap['deleted']])) {

            $rank_num = $this->getPrizedNum($activity_id);
            if ($rank_num) {
                //平分奖金
                $base_bonus_amount = $rank_num ? $activity['base_bonus_amount'] / $rank_num : 0;

                //加奖奖金
                $index = $count_num = $ranking_bonus_amount = 0;
                if(1 == $activity['extra_bonus_type']){
                    //有加奖
                    $extBonus  = explode(',', $activity['extra_bonus_by_ranking']);
                    $count_num = count($extBonus);
                }
                $rs = $this->getPrizeRank($activity_id);

                $rank  = 1;
                $users = User::getInstance()->getUsersInfo(array_keys($rs)) ?: [];
                foreach ($rs as $uid => $time) {
                    if (isset($users[$uid])) {
                        if($extBonus){
                            $ranking_bonus_amount = $index < $count_num ? $extBonus[$index] : 0;
                            $index++;
                        }

                        $val = [
                            'rank'                  => $rank,
                            'uid'                   => $uid,
                            'user_nicename'         => $users[$uid]['user_nicename'],
                            'time'                  => $time,
                            'base_bonus_amount'     => $base_bonus_amount,
                            'ranking_bonus_amount'  => $ranking_bonus_amount,
                        ];
                        $rank ++;
                        $ret[] = $val;
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * 后台过滤/恢复答对用户
     * @param $activity_id
     * @param $question_id
     * @param $uid
     * @param $type
     * @return bool|int
     */
    public function filterPlayerStatus($activity_id, $question_id, $uid, $type)
    {
        if (!$activity_id || !$question_id || !$uid || !$type) {
            $this->setError('非法请求, 参数为空');
            return false;
        }

        if (!in_array($type, ['filter', 'regain'])) {
            $this->setError("未定义设置类型");
            return false;
        }

        //判断是否已经公布答案
        $current_num = (int)$this->getActivityDynamic($activity_id, 'current_question_no');
        if ($current_num != $question_id) {
            $this->setError('非当前题目不可修改');
            return false;
        }

        $current_status = (int)$this->getActivityDynamic($activity_id, 'published_status');
        if (self::$ActivityDynamicDataStatusMap['finished'] == $current_status) {
            $this->setError('已公布答案, 不可修改');
            return false;
        }

        //当前问题用户答案
        $answer = '';
        if($question_id){
            $right_str = $this->getPlayerData($activity_id, $uid, self::$playDataHashKey['right_answer']);
            $right_arr = array_filter(explode(',', $right_str));
            $current_right_answer_key = (int)$current_num - 1;
            $answer = $right_arr[$current_right_answer_key];
        }

        //过滤
        if ('filter' == $type) {
            $this->sRemRightAnswer($activity_id, $uid);
            $rs = $this->setEveryOneAnswerTimeFilter($activity_id, $question_id, $uid);
            if(false == $rs){
                $this->setError('请勿重复操作');
                return false;
            }
            $this->setEveryOneAnswerNumAdd($activity_id, $question_id, $answer, -1);
        }

        //恢复
        if ('regain' == $type) {
            $this->sAddRightAnswer($activity_id, $uid);
            $rs = $this->delEveryOneAnswerTimeFilter($activity_id, $question_id, $uid);
            if(false == $rs){
                $this->setError('请勿重复操作');
                return false;
            }
            $this->setEveryOneAnswerNumAdd($activity_id, $question_id, $answer, 1);
        }
        return true;
    }



    /**
     * 公布榜单后, 当前活动数据日志
     * @param $activity_id
     */
    public function setActivityRsToDB($activity_id)
    {
        $activity = $this->getActivity($activity_id);
        if ($activity) {
            $prizedNum = $this->getPrizedNum($activity_id);
            $bonus = $prizedNum ? $activity['base_bonus_amount'] / $prizedNum : 0;

            $data = [
                'activity_id'           => $activity['id'],
                'join_total_num'        => $this->getEnrollNum($activity_id),
                'issue_amount'          => $activity['base_bonus_amount'],
                'winner_num'            => $prizedNum,
                'everyone_get_bonuses'  => $bonus,
                'restraint_extend'      => json_encode($activity['join_requirements'], JSON_UNESCAPED_UNICODE),
                'created_at'            => time(),
                'updated_at'            => time(),
            ];
        }
        M('activity_finished_log')->add($data);
    }

    /**
     * 记录玩家详情日志
     */
    public function setActivityPlayerDetailLog()
    {
        $rs = $this->getPlayerDetailLogAll();
        if ($rs) {
            $model = M('activity_user_detail_log');
            $time = time();
            foreach ($rs as $v) {
                $data = [
                    'activity_id'       => $v['activity_id'],
                    'live_id'           => $v['live_id'],
                    'question_name'     => $v['question_name'],
                    'right_answer'      => $v['right_answer'],
                    'uid'               => $v['uid'],
                    'status'            => $v['status'],
                    'u_answer'          => $v['u_answer'],
                    'u_answer_at'       => $v['u_answer_time'],
                    'u_answer_used_at'  => $v['u_answer_used_time'],
                    'created_at'        => $time,
                    'updated_at'        => $time,
                ];
                $model->add($data);
            }
        }
    }

    /**
     * 记录玩家获取奖金日志日志
     */
    public function setActivityPlayerTotalLog()
    {
        $rs = $this->getPlayerTotalLogAll();
        if ($rs) {
            $model = M('activity_user_total_log');
            $time = time();
            foreach ($rs as $v) {
                $data = [
                    'activity_id' => $v['activity_id'],
                    'live_id'               => $v['live_id'],
                    'uid'                   => $v['uid'],
                    'base_bonus_amount'     => $v['base_bonus_amount'],
                    'extra_bonus_amount'    => $v['extra_bonus_amount'],
                    'ranking_bonus_amount'  => $v['ranking_bonus_amount'],
                    'total_bonus_amount'    => $v['total_bonus_amount'],
                    'created_at'            => $time,
                    'updated_at'            => $time,
                ];
                $model->add($data);
            }
        }
    }
}
