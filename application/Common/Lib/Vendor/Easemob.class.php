<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Vendor;

use Common\Lib\Auth\User;
use Common\Lib\Service;

class Easemob extends Service
{
    private $client_id;
    private $client_secret;
    private $org_name;
    private $app_name;
    private $url;

    private $tmpFile = '/tmp/resource/txtfile';

    //------------------------------------------------------用户体系
    public function __construct()
    {
        $options = C('EASEMOB')[APP_KEY];
        $this->client_id = isset ($options ['client_id']) ? $options ['client_id'] : '';
        $this->client_secret = isset ($options ['client_secret']) ? $options ['client_secret'] : '';
        $this->org_name = isset ($options ['org_name']) ? $options ['org_name'] : '';
        $this->app_name = isset ($options ['app_name']) ? $options ['app_name'] : '';
        if ($this->org_name && $this->app_name) {
            $this->url = 'https://a1.easemob.com/' . $this->org_name . '/' . $this->app_name . '/';
        }
    }

    /**
     *获取token
     */
    function getToken()
    {
        $options = [
            "grant_type" => "client_credentials",
            "client_id" => $this->client_id,
            "client_secret" => $this->client_secret,
        ];
        $body = json_encode($options);
        $url = $this->url . 'token';
        $tokenResult = $this->postCurl($url, $body, $header = []);
        return "Authorization:Bearer " . $tokenResult['access_token'];
    }

    /**
     * 授权注册
     */
    function createUser($username, $password)
    {
        $url = $this->url . 'users';
        $options = [
            "username" => $username,
            "password" => $password,
        ];
        $body = json_encode($options);
        $header = [$this->getToken()];
        return $this->postCurl($url, $body, $header);
    }

    /*
        批量注册用户
    */
    function createUsers($options)
    {
        $url = $this->url . 'users';
        $body = json_encode($options);
        $header = [$this->getToken()];
        return $this->postCurl($url, $body, $header);
    }

    /*
        重置用户密码
    */
    function resetPassword($username, $newpassword)
    {
        $url = $this->url . 'users/' . $username . '/password';
        $options = [
            "newpassword" => $newpassword,
        ];
        $body = json_encode($options);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, "PUT");
        return $result;
    }

    /*
        获取单个用户
    */
    function getUser($username)
    {
        $url = $this->url . 'users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        return $result;
    }

    /*
        获取批量用户----不分页
    */
    function getUsers($limit = 0)
    {
        if (!empty($limit)) {
            $url = $this->url . 'users?limit=' . $limit;
        } else {
            $url = $this->url . 'users';
        }
        $header = [$this->getToken()];
        return $this->postCurl($url, '', $header, "GET");
    }

    /*
        获取批量用户---分页
    */
    function getUsersForPage($limit = 0, $cursor = '')
    {
        $url = $this->url . 'users?limit=' . $limit . '&cursor=' . $cursor;

        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        if (!empty($result["cursor"])) {
            $cursor = $result["cursor"];
            $this->writeCursor("userfile.txt", $cursor);
        }
        return $result;
    }

    //创建文件夹
    function mkdirs($dir, $mode = 0777)
    {
        if (is_dir($dir) || @mkdir($dir, $mode)) return TRUE;
        if (!$this->mkdirs(dirname($dir), $mode)) return FALSE;
        return @mkdir($dir, $mode);
    }

    //写入cursor
    function writeCursor($filename, $content)
    {
        //判断文件夹是否存在，不存在的话创建
        if (!file_exists($this->tmpFile)) {
            $this->mkdirs($this->tmpFile);
        }
        $myfile = @fopen("{$this->tmpFile}/" . $filename, "w+");
        @fwrite($myfile, $content);
        fclose($myfile);
    }

    //读取cursor
    function readCursor($filename)
    {
        //判断文件夹是否存在，不存在的话创建
        if (!file_exists($this->tmpFile)) {
            $this->mkdirs($this->tmpFile);
        }
        $file = "{$this->tmpFile}/" . $filename;
        $fp = fopen($file, "a+");//这里这设置成a+
        if ($fp) {
            while (!feof($fp)) {
                //第二个参数为读取的长度
                $data = fread($fp, 1000);
            }
            fclose($fp);
        }
        return $data;
    }

    /*
        删除单个用户
    */
    function deleteUser($username)
    {
        $url = $this->url . 'users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        删除批量用户
        limit:建议在100-500之间，、
        注：具体删除哪些并没有指定, 可以在返回值中查看。
    */
    function deleteUsers($limit)
    {
        $url = $this->url . 'users?limit=' . $limit;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        修改用户昵称
    */
    function editNickname($username, $nickname)
    {
        $url = $this->url . 'users/' . $username;
        $options = [
            "nickname" => $nickname,
        ];
        $body = json_encode($options);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, 'PUT');
        return $result;
    }

    /*
        添加好友-
    */
    function addFriend($username, $friend_name)
    {
        $url = $this->url . 'users/' . $username . '/contacts/users/' . $friend_name;
        $header = [$this->getToken(), 'Content-Type:application/json'];
        $result = $this->postCurl($url, '', $header, 'POST');
        return $result;
    }

    /*
        删除好友
    */
    function deleteFriend($username, $friend_name)
    {
        $url = $this->url . 'users/' . $username . '/contacts/users/' . $friend_name;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        查看好友
    */
    function showFriends($username)
    {
        $url = $this->url . 'users/' . $username . '/contacts/users';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        查看用户黑名单
    */
    function getBlacklist($username)
    {
        $url = $this->url . 'users/' . $username . '/blocks/users';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        往黑名单中加人
    */
    function addUserForBlacklist($username, $usernames)
    {
        $url = $this->url . 'users/' . $username . '/blocks/users';
        $body = json_encode($usernames);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, 'POST');
        return $result;
    }

    /*
        从黑名单中减人
    */
    function deleteUserFromBlacklist($username, $blocked_name)
    {
        $url = $this->url . 'users/' . $username . '/blocks/users/' . $blocked_name;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
       查看用户是否在线
    */
    function isOnline($username)
    {
        $url = $this->url . 'users/' . $username . '/status';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        查看用户离线消息数
    */
    function getOfflineMessages($username)
    {
        $url = $this->url . 'users/' . $username . '/offline_msg_count';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        查看某条消息的离线状态
        ----deliverd 表示此用户的该条离线消息已经收到
    */
    function getOfflineMessageStatus($username, $msg_id)
    {
        $url = $this->url . 'users/' . $username . '/offline_msg_status/' . $msg_id;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        禁用用户账号
    */
    function deactiveUser($username)
    {
        $url = $this->url . 'users/' . $username . '/deactivate';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header);
        return $result;
    }

    /*
        解禁用户账号
    */
    function activeUser($username)
    {
        $url = $this->url . 'users/' . $username . '/activate';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header);
        return $result;
    }

    /*
        强制用户下线
    */
    function disconnectUser($username)
    {
        $url = $this->url . 'users/' . $username . '/disconnect';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }
    //--------------------------------------------------------上传下载
    /*
        上传图片或文件
    */
    function uploadFile($filePath)
    {
        $url = $this->url . 'chatfiles';
        $file = file_get_contents($filePath);
        $body['file'] = $file;
        $header = ['Content-type: multipart/form-data', $this->getToken(), "restrict-access:true"];
        $result = $this->postCurl($url, $body, $header, 'XXX');
        return $result;
    }

    /*
        下载文件或图片
    */
    function downloadFile($uuid, $shareSecret, $ext)
    {
        $url = $this->url . 'chatfiles/' . $uuid;
        $header = ["share-secret:" . $shareSecret, "Accept:application/octet-stream", $this->getToken(),];

        if ($ext == "png") {
            $result = $this->postCurl($url, '', $header, 'GET');
        } else {
            $result = $this->getFile($url);
        }
        $filename = md5(time() . mt_rand(10, 99)) . "." . $ext; //新图片名称
        if (!file_exists("resource/down")) {
            mkdir("resource/down/");
        }

        $file = @fopen("resource/down/" . $filename, "w+");//打开文件准备写入
        @fwrite($file, $result);//写入
        fclose($file);//关闭
        return $filename;

    }

    function getFile($url)
    {
        set_time_limit(0); // unlimited max execution time

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600); //max 10 minutes
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /*
        下载图片缩略图
    */
    function downloadThumbnail($uuid, $shareSecret)
    {
        $url = $this->url . 'chatfiles/' . $uuid;
        $header = ["share-secret:" . $shareSecret, "Accept:application/octet-stream", $this->getToken(), "thumbnail:true"];
        $result = $this->postCurl($url, '', $header, 'GET');
        $filename = md5(time() . mt_rand(10, 99)) . "th.png"; //新图片名称
        if (!file_exists("resource/down")) {
            $this->mkdirs("resource/down/");
        }

        $file = @fopen("resource/down/" . $filename, "w+");//打开文件准备写入
        @fwrite($file, $result);//写入
        fclose($file);//关闭
        return $filename;
    }

    //--------------------------------------------------------发送消息
    /*
        发送文本消息
    */
    function sendText($from = "admin", $target_type, $target, $content, $ext)
    {
        $url = $this->url . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "txt";
        $options['msg'] = $content;
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $b, $header);
        return $result;
    }

    /*
        发送透传消息
    */
    function sendCmd($from = "admin", $target_type, $target, $action, $ext)
    {
        $url = $this->url . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "cmd";
        $options['action'] = $action;
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $b, $header);
        return $result;
    }

    /*
        发图片消息
    */
    function sendImage($filePath, $from = "admin", $target_type, $target, $filename, $ext)
    {
        $result = $this->uploadFile($filePath);
        $uri = $result['uri'];
        $uuid = $result['entities'][0]['uuid'];
        $shareSecret = $result['entities'][0]['share-secret'];
        $url = $this->url . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "img";
        $options['url'] = $uri . '/' . $uuid;
        $options['filename'] = $filename;
        $options['secret'] = $shareSecret;
        $options['size'] = [
            "width" => 480,
            "height" => 720,
        ];
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $b, $header);
        return $result;
    }

    /*
        发语音消息
    */
    function sendAudio($filePath, $from = "admin", $target_type, $target, $filename, $length, $ext)
    {
        $result = $this->uploadFile($filePath);
        $uri = $result['uri'];
        $uuid = $result['entities'][0]['uuid'];
        $shareSecret = $result['entities'][0]['share-secret'];
        $url = $this->url . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "audio";
        $options['url'] = $uri . '/' . $uuid;
        $options['filename'] = $filename;
        $options['length'] = $length;
        $options['secret'] = $shareSecret;
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $b, $header);
        return $result;
    }

    /*
        发视频消息
    */
    function sendVedio($filePath, $from = "admin", $target_type, $target, $filename, $length, $thumb, $thumb_secret, $ext)
    {
        $result = $this->uploadFile($filePath);
        $uri = $result['uri'];
        $uuid = $result['entities'][0]['uuid'];
        $shareSecret = $result['entities'][0]['share-secret'];
        $url = $this->url . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "video";
        $options['url'] = $uri . '/' . $uuid;
        $options['filename'] = $filename;
        $options['thumb'] = $thumb;
        $options['length'] = $length;
        $options['secret'] = $shareSecret;
        $options['thumb_secret'] = $thumb_secret;
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $b, $header);
        return $result;
    }

    /*
    发文件消息
    */
    function sendFile($filePath, $from = "admin", $target_type, $target, $filename, $length, $ext)
    {
        $result = $this->uploadFile($filePath);
        $uri = $result['uri'];
        $uuid = $result['entities'][0]['uuid'];
        $shareSecret = $result['entities'][0]['share-secret'];
        $url = $GLOBALS['base_url'] . 'messages';
        $body['target_type'] = $target_type;
        $body['target'] = $target;
        $options['type'] = "file";
        $options['url'] = $uri . '/' . $uuid;
        $options['filename'] = $filename;
        $options['length'] = $length;
        $options['secret'] = $shareSecret;
        $body['msg'] = $options;
        $body['from'] = $from;
        $body['ext'] = $ext;
        $b = json_encode($body);
        $header = [getToken()];
        $result = postCurl($url, $b, $header);
        return $result;
    }
    //-------------------------------------------------------------群组操作

    /*
        获取app中的所有群组----不分页
    */
    function getGroups($limit = 0)
    {
        if (!empty($limit)) {
            $url = $this->url . 'chatgroups?limit=' . $limit;
        } else {
            $url = $this->url . 'chatgroups';
        }

        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        return $result;
    }

    /*
        获取app中的所有群组---分页
    */
    function getGroupsForPage($limit = 0, $cursor = '')
    {
        $url = $this->url . 'chatgroups?limit=' . $limit . '&cursor=' . $cursor;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");

        if (!empty($result["cursor"])) {
            $cursor = $result["cursor"];
            $this->writeCursor("groupfile.txt", $cursor);
        }
        return $result;
    }

    /*
        获取一个或多个群组的详情
    */
    function getGroupDetail($group_ids)
    {
        $g_ids = implode(',', $group_ids);
        $url = $this->url . 'chatgroups/' . $g_ids;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        创建一个群组
    */
    function createGroup($options)
    {
        $url = $this->url . 'chatgroups';
        $header = [$this->getToken()];
        $body = json_encode($options);
        $result = $this->postCurl($url, $body, $header);
        return $result;
    }

    /*
        修改群组信息
    */
    function modifyGroupInfo($group_id, $options)
    {
        $url = $this->url . 'chatgroups/' . $group_id;
        $body = json_encode($options);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, 'PUT');
        return $result;
    }

    /*
        删除群组
    */
    function deleteGroup($group_id)
    {
        $url = $this->url . 'chatgroups/' . $group_id;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        获取群组中的成员
    */
    function getGroupUsers($group_id)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/users';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        群组单个加人
    */
    function addGroupMember($group_id, $username)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/users/' . $username;
        $header = [$this->getToken(), 'Content-Type:application/json'];
        $result = $this->postCurl($url, '', $header);
        return $result;
    }

    /*
        群组批量加人
    */
    function addGroupMembers($group_id, $usernames)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/users';
        $body = json_encode($usernames);
        $header = [$this->getToken(), 'Content-Type:application/json'];
        $result = $this->postCurl($url, $body, $header);
        return $result;
    }

    /*
        群组单个减人
    */
    function deleteGroupMember($group_id, $username)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        群组批量减人
    */
    function deleteGroupMembers($group_id, $usernames)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/users/' . $usernames;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        获取一个用户参与的所有群组
    */
    function getGroupsForUser($username)
    {
        $url = $this->url . 'users/' . $username . '/joined_chatgroups';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        群组转让
    */
    function changeGroupOwner($group_id, $options)
    {
        $url = $this->url . 'chatgroups/' . $group_id;
        $body = json_encode($options);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, 'PUT');
        return $result;
    }

    /*
        查询一个群组黑名单用户名列表
    */
    function getGroupBlackList($group_id)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/blocks/users';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        群组黑名单单个加人
    */
    function addGroupBlackMember($group_id, $username)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/blocks/users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header);
        return $result;
    }

    /*
        群组黑名单批量加人
    */
    function addGroupBlackMembers($group_id, $usernames)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/blocks/users';
        $body = json_encode($usernames);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header);
        return $result;
    }

    /*
        群组黑名单单个减人
    */
    function deleteGroupBlackMember($group_id, $username)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/blocks/users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        群组黑名单批量减人
    */
    function deleteGroupBlackMembers($group_id, $usernames)
    {
        $url = $this->url . 'chatgroups/' . $group_id . '/blocks/users';
        $body = json_encode($usernames);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header, 'DELETE');
        return $result;
    }
    //-------------------------------------------------------------聊天室操作
    /*
        创建聊天室
    */
    function createChatRoom($options)
    {
        $url = $this->url . 'chatrooms';
        $header = [$this->getToken()];
        $body = json_encode($options);
        $result = $this->postCurl($url, $body, $header);
        return $result;
    }

    /*
        修改聊天室信息
    */
    function modifyChatRoom($chatroom_id, $options)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id;
        $body = json_encode($options);
        $result = $this->postCurl($url, $body, $header, 'PUT');
        return $result;
    }

    /*
        删除聊天室
    */
    function deleteChatRoom($chatroom_id)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        获取app中所有的聊天室
    */
    function getChatRooms()
    {
        $url = $this->url . 'chatrooms';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        return $result;
    }

    /*
        获取一个聊天室的详情
    */
    function getChatRoomDetail($chatroom_id)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        获取一个用户加入的所有聊天室
    */
    function getChatRoomJoined($username)
    {
        $url = $this->url . 'users/' . $username . '/joined_chatrooms';
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'GET');
        return $result;
    }

    /*
        聊天室单个成员添加
    */
    function addChatRoomMember($chatroom_id, $username)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id . '/users/' . $username;
        $header = [$this->getToken(), 'Content-Type:application/json'];
        $result = $this->postCurl($url, '', $header);
        return $result;
    }

    /*
        聊天室批量成员添加
    */
    function addChatRoomMembers($chatroom_id, $usernames)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id . '/users';
        $body = json_encode($usernames);
        $header = [$this->getToken()];
        $result = $this->postCurl($url, $body, $header);
        return $result;
    }

    /*
        聊天室单个成员删除
    */
    function deleteChatRoomMember($chatroom_id, $username)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id . '/users/' . $username;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }

    /*
        聊天室批量成员删除
    */
    function deleteChatRoomMembers($chatroom_id, $usernames)
    {
        $url = $this->url . 'chatrooms/' . $chatroom_id . '/users/' . $usernames;
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, 'DELETE');
        return $result;
    }
    //-------------------------------------------------------------聊天记录

    /*
        导出聊天记录----不分页
    */
    function getChatRecord($ql)
    {
        if (!empty($ql)) {
            $url = $this->url . 'chatmessages?ql=' . $ql;
        } else {
            $url = $this->url . 'chatmessages';
        }
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        return $result;
    }

    /*
        导出聊天记录---分页
    */
    function getChatRecordForPage($ql, $limit = 0, $cursor)
    {
        if (!empty($ql)) {
            $url = $this->url . 'chatmessages?ql=' . $ql . '&limit=' . $limit . '&cursor=' . $cursor;
        }
        $header = [$this->getToken()];
        $result = $this->postCurl($url, '', $header, "GET");
        $cursor = isset ($result["cursor"]) ? $result["cursor"] : '-1';
        $this->writeCursor("chatfile.txt", $cursor);
        return $result;
    }

    /**
     * @param $url
     * @param string $body
     * @param array $header
     * @param string $type
     * @return mixed
     */
    function postCurl($url, $body = '', $header = [], $type = "POST")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        array_push($header, 'Accept:application/json');
        array_push($header, 'Content-Type:application/json');
        array_push($header, 'http:multipart/form-data');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (count($body) > 0) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        if (count($header) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        switch ($type) {
            case "GET":
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                break;
            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
        }

        curl_setopt($ch, CURLOPT_USERAGENT, 'SSTS Browser/1.0');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36');
        $res = curl_exec($ch);
        $result = json_decode($res, true);
        curl_close($ch);

        return $result;
    }

    //创建环信用户
    public function createEasemobUser($uid, $token)
    {
        if (!$token) {
            $this->setError('缺参数token');
            return false;
        }
        $user = User::getInstance()->getUserInfo($uid);
        if (!$user) {
            $this->setError('用户不存在');
            return false;
        }

        if ($token != $user['token']) {
            $this->setError('token不合法');
            return false;
        }

        if (User::getInstance()->isVisitor($uid)) {
            $username = $uid;
            $password = 'tmpPwd' . str_replace('temp_', '', $uid);
        } else {
            $username = $uid;
            $password = "fmscms{$uid}";
        }

        $rs = $this->createUser($username, $password);
        if (!$rs) {
            $this->setError('注册失败');
            return false;
        }

        return true;
    }

}
