<?php
namespace Common\Lib\Kaishi;

use Common\Lib\Service;
use Common\Lib\Helpers\Curl;

/**
 * 凯时平台API接口类
 * Class KaishiAPI
 * @package Common\Lib\Kaishi
 */
class KaishiAPI extends Service
{
    private $api_host;
    private static $dev_host  = 'http://php.a01webapi.com';
    private static $prod_host = 'http://a01webapi.fastgoapi.com';
    private static $key = '';

    public static $product_id = [

        //预约限制
        'checkUserRequirements' => 'A06',
    ];


    public function __construct()
    {
        $this->api_host = (ENV_DEV) ? self::$dev_host : self::$prod_host;
    }


    private function getToken($product_id, $login_name)
    {
        return md5($product_id . $login_name . $this->getTime() . self::$key);
    }

    public function getTime()
    {
        return time();
    }

    //预约限制API
    public function checkUserRequirements($arr)
    {
        if( !isset($arr['login_name'])){
            $this->setError('非法请求, 未定义参数');
        }

        $map = [
            'token'          => $this->getToken(self::$product_id[__FUNCTION__], $arr['login_name']),
            'timestamp'      => $this->getTime(),
            'login_name'     => $arr['login_name'],
        ];

        if(isset($arr['flow_limit'])){
            $map['flow_limit'] = $arr['flow_limit'];
        }
        if(isset($arr['recharge_limit'])){
            $map['recharge_limit'] = $arr['recharge_limit'];
        }

        //$ret = Curl::getInstance()->post($this->api_host, $map);
        //return (isset($ret['result']) && ( true == $ret['result'])) ? true : false;

        //目前默认return true
        return true;
    }





















}