<?php

namespace Common\Lib;

use Common\Lib\Helpers\Curl;

class ChinaCache extends Service
{
    const SECRET_KEY = '#M%W1MUA97eD7W#R';
    const VHOST = '';
    const APP = 'live';
    const RECORD_HOST = 'http://record_center:80/center';

    private function buildSecretKey()
    {
        return md5(self::VHOST . self::SECRET_KEY);
    }

    /**
     * 开始录制命令
     * @param $stream
     * @param string $start
     * @param string $end
     * @return array|mixed
     */
    public function record($stream, $start = '', $end = '')
    {
        $data = [
            'vhost' => self::VHOST,
            'app' => self::APP,
            'stream' => $stream,
            'start' => $start,  //"start=2016-11-06_20:35:00&end=2016-11-07_20:35:00"
            'end' => $end,
            'secrectkey' => $this->buildSecretKey(),
            'action' => 'record',
            'onlyone' => 0,
        ];

        return Curl::getInstance()->get(self::RECORD_HOST, $data);
    }

    /**
     * 停止录制命令
     * @param $stream
     * @return array|mixed
     */
    public function stop($stream)
    {
        $data = [
            'vhost' => self::VHOST,
            'app' => self::APP,
            'stream' => $stream,
            'secrectkey' => $this->buildSecretKey(),
            'action' => 'stop',
        ];

        return Curl::getInstance()->get(self::RECORD_HOST, $data);
    }

    /**
     * 查询
     * @param $stream
     * @return array|mixed
     */
    public function query($stream)
    {
        $data = [
            'vhost' => self::VHOST,
            'app' => self::APP,
            'type' => $stream,
            'secrectkey' => $this->buildSecretKey(),
            'action' => 'query',
        ];

        return Curl::getInstance()->get(self::RECORD_HOST, $data);
    }

}
