<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Helpers;

class Func
{
    const KEY_PUBLIC_CONFIG = 'publicConfig';
    const KEY_PRIVATE_CONFIG = 'privateConfig';

    /**
     * 返回带协议的域名
     */
    public static function getHost()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = is_ssl() ? "https://" : "http://";
        return $protocol . $host;
    }

    public static function getRequestURL()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .
            $_SERVER['DOCUMENT_URI'] . '?' . $_SERVER['QUERY_STRING'];
    }

    /**
     * 转化数据库保存的文件路径，为可以访问的url
     * @param $file
     * @param int $defaultType
     * @return string
     */
    public static function getUrl($file = '', $defaultType = 1)
    {
        if (!$file) {
            switch ($defaultType) {
                case 2:
                    $file = self::getSingleVideoDefaultImage();
                    break;
                case 3:
                    $file = self::getYouKeVestIcon();
                    break;
                case 1:
                default:
                    $file = self::getUserDefaultImage();
            }
            return $file;
        }

        if (strpos($file, 'http') === 0) {
            return $file;
        } else if (strpos($file, '/') === 0) {
            return self::getHost() . $file;
        } else {
            return $file;
        }
    }

    private static function getUserDefaultImage()
    {
        return self::getUrl('/default.jpg');
    }

    private static function getSingleVideoDefaultImage()
    {
        return self::getUrl('/public/images/home/video-default-image.jpg');
    }

    private static function getYouKeVestIcon()
    {
        return self::getUrl('/public/images/user/vest-icon-youke.png');
    }

    /**
     * index data
     * @param array $array
     * @param string $key
     * @param string $sort
     * @return array
     */
    public static function index(array $array, $key = 'id', $sort = 'asc')
    {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$key]) && $v) {
                $result[$v[$key]] = $v;
            }
        }
        $array = null;
        if ($sort == 'desc') {
            krsort($result);
        } else {
            ksort($result);
        }

        return $result;
    }

    public static function usort(&$arr, $field, $sorting = SORT_ASC)
    {
        usort($arr, function ($a, $b) use ($field, $sorting) {
            if ($sorting == SORT_ASC) {
                if ($a[$field] > $b[$field]) return 1;
                if ($a[$field] < $b[$field]) return -1;
                return 0;
            } else {
                if ($b[$field] > $a[$field]) return 1;
                if ($b[$field] < $a[$field]) return -1;
                return 0;
            }
        });
    }

    public static function usortByIds(&$arr, $field, $ids)
    {
        usort($arr, function ($a, $b) use ($field, $ids) {
            if (array_search($a[$field], $ids) > array_search($b[$field], $ids))
                return 1;
            if (array_search($a[$field], $ids) < array_search($b[$field], $ids))
                return -1;
            return 0;
        });
    }

    /**
     * 静态资源版本号
     * @param bool $update
     * @return false|string
     */
    public static function getSetStaticVersion($update = false)
    {
        $key = md5(__METHOD__);
        $redis = CRedis::getInstance();
        $defaultVersion = date('mdHi');
        $redisVersion = $redis->get($key);
        $version = $update ? $defaultVersion : ($redisVersion ?: $defaultVersion);
        if ($update || empty($redisVersion)) {
            $redis->set($key, $version);
        }

        return $version;
    }

    /**
     * 清除缓存
     */
    public static function clearCache()
    {
        sp_clear_cache();
        @exec('rm -rf ' . RUNTIME_PATH . '/*');
        self::getSetStaticVersion(true);
    }

    public static function getPublicConfig($update = false)
    {
        $key = self::KEY_PUBLIC_CONFIG;
        $redis = CRedis::getInstance();
        $config = json_decode($redis->get($key), true);
        if (!$config || $update) {
            $config = M('config')->where(['id' => 1])->find();
            $redis->set($key, json_encode($config), 300);
        }

        return $config;
    }

    public static function getPrivateConfig($update = false)
    {
        $key = self::KEY_PRIVATE_CONFIG;
        $redis = CRedis::getInstance();
        $config = json_decode($redis->get($key), true);
        if (!$config || $update) {
            $config = M('config_private')->where(['id' => 1])->find();
            $redis->set($key, json_encode($config), 300);
        }

        return $config;
    }

    /**
     * 过滤关键词
     * @param $field
     * @return null|string|string[]
     */
    public static function filterWord($field)
    {
        $configpri = self::getPrivateConfig();
        $sensitive_field = $configpri['sensitive_field'];
        $sensitive = explode(',', $sensitive_field);
        $replace = [];
        $preg = [];
        foreach ($sensitive as $k => $v) {
            if ($v) {
                $re = '';
                $num = mb_strlen($v);
                for ($i = 0; $i < $num; $i++) {
                    $re .= '*';
                }
                $replace[$k] = $re;
                $preg[$k] = '/' . $v . '/i';
            } else {
                unset($sensitive[$k]);
            }
        }

        return preg_replace($preg, $replace, $field);
    }

    /**
     * @param $cha
     * @return string
     */
    public static function formatTime($cha)
    {
        $iz = floor($cha / 60);
        $hz = floor($iz / 60);
        $s = $cha % 60;
        $i = floor($iz % 60);
        if ($s < 10) {
            $s = '0' . $s;
        }
        if ($i < 10) {
            $i = '0' . $i;
        }
        if ($hz < 10) {
            $hz = '0' . $hz;
        }

        return $hz . ':' . $i . ':' . $s;
    }

    /**
     * @deprecated
     * @param $data
     * @param $info
     * @param $status
     * @return array
     */
    public static function ajaxReturn($data, $info, $status)
    {
        $return = [];
        $return['data'] = $data;
        $return['info'] = $info;
        $return['status'] = $status;
        return $return;
    }

    /**
     * 应用标识(AppKey)
     * @return string
     */
    public static function getAppKey()
    {
        return APP_KEY;
    }

    public static function getLoginNamePrefix()
    {
        $map = [
            'a01' => 'g',
            'a04' => 'm',
            'xsm' => 'c',
        ];
        $key = self::getAppKey();

        return isset($map[$key]) ? $map[$key] : '';
    }

    public static function getFaviconIco()
    {
        $appKey = self::getAppKey();
        return "/public/images/favicon-{$appKey}.ico";
    }


}
