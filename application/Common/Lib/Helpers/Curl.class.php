<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Helpers;

use Common\Lib\Service;

class Curl extends Service
{
    /**
     * Send a GET request using cURL
     * @param string $url to request
     * @param array $get values to send
     * @param array $options for cURL
     * @return array|mixed
     */
    public function get($url, array $get = null, array $options = [])
    {
        $defaults = [
            CURLOPT_URL => $url . (strpos($url, '?') === false ? '?' : '') . http_build_query($get),
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 4
        ];

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if (!$res = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);

        return $this->jsonDecode($res);
    }

    public function post($url, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($curl);
        curl_close($curl);

        return $this->jsonDecode($res);
    }

    private function jsonDecode($data)
    {
        return json_decode($data, true) ? json_decode($data, true) : $data;
    }

    /**
     * 资源是否存在
     * @param $path
     * @return bool
     */
    public function resourcesExist($path)
    {
        if (!$path) {
            return false;
        }

        $headers = get_headers($path);
        $statusCode = substr($headers[0], 9, 3);
        if (in_array($statusCode, [200, 304])) {
            return true;
        } else {
            return false;
        }
    }

}
