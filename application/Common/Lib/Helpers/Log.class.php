<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Helpers;

use Common\Lib\Service;

class Log extends Service
{
    private $tmpDir;

    protected function __construct()
    {
        $this->tmpDir = sys_get_temp_dir() . '/log/';
        if (!is_dir($this->tmpDir)) {
            mkdir($this->tmpDir);
        }
    }

    /**
     * 临时使用
     * @param array $data
     * @return bool
     */
    public function log(array $data)
    {
        if (!ENV_DEV) {
            return null;
        }

        $filename = $this->tmpDir . date('Ymd') . '.log';
        $requestUrl = Func::getRequestURL();
        $string = '[' . $requestUrl . '] ' . json_encode($data) . PHP_EOL;
        file_put_contents($filename, $string, FILE_APPEND);
        return true;
    }

    public function getRequest()
    {
        return json_encode([
            'get' => $_GET,
            'post' => $_POST,
            'session' => $_SESSION,
            'cookie' => $_COOKIE,
            'headers' => $_SERVER,
        ]);
    }

    public function logFile($file, $string)
    {
        file_put_contents($this->tmpDir . $file, $string, FILE_APPEND);
    }

}
