<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Live;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Curl;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;

class Live extends Service
{
    const KEY_ROOM_GAG = 'roomgag:';
    const KEY_CHANNEL_GAG = 'channelgag:';
    const KEY_VISITED_NUM = 'visitednum:';
    const KEY_SLIDE_LIVE_LIST = 'app_slide_live_list';
    const KEY_CACHE_UVL = 'cache:uvl:';
    const KEY_CACHE_USERS_VEST = 'cache:users_vest';

    /**
     * 每日发言数字
     */
    const DAILY_SEND_MSG_NUM = 'daily_send_msg_num';

    /**
     * 直播公告分组信息配置
     * @var array
     */
    public static $liveAnnouncementGroupMap = [
        'huodong' => '活动',
        'tongzhi' => '通知',
        'kuaixun' => '快讯',
        'zhaomu' => '招募',
        'youhui' => '优惠',
        'youjiang' => '有奖',
    ];

    /**
     * 获取m3u8流
     * @param $stream
     * @return array|string
     */
    public function getM3u8Stream($stream)
    {
        $configPri = Func::getPrivateConfig();
        if ($configPri['cdn_switch'] == 4 && $configPri['ws_hls_pull']) {
            $stream .= '/index.m3u8';
        } else {
            $stream .= '.m3u8';
        }

        return $this->getStream('http', $stream, 0);
    }

    /**
     * 扣减用户额度
     *   A02  其他平台方式
     *   C10  自建平台方式，只判断直播券livecoin
     * @param $userLogin
     * @param $userPass
     * @param int $amount
     * @return array
     */
    public function deducteUserCoin($userLogin, $userPass, $amount = 0)
    {
        $channel = Func::getAppKey();
        switch ($channel) {
            case 'xsm':
                $res = $this->deducteC01UserCoin($userLogin, $amount);
                break;
            default:
                $res = ['code' => 400, 'message' => '账户体系有问题'];
        }

        return $res;
    }

    /**
     * @param $userLogin
     * @param $amount
     * @return array
     */
    private function deducteC01UserCoin($userLogin, $amount)
    {
        $liveCoin = $this->cnyToLiveCoin($amount);
        if (M('users')->where(['user_login' => $userLogin])->setDec('livecoin', $liveCoin)) {
            $res = ['code' => 200];
        } else {
            $res = ['code' => 400, 'message' => '操作失败，请重试'];
        }

        return $res;
    }

    /**
     * 获取用户余额 CNY
     * @param $userLogin
     * @param $userPass
     * @return float|int
     */
    public function getUserCoin($userLogin, $userPass)
    {
        $channel = Func::getAppKey();
        switch ($channel) {
            case 'xsm':
                $coin = $this->getC01UserCoin($userLogin);
                break;
            default:
                $coin = 0;
        }

        return $coin;
    }

    /**
     * 获取C01用户余额，取直播券换算成 CNY
     * @param $userLogin
     * @return float|int
     */
    private function getC01UserCoin($userLogin)
    {
        $user = M('users')->where(['user_login' => $userLogin])->find();
        $coin = 0;
        if ($user && $user['livecoin']) {
            $coin = $this->liveCoinToCny($user['livecoin']);
        }

        return $coin;
    }

    /**
     * 货币单位转换
     * @param $cny
     * @return string
     */
    public function cnyToLiveCoin($cny)
    {
        return $cny * $this->getExchangeRate();
    }

    public function liveCoinToCny($liveCoin = 0)
    {
        return $liveCoin / $this->getExchangeRate();
    }

    public function formatLiveCoin($liveCoin = 0)
    {
        return number_format($liveCoin, 2);
    }

    /**
     * 获取汇率
     * @return int
     */
    public function getExchangeRate()
    {
        $config = Func::getPrivateConfig();
        $rate = isset($config['live_coin_percent']) ? $config['live_coin_percent'] : 1;
        if ($rate <= 0) {
            $rate = 1;
        }

        return $rate;
    }

    public function getManager($liveId)
    {
        return M('users_livemanager')->where(['liveuid' => $liveId])->select();
    }

    /**
     * 获取礼物信息
     * @param $giftID
     * @return mixed
     */
    public function getGiftInfo($giftID)
    {
        $giftInfo = M('gift')->where(['id' => $giftID])->find();
        $giftInfo['gifticon'] = Func::getUrl($giftInfo['gifticon']);
        return $giftInfo;
    }

    /**
     * 被观看次数
     */
    public function getRoomLivedNum($liveType, $liveId)
    {
        return (int)CRedis::getInstance()->get($this->getVisitedNumKey($liveType, $liveId));
    }

    public function setRoomLivedNum($liveType, $liveId)
    {
        return CRedis::getInstance()->incr($this->getVisitedNumKey($liveType, $liveId));
    }

    private function getVisitedNumKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_VISITED_NUM . "{$liveType}:{$liveId}";
    }

    /**
     * 获取直播房间在线用户列表数据
     * @param $liveType
     * @param $liveId
     * @return int
     */
    public function getOnlineUserNum($liveType, $liveId)
    {
        return (int)CRedis::getInstance()->hlen($this->getOnlineUserKey($liveType, $liveId));
    }

    /**
     * 排序 黑马 紫马 橙马 黄马 绿马 蓝马 白马 游客
     * @param $liveType
     * @param $liveId
     * @return array
     */
    public function getOnlineUserList($liveType, $liveId)
    {
        $list = CRedis::getInstance()->hGetAll($this->getOnlineUserKey($liveType, $liveId)) ?: [];
        $list = array_reverse($list);
        $black = $purple = $orange = $yellow = $green = $blue = $white = $other = $tmp = [];
        foreach ($list as $v) {
            $user = json_decode($v, true);
            unset($user['user_login'], $user['last_login_ip']);
            if (is_numeric($user['id']) && $user['id'] > 0) {
                switch ($user['vest_id']) {
                    case 7:
                        $black[] = $user;
                        break;
                    case 6:
                        $purple[] = $user;
                        break;
                    case 5:
                        $orange[] = $user;
                        break;
                    case 4:
                        $yellow[] = $user;
                        break;
                    case 3:
                        $blue[] = $user;
                        break;
                    case 2:
                        $green[] = $user;
                        break;
                    case 1:
                        $white[] = $user;
                        break;
                    default:
                        $other[] = $user;
                }
            } else {
                $tmp[] = $user;
            }
        }

        return array_merge($black, $purple, $orange, $yellow, $green, $blue, $white, $other, $tmp);
    }

    public function getOnlineUserKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return "online:{$liveType}:{$liveId}";
    }

    public function addOnlineUser($liveType, $liveId, $uid, $data)
    {
        return CRedis::getInstance()->hSet($this->getOnlineUserKey($liveType, $liveId), $uid, json_encode($data));
    }

    public function delOnlineUser($liveType, $liveId, $uid)
    {
        return CRedis::getInstance()->hDel($this->getOnlineUserKey($liveType, $liveId), $uid);
    }

    /**
     * 统一方法
     * @param $data
     * @return mixed
     */
    public function addUserCoinRecord($data)
    {
        return M('users_coinrecord')->add($data);
    }

    //踢人
    public function setKick($liveType, $liveId, $touid)
    {
        CRedis::getInstance()->hSet($this->getKickKey($liveType, $liveId), $touid, time() + 30);
        $this->delOnlineUser($liveType, $liveId, $touid);

        return true;
    }

    public function getKick($liveType, $liveId, $uid)
    {
        return CRedis::getInstance()->hGet($this->getKickKey($liveType, $liveId), $uid);
    }

    public function delKick($liveType, $liveId, $uid)
    {
        return CRedis::getInstance()->hDel($this->getKickKey($liveType, $liveId), $uid);
    }

    private function getKickKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return "kick:{$liveType}:{$liveId}";
    }

    /**
     * 禁言
     * @param $liveType
     * @param $liveId
     * @param $uid
     * @param $gagTime
     * @return bool
     */
    public function gag($liveType, $liveId, $uid, $gagTime)
    {
        $result = (int)$this->getGag($liveType, $liveId, $uid);
        $time = time();
        if ($time < $result) {
            return true;
        }

        if ($gagTime) {
            $time = $time + $gagTime * 60;
        } else {
            $time = $time + 10 * 60;
        }

        $this->setGag($liveType, $liveId, $uid, $time);

        return true;
    }

    public function setGag($liveType, $liveId, $uid, $time)
    {
        return CRedis::getInstance()->hSet($this->getGagKey($liveType, $liveId), $uid, $time);
    }

    public function getGag($liveType, $liveId, $uid)
    {
        return CRedis::getInstance()->hGet($this->getGagKey($liveType, $liveId), $uid);
    }

    private function getGagKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_ROOM_GAG . "{$liveType}:{$liveId}";
    }

    /**
     * @param $liveType
     * @param $liveId
     * @param $touid
     * @return bool|int 0-你未被禁言,1-你已经被禁言
     */
    public function isGag($liveType, $liveId, $touid)
    {
        $key = $this->getGagKey($liveType, $liveId);
        if (!$key) {
            return 0;
        }

        $result = CRedis::getInstance()->hGet($key, $touid);
        if ($result && time() < $result) {
            return 1;
        }

        return 0;
    }

    /**
     * @param $uid
     * @param $liveType
     * @param $liveId
     * @param bool $forceUpdate
     * @return bool|mixed|string
     */
    public function getUserVest($uid, $liveType, $liveId, $forceUpdate = false)
    {
        $key = $this->getUserVestKey($uid, $liveType, $liveId);
        $redis = CRedis::getInstance();
        $value = $redis->get($key);
        if ($value && !$forceUpdate) {
            return json_decode($value, true);
        }

        $value = M('users_vest_lists')->where([
            'uid' => $uid,
            'live_type' => $liveType,
            'liveid' => $liveId,
        ])->find();

        if ($value) {
            $redis->set($key, json_encode($value));
        }

        return $value;
    }

    private function getUserVestKey($uid, $liveType, $liveId)
    {
        return self::KEY_CACHE_UVL . "{$uid}:{$liveType}:{$liveId}";
    }

    /**
     * 更换马甲（超管）
     * @param $liveType
     * @param $liveId
     * @param $touid
     * @param $vestid
     * @return bool|mixed
     */
    public function changeVest($liveType, $liveId, $touid, $vestid)
    {
        if (!$liveId || !$touid || !$vestid || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        //获取被更换马甲的昵称
        $toUser = User::getInstance()->getUserInfo($touid);
        if (!$toUser) {
            $this->setError('用户不存在');
            return false;
        }

        if ($liveType == 0) {
            $live = M('users_live')->where(['uid' => $liveId])->find();
        } elseif ($liveType == 1) {
            $live = M('muti_show')->where(['id' => $liveId])->find();
        } else {
            $live = null;
        }
        if (!$live) {
            $this->setError('直播不存在');
            return false;
        }

        $userVest = $this->getUserVest($touid, $liveType, $liveId);
        if (!$userVest) {
            $res = M('users_vest_lists')->add([
                'uid' => $touid,
                'live_type' => $liveType,
                'liveid' => $liveId,
                'vestid' => $vestid,
                'addtime' => time(),
            ]);
            if ($res) {
                return true;
            }
        }

        $res = M('users_vest_lists')->where(['id' => $userVest['id']])->save([
            'vestid' => $vestid,
            'addtime' => time(),
        ]);

        if (false === $res) {
            $this->setError('更换马甲失败');
            return false;
        }

        //update users_vest_lists cache
        $this->getUserVest($touid, $liveType, $liveId, true);

        $redis = CRedis::getInstance();
        $sign = md5($liveId . '_' . $touid);
        $key = $this->getOnlineUserKey($liveType, $liveId);
        $userStr = $redis->hGet($key, $sign);
        if ($userStr) {
            $userArr = json_decode($userStr, true);
            $userArr['vestid'] = $vestid;
            $sex = $toUser['sex'];
            if ($sex == 0) {
                $sex = 1;
            }
            $vest = $this->getUsersVest($vestid);
            $vestIcon = Func::getUrl('', 3);
            if ($vest) {
                if ($sex == 1) {
                    $vestIcon = $vest['vest_man_url'];
                }
                if ($sex == 2) {
                    $vestIcon = $vest['vest_woman_url'];
                }
            }

            $userArr['vestIcon'] = $vestIcon;
            $redis->hSet($key, $sign, json_encode($userArr));
        }

        return [
            'user_nicename' => $toUser['user_nicename'],
        ];
    }

    /**
     * 频道禁言
     * @param $liveType
     * @param $liveId
     * @param $userId
     * @return array|bool
     */
    public function channelShutUp($liveType, $liveId, $userId)
    {
        if (!$userId) {
            $this->setError('请先登录');
            return false;
        }

        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        $key = $this->getChannelShutUpKey($liveType, $liveId);
        $redis = CRedis::getInstance();
        if ($redis->get($key)) {
            return true;
        }

        //判断当前用户是否具备频道禁言权限
        $userInfo = M('users')->where(['id' => $userId])->find();
        $userIdentity = User::getInstance()->getUserIdentity($userId, $liveId, $liveType);
        if ($userIdentity == 60) {//超管
            $redis->set($key, $userId);
            return true;
        } elseif (in_array($userIdentity, [40, 50])) {//主播
            $redis->set($key, $userId);
            return true;
        } else {//普通用户
            if ($userInfo['vest_id'] == 6 || $userInfo['vest_id'] == 7) {//用户在后台被设置了紫马或黑马
                $vestInfo = $this->getUsersVest($userInfo['vest_id']);
                if ($vestInfo['gap_all'] == 1) {
                    $redis->set($key, $userId);
                    return [
                        'msg' => '频道禁言成功',
                        'vestName' => $vestInfo['vest_name'],
                    ];
                }
            } else {
                //判断用户的房间马甲权限
                $vestListInfo = $this->getUserVest($userId, $liveType, $liveId);
                if ($vestListInfo) {
                    //获取马甲权限
                    $vestInfo = $this->getUsersVest($vestListInfo['vestid']);
                    if ($vestInfo['gap_all'] == 1) {
                        $redis->set($key, $userId);
                        return [
                            'msg' => '频道禁言成功',
                            'vestName' => $vestInfo['vest_name'],
                        ];
                    }
                }
            }
        }

        $this->setError('您没有房间禁言权限');
        return false;
    }

    /**
     * 判断频道是否禁言
     * @param $liveType
     * @param $liveId
     * @return bool|string
     */
    public function isChannelShutUp($liveType, $liveId)
    {
        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }
        return (int)CRedis::getInstance()->get($this->getChannelShutUpKey($liveType, $liveId));
    }

    private function getChannelShutUpKey($liveType, $liveId)
    {
        $liveType = (int)$liveType;
        $liveId = (int)$liveId;
        return self::KEY_CHANNEL_GAG . "{$liveType}:{$liveId}";
    }

    /**
     * 删除禁言
     * @param $liveType
     * @param $liveId
     * @param $userId
     * @return bool|array
     */
    public function delChannelShutUp($liveType, $liveId, $userId)
    {
        if (!$userId) {
            $this->setError('请先登录');
            return false;
        }

        if (!$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('参数错误');
            return false;
        }

        $channelShutUpUid = Live::getInstance()->isChannelShutUp($liveType, $liveId);
        if (!$channelShutUpUid) {
            $this->setError('该房间未被禁言');
            return false;
        }

        //判断当前用户是否是超管
        $userInfo = M('users')->where(['id' => $userId])->find();
        $userIsSuper = $userInfo['issuper'];
        //判断禁言房间用户是否是超管
        $channelShutUpUser = M('users')->where(['id' => $channelShutUpUid])->find();
        $key = $this->getChannelShutUpKey($liveType, $liveId);
        $redis = CRedis::getInstance();
        $userIdentity = User::getInstance()->getUserIdentity($userId, $liveId, $liveType);
        $channelShutUpUserIdentity = User::getInstance()->getUserIdentity($channelShutUpUid, $liveId, $liveType);
        if ($channelShutUpUser['issuper'] == 1) {//超管禁言
            if ($userIsSuper == 1) {//是超管
                $redis->del($key);
                return true;
            }
        } else {
            if (in_array($channelShutUpUserIdentity, [40, 50])) {//是主播禁言
                //判断当前用户是否是超管
                if ($userIdentity == 60) {
                    $redis->del($key);
                    return true;
                } elseif (in_array($userIdentity, [40, 50])) {//判断当前用户是否是主播
                    $redis->del($key);
                    return true;
                }
            } else {//马甲用户禁言
                //判断当前用户是否是超管
                if ($userIsSuper == 1) {
                    $redis->del($key);
                    return true;
                } elseif (in_array($userIdentity, [40, 50])) {//判断当前用户是否是主播
                    $redis->del($key);
                    return true;
                } else {
                    //获取当前用户的马甲
                    //判断当前用户的马甲等级
                    if ($userInfo['vest_id'] > 1) {
                        $currentUserVestID = $userInfo['vest_id'];
                    } else {
                        $vestListUserInfo = $this->getUserVest($userId, $liveType, $liveId);
                        if ($vestListUserInfo) {
                            $currentUserVestID = $vestListUserInfo['vestid'];
                        } else {
                            $currentUserVestID = 1;//默认白马甲
                        }
                    }

                    //判断禁言用户的马甲ID
                    if ($channelShutUpUser['vest_id'] > 1) {
                        $gagUserVestID = $channelShutUpUser['vest_id'];
                    } else {
                        //从房间马甲列表里读取
                        $gagListUserInfo = $this->getUserVest($channelShutUpUid, $liveType, $liveId);
                        if ($gagListUserInfo) {
                            $gagUserVestID = $gagListUserInfo['vestid'];
                        } else {
                            $gagUserVestID = 1;//默认白马甲
                        }
                    }

                    //判断当前用户的马甲权限级别是否比禁言用户的马甲权限等级高
                    if ($currentUserVestID > $gagUserVestID) {
                        //判断当前用户马甲等级是否有频道禁言权限
                        $vestInfo = $this->getUsersVest($currentUserVestID);
                        if ($vestInfo['gap_all'] == 1) {//当前用户马甲有频道禁言权限
                            $redis->del($key);
                            return [
                                'msg' => '房间解禁成功',
                                'vestName' => $vestInfo['vest_name'],
                            ];
                        }
                    } else if ($currentUserVestID == $gagUserVestID) {//马甲等级相同
                        if ($userId == $channelShutUpUid) {//当前用户是频道禁言用户
                            //判断当前用户马甲等级是否有频道禁言权限
                            $vestInfo = $this->getUsersVest($currentUserVestID);
                            if ($vestInfo['gap_all'] == 1) {//当前用户马甲有频道禁言权限
                                $redis->del($key);
                                return [
                                    'msg' => '房间解禁成功',
                                    'vestName' => $vestInfo['vest_name'],
                                ];
                            }
                        }
                    }
                }
            }
        }

        $this->setError('您的等级不够,无法解除禁言');
        return false;
    }

    /**
     * 获取收到礼物数量(tsd) 以及送出的礼物数量（tsc）
     * @param $uid
     * @return mixed
     */
    public function getGif($uid)
    {
        return M("users_coinrecord")->query('select sum(case when touid=' . $uid .
            ' then 1 else 0 end) as tsd,sum(case when uid=' . $uid .
            ' then 1 else 0 end) as tsc from cmf_users_coinrecord');
    }

    /**
     * 直播间判断是否开启僵尸粉
     * @param $uid
     * @return mixed
     */
    public function isZombie($uid)
    {
        $user = M('users')->field('iszombie')->where(['id' => $uid])->find();
        return $user ? $user['iszombie'] : 0;
    }

    /**
     * @desc 获取推拉流地址
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    public function getStream($host, $stream, $type)
    {
        $config = Func::getPrivateConfig();
        $cdn_switch = $config['cdn_switch'];
        switch ($cdn_switch) {
            case '1':
                $url = $this->getStreamAli($host, $stream, $type);
                break;
            case '2':
                $url = $this->getStreamTx($host, $stream, $type);
                break;
            case '3':
                $url = $this->getStreamQn($host, $stream, $type);
                break;
            case '4':
                $url = $this->getStreamWs($host, $stream, $type);
                break;
            default:
                $url = '';
        }

        return $url;
    }

    /**
     * @desc 阿里云直播A类鉴权
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    private function getStreamAli($host, $stream, $type)
    {
        $configpri = Func::getPrivateConfig();
        $key = $configpri['auth_key'];
        if ($type == 1) {
            $domain = $host . '://' . $configpri['push_url'];
            $time = time() + 60 * 60 * 10;
        } else {
            $domain = $host . '://' . $configpri['pull_url'];
            $time = time() - 60 * 30 + $configpri['auth_length'];
        }

        $filename = "/5showcam/" . $stream;
        if ($key != '') {
            $sstring = $filename . "-" . $time . "-0-0-" . $key;
            $md5 = md5($sstring);
            $auth_key = "auth_key=" . $time . "-0-0-" . $md5;
        }
        if ($type == 1) {
            if ($auth_key) {
                $auth_key = '&' . $auth_key;
            }
            $url = [
                'cdn' => urlencode($domain . '/5showcam'),
                'stream' => urlencode($stream . '?vhost=' . $configpri['pull_url'] . $auth_key),
            ];
        } else {
            if ($auth_key) {
                $auth_key = '?' . $auth_key;
            }
            $url = $domain . $filename . $auth_key;
        }

        return $url;
    }

    /**
     * @desc 腾讯云推拉流地址
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    private function getStreamTx($host, $stream, $type)
    {
        $configpri = Func::getPrivateConfig();
        $bizid = $configpri['tx_bizid'];
        $push_url_key = $configpri['tx_push_key'];

        $stream_a = explode('.', $stream);
        $streamKey = $stream_a[0];
        $live_code = $bizid . "_" . $streamKey;
        $now_time = time() + 3 * 60 * 60;
        $txTime = dechex($now_time);

        $txSecret = md5($push_url_key . $live_code . $txTime);
        $safe_url = "&txSecret=" . $txSecret . "&txTime=" . $txTime;

        if ($type == 1) {
            $url = [
                'cdn' => urlencode("rtmp://" . $bizid . ".livepush2.myqcloud.com/live/"),
                'stream' => urlencode(live_code . "?bizid=" . $bizid . "" . $safe_url),
            ];
        } else {
            $url = 'http://' . $bizid . ".liveplay.myqcloud.com/live/" . $live_code . ".flv";
        }

        return $url;
    }

    /**
     * @desc 七牛云直播
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    private function getStreamQn($host, $stream, $type)
    {
        require_once SITE_PATH . 'api/public/qiniucdn/Pili_v2.php';

        $configpri = Func::getPrivateConfig();
        $ak = $configpri['qn_ak'];
        $sk = $configpri['qn_sk'];
        $hubName = $configpri['qn_hname'];
        $push = $configpri['qn_push'];
        $pull = $configpri['qn_pull'];
        $stream_a = explode('.', $stream);
        $streamKey = $stream_a[0];
        $ext = $stream_a[1];

        if ($type == 1) {
            $time = time() + 60 * 60 * 10;
            //RTMP 推流地址
            $url2 = \Qiniu\Pili\RTMPPublishURL($push, $hubName, $streamKey, $time, $ak, $sk);
            $url_a = explode('/', $url2);
            $url = [
                'cdn' => urlencode($url_a[0] . '//' . $url_a[2] . '/' . $url_a[3]),
                'stream' => urlencode($url_a[4]),
            ];
        } else {
            if ($ext == 'flv') {
                $pull = str_replace('pili-live-rtmp', 'pili-live-hdl', $pull);
                //HDL 直播地址
                $url = \Qiniu\Pili\HDLPlayURL($pull, $hubName, $streamKey);
            } else if ($ext == 'm3u8') {
                $pull = str_replace('pili-live-rtmp', 'pili-live-hls', $pull);
                //HLS 直播地址
                $url = \Qiniu\Pili\HLSPlayURL($pull, $hubName, $streamKey);
            } else {
                //RTMP 直播放址
                $url = \Qiniu\Pili\RTMPPlayURL($pull, $hubName, $streamKey);
            }
        }

        return $url;
    }

    /**
     * @desc 网宿推拉流
     * @param string $host 协议，如:http、rtmp
     * @param string $stream 流名,如有则包含 .flv、.m3u8
     * @param int $type 类型，0表示播流，1表示推流
     * @return string
     */
    private function getStreamWs($host, $stream, $type)
    {
        $configpri = Func::getPrivateConfig();
        if ($type == 1) {
            $domain = $host . '://' . $configpri['ws_push'];
            $filename = "/" . $configpri['ws_apn'];
            $url = [
                'cdn' => urlencode($domain . $filename),
                'stream' => urlencode($stream),
            ];
        } else {
            $domain = $host . '://' . $configpri['ws_pull'];
            $filename = "/" . $configpri['ws_apn'] . "/" . $stream;
            $url = $domain . $filename;
        }

        return $url;
    }

    /**
     * 是否已经付过费用
     * @param $uid
     * @param $touid
     * @param $liveType
     * @param $liveId
     * @param $stream
     * @return bool|mixed
     */
    public function isRoomCharged($uid, $touid, $liveType, $liveId, $stream = '')
    {
        $where = [
            'type' => 'expend',
            'action' => 'roomcharge',
            'uid' => $uid,
            'touid' => $touid,
            'live_type' => $liveType,
            'liveid' => $liveId,
        ];
        if ($liveType == 0) {
            $where['stream'] = $stream;
            return M('users_coinrecord')->where($where)->find();
        } elseif ($liveType == 1) {
            return M('users_coinrecord')->where($where)->find();
        } else {
            return true;
        }
    }

    public function checkRoomPassword($liveType, $liveId, $password)
    {
        if (!in_array($liveType, [0, 1])) {
            $this->setError('直播类型错误');
            return false;
        }
        if (!$liveId || !$password) {
            $this->setError('缺失参数');
            return false;
        }

        if ($liveType == 0) {
            $live = M('users_live')->where(['uid' => $liveId])->find();
            if (!$live) {
                $this->setError('直播间已关闭');
                return false;
            }
            if ($live['type_val'] == $password) {
                return true;
            }
        } elseif ($liveType == 1) {
            $roomPass = MultiLive::getInstance()->getMultiShowExtData($liveId);
            $roomPass = $roomPass ? $roomPass['room_password'] : '';
            if ($roomPass == $password) {
                return true;
            }
        }

        $this->setError('密码错误');
        return false;
    }

    /**
     * 清除APP缓存
     * @return int
     */
    public function delSlideLiveList()
    {
        CRedis::getInstance()->del('app_slide_live_v2');
        return CRedis::getInstance()->del(self::KEY_SLIDE_LIVE_LIST);
    }

    /**
     * 暂时不处理登陆用户
     * @param $liveType
     * @param $liveId
     * @param $uid
     * @return bool|int
     */
    public function setDailySendMsgNum($liveType, $liveId, $uid)
    {
        if (!in_array($liveType, [0, 1])) {
            $this->setError('直播类型错误');
            return false;
        }
        if (!$liveId || !$uid) {
            $this->setError('缺失参数');
            return false;
        }
        if (!User::getInstance()->isVisitor($uid)) {
            return true;
        }

        $key = $this->getDailySendMsgNumKey($liveType, $liveId, $uid);
        CRedis::getInstance()->incr($key);
        CRedis::getInstance()->expire($key, 2 * 24 * 3600);

        return true;
    }

    public function getDailySendMsgNum($liveType, $liveId, $uid)
    {
        return (int)CRedis::getInstance()->get($this->getDailySendMsgNumKey($liveType, $liveId, $uid));
    }

    /**
     * 每日数据
     * @param $liveType
     * @param $liveId
     * @param $uid
     * @return string
     */
    private function getDailySendMsgNumKey($liveType, $liveId, $uid)
    {
        return self::DAILY_SEND_MSG_NUM . date('Y-m-d') . $liveType . $liveId . $uid;
    }

    /**
     * 私聊权限  @see http://10.71.42.71/showdoc/index.php?s=/1&page_id=26
     * @param $uid
     * @param $touid
     * @param $liveType
     * @param $liveId
     * @return mixed
     */
    public function getPrivateChatPermission($uid, $touid, $liveType, $liveId)
    {
        if (!$uid || !$touid || !$liveId || !in_array($liveType, [0, 1])) {
            $this->setError('缺参数');
            return false;
        }

        if ($uid == $touid) {
            $this->setError('你没有私信权限');
            return false;
        }

        //user vest_id
        $uidVestId = $this->getUserVestId($uid, $liveType, $liveId);
        $touidVestId = $this->getUserVestId($touid, $liveType, $liveId);
        //user vest info
        $userVest = $this->getUsersVest($uidVestId);
        $touserVest = $this->getUsersVest($touidVestId);

        if ($userVest && $userVest['private_chat'] != 1) {
            $this->setError('你没有私信权限');
            return false;
        }
        if ($touserVest && $touserVest['private_chat'] != 1) {
            $this->setError('对方没有私信权限');
            return false;
        }

        if ($uidVestId <= 0) {
            if ($touidVestId <= 1) {
                $this->setError('没有私信权限');
                return false;
            }
        } elseif ($uidVestId == 1) {
            if ($touidVestId <= 1) {
                $this->setError('没有私信权限');
                return false;
            }
        }

        return true;
    }

    public function getUsersVest($id, $forceUpdate = false)
    {
        if (!$id) {
            return false;
        }

        $redis = CRedis::getInstance();
        $key = $this->getUsersVestKey($id);
        $value = $redis->get($key);
        if ($value && !$forceUpdate) {
            return json_decode($value, true);
        }

        $value = M('users_vest')->where(['id' => $id])->find();
        if ($value) {
            $redis->set($key, json_encode($value), 24 * 3600);
        }

        return $value;
    }

    public function getVestLists($forceUpdate = false)
    {
        $key = self::KEY_CACHE_USERS_VEST;
        $redis = CRedis::getInstance();
        $list = $redis->get($key);
        if ($list && !$forceUpdate) {
            return json_decode($list, true);
        }

        $list = M('users_vest')->select() ?: [];
        foreach ($list as &$v) {
            $v['vest_man_url'] = Func::getUrl($v['vest_man_url']);
            $v['vest_woman_url'] = Func::getUrl($v['vest_woman_url']);
        }
        if ($list) {
            $redis->set($key, json_encode($list), 3600);
        }

        return $list;
    }

    private function getUsersVestKey($id)
    {
        return "cache:uv:{$id}";
    }

    /**
     * 获取用户vest_id
     * @param $uid
     * @param $liveType
     * @param $liveId
     * @return int
     */
    public function getUserVestId($uid, $liveType, $liveId)
    {
        if ($liveId) {
            $userVestList = $this->getUserVest($uid, $liveType, $liveId);
            if ($userVestList) {
                return (int)$userVestList['vestid'];
            }
        }

        $user = User::getInstance()->getUserInfo($uid);
        if (!$user) {
            return 0;
        }

        return (int)$user['vest_id'];
    }

}
