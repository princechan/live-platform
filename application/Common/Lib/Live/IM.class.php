<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Live;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;

class IM extends Service
{
    const KEY_CACHE_TIME = 3600;
    const KEY_CACHE_GROUP = 'cache:group:';
    const KEY_CACHE_MY_GROUP = 'cache:my_group:';
    const KEY_CACHE_MY_FRIEND = 'cache:my_friend:';

    /**
     * 消息类型
     */
    const ADD_USER_MSG = 1;//为请求添加用户
    const ADD_USER_SYS = 2;//为系统消息（添加好友）
    const ADD_GROUP_MSG = 3;//为请求加群
    const ADD_GROUP_SYS = 4;//为系统消息（添加群）
    const ADD_ADMIN = 6;//为添加管理
    const REMOVE_ADMIN = 7;//为取消管理
    const ALLUSER_SYS = 5;// 全体会员消息
    /**
     * 消息状态
     */
    const UNREAD = 1;//未读
    const AGREE_BY_TO = 2;//同意
    const DISAGREE_BY_TO = 3;//拒绝
    const AGREE_BY_FROM = 4;//同意且返回消息已读
    const DISAGREE_BY_FROM = 5;//拒绝且返回消息已读
    const READ = 6;//全体消息已读

    /**
     * 表
     */
    private static $tb_skin = 'tb_skin';
    private static $tb_msg = 'tb_msg';
    private static $tb_chatlog = 'tb_chatlog';
    private static $tb_group = 'tb_group';
    private static $tb_my_friend = 'tb_my_friend';
    private static $tb_my_group = 'tb_my_group';
    private static $tb_group_member = 'tb_group_member';

    /**
     * 记录聊天记录
     */
    public function addChatLog($uid, $touid, $content, $sendTime, $type, $sysLog)
    {
        $uid = User::getInstance()->getRUid($uid);
        $touid = User::getInstance()->getRUid($touid);
        $user = User::getInstance()->getUserInfo($uid);
        $touser = User::getInstance()->getUserInfo($touid);

        $res['code'] = -1;
        $res['msg'] = '';
        if (!$user || !$touser) {
            return $res;
        }

        //管理员才能主动发起聊天，直接添加注册用户为好友
        if ($uid > 0) {
            if ($user['vest_id'] < 6) {
                return $res;
            } else {
                if ($touid > 0) {
                    $rs = $this->initMyGroupMyFriend($uid, $touid, $touser['user_nicename']);
                    if (false === $rs) {
                        $res['msg'] = $this->getLastErrMsg();
                        return $res;
                    }
                    $rs = $this->initMyGroupMyFriend($touid, $uid, $user['user_nicename']);
                    if (false === $rs) {
                        $res['msg'] = $this->getLastErrMsg();
                        return $res;
                    }
                }
            }
        }

        //游客只能跟管理员聊
        if ($uid < 0) {
            if ($touser['vest_id'] < 6) {
                return $res;
            }
        }

        $data['to'] = $touid;
        $data['content'] = $content;
        $data['sendTime'] = $sendTime;
        $data['type'] = $type;
        $data['from'] = $uid;
        if (!$data['from']) {
            $res['code'] = -1;
            return $res;
        }
        if ($sysLog) {
            $data['from'] = 0;
        }
        $data['from'] = $uid;
        $success = MIM('chatlog')->add($data);
        if ($success) {
            $res['code'] = 0;
        }

        return $res;
    }

    /**
     * 初始化我的分组
     */
    private function initMyGroupMyFriend($uid, $touid, $tousername, $name = '我的好友')
    {
        if (!$uid || !$touid) {
            $this->setError('缺参数');
            return false;
        }

        $myGroup = $this->getMyGroup($uid, $name);
        if (!$myGroup) {
            MIM('my_group')->add([
                'memberIdx' => $uid,
                'mygroupName' => $name,
            ]);
        }

        $myGroup = $this->getMyGroup($uid, $name);
        if (!$myGroup) {
            $this->setError('初始化我的分组失败');
            return false;
        }

        $myFriend = $this->getMyFriend($myGroup['mygroupIdx'], $touid);
        if (!$myFriend) {
            MIM('my_friend')->add([
                'mygroupIdx' => $myGroup['mygroupIdx'],
                'memberIdx' => $touid,
                'nickName' => $tousername,
            ]);
        }
        $myFriend = $this->getMyFriend($myGroup['mygroupIdx'], $touid);
        if (!$myFriend) {
            $this->setError('添加我的好友失败');
            return false;
        }

        return true;
    }

    public function getMyGroup($uid, $name, $forceUpdate = false)
    {
        $key = $this->getMyGroupCacheKey($uid . $name);
        $redis = CRedis::getInstance();
        $rs = $redis->get($key);
        if ($rs && !$forceUpdate) {
            return json_decode($rs, true);
        }

        $rs = MIM('my_group')->where(['memberIdx' => $uid, 'mygroupName' => $name])->find();
        if ($rs) {
            $redis->set($key, json_encode($rs));
        }

        return $rs;
    }

    public function getMyFriend($myGroupId, $memberId, $forceUpdate = false)
    {
        $key = $this->getMyFriendCacheKey($myGroupId . $memberId);
        $redis = CRedis::getInstance();
        $rs = $redis->get($key);
        if ($rs && !$forceUpdate) {
            return json_decode($rs, true);
        }

        $rs = MIM('my_friend')->where(['mygroupIdx' => $myGroupId, 'memberIdx' => $memberId])->find();
        if ($rs) {
            $redis->set($key, json_encode($rs));
        }

        return $rs;
    }

    /**
     * 好友请求已通过
     */
    public function subscribed($uid, $touid)
    {
        $res['code'] = -1;
        $res['msg'] = '';
        $res['data'] = '参数错误';
        if (!$uid || !$touid) {
            return $res;
        }

        $touser = User::getInstance()->getUserInfo($touid);
        $msg = MIM('msg')->where(['from' => $uid, 'to' => $touid])->find();
        if ($msg) {
            MIM('my_friend')->add([
                'mygroupIdx' => $msg['mygroupIdx'],
                'memberIdx' => $touid,
            ]);
        }

        $data = [];
        if ($touser) {
            $data = [
                'memberIdx' => $touser['id'],
                'memberName' => $touser['user_nicename'],
                'signature' => $touser['signature'],
                'memberSex' => $touser['sex'],
                'mygroupIdx' => $msg ? $msg['mygroupIdx'] : 0,
            ];
        }

        $res['code'] = 0;
        $res['msg'] = '';
        $res['data'] = $data;

        return $res;
    }

    /**
     * 请求添加，好友请求
     */
    public function addMsg($uid, $touid, $msgType, $mygroupIdx, $remark)
    {
        $uid = User::getInstance()->getRUid($uid);
        $touid = User::getInstance()->getRUid($touid);
        $user = User::getInstance()->getUserInfo($uid);
        $touser = User::getInstance()->getUserInfo($touid);

        $res['code'] = -1;
        $res['msg'] = '';
        $res['data'] = -1;
        if (!$uid || !$touid) {
            return $res;
        }

        $data['from'] = $uid;
        $data['to'] = $touid;
        $data['msgType'] = $msgType;
        $data['remark'] = $remark;
        $data['mygroupIdx'] = $mygroupIdx;
        $data['sendTime'] = $data['time'] = time();
        $data['status'] = 1;
        //发出的申请是否已存在
        $msgIdx = MIM('msg')->field('msgIdx')->where(['from' => $uid, 'to' => $touid,])->find();
        if (empty($msgIdx['msgIdx'])) {
            $success = MIM('msg')->add($data);
        } else {
            $success = MIM('msg')->where(['msgIdx' => $msgIdx['msgIdx']])->save($data);
        }

        //直接加好友
        if ($msgType == 1) {
            //管理员才能主动发起聊天，直接添加注册用户为好友
            if ($uid > 0) {
                if ($user['vest_id'] >= 6 && $touid > 0) {
                    $rs = $this->initMyGroupMyFriend($uid, $touid, $touser['user_nicename']);
                    if (false === $rs) {
                        $res['msg'] = $this->getLastErrMsg();
                        return $res;
                    }
                    $rs = $this->initMyGroupMyFriend($touid, $uid, $user['user_nicename']);
                    if (false === $rs) {
                        $res['msg'] = $this->getLastErrMsg();
                        return $res;
                    }
                }
            }
        }

        $res['code'] = 0;
        $res['msg'] = '';
        $res['data'] = $success;

        return $res;
    }

    /**
     * 首页数据
     */
    public function getUserData($uid)
    {
        $uid = User::getInstance()->getRUid($uid);
        //获取默认皮肤
        $get_skin = MIM('skin')->where(['memberIdx' => $uid])->find();
        //获取未读消息数量
        $tbMsg = self::$tb_msg;
        $sql_msg = "SELECT COUNT(*) as count from $tbMsg where (`to` = $uid AND (msgType = " . self::ADD_USER_MSG . " OR msgType = " . self::ADD_GROUP_MSG . ") AND status = " . self::UNREAD . " ) 
        OR ( `from` = $uid AND (msgType = " . self::ADD_USER_SYS . " OR msgType = " . self::ADD_GROUP_SYS . " ) AND (status = " . self::AGREE_BY_TO . " OR status = " . self::DISAGREE_BY_TO . ") )";
        $msgBox = M()->query($sql_msg);
        $count = $msgBox && !empty($msgBox[0]) ? $msgBox[0]['count'] : 0;

        $res['code'] = 0;
        $res['msg'] = "";
        $res['data']['mine'] = $this->getMineInfo($uid);
        $res['data']['friend'] = $this->getMyFriendList($uid);
        $res['data']['group'] = [];
        // $res['data']['mine']['skin'] = $get_skin['isUserUpload'] == 1 ? "./uploads/skin/" . $get_skin['url'] : "/public/js/imPackage/lib/layui/css/modules/layim/skin" . $get_skin['url'];
        $res['data']['mine']['msgBox'] = $count;
        return $res;
    }

    public function getMyFriendList($memberIdx)
    {
        if ($memberIdx <= 0) {
            return [];
        }

        $myGroups = MIM('my_group')->field('mygroupIdx,mygroupName groupname')
            ->where(['memberIdx' => $memberIdx])->order('weight ASC')->select();
        if (!$myGroups) {
            return [];
        }

        $myFriends = MIM('my_friend')->where(['mygroupIdx' => ['in', array_column($myGroups, 'mygroupIdx')]])->select() ?: [];
        $users = User::getInstance()->getUsersInfo(array_column($myFriends, 'memberIdx'));
        $myFriendsInGroup = [];
        foreach ($myFriends as $value) {
            $myFriendsInGroup[$value['mygroupIdx']][] = $value;
        }

        foreach ($myGroups as &$g) {
            $g['id'] = $g['mygroupIdx'];
            $g['list'] = [];
            if (empty($myFriendsInGroup[$g['id']])) {
                continue;
            }

            $friends = $myFriendsInGroup[$g['id']];
            foreach ($friends as &$f) {
                if (empty($users[$f['memberIdx']])) {
                    continue;
                }

                $user = $users[$f['memberIdx']];
                $f['id'] = $f['memberIdx'];
                $f['nickName'] = $user['user_nicename'];
                $f['username'] = $user['user_nicename'];
                $f['avatar'] = $user['avatar'];
                $f['sign'] = $user['signature'];
            }
            $g['list'] = $friends;
        }

        return $myGroups;
    }

    private function getMineInfo($uid)
    {
        $user = User::getInstance()->getUserInfo($uid);
        if (!$user) {
            return [];
        }

        return [
            'id' => $user['id'],
            'sign' => $user['signature'],
            'username' => $user['user_nicename'],
            'avatar' => $user['avatar'],
            'oauth_token' => '',
            'easemob_token' => '',
            'access_token' => '',
        ];
    }

    public function getImUserInfo($uid)
    {
        return User::getInstance()->getUserInfo($uid);
    }

    /**
     * 获取聊天总的条数
     */
    public function getChatLogTotal($memberIdx, $id, $type, $limit = 20)
    {
        $memberIdx = User::getInstance()->getRUid($memberIdx);
        $tbChatLog = self::$tb_chatlog;
        if ($type == 'friend') {
            $sql = "SELECT COUNT(*) as count from $tbChatLog 
where ((`to` = $memberIdx AND `from` = $id ) OR (`to` = $id AND `from` = $memberIdx) AND status = 1 AND `type`='$type')";
        } elseif ($type == 'group') {
            $sql = "SELECT COUNT(*) as count from $tbChatLog where `to` = $id AND status = 1 AND `type`='$type'";
        } else {
            $sql = '';
        }

        $rs = $sql ? M()->query($sql) : [];
        $count = 0;
        if ($rs && !empty($rs[0])) {
            $count = $rs[0]['count'];
        }

        $res['code'] = 0;
        $res['count'] = "";
        $res['data']['count'] = $count;
        $res['data']['limit'] = $limit; //每页显示数量

        return $res;
    }

    /**
     * 聊天记录
     */
    public function getChatLog($memberIdx, $id, $type, $page = 1, $rows)
    {
        $memberIdx = User::getInstance()->getRUid($memberIdx);

        $tbChatLog = self::$tb_chatlog;
        $select_from = ($page - 1) * $rows;
        if ($type == 'friend') {
            $sql = "SELECT `content`, `sendTime` `timestamp`, `from`, `to` FROM $tbChatLog     
WHERE ((`to` = $memberIdx AND `from` = $id) OR (`to` = $id AND `from` = $memberIdx) AND `status` = 1 AND `type`='$type') LIMIT $select_from ,$rows";
        } elseif ($type == 'group') {
            $sql = "SELECT `content`, `sendTime` `timestamp`, `from`, `to` FROM $tbChatLog 
WHERE `to` = $id AND status=1 AND `type`='$type' LIMIT $select_from , $rows";
        } else {
            $sql = '';
        }

        $list = $sql ? M()->query($sql) : [];
        switch ($type) {
            case 'friend':
                $list = $this->getFriendChatLog($list);
                break;
            case 'group':
                $list = $this->getGroupChatLog($list);
                break;
            default:
                $list = [];
        }

        return [
            'code' => 0,
            'count' => '',
            'data' => $list,
        ];
    }

    public function getFriendChatLog(array $list)
    {
        $uids = [];
        $tmpUsers = [];
        foreach ($list as $li) {
            if ($li['from'] > 0) {
                $uids[] = $li['from'];
            } else {
                $tmpUsers[$li['from']] = User::getInstance()->getUserInfo($li['from']);
            }
            if ($li['to'] > 0) {
                $uids[] = $li['to'];
            } else {
                $tmpUsers[$li['to']] = User::getInstance()->getUserInfo($li['to']);
            }
        }
        $users = User::getInstance()->getUsersInfo($uids);
        foreach ($list as &$value) {
            $value['fromName'] = '';
            $value['toName'] = '';
            if (isset($users[$value['from']])) {
                $value['fromName'] = $users[$value['from']]['user_nicename'];
            } elseif (isset($tmpUsers[$value['from']])) {
                $value['fromName'] = $tmpUsers[$value['from']]['user_nicename'];
            }
            if (isset($users[$value['to']])) {
                $value['toName'] = $users[$value['to']]['user_nicename'];
            } elseif (isset($tmpUsers[$value['to']])) {
                $value['toName'] = $tmpUsers[$value['to']]['user_nicename'];
            }

            unset($value['from'], $value['to']);
        }

        return $list;
    }

    public function getGroupChatLog(array $list)
    {
        $gids = [];
        $tmpUsers = [];
        foreach ($list as $li) {
            if ($li['from'] > 0) {
                $gids[] = $li['from'];
            } else {
                $tmpUsers[$li['from']] = User::getInstance()->getUserInfo($li['from']);
            }
            if ($li['to'] > 0) {
                $gids[] = $li['to'];
            } else {
                $tmpUsers[$li['to']] = User::getInstance()->getUserInfo($li['to']);
            }
        }
        $users = $this->getGroupsInfo($gids);
        foreach ($list as &$value) {
            $value['fromName'] = '';
            $value['toName'] = '';
            if (isset($users[$value['from']])) {
                $value['fromName'] = $users[$value['from']]['groupName'];
            } elseif (isset($tmpUsers[$value['from']])) {
                $value['fromName'] = $tmpUsers[$value['from']]['user_nicename'];
            }
            if (isset($users[$value['to']])) {
                $value['toName'] = $users[$value['to']]['groupName'];
            } elseif (isset($tmpUsers[$value['to']])) {
                $value['toName'] = $tmpUsers[$value['to']]['user_nicename'];
            }

            unset($value['from'], $value['to']);
        }

        $res['code'] = 0;
        $res['count'] = '';
        $res['data'] = $list;

        return $res;
    }

    /**
     * 获取组信息
     * @param array $ids
     * @return array|bool|mixed|string
     */
    public function getGroupsInfo(array $ids)
    {
        if (!$ids) {
            return false;
        }

        $redis = CRedis::getInstance();
        $key = $this->getGroupCacheKey($ids);
        $list = $redis->get($key);
        if ($list) {
            return json_decode($list, true);
        }

        $list = MIM('group')->where(['groupIdx' => ['in', $ids]])->select() ?: [];
        $list = $list ? Func::index($list, 'groupIdx') : [];
        if ($list) {
            $redis->set($key, json_encode($list), self::KEY_CACHE_TIME);
        }

        return $list;
    }

    private function getGroupCacheKey($id)
    {
        return self::KEY_CACHE_GROUP . (is_array($id) ? json_encode($id) : $id);
    }

    private function getMyGroupCacheKey($id)
    {
        return self::KEY_CACHE_MY_GROUP . (is_array($id) ? json_encode($id) : $id);
    }

    private function getMyFriendCacheKey($id)
    {
        return self::KEY_CACHE_MY_FRIEND . (is_array($id) ? json_encode($id) : $id);
    }

    /**
     * 好友或群总的条数
     */
    public function findFriendTotal($type, $value, $limit)
    {
        $tbUser = 'cmf_users';
        $tbGroup = self::$tb_group;
        if ($type == 'friend') {//好友
            $sql = "select COUNT(*) as count from $tbUser where (id LIKE '%{$value}%' OR user_nicename LIKE '%{$value}%')";
        } else {
            $sql = "select COUNT(*) as count from $tbGroup where (groupIdx LIKE '%{$value}%' OR groupName LIKE '%{$value}%')";
        }

        $count = M()->query($sql);
        if ($count && !empty($count[0])) {
            $res['code'] = 0;
        } else {
            $res['code'] = -1;
        }
        $res['count'] = "";
        $res['data']['count'] = $count[0]['count'];
        $res['data']['limit'] = $limit; //每页显示数量

        return $res;
    }

    /**
     * 查找好友或群
     */
    public function findFriend($type, $value, $page, $rows)
    {
        $select_from = ($page - 1) * $rows;
        $tbUser = 'cmf_users';
        $tbGroup = self::$tb_group;
        if ($type == 'friend') {//好友
            $sql = "select id memberIdx,user_nicename memberName,signature, sex memberSex from $tbUser 
where (id LIKE '%{$value}%' OR user_nicename LIKE '%{$value}%') limit $select_from , $rows";
        } else {
            $sql = "select groupIdx,groupName,des,number,approval from $tbGroup 
where (groupIdx LIKE '%{$value}%' OR groupName LIKE '%{$value}%') limit " . $select_from . ',' . $rows;
        }
        $get_friend = M()->query($sql);
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data'] = $get_friend;

        return $res;
    }

    /**
     * 删除好友
     */
    public function removeFriends($memberIdx, $friendId)
    {
        $memberIdx = User::getInstance()->getRUid($memberIdx);
        $friendId = User::getInstance()->getRUid($friendId);

        //从我的好友列表删除
        $myGroupList = MIM('my_group')->where(['memberIdx' => $memberIdx])->select();
        if ($myGroupList) {
            MIM('my_friend')->where([
                'mygroupIdx' => ['in', array_column($myGroupList, 'mygroupIdx')],
                'memberIdx' => $friendId,
            ])->delete();
        }

        //从好友列表删除我
        $myGroupList = MIM('my_group')->where(['memberIdx' => $friendId])->select();
        if ($myGroupList) {
            MIM('my_friend')->where([
                'mygroupIdx' => ['in', array_column($myGroupList, 'mygroupIdx')],
                'memberIdx' => $memberIdx,
            ])->delete();
        }

        $tbMsg = self::$tb_msg;
        //删除消息记录
        M()->execute("DELETE FROM `{$tbMsg}` WHERE (`from` = $friendId AND `to` = $memberIdx) OR (`from` = $memberIdx AND `to` = $friendId)");

        $res['code'] = 0;
        $res['msg'] = '修改成功';
        $res['data'] = '';

        return $res;
    }

    /**
     * 获取好友信息
     */
    public function getOneUserData($uid)
    {
        $user = User::getInstance()->getUserInfo($uid);
        $data = [];
        if ($user) {
            $data = [
                'memberIdx' => $uid,
                'memberName' => $user['user_nicename'],
                'signature' => $user['signature'],
                'memberSex' => $user['sex'],
            ];
        }

        $res['code'] = 0;
        $res['msg'] = '';
        $res['data'] = $data;

        return $res;
    }

    public function userStatus()
    {
        $id = $_GET['id'];
        $access_token = $this->getUserToken($id);
        if ($access_token) {
            $authorization = 'Bearer ' . $access_token['access_token'];
            $headers = ['Authorization:' . $authorization];
            $url_friend = $BASEURL . 'users/' . $id . '/status';
            $online = json_decode(Get($headers, $url_friend), true);
            $res['code'] = 0;
            $res['msg'] = '';
            $res['data'] = $online['data'][$id];
        } else {
            $res['code'] = -1;
            $res['msg'] = '';
            $res['data'] = '';
        }

        return $res;
    }

    private function getUserToken($memberIdx)
    {
        $get_user = MIM('person')->field('easemob_token,loginTime,expires_in')->where(['memberIdx' => $memberIdx])->find();
        if ($get_user['loginTime'] && $get_user['expires_in']) {
            $lastTime = $get_user['loginTime'] + $get_user['expires_in'] - 3600 * 24 * 5;//还有5天则更新token
        } else {
            $lastTime = 1;
        }
        $time = time();
        if ($time >= $lastTime) {//token失效
            $url = 'http://a1.easemob.com/1199170801115017/layim/token';
            $pwd = md5($memberIdx . 'easemob');
            $data = ['grant_type' => 'password', 'password' => $pwd, 'username' => $memberIdx];
            $json_data = json_encode($data);
            $info = Post($json_data, $url);
            $user_token = json_decode($info, true);
            $data_token['easemob_token'] = $user_token['access_token'];
            $data_token['loginTime'] = $time;
            $data_token['expires_in'] = $user_token['expires_in'];
            MIM('person')->where(['memberIdx' => $memberIdx])->save($data_token);//更新token
        } else {
            $user_token['access_token'] = $get_user['easemob_token'];
        }

        return $user_token;
    }

    public function uploadImage()
    {
        $url = $BASEURL . 'chatfiles';
        $headers = ['Content-type: multipart/form-data', 'Authorization:Bearer ' . $_SESSION['info']['access_token'], 'restrict-access: true'];
        $file = file_get_contents($_FILES['file']['tmp_name']);
        $Upload_data = ['file' => $file];
        $data_info = Post($Upload_data, $url, $headers);
        echo '{"code": 0 ,"msg": ""  ,"data": ' . $data_info . '}';
    }

    public function uploadFile()
    {
        $auth = new Auth(AK, SK);
        $bucket = 'lite-im';
        // 生成上传Token
        $token = $auth->uploadToken($bucket);
        // 要上传文件的本地路径
        $file = $_FILES['file'];
        $uploadfile = $file['tmp_name'];
        // 上传到七牛后保存的文件名
        $key = $file['name'];
        // 初始化 UploadManager 对象并进行文件的上传。
        $uploadMgr = new UploadManager();
        // 调用 UploadManager 的 putFile 方法进行文件的上传。
        list($ret, $err) = $uploadMgr->putFile($token, $key, $uploadfile);
        if ($err !== null) {
            $res['code'] = -1;
            $res['msg'] = $err;
            $res['data'] = '';
        } else {
            $res['code'] = 0;
            $res['msg'] = "";
            $res['data']['src'] = QN_FILE . $ret['key'];
            $res['data']['name'] = $key;
        }

        return $res;
    }

    //全部群成员
    public function groupMembers($groupId, $uid)
    {
        if (!$groupId || !$uid) {
            $this->setError('缺参数');
            return false;
        }

        $groupMember = MIM('group_member')->where(['groupidx' => $groupId, 'status' => 1])
            ->order('`type` ASC, groupmemberidx ASC')->select();
        if (!$groupMember) {
            return [];
        }

        $list = [];
        $users = User::getInstance()->getUsersInfo(array_column($groupMember, 'memberidx'));
        foreach ($groupMember as $value) {
            if (empty($users[$value['memberidx']])) {
                continue;
            }
            $user = $users[$value['memberidx']];
            $value['avatar'] = $user['avatar'];
            $value['user_nicename'] = $user['user_nicename'];
            $list[] = $value;
        }

        $myGroup = MIM('my_group')->field('mygroupidx')->where(['memberidx' => $uid])->select();
        $myFriend = $myGroup ? MIM('my_friend')->field('memberidx')
            ->where(['mygroupidx' => ['in', array_column($myGroup, 'mygroupidx')]])->select() : [];
        foreach ($list as &$li) {
            $li['friendship'] = in_array($li[''], $myFriend) ? 1 : 0;
        }

        return $list;
    }

    public function changeSignature($uid, $signature)
    {
        $data['signature'] = $_POST['sign'];
    }

    public function uploadSkin()
    {
        $memberIdx = $_SESSION['info']['id'];
        $isSetSkin = $PdoMySQL->find(self::$tb_skin, 'memberIdx = "' . $memberIdx . '"', 'url,isUserUpload');
        $file = $_FILES['file'];
        $ext = explode('.', $file['name']);
        if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png') {
            echo '{"code":"9999","status":"n","info":"请上传格式为jpg或png的图片"}';
        }
        $data['url'] = $memberIdx . '_' . time() . '.' . $ext[count($ext) - 1];
        move_uploaded_file($file['tmp_name'], '../uploads/skin/' . $data['url']);
        $data['isUserUpload'] = '1';
        if ($isSetSkin) {
            if ($isSetSkin['isUserUpload'] == 1) {
                unlink('../uploads/skin/' . $isSetSkin['url']);
            }
            $success = $PdoMySQL->update($data, self::$tb_skin, 'memberIdx = "' . $memberIdx . '"');
        } else {
            $data['memberIdx'] = $memberIdx;
            $success = $PdoMySQL->add($data, self::$tb_skin);
        }
        $res['data']['src'] = '/uploads/skin/' . $data['url'];
        $res['code'] = 0;
        $res['msg'] = "";

        return $res;
    }

    public function systemSkin()
    {
        $memberIdx = $_SESSION['info']['id'];
        $type = $_POST['type'];
        $isSetSkin = $PdoMySQL->find(self::$tb_skin, 'memberIdx = "' . $memberIdx . '"', 'url');
        $skin = explode('/', $type);
        $data['url'] = $skin[count($skin) - 1];
        $data['isUserUpload'] = 0;

        if ($isSetSkin) {
            $success = $PdoMySQL->update($data, self::$tb_skin, 'memberIdx = "' . $memberIdx . '"');
        } else {
            $data['memberIdx'] = $memberIdx;
            $success = $PdoMySQL->add($data, self::$tb_skin);
        }
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data']['src'] = $type;

        return $res;
    }

    /**
     * 获取默认好友推荐
     */
    public function getRecommend()
    {
        $sql = "select memberIdx,memberName,signature,birthday,memberSex from cmf_users order by rand() limit 16 ";
        $get_recommend = $PdoMySQL->getAll($sql);
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data'] = $get_recommend;

        return $res;
    }

    /**
     * 获取好友资料
     */
    public function getInformation()
    {
        $id = $_GET['?id'];//好友/群 id
        $type = $_GET['type'];//当前类型
        if ($type == 'friend') {//好友
            $sql = "select memberIdx,memberName,memberSex,birthday,signature,emailAddress,phoneNumber,blood_type,job,qq,wechat from cmf_users where memberIdx = $id";
        }
        $getInformation = $PdoMySQL->getRow($sql);
        $getInformation['type'] = $type;
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data'] = $getInformation;

        return $res;
    }

    public function userMaxGroupNumber()
    {
        //判断用户最大建群数
        $memberIdx = $_SESSION['info']['id'];
        if (!$memberIdx) {
            exit();
        }
        $sql = "select count(*) as cn from tb_group where belong = '{$memberIdx}' ";
        $cn = $PdoMySQL->getRow($sql);
        if ($cn['cn'] < 6) {//最多建5个群
            $res['code'] = 0;
            $res['msg'] = "";
            $res['data'] = $groupIdx;
        } else {
            $res['code'] = -1;
            $res['msg'] = "超过最大建群数 5";
            $res['data'] = '';
        }

        return $res;
    }

    public function commitGroupInfo()
    {
        //提交建群信息
        $data_group['memberIdx'] = $data['belong'] = $_SESSION['info']['id'];
        $data_group['groupIdx'] = $data['groupIdx'] = $_GET['groupIdx'];
        $data['groupName'] = $_GET['groupName'];
        $data['des'] = $_GET['des'];
        $data['number'] = $_GET['number'];
        $data['approval'] = $_GET['approval'];
        $groupIdx = $PdoMySQL->find(self::$tb_group, 'groupIdx ="' . $data['groupIdx'] . '" OR groupName = "' . $data['groupName'] . '"', 'groupIdx');
        $sql = "select count(*) as cn from tb_group where belong = '{$data['belong']}' ";
        $cn = $PdoMySQL->getRow($sql);
        if (!$groupIdx['groupIdx'] && $cn['cn'] < 6) {//最多建5个群
            $success = $PdoMySQL->add($data, self::$tb_group);
            $data_group['addTime'] = time();
            $data_group['type'] = 1;//群主
            $PdoMySQL->add($data_group, self::$tb_group_member);
            $res['code'] = 0;
            $res['msg'] = "群 " . $data['groupName'] . " 创建成功";
            $res['data'] = $groupIdx;
        } else {
            $res['code'] = -1;
            $res['msg'] = "群名称已存在或超过最大建群数 5";
            $res['data'] = '';
        }

        return $res;
    }

    public function get_one_group_data()
    {
        //获取默群信息
        $groupIdx = $_GET['groupIdx'];
        $group = $PdoMySQL->find(self::$tb_group, 'groupIdx = "' . $groupIdx . '"', 'groupName');
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data'] = $group;

        return $res;
    }

    public function getMsgBox()
    {
        //获取消息盒子
        $memberIdx = $_SESSION['info']['id'];
        $page = $_GET['page'];
        $rows = 10;//每页显示数量
        $select_from = ($page - 1) * $rows;
        $sql_msg = "select * from tb_msg where (`to` = " . $memberIdx . " OR `from` = " . $memberIdx . " OR find_in_set(" . $memberIdx . ", adminGroup) ) ORDER BY  time DESC  limit " . $select_from . ',' . $rows;
        $msgBox = $PdoMySQL->getAll($sql_msg);
        foreach ($msgBox as $key => &$value) {
            if ($value['msgType'] == self::ADD_USER_MSG || $value['msgType'] == self::ADD_USER_SYS) {
                if ($value['to'] == $memberIdx) {
                    $userId = $value['from'];//收到加好友消息（被添加者接收消息）
                } elseif ($value['from'] == $memberIdx) {
                    $userId = $value['to'];//收到系统消息(申请是否通过) 加好友消息（添加者接收消息）
                }
            }
            if ($value['msgType'] == self::ADD_GROUP_MSG || $value['msgType'] == self::ADD_GROUP_SYS) {//收到加群消息（群主接收消息）
                $userId = $value['from'];//发出消息的人
                $sql_msg = "select groupName from tb_group where groupIdx = " . $value['to'];
                $group = $PdoMySQL->getRow($sql_msg);
                $value['groupName'] = $group['groupName'];
                $value['groupIdx'] = $value['to'];
                if ($value['handle']) {
                    $username = User::getInstance()->getUserInfo($value['handle']);
                    $value['handle'] = $username ? $username['user_nicename'] : ''; //处理该请求的管理员
                }
            };
            if (!empty($userId)) {
                $username = User::getInstance()->getUserInfo($userId);
                $value['username'] = $username ? $username['user_nicename'] : '';
                $value['signature'] = $username ? $username['signature'] : '';
            } else {//平台发布消息
                $value['username'] = '平台发布';
            }
        }
        $sql_msg = "select COUNT(*) as count from tb_msg where (`to` = " . $memberIdx . " OR `from` = " . $memberIdx . " OR find_in_set(" . $memberIdx . ", adminGroup) )";
        $pages = $PdoMySQL->getRow($sql_msg);
        $res['code'] = 0;
        $res['pages'] = ceil($pages['count'] / $rows);
        $res['data'] = $msgBox;
        $res['memberIdx'] = $memberIdx;

        return $res;
    }

    public function add_admin_msg()
    {
        $data['from'] = $_GET['from'];
        $data['to'] = $_GET['to'];
        if (!$data['to'] || !$data['from']) {
            $res['code'] = -1;
            $res['msg'] = "";
            $res['data'] = -1;
            echo json_encode($res);
            exit();
        }
        $data['adminGroup'] = $_GET['adminGroup'];
        $msgIdx = $PdoMySQL->find(self::$tb_msg, '( `to` = "' . $data['to'] . '" AND `from` = "' . $data['from'] . '")', 'msgIdx,adminGroup'); //发出的申请是否已存在
        if ($msgIdx['msgIdx']) {
            if ($msgIdx['adminGroup'] && $msgIdx['adminGroup'] != $data['adminGroup']) {
                $data['adminGroup'] = $msgIdx['adminGroup'] . ',' . $data['adminGroup'];
            }
            $success = $PdoMySQL->update($data, self::$tb_msg, 'msgIdx = "' . $msgIdx['msgIdx'] . '"');
        }
        $res['code'] = 0;
        $res['msg'] = "";
        $res['data'] = $success;

        return $res;
    }

    public function set_allread()
    {
        //系统消息全部设置为已读
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = "select msgIdx,status from tb_msg where  ( `from` = " . $memberIdx . " AND ( `status` = " . self::AGREE_BY_TO . " OR `status` = " . self::DISAGREE_BY_TO . " ) ) ";
        $msgBox = $PdoMySQL->getAll($sql_msg);
        foreach ($msgBox as $key => $value) {
            $data['status'] = $value['status'] + 2;
            $username = $PdoMySQL->update($data, self::$tb_msg, 'msgIdx = "' . $value['msgIdx'] . '"');
        }
        $res['code'] = 0;
        $res['msg'] = "";

        return $res;
    }

    public function modify_msg()
    {
        //修改添加状态
        $data['msgType'] = $_GET['msgType'];
        $msgIdx = $_GET['msgIdx'];
        $status = $_GET['status'];
        $data['status'] = $status == self::AGREE_BY_TO ? self::AGREE_BY_TO : self::DISAGREE_BY_TO;
        $data_group_member['addTime'] = $data['readTime'] = $data['time'] = time();
        $memberIdx = $_SESSION['info']['id'];
        if ($data['msgType'] == 2) {//添加好友
            $friendIdx = $_GET['friendIdx'];//好友id
            $mygroupIdx = $_GET['mygroupIdx']; //好友分组
            $from = $PdoMySQL->find(self::$tb_msg, 'msgIdx =' . $msgIdx, 'from');
            if ($friendIdx != $from['from']) {
                $res['code'] = -1;
                $res['msg'] = "非法请求";
                $res['data'] = "";
                return $res;
            }

            $success = $PdoMySQL->update($data, self::$tb_msg, ' `to` = "' . $memberIdx . '"  AND `msgIdx` = "' . $msgIdx . '"');
            if ($success) {
                $isfriend = $PdoMySQL->find(self::$tb_my_friend, 'mygroupIdx =' . $mygroupIdx . ' AND memberIdx = ' . $friendIdx, 'myfriendIdx');
                if (!$isfriend['myfriendIdx']) {
                    $data_my_friend['mygroupIdx'] = $mygroupIdx;
                    $data_my_friend['memberIdx'] = $friendIdx;
                    $PdoMySQL->add($data_my_friend, self::$tb_my_friend);
                }
                $res['code'] = 0;
            } else {
                $res['code'] = -1;
            }
            $res['msg'] = "";
        } else {
            $data['handle'] = $memberIdx;
            $handle = $PdoMySQL->find(self::$tb_msg, 'msgIdx =' . $msgIdx, 'handle,to,from');
            if ($handle['handle']) {
                $res['msg'] = "群消息已处理";
                $res['code'] = 1;
            } else {
                $success = $PdoMySQL->update($data, self::$tb_msg, 'find_in_set("' . $memberIdx . '", adminGroup) AND `msgIdx` = "' . $msgIdx . '"');
                if ($success) {
                    $data_group_member['groupIdx'] = $handle['to'];
                    $data_group_member['memberIdx'] = $handle['from'];
                    $PdoMySQL->add($data_group_member, self::$tb_group_member);//加入群
                    $res['code'] = 0;
                    $res['msg'] = "群消息处理成功";
                } else {
                    $res['code'] = -1;
                    $res['msg'] = "群消息处理失败";
                }
            }
        }

        return $res;
    }

    public function addMyGroup()
    {
        //添加好友分组
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT count(*) AS count from tb_my_group where memberIdx = $memberIdx");
        $count = $PdoMySQL->getRow($sql_msg);
        if ($count['count'] >= 20) {
            $res['code'] = -1;
            $res['msg'] = '最多创建20个分组';
        } else {
            $data['memberIdx'] = $memberIdx;
            $data['mygroupName'] = '未命名';
            $data['weight'] = ($count['count'] + 1);
            $id = $PdoMySQL->add($data, self::$tb_my_group);
            $res['code'] = 0;
            $res['msg'] = "创建成功";
            $res['data']['name'] = $data['mygroupName'];
            $res['data']['id'] = $PdoMySQL->getLastInsertId();
        }

        return $res;
    }

    public function delMyGroup()
    {
        //删除分组
        $mygroupIdx = $_GET['mygroupIdx'];
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT count(*) AS count from tb_my_group where memberIdx = $memberIdx AND mygroupIdx = $mygroupIdx");
        $count = $PdoMySQL->getRow($sql_msg);
        if ($count['count']) {//存在分组
            $sql_msg = sprintf(" SELECT mygroupIdx from tb_my_group where memberIdx = $memberIdx");
            $default_group = $PdoMySQL->getRow($sql_msg); //获取第一个分组为默认分组
            $PdoMySQL->delete(self::$tb_my_group, "mygroupIdx=" . $mygroupIdx);
            $data_group['mygroupIdx'] = $default_group['mygroupIdx'];
            $PdoMySQL->update($data_group, self::$tb_my_friend, 'mygroupIdx = "' . $mygroupIdx . '"');
            $res['code'] = 0;
            $res['msg'] = "删除成功";
            $res['data'] = $default_group['mygroupIdx'];
        } else {
            $res['code'] = 0;
            $res['msg'] = "删除成功";
        }

        return $res;
    }

    public function editGroupName()
    {
        //编辑分组名称
        $mygroupIdx = $_GET['mygroupIdx'];
        $mygroupName = $_GET['mygroupName'];
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT mygroupIdx from tb_my_group where memberIdx = $memberIdx AND mygroupName = '$mygroupName'");
        $mygroup = $PdoMySQL->getRow($sql_msg);
        if ($mygroup['mygroupIdx'] != $mygroupIdx && $mygroup['mygroupIdx']) {//存在分组名
            $res['code'] = -1;
            $res['msg'] = "分组名已存在，换一个名字吧";
            $res['data'] = '';
        } else {
            $data_group['mygroupName'] = $mygroupName;
            $PdoMySQL->update($data_group, self::$tb_my_group, 'mygroupIdx = "' . $mygroupIdx . '"');
            $res['code'] = 0;
            $res['msg'] = "修改成功";
        }

        return $res;
    }

    public function editNickName()
    {
        //编辑好友名称
        $friend_id = $_GET['friend_id'];
        $nickName = $_GET['nickName'];
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT a.myfriendIdx from tb_my_friend AS a INNER JOIN tb_my_group AS b ON a.mygroupIdx = b.mygroupIdx where b.memberIdx = $memberIdx AND a.memberIdx = $friend_id");
        $myfriendIdx = $PdoMySQL->getRow($sql_msg);
        if ($myfriendIdx['myfriendIdx']) {//存在该好友
            $data_friend['nickName'] = $nickName;
            if (!$nickName || $nickName == '') {
                $friendName = User::getInstance()->getUserInfo($friend_id);
                $nickName = $friendName ? $friendName['user_nicename'] : '';
            }
            $PdoMySQL->update($data_friend, self::$tb_my_friend, 'myfriendIdx = ' . $myfriendIdx['myfriendIdx']);
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = $nickName;
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function moveFriend()
    {
        //移动好友
        $friend_id = $_GET['friend_id'];
        $data_friend['mygroupIdx'] = $_GET['groupidx'];
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT a.myfriendIdx from tb_my_friend AS a INNER JOIN tb_my_group AS b ON a.mygroupIdx = b.mygroupIdx where b.memberIdx = $memberIdx AND a.memberIdx = $friend_id");
        $myfriendIdx = $PdoMySQL->getRow($sql_msg);
        if ($myfriendIdx['myfriendIdx']) {//存在该好友

            $data = $PdoMySQL->update($data_friend, self::$tb_my_friend, 'myfriendIdx = ' . $myfriendIdx['myfriendIdx']);
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = $data_friend['mygroupIdx'];
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function editGroupNickName()
    {
        //编辑群名片
        $memberIdx = $_GET['memberIdx'];//传递过来的用户
        $groupIdx = $_GET['groupIdx'];
        $data_group_name['nickName'] = $_GET['nickName'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        if ($memberIdx == $memberIdx_AT) {//修改自己的名片
            $data = $PdoMySQL->update($data_group_name, self::$tb_group_member, 'memberIdx = ' . $memberIdx . ' AND groupIdx=' . $groupIdx);
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = $data_group_name['nickName'];
        } else {
            $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx");
            $memberType = $PdoMySQL->getRow($sql);

            $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
            $manager = $PdoMySQL->getRow($sql);
            if ($manager['type'] == 1 || ($manager['type'] == 2 && $memberType['type'] == 3)) {//群主 或者群管理并且被修改的是群员
                $data = $PdoMySQL->update($data_group_name, self::$tb_group_member, 'memberIdx = ' . $memberIdx . ' AND groupIdx=' . $groupIdx);
                $res['code'] = 0;
                $res['msg'] = "修改成功";
                $res['data'] = $data_group_name['nickName'];
            } else {
                $res['code'] = -1;
                $res['msg'] = "参数错误";
            }
        }

        return $res;
    }

    public function setAdmin()
    {
        //设置群管理
        $manager = $_GET['memberIdx'];
        $groupIdx = $_GET['groupidx'];
        $type = $_GET['type'];
        $memberIdx = $_SESSION['info']['id'];
        $sql_msg = sprintf(" SELECT groupMemberIdx,type from tb_group_member where memberIdx = $memberIdx AND groupIdx = $groupIdx");
        $belong = $PdoMySQL->getRow($sql_msg);
        if ($belong['type'] == 1) {//群主
            $sql_msg = sprintf(" SELECT groupMemberIdx,type from tb_group_member where memberIdx = $manager AND groupIdx = $groupIdx");
            $member = $PdoMySQL->getRow($sql_msg); //群员
            if ($type == 2) {//设置为管理
                if ($member['type'] == 3) {
                    $data_manager['type'] = 2;
                    $data = $PdoMySQL->update($data_manager, self::$tb_group_member, 'groupMemberIdx = ' . $member['groupMemberIdx']);
                    $res['code'] = 0;
                    $res['msg'] = "设置成功";
                } elseif ($member['type'] == 2) {
                    $res['code'] = 1;
                    $res['msg'] = "请勿重复设置";
                }
            } else {
                if ($member['type'] == 2) {
                    $data_manager['type'] = 3;
                    $data = $PdoMySQL->update($data_manager, self::$tb_group_member, 'groupMemberIdx = ' . $member['groupMemberIdx']);
                    $res['code'] = 0;
                    $res['msg'] = "设置成功";
                } elseif ($member['type'] == 3) {
                    $res['code'] = 1;
                    $res['msg'] = "请勿重复设置";
                }
            }

            $res['data'] = '';
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function groupAllMemberGag()
    {
        //全员禁言
        $groupIdx = $_GET['groupidx'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
        $manager = $PdoMySQL->getRow($sql);
        if ($manager['type'] == 1 || $manager['type'] == 2) {//群主 或者群管理并且被修改的是群员
            $data_gag['gagTime'] = -1;//长久禁言
            $data = $PdoMySQL->update($data_gag, self::$tb_group_member, 'groupIdx = ' . $groupIdx . ' AND type = 3');
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = '';
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function liftGroupAllMemberGag()
    {
        //解除全员禁言
        $groupIdx = $_GET['groupidx'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
        $manager = $PdoMySQL->getRow($sql);
        if ($manager['type'] == 1 || $manager['type'] == 2) {//群主 或者群管理并且被修改的是群员
            $data_gag['gagTime'] = 0;//解除长久禁言
            $data = $PdoMySQL->update($data_gag, self::$tb_group_member, 'groupIdx = ' . $groupIdx . ' AND type = 3');
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = '';
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function groupMemberGag()
    {
        //群员禁言
        $memberIdx = $_GET['friend_id'];//传递过来的用户
        $groupIdx = $_GET['groupidx'];
        $gagTime = $_GET['gagTime'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        $sql = sprintf(" SELECT type,groupMemberIdx from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx");
        $memberType = $PdoMySQL->getRow($sql);
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
        $manager = $PdoMySQL->getRow($sql);
        if ($manager['type'] == 1 || ($manager['type'] == 2 && $memberType['type'] == 3)) {//群主 或者群管理并且被修改的是群员
            $arr = preg_split("/([0-9]+)/", $gagTime, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            switch ($arr[1]) {
                case 's'://禁言多少秒
                    $gagTime = $arr[0];
                    break;
                case 'm'://禁言多少分钟
                    $gagTime = $arr[0] * 60;
                    break;
                case 'h'://禁言多少小时
                    $gagTime = $arr[0] * 3600;
                    break;
                case 'd'://禁言多少天
                    $gagTime = $arr[0] * 3600 * 24;
                    break;
            }
            $data_gag['gagTime'] = ($gagTime + time()) . '000';
            // $data_gag['type'] = $memberType['type']+2;
            $data = $PdoMySQL->update($data_gag, self::$tb_group_member, 'groupMemberIdx = ' . $memberType['groupMemberIdx']);
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data']['gagTime'] = $data_gag['gagTime'];
            $res['data']['time'] = time() . '000';
            $res['data']['s'] = $gagTime;
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function liftGroupMemberGag()
    {
        //解除群员禁言
        $memberIdx = $_GET['friend_id'];//传递过来的用户
        $groupIdx = $_GET['groupidx'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        $sql = sprintf(" SELECT type,groupMemberIdx from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx");
        $memberType = $PdoMySQL->getRow($sql);
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
        $manager = $PdoMySQL->getRow($sql);
        if ($manager['type'] == 1 || ($manager['type'] == 2 && $memberType['type'] == 3)) {//群主 或者群管理并且被修改的是群员
            $data_gag['gagTime'] = 0;
            $data = $PdoMySQL->update($data_gag, self::$tb_group_member, 'groupMemberIdx = ' . $memberType['groupMemberIdx']);
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = '';
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

    public function leaveGroup()
    {
        //退群
        $memberIdx = $_GET['memberIdx'];//传递过来的用户
        if (!$memberIdx) {
            $list = $_GET['list'];
            $memberIdx = $list[0];
        }
        $groupIdx = $_GET['groupIdx'];
        $memberIdx_AT = $_SESSION['info']['id'];//当前登陆的用户
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx");
        $memberType = $PdoMySQL->getRow($sql);
        if (!$memberType['type']) {//群员不存在
            $res['code'] = -2;
            $res['msg'] = "群员不存在";
            $res['data'] = '';
            echo json_encode($res);
            return false;
        }
        $sql = sprintf(" SELECT type from tb_group_member  where groupIdx = $groupIdx AND memberIdx = $memberIdx_AT");
        $manager = $PdoMySQL->getRow($sql);
        if ($memberIdx_AT == $memberIdx || $manager['type'] == 1 || ($manager['type'] == 2 && $memberType['type'] == 3)) {//自己退群 群主 或者群管理并且被踢的是群员
            $PdoMySQL->delete(self::$tb_group_member, "memberIdx=" . $memberIdx . ' AND groupIdx = ' . $groupIdx);//从群员列表删除
            $PdoMySQL->delete(self::$tb_msg, '`from` = ' . $memberIdx . ' AND `to` = ' . $groupIdx);//删除消息记录
            $res['code'] = 0;
            $res['msg'] = "修改成功";
            $res['data'] = $memberIdx;
        } else {
            $res['code'] = -1;
            $res['msg'] = "参数错误";
        }

        return $res;
    }

}
