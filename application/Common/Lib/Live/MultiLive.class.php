<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Live;

use Common\Lib\Auth\User;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;

class MultiLive extends Service
{
    /**
     * muti_show config
     */
    const ROOM_TYPE_GENERAL = 0;
    const ROOM_TYPE_LOCK = 1;
    const ROOM_TYPE_LIVE_COIN = 2;

    const ANCHOR_LIMIT = 50;

    //路由前缀，专门给多人房间
    const ROUTE_PREFIX = '0';

    const KEY_MULTI_SHOW = 'muti_show';

    /**
     * muti_show主表附属属性
     * @var array
     */
    public static $multiShowExtField = [
        'room_type' => 0, //房间类型 0-普通，1-锁门，2-扣券
        'room_password' => '',
        'room_live_coin' => 0,
    ];

    /**
     * 获取房间信息统一方法
     * @param $multiId
     * @return array|bool
     */
    public function getMultiShowInfo($multiId)
    {
        $show = M('muti_show')->where(['id' => $multiId])->find();
        if (!$show) {
            return false;
        }

        return array_merge($show, $this->getMultiShowExtData($multiId) ?: []);
    }

    /**
     * 详情
     * @param $roomId
     * @param $uid
     * @return bool|mixed
     */
    public function getRoomInfo($roomId, $uid)
    {
        if (!$roomId) {
            return false;
        }

        $show = $this->getMultiShowInfo($roomId);
        if (!$show) {
            $this->setError('多视频直播不存在');
            return false;
        }

        $show['avatar'] = Func::getUrl($show['avatar']);
        $show['focus'] = Func::getUrl($show['focus']);
        $show['multi_stream'] = $show['multi_stream'] ? json_decode($show['multi_stream'], true) : '';
        $list = M('muti_showmanager')->where(['muti_id' => $roomId])->select() ?: [];
        $users = User::getInstance()->getUsersInfo(array_column($list, 'uid')) ?: [];
        $show['anchor'] = [];
        $show['dj'] = [];
        $show['ami_anchor'] = false;
        $show['ami_dj'] = false;

        foreach ($list as $li) {
            if (empty($users[$li['uid']])) {
                continue;
            }

            unset($li['muti_id'], $li['sort_no']);
            $user = $users[$li['uid']];
            $li['user_nicename'] = $user['user_nicename'];
            $li['avatar'] = $user['avatar'];
            $li['sex'] = $user['sex'];

            switch ($li['vest_type']) {
                case User::VEST_TYPE_ANCHOR:
                    if ($li['uid'] == $uid) {
                        $show['ami_anchor'] = true;
                    }
                    $show['anchor'][] = $li;
                    break;
                case User::VEST_TYPE_DJ:
                    if ($li['uid'] == $uid) {
                        $show['ami_dj'] = true;
                    }
                    $show['dj'][] = $li;
                    break;
            }
        }

        return $show;
    }

    /**
     * 开始/关闭直播,开始/关闭喊麦API
     * @param $uid
     * @param $multiId
     * @param $vestType
     * @param $status
     * @return bool
     */
    public function setMultiShowManagerStatus($uid, $multiId, $vestType, $status)
    {
        if (!$uid || !$multiId || !$vestType) {
            $this->setError('缺参数');
            return false;
        }
        if (!in_array($status, [0, 1])) {
            $this->setError('status参数错误');
            return false;
        }
        switch ($vestType) {
            case 'anchor':
                $vestType = User::VEST_TYPE_ANCHOR;
                break;
            case 'dj':
                $vestType = User::VEST_TYPE_DJ;
                break;
            default:
                $this->setError('vest_type参数错误');
                return false;
        }

        $multi = M('muti_showmanager');
        $where = ['muti_id' => $multiId, 'uid' => $uid, 'vest_type' => $vestType];
        if (!$multi->where($where)->find()) {
            $this->setError('您不在该房间');
            return false;
        }

        if ($vestType == User::VEST_TYPE_ANCHOR) {
            $showNum = $this->getMultiShowNum($multiId, $vestType, 1);
            if ($showNum > self::ANCHOR_LIMIT) {
                $this->setError('当前房间已满');
                return false;
            }
        }

        $rs = $multi->where($where)->save([
            'status' => $status,
            'updated_at' => time(),
            'ispc' => 1,
        ]);
        if (false === $rs) {
            $this->setError('操作失败');
            return false;
        }
        M('muti_show')->where(['id' => $multiId])->save(['updated_at' => time()]);

        return true;
    }

    public function getMultiShowNum($multiId, $vestType = 0, $status = 0)
    {
        return (int)M('muti_showmanager')->where([
            'muti_id' => $multiId,
            'vest_type' => $vestType,
            'status' => $status,
        ])->count();
    }

    /**
     * 检查路由是否是多视频直播
     * @param $roomNum
     * @return bool
     */
    public function routeIsMultiLive($roomNum)
    {
        return 0 === stripos($roomNum, MultiLive::ROUTE_PREFIX);
    }

    /**
     * 设置房间密码
     * @param $uid
     * @param $multiId
     * @param $password
     * @return bool
     */
    public function setMultiShowPassword($uid, $multiId, $password)
    {
        if (!$uid || !$multiId || !$password) {
            $this->setError('缺参数');
            return false;
        }
        if (!$this->validateRoomPassword($password)) {
            return false;
        }

        if (!$this->isFirstManager($uid, $multiId)) {
            $this->setError('没有权限，你不是第一个主播');
            return false;
        }

        if (!$this->getMultiShowInfo($multiId)) {
            $this->setError('房间不存在');
            return false;
        }

        return $this->setMultiShowExtData($multiId, ['room_password' => $password]);
    }

    public function setMultiShowExtData($multiId, array $param)
    {
        $fields = array_keys(self::$multiShowExtField);
        foreach ($param as $key => $value) {
            if (in_array($key, $fields)) {
                $rs = CRedis::getInstance()->hSet(self::KEY_MULTI_SHOW . $multiId, $key, $value);
                if (false === $rs) {
                    $this->setError('设置失败');
                    return false;
                }
            }
        }

        M('muti_show')->where(['id' => $multiId])->save(['updated_at' => time()]);

        return true;
    }

    public function getMultiShowExtData($multiId)
    {
        $data = CRedis::getInstance()->hGetAll(self::KEY_MULTI_SHOW . $multiId) ?: [];
        return array_merge(self::$multiShowExtField, $data);
    }

    /**
     * 是否是1号角色
     * @param $uid
     * @param $multiId
     * @param int $vestType
     * @return bool
     */
    public function isFirstManager($uid, $multiId, $vestType = User::VEST_TYPE_ANCHOR)
    {
        if (!$uid || !$multiId) {
            return false;
        }

        $first = M('muti_showmanager')
            ->where(['muti_id' => $multiId, 'vest_type' => $vestType,])
            ->order(['sort_no' => 'DESC'])
            ->find();
        if (!$first) {
            return false;
        }

        return $first['uid'] == $uid;
    }

    /**
     * validate room password
     * @param $password
     * @return bool
     */
    public function validateRoomPassword($password)
    {
        if (!$password) {
            $this->setError('密码为空');
            return false;
        }
        if (!is_numeric($password) || mb_strlen($password) > 6) {
            $this->setError('只能输入数字，最多6位');
            return false;
        }

        return true;
    }

    /**
     * 直播中状态在前，休息中状态在后
     * @return array
     */
    public function getList()
    {
        $list = M('muti_show')
            ->where(['status' => ['in', [1, 2]]])
            ->order(['status' => 'ASC', 'updated_at' => 'DESC'])->limit(8)->select() ?: [];
        foreach ($list as &$li) {
            $li['nums'] = Live::getInstance()->getRoomLivedNum(1, $li['id']);
        }

        return $list;
    }

}
