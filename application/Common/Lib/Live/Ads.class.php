<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Live;

use Common\Lib\Service;

class Ads extends Service
{
    /**
     * 首页右边栏广告
     */
    const INDEX_RIGHT_ID = 1;

    public function getIndexRightAds()
    {
        $adsSort = M('ads_sort')->where(['' => self::INDEX_RIGHT_ID])->find();
        if (!$adsSort) {
            return false;
        }

        return M('ads')->where(['sid' => $adsSort['id']])->order(['orderno' => 'ASC'])->limit(7)->select();
    }

    /**
     * 拼接数据，给恶心的tp template 循环使用
     */
    public function getLiveAnnouncementGroupMap()
    {
        $groupMap = [];
        foreach (Live::$liveAnnouncementGroupMap as $key => $value) {
            $groupMap[] = [
                'key' => $key,
                'value' => $value,
            ];
        }

        return $groupMap;
    }

}
