//判断访问终端
var browser={
    versions:function(){
        var u = navigator.userAgent, app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
            iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
            qq: u.match(/\sQQ/i) == " qq" //是否QQ
        };
    }(),
    language:(navigator.browserLanguage || navigator.language).toLowerCase()
}
var rtc={
    client:{},			                            		//客户端对象
    localStream:{},                              		    //本地流对象
    devices:{                                              //本地设备列表
        videoArray:[],
        audioArray:[]
    },
    role:"user",                                        //当前角色：user 普通用户,anchor 连麦主播,speaker 连麦麦手
    audioCombo:"audioSource",                           //音频设备选择下拉元素id
    videoCombo:"videoSource",                           //视频设备选择下拉元素id
    prevRegion:"previewRegion",                         //视频预览区域元素id
    streamArray:[],                                     //连麦流对象数组(包含本地流和远端流)
    config:{
        appkey:"e649a576c3df4b5981c342cbc5a47b9a",     //appKey 默认
        channel:"1000"
    },
    user:{
        uid:null,
        role:"user"                                 //默认用户角色
    },
	
	/*
    根据角色检查浏览器兼容性
    client.init
    @param: config
    @param: user
    return: null
    */
	checkBrowser:function(roleType){
		var isSupport=true;
		try{
			if(!AgoraRTC.checkSystemRequirements() && roleType!="user") {   //非用户角色才严格检查浏览器
				isSupport=false;
			}else { //角色检查是否使用谷歌火狐opera类浏览器
				isSupport=(browser.versions.presto || browser.versions.webKit || browser.versions.gecko || browser.versions.android || browser.versions.weixin)?true:false;
			}
		}catch(e){
			isSupport=false;
		}
		return isSupport;
	},

    /*
    初始化配置及创建Client 对象
    client.init
    @param: config
    @param: user
    return: null
    */
    init:function(config,user){
		if(!rtc.checkBrowser(user.role)){
			$(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>当前浏览器不支持本直播间。请下载安装chrome firefox或使用极速(chrome)模式</strong></div>');
			return;
		}
		//判断是否移动端
		if(browser.versions.mobile||browser.versions.android||browser.versions.ios){ alert("移动端"); }
        rtc.config=$.extend(rtc.config,config);
        rtc.user=$.extend(rtc.user,user);
        rtc.curRole=rtc.user.curRole||"user",          //如果config.curRole为空。则默认为普通用户角色
		console.log(" init user id="+rtc.user.uid)
        rtc.join();
		console.log(rtc);
		if(rtc.curRole!="user"){
			rtc.getDevices();
		}
    },
    /*
	离开当前房间频道
	rtc.leave
	@param: null
	return: null
	*/
    leave:function(){
        $('#leave').attr('disabled',"true");
        rtc.client.leave(function () {
            console.log("Leavel channel successfully");
        }, function (err) {
            console.log("Leave channel failed");
        });
    },
    /*
    预览展示
    rtc.preview
    @param: null
    return: null
    */
    preview:function(){
		if(rtc.localStream && rtc.localStream.local){
			rtc.localStream.stop(rtc.prevRegion);
		}
        this.createStream();
        if(rtc.localStream){
            rtc.localStream.init(function() {
                console.log("getUserMedia successfully");
				rtc.getDevices();						//得到用户浏览器授权后。再调一次。写入设备ID至下拉框
                rtc.localStream.play(rtc.prevRegion);
            }, function (err) {
                console.log("getUserMedia failed", err);
            });
        }
    },
    /*
	发布本地视频
	rtc.publish
	@param: null
	return: null
	*/
    publish:function(){
        var allowDeivce=$(".showMessage > .alert-success").length>0?true:false;
        if(allowDeivce){
			console.log(rtc.localStream);
            if(!rtc.localStream){
                this.createStream();
            }
            rtc.play(rtc.localStream,true);      // 在指定位置播放
			rtc.client.enableDualStream(function(){
				console.log("Enable dual stream success");
			},function(err){console.log("Enable dual stream failed:"+err);});
            rtc.client.publish(rtc.localStream, function (err) {
                console.log("Publish local stream error: " + err);
                $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>Publish Stream Error!</strong>{0}</div>'.replace("{0}",err));
            });
            rtc.client.on('stream-published', function (evt) {
                $("#steamId").val(evt.stream.getId());
                $(".showMessage").html('<div class="alert alert-success" role="alert"><strong>Well done!</strong>本地流发布成功</div>');
            });
        }
    },
    /*
	 创建本地视频流
	 rtc.createStream
	 @param: null
	 return: null
	 */
    createStream:function(){
        var config={streamID: rtc.user.uid, audio:true,video:true,cameraId:$("#"+rtc.videoCombo).val(),microphoneId: $("#"+rtc.audioCombo).val(),  screen: false};
        if(rtc.curRole=="speaker"){
            config.video=false;
            config.cameraId="";
        }
        rtc.localStream = AgoraRTC.createStream(config);
        if (config.video) {
            rtc.localStream.setVideoProfile('360P_1');
        }
        //如果用户有授权音视频成功
        rtc.localStream.on("accessAllowed", function() {
            $(".showMessage").html('<div class="alert alert-success" role="alert"><strong>Well done!</strong>你已成功授权访问摄像头或音频设备</div>');
        });
        //如果用户有授权失败
        rtc.localStream.on("accessDenied", function() {
            $(".showMessage").html('<div class="alert alert-warning" role="alert"><strong>Warning!</strong> 您的摄像头或音频设备未成功授权访问！</div>');
        });
    },
    /*
	取消发布本地视频
	rtc.unpublish
	@param: null
	return: null
	*/
    unpublish:function() {
		$("#btnLive").text("开始直播");
		$("#btnLive").attr("onclick","rtc.publish();");
        rtc.client.unpublish(rtc.localStream, function (err) {
            $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>Unpublish Stream failed!</strong>{0}</div>'.replace("{0}",err));
        });
		rtc.localStream.stop();
    },
	
	/*
	播放本地或远程视频。并调整video顺序
	rtc.unpublish
	@param: stream 流对像
	return: isLocal 是否为本地流（即是否为上播）
	*/
    play:function(stream,isLocal){
        var changeType="append";
        var item={id:stream.getId(),obj:stream};
        if(isLocal){															   //如果是本地视频。
            if(rtc.streamArray.length==0){                                         //不存在上播流则队尾新增待播放
                rtc.streamArray.push(item);
            }else{
                if(stream.getId()==rtc.streamArray[0].id){                         //队首相同，则替换
                    changeType="repalce";
                    rtc.streamArray[0]=item;
					
                }else{                                                             //队首不同，则在队首插入
                    changeType="insert";
                    rtc.streamArray.unshift(item);
                }
            }
        }else{
			//观众模式下。1号播放大流，其它播放小流
			var streamSize=(rtc.streamArray.length==0?0:1);
			rtc.client.setRemoteVideoStreamType(stream, streamSize);
			console.log("播放流对象["+stream.getId()+"]的"+(streamSize==0?"大流":"小流"));
            rtc.streamArray.push({id:stream.getId(),obj:stream});
        }
        switch(changeType){
            case "append":                                                          //append 为队尾添加
                $(".showLive").eq(rtc.streamArray.length-1).attr("id","video_"+stream.getId());
                stream.play("video_"+stream.getId());
                break;
            case "insert":                                                          //insert为队首添加
                $.each(rtc.streamArray,function(index,item){
                    item.obj.stop("video_"+item.id);
                    $(".showLive").eq(index).attr("id","video_"+item.id);
                    if(item.obj){
						if(index>0){//主播上播时，插入队首，所以除主流外，次流应全部播放小流模式
							rtc.client.setRemoteVideoStreamType(item.obj, 1);
							console.log("播放流对象["+item.obj.getId()+"]的小流");
						}
						item.obj.play("video_"+item.id);
                    }
                });
                break;
            case "repalce":                                                         //repalce 为队首替换
                $(".showLive").eq(0).attr("id","video_"+stream.getId());
                stream.play("video_"+stream.getId());
                break;
        }
    },
    /*
	加入房间,不区分角色。任何角色都应该可以播放
	rtc.join
	@param: null
	return: null
	*/
    join:function(){
        $('#join,#hasVideo').attr('disabled',"true");
        var channel_key = null;
        rtc.client = AgoraRTC.createClient({mode: 'interop'});
        rtc.client.init(rtc.config.appkey, function () {
            console.log("AgoraRTC client initialized:"+rtc.user.uid);
            rtc.client.join(null, rtc.config.channel, rtc.user.uid, function() {
                console.log("User " + rtc.user.uid + " join channel:"+rtc.config.channel+" successfully");
            }, function(err) {
                console.log("Join channel failed", err);
                $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>Join channel Failed!</strong>{0}</div>'.replace("{0}",err));
            });
        }, function (err) {
            $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>AgoraRTC client init failed!</strong>{0}</div>'.replace("{0}",err));
        });
        rtc.addClentEvent();
    },
    /*
	添加事件处理
	rtc.addClentEvent
	@param: null
	return: null
	*/

    addClentEvent:function(){
        if(rtc.client){
            var channelKey = "";
            rtc.client.on('error', function(err) {
                console.log("Got error msg:", err.reason);
                if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
                    rtc.client.renewChannelKey(channelKey, function(){
                        console.log("Renew channel key successfully");
                    }, function(err){
                        console.log("Renew channel key failed: ", err);
                        $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>Renew channel key failed: !</strong>{0}</div>'.replace("{0}",err));
                    });
                }
            });
            //远程音视频流加入事件
            rtc.client.on('stream-added', function (evt) {
                var stream = evt.stream;
                console.log("New stream added: " + stream.getId());
                console.log("Subscribe ", stream);
                rtc.client.subscribe(stream, function (err) {
                    $(".showMessage").html('<div class="alert alert-danger" role="alert"><strong>Subscribe stream failed !</strong>{0}</div>'.replace("{0}",err));
                });
            });
            //远程音视频流订阅成功事件
            rtc.client.on('stream-subscribed', function (evt) {
                var stream = evt.stream;
				console.log("远程音视频流Id:"+stream.getId()+" 当前"+$(".userList li[streamId='"+stream.getId()+"']").length==0?"不存在":"已存在");
				if($(".userList li[streamId='"+stream.getId()+"']").length==0){
				$(".userList").append('<li class="list-group-item" streamId='+stream.getId()+'>用户('+stream.getId()+')</li>');
				}
				rtc.play(stream,false);
            });
            //远程音视频流已删除事件
            rtc.client.on('stream-removed', function (evt) {
                var stream = evt.stream;
                stream.stop();
                $('#agora_remote' + stream.getId()).remove();
                console.log("Remote stream is removed " + stream.getId());
            });
            //用户已离开会议室事件 即对方调用了 client.leave().
            rtc.client.on('peer-leave', function (evt) {
                var stream = evt.stream;
                if (stream) {
                    stream.stop();
                    $('#agora_remote' + stream.getId()).remove();
					$(".userList").append('<li class="list-group-item">用户('+stream.getId()+')</li>');
                    console.log(evt.uid + " leaved from this channel");
                }
            });
            //通知应用程序频道频道内谁在说话事件
            rtc.client.on('active-speaker', function(evt) {
                var uid = evt.uid;
                console.log("update active speaker: client " + uid);
            });
        }
    },
    /*
	得到相关设置并填充设备选择框
	rtc.getDevices
	@param: null
	return: null
	*/
    getDevices:function(){
		$("#{0},#{1}".replace("{0}",rtc.audioCombo).replace("{1}",rtc.videoCombo)).empty();
        AgoraRTC.getDevices(function (devices) {
            $.each(devices,function(i,device){
                var combox,content;
                if(device.kind === 'audioinput'){
                    rtc.devices.audioArray.push(device);
                    combox=rtc.audioCombo;
                    content=device.label || 'microphone ' + (rtc.devices.audioArray.length);
                }else if (device.kind === 'videoinput'){
                    rtc.devices.videoArray.push(device);
                    combox=rtc.videoCombo;
                    content=device.label || 'camera ' + (rtc.devices.videoArray.length);
                }
                combox="#"+combox;
                if($(combox)){
                    $(combox).append("<option value='{0}'>{1}</option>".replace('{0}',device.deviceId).replace('{1}',content));
                }
            });
        });
    }
}