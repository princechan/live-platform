var rtc={
	client:{},
	localStream:{},
	camera:{},
	microphone:{},
	devices:{videoArray:[],audioArray:[]},
	config:{appkey:"31a76a94f29f4a4ba8ad30f663bb1691"},
	audioCombo:"#audioSource",
	videoCombo:"#videoSource",
	init:function(_config){
		if(!AgoraRTC.checkSystemRequirements()) {
		  alert("browser is no support webRTC");
		  return;
		}
		rtc.config=$.extend(rtc.config,_config);
		console.log(rtc.config);
		rtc.getDevices();
		
	},
	leave:function(){
		  $('#leave').attr('disabled',"true");
		  rtc.client.leave(function () {
			console.log("Leavel channel successfully");
		  }, function (err) {
			console.log("Leave channel failed");
		  });
	},
	publish:function() {
		 $('#publish').attr('disabled',"true");
		 $('#unpublish').removeAttr("disabled");
		rtc.client.publish(rtc.localStream, function (err) {
		console.log("Publish local stream error: " + err);
	  });
	},
	unpublish:function () {
		  $('#unpublish').attr('disabled',"true");
		  $('#publish').removeAttr("disabled");
		  rtc.client.unpublish(rtc.localStream, function (err) {
			console.log("Unpublish local stream failed" + err);
		  });
	},
	play:function(){
		rtc.client = AgoraRTC.createClient({mode: 'interop'});
		rtc.client.init(rtc.config.appkey, function () {
				console.log("AgoraRTC client initialized");
				rtc.client.join(null, rtc.config.channel, null, function(uid) {
				  console.log("User " + uid + " join channel successfully");
				}, function(err) {
				  console.log("Join channel failed", err);
				});
			  }, function (err) {
				console.log("AgoraRTC client init failed", err);
			  });
		rtc.client.on('stream-added', function (evt) {
				var stream = evt.stream;
				//$("#steamId").val(evt.stream.getId());
				$("#lab").text("订阅观看："+evt.stream.getId());
				console.log("stream added id: " + stream.getId());
				console.log(stream);
				rtc.client.subscribe(stream, function (err) {
				  console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", err);
				  console.log("Subscribe stream failed", err);
				});
			  });

		rtc.client.on('stream-subscribed', function (evt) {
				var stream = evt.stream;
				
				console.log("Subscribe remote stream successfully: " + stream.getId());
				if ($('div#video #agora_remote'+stream.getId()).length === 0) {
				  $('div#video').append('<div id="agora_remote'+stream.getId()+'" style="float:left; width:810px;height:607px;display:inline-block;"></div>');
				}
				stream.play('agora_remote' + stream.getId());
				
		});
	},
	join:function(){
		AgoraRTC.Logger.info('call join funcion');
		$('#join,#hasVideo').attr('disabled',"true");
			  var channel_key = null;
			  rtc.client = AgoraRTC.createClient({mode: 'interop'});
			  rtc.client.init(rtc.config.appkey, function () {
				console.log("AgoraRTC client initialized");
				rtc.client.join(null, rtc.config.channel, null, function(uid) {
				  console.log("User " + uid + " join channel successfully");
				  console.log("hasVide="+$('#hasVideo').attr('checked'));
				  if ($('#hasVideo').prop('checked')) {
					rtc.camera = $(rtc.videoCombo).val();
					rtc.microphone =$(rtc.audioCombo).val();
					rtc.localStream = AgoraRTC.createStream({streamID: uid, audio: true, cameraId: rtc.camera, microphoneId: rtc.microphone, video:$('#hasVideo').prop('checked'), screen: false});
					//localStream = AgoraRTC.createStream({streamID: uid, audio: false, cameraId: camera, microphoneId: microphone, video: false, screen: true, extensionId: 'minllpmhdgpndnkomcoccfekfegnlikg'});
					if ($('#hasVideo').prop('checked')) {
					  rtc.localStream.setVideoProfile('720p_3');
					}

					// The user has granted access to the camera and mic.
					rtc.localStream.on("accessAllowed", function() {
					  console.log("accessAllowed");
					});

					// The user has denied access to the camera and mic.
					rtc.localStream.on("accessDenied", function() {
					  console.log("accessDenied");
					});

					rtc.localStream.init(function() {
					  console.log("getUserMedia successfully");
					  rtc.localStream.play('agora_local');
					  rtc.client.publish(rtc.localStream, function (err) {
						console.log("Publish local stream error: " + err);
					  });

					  rtc.client.on('stream-published', function (evt) {
						$("#steamId").val(evt.stream.getId());
						console.log("Publish local stream successfully");
					  });
					}, function (err) {
					  console.log("getUserMedia failed", err);
					});
				  }
				}, function(err) {
				  console.log("Join channel failed", err);
				});
			  }, function (err) {
				console.log("AgoraRTC client init failed", err);
			  });

			 var channelKey = "";
			  rtc.client.on('error', function(err) {
				console.log("Got error msg:", err.reason);
				if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
				  rtc.client.renewChannelKey(channelKey, function(){
					console.log("Renew channel key successfully");
				  }, function(err){
					console.log("Renew channel key failed: ", err);
				  });
				}
			  });


			  rtc.client.on('stream-added', function (evt) {
				var stream = evt.stream;
				console.log("New stream added: " + stream.getId());
				console.log("Subscribe ", stream);
				rtc.client.subscribe(stream, function (err) {
				  console.log("Subscribe stream failed", err);
				});
			  });

			  rtc.client.on('stream-subscribed', function (evt) {
				var stream = evt.stream;
				console.log("Subscribe remote stream successfully: " + stream.getId());
				if ($('div#video #agora_remote'+stream.getId()).length === 0) {
				  $('div#video').append('<div id="agora_remote'+stream.getId()+'" style="float:left; width:810px;height:607px;display:inline-block;"></div>');
				}
				stream.play('agora_remote' + stream.getId());
			  });

			  rtc.client.on('stream-removed', function (evt) {
				var stream = evt.stream;
				stream.stop();
				$('#agora_remote' + stream.getId()).remove();
				console.log("Remote stream is removed " + stream.getId());
			  });

			  rtc.client.on('peer-leave', function (evt) {
				var stream = evt.stream;
				if (stream) {
				  stream.stop();
				  $('#agora_remote' + stream.getId()).remove();
				  console.log(evt.uid + " leaved from this channel");
				}
			  });
	},
	getDevices:function(){
		AgoraRTC.getDevices(function (devices) {
			var text="";
			$.each(devices,function(i,device){
				var combox,content;
				if(device.kind === 'audioinput'){
					rtc.devices.audioArray.push(device);
					combox=rtc.audioCombo;
					content=device.label || 'microphone ' + (rtc.devices.audioArray.length);
				}else if (device.kind === 'videoinput'){
					rtc.devices.videoArray.push(device);
					combox=rtc.videoCombo;
					content=device.label || 'camera ' + (rtc.devices.videoArray.length);
				}
				if($(combox)){
					  $(combox).append("<option value='{0}'>{1}</option>".replace('{0}',device.deviceId).replace('{1}',content));
				}
			});
		});
	}
}