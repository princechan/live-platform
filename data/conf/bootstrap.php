<?php

define('CONFIG_PATH', dirname(__FILE__) . '/');

if (in_array(isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '', ['127.0.0.1', '10.71.42.70',])) {
    define('ENV', 'dev');
    ini_set('display_errors', 1);
    error_reporting(E_ALL || E_STRICT);
    header('Access-Control-Allow-Origin:*'); //测试环境允许跨域
} else {
    define('ENV', 'prod');
}

define('ENV_DEV', ENV === 'dev' ? true : false);
define('APP_DEBUG', ENV_DEV);

//应用标识
$appKeyFile = CONFIG_PATH . '../../appkey';
if (ENV_DEV && file_exists($appKeyFile) && $appKey = file_get_contents($appKeyFile)) {
    define('APP_KEY', $appKey);
} else {
    define('APP_KEY', 'app_key');
}

class Configure
{
    private static $config;

    public static function get()
    {
        if (empty(self::$config)) {
            self::$config = include_once 'config.php';
        }
        return self::$config;
    }
}

$config = Configure::get();

//redis
define('NOSQL_REDIS_HOST', $config['redis'][APP_KEY][0]['host']);
define('NOSQL_REDIS_PORT', $config['redis'][APP_KEY][0]['port']);
define('NOSQL_REDIS_AUTH', $config['redis'][APP_KEY][0]['auth']);

//mysql
define('DATABASE_TYPE', 'mysqli');
define('DATABASE_HOST', $config['mysql'][APP_KEY][0]['host']);
define('DATABASE_NAME', $config['mysql'][APP_KEY][0]['name']);
define('DATABASE_USER', $config['mysql'][APP_KEY][0]['user']);
define('DATABASE_PWD', $config['mysql'][APP_KEY][0]['pwd']);
define('DATABASE_PORT', $config['mysql'][APP_KEY][0]['port']);
define('DATABASE_PREFIX', $config['mysql'][APP_KEY][0]['prefix']);
