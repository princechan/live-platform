<?php

return [
    'SP_SITE_ADMIN_URL_PASSWORD' => '',
    'SP_DEFAULT_THEME' => 'simplebootx',
    'DEFAULT_THEME' => 'simplebootx',
    'SP_ADMIN_STYLE' => 'flat',
    'URL_HTML_SUFFIX' => '',
    'UCENTER_ENABLED' => 1,
    'COMMENT_NEED_CHECK' => 0,
    'COMMENT_TIME_INTERVAL' => 60,
    'MOBILE_TPL_ENABLED' => 0,
    'HTML_CACHE_ON' => false,
    'FILE_UPLOAD_TYPE' => 'Local',
    'UPLOAD_TYPE_CONFIG' => null,
    'SP_MEMBER_EMAIL_ACTIVE' => 0,
    'SP_MAIL_ADDRESS' => 'admin@qq.com',
    'SP_MAIL_SENDER' => 'PhoneLive',
    'SP_MAIL_SMTP' => '25',
    'SP_MAIL_SMTP_PORT' => '25',
    'SP_MAIL_LOGINNAME' => 'admin@qq.com',
    'SP_MAIL_PASSWORD' => '25',
    'URL_MODEL' => 0,
    'URL_CASE_INSENSITIVE' => true,
    'AUTHCODE' => 'rCt52pF2cnnKNB3Hkp',
    'COOKIE_PREFIX' => 'AJ1sOD_',
    // 启用字段缓存
    'DB_FIELDS_CACHE' => ENV_DEV ? false : true,
    'DB_PARAMS' => [PDO::ATTR_CASE => PDO::CASE_NATURAL,],
    'redis' => [
        'xsm' => [
            [
                'host' => '10.71.42.70',
                'port' => 6379,
                'auth' => 'tieweishivps',
            ],
        ],
        'a01' => [
            [
                'host' => '10.71.42.70',
                'port' => 6381,
                'auth' => 'tieweishivps',
            ],
        ],
        'a03' => [
            [
                'host' => '10.71.42.70',
                'port' => 6383,
                'auth' => 'tieweishivps',
            ],
        ],
        'a04' => [
            [
                'host' => '10.71.42.70',
                'port' => 6382,
                'auth' => 'tieweishivps',
            ],
        ],
        'a06' => [
            [
                'host' => '10.71.42.70',
                'port' => 6384,
                'auth' => 'tieweishivps',
            ],
        ],
    ],
    'mysql' => [
        'xsm' => [
            [
                'host' => '10.71.42.70',
                'name' => 'xsm',
                'user' => 'phonelive',
                'pwd' => 'tieweishivps',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
        'a01' => [
            [
                'host' => '10.71.42.70',
                'name' => 'a01',
                'user' => 'phonelive',
                'pwd' => 'tieweishivps',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
        'a03' => [
            [
                'host' => '10.71.42.70',
                'name' => 'a03',
                'user' => 'phonelive',
                'pwd' => 'tieweishivps',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
        'a04' => [
            [
                'host' => '10.71.42.70',
                'name' => 'a04',
                'user' => 'phonelive',
                'pwd' => 'tieweishivps',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
        'a06' => [
            [
                'host' => '10.71.42.70',
                'name' => 'a06',
                'user' => 'phonelive',
                'pwd' => 'tieweishivps',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
    ],
    'easemob' => [
        'xsm' => [
            'client_id' => 'YXA6wggxYOt0Eee0DD0tNtzVDw',
            'client_secret' => 'YXA6EfQZpfLbOfZO2LSdkzbvFvagu8c',
            'org_name' => '1150171228115205',
            'app_name' => 'yunbaodemo',
        ],
        'a01' => [
            'client_id' => 'YXA6wggxYOt0Eee0DD0tNtzVDw',
            'client_secret' => 'YXA6EfQZpfLbOfZO2LSdkzbvFvagu8c',
            'org_name' => '1150171228115205',
            'app_name' => 'yunbaodemo',
        ],
        'a04' => [
            'client_id' => 'YXA6wggxYOt0Eee0DD0tNtzVDw',
            'client_secret' => 'YXA6EfQZpfLbOfZO2LSdkzbvFvagu8c',
            'org_name' => '1150171228115205',
            'app_name' => 'yunbaodemo',
        ],
    ],
];
