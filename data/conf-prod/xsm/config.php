<?php

return [
    'SP_SITE_ADMIN_URL_PASSWORD' => '',
    'SP_DEFAULT_THEME' => 'simplebootx',
    'DEFAULT_THEME' => 'simplebootx',
    'SP_ADMIN_STYLE' => 'flat',
    'URL_HTML_SUFFIX' => '',
    'UCENTER_ENABLED' => 1,
    'COMMENT_NEED_CHECK' => 0,
    'COMMENT_TIME_INTERVAL' => 60,
    'MOBILE_TPL_ENABLED' => 0,
    'HTML_CACHE_ON' => false,
    'FILE_UPLOAD_TYPE' => 'Local',
    'UPLOAD_TYPE_CONFIG' => null,
    'SP_MEMBER_EMAIL_ACTIVE' => 0,
    'SP_MAIL_ADDRESS' => 'admin@qq.com',
    'SP_MAIL_SENDER' => 'PhoneLive',
    'SP_MAIL_SMTP' => '25',
    'SP_MAIL_SMTP_PORT' => '25',
    'SP_MAIL_LOGINNAME' => 'admin@qq.com',
    'SP_MAIL_PASSWORD' => '25',
    'URL_MODEL' => 0,
    'URL_CASE_INSENSITIVE' => true,
    'AUTHCODE' => 'rCt52pF2cnnKNB3Hkp',
    'COOKIE_PREFIX' => 'AJ1sOD_',
    // 启用字段缓存
    'DB_FIELDS_CACHE' => ENV_DEV ? false : true,
    'DB_PARAMS' => [PDO::ATTR_CASE => PDO::CASE_NATURAL,],
    'redis' => [
        'xsm' => [
            [
                'host' => '192.168.1.3',
                'port' => 6379,
                'auth' => '73x62rkEp4X',
            ],
        ],
    ],
    'mysql' => [
        'xsm' => [
            [
                'host' => '192.168.1.3',
                'name' => 'phonelive',
                'user' => 'phonelive',
                'pwd' => '73x62rkEp4X',
                'port' => 3306,
                'prefix' => 'cmf_',
            ],
        ],
    ],
    'easemob' => [
        'xsm' => [
            'client_id' => 'YXA6mRwYIAcwEeigC-_UFeYwRw',
            'client_secret' => 'YXA6iB3jL13gZ7IkRVnNXaqlfOU6fgk',
            'org_name' => '1123180125178876',
            'app_name' => 'xiaoshimei',
        ],
    ],
];
