var socketio = require('socket.io'),
    fs = require('fs'),
    https = require('https'),
    domain = require('domain'),
    redis = require('redis'),
    redisio = require('socket.io-redis'),
    request = require('request'),
    config = require('./config.js');
/**
 * redis keys
 */
var redis_keys = {
    online: 'online:',
    roomgag: 'roomgag:',
    public_chat_room_system_msg: 'public_chat_room_system_msg'
};

var options = {
    key: fs.readFileSync('/data/IM/keys/www.testzhibo.com.key'),
    cert: fs.readFileSync('/data/IM/keys/www.testzhibo.com.crt')
};

var numscount = 0;
var sockets = {};
var chat_history = {};
var chat_interval = {};
var field = [];

/**
 * domain
 */
var d = domain.create();
d.on("error", function (err) {
    console.log(err);
});

/**
 * redis
 */
var clientRedis = redis.createClient(config['REDISPORT'], config['REDISHOST']);
clientRedis.auth(config['REDISPASS']);

/**
 * server
 */
var server = https.createServer(options, function (req, res) {
    res.writeHead(200, {
        'Content-type': 'text/html;charset=utf-8'
    });
    res.end();
}).listen(19967, function () {
    console.log('IM Service Start on 19967');
});

var io = socketio.listen(server, {
    pingTimeout: 60000,
    pingInterval: 25000
});

io.on('connection', function (socket) {
    numscount++;
    clientRedis.set("totalNums", numscount);
    /* 获取服务端配置 */
    request(config['WEBADDRESS'] + "?service=Home.getFilterField", function (error, response, body) {
        if (error) return;
        if (!body) return;
        res = evalJson(body);
        field = (res.data.info) ? (res.data.info) : [];
        console.log("Socket[" + socket.id + "] connection" + (field.length > 0 ? " && success load Filter Field:" + field.join(",") : " Filter Field null"));
    });
    //进入房间
    socket.on('conn', function (data) {
        if (!data || !data.token) {
            console.error("conn room.but data or data.token is null ");
            return !1;
        }
        userid = data.uid;
        old_socket = sockets[userid];
        if (old_socket && old_socket != socket) {
            console.log("delete old socket" + old_socket.id);
            if (data.uid == data.roomnum && data.stream == old_socket.stream) {
                old_socket.reusing = 1;
            }
            old_socket.disconnect();
        }
        console.log("UID[" + data.uid + "],Token[" + data.token + "],Room[" + data.roomnum + ']---stream=' + data.stream);
        clientRedis.get(data.token, function (error, res) {
            if (error) {
                console.error(error);
                return;
            } else if (res == null) {
                console.error("[redis key token no value]" + data.uid);
            } else {

                if (res != null) {
                    var userInfo = evalJson(res);
                    if (userInfo['id'] == data.uid) {
                        //获取验证token
                        console.log("用户[" + data.uid + "]初始化验证成功 sign[" + (userInfo['sign'] ? userInfo['sign'] : "null") + "]");
                        socket.token = data.token;
                        socket.sign = userInfo['sign'];
                        socket.roomnum = data.roomnum;
                        socket.stream = data.stream;
                        socket.nicename = userInfo['user_nicename'];
                        socket.uType = parseInt(userInfo['userType']);
                        socket.uid = data.uid;
                        socket.live_type = data.live_type;
                        socket.live_id = data.live_id;
                        socket.reusing = 0;
                        socket.join(data.roomnum);
                        sockets[userid] = socket;
                        socket.emit('conn', ['ok']);
                        var data_str = "{\"msg\":[{\"_method_\":\"SendMsg\",\"action\":\"0\",\"ct\":{\"id\":\"" + userInfo['id'] + "\",\"user_nicename\":\"" + userInfo['user_nicename'] + "\",\"avatar\":\"" + userInfo['avatar'] + "\",\"level\":\"" + userInfo['level'] + "\"},\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
                        process_msg(io, socket.roomnum, data_str);

                        var onlineKey = redis_keys.online + parseInt(data.live_type) + ':' + parseInt(data.live_id);
                        if (onlineKey && data.uid) {
                            console.log("set User online key=" + onlineKey + ",hashKey=" + data.uid);
                            clientRedis.hset(onlineKey, data.uid, res);
                        }

                        //system msg
                        clientRedis.get(redis_keys.public_chat_room_system_msg, function (error, res) {
                            var systemMsg = '直播内容包含任何低俗、暴露和涉黄内容，账号会被封禁；安全部门会24小时巡查哦～';
                            if (!error) {
                                systemMsg = res ? res : systemMsg;
                            }

                            sendSystemMsg(socket, systemMsg);
                        });

                        return;
                    } else {
                        socket.disconnect();
                    }
                }
            }
            socket.emit('conn', ['no']);
        });
    });
    socket.on('broadcast', function (data) {
        if (socket.token != undefined) {
            var dataObj = typeof data == 'object' ? data : evalJson(data);
            console.log("broadcast:用户UID:" + socket.uid + ",token[" + socket.token + "]");
            var msg = dataObj['msg'][0];
            var action = msg['_method_'];
            var data_str = typeof data == 'object' ? JSON.stringify(data) : data;
            switch (action) {
                case 'SendMsg': {     //聊天
                    if (!dataObj['msg'][0]['isfilter']) {
                        dataObj['msg'][0]['isfilter'] = 0;
                    }
                    if (dataObj['msg'][0]['isfilter'] == '0') {
                        dataObj['msg'][0]['ct'] = filter(dataObj['msg'][0]['ct']);
                    }
                    clientRedis.hget("super", socket.uid, function (error, res) {
                        if (error) return;
                        if (res != null) {
                            var data_str = '{"msg":[{"_method_":"SystemNot","action":"1","ct":"' +
                                dataObj['msg'][0]['ct'] + '","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}';
                            process_msg(io, socket.live_id, data_str);
                        } else {
                            var keyRoomGag = redis_keys.roomgag + parseInt(socket.live_type) + ':' + parseInt(socket.live_id);
                            console.log("[broadcast 监听聊天]" + (socket.live_type == 0 ? "单直播" : "多直播") +
                                "-SET REDIS KEY[" + keyRoomGag + "]---->LiveId[" + socket.live_id + "],Uid[" + socket.uid + "]");
                            data_str = JSON.stringify(dataObj);
                            clientRedis.hget(keyRoomGag, socket.uid, function (error, res) {
                                if (error) return;
                                if (res != null) {
                                    var time = Date.parse(new Date()) / 1000;
                                    if ((time < parseInt(res))) {
                                        var newData = dataObj;
                                        newData['retcode'] = '409002';
                                        console.log("retcode==409002");
                                        socket.emit('broadcastingListen', [JSON.stringify(newData)]);
                                    } else {//解除禁言
                                        clientRedis.del(keyRoomGag, socket.uid);
                                        console.log("解除禁言 删除Key:" + keyRoomGag);
                                        process_msg(io, socket.live_id, data_str);
                                    }
                                } else {
                                    process_msg(io, socket.live_id, data_str);
                                }
                            });
                        }
                        console.log("发送消息内容:" + dataObj['msg'][0]['ct']);
                    });
                    break;
                }
                case 'SendGift': {    //送礼物
                    console.log("[broadcast 送礼物]");
                    var gifToken = dataObj['msg'][0]['ct'];
                    clientRedis.get(gifToken, function (error, res) {
                        if (!error && res != null) {
                            var resObj = evalJson(res);
                            dataObj['msg'][0]['ct'] = resObj;
                            io.sockets.in(socket.roomnum).emit('broadcastingListen', [JSON.stringify(dataObj)]);
                            clientRedis.del(gifToken);
                        }
                    });
                    break;
                }
                case 'SendBarrage': {    //弹幕
                    console.log("[broadcast 弹幕]");
                    var barragetoken = dataObj['msg'][0]['ct'];
                    clientRedis.get(barragetoken, function (error, res) {
                        if (!error && res != null) {
                            var resObj = evalJson(filter(res));
                            dataObj['msg'][0]['ct'] = resObj;
                            var data_str = JSON.stringify(dataObj);
                            process_msg(io, socket.roomnum, data_str);
                            clientRedis.del(barragetoken);
                        }
                    });
                    break;
                }
                case 'SendFly' : {    //飞屏
                    console.log("[broadcast 飞屏]");
                    clientRedis.get(socket.uid + 'SendFly', function (error, res) {
                        if (!error && res == '1') {
                            process_msg(io, socket.roomnum, data_str);
                            clientRedis.del(socket.uid + 'SendFly');
                        } else {

                        }
                    });
                    break;
                }
                case 'clearScreen' : {    //清屏
                    console.log("[broadcast 清屏]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'fetch_sofa' : { //抢座
                    console.log("[broadcast 抢座]");
                    clientRedis.get(socket.uid + 'fetch_sofa', function (error, res) {
                        if (!error && res == '1') {
                            process_msg(io, socket.roomnum, data_str);
                            clientRedis.del(socket.uid + 'fetch_sofa');
                        } else {

                        }

                    });
                    break;
                }
                case 'ConnectVideo' : { //连麦
                    console.log("[broadcast 连麦]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'CloseVideo' : { //下麦
                    console.log("[broadcast 下麦]");
                    clientRedis.hget('ShowVideo', socket.roomnum, function (error, res) {
                        if (!error && (socket.uid == res || socket.uid == socket.roomnum)) {
                            clientRedis.hdel('ShowVideo', socket.roomnum);
                            process_msg(io, socket.roomnum, data_str);
                        }
                    });
                    break;
                }
                case 'ShowVideo' : { //上麦显示
                    console.log("[broadcast 上麦显示]");
                    clientRedis.hget('ShowVideo', socket.roomnum, function (error, res) {
                        if (!error && socket.uid == res) {
                            process_msg(io, socket.roomnum, data_str);
                        }
                    });
                    break;
                }
                case 'SendTietiao' : { //贴条
                    console.log("[broadcast 贴条]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'SendHb' : {      //送红包
                    console.log("[broadcast 送红包]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'VodSong' : {     //点歌
                    console.log("[broadcast 点歌]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'light' : {     //点亮
                    console.log("[broadcast 点亮]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'mimadoor' : {     //锁门
                    console.log("[broadcast 锁门]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'SendRedPack': {//发红包
                    console.log("[broadcast 发红包]");
                    if (dataObj.roomnum) {
                        process_msg(io, data.roomnum, data_str);
                    } else {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'giftPK': {//礼物PK
                    console.log("[broadcast 礼物PK]");
                    process_msg(io, socket.roomnum, data_str);

                    break;
                }
                case 'GrabBench': {//抢板凳
                    console.log("[broadcast 抢板凳]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'Carouse': {//转盘游戏
                    console.log("[broadcast 转盘游戏]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'lianmai': {//PC端连麦
                    console.log("[broadcast PC端连麦]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'SetBackground' : {//设置背景
                    console.log("[broadcast 设置背景]");
                    if (socket.uType == 50) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'CancelBackground' : {//取消背景
                    console.log("[broadcast 取消背景]");
                    if (socket.uType == 50) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'AgreeSong' : {//同意点歌
                    console.log("[broadcast 同意点歌]");
                    if (socket.uType == 50) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'SubmitBroadcast' : {//广播
                    console.log("[broadcast 广播]");
                    clientRedis.get(socket.uid + 'SubmitBroadcast', function (error, res) {
                        if (!error && res == '1') {
                            process_msg(io, socket.roomnum, data_str);
                            clientRedis.del(socket.uid + 'SubmitBroadcast');
                        }
                    });
                    break;
                }
                case 'MoveRoom' : {//转移房间
                    console.log("[broadcast 转移房间]");
                    if (socket.uType == 50 || socket.uType == 40) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'SetchatPublic' : {//开启关闭公聊
                    console.log("[broadcast 开启关闭公聊]");
                    if (socket.uType == 50 || socket.uType == 40) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'CloseLive' : {//关闭直播
                    console.log("[broadcast 关闭直播]");
                    if (socket.uType == 50 || socket.uType == 40) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'SetBulletin' : {//房间公告
                    console.log("[broadcast 房间公告]");
                    if (socket.uType == 50 || socket.uType == 40) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'NoticeMsg' : {//全站礼物
                    console.log("[broadcast 全站礼物]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'KickUser' : {//踢人
                    console.log("[broadcast 踢人]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'ShutUpUser' : {//禁言
                    console.log("[broadcast 禁言]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'channel_gap': {//直播间禁言
                    console.log("[broadcast 直播间禁言]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'changeVest': {//更换马甲
                    console.log("[broadcast 更换马甲]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'stopLive' : {//超管关播
                    console.log("[broadcast 超管关播]");
                    clientRedis.hget("super", socket.uid, function (error, res) {
                        if (error) return;
                        if (res != null) {
                            process_msg(io, socket.roomnum, 'stopplay');
                        }
                    });
                    break;
                }
                case 'DisableAudio' : {//麦手让主播静音
                    // TODO 此处需要验证是否是麦手身份
                    console.log("[broadcast 麦手让主播静音]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'multiStopLive' : {//超管关闭多直播的直播
                    console.log("[broadcast 超管关闭多直播的直播]");
                    clientRedis.hget("super", socket.uid, function (error, res) {
                        if (error) return;
                        if (res != null) {
                            process_msg(io, socket.roomnum, data_str);
                        }
                    });
                    break;
                }
                case 'multiStopMac' : {//超管关闭多直播间喊麦
                    console.log("[broadcast 超管关闭多直播间喊麦]");
                    clientRedis.hget("super", socket.uid, function (error, res) {
                        if (error) return;
                        if (res != null) {
                            process_msg(io, socket.roomnum, data_str);
                        }
                    });
                    break;
                }
                case 'SendPrvMsg' : {//私聊
                    console.log("[broadcast 私聊]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'ResumeUser' : {//恢复发言
                    console.log("[broadcast 恢复发言]");
                    if (socket.uType == 50 || socket.uType == 40) {
                        process_msg(io, socket.roomnum, data_str);
                    }
                    break;
                }
                case 'StartEndLive': {
                    if (socket.uType == 50) {
                        socket.broadcast.to(socket.roomnum).emit('broadcastingListen', [data_str]);
                    } else {
                        clientRedis.get("LiveAuthority" + socket.uid, function (error, res) {
                            if (error) return;
                            if (parseInt(res) == 5 || parseInt(res) == 1 || parseInt(res) == 2) {
                                socket.broadcast.to(socket.roomnum).emit('broadcastingListen', [data_str]);
                            }
                        })
                    }
                    break;
                }
                case 'showuserinfo': {//在线用户
                    console.log("[broadcast 在线用户]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'RowMike': {//排麦
                    console.log("[broadcast 排麦]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'OpenGuard': {//购买守护
                    console.log("[broadcast 购买守护]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'SystemNot': {//系统通知
                    console.log("[broadcast 系统通知]");
                    process_msg(io, socket.roomnum, data_str);
                    break;
                }
                case 'startGame': {//炸金花游戏
                    process_msg(io, socket.roomnum, data_str);
                    var action = msg['action'];
                    if (action == 4) {
                        var time = msg['time'] * 1000;
                        var gameid = msg['gameid'];
                        setTimeout(function () {//定时发送结果
                            var token = msg['token'];
                            clientRedis.get(token + "_Game", function (error, res) {
                                if (!error && res != null) {
                                    var resObj = JSON.parse(res);
                                    dataObj['msg'][0]['ct'] = resObj;
                                    dataObj['msg'][0]['_method_'] = "startGame";
                                    dataObj['msg'][0]['action'] = "6";
                                    var data_str = JSON.stringify(dataObj);
                                    process_msg(io, socket.roomnum, data_str);
                                    request(config['WEBADDRESS'] + "?service=Game.endGame&liveuid=" + socket.uid +
                                        "&token=" + socket.token + "&gameid=" + gameid + "&type=1", function (error, response, body) {
                                    });
                                }
                            });
                        }, time);
                    }
                    break;
                }
                case 'startRotationGame': {//转盘
                    process_msg(io, socket.roomnum, data_str);
                    var action = msg['action'];
                    if (action == 4) {
                        var time = msg['time'] * 1000;
                        var gameid = msg['gameid'];
                        setTimeout(function () {//定时发送结果
                            var token = msg['token'];
                            clientRedis.get(token + "_Game", function (error, res) {
                                if (!error && res != null) {
                                    var resObj = JSON.parse(res);
                                    dataObj['msg'][0]['ct'] = resObj;
                                    dataObj['msg'][0]['_method_'] = "startRotationGame";
                                    dataObj['msg'][0]['action'] = "6";
                                    var data_str = JSON.stringify(dataObj);
                                    process_msg(io, socket.roomnum, data_str);
                                    request(config['WEBADDRESS'] + "?service=Game.endGame&liveuid=" + socket.uid +
                                        "&token=" + socket.token + "&gameid=" + gameid + "&type=1", function (error, response, body) {
                                    });
                                }
                            });
                        }, time);
                    }
                    break;
                }
                case 'startCattleGame': {//开心牛仔
                    process_msg(io, socket.roomnum, data_str);
                    var action = msg['action'];
                    if (action == 4) {
                        var time = msg['time'] * 1000;
                        var gameid = msg['gameid'];
                        setTimeout(function () {//定时发送结果
                            var token = msg['token'];
                            clientRedis.get(token + "_Game", function (error, res) {
                                if (!error && res != null) {
                                    var resObj = JSON.parse(res);
                                    dataObj['msg'][0]['ct'] = resObj;
                                    dataObj['msg'][0]['_method_'] = "startCattleGame";
                                    dataObj['msg'][0]['action'] = "6";
                                    var data_str = JSON.stringify(dataObj);
                                    process_msg(io, socket.roomnum, data_str);
                                    request(config['WEBADDRESS'] + "?service=Game.Cowboy_end&liveuid=" + socket.uid +
                                        "&token=" + socket.token + "&gameid=" + gameid + "&type=1", function (error, response, body) {
                                    });
                                }
                            });
                        }, time);
                    }
                    break;
                }
                case 'startLodumaniGame': {//海盗船长
                    process_msg(io, socket.roomnum, data_str);
                    var action = msg['action'];
                    if (action == 4) {
                        var time = msg['time'] * 1000;
                        var gameid = msg['gameid'];
                        setTimeout(function () {//定时发送结果
                            var token = msg['token'];
                            clientRedis.get(token + "_Game", function (error, res) {
                                if (!error && res != null) {
                                    var resObj = JSON.parse(res);
                                    dataObj['msg'][0]['ct'] = resObj;
                                    dataObj['msg'][0]['_method_'] = "startLodumaniGame";
                                    dataObj['msg'][0]['action'] = "6";
                                    var data_str = JSON.stringify(dataObj);
                                    process_msg(io, socket.roomnum, data_str);
                                    request(config['WEBADDRESS'] + "?service=Game.Cowboy_end&liveuid=" + socket.uid +
                                        "&token=" + socket.token + "&gameid=" + gameid + "&type=1", function (error, response, body) {
                                    });
                                }
                            });
                        }, time);
                    }
                    break;
                }
                case 'startShellGame': {//二八贝
                    process_msg(io, socket.roomnum, data_str);
                    var action = msg['action'];
                    if (action == 4) {
                        var time = msg['time'] * 1000;
                        var gameid = msg['gameid'];
                        setTimeout(function () {//定时发送结果
                            var token = msg['token'];
                            clientRedis.get(token + "_Game", function (error, res) {
                                if (!error && res != null) {
                                    var resObj = JSON.parse(res);
                                    dataObj['msg'][0]['ct'] = resObj;
                                    dataObj['msg'][0]['_method_'] = "startShellGame";
                                    dataObj['msg'][0]['action'] = "6";
                                    var data_str = JSON.stringify(dataObj);
                                    process_msg(io, socket.roomnum, data_str);
                                    request(config['WEBADDRESS'] + "?service=Game.Cowboy_end&liveuid=" + socket.uid +
                                        "&token=" + socket.token + "&gameid=" + gameid + "&type=1", function (error, response, body) {
                                    });
                                }
                            });
                        }, time);
                    }
                    break;
                }
                case 'requestFans': {
                    request(config['WEBADDRESS'] + "?service=Live.getZombie&stream=" + socket.stream + "&uid=" + socket.uid, function (error, response, body) {
                        if (error) return;
                        var res = evalJson(body);
                        if (response.statusCode == 200 && res.data.code == 0) {
                            var data_str = "{\"msg\":[{\"_method_\":\"requestFans\",\"action\":\"3\",\"ct\": " +
                                body + ",\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
                            process_msg(io, socket.roomnum, data_str);
                        }
                    });
                }
            }
        } else {
            console.error("broadcast error because socket.token is null!");
            socket.emit('conn', ['reconnection', '' + socket.id]);
        }
    });
    socket.on('superadminaction', function (data) {
        if (data['token'] == config['TOKEN']) {
            process_msg(io, data['roomnum'], 'stopplay');
        }
    });
    /*系统管理员发送红包*/
    socket.on('superRedPacket', function (data) {
        var dataObj = typeof data == 'object' ? data : evalJson(data);
        var data_str = typeof data == 'object' ? JSON.stringify(data) : data;
        process_msg(io, dataObj.msg[0].roomnum, data_str);
    });
    socket.on('virtualenterRoom', function (data) {
        //取用僵尸粉用户信息data.virtualinfo
        //取出哪个房间的，获取房间流名data.stream
        if (data['token'] == config['TOKEN']) {
            var data_str = "{\"msg\":[{\"_method_\":\"SendMsg\",\"action\":\"0\",\"ct\":{\"id\":\"" +
                data['virtualinfo']['id'] + "\",\"user_nicename\":\"" + data['virtualinfo']['user_nicename'] +
                "\",\"avatar\":\"" + data['virtualinfo']['avatar'] + "\",\"level\":\"" + data['virtualinfo']['level'] +
                "\"},\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
            process_msg(io, data['roomnum'], data_str);
        }
    });
    /* 系统信息 */
    socket.on('systemadmin', function (data) {
        if (data['token'] == config['TOKEN']) {
            io.emit('broadcastingListen', ['{"msg":[{"_method_":"SystemNot","action":"1","ct":"' + data.content +
            '","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
        }
    });
    /* 系统直播间广告 */
    socket.on('systemadvertise', function (data) {
        if (data['token'] == config['TOKEN']) {
            io.emit('broadcastingListen', ['{"msg":[{"_method_":"SystemAdvertise","action":"1","ct":"' + data.content +
            '","url":"' + data.url + '","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
        }
    });
    //资源释放
    socket.on('disconnect', function () {
        numscount--;
        if (numscount < 0) {
            numscount = 0;
        }

        clientRedis.set("totalNums", numscount);
        if (socket.roomnum == null || socket.token == null || socket.roomnum == undefined) {
            return !1;
        }
        console.log("disconnect roomId=" + socket.roomnum);
        d.run(function () {
            /* 连麦 */
            clientRedis.hget('ShowVideo', socket.roomnum, function (error, res) {
                if (!error && (socket.uid == res || socket.uid == socket.roomnum)) {
                    clientRedis.hdel('ShowVideo', socket.roomnum);
                    var data_str = "{\"msg\":[{\"_method_\":\"CloseVideo\",\"action\":\"0\",\"msgtype\":\"20\",\"uid\":\"" +
                        socket.uid + "\",\"uname\":\"" + socket.nicename + "\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
                    console.log("disconnect[ShowVideo]");
                    process_msg(io, socket.roomnum, data_str);
                }
            });

            if (socket.roomnum == socket.uid) {//主播
                if (socket.reusing == 0) {
                    var stopSingleRoomUrl = config['WEBADDRESS'] + "?service=Live.stopRoom";
                    var requestData = {
                        "live_type": 0,
                        "liveid": socket.uid,
                        "uid": socket.uid,
                        "token": socket.token,
                        "stream": socket.stream
                    };
                    console.log('stop single room url: ' + stopSingleRoomUrl, 'data:', requestData);
                    request.post({
                        url: stopSingleRoomUrl,
                        form: requestData,
                        rejectUnauthorized: false
                    }, function (error, response, body) {
                        console.log('stop single room error info:', error, '\n', 'response:', response, 'body:', body);
                    });

                    var data_str = '{"retmsg":"ok","retcode":"000000","msg":[{"msgtype":"1","_method_":"StartEndLive","action":"18","ct":"直播关闭"}]}';
                    console.log("disconnect[" + (socket.live_type == 0 ? "单" : "多") + "直播主播" + socket.uid + "关闭直播]");
                    process_msg(io, socket.roomnum, data_str);
                }
            } else {//观众
                var onlineKey = redis_keys.online + parseInt(socket.live_type) + ':' + parseInt(socket.live_id);
                console.log("disconnect==[" + (socket.live_type == 0 ? "单" : "多") + "直播观众" + socket.uid + "],token[" + socket.token + "],离开房间[" + socket.roomnum + "].");
                if (socket.uid) {
                    console.log("直播观众" + socket.uid + "[socket.sign=" + socket.sign + "]");
                    clientRedis.hget(onlineKey, socket.uid, function (error, res) {
                        if (error) return;
                        if (res != null) {
                            var user = JSON.parse(res);
                            var data_str = "{\"msg\":[{\"_method_\":\"disconnect\",\"action\":\"flushOnlineList\",\"ct\":{\"id\":\"" +
                                user['id'] + "\",\"user_nicename\":\"" + user['user_nicename'] + "\",\"avatar\":\"" +
                                user['avatar'] + "\",\"level\":\"" + user['level'] + "\"},\"msgtype\":\"0\",\"uid\":\"" +
                                socket.uid + "\",\"uname\":\"" + user.user_nicename + "\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
                            process_msg(io, socket.roomnum, data_str);
                        }
                        console.log("离开后删除用户键:" + onlineKey + ",socket.uid=" + socket.uid);
                        clientRedis.hdel(onlineKey, socket.uid);
                    });
                }
            }

            socket.leave(socket.roomnum);
            delete io.sockets.sockets[socket.id];
            sockets[socket.uid] = null;
            delete sockets[socket.uid];
        });
    });
});

function sendSystemMsg(socket, msg) {//resend是為了安卓用戶進入直播間時系統消息出現兩次而用來判斷的變量
    socket.emit('broadcastingListen', ["{\"msg\":[{\"_method_\":\"SystemNot\",\"action\":\"1\",\"resend\":\"1\",\"ct\":\"" +
    msg + "\",\"msgtype\":\"4\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}"]);
}

function evalJson(data) {
    var json = {};
    try {
        json = eval("(" + data + ")");
    } catch (e) {
        console.error(e);
        console.error(data);
    }
    return json;
}

function process_msg(io, roomnum, data) {
    if (!chat_history[roomnum]) {
        chat_history[roomnum] = [];
    }
    chat_history[roomnum].push(data);
    chat_interval[roomnum] || (chat_interval[roomnum] = setInterval(function () {
        if (chat_history[roomnum].length > 0) {
            send_msg(io, roomnum);
        } else {
            clearInterval(chat_interval[roomnum]);
            chat_interval[roomnum] = null;
        }
    }, 200));
}

function send_msg(io, roomnum) {
    var data = chat_history[roomnum].splice(0, chat_history[roomnum].length);
    io.sockets.in(roomnum).emit("broadcastingListen", data);
}

function filter(str) {
    // 多个敏感词，这里直接以数组的形式展示出来
    var arrMg = field;
    var showContent = str;
    // 正则表达式
    // \d 匹配数字
    if (arrMg && arrMg != undefined) {
        for (var i = 0; i < arrMg.length; i++) {
            // 创建一个正则表达式
            var r = new RegExp(arrMg[i], "ig");
            var re = '';
            for (var n = 0; n < arrMg[i].length; n++) {
                re += '*';
            }
            showContent = showContent.replace(r, re);
        }
    }
    return showContent;
}